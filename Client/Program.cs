﻿using System;

namespace Client
{
    public static class Program
    {
        [STAThread]
        static void Main(string[] arg)
        {
            bool updated = (arg.Length > 0 && arg[0] == "UpToDate") ? true : false;
            using (var game = new Helbreath(updated))
            {
                game.Run();
            }
        }
    }
}
