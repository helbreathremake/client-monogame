﻿using Client.Assets;
using Client.Assets.Game;
using Client.Assets.State;
using Client.Assets.UI;
using Helbreath.Common;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Reflection;
using System.Xml;

namespace Client
{
    /// <summary>
    /// This is the main type for your game
    /// </summary>
    public class Helbreath : Game
    {
        private GraphicsDeviceManager graphics;
        private SpriteBatch spriteBatch;
        private GameDisplay display;

        private bool updated;
        private string username;
        private string password;
        private string selectedCharacter;
        private List<Player> characters;
        private string loginAddress;
        private string gameAddress;
        private bool resolveDNS;
        private bool addressOverride;
        private int loginPort;
        private int gamePort;

        //BACKGROUND
        private LinkedList<BackGroundImage> backgroundImages;
        private BackGroundImage[] backgroundcollection;
        private BackGroundImage background;
        private BackGroundImage backgroundblackbar;
        private BackGroundImage legs;
        private BackGroundImage weapon;
        private BackGroundImage tree;
        private BackGroundImage grass;
        private BackGroundImage grass2;
        private BackGroundImage grass3;
        private BackGroundImage smoke1;
        private BackGroundImage smoke2;
        private BackGroundImage smoke3;
        private BackGroundImage smoke4;
        private BackGroundImage smoke5;
        private BackGroundImage smoke6;
        private BackGroundImage smoke7;
        private BackGroundImage smoke8;
        private BackGroundImage logo;
        private BackGroundImage logoglow;
        private BackGroundImage warriorHead;
        private BackGroundImage warriorBody;

        private Texture2D backgroundTexture;
        private Texture2D backgroundblackbarTexture;
        private Texture2D legsTexture;
        private Texture2D weaponTexture;
        private Texture2D treeTexture;
        private Texture2D grassTexture;
        private Texture2D grass2Texture;
        private Texture2D grass3Texture;
        private Texture2D smoke1Texture;
        private Texture2D smoke2Texture;
        private Texture2D smoke3Texture;
        private Texture2D smoke4Texture;
        private Texture2D smoke5Texture;
        private Texture2D smoke6Texture;
        private Texture2D smoke7Texture;
        private Texture2D smoke8Texture;
        private Texture2D logoTexture;
        private Texture2D logoglowTexture;
        private Texture2D warriorHeadTexture;
        private Texture2D warriorBodyTexture;

        private Texture2D corner;
        private Texture2D textbox;
        private Texture2D dialogFader;
        private Texture2D dialogBorder;

        private bool fadeDirection;
        private bool fadeStage;
        private int fadeDelay = 10;
        private int backgroundDelay = 16;

        //EFFECTS
        private Effect berserkShader;
        private Effect weaponShader;
        EffectParameter weaponParameters;

        //DEFAULT STATE
        private IDefaultState defaultState;

        // keyboard
        private KeyboardDispatcher keyboardDispatcher;

        private MainMenu mainMenuCache;
        //private RenderTarget2D mainRenderTarget;


        public Helbreath(bool updated)
        {
            this.updated = true; //When auto updater is fixed, change this //TODO
            base.IsFixedTimeStep = false;
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            base.Initialize();
        }


        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            spriteBatch = new SpriteBatch(GraphicsDevice);

            //LOAD GAME SETTINGS
            LoadGameSettings();

            // LOAD LOGIN CONFIG
            LoadLoginConfiguration();

            // LOAD MOUSE
            SpriteFile interfaceSprites = new SpriteFile("Sprites\\interface.spr");
            GameMouse mouse = new GameMouse(Texture2D.FromStream(GraphicsDevice, new MemoryStream(interfaceSprites[0].ImageData)));
            foreach (SpriteFrame frame in interfaceSprites[0].Frames)
                mouse.Frames.Add(new AnimationFrame(frame.Left, frame.Top, frame.Width, frame.Height, frame.PivotX, frame.PivotY));

            //// LOAD KEYBOARD
            GameKeyboard keyboard = new GameKeyboard();
            keyboardDispatcher  = new KeyboardDispatcher(this.Window,100,5);
            keyboard.Dispatcher = keyboardDispatcher;

            // LOAD FONTS
            //SpriteFile interface2Sprites = new SpriteFile("Sprites\\interface2.spr");
            //GameFont font = new GameFont(Texture2D.FromStream(GraphicsDevice, new MemoryStream(interface2Sprites[1].ImageData)));
            //font.InitCharacters(interface2Sprites[1].Frames);

            // LOAD INTERFACE
            display = new GameDisplay(Assets.Cache.GameSettings.Resolution, Assets.Cache.GameSettings.FullScreen);
            display.Device = GraphicsDevice;
            display.DeviceManager = graphics;
            //gameInterface.GameFont = font;

            //FONTS
            Assets.Cache.Fonts = new Dictionary<FontType, SpriteFont>();
            Assets.Cache.Fonts.Add(FontType.GeneralSize10, Content.Load<SpriteFont>("Fonts\\General-Size-10"));
            Assets.Cache.Fonts.Add(FontType.GeneralSize10Bold, Content.Load<SpriteFont>("Fonts\\General-Size-10-Bold"));
            Assets.Cache.Fonts.Add(FontType.DamageSmallSize11, Content.Load<SpriteFont>("Fonts\\DamageSmall-Size-11"));
            Assets.Cache.Fonts.Add(FontType.DamageMediumSize13, Content.Load<SpriteFont>("Fonts\\DamageMedium-Size-13"));
            Assets.Cache.Fonts.Add(FontType.DamageLargeSize14Bold, Content.Load<SpriteFont>("Fonts\\DamageLarge-Size-14-Bold"));
            Assets.Cache.Fonts.Add(FontType.MagicMedieval14, Content.Load<SpriteFont>("Fonts\\MagicMedieval14"));
            Assets.Cache.Fonts.Add(FontType.MagicMedieval18, Content.Load<SpriteFont>("Fonts\\MagicMedieval18"));
            Assets.Cache.Fonts.Add(FontType.MagicMedieval24, Content.Load<SpriteFont>("Fonts\\MagicMedieval24"));
            Assets.Cache.Fonts.Add(FontType.MagicMedieval40, Content.Load<SpriteFont>("Fonts\\MagicMedieval40"));
            Assets.Cache.Fonts.Add(FontType.DisplayNameSize13Spacing1, Content.Load<SpriteFont>("Fonts\\DisplayName-Size-13-Spacing-1"));
            Assets.Cache.Fonts.Add(FontType.DialogSize8Bold, Content.Load<SpriteFont>("Fonts\\Dialog-Size-8-Bold"));
            Assets.Cache.Fonts.Add(FontType.DialogsSmallSize8, Content.Load<SpriteFont>("Fonts\\DialogSmall-Size-8"));
            Assets.Cache.Fonts.Add(FontType.DialogsSmallerSize7, Content.Load<SpriteFont>("Fonts\\DialogSmaller-Size-7"));

            //GAMECOLORS
            Assets.Cache.Colors = ConfigurationHelper.LoadGameColors();

            display.Mouse = mouse;
            display.Keyboard = keyboard;

            Assets.Cache.BackgroundTexture = new Texture2D(GraphicsDevice, 1, 1);
            Assets.Cache.BackgroundTexture.SetData(new[] { Color.White });

            // SET UP GRAPHICS DEVICE
            graphics.PreferredBackBufferWidth = display.ResolutionWidth;
            graphics.PreferredBackBufferHeight = display.ResolutionHeight;
            graphics.IsFullScreen = display.FullScreen;
            graphics.SynchronizeWithVerticalRetrace = false;
            graphics.ApplyChanges();

            //LOAD EFFECTS
            berserkShader = Content.Load<Effect>("Berserk");
            weaponShader = Content.Load<Effect>("Weapon");
            weaponParameters = weaponShader.Parameters["weaponColorTint"];

            // LOAD SPRITE CACHE DICTIONARIES
            Assets.Cache.Tiles = new Dictionary<int, Tile>();
            Assets.Cache.SpriteQueue = new Queue<SpriteLoad>();
            Assets.Cache.HumanAnimations = new Dictionary<int, Animation>();
            Assets.Cache.MonsterAnimations = new Dictionary<int, Animation>();
            Assets.Cache.Equipment = new Dictionary<int, Animation>();
            Assets.Cache.Effects = new Dictionary<int, Animation>();
            Assets.Cache.Interface = new Dictionary<int, Animation>();
            Assets.Cache.MiniMaps = new Dictionary<string, Texture2D>();
            Assets.Cache.ZoomMaps = new Dictionary<string, Texture2D>();
            Assets.Cache.Music = new Dictionary<string, SoundEffect>();
            Assets.Cache.SoundEffects = new Dictionary<string, SoundEffect>();
            Assets.Cache.PixelShaders = new Dictionary<int, Effect>();
            Assets.Cache.PixelShaders.Add((int)PixelShader.Berserk, berserkShader);
            Assets.Cache.PixelShaders.Add((int)PixelShader.Weapon, weaponShader);
            Assets.Cache.ShaderParameters = new Dictionary<Effect, EffectParameter>();
            Assets.Cache.ShaderParameters.Add(weaponShader, weaponParameters);
            Assets.Cache.TransparencyFaders = new TransparencyFaders();

            // SET UP FIRST SCREEN
            Loading loading = new Loading(updated);
            loading.Content = this.Content;
            loading.Display = display;
            loading.MainRenderTarget = LoadRenderTarget2D(loading.MainRenderTarget);

            //LOAD BACKGROUND
            LoadBackGroundTextures();
            LoadBackGroundImages(display.Resolution);
            loading.BackgroundImages = backgroundImages;
            loading.DialogBorder = DialogBorder;
            loading.DialogFader = DialogFader;
            loading.Textbox = Textbox;
            loading.Corner = Corner;
            loading.Init(Window);

            defaultState = (IDefaultState)loading;

        }

        private void LoadLoginConfiguration()
        {
            try
            {
                string installPath = Path.GetDirectoryName(Assembly.GetExecutingAssembly().GetName().CodeBase).Replace(@"file:\", "");

                if (!File.Exists(installPath + @"\login.xml")) return;

                XmlDocument document = new XmlDocument();
                document.Load(installPath + @"\login.xml");

                // server
                resolveDNS = false;
                XmlNode node = document.DocumentElement.SelectSingleNode("//Server");
                if (node.Attributes["ResolveDNS"] != null)
                    resolveDNS = Boolean.Parse(node.Attributes["ResolveDNS"].InnerText);

                if (node.Attributes["Override"] != null)
                    addressOverride = Boolean.Parse(node.Attributes["Override"].InnerText);

                if (resolveDNS)
                {

                    IPHostEntry hostEntry = new IPHostEntry();
                    hostEntry.AddressList = Dns.GetHostAddresses(node.InnerText);

                    if (hostEntry.AddressList.Length > 0)
                    {
                        loginAddress = gameAddress = hostEntry.AddressList[0].ToString();
                    }
                }
                else
                {
                    loginAddress = gameAddress = node.InnerText;
                }

                // port
                node = document.DocumentElement.SelectSingleNode("//Port");
                if (Int32.TryParse(node.InnerText, out loginPort)) gamePort = loginPort;
            }
            catch
            {
            }
        }

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// all content.
        /// </summary>
        protected override void UnloadContent()
        {
            // TODO: Unload any non ContentManager content here
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
            switch (defaultState.State)
            {
                case DefaultState.Loading:
                    // CHECK IF LOADING IS COMPLETE, THEN GO TO MAIN MENU
                    if (defaultState.IsComplete)
                    {
                        MainMenu menu = new MainMenu(loginAddress, loginPort);
                        menu.Exit += OnExit;
                        menu.Display = display;
                        menu.MainRenderTarget = LoadRenderTarget2D(menu.MainRenderTarget);

                        Loading load = (Loading)defaultState;
                        defaultState = (IDefaultState)menu;
                        menu.Init(Window);


                        //Background
                        menu.DialogBorder = DialogBorder;
                        menu.DialogFader = DialogFader;
                        menu.Textbox = Textbox;
                        menu.Corner = Corner;
                        menu.BackgroundImages = BackgroundImages;

                        mainMenuCache = menu; // store for log out
                    }
                    break;
                case DefaultState.MainMenu:
                    if (defaultState.ResolutionChange)
                    {
                        MainMenu oldMenuState = (MainMenu)defaultState;
                        MainMenu menu = new MainMenu(loginAddress, loginPort);
                        switch (display.Resolution)
                        {
                            case Resolution.Classic:
                                {
                                    Assets.Cache.GameSettings.Resolution = display.Resolution;
                                    display.DeviceManager.PreferredBackBufferWidth = 640;
                                    display.DeviceManager.PreferredBackBufferHeight = 480;
                                    display.DeviceManager.ApplyChanges();
                                    break;
                                }
                            case Resolution.Standard:
                                {
                                    Assets.Cache.GameSettings.Resolution = display.Resolution;
                                    display.DeviceManager.PreferredBackBufferWidth = 800;
                                    display.DeviceManager.PreferredBackBufferHeight = 600;
                                    display.DeviceManager.ApplyChanges();
                                    break;
                                }
                            case Resolution.Large:
                                {
                                    Assets.Cache.GameSettings.Resolution = display.Resolution;
                                    display.DeviceManager.PreferredBackBufferWidth = 1024;
                                    display.DeviceManager.PreferredBackBufferHeight = 768;
                                    display.DeviceManager.ApplyChanges();
                                    break;
                                }
                        }

                        menu.Display = display;
                        menu.MainRenderTarget = LoadRenderTarget2D(menu.MainRenderTarget);
                        LoadBackGroundImages(display.Resolution);
                        menu.BackgroundImages = backgroundImages;
                        menu.DialogBorder = DialogBorder;
                        menu.DialogFader = DialogFader;
                        menu.Textbox = Textbox;
                        menu.Corner = Corner;
                        menu.Init(Window);

                        foreach (KeyValuePair<MenuDialogBoxType, IMenuDialogBox> box in menu.DialogBoxes)
                        {
                            if (box.Key != MenuDialogBoxType.Settings)
                            {
                                box.Value.Hide();
                            }
                        }
                        menu.DialogBoxes[MenuDialogBoxType.Settings].Show();

                        defaultState = (IDefaultState)menu;
                        defaultState.ResolutionChange = false;
                    }

                    // CHECK IF LOGIN IS COMPLETE, THEN GO TO GAME
                    if (defaultState.IsComplete)
                    {
                        username = ((MainMenu)defaultState).Username;
                        password = ((MainMenu)defaultState).Password;
                        characters = ((MainMenu)defaultState).Characters;

                        CharacterSelect select = new CharacterSelect(loginAddress, loginPort, username, characters);
                        select.Display = display;
                        select.MainRenderTarget = LoadRenderTarget2D(select.MainRenderTarget);
                        MainMenu menu = (MainMenu)defaultState;
                        defaultState = (IDefaultState)select;
                        select.Init(Window);

                        select.DialogBorder = DialogBorder;
                        select.DialogFader = DialogFader;
                        select.Textbox = Textbox;
                        select.Corner = Corner;
                        select.BackgroundImages = BackgroundImages;
                    }
                    break;
                case DefaultState.CharacterSelect:
                    if (defaultState.Back)
                    {
                        MainMenu menu = new MainMenu(loginAddress, loginPort);
                        menu.Display = display;
                        menu.MainRenderTarget = LoadRenderTarget2D(menu.MainRenderTarget);
                        CharacterSelect select = (CharacterSelect)defaultState;
                        defaultState = (IDefaultState)menu;
                        menu.Init(Window);

                        menu.DialogBorder = DialogBorder;
                        menu.DialogFader = DialogFader;
                        menu.Textbox = Textbox;
                        menu.Corner = Corner;
                        menu.BackgroundImages = BackgroundImages;
                    }
                    else if (defaultState.IsComplete)
                    {
                        selectedCharacter = ((CharacterSelect)defaultState).SelectedCharacter;
                        gameAddress = (addressOverride ? loginAddress : ((CharacterSelect)defaultState).GameServerAddress);
                        gamePort = ((CharacterSelect)defaultState).GameServerPort;

                        MainGame game = new MainGame(gameAddress, gamePort);
                        game.Display = display;
                        game.MainRenderTarget = LoadRenderTarget2D(game.MainRenderTarget, RenderTargetUsage.PreserveContents);
                        game.RenderTargetObjects = LoadRenderTarget2D(game.RenderTargetObjects, RenderTargetUsage.DiscardContents);

                        game.Player = new Player(username, password, selectedCharacter);
                        defaultState = (IDefaultState)game;
                        game.Disconnect += new EventHandler(Disconnect);
                        game.Init(Window);
                    }
                    break;
                case DefaultState.Playing:
                    {
                        if (defaultState.ResolutionChange)
                        {
                            MainGame oldGameState = (MainGame)defaultState;
                            switch (oldGameState.Display.Resolution)
                            {
                                case Resolution.Classic:
                                    {
                                        Assets.Cache.GameSettings.Resolution = display.Resolution;
                                        oldGameState.Display.DeviceManager.PreferredBackBufferWidth = 640;
                                        oldGameState.Display.DeviceManager.PreferredBackBufferHeight = 480;
                                        oldGameState.Display.DeviceManager.ApplyChanges();
                                        break;
                                    }
                                case Resolution.Standard:
                                    {
                                        Assets.Cache.GameSettings.Resolution = display.Resolution;
                                        oldGameState.Display.DeviceManager.PreferredBackBufferWidth = 800;
                                        oldGameState.Display.DeviceManager.PreferredBackBufferHeight = 600;
                                        oldGameState.Display.DeviceManager.ApplyChanges();
                                        break;
                                    }
                                case Resolution.Large:
                                    {
                                        Assets.Cache.GameSettings.Resolution = display.Resolution;
                                        oldGameState.Display.DeviceManager.PreferredBackBufferWidth = 1024;
                                        oldGameState.Display.DeviceManager.PreferredBackBufferHeight = 768;
                                        oldGameState.Display.DeviceManager.ApplyChanges();
                                        break;
                                    }
                            }

                            LoadBackGroundImages(display.Resolution);
                            oldGameState.MainRenderTarget = LoadRenderTarget2D(oldGameState.MainRenderTarget, RenderTargetUsage.PreserveContents);
                            oldGameState.RenderTargetObjects = LoadRenderTarget2D(oldGameState.RenderTargetObjects, RenderTargetUsage.DiscardContents);
                            oldGameState.BackgroundImages = BackgroundImages;
                            oldGameState.UpdateResolution();
                            defaultState = (IDefaultState)oldGameState;
                            defaultState.ResolutionChange = false;
                        }
                        break;
                    }
            }
            defaultState.Update(gameTime, this);
            base.Update(gameTime);
        }

        private void OnExit(object sender, EventArgs e)
        {
            Exit();
        }

        private void Disconnect(object sender, EventArgs e)
        {
            defaultState = (IDefaultState)mainMenuCache;
        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            //Set the current render target to our mainRenderTarget
            graphics.GraphicsDevice.SetRenderTarget(defaultState.MainRenderTarget);
            //Clear the render target (make it all black)
            graphics.GraphicsDevice.Clear(Color.Black);
            //Start our spritebatch
            //Note: Add parameters to this as is necessary for your game
            spriteBatch.Begin(SpriteSortMode.Deferred, null, null, null, null, null);
            //...
            //Do all your drawing here
            base.Draw(gameTime);
            if (!defaultState.ResolutionChange)
            {
                defaultState.Draw(spriteBatch, gameTime);
            }

            //End the spritebatch
            spriteBatch.End();
            //Set the render target to the back buffer again by passing null
            graphics.GraphicsDevice.SetRenderTarget(null);
            //Clear the back buffer
            graphics.GraphicsDevice.Clear(Color.Black);
            //Start the spritebatch we're going to use to draw what we've
            //   rendered (using a cool effect) to the screen
            //Note: the SpriteSortMode and BlendState may be different in
            //   your case.
            spriteBatch.Begin(SpriteSortMode.Deferred, null,
                              null, null, null, null);
            //DrawItemPopup the render target to the screen with no tint
            spriteBatch.Draw(defaultState.MainRenderTarget, Vector2.Zero, Color.White);
            //if (defaultState.State == defaultState.Playing) { spriteBatch.DrawItemPopup(defaultState.MainRenderTarget, display.Device.Viewport.Bounds, display.Zoom, Color.White); }
            //else { spriteBatch.DrawItemPopup(defaultState.MainRenderTarget, Vector2.Zero, Color.White); }
            //End the spritebatch
            spriteBatch.End();

            //OLD CODE HERE
            //base.DrawItemPopup(gameTime);

            //GraphicsDevice.Clear(Color.Black);

            //spriteBatch.Begin();
            //if (!defaultState.ResolutionChange)
            //{
            //    defaultState.DrawItemPopup(spriteBatch, gameTime);
            //}
            //spriteBatch.End();
        }

        private void LoadBackGroundTextures()
        {
            BackgroundImages = new LinkedList<BackGroundImage>();
            SpriteFile backgroundSprites = new SpriteFile("Sprites\\loadingbg.spr");
            backgroundTexture = Texture2D.FromStream(GraphicsDevice, new MemoryStream(backgroundSprites[0].ImageData));
            backgroundblackbarTexture = Texture2D.FromStream(GraphicsDevice, new MemoryStream(backgroundSprites[1].ImageData));
            legsTexture = Texture2D.FromStream(GraphicsDevice, new MemoryStream(backgroundSprites[2].ImageData));
            weaponTexture = Texture2D.FromStream(GraphicsDevice, new MemoryStream(backgroundSprites[3].ImageData));
            warriorHeadTexture = Texture2D.FromStream(GraphicsDevice, new MemoryStream(backgroundSprites[4].ImageData));
            warriorBodyTexture = Texture2D.FromStream(GraphicsDevice, new MemoryStream(backgroundSprites[5].ImageData));
            treeTexture = Texture2D.FromStream(GraphicsDevice, new MemoryStream(backgroundSprites[6].ImageData));
            grassTexture = Texture2D.FromStream(GraphicsDevice, new MemoryStream(backgroundSprites[7].ImageData));
            grass2Texture = Texture2D.FromStream(GraphicsDevice, new MemoryStream(backgroundSprites[7].ImageData));
            grass3Texture = Texture2D.FromStream(GraphicsDevice, new MemoryStream(backgroundSprites[7].ImageData));
            smoke1Texture = Texture2D.FromStream(GraphicsDevice, new MemoryStream(backgroundSprites[8].ImageData)).PreMultiply();
            smoke2Texture = Texture2D.FromStream(GraphicsDevice, new MemoryStream(backgroundSprites[8].ImageData)).PreMultiply();
            smoke3Texture = Texture2D.FromStream(GraphicsDevice, new MemoryStream(backgroundSprites[9].ImageData)).PreMultiply();
            smoke4Texture = Texture2D.FromStream(GraphicsDevice, new MemoryStream(backgroundSprites[9].ImageData)).PreMultiply();
            smoke5Texture = Texture2D.FromStream(GraphicsDevice, new MemoryStream(backgroundSprites[8].ImageData)).PreMultiply();
            smoke6Texture = Texture2D.FromStream(GraphicsDevice, new MemoryStream(backgroundSprites[8].ImageData)).PreMultiply();
            smoke7Texture = Texture2D.FromStream(GraphicsDevice, new MemoryStream(backgroundSprites[9].ImageData)).PreMultiply();
            smoke8Texture = Texture2D.FromStream(GraphicsDevice, new MemoryStream(backgroundSprites[9].ImageData)).PreMultiply();

            dialogBorder = Texture2D.FromStream(GraphicsDevice, new MemoryStream(backgroundSprites[10].ImageData));
            corner = Texture2D.FromStream(GraphicsDevice, new MemoryStream(backgroundSprites[11].ImageData)).PreMultiply();
            dialogFader = Texture2D.FromStream(GraphicsDevice, new MemoryStream(backgroundSprites[12].ImageData)).PreMultiply();
            textbox = Texture2D.FromStream(GraphicsDevice, new MemoryStream(backgroundSprites[13].ImageData)).PreMultiply();
        }
        private void LoadBackGroundImages(Resolution resolution)
        {
            int height = display.ResolutionHeight;
            int width = display.ResolutionWidth;

            switch (resolution)
            {
                //4:3 aspect ratio (1440 x 1080)
                case Resolution.Classic: //640 x 480
                    {
                        background = new BackGroundImage(BackgroundType.Stationary, 0, 0, 0.444444f, new Microsoft.Xna.Framework.Rectangle(240, 0, 1440, 1080));
                        backgroundblackbar = new BackGroundImage(BackgroundType.Stationary, height - 10);
                        legs = new BackGroundImage(BackgroundType.Legs, width - 120 - 50, height - 135 - 30, 0.5f);
                        weapon = new BackGroundImage(BackgroundType.Weapon, width - 120 + 15, height - 165 - 15, new Vector2(175, 290), 0.5f);
                        warriorHead = new BackGroundImage(BackgroundType.Head, width - 120 + 5, height - 210 - 28, 0.5f);
                        warriorBody = new BackGroundImage(BackgroundType.Body, width - 120 - 13, height - 190 - 25, 0.5f);
                        grass = new BackGroundImage(BackgroundType.Grass, 0, height - 84 - 5);
                        grass2 = new BackGroundImage(BackgroundType.Grass, 480, height - 84, 41);
                        grass3 = new BackGroundImage(BackgroundType.Grass, 800, height - 84 - 10, 41);
                        tree = new BackGroundImage(BackgroundType.Stationary, -150, height - 300 - 80, 0.4f);
                        smoke1 = new BackGroundImage(BackgroundType.Smoke, height - 1080 + 400, 0.8f);
                        smoke2 = new BackGroundImage(BackgroundType.Smoke, 50, height - 1080 + 400, 0.8f);
                        smoke3 = new BackGroundImage(BackgroundType.Smoke, height - 1080 + 400, 0.8f);
                        smoke4 = new BackGroundImage(BackgroundType.Smoke, 50, height - 1080 + 400, 0.8f);
                        smoke5 = new BackGroundImage(BackgroundType.Smokebehind, height - 1080 + 250, 0.8f);
                        smoke6 = new BackGroundImage(BackgroundType.Smokebehind, 50, height - 1080 + 250, 0.8f);
                        smoke7 = new BackGroundImage(BackgroundType.Smokebehind, height - 1080 + 250, 0.8f);
                        smoke8 = new BackGroundImage(BackgroundType.Smokebehind, 50, height - 1080 + 250, 0.8f);
                        break;
                    }
                case Resolution.Standard: //800 x 600
                    {
                        background = new BackGroundImage(BackgroundType.Stationary, 0, 0, 0.555555f, new Microsoft.Xna.Framework.Rectangle(240, 0, 1440, 1080));
                        backgroundblackbar = new BackGroundImage(BackgroundType.Stationary, height - 10);
                        legs = new BackGroundImage(BackgroundType.Legs, width - 200 - 50, height - 200, 0.7f);
                        weapon = new BackGroundImage(BackgroundType.Weapon, width - 200 + 40, height - 294 + 55, new Vector2(175, 290), 0.7f);
                        warriorHead = new BackGroundImage(BackgroundType.Head, width - 200 + 33, height - 264 - 48, 0.7f);
                        warriorBody = new BackGroundImage(BackgroundType.Body, width - 200 + 10, height - 275, 0.7f);
                        grass = new BackGroundImage(BackgroundType.Grass, 0, height - 84 - 5);
                        grass2 = new BackGroundImage(BackgroundType.Grass, 480, height - 84, 41);
                        grass3 = new BackGroundImage(BackgroundType.Grass, 800, height - 84 - 10, 41);
                        tree = new BackGroundImage(BackgroundType.Stationary, -(1067 / 2), height - 765 - 50);
                        smoke1 = new BackGroundImage(BackgroundType.Smoke, height - 1080 + 230);
                        smoke2 = new BackGroundImage(BackgroundType.Smoke, 50, height - 1080 + 230);
                        smoke3 = new BackGroundImage(BackgroundType.Smoke, height - 1080 + 230);
                        smoke4 = new BackGroundImage(BackgroundType.Smoke, 50, height - 1080 + 230);
                        smoke5 = new BackGroundImage(BackgroundType.Smokebehind, height - 1080 - 40);
                        smoke6 = new BackGroundImage(BackgroundType.Smokebehind, 50, height - 1080 - 40);
                        smoke7 = new BackGroundImage(BackgroundType.Smokebehind, height - 1080 - 40);
                        smoke8 = new BackGroundImage(BackgroundType.Smokebehind, 50, height - 1080 - 40);
                        break;
                    }
                case Resolution.Large: //1024 x 768
                    {
                        background = new BackGroundImage(BackgroundType.Stationary, 0, 0, 0.711111f, new Microsoft.Xna.Framework.Rectangle(240, 0, 1440, 1080));
                        backgroundblackbar = new BackGroundImage(BackgroundType.Stationary, height - 10);
                        legs = new BackGroundImage(BackgroundType.Legs, width - 367 - 50 + 100, height - 255);
                        weapon = new BackGroundImage(BackgroundType.Weapon, width - 367 + 60 + 100, height - 364 + 55, new Vector2(175, 290));
                        warriorHead = new BackGroundImage(BackgroundType.Head, width - 367 + 60 + 100, height - 364 - 53);
                        warriorBody = new BackGroundImage(BackgroundType.Body, width - 367 + 25 + 100, height - 364);
                        grass = new BackGroundImage(BackgroundType.Grass, 0, height - 84 - 5);
                        grass2 = new BackGroundImage(BackgroundType.Grass, 480, height - 84, 41);
                        grass3 = new BackGroundImage(BackgroundType.Grass, 800, height - 84 - 10, 41);
                        tree = new BackGroundImage(BackgroundType.Stationary, -550, height - 810);
                        smoke1 = new BackGroundImage(BackgroundType.Smoke, height - 1080 + 230);
                        smoke2 = new BackGroundImage(BackgroundType.Smoke, 50, height - 1080 + 230);
                        smoke3 = new BackGroundImage(BackgroundType.Smoke, height - 1080 + 230);
                        smoke4 = new BackGroundImage(BackgroundType.Smoke, 50, height - 1080 + 230);
                        smoke5 = new BackGroundImage(BackgroundType.Smokebehind, height - 1080 - 40);
                        smoke6 = new BackGroundImage(BackgroundType.Smokebehind, 50, height - 1080 - 40);
                        smoke7 = new BackGroundImage(BackgroundType.Smokebehind, height - 1080 - 40);
                        smoke8 = new BackGroundImage(BackgroundType.Smokebehind, 50, height - 1080 - 40);
                        break;
                    }

                    //16:9 aspect ratio (1920 x 1080)
            }

            background.Texture = backgroundTexture;
            backgroundblackbar.Texture = backgroundblackbarTexture;
            backgroundblackbar.UpdateSource();
            legs.Texture = legsTexture;
            legs.UpdateSource();
            weapon.Texture = weaponTexture;
            weapon.UpdateSource();
            warriorHead.Texture = warriorHeadTexture;
            warriorHead.UpdateSource();
            warriorBody.Texture = warriorBodyTexture;
            warriorBody.UpdateSource();
            tree.Texture = treeTexture;
            tree.UpdateSource();
            grass.Texture = grassTexture;
            grass.UpdateSource();
            grass2.Texture = grass2Texture;
            grass2.UpdateSource();
            grass3.Texture = grass3Texture;
            grass3.UpdateSource();
            smoke1.Texture = smoke1Texture;
            smoke1.UpdateSource();
            smoke2.Texture = smoke2Texture;
            smoke2.UpdateSource();
            smoke3.Texture = smoke3Texture;
            smoke3.UpdateSource();
            smoke4.Texture = smoke4Texture;
            smoke4.UpdateSource();
            smoke5.Texture = smoke5Texture;
            smoke5.UpdateSource();
            smoke6.Texture = smoke6Texture;
            smoke6.UpdateSource();
            smoke7.Texture = smoke7Texture;
            smoke7.UpdateSource();
            smoke8.Texture = smoke8Texture;
            smoke8.UpdateSource();

            backgroundImages.Clear();
            backgroundImages.AddFirst(background);
            backgroundImages.AddLast(smoke2);
            backgroundImages.AddLast(grass3);
            backgroundImages.AddLast(smoke1);
            backgroundImages.AddLast(smoke4);
            backgroundImages.AddLast(smoke6);
            backgroundImages.AddLast(smoke8);

            backgroundImages.AddLast(weapon);
            backgroundImages.AddLast(warriorBody);
            backgroundImages.AddLast(legs);
            backgroundImages.AddLast(warriorHead);

            backgroundImages.AddLast(grass);
            backgroundImages.AddLast(smoke3);
            backgroundImages.AddLast(smoke5);
            backgroundImages.AddLast(smoke7);
            backgroundImages.AddLast(grass2);
            backgroundImages.AddLast(backgroundblackbar);
        }

        private bool LoadGameSettings()
        {
            try
            {
                string configPath = Path.GetDirectoryName(Assembly.GetExecutingAssembly().GetName().CodeBase).Replace(@"file:\", "") + @"\Configs\gamesettings.xml";

                Assets.Cache.GameSettings = ConfigurationHelper.LoadGameSettings(configPath);

                return true;
            }
            catch (Exception ex)
            {
                Assets.Cache.GameSettings = GameSettings.Default();
                return false;
            }
        }

        protected override void OnExiting(object sender, EventArgs args)
        {
            switch (defaultState.State)
            {
                case DefaultState.Loading:
                    break;
                case DefaultState.MainMenu:
                    break;
                case DefaultState.CharacterSelect:
                    break;
                case DefaultState.Playing:
                    return;
            }

            base.OnExiting(sender, args);
        }

        public RenderTarget2D LoadRenderTarget2D(RenderTarget2D renderTarget)
        {
            //Get the current presentation parameters
            PresentationParameters pp = display.Device.PresentationParameters;
            //Create our new render target
            renderTarget = new RenderTarget2D(display.Device,
                     pp.BackBufferWidth, //Same width as backbuffer
                     pp.BackBufferHeight, //Same height
                     false, //No mip-mapping
                     pp.BackBufferFormat, //Same colour format
                     pp.DepthStencilFormat, //Same depth stencil
                     pp.MultiSampleCount,
                     RenderTargetUsage.PreserveContents);
            return renderTarget;
        }

        public RenderTarget2D LoadRenderTarget2D(RenderTarget2D renderTarget, RenderTargetUsage renderUsage)
        {
            //Get the current presentation parameters
            PresentationParameters pp = display.Device.PresentationParameters;
            //Create our new render target
            renderTarget = new RenderTarget2D(display.Device,
                     pp.BackBufferWidth, //Same width as backbuffer
                     pp.BackBufferHeight, //Same height
                     false, //No mip-mapping
                     pp.BackBufferFormat, //Same colour format
                     pp.DepthStencilFormat, //Same depth stencil
                     pp.MultiSampleCount,
                     renderUsage);
            return renderTarget;
        }


        public Texture2D Corner { get { return corner; } set { corner = value; } }
        public Texture2D Textbox { get { return textbox; } set { textbox = value; } }
        public Texture2D DialogFader { get { return dialogFader; } set { dialogFader = value; } }
        public Texture2D DialogBorder { get { return dialogBorder; } set { dialogBorder = value; } }
        public LinkedList<BackGroundImage> BackgroundImages { get { return backgroundImages; } set { backgroundImages = value; } }
    }
}


//public void SetFrameRate(
//   GraphicsDeviceManager manager, int frames)
//{
//   double dt = (double)1000 / (double)frames;
//   manager.SynchronizeWithVerticalRetrace = false;
//   game.TargetElapsedTime = TimeSpan.FromMilliseconds(dt);
//   manager.ApplyChanges();
//}

//graphicsDeviceManager.SynchronizeWithVerticalRetrace = false;
//game.IsFixedTimeStep = false;
//graphicsDeviceManager.ApplyChanges();