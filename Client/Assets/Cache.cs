﻿using System;
using System.Collections.Generic;

using Client.Assets.State;
using Helbreath.Common;
using Helbreath.Common.Assets;
using Helbreath.Common.Assets.Objects;
using Client.Assets.UI;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Graphics;
using Client.Assets.Game;

namespace Client.Assets
{
    public static class Cache
    {
        // GAME
        public static MapView MapView;
        public static IDefaultState DefaultState;
        public static GameSettings GameSettings;

        // GRAPHICS
        public static Texture2D BackgroundTexture;
        public static TransparencyFaders TransparencyFaders;

        //PIXEL SHADERS
        public static Dictionary<int, Microsoft.Xna.Framework.Graphics.Effect> PixelShaders;
        public static Dictionary<Microsoft.Xna.Framework.Graphics.Effect, EffectParameter> ShaderParameters; //Not used yet

        // SPRITE QUEUE
        public static Queue<SpriteLoad> SpriteQueue;

        // HUMAN
        public static Dictionary<int, Animation> HumanAnimations;

        // MONSTER
        public static Dictionary<int, Animation> MonsterAnimations;

        // EQUIPMENT
        public static Dictionary<int, Animation> Equipment;

        // EFFECTS
        public static Dictionary<int, Animation> Effects;

        // INTERFACE
        public static Dictionary<int, Animation> Interface;

        // MAP TILES
        public static Dictionary<int, Tile> Tiles;

        // MINI MAPS
        public static Dictionary<string, Texture2D> MiniMaps;
        public static Dictionary<string, Texture2D> ZoomMaps;

        // OWNERS
        public static Dictionary<UInt16, IOwner> OwnerCache;

        // DYNAMIC OBJECTS
        public static Dictionary<int, DynamicObject> DynamicObjects;

        // GUILDS
        public static Dictionary<int, Guild> GuildCache;

        // MAGIC
        public static Dictionary<int, Magic> MagicConfiguration;

        // ITEMS
        public static Dictionary<int, Item> ItemConfiguration;

        // NPCS
        public static Dictionary<int, Npc> NpcConfiguration;

        // SPRITES
        public static Dictionary<string, SpriteConfig> SpriteConfiguration;
        public static Texture2D LightningSegment;
        public static Texture2D LightningEnd;
        //public static Texture2D Pixel;

        // ANIMATIONS
        public static int[,] AnimationSpeeds;
        public static string[,] AnimationSounds;

        //HOTKEYS
        public static Dictionary<int, HotKey> HotKeyConfiguration;
        public static Dictionary<int, HotBarIcon> HotBarConfiguration;

        //FONTS
        public static Dictionary<FontType, SpriteFont> Fonts;

        //COLOURS
        //public static Dictionary<OwnerSide, Color> TownColours;
        //public static Dictionary<ChatType, Color> ChatColours;
        public static Dictionary<GameColor, Color> Colors;

        // SOUNDS
        public static SoundEffectInstance BackgroundMusic;
        public static Dictionary<string, SoundEffect> Music;
        public static Dictionary<string, SoundEffect> SoundEffects;

        //DIALOG BOXES //TODO split saving of dialogs into per character .xml to allow for multiclient access to files
        public static Dictionary<GameDialogBoxType, GameDialogBoxConfiguration>[] DialogBoxStates;
        public static Dictionary<string, Dictionary<GameDialogBoxType, GameDialogBoxConfiguration>[]> LoadedDialogBoxStatesArray; 
    }
}
