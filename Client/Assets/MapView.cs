﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Helbreath.Common;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Client.Assets
{
    public class MapView
    {
        private Map currentMap;
        private int pivotX; // top left real X
        private int pivotY; // top left real Y
        private int width;
        private int height;

        private int playerId;
        private Dictionary<int, IOwner> owners;
        private bool isReady;

        public MapView()
        {
            owners = new Dictionary<int, IOwner>();
        }

        public void Init(Map map, Player player)
        {
            this.currentMap = map;
            this.playerId = player.ObjectId;
            player.Init();
            owners.Clear();
            owners.Add(player.ObjectId, player);

            isReady = true;
        }

        public void Update()
        {
            if (!isReady) return;
        }

        public void Draw(SpriteBatch spriteBatch)
        {
            if (!isReady) return;
        }

        public void SetPivot(int playerX, int playerY)
        {
            pivotX = playerX - Globals.Resolutions[(int)Cache.DefaultState.Display.Resolution, (int)ResolutionSetting.CenterX] - Globals.OffScreenCells;
            pivotY = playerY - Globals.Resolutions[(int)Cache.DefaultState.Display.Resolution, (int)ResolutionSetting.CenterY] - Globals.OffScreenCells;
        }

        public void Pan(MotionDirection direction)
        {
            switch (direction)
            {
                case MotionDirection.North: pivotY--; break;
                case MotionDirection.NorthEast: pivotY--; pivotX++; break;
                case MotionDirection.East: pivotX++; break;
                case MotionDirection.SouthEast: pivotX++; pivotY++;break;
                case MotionDirection.South: pivotY++; break;
                case MotionDirection.SouthWest: pivotY++; pivotX--; break;
                case MotionDirection.West: pivotX--; break;
                case MotionDirection.NorthWest: pivotX--; pivotY--; break;
                default: return;
            }
        }

        public bool IsWithinView(Resolution resolution, IOwner other)
        {
            int rangeX = (Globals.Resolutions[(int)resolution, (int)ResolutionSetting.CellsWide] / 2) + Globals.OffScreenCells;
            int rangeY = (Globals.Resolutions[(int)resolution, (int)ResolutionSetting.CellsHeigh] / 2) + Globals.OffScreenCells;

            if ((currentMap == other.Map) &&
                (owners[playerId].X >= other.X - rangeX) &&
                (owners[playerId].X <= other.X + rangeX) &&
                (owners[playerId].Y >= other.Y - rangeY) &&
                (owners[playerId].Y <= other.Y + rangeY))
                return true;
            else return false;
        }

        public bool IsWithinBounds(int x, int y)
        {
            if (y < 0 || y > Map.Rows.Count-1 || x < 0 || x > Map.Rows[y].Tiles.Count-1)
                 return false;
            else return true;
        }

        public Map Map { get { return currentMap; } set { currentMap = value; } }
        public int PivotX { get { return pivotX; } }
        public int PivotY { get { return pivotY; } }
        public int Width { get { return width; } set { width = value; } }
        public int Height { get { return height; } set { height = value; } }
    }
}
