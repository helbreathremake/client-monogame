﻿using System;
using System.Collections.Generic;
using Helbreath.Common;
using Helbreath.Common.Assets;
using Microsoft.Xna.Framework.Graphics;


namespace Client.Assets
{
    public interface IOwner
    {
        UInt16 ObjectId { get; set; }
        int Id { get; set; }
        void Init(int x, int y);
        void Remove();
        void Parse(byte[] data, int countIn, out int count);
        void ParseMotion(CommandMessageType messageType, byte[] data);
        void SetLocation(Map map, int x, int y);
        bool IsMoving { get; set; }
        int X { get; }
        int Y { get; }
        int OffsetX { get; }
        int OffsetY { get; }
        int Type { get; set; }
        MotionDirection Direction { get; set; }
        MotionType Motion { get; set; }
        DateTime AnimationTime { get; set; }
        OwnerSide Side { get; set; }
        OwnerSideStatus SideStatus { get; set; }
        OwnerType OwnerType { get; }
        int AnimationFrame { get; set; }
        Map Map { get; set; }
        MapTile Location { get; }
        string Name { get; set; }
        List<NpcPerk> Perks { get; }
        int Appearance1 { get; set; }
        int Appearance2 { get; set; }
        int Appearance3 { get; set; }
        int Appearance4 { get; set; }
        int AppearanceColour { get; set; }
        Item Helmet { get; }
        Item BodyArmour { get; }
        Item Hauberk { get; }
        Item Leggings { get; }
        Item Boots { get; }
        Item Cape { get; }
        Item Weapon { get; }
        Item Shield { get; }
        int Status { get; set; }
        string GuildName { get; set; }
        int GuildRank { get; set; }
        int MerchantId { get; }
        MerchantType MerchantType { get; }
        bool IsMerchant { get; }
        bool IsCombatMode { get; }
        bool IsDead { get; }
        bool IsCorpseExploited { get; set; }
        bool IsBerserked { get; }
        bool IsHaste { get; }
        bool IsInvisible { get; }
        bool IsFrozen { get; }
        ChatMessage DamageHistory { get; set; }
        List<ChatMessage> DamageHistoryStageTwo { get; set; }
        ChatMessage SpellHistory { get; set; }
        ChatMessage ChatHistory { get; set; }

        void Idle(MotionDirection direction);
        void Bow(MotionDirection direction);
        void PickUp();
        void Update();
        void Attack(int destinationX, int destinationY, AttackType type);
        void BowAttack();
        void Dying(int damage, DamageType damageType, int hitCount);
        void Cast(int magicId);
        void Run(MotionDirection direction);
        void Walk(MotionDirection direction);
        void Dash(MotionDirection direction, int destinationX, int destinationY, int weaponId);
        void TakeDamage(int damage, DamageType damageType, int hitCount, bool stun = true);
        void VitalChanged(int amount, VitalType vitalType);
        void Fly(int damage, int hitCount, MotionDirection direction, DamageType damageType);
        bool Draw(SpriteBatch spriteBatch, GameDisplay display, int x, int y, int panX, int panY);
        bool Draw(SpriteBatch spriteBatch, GameDisplay display);
        OwnerRelationShip GetRelationship(OwnerSide side);
        void FadeOut();
    }
}
