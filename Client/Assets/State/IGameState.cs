﻿using Client.Assets.UI;
using Helbreath.Common;
using System.Collections.Generic;


namespace Client.Assets.State
{
    public interface IGameState
    {
        Dictionary<GameDialogBoxType, IGameDialogBox> DialogBoxes { get; }
        LinkedList<GameDialogBoxType> DialogBoxDrawOrder { get; set; }
        void BringToFront(GameDialogBoxType type);
        void HideMessageBox(GameDialogBoxType type);
        GameDialogBoxType ClickedDialogBox { get; set; }
        void ShowMessageBox(GameMessageBox box);
    }
}
