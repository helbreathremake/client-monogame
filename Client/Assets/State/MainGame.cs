﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Net.Sockets;
using System.Net.NetworkInformation;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Client.Assets.Effects;
using Client.Assets.UI;
using Helbreath.Common;
using Helbreath.Common.Events;
using Helbreath.Common.Assets;
using Client.Assets.Game;
using System.Threading.Tasks;

namespace Client.Assets.State
{
    public class MainGame : IDefaultState, IGameState
    {
        // events
        public event EventHandler Disconnect;

        private bool isComplete;
        private bool back;
        private string gameServerName = "";
        private ClientInfo gameServer;
        private string ipAddress;
        private int port;
        private bool resolutionChange = false;

        private Queue<Command> processQueue;
        public readonly object processQueueLock = new object();

        private GameDisplay display;
        private Player player;
        private Weather weather;
        private TimeOfDay timeOfDay;
        private List<ChatMessage> chatMessages;
        private List<ChatMessage> chatHistory;
        private List<ChatMessage> levelHistory;
        private List<EventMessage> eventHistory;
        private List<IGameEffect> effects;
        private Dictionary<Keys[], HotKeyActions> Hotkeys = new Dictionary<Keys[],HotKeyActions>();

        private Texture2D cellHighlightTexture;
        private bool overlayMode;
        private bool overlayMagicMode;
        private bool overlayWeaponMode;
        private bool overlaymagicrangeMode; //Future additon, disable spells from being shot X amount of cells away, show range in overlay mode some how
        private bool overlayMovePath;
        private bool overlayColorMode;
        private bool chatMode;
        private bool fullScreenMode;
        private bool tileoverlaymode;
        private int testIndex = 0;

        private float regenRotation = 0.0f;
        private bool isRotationAdditive = true;
        //public List<float[]> bloodSplaters;

        // chat
        private TextBox chatBox;
        private Rectangle chatBoxBackground;
        private List<string> chatBoxHistory;
        private int chatBoxHistoryIndex;

        // dialog
        private Dictionary<GameDialogBoxType, IGameDialogBox> dialogBoxes;
        private LinkedList<GameDialogBoxType> dialogBoxDrawOrder;
        private GameDialogBoxType clickedDialogBox;
        private GameDialogBoxType highlightedDialogBox;
        private GameDialogBoxType topdialogBox;
        private int selectedObjectId;
        private ObjectType selectedObjectType;
        private int selectedObjectX;
        private int selectedObjectY;
        private SelectionMode selectionMode;
        private int selectionModeId;
        private LinkedList<BackGroundImage> backgroundImages;

        //popup
        GameDialogBoxType popupType;
        Item popupItem;
        int popupMagicIndex;
        int popupX;
        int popupY;
        Vector2 popupLocation;

        //draggedItem
        DraggedType draggedType;
        int draggedIndex;
        Item draggedItem;

        //draw
        private RenderTarget2D mainRenderTarget;
        private RenderTarget2D renderTargetObjects;
        private RenderTarget2D gameBoardRenderTarget; //Used to shift player into center of the viewport
        private int offScreenCells = 9;

        // frame rate
        private int frameRate = 0;
        private int frameCounter = 0;
        private TimeSpan frameElapsedTime = TimeSpan.Zero;
        private int dataInCounter = 0;
        //private int dataOutCounter = 0;
        public double dataInRate = 0;
        public double dataOutRate = 0;

        // global events
        private string globalMessage;
        private DateTime globalMessageTime;

        // ping
        private int ping = 0;
        private Timer pingCallback;

        // sprites
        private Rectangle levelUpRectangle;
        private Timer spriteQueueCallback;

        // public event
        private PublicEventState publicEventState;
        private bool publicEventParticipation;
        private PublicEventType publicEventType;
        private int publicEventX;
        private int publicEventY;
        private PublicEventDifficulty publicEventDifficulty;
        private DateTime publicEventStartTime;
        private DateTime publicEventEndTime;
        
        public MainGame(string ipAddress, int port)
        {
            processQueue = new Queue<Command>();
            chatMessages = new List<ChatMessage>();
            chatHistory = new List<ChatMessage>();
            levelHistory = new List<ChatMessage>();
            eventHistory = new List<EventMessage>();
            chatBoxHistory = new List<string>();
            effects = new List<IGameEffect>();

            Cache.OwnerCache = new Dictionary<ushort, IOwner>();
            Cache.DynamicObjects = new Dictionary<int, DynamicObject>();
            Cache.GuildCache = new Dictionary<int, Guild>();

            int maxResolutions = Enum.GetValues(typeof(Resolution)).Length;
            Cache.DialogBoxStates = new Dictionary<GameDialogBoxType, GameDialogBoxConfiguration>[maxResolutions];
            for (int i = 0; i < maxResolutions; i++)
            {
                Cache.DialogBoxStates[i] = new Dictionary<GameDialogBoxType, GameDialogBoxConfiguration>();
            }

            Cache.LoadedDialogBoxStatesArray = new Dictionary<string, Dictionary<GameDialogBoxType, GameDialogBoxConfiguration>[]>();

            this.ipAddress = ipAddress;
            this.port = port;

            Cache.DefaultState = this;
            selectionMode = SelectionMode.None;
            selectionModeId = -1;

            //bloodSplaters = new List<float[]>();

            publicEventType = PublicEventType.None;
        }

        public void Init(GameWindow window)
        {    
            Display.Mouse.State = GameMouseState.Normal;

            Cache.MapView = new MapView();
            Cache.MapView.Width = display.ResolutionWidth / 32;
            Cache.MapView.Height = display.ResolutionHeight / 32;

            /* INITIALIZE GAME SERVER TCP CONNECTION */
                Socket socket = Sockets.CreateTCPSocket(ipAddress, port);
                gameServer = new ClientInfo(socket, true);
                gameServer.EncryptionType = EncryptionType.None;
                gameServer.OnReadBytes += new ConnectionReadBytes(ReadMessageFromGameServer);
                gameServer.OnClose += ForceClose;
         

            /* INITIALIZE CHAT BOX */
            chatBox = new TextBox(Cache.BackgroundTexture, Cache.Fonts[FontType.GeneralSize10]);
            chatBox.X = 10;
            chatBox.Width = Display.ResolutionWidth;
            chatBox.MaxCharacters = 120;

            switch (display.Resolution)
            {
                case Resolution.Classic:
                    chatBox.Y = 400;
                    chatBoxBackground = new Rectangle(0, 400, Display.ResolutionWidth, 20);
                    break;
                case Resolution.Standard:
                    chatBox.Y = 520;
                    chatBoxBackground = new Rectangle(0, 520, Display.ResolutionWidth, 20);
                    break;
                case Resolution.Large:
                    chatBox.Y = 668;
                    chatBoxBackground = new Rectangle(0, 668, Display.ResolutionWidth, 20);
                    break;
            }

            //SET UP GAME BOARD RENDER TARGET
            gameBoardRenderTarget = GameBoardRenderTargetSetUp(gameBoardRenderTarget);
            
            /* INITIALIZE GRAPHICS */
            if (Display.FullScreen) fullScreenMode = true;

            cellHighlightTexture = new Texture2D(Display.Device, 32, 32);
            Color[] data = new Color[32 * 32];
            for (int i = 0; i < data.Length; ++i) data[i] = Color.White;
            cellHighlightTexture.SetData(data);

            levelUpRectangle = new Rectangle(display.ResolutionWidth - 100, display.ResolutionHeight - 85,
                                            (int)Cache.Fonts[FontType.DisplayNameSize13Spacing1].MeasureString("Level Up!").X,
                                            (int)Cache.Fonts[FontType.DisplayNameSize13Spacing1].MeasureString("Level Up!").Y);

            LoadHotKeyConfiguration();


            /* INITIALIZE MISC */
            LoadDefaultDialogBoxConfigurations(); //Load Default First, assign to cache
            LoadSavedDialogBoxLocations(); //Attempt to overwrite Cache with loaded saved locations from xml
            InitializeDialogBoxes(); //Create Dialogsboxes from dialogconfig array add to DialogBoxes and DialogBoxDrawOrder

            ChangeDetailMode(Cache.GameSettings.DetailMode, false);

            /* INITIALIZE HOTKEYS */
            foreach (KeyValuePair<int, HotKey> value in Cache.HotKeyConfiguration)
            {
                HotKey hotkey = value.Value;
                Keys press = hotkey.Press;
                Keys hold = hotkey.Hold;
                HotKeyActions action = hotkey.Action;
                Keys[] keys = { press, hold };
                Hotkeys.Add(keys, action);
            }

            /* INITIALIZE GLOBALS */
            Globals.ExperienceTable = new long[Globals.MaximumLevel + 21];
            for (int level = 1; level < Globals.ExperienceTable.Length; level++)
                Globals.ExperienceTable[level] = Globals.ExperienceTable[level - 1] + level * (50 + (level * (level / 17) * (level / 17)));

            Globals.SkillExperienceTable = new int[102];
            for (int level = 0; level < Globals.SkillExperienceTable.Length; level++)
            {
                Globals.SkillExperienceTable[level] = level + 1;
            }


            //Globals.SkillExperienceTable = new int[102];
            //for (int level = 0; level < Globals.SkillExperienceTable.Length; level++)
            //    if (level < 1) Globals.SkillExperienceTable[level] = 1;
            //    else if (level <= 50)
            //        Globals.SkillExperienceTable[level] = Globals.SkillExperienceTable[level - 1] + level;
            //    else Globals.SkillExperienceTable[level] = Globals.SkillExperienceTable[level - 1] + ((level * level) / 10);

            pingCallback = new Timer(new TimerCallback(Pinger), null, 0, 3000);

            // Creates threads to process sprite loading
            spriteQueueCallback = new Timer(new TimerCallback(SpriteHelper.HandleSpriteQueue), null, 0, 250);

            InitPlayer();
        }

        private void Pinger(object o)
        {
            if (string.IsNullOrEmpty(ipAddress)) return;

            using (Ping ping = new Ping())
            {
                ping.PingCompleted += new PingCompletedEventHandler(PingCompleted);
                ping.SendAsync(ipAddress, 1000);
            }
        }

        public void PingCompleted(object sender, PingCompletedEventArgs e)
        {
            if (e != null && e.Reply != null)
                switch (e.Reply.Status)
                {
                    case IPStatus.Success: this.ping = (int)e.Reply.RoundtripTime; break;
                    default: this.ping = -1; break;
                }
        }

        //Write over message box type to show
        public void ShowMessageBox(GameMessageBox box)
        {
            if (dialogBoxes.ContainsKey(box.Type))
            {
                dialogBoxDrawOrder.Remove(box.Type);
                dialogBoxes.Remove(box.Type);
            }
            dialogBoxes.Add(box.Type, box);
            dialogBoxDrawOrder.AddFirst(box.Type);
            box.Show();
        }

        //Remove from list to Hide message box
        public void HideMessageBox(GameDialogBoxType type)
        {
            if (clickedDialogBox == type) clickedDialogBox = GameDialogBoxType.None;
            if (highlightedDialogBox == type) highlightedDialogBox = GameDialogBoxType.None;
            dialogBoxDrawOrder.Remove(type);
            dialogBoxes.Remove(type);
        }

        public void InitPlayer()
        {
            byte[] command = new byte[45];
            Buffer.BlockCopy(Utility.GetCommandTypeBytes(CommandType.RequestInitPlayer), 0, command, 0, 4);
            Buffer.BlockCopy(Utility.GetCommandTypeBytes(CommandMessageType.Confirm), 0, command, 4, 2);
            Buffer.BlockCopy(player.Name.GetBytes(10), 0, command, 6, 10);
            Buffer.BlockCopy(player.Username.GetBytes(10), 0, command, 16, 10);
            Buffer.BlockCopy(player.Password.GetBytes(10), 0, command, 26, 10);
            command[27] = (byte)0; // observer
            Buffer.BlockCopy(gameServerName.GetBytes(10), 0, command, 28, 10);
            command[38] = (byte)((int)display.Resolution);

            byte[] data = Utility.Encrypt(command, false);
            if (gameServer != null) gameServer.Send(data);
        }

        private void InitData()
        {
            byte[] command = new byte[45];
            Buffer.BlockCopy(Utility.GetCommandTypeBytes(CommandType.RequestInitData), 0, command, 0, 4);
            Buffer.BlockCopy(Utility.GetCommandTypeBytes(CommandMessageType.Confirm), 0, command, 4, 2);
            Buffer.BlockCopy(player.Name.GetBytes(10), 0, command, 6, 10);
            Buffer.BlockCopy(player.Username.GetBytes(10), 0, command, 16, 10);
            Buffer.BlockCopy(player.Password.GetBytes(10), 0, command, 26, 10);
            command[27] = (byte)0; // observer
            Buffer.BlockCopy(gameServerName.GetBytes(10), 0, command, 28, 10);

            byte[] data = Utility.Encrypt(command, false);
            if (gameServer != null) gameServer.Send(data);
        }

        public void UpdateResolution()
        {
            byte[] command = new byte[10];
            Buffer.BlockCopy(Utility.GetCommandTypeBytes(CommandType.RequestResolutionChange), 0, command, 0, 4);
            Buffer.BlockCopy(Utility.GetCommandTypeBytes(CommandMessageType.Confirm), 0, command, 4, 2);
            command[6] = (byte)((int)display.Resolution);

            byte[] data = Utility.Encrypt(command, false);
            if (gameServer != null) gameServer.Send(data);

            GameMessageBox box = new GameMessageBox(GameDialogBoxType.HUDResetConfirmation, "Reset HUD to resolution defaults?", "Yes", "No");
            box.ResponseSelected += HUDResetConfirmation;
            ShowMessageBox(box);

            levelUpRectangle = new Rectangle(display.ResolutionWidth - 100, display.ResolutionHeight - 85,
                                (int)Cache.Fonts[FontType.DisplayNameSize13Spacing1].MeasureString("Level Up!").X,
                                (int)Cache.Fonts[FontType.DisplayNameSize13Spacing1].MeasureString("Level Up!").Y);

            UpdateDialogBoxes();

            // Classic resolution cannot use v2 inventory or magic
            if (display.Resolution == Resolution.Classic)
            {
                Cache.GameSettings.InventoryV2 = false;
                //Cache.GameSettings.SpellBookV2 = false;

                dialogBoxes[GameDialogBoxType.CharacterV2].Hide();
                //dialogBoxes[GameDialogBoxType.SpellBookV2].Hide();
            }

            display.Zoom = new Rectangle(display.GameBoardShiftX, display.GameBoardShiftY, display.ResolutionWidth, display.ResolutionHeight); 
            gameBoardRenderTarget = GameBoardRenderTargetSetUp(gameBoardRenderTarget);
            SaveGameSettings();
        }

        private void Restart()
        {
            byte[] command = new byte[10];
            Buffer.BlockCopy(Utility.GetCommandTypeBytes(CommandType.RequestRestart), 0, command, 0, 4);
            Buffer.BlockCopy(Utility.GetCommandTypeBytes(CommandMessageType.Confirm), 0, command, 4, 2);

            byte[] data = Utility.Encrypt(command, false);
            if (gameServer != null) gameServer.Send(data);
        }

        private void ForceClose(ClientInfo ci)
        {
            Logout(true);
        }

        public void Chat(string message)
        {
            byte[] command = new byte[30 + message.Length];
            Buffer.BlockCopy(Utility.GetCommandTypeBytes(CommandType.Chat), 0, command, 0, 4);
            Buffer.BlockCopy(player.X.GetBytes(), 0, command, 6, 2);
            Buffer.BlockCopy(player.Y.GetBytes(), 0, command, 8, 2);
            Buffer.BlockCopy(player.Name.GetBytes(10), 0, command, 10, 10);
            command[21] = (byte)0;
            Buffer.BlockCopy(message.GetBytes(message.Length), 0, command, 22, message.Length);

            byte[] data = Utility.Encrypt(command, false);
            if (gameServer != null) gameServer.Send(data);
        }

        public void Idle(MotionDirection direction)
        {
            byte[] command = new byte[45];
            Buffer.BlockCopy(Utility.GetCommandTypeBytes(CommandType.RequestMotion), 0, command, 0, 4);
            Buffer.BlockCopy(((int)MotionType.Idle).GetBytes(), 0, command, 4, 2);
            Buffer.BlockCopy(player.X.GetBytes(), 0, command, 6, 2);
            Buffer.BlockCopy(player.Y.GetBytes(), 0, command, 8, 2);
            command[10] = (byte)(int)direction;

            byte[] data = Utility.Encrypt(command, false);
            if (gameServer != null) gameServer.Send(data);
        }

        public void Move(MotionDirection direction, bool running)
        {
            byte[] command = new byte[45];
            Buffer.BlockCopy(Utility.GetCommandTypeBytes(CommandType.RequestMotion), 0, command, 0, 4);
            Buffer.BlockCopy(((player.IsRunning) ? ((int)MotionType.Run) : ((int)MotionType.Move)).GetBytes(), 0, command, 4, 2);
            Buffer.BlockCopy(player.X.GetBytes(), 0, command, 6, 2);
            Buffer.BlockCopy(player.Y.GetBytes(), 0, command, 8, 2);
            command[10] = (byte)(int)direction;

            byte[] data = Utility.Encrypt(command, false);
            if (gameServer != null) gameServer.Send(data);
        }

        public void Teleport()
        {
            byte[] command = new byte[10];

            Buffer.BlockCopy(Utility.GetCommandTypeBytes(CommandType.RequestTeleport), 0, command, 0, 4);
            Buffer.BlockCopy(Utility.GetCommandTypeBytes(CommandMessageType.Confirm), 0, command, 4, 2);

            byte[] data = Utility.Encrypt(command, false);
            if (gameServer != null) gameServer.Send(data);
        }

        public void Cast(int spellId)
        {
            byte[] command = new byte[45];
            Buffer.BlockCopy(Utility.GetCommandTypeBytes(CommandType.RequestMotion), 0, command, 0, 4);
            Buffer.BlockCopy(((int)MotionType.Magic).GetBytes(), 0, command, 4, 2);
            Buffer.BlockCopy(player.X.GetBytes(), 0, command, 6, 2);
            Buffer.BlockCopy(player.Y.GetBytes(), 0, command, 8, 2);
            command[10] = (byte)(int)player.Direction;
            Buffer.BlockCopy(spellId.GetBytes(), 0, command, 11, 2);

            byte[] data = Utility.Encrypt(command, false);
            if (gameServer != null) gameServer.Send(data);
        }

        public void Magic(int destinationX, int destinationY)
        {
            byte[] command = new byte[27];
            Buffer.BlockCopy(Utility.GetCommandTypeBytes(CommandType.Common), 0, command, 0, 4);
            Buffer.BlockCopy(((int)CommandMessageType.CastMagic).GetBytes(), 0, command, 4, 2);
            Buffer.BlockCopy(player.X.GetBytes(), 0, command, 6, 2);
            Buffer.BlockCopy(player.Y.GetBytes(), 0, command, 8, 2);
            command[10] = (byte)(int)player.Direction;
            Buffer.BlockCopy(destinationX.GetBytes(), 0, command, 11, 4);
            Buffer.BlockCopy(destinationY.GetBytes(), 0, command, 15, 4);
            Buffer.BlockCopy((player.SpellCharged+100).GetBytes(), 0, command, 19, 4);

            byte[] data = Utility.Encrypt(command, false);
            if (gameServer != null) gameServer.Send(data);
        }


        public void BuySpell(int spellIndex)
        {
            byte[] command = new byte[14];
            Buffer.BlockCopy(Utility.GetCommandTypeBytes(CommandType.Common), 0, command, 0, 4);
            Buffer.BlockCopy(Utility.GetCommandTypeBytes(CommandMessageType.BuySpell), 0, command, 4, 2);
            Buffer.BlockCopy(spellIndex.GetBytes(), 0, command, 6, 4);

            byte[] data = Utility.Encrypt(command, false);
            if (gameServer != null) gameServer.Send(data);
        }

        public void UnLearnSpell(int spellIndex)
        {
            byte[] command = new byte[14];
            Buffer.BlockCopy(Utility.GetCommandTypeBytes(CommandType.Common), 0, command, 0, 4);
            Buffer.BlockCopy(Utility.GetCommandTypeBytes(CommandMessageType.UnLearnSpell), 0, command, 4, 2);
            Buffer.BlockCopy(spellIndex.GetBytes(), 0, command, 6, 4);

            byte[] data = Utility.Encrypt(command, false);
            if (gameServer != null) gameServer.Send(data);
        }

        public void ItemToWarehouse(int itemIndex, int destinationIndex)
        {
            byte[] command = new byte[20];
            Buffer.BlockCopy(Utility.GetCommandTypeBytes(CommandType.Common), 0, command, 0, 4);
            Buffer.BlockCopy(Utility.GetCommandTypeBytes(CommandMessageType.ItemToWarehouse), 0, command, 4, 2);
            Buffer.BlockCopy(itemIndex.GetBytes(), 0, command, 6, 4);
            Buffer.BlockCopy(destinationIndex.GetBytes(), 0, command, 10, 4);

            byte[] data = Utility.Encrypt(command, false);
            if (gameServer != null) gameServer.Send(data);
        }

        public void ItemFromWarehouse(int itemIndex, int destinationIndex)
        {
            byte[] command = new byte[20];
            Buffer.BlockCopy(Utility.GetCommandTypeBytes(CommandType.Common), 0, command, 0, 4);
            Buffer.BlockCopy(Utility.GetCommandTypeBytes(CommandMessageType.ItemFromWarehouse), 0, command, 4, 2);
            Buffer.BlockCopy(itemIndex.GetBytes(), 0, command, 6, 4);
            Buffer.BlockCopy(destinationIndex.GetBytes(), 0, command, 10, 4);

            byte[] data = Utility.Encrypt(command, false);
            if (gameServer != null) gameServer.Send(data);
        }

        public void AddSellItem(int itemIndex)
        {
            if (player.Inventory[itemIndex] != null)
            {
                Item item = player.Inventory[itemIndex];

                if (item.Set != ItemSet.None)
                    AddEvent("This item cannot be sold.");
                else ((MerchantDialogBox)dialogBoxes[GameDialogBoxType.Merchant]).AddSellItem(itemIndex);
            }
        }

        public void AddRepairItem(int itemIndex)
        {
            if (player.Inventory[itemIndex] != null && !player.Inventory[itemIndex].IsStackable)
                ((MerchantDialogBox)dialogBoxes[GameDialogBoxType.Merchant]).AddRepairItem(itemIndex);
        }

        public void RequestTrade(int playerId, int itemIndex = -1)
        {
            // reset ID
            player.TradePartnerID = playerId;

            if (itemIndex != -1) AddTradeItem(itemIndex, true);

            byte[] command = new byte[20];
            Buffer.BlockCopy(Utility.GetCommandTypeBytes(CommandType.Common), 0, command, 0, 4);
            Buffer.BlockCopy(Utility.GetCommandTypeBytes(CommandMessageType.TradeRequest), 0, command, 4, 2);
            Buffer.BlockCopy(playerId.GetBytes(), 0, command, 6, 4);
            Buffer.BlockCopy(itemIndex.GetBytes(), 0, command, 10, 4);

            byte[] data = Utility.Encrypt(command, false);
            if (gameServer != null) gameServer.Send(data);
        }

        public void AcceptTrade(bool state)
        {
            byte[] command = new byte[20];
            Buffer.BlockCopy(Utility.GetCommandTypeBytes(CommandType.Common), 0, command, 0, 4);
            Buffer.BlockCopy(Utility.GetCommandTypeBytes(CommandMessageType.TradeAccept), 0, command, 4, 2);
            command[6] = (byte)(state ? 1 : 0);

            byte[] data = Utility.Encrypt(command, false);
            if (gameServer != null) gameServer.Send(data);
        }

        public void AddTradeItem(int itemIndex, bool firstItem = false)
        {
            if (player.Inventory[itemIndex] != null &&
                ((TradeV2DialogBox)dialogBoxes[GameDialogBoxType.TradeV2]).AddTradeItem(itemIndex))
            {

                if (!firstItem)
                {
                    byte[] command = new byte[15];
                    Buffer.BlockCopy(Utility.GetCommandTypeBytes(CommandType.Common), 0, command, 0, 4);
                    Buffer.BlockCopy(Utility.GetCommandTypeBytes(CommandMessageType.TradeAddItem), 0, command, 4, 2);
                    Buffer.BlockCopy(itemIndex.GetBytes(), 0, command, 6, 4);

                    byte[] data = Utility.Encrypt(command, false);
                    if (gameServer != null) gameServer.Send(data);
                }
            }
        }

        public void RemoveTradeItem(int itemIndex)
        {
            if (player.TradePartnerID == -1) return;

            if (player.Inventory[itemIndex] != null)
            {
                ((TradeV2DialogBox)dialogBoxes[GameDialogBoxType.TradeV2]).RemoveTradeItem(itemIndex);

                byte[] command = new byte[15];
                Buffer.BlockCopy(Utility.GetCommandTypeBytes(CommandType.Common), 0, command, 0, 4);
                Buffer.BlockCopy(Utility.GetCommandTypeBytes(CommandMessageType.TradeRemoveItem), 0, command, 4, 2);
                Buffer.BlockCopy(itemIndex.GetBytes(), 0, command, 6, 4);

                byte[] data = Utility.Encrypt(command, false);
                if (gameServer != null) gameServer.Send(data);
            }
        }

        public void AddTradePartnerItem(Item item)
        {
            if (player.TradePartnerID == -1) return;
            ((TradeV2DialogBox)dialogBoxes[GameDialogBoxType.TradeV2]).AddTradePartnerItem(item);
        }

        public void RemoveTradePartnerItem(int itemIndex)
        {
            if (player.TradePartnerID == -1) return;
            ((TradeV2DialogBox)dialogBoxes[GameDialogBoxType.TradeV2]).RemoveTradePartnerItem(itemIndex);
        }

        public void EndTrade(TradeState state)
        {
            player.TradePartnerID = -1;

            switch (state)
            {
                case TradeState.Rejected: AddEvent("Trade rejected."); break;
                case TradeState.Cancelled: 
                    AddEvent("Trade cancelled.");
                    
                    byte[] command = new byte[15];
                    Buffer.BlockCopy(Utility.GetCommandTypeBytes(CommandType.Common), 0, command, 0, 4);
                    Buffer.BlockCopy(Utility.GetCommandTypeBytes(CommandMessageType.TradeCancel), 0, command, 4, 2);

                    byte[] data = Utility.Encrypt(command, false);
                    if (gameServer != null) gameServer.Send(data);
                    break;
                case TradeState.Completed: AddEvent("Trade completed."); break;
            }

            dialogBoxes[GameDialogBoxType.TradeV2].Hide(); 
        }

        public void UseItem(int itemIndex)
        {
            if (player.IsDead) return;

            Item item = player.Inventory[itemIndex];

            // clear from merchant selections
            if (((MerchantDialogBox)dialogBoxes[GameDialogBoxType.Merchant]).SellList.Contains(itemIndex))
                ((MerchantDialogBox)dialogBoxes[GameDialogBoxType.Merchant]).SellList.Remove(itemIndex);
            if (((MerchantDialogBox)dialogBoxes[GameDialogBoxType.Merchant]).RepairList.Contains(itemIndex))
                ((MerchantDialogBox)dialogBoxes[GameDialogBoxType.Merchant]).RepairList.Remove(itemIndex);
            // clear from trade selections
            if (((TradeV2DialogBox)dialogBoxes[GameDialogBoxType.TradeV2]).TradeList.Contains(itemIndex)) RemoveTradeItem(itemIndex);

            if (item != null)
                switch (item.Type)
                {
                    case ItemType.Eat:
                    case ItemType.Consume:
                    case ItemType.Deplete:
                        byte[] command = new byte[27];
                        Buffer.BlockCopy(Utility.GetCommandTypeBytes(CommandType.Common), 0, command, 0, 4);
                        Buffer.BlockCopy(Utility.GetCommandTypeBytes(CommandMessageType.UseItem), 0, command, 4, 2);
                        Buffer.BlockCopy((0).GetBytes(), 0, command, 6, 2);
                        Buffer.BlockCopy((0).GetBytes(), 0, command, 8, 2);
                        command[10] = (int)0;
                        Buffer.BlockCopy((itemIndex).GetBytes(), 0, command, 11, 4);
                        Buffer.BlockCopy((0).GetBytes(), 0, command, 15, 4);
                        Buffer.BlockCopy((0).GetBytes(), 0, command, 19, 4);

                        byte[] data = Utility.Encrypt(command, false);
                        if (gameServer != null) gameServer.Send(data);
                        break;
                    case ItemType.Equip:
                        if (player.Inventory[dialogBoxes[clickedDialogBox].SelectedItemIndex].IsEquipped)
                        {
                            UnEquipItem(dialogBoxes[clickedDialogBox].SelectedItemIndex, -1, true, true);
                        }
                        else EquipItem(dialogBoxes[clickedDialogBox].SelectedItemIndex); 
                        break;
                    case ItemType.DepleteDestination:
                        selectionMode = SelectionMode.ApplyItem;
                        selectionModeId = itemIndex;
                        break;
                    case ItemType.EnableDialogBox:
                        switch (item.EffectType)
                        {
                            case ItemEffectType.Xelima:
                            case ItemEffectType.Merien:
                                {
                                    if (dialogBoxes[GameDialogBoxType.UpgradeV2].Config.Hidden)
                                        dialogBoxes[GameDialogBoxType.UpgradeV2].Show();
                                    ChangeUpgradeItem(itemIndex);
                                    break;
                                }
                            case ItemEffectType.Slate:
                                if (dialogBoxes[GameDialogBoxType.Slate].Config.Hidden)
                                    dialogBoxes[GameDialogBoxType.Slate].Show();
                                break;
                            case ItemEffectType.Alchemy:
                                if (dialogBoxes[GameDialogBoxType.Alchemy].Config.Hidden)
                                    dialogBoxes[GameDialogBoxType.Alchemy].Show();
                                break;
                            case ItemEffectType.Manufacture:
                                if (dialogBoxes[GameDialogBoxType.Manufacture].Config.Hidden)
                                    dialogBoxes[GameDialogBoxType.Manufacture].Show();
                                break;
                            case ItemEffectType.Craft:
                                if (dialogBoxes[GameDialogBoxType.Craft].Config.Hidden)
                                    dialogBoxes[GameDialogBoxType.Craft].Show();
                                break;
                            default: break;

                        }
                        break;

                }
        }

        public void ApplyItem(int itemIndex, int destinationItemIndex)
        {
            if (player.IsDead) return;
            Item item = player.Inventory[itemIndex];

            switch (item.Type)
            {
                case ItemType.DepleteDestination:
                    byte[] command = new byte[60];
                    Buffer.BlockCopy(Utility.GetCommandTypeBytes(CommandType.Common), 0, command, 0, 4);
                    Buffer.BlockCopy(Utility.GetCommandTypeBytes(CommandMessageType.UseItem), 0, command, 4, 2);
                    Buffer.BlockCopy((0).GetBytes(), 0, command, 6, 2);
                    Buffer.BlockCopy((0).GetBytes(), 0, command, 8, 2);
                    command[10] = (int)0;
                    Buffer.BlockCopy(itemIndex.GetBytes(), 0, command, 11, 4);
                    Buffer.BlockCopy((0).GetBytes(), 0, command, 15, 4);
                    Buffer.BlockCopy((0).GetBytes(), 0, command, 19, 4);
                    Buffer.BlockCopy(destinationItemIndex.GetBytes(), 0, command, 53, 4);

                    byte[] data = Utility.Encrypt(command, false);
                    if (gameServer != null) gameServer.Send(data);
                    break;
            }
        }

        public void SetBagLocation(int itemIndex)
        {
            if (player.Inventory[itemIndex] == null) return;

            Item item = player.Inventory[itemIndex];

            byte[] command = new byte[20];
            Buffer.BlockCopy(Utility.GetCommandTypeBytes(CommandType.SetItemPosition), 0, command, 0, 4);
            command[6] = (byte)itemIndex;
            Buffer.BlockCopy((item.BagX).GetBytes(), 0, command, 7, 2);
            Buffer.BlockCopy((item.BagY).GetBytes(), 0, command, 9, 2);

            byte[] data = Utility.Encrypt(command, false);
            if (gameServer != null) gameServer.Send(data);
        }

        public void DropItem(int itemIndex)
        {
            Item item = player.Inventory[itemIndex];
            if (item == null) return;
            if (item.IsEquipped) { UnEquipItem(itemIndex); return; }
            if (item.IsUpgradeItem || item.IsUpgradeIngredient) { ChangeUpgradeItem(itemIndex); return; }

            // clear from merchant selections
            if (((MerchantDialogBox)dialogBoxes[GameDialogBoxType.Merchant]).SellList.Contains(itemIndex))
                ((MerchantDialogBox)dialogBoxes[GameDialogBoxType.Merchant]).SellList.Remove(itemIndex);
            if (((MerchantDialogBox)dialogBoxes[GameDialogBoxType.Merchant]).RepairList.Contains(itemIndex))
                ((MerchantDialogBox)dialogBoxes[GameDialogBoxType.Merchant]).RepairList.Remove(itemIndex);
            // clear from trade selections
            if (((TradeV2DialogBox)dialogBoxes[GameDialogBoxType.TradeV2]).TradeList.Contains(itemIndex)) RemoveTradeItem(itemIndex);

            byte[] command = new byte[60];
            Buffer.BlockCopy(Utility.GetCommandTypeBytes(CommandType.Common), 0, command, 0, 4);
            Buffer.BlockCopy(Utility.GetCommandTypeBytes(CommandMessageType.DropItem), 0, command, 4, 2);
            Buffer.BlockCopy((0).GetBytes(), 0, command, 6, 2);
            Buffer.BlockCopy((0).GetBytes(), 0, command, 8, 2);
            command[9] = (byte)0;
            Buffer.BlockCopy(itemIndex.GetBytes(), 0, command, 11, 4);
            Buffer.BlockCopy(item.ItemId.GetBytes(), 0, command, 15, 4);
            Buffer.BlockCopy(item.Count.GetBytes(), 0, command, 19, 4); // TODO count

            byte[] data = Utility.Encrypt(command, false);
            if (gameServer != null) gameServer.Send(data);

            player.RemoveFromInventory(itemIndex);
        }

        public void SwapItem(int itemIndex, int destinationItemIndex)
        {
            if (itemIndex == -1 || destinationItemIndex == -1) return;
            if (itemIndex == destinationItemIndex) return;
            Item item = player.Inventory[itemIndex];
            if (item == null) return;
            EquipType type = item.EquipType;

            // clear from merchant selections
            if (((MerchantDialogBox)dialogBoxes[GameDialogBoxType.Merchant]).SellList.Contains(itemIndex))
                ((MerchantDialogBox)dialogBoxes[GameDialogBoxType.Merchant]).SellList.Remove(itemIndex);
            if (((MerchantDialogBox)dialogBoxes[GameDialogBoxType.Merchant]).RepairList.Contains(itemIndex))
                ((MerchantDialogBox)dialogBoxes[GameDialogBoxType.Merchant]).RepairList.Remove(itemIndex);
            if (((MerchantDialogBox)dialogBoxes[GameDialogBoxType.Merchant]).SellList.Contains(destinationItemIndex))
                ((MerchantDialogBox)dialogBoxes[GameDialogBoxType.Merchant]).SellList.Remove(destinationItemIndex);
            if (((MerchantDialogBox)dialogBoxes[GameDialogBoxType.Merchant]).RepairList.Contains(destinationItemIndex))
                ((MerchantDialogBox)dialogBoxes[GameDialogBoxType.Merchant]).RepairList.Remove(destinationItemIndex);

            // clear from trade selections
            if (((TradeV2DialogBox)dialogBoxes[GameDialogBoxType.TradeV2]).TradeList.Contains(itemIndex)) RemoveTradeItem(itemIndex);
            if (((TradeV2DialogBox)dialogBoxes[GameDialogBoxType.TradeV2]).TradeList.Contains(destinationItemIndex)) RemoveTradeItem(destinationItemIndex);


            if (itemIndex < Globals.MaximumEquipment)// if moving from equipment slot
            {
                if (destinationItemIndex < Globals.MaximumEquipment) return; //Cant move equipment to a new equipment slot

                if (destinationItemIndex >= Globals.MaximumEquipment) //If moving from equipment slot to inventory slot
                {
                    UnEquipItem(itemIndex, destinationItemIndex); //Always unequip equipped item
                }
            }
            else // from to inventory slot
            {
                if (destinationItemIndex < Globals.MaximumEquipment)// moving to equipment always equip
                {
                    //figure

                    EquipItem(itemIndex); return; //equip item, don't swap
                }
                else
                {
                    byte[] command = new byte[60];
                    Buffer.BlockCopy(Utility.GetCommandTypeBytes(CommandType.Common), 0, command, 0, 4);
                    Buffer.BlockCopy(Utility.GetCommandTypeBytes(CommandMessageType.SwapItem), 0, command, 4, 2);
                    Buffer.BlockCopy((0).GetBytes(), 0, command, 6, 2);
                    Buffer.BlockCopy((0).GetBytes(), 0, command, 8, 2);
                    command[9] = (byte)0;
                    Buffer.BlockCopy(itemIndex.GetBytes(), 0, command, 11, 4);
                    Buffer.BlockCopy(destinationItemIndex.GetBytes(), 0, command, 15, 4);

                    byte[] data = Utility.Encrypt(command, false);
                    if (gameServer != null) gameServer.Send(data);

                    player.SwapItem(itemIndex, destinationItemIndex);
                }
            }
            //from inventory to inventory, just swap
        }

        public void SwapWarehouseItem(int itemIndex, int destinationItemIndex)
        {
            if (itemIndex == -1 || destinationItemIndex == -1) return;
            Item item = player.Warehouse[itemIndex];
            if (item == null) return;

            byte[] command = new byte[60];
            Buffer.BlockCopy(Utility.GetCommandTypeBytes(CommandType.Common), 0, command, 0, 4);
            Buffer.BlockCopy(Utility.GetCommandTypeBytes(CommandMessageType.SwapWarehouseItem), 0, command, 4, 2);
            Buffer.BlockCopy((0).GetBytes(), 0, command, 6, 2);
            Buffer.BlockCopy((0).GetBytes(), 0, command, 8, 2);
            command[9] = (byte)0;
            Buffer.BlockCopy(itemIndex.GetBytes(), 0, command, 11, 4);
            Buffer.BlockCopy(destinationItemIndex.GetBytes(), 0, command, 15, 4);

            byte[] data = Utility.Encrypt(command, false);
            if (gameServer != null) gameServer.Send(data);

            player.SwapWarehouseItem(itemIndex, destinationItemIndex);
        }

        public void ResetItemSet(int setNumber)
        {
            byte[] command = new byte[15];
            Buffer.BlockCopy(Utility.GetCommandTypeBytes(CommandType.Common), 0, command, 0, 4);
            Buffer.BlockCopy(Utility.GetCommandTypeBytes(CommandMessageType.ResetItemSet), 0, command, 4, 2);
            command[6] = (byte)setNumber;

            byte[] data = Utility.Encrypt(command, false);
            if (gameServer != null) gameServer.Send(data);

            for (int x = 0; x < Globals.MaximumTotalItems; x++) //Go through equipped items and inventory
            {
                if (player.Inventory[x] != null && player.Inventory[x].SetNumber == setNumber)
                {
                    player.Inventory[x].SetNumber = 0;
                }
            }

            AddEvent(string.Format("Set {0} cleared!", setNumber));
        }

        public void UseItemSet(int setNumber)
        {
            bool hasItems = false;
            switch (setNumber)
            {
                case 1: if (player.Inventory.ItemSet1HasItems) hasItems = true; break;
                case 2: if (player.Inventory.ItemSet2HasItems) hasItems = true; break;
                case 3: if (player.Inventory.ItemSet3HasItems) hasItems = true; break;
                case 4: if (player.Inventory.ItemSet4HasItems) hasItems = true; break;
                case 5: if (player.Inventory.ItemSet5HasItems) hasItems = true; break;
                default: break;
            }

            if (hasItems)
            {
                byte[] command = new byte[15];
                Buffer.BlockCopy(Utility.GetCommandTypeBytes(CommandType.Common), 0, command, 0, 4);
                Buffer.BlockCopy(Utility.GetCommandTypeBytes(CommandMessageType.UseItemSet), 0, command, 4, 2);
                command[6] = (byte)setNumber;

                byte[] data = Utility.Encrypt(command, false);
                if (gameServer != null) gameServer.Send(data);
            }
            else { AddEvent(string.Format("Set {0} is empty!", setNumber)); }
        }

        public void SetItemSet(int setNumber)
        {
            bool hasEquippedItems = false;
            for (int i = 0; i < Globals.MaximumEquipment; i++)
            {
                if (player.Inventory[i] != null) hasEquippedItems = true;
            }

            if (hasEquippedItems)
            {
                byte[] command = new byte[15];
                //byte[] command = new byte[15 + itemSlotIds.Count];
                Buffer.BlockCopy(Utility.GetCommandTypeBytes(CommandType.Common), 0, command, 0, 4);
                Buffer.BlockCopy(Utility.GetCommandTypeBytes(CommandMessageType.SetItemSet), 0, command, 4, 2);
                command[6] = (byte)setNumber;
                //command[7] = (byte)itemSlotIds.Count;
                //for (int i = 0; i < itemSlotIds.Count; i++)
                //   command[8 + i] = (byte)itemSlotIds[i];

                byte[] data = Utility.Encrypt(command, false);
                if (gameServer != null) gameServer.Send(data);

                for (int x = 0; x < Globals.MaximumTotalItems; x++) //Go through equipped items and inventory
                {
                    if (player.Inventory[x] != null && player.Inventory[x].SetNumber == setNumber)
                    {
                        player.Inventory[x].SetNumber = 0;
                    }
                }

                AddEvent(string.Format("Set {0} filled!", setNumber));
            }
            else { AddEvent("No items equipped to fill set with."); }
        }

        public void EquipItem(int itemIndex)
        {
            Item item = player.Inventory[itemIndex];

            if (item == null || item.IsEquipped) return;

            if (item.IsBroken)
            {
                AddEvent("This item is broken!");
                return;
            }

            if (item.LevelLimit > player.Level)
            {
                AddEvent("You are too low level to use this item!");
                return;
            }

            if (item.MinimumStrength > player.Strength + player.StrengthBonus)
            {
                AddEvent("You don't have enough strength to use this item!");
                return;
            }

            if (item.MinimumDexterity > player.Dexterity + player.DexterityBonus)
            {
                AddEvent("You don't have enough dexerity to use this item!");
                return;
            }

            if (item.MinimumIntelligence > player.Intelligence + player.IntelligenceBonus)
            {
                AddEvent("You don't have enough intelligence to use this item!");
                return;
            }

            if (item.MinimumMagic > player.Magic + player.MagicBonus)
            {
                AddEvent("You don't have enough magic to use this item!");
                return;
            }

            if (item.MinimumVitality > player.Vitality + player.VitalityBonus)
            {
                AddEvent("You don't have enough vitality to use this item!");
                return;
            }

            if (item.MinimumAgility > player.Agility + player.AgilityBonus)
            {
                AddEvent("You don't have enough agility to use this item!");
                return;
            }

            if (item.EquipType == EquipType.None)
            {
                AddEvent("You cannot equip this item!");
                return;
            }

            if (item.GenderLimit != GenderType.None && item.GenderLimit != player.Gender)
            {
                AddEvent("This item is not for your gender!");
                return;
            }

            //Todo add cannot equip because item is bound to another player
            
            // clear from merchant selections
            if (((MerchantDialogBox)dialogBoxes[GameDialogBoxType.Merchant]).SellList.Contains(itemIndex))
                ((MerchantDialogBox)dialogBoxes[GameDialogBoxType.Merchant]).SellList.Remove(itemIndex);
            if (((MerchantDialogBox)dialogBoxes[GameDialogBoxType.Merchant]).RepairList.Contains(itemIndex))
                ((MerchantDialogBox)dialogBoxes[GameDialogBoxType.Merchant]).RepairList.Remove(itemIndex);
            // clear from trade selections
            if (((TradeV2DialogBox)dialogBoxes[GameDialogBoxType.TradeV2]).TradeList.Contains(itemIndex)) RemoveTradeItem(itemIndex);

            byte[] command = new byte[27];
            Buffer.BlockCopy(Utility.GetCommandTypeBytes(CommandType.Common), 0, command, 0, 4);
            Buffer.BlockCopy(Utility.GetCommandTypeBytes(CommandMessageType.EquipItem), 0, command, 4, 2);
            Buffer.BlockCopy((0).GetBytes(), 0, command, 6, 2);
            Buffer.BlockCopy((0).GetBytes(), 0, command, 8, 2);
            command[10] = (int)0;
            Buffer.BlockCopy((itemIndex).GetBytes(), 0, command, 11, 4);
            Buffer.BlockCopy((0).GetBytes(), 0, command, 15, 4);
            Buffer.BlockCopy((0).GetBytes(), 0, command, 19, 4);

            byte[] data = Utility.Encrypt(command, false);
            if (gameServer != null) gameServer.Send(data);

            //switch (item.EquipType)
            //{
            //    case EquipType.Head:
            //    case EquipType.Body:
            //    case EquipType.Arms:
            //    case EquipType.Legs:
            //    case EquipType.Feet:
            //    case EquipType.Back:
            //        if (player.Inventory[(int)EquipType.Body] != null && player.Inventory[(int)EquipType.Body].EquipType == EquipType.FullBody)
            //        { 
            //            UnEquipItem((int)EquipType.Body, -1, false); 
            //        }
            //        UnEquipItem((int)item.EquipType, -1, false);
            //        break;
            //    case EquipType.FullBody:
            //        UnEquipItem((int)EquipType.Head, -1, false);
            //        UnEquipItem((int)EquipType.Body, -1, false);
            //        UnEquipItem((int)EquipType.Arms, -1, false);
            //        UnEquipItem((int)EquipType.Legs, -1, false);
            //        UnEquipItem((int)EquipType.Feet, -1, false);
            //        UnEquipItem((int)EquipType.Back, -1, false);
            //        break;
            //    case EquipType.LeftHand:
            //    case EquipType.RightHand:
            //        if (player.Inventory[(int)EquipType.RightHand] != null && player.Inventory[(int)EquipType.RightHand].EquipType == EquipType.DualHand)
            //        { 
            //            UnEquipItem((int)EquipType.RightHand, -1, false); 
            //        }
            //        UnEquipItem((int)item.EquipType, -1, false);
            //        break;
            //    case EquipType.DualHand:
            //        UnEquipItem((int)EquipType.RightHand, -1, false);
            //        UnEquipItem((int)EquipType.LeftHand, -1, false);
            //        break;
            //    default: UnEquipItem((int)item.EquipType, -1, false); break;
            //}

            //player.EquipItem(itemIndex);
            //AddEvent(string.Format("{0} has been equipped.", Cache.ItemConfiguration[item.ItemId].FriendlyName));
            //AudioHelper.PlaySound("E28");
        }

        //TODO don't need destination index anymore?
        public void UnEquipItem(int itemIndex, int destinationItemIndex = -1, bool notify = true, bool playSound = false)
        {
            Item item = player.Inventory[itemIndex];

            // clear from merchant selections
            if (((MerchantDialogBox)dialogBoxes[GameDialogBoxType.Merchant]).SellList.Contains(itemIndex))
                ((MerchantDialogBox)dialogBoxes[GameDialogBoxType.Merchant]).SellList.Remove(itemIndex);
            if (((MerchantDialogBox)dialogBoxes[GameDialogBoxType.Merchant]).RepairList.Contains(itemIndex))
                ((MerchantDialogBox)dialogBoxes[GameDialogBoxType.Merchant]).RepairList.Remove(itemIndex);
            // clear from trade selections
            if (((TradeV2DialogBox)dialogBoxes[GameDialogBoxType.TradeV2]).TradeList.Contains(itemIndex)) RemoveTradeItem(itemIndex);

            if (item != null && item.IsEquipped)
            {
                if (notify) // fix for equip/unequip swap (happens twice otherwise, causing both items to become unequipeped)
                {
                    byte[] command = new byte[27];
                    Buffer.BlockCopy(Utility.GetCommandTypeBytes(CommandType.Common), 0, command, 0, 4);
                    Buffer.BlockCopy(Utility.GetCommandTypeBytes(CommandMessageType.UnEquipItem), 0, command, 4, 2);
                    Buffer.BlockCopy((0).GetBytes(), 0, command, 6, 2);
                    Buffer.BlockCopy((0).GetBytes(), 0, command, 8, 2);
                    command[10] = (int)0;
                    Buffer.BlockCopy((itemIndex).GetBytes(), 0, command, 11, 4);
                    Buffer.BlockCopy((destinationItemIndex).GetBytes(), 0, command, 15, 4);
                    Buffer.BlockCopy((0).GetBytes(), 0, command, 19, 4);

                    byte[] data = Utility.Encrypt(command, false);
                    if (gameServer != null) gameServer.Send(data);
                }

                //player.UnEquipItem(itemIndex, destinationItemIndex);
                //AddEvent(string.Format("{0} has been unequipped.", Cache.ItemConfiguration[item.ItemId].FriendlyName));
                if (playSound)
                    if (item.Category == ItemCategory.Angels)
                        AudioHelper.PlaySound("E53");
                    else AudioHelper.PlaySound("E29");
            }
        }

        public void ChangeDetailMode(GraphicsDetail detailMode, bool notify = true)
        {
            Cache.GameSettings.DetailMode = detailMode;

            switch (detailMode)
            {
                case GraphicsDetail.High: 
                    //Globals.OffScreenCells = 9;
                    if (Cache.MapView != null) Cache.MapView.SetPivot(player.X, player.Y);
                    break;
                case GraphicsDetail.Medium: 
                    //Globals.OffScreenCells = 6;
                    if (Cache.MapView != null) Cache.MapView.SetPivot(player.X, player.Y);
                    break;
                case GraphicsDetail.Low: 
                    //Globals.OffScreenCells = 3;
                    if (Cache.MapView != null) Cache.MapView.SetPivot(player.X, player.Y);
                    break;
            }

            if (notify) 
                AddEvent("Detail Level : " + detailMode.ToString().ToUpper());
        }

        public void ChangeStats()
        {
            byte[] command = new byte[35];
            Buffer.BlockCopy(Utility.GetCommandTypeBytes(CommandType.ChangeStatsLevelUp), 0, command, 0, 4);

            Buffer.BlockCopy(player.LevelUpStrength.GetBytes(), 0, command, 6, 4);
            Buffer.BlockCopy(player.LevelUpVitality.GetBytes(), 0, command, 10, 4);
            Buffer.BlockCopy(player.LevelUpDexterity.GetBytes(), 0, command, 14, 4);
            Buffer.BlockCopy(player.LevelUpIntelligence.GetBytes(), 0, command, 18, 4);
            Buffer.BlockCopy(player.LevelUpMagic.GetBytes(), 0, command, 22, 4);
            Buffer.BlockCopy(player.LevelUpAgility.GetBytes(), 0, command, 26, 4);
            

            byte[] data = Utility.Encrypt(command, false);
            if (gameServer != null) gameServer.Send(data);
        }

        public void Fish(int dX, int dY)
        {
            byte[] command = new byte[20];
            Buffer.BlockCopy(Utility.GetCommandTypeBytes(CommandType.Common), 0, command, 0, 4);
            Buffer.BlockCopy(Utility.GetCommandTypeBytes(CommandMessageType.Fish), 0, command, 4, 2);
            Buffer.BlockCopy(BitConverter.GetBytes(player.X), 0, command, 6, 2);
            Buffer.BlockCopy(BitConverter.GetBytes(player.Y), 0, command, 8, 2);
            Buffer.BlockCopy(BitConverter.GetBytes(dX), 0, command, 10, 2);
            Buffer.BlockCopy(BitConverter.GetBytes(dY), 0, command, 12, 2);

            byte[] data = Utility.Encrypt(command, false);
            if (gameServer != null) gameServer.Send(data);
        }

        public void ActivateAbility(ItemSpecialAbilityType type)
        {
            byte[] command = new byte[15];
            Buffer.BlockCopy(Utility.GetCommandTypeBytes(CommandType.Common), 0, command, 0, 4);
            Buffer.BlockCopy(Utility.GetCommandTypeBytes(CommandMessageType.UseSpecialAbility), 0, command, 4, 2);
            Buffer.BlockCopy(BitConverter.GetBytes((int)type), 0, command, 6, 4);

            byte[] data = Utility.Encrypt(command, false);
            if (gameServer != null) gameServer.Send(data);
        }

        public void Attack(MotionDirection direction, AttackType attackType, int dX, int dY)
        {
            byte[] command = new byte[25];
            Buffer.BlockCopy(Utility.GetCommandTypeBytes(CommandType.RequestMotion), 0, command, 0, 4);
            Buffer.BlockCopy(((int)MotionType.Attack).GetBytes(), 0, command, 4, 2);
            Buffer.BlockCopy(BitConverter.GetBytes(player.X), 0, command, 6, 2);
            Buffer.BlockCopy(BitConverter.GetBytes(player.Y), 0, command, 8, 2);
            command[10] = (byte)((int)direction);
            Buffer.BlockCopy(BitConverter.GetBytes(dX), 0, command, 11, 2);
            Buffer.BlockCopy(BitConverter.GetBytes(dY), 0, command, 13, 2);
            command[15] = (byte)((int)attackType);
            Buffer.BlockCopy(BitConverter.GetBytes(selectedObjectId), 0, command, 16, 2);
            Buffer.BlockCopy(BitConverter.GetBytes((int)selectedObjectType), 0, command, 18, 2);

            byte[] data = Utility.Encrypt(command, false);
            if (gameServer != null) gameServer.Send(data);
        }

        public void Dash(MotionDirection direction, AttackType attackType, int dX, int dY)
        {
            byte[] command = new byte[45];
            Buffer.BlockCopy(Utility.GetCommandTypeBytes(CommandType.RequestMotion), 0, command, 0, 4);
            Buffer.BlockCopy(((int)MotionType.Dash).GetBytes(), 0, command, 4, 2);
            Buffer.BlockCopy(BitConverter.GetBytes(player.X), 0, command, 6, 2);
            Buffer.BlockCopy(BitConverter.GetBytes(player.Y), 0, command, 8, 2);
            command[10] = (byte)((int)direction);
            Buffer.BlockCopy(BitConverter.GetBytes(dX), 0, command, 11, 2);
            Buffer.BlockCopy(BitConverter.GetBytes(dY), 0, command, 13, 2);
            command[15] = (byte)((int)attackType);
            Buffer.BlockCopy(BitConverter.GetBytes(selectedObjectId), 0, command, 16, 2);
            Buffer.BlockCopy(BitConverter.GetBytes((int)selectedObjectType), 0, command, 18, 2);

            byte[] data = Utility.Encrypt(command, false);
            if (gameServer != null) gameServer.Send(data);
        }

        public void Fly(MotionDirection direction)
        {
            byte[] command = new byte[25];
            Buffer.BlockCopy(Utility.GetCommandTypeBytes(CommandType.RequestMotion), 0, command, 0, 4);
            Buffer.BlockCopy(((int)MotionType.Fly).GetBytes(), 0, command, 4, 2);
            Buffer.BlockCopy(BitConverter.GetBytes(player.X), 0, command, 6, 2);
            Buffer.BlockCopy(BitConverter.GetBytes(player.Y), 0, command, 8, 2);
            command[10] = (byte)((int)direction);

            byte[] data = Utility.Encrypt(command, false);
            if (gameServer != null) gameServer.Send(data);
        }

        public void GetGuildName(uint ownerId)
        {
            byte[] command = new byte[27];
            Buffer.BlockCopy(Utility.GetCommandTypeBytes(CommandType.Common), 0, command, 0, 4);
            Buffer.BlockCopy(Utility.GetCommandTypeBytes(CommandMessageType.GetGuildName), 0, command, 4, 2);
            Buffer.BlockCopy(BitConverter.GetBytes(ownerId), 0, command, 11, 4);

            byte[] data = Utility.Encrypt(command, false);
            if (gameServer != null) gameServer.Send(data);
        }

        public void GetMerchantData(int merchantId, int page, ItemDisplayGroup filter, Dictionary<ItemStat, bool> statFilter, Dictionary<ItemCategory, bool> categoryFilter, string textFilter)
        {
            byte[] command = new byte[32 + statFilter.Count + categoryFilter.Count];
            Buffer.BlockCopy(Utility.GetCommandTypeBytes(CommandType.RequestMerchantData), 0, command, 0, 4);
            command[6] = (byte)merchantId;
            Buffer.BlockCopy(page.GetBytes(), 0, command, 7, 2);
            command[9] = (byte)(int)filter;
            Buffer.BlockCopy(textFilter.GetBytes(20), 0, command, 10, 20);
            // 30 = stat filter count
            // 31 = category filter count

            int index = 31;
            int count = 0;
            foreach (KeyValuePair<ItemStat, bool> stat in statFilter)
                if (stat.Value)
                {
                    command[++index] = (byte)(int)stat.Key;
                    count++;
                }
            command[30] = (byte)count; // count

            count = 0;
            foreach (KeyValuePair<ItemCategory, bool> category in categoryFilter)
                if (category.Value)
                {
                    command[++index] = (byte)(int)category.Key;
                    count++;
                }
            command[31] = (byte)count; // count

            byte[] data = Utility.Encrypt(command, false);
            if (gameServer != null) gameServer.Send(data);
        }

        public void BuyItem(int merchantId, int itemId, int page, ItemDisplayGroup filter)
        {
            byte[] command = new byte[20];
            Buffer.BlockCopy(Utility.GetCommandTypeBytes(CommandType.Common), 0, command, 0, 4);
            Buffer.BlockCopy(Utility.GetCommandTypeBytes(CommandMessageType.BuyMerchantItem), 0, command, 4, 2);
            command[6] = (byte)merchantId;
            Buffer.BlockCopy(itemId.GetBytes(), 0, command, 7, 4);
            Buffer.BlockCopy(page.GetBytes(), 0, command, 11, 2);
            command[13] = (byte)(int)filter;

            byte[] data = Utility.Encrypt(command, false);
            if (gameServer != null) gameServer.Send(data);
        }

        public void SellItem(int merchantId, int itemIndex, int page, ItemDisplayGroup filter)
        {
            byte[] command = new byte[20];
            Buffer.BlockCopy(Utility.GetCommandTypeBytes(CommandType.Common), 0, command, 0, 4);
            Buffer.BlockCopy(Utility.GetCommandTypeBytes(CommandMessageType.SellMerchantItem), 0, command, 4, 2);
            command[6] = (byte)merchantId;
            Buffer.BlockCopy(itemIndex.GetBytes(), 0, command, 7, 4);
            Buffer.BlockCopy(page.GetBytes(), 0, command, 11, 2);
            command[13] = (byte)(int)filter;

            byte[] data = Utility.Encrypt(command, false);
            if (gameServer != null) gameServer.Send(data);
        }

        public void SellItemList(int merchantId, List<int> itemIndexes, int page, ItemDisplayGroup filter)
        {
            byte[] command = new byte[15 + (itemIndexes.Count*4)];
            Buffer.BlockCopy(Utility.GetCommandTypeBytes(CommandType.Common), 0, command, 0, 4);
            Buffer.BlockCopy(Utility.GetCommandTypeBytes(CommandMessageType.SellMerchantItemList), 0, command, 4, 2);
            command[6] = (byte)merchantId;
            Buffer.BlockCopy(page.GetBytes(), 0, command, 7, 2);
            command[9] = (byte)(int)filter;
            command[10] = (byte)itemIndexes.Count;
            for (int i = 0; i < itemIndexes.Count; i++)
                Buffer.BlockCopy(itemIndexes[i].GetBytes(), 0, command, 11 + (i*4), 4);

            byte[] data = Utility.Encrypt(command, false);
            if (gameServer != null) gameServer.Send(data);
        }

        public void RepairItem(int merchantId, int itemIndex, int page, ItemDisplayGroup filter)
        {
            byte[] command = new byte[20];
            Buffer.BlockCopy(Utility.GetCommandTypeBytes(CommandType.Common), 0, command, 0, 4);
            Buffer.BlockCopy(Utility.GetCommandTypeBytes(CommandMessageType.RepairMerchantItem), 0, command, 4, 2);
            command[6] = (byte)merchantId;
            Buffer.BlockCopy(itemIndex.GetBytes(), 0, command, 7, 4);
            Buffer.BlockCopy(page.GetBytes(), 0, command, 11, 2);
            command[13] = (byte)(int)filter;

            byte[] data = Utility.Encrypt(command, false);
            if (gameServer != null) gameServer.Send(data);
        }

        public void RepairItemList(int merchantId, List<int> itemIndexes, int page, ItemDisplayGroup filter)
        {
            byte[] command = new byte[15 + (itemIndexes.Count * 4)];
            Buffer.BlockCopy(Utility.GetCommandTypeBytes(CommandType.Common), 0, command, 0, 4);
            Buffer.BlockCopy(Utility.GetCommandTypeBytes(CommandMessageType.RepairMerchantItemList), 0, command, 4, 2);
            command[6] = (byte)merchantId;
            Buffer.BlockCopy(page.GetBytes(), 0, command, 7, 2);
            command[9] = (byte)(int)filter;
            command[10] = (byte)itemIndexes.Count;
            for (int i = 0; i < itemIndexes.Count; i++)
                Buffer.BlockCopy(itemIndexes[i].GetBytes(), 0, command, 11 + (i * 4), 4);

            byte[] data = Utility.Encrypt(command, false);
            if (gameServer != null) gameServer.Send(data);
        }

        public void ChangeUpgradeItem(int itemIndex)
        {
            if (itemIndex >= 0 && itemIndex < Globals.MaximumTotalItems)
            {
                if (player.Inventory[itemIndex] != null)
                {
                    Item item = player.Inventory[itemIndex];                   
                    if (item.UpgradeType != ItemUpgradeType.None)
                    {
                        if (item.UpgradeType == ItemUpgradeType.Hero) { AddEvent("Hero upgrade not implemented"); return; }
                        if (item.Level == Globals.MaximumItemLevel) { AddEvent("Item is max level"); return; }
                        if (item.IsBroken) { AddEvent("Broken Items can not be upgraded"); return; }
                        if (item.UpgradeType == ItemUpgradeType.Ingredient)
                        {
                            if (player.Inventory.UpgradeItemIndex != -1 && player.Inventory[player.Inventory.UpgradeItemIndex] != null)
                            {
                                if (player.Inventory[player.Inventory.UpgradeItemIndex].UpgradeType == ItemUpgradeType.Majestic)
                                {
                                    AddEvent("Upgrade ingredient not needed"); return;
                                }
                            }
                        }



                        byte[] command = new byte[15];
                        Buffer.BlockCopy(Utility.GetCommandTypeBytes(CommandType.Common), 0, command, 0, 4);
                        Buffer.BlockCopy(Utility.GetCommandTypeBytes(CommandMessageType.ChangeUpgradeItem), 0, command, 4, 2);
                        Buffer.BlockCopy(itemIndex.GetBytes(), 0, command, 6, 4);

                        byte[] data = Utility.Encrypt(command, false);
                        if (gameServer != null) gameServer.Send(data);
                    }
                    else { AddEvent("Item cannot be upgraded or is not an upgrade ingredient"); }
                }
            }
        }

        public void ClearUpgradeItems()
        {
            player.Inventory.ClearUpgradeItems();

            byte[] command = new byte[15];
            Buffer.BlockCopy(Utility.GetCommandTypeBytes(CommandType.Common), 0, command, 0, 4);
            Buffer.BlockCopy(Utility.GetCommandTypeBytes(CommandMessageType.ClearUpgradeItems), 0, command, 4, 2);

            byte[] data = Utility.Encrypt(command, false);
            if (gameServer != null) gameServer.Send(data);
        }


        public void UpgradeItem() //double check this at some point
        {
            int itemIndex = player.Inventory.UpgradeItemIndex;
            int ingredientIndex = player.Inventory.UpgradeIngredientIndex;

            if (itemIndex == -1) { AddEvent("No item to upgrade"); return; }

            if (player.Inventory[itemIndex] != null)
            {
                Item item = player.Inventory[itemIndex];
                if (item.UpgradeType == ItemUpgradeType.None) { AddEvent("Item is not upgradedable"); return; }
                if (item.UpgradeType == ItemUpgradeType.Hero) { AddEvent("Not implemented"); return; }
                if (item.Level >= Globals.MaximumItemLevel) { AddEvent("Item is already max level"); return; }
                if (item.IsBroken) { AddEvent("Broken items cannot be upgraded"); return; }
                if (item.UpgradeType == ItemUpgradeType.Majestic && player.Majestics < item.MajesticUpgradeCost()) { AddEvent(string.Format("Item requires {0} more Majestics to upgrade", item.MajesticUpgradeCost() - player.Majestics)); return; }
                if (item.UpgradeType == ItemUpgradeType.Merien || item.UpgradeType == ItemUpgradeType.Xelima)
                {
                    if (ingredientIndex == -1) { AddEvent("No upgrade ingredient present"); return; }
                    if (player.Inventory[ingredientIndex] != null)
                    {
                        Item ingredient = player.Inventory[ingredientIndex];
                        switch (item.UpgradeType)
                        {
                            case ItemUpgradeType.Merien: if (ingredient.EffectType != ItemEffectType.Merien) { AddEvent("Incorrect upgrade ingredient."); return; } break;
                            case ItemUpgradeType.Xelima: if (ingredient.EffectType != ItemEffectType.Xelima) { AddEvent("Incorrect upgrade ingredient."); return; } break;
                            default: break;
                        }
                    }
                }

                byte[] command = new byte[15];
                Buffer.BlockCopy(Utility.GetCommandTypeBytes(CommandType.Common), 0, command, 0, 4);
                Buffer.BlockCopy(Utility.GetCommandTypeBytes(CommandMessageType.UpgradeItem), 0, command, 4, 2);
                
                byte[] data = Utility.Encrypt(command, false);
                if (gameServer != null) gameServer.Send(data);
            }
            else { AddEvent("No item to upgrade."); }
        }

        public void PickUp(MotionDirection direction)
        {
            byte[] command = new byte[45];
            Buffer.BlockCopy(Utility.GetCommandTypeBytes(CommandType.RequestMotion), 0, command, 0, 4);
            Buffer.BlockCopy(((int)MotionType.PickUp).GetBytes(), 0, command, 4, 2);
            Buffer.BlockCopy(player.X.GetBytes(), 0, command, 6, 2);
            Buffer.BlockCopy(player.Y.GetBytes(), 0, command, 8, 2);
            command[10] = (byte)(int)direction;

            byte[] data = Utility.Encrypt(command, false);
            if (gameServer != null) gameServer.Send(data);
        }

        public void ToggleCombatMode()
        {
            if (player.IsCombatMode) { AddEvent("Converted to peace mode."); }
            else { AddEvent("Converted to attack mode."); }

            byte[] command = new byte[15];
            Buffer.BlockCopy(Utility.GetCommandTypeBytes(CommandType.Common), 0, command, 0, 4);
            Buffer.BlockCopy(Utility.GetCommandTypeBytes(CommandMessageType.ToggleCombatMode), 0, command, 4, 2);

            byte[] data = Utility.Encrypt(command, false);
            if (gameServer != null) gameServer.Send(data);
        }

        public void ToggleSafeMode()
        {
            byte[] command = new byte[15];
            Buffer.BlockCopy(Utility.GetCommandTypeBytes(CommandType.Common), 0, command, 0, 4);
            Buffer.BlockCopy(Utility.GetCommandTypeBytes(CommandMessageType.ToggleSafeMode), 0, command, 4, 2);

            byte[] data = Utility.Encrypt(command, false);
            if (gameServer != null) gameServer.Send(data);
        }

        private void Logout(bool force = false)
        {
            AudioHelper.StopMusic();
            SaveDialogBoxConfiguration();
            SaveHotKeyConfiguration();
            SaveGameSettings();
            LeaveParty();

            if (Disconnect != null) Disconnect(null, null);
            Cache.GameSettings.IsLoggingOut = false;

            if (!force)
            {
                gameServer.Close("Clean Log Out");
            }
        }

        public void Update(GameTime gameTime, Microsoft.Xna.Framework.Game game)
        {
            // frame rate
            frameElapsedTime += gameTime.ElapsedGameTime;
            if (frameElapsedTime > TimeSpan.FromSeconds(1))
            {
                frameElapsedTime -= TimeSpan.FromSeconds(1);
                frameRate = frameCounter;
                frameCounter = 0;
                //data in & out
                dataInRate = ((double)dataInCounter / 1024);
                dataOutRate = ((double)gameServer.dataOutCounter / 1024);
                dataInCounter = gameServer.dataOutCounter = 0;
            }

            // handle input
            Display.Mouse.Update(Mouse.GetState());
            Display.Keyboard.Update(Keyboard.GetState());
            HandleKeyboard(game.IsActive);
            HandleMouse(game.IsActive);

            //Update regen rotation effect
            if (isRotationAdditive)
            {
                if (regenRotation > 10)
                {
                    regenRotation = 10;
                    isRotationAdditive = false;
                }
                else
                {
                    regenRotation += 0.01f;
                }
            }
            else
            {
                if (regenRotation < 0)
                {
                    regenRotation = 0;
                    isRotationAdditive = true;
                }
                else
                {
                    regenRotation -= 0.01f;
                }
            }

            Cache.TransparencyFaders.Update();

            ////Update blood Effect
            //if (player.IsDamaged == true)
            //{
            //    TimeSpan ts = DateTime.Now - player.FlashDamage;
            //    if (ts.TotalSeconds >= 3)
            //    {
            //        player.FlashDamage = DateTime.Now;
            //        Random rand = new Random();
            //        int bloodspots = rand.Next(15, 30);
            //        int largeBloodSpots = rand.Next(7, 15);
            //        int smallBloodSpots = bloodspots - largeBloodSpots;
            //        for (int j = 0; j < largeBloodSpots; j++)
            //        {
            //            float frame = rand.Next(0, 1);
            //            float texture = (float)SpriteId.BloodLarge;
            //            float rotation = rand.Next(0, 10);
            //            float x = rand.Next(display.Device.Viewport.Width / 6, (display.Device.Viewport.Width / 6) * 5);
            //            float y = rand.Next(display.Device.Viewport.Height / 6, (display.Device.Viewport.Height / 6) * 5);
            //            float[] bloodspot = new float[6] { frame, 0.4f, x, y, rotation, texture };
            //            bloodSplaters.Add(bloodspot);
            //            player.IsDamaged = false;
            //        }

            //        for (int i = 0; i < smallBloodSpots; i++)
            //        {
            //            float frame = rand.Next(0, 8);
            //            float texture = (float)SpriteId.Blood;
            //            float rotation = rand.Next(0, 10);
            //            float x = rand.Next(display.Device.Viewport.Width / 6, (display.Device.Viewport.Width / 6) * 5);
            //            float y = rand.Next(display.Device.Viewport.Height / 6, (display.Device.Viewport.Height / 6) * 5);
            //            float[] bloodspot = new float[6] { frame, 0.4f, x, y, rotation, texture };
            //            bloodSplaters.Add(bloodspot);
            //        }
            //        player.IsDamaged = false;
            //    }
            //}

            //if (bloodSplaters.Count > 0)
            //{
            //    for (int i = 0; i < bloodSplaters.Count; i++)
            //    {
            //        if (bloodSplaters[i][1] > 0.324f) { bloodSplaters[i][1] = bloodSplaters[i][1] - 0.002f; }
            //        else { bloodSplaters[i][1] = bloodSplaters[i][1] - 0.01f; }
            //        if (bloodSplaters[i][1] <= 0.00f) { bloodSplaters.RemoveAt(i); }
            //    }
            //}

            // handle network commands
            while (processQueue.Count > 0 && processQueue.Peek() != null)
            {
                Command command;
                lock (processQueueLock)
                {
                    command = processQueue.Dequeue();
                }
                ServerProcess(Utility.Decrypt(command.Data), command.Identity, command.IPAddress);
            }

            // update objects on screen
            if (Cache.MapView.Map != null)
            {
                int indexY = Cache.MapView.PivotY;
                for (int iy = -(Globals.CellHeight * Globals.OffScreenCells); iy < display.GameBoardHeight + (Globals.CellHeight * Globals.OffScreenCells); iy += Globals.CellHeight)
                {
                    int indexX = Cache.MapView.PivotX;
                    for (int ix = -(Globals.CellWidth * Globals.OffScreenCells); ix < display.GameBoardWidth + (Globals.CellWidth * Globals.OffScreenCells); ix += Globals.CellWidth)
                    {
                        if (Cache.MapView.IsWithinBounds(indexX, indexY))
                        {
                            if (Cache.MapView.Map[indexY][indexX].DeadOwner != null) { Cache.MapView.Map[indexY][indexX].DeadOwner.Update(); }

                            if (Cache.MapView.Map[indexY][indexX].Owner != null) { Cache.MapView.Map[indexY][indexX].Owner.Update(); }

                            if (Cache.MapView.Map[indexY][indexX].DynamicObject != null) { Cache.MapView.Map[indexY][indexX].DynamicObject.Update(); }
                        }
                        indexX++;
                    }
                    indexY++;
                }
            }

            // update effects
            for (int i = 0; i < effects.Count; i++) effects[i].Update();

            /* update weather */
            if (weather != null) weather.Update();

            // remove old chat
            if (chatHistory.Count > 0)
                for (int i = 0; i < chatHistory.Count; i++)
                    if ((DateTime.Now - chatHistory[i].MessageTime).Seconds >= 5)
                        chatHistory.RemoveAt(i);

            // remove old events
            if (eventHistory.Count > 0)
                for (int i = 0; i < eventHistory.Count; i++)
                    if ((DateTime.Now - eventHistory[i].MessageTime).Seconds >= 5)
                        eventHistory.RemoveAt(i);

            // remove old objects
            List<ushort> ids = new List<ushort>();
            foreach (ushort id in Cache.OwnerCache.Keys)
                ids.Add(id);

            foreach (ushort id in ids)
                if (!Cache.MapView.IsWithinView(display.Resolution, Cache.OwnerCache[id]))
                    Cache.OwnerCache[id].Remove();

            // update dialogboxes

            foreach (IGameDialogBox box in dialogBoxes.Values) box.Update(gameTime);
            // update chat box
            if (chatMode) chatBox.Update(gameTime);

            // handle log out
            if (Cache.GameSettings.IsLoggingOut)
                if ((DateTime.Now - Cache.GameSettings.LogOutTime).TotalMilliseconds > 1000)
                {
                    Cache.GameSettings.LogOutCount--;
                    AddEvent("Logging out..." + Cache.GameSettings.LogOutCount);
                    Cache.GameSettings.LogOutTime = DateTime.Now;

                    if (Cache.GameSettings.LogOutCount <= 0) Logout();
                }

            // handle restart
            if (Cache.GameSettings.IsRestarting)
                if ((DateTime.Now - Cache.GameSettings.RestartTime).TotalMilliseconds > 1000)
                {
                    player.DamageHistory.Clear();
                    player.DamageHistoryStageTwo.Clear();
                    player.SpellHistory.Clear();
                    player.Flying = false;
                    Cache.GameSettings.RestartCount--;
                    AddEvent("Restarting game..." + Cache.GameSettings.RestartCount);
                    Cache.GameSettings.RestartTime = DateTime.Now;

                    if (Cache.GameSettings.RestartCount <= 0)
                    {
                        Restart();
                        Cache.GameSettings.IsRestarting = false;
                    }
                }

            //Level History
            if (levelHistory.Count > 0)
            {
                for (int i = 0; i < levelHistory.Count; i++)
                {
                    ChatMessage message = levelHistory[i];
                    if (((int)(DateTime.Now - message.MessageTime).TotalMilliseconds / 20) > message.AnimationTime)
                    {
                        message.AnimationTime += 1;
                        //message.Y += 1;
                        if (message.AnimationTime > 100) { message.Transparency -= 0.02f; }
                    }

                    //Remove  
                    if ((DateTime.Now - message.MessageTime).Seconds >= 3) { levelHistory.RemoveAt(i); }
                }
            }
        }

        public void Draw(SpriteBatch spriteBatch, GameTime gameTime)
        {
            //Set Render Target to draw game board
            spriteBatch.End();
            Display.Device.SetRenderTarget(gameBoardRenderTarget);
            Display.Device.Clear(Color.Black);
            spriteBatch.Begin(SpriteSortMode.Deferred, null, null, null, null, null);

            Rectangle playerBounds = Rectangle.Empty;

            //reset popup info
            popupType = GameDialogBoxType.None;

            //reset draggedItem
            //draggedType = DraggedType.None;

            frameCounter++;

            int drawX, drawY;
            if (Cache.MapView.Map != null)
            {
                if (selectionMode != SelectionMode.None)
                    Display.Mouse.State = GameMouseState.Selection;
                else if (player.IsCasting)
                {
                    if (player.SpellReady)
                        Display.Mouse.State = GameMouseState.Magic;
                    else Display.Mouse.State = GameMouseState.Loading;
                }
                else Display.Mouse.State = GameMouseState.Normal;

                /* LAYER 1 - MAP TILES */ //TODO arrange maptiles into large 2048x2048 textures for fast drawing, create grid texture to draw
                int indexX, indexY, sprite, spriteFrame, spriteColour;
                GameColor itemColorType = GameColor.Normal;

                indexY = Cache.MapView.PivotY;
                for (int iy = -(Globals.CellHeight * Globals.OffScreenCells); iy < display.GameBoardHeight + (Globals.CellHeight * Globals.OffScreenCells); iy += Globals.CellHeight)
                {
                    indexX = Cache.MapView.PivotX;
                    for (int ix = -(Globals.CellWidth * Globals.OffScreenCells); ix < display.GameBoardWidth + (Globals.CellWidth * Globals.OffScreenCells); ix += Globals.CellWidth)
                    {
                        if (Cache.MapView.IsWithinBounds(indexX, indexY))
                        {
                            sprite = Cache.MapView.Map[indexY][indexX].Sprite;
                            spriteFrame = Cache.MapView.Map[indexY][indexX].SpriteFrame;

                            if (Cache.Tiles.ContainsKey(sprite) && Cache.Tiles[sprite].Frames.Count > spriteFrame && Cache.Tiles[sprite].Frames[spriteFrame] != null)
                            {
                                spriteBatch.Draw(Cache.Tiles[sprite].Texture, new Vector2(ix + -player.OffsetX, iy + -player.OffsetY), Cache.Tiles[sprite].Frames[spriteFrame].GetRectangle(), Color.White);

                                if (display.DebugMode || tileoverlaymode)
                                {
                                    Rectangle r = new Rectangle(ix + -player.OffsetX, iy + -player.OffsetY, 32, 32);
                                    Color c = (Cache.MapView.Map[indexY][indexX].IsTeleport ? Color.Blue : (Cache.MapView.Map[indexY][indexX].IsMoveAllowed) ? Color.Black : Color.Yellow);

                                    spriteBatch.Draw(Cache.BackgroundTexture, new Rectangle(r.Left + 1, r.Top + 1, 1, r.Height), c); // Left
                                    spriteBatch.Draw(Cache.BackgroundTexture, new Rectangle(r.Right - 1, r.Top + 1, 1, r.Height), c); // Right
                                    spriteBatch.Draw(Cache.BackgroundTexture, new Rectangle(r.Left + 1, r.Top + 1, r.Width, 1), c); // Top
                                    spriteBatch.Draw(Cache.BackgroundTexture, new Rectangle(r.Left + 1, r.Bottom - 1, r.Width, 1), c); // Bottom
                                }
                            }
                            indexX++;
                        }
                    }
                    indexY++;
                }

                /* LAYER 1.5 - WEAPON RANGE OVERLAY */ //TODO - Create a texture to reference for this add to maptile? or item
                if (overlayMode) //Show overlay mode or not
                {
                    List<int[]> hitLocations, linehitLocations, spiralHitLocations;
                    int[] center;
                    Rectangle rangeOverlay;
                    GameColor color = (player.Side == OwnerSide.Aresden) ? GameColor.Global : (player.Side == OwnerSide.Elvine) ? GameColor.Town : GameColor.GameMaster;
                    float colorMultiplier = 0.2f;
                    Color rangeOverlayColour = Cache.Colors[color] * colorMultiplier;
                    IOwner owner;
                    if (overlayMagicMode) //Show magic overlay //TODO clean this up now that I know better ughhh
                    {
                        if (player.IsCasting || player.SpellReady)
                        {
                            int magicID = player.SpellCharged;
                            if (Cache.MagicConfiguration.ContainsKey(magicID))
                            {
                                Magic spell = Cache.MagicConfiguration[magicID];

                                // get the center X,Y for the overlay
                                int destinationX, destinationY;
                                switch (spell.Type)
                                {
                                    case MagicType.Kinesis: // kinetic spells originate from the player
                                    case MagicType.ExploitCorpse: // exploits corpse in radius of player
                                    case MagicType.TurnUndead: // turns players undead
                                    case MagicType.GhostSpiral: // spirit ghost spell
                                        destinationX = Display.PlayerCenterX;
                                        destinationY = Display.PlayerCenterY;
                                        break;
                                    default:
                                        if (selectedObjectId != -1 && selectedObjectType == ObjectType.Owner && Cache.OwnerCache.ContainsKey((UInt16)selectedObjectId))
                                        {
                                            destinationX = Cache.OwnerCache[(UInt16)selectedObjectId].X - Cache.MapView.PivotX - Globals.OffScreenCells;
                                            destinationY = Cache.OwnerCache[(UInt16)selectedObjectId].Y - Cache.MapView.PivotY - Globals.OffScreenCells;
                                        }
                                        else
                                        {
                                            destinationX = Display.Mouse.CellX;
                                            destinationY = Display.Mouse.CellY;
                                        }
                                        break;
                                }

                                switch (spell.Type)
                                {
                                    default:
                                    case MagicType.Berserk:
                                    case MagicType.Cancellation:
                                    case MagicType.CreateItem:
                                    case MagicType.Paralyze:
                                    case MagicType.Inhibition:
                                    case MagicType.Invisibility:
                                    case MagicType.Poison:
                                    case MagicType.Possession:
                                    case MagicType.Protect:
                                    case MagicType.Resurrection:
                                    case MagicType.Scan:
                                    case MagicType.SPDownSingle:
                                    case MagicType.SPUpSingle:
                                    case MagicType.Summon:
                                    case MagicType.Teleport:
                                    case MagicType.DamageSingle:
                                    case MagicType.DamageSingleSPDown:
                                    case MagicType.HPUpSingle:
                                    case MagicType.SPDownArea:
                                    case MagicType.SPUpArea:
                                    case MagicType.DamageAreaNoSingle:
                                    case MagicType.DamageAreaNoSingleSPDown:
                                    case MagicType.Confuse:
                                        {
                                            int width = (((spell.RangeX - 1) * 2) + 1) * 32;
                                            int height = (((spell.RangeY - 1) * 2) + 1) * 32;
                                            Rectangle rangeTotalOverlay = new Rectangle(((destinationX * 32) - width / 2) + 16, ((destinationY * 32) - height / 2) + 16, width, height);
                                            spriteBatch.Draw(Cache.BackgroundTexture, rangeTotalOverlay, rangeOverlayColour); // main overlay
                                            spriteBatch.Draw(Cache.BackgroundTexture, new Rectangle(rangeTotalOverlay.Left, rangeTotalOverlay.Top + 2, 2, rangeTotalOverlay.Height - 4), rangeOverlayColour); // Left
                                            spriteBatch.Draw(Cache.BackgroundTexture, new Rectangle(rangeTotalOverlay.Right - 2, rangeTotalOverlay.Top + 2, 2, rangeTotalOverlay.Height - 4), rangeOverlayColour); // Right
                                            spriteBatch.Draw(Cache.BackgroundTexture, new Rectangle(rangeTotalOverlay.Left, rangeTotalOverlay.Top, rangeTotalOverlay.Width, 2), rangeOverlayColour); // Top
                                            spriteBatch.Draw(Cache.BackgroundTexture, new Rectangle(rangeTotalOverlay.Left, rangeTotalOverlay.Bottom - 2, rangeTotalOverlay.Width, 2), rangeOverlayColour); // Bottom

                                            if (overlayColorMode)
                                            {
                                                colorMultiplier = 0.8f;
                                                for (int y = destinationY - spell.RangeY + 1; y < destinationY + spell.RangeY - 1; y++)
                                                    for (int x = destinationX - spell.RangeX + 1; x < destinationX + spell.RangeX - 1; x++)
                                                    {
                                                        if (player.Map[y + Cache.MapView.PivotY + Globals.OffScreenCells][x + Cache.MapView.PivotX + Globals.OffScreenCells].IsOccupied)
                                                        {
                                                            owner = player.Map[y + Cache.MapView.PivotY + Globals.OffScreenCells][x + Cache.MapView.PivotX + Globals.OffScreenCells].Owner;
                                                            rangeOverlay = new Rectangle(x * 32, y * 32, 32, 32);
                                                            rangeOverlay.Offset(owner.OffsetX, owner.OffsetY);
                                                            color = (owner.Side == OwnerSide.Aresden) ? GameColor.Global : (owner.Side == OwnerSide.Elvine) ? GameColor.Town : (player.Side == OwnerSide.Aresden) ? GameColor.Global : (player.Side == OwnerSide.Elvine) ? GameColor.Town : GameColor.GameMaster;
                                                            rangeOverlayColour = Cache.Colors[color] * colorMultiplier;
                                                            spriteBatch.Draw(Cache.BackgroundTexture, new Rectangle(rangeOverlay.Left, rangeOverlay.Top + 2, 2, rangeOverlay.Height - 4), rangeOverlayColour); // Left
                                                            spriteBatch.Draw(Cache.BackgroundTexture, new Rectangle(rangeOverlay.Right - 2, rangeOverlay.Top + 2, 2, rangeOverlay.Height - 4), rangeOverlayColour); // Right
                                                            spriteBatch.Draw(Cache.BackgroundTexture, new Rectangle(rangeOverlay.Left, rangeOverlay.Top, rangeOverlay.Width, 2), rangeOverlayColour); // Top
                                                            spriteBatch.Draw(Cache.BackgroundTexture, new Rectangle(rangeOverlay.Left, rangeOverlay.Bottom - 2, rangeOverlay.Width, 2), rangeOverlayColour); // Bottom                                            
                                                            //spriteBatch.DrawItemPopup(Cache.BackgroundTexture, rangeOverlay, rangeOverlayColour); //Object inside range of overlay
                                                        }
                                                    }
                                            }
                                            break;
                                        }
                                    case MagicType.DamageLine:
                                    case MagicType.DamageLineSPDown:
                                    case MagicType.IceLine:
                                        {
                                            // Hit 1: "line of site". each "point" in the LOS should have a cross-shaped hit, except the last point (next to caster) 
                                            // note that like AREA spells, the center point is sent to CharacterV1.MagicAttack(x, y... to ensure players are flown away from the center point
                                            linehitLocations = Utility.GetMagicLineLocations(Display.PlayerCenterX, Display.PlayerCenterY, destinationX, destinationY);
                                            hitLocations = new List<int[]>();

                                            foreach (int[] hitLocation in linehitLocations)
                                            {
                                                int x = (hitLocation[0]);
                                                int y = (hitLocation[1]);
                                                int[] target = { x, y };
                                                int[] target1 = { x + 1, y };
                                                int[] target2 = { x, y + 1 };
                                                int[] target3 = { x - 1, y };
                                                int[] target4 = { x, y - 1 };
                                                hitLocations.Add(target);
                                                hitLocations.Add(target1);
                                                hitLocations.Add(target2);
                                                hitLocations.Add(target3);
                                                hitLocations.Add(target4);
                                            }

                                            // Hit 2: Destination should hit like damagearea  
                                            for (int y = destinationY - spell.RangeY; y <= destinationY + spell.RangeY; y++)
                                                for (int x = destinationX - spell.RangeX; x <= destinationX + spell.RangeX; x++)
                                                {
                                                    int[] target = { x, y };
                                                    hitLocations.Add(target);
                                                }

                                            //Hit 3: center
                                            center = new int[] { destinationX, destinationY };
                                            hitLocations.Add(center);

                                            foreach (int[] hitLocation in hitLocations)
                                            {
                                                int x = (hitLocation[0]);
                                                int y = (hitLocation[1]);

                                                color = (player.Side == OwnerSide.Aresden) ? GameColor.Global : (player.Side == OwnerSide.Elvine) ? GameColor.Town : GameColor.GameMaster;
                                                colorMultiplier = 0.1f;
                                                rangeOverlayColour = Cache.Colors[color] * colorMultiplier;
                                                rangeOverlay = new Rectangle(x * 32, y * 32, 32, 32);
                                                spriteBatch.Draw(Cache.BackgroundTexture, rangeOverlay, rangeOverlayColour);

                                                if (overlayColorMode)
                                                {
                                                    if (player.Map[y + Cache.MapView.PivotY + Globals.OffScreenCells][x + Cache.MapView.PivotX + Globals.OffScreenCells].IsOccupied) //Map out of range 
                                                    {
                                                        colorMultiplier = 0.8f;
                                                        owner = player.Map[y + Cache.MapView.PivotY + Globals.OffScreenCells][x + Cache.MapView.PivotX + Globals.OffScreenCells].Owner;
                                                        rangeOverlay.Offset(owner.OffsetX, owner.OffsetY);
                                                        color = (owner.Side == OwnerSide.Aresden) ? GameColor.Global : (owner.Side == OwnerSide.Elvine) ? GameColor.Town : (player.Side == OwnerSide.Aresden) ? GameColor.Global : (player.Side == OwnerSide.Elvine) ? GameColor.Town : GameColor.GameMaster;
                                                        rangeOverlayColour = Cache.Colors[color] * colorMultiplier;
                                                        spriteBatch.Draw(Cache.BackgroundTexture, new Rectangle(rangeOverlay.Left, rangeOverlay.Top + 2, 2, rangeOverlay.Height - 4), rangeOverlayColour); // Left
                                                        spriteBatch.Draw(Cache.BackgroundTexture, new Rectangle(rangeOverlay.Right - 2, rangeOverlay.Top + 2, 2, rangeOverlay.Height - 4), rangeOverlayColour); // Right
                                                        spriteBatch.Draw(Cache.BackgroundTexture, new Rectangle(rangeOverlay.Left, rangeOverlay.Top, rangeOverlay.Width, 2), rangeOverlayColour); // Top
                                                        spriteBatch.Draw(Cache.BackgroundTexture, new Rectangle(rangeOverlay.Left, rangeOverlay.Bottom - 2, rangeOverlay.Width, 2), rangeOverlayColour); // Bottom                                            
                                                        //spriteBatch.DrawItemPopup(Cache.BackgroundTexture, rangeOverlay, rangeOverlayColour); //Object inside range of overlay
                                                    }
                                                }
                                            }
                                            break;
                                        }
                                    case MagicType.ArmourBreak:
                                    case MagicType.Ice:
                                    case MagicType.DamageArea:
                                    case MagicType.Tremor:
                                    case MagicType.ExploitCorpse:
                                    case MagicType.Kinesis:
                                        {

                                            int width = (((spell.RangeX - 1) * 2) + 1) * 32;
                                            int height = (((spell.RangeY - 1) * 2) + 1) * 32;
                                            Rectangle rangeTotalOverlay = new Rectangle(((destinationX * 32) - width / 2) + 16, ((destinationY * 32) - height / 2) + 16, width, height);
                                            spriteBatch.Draw(Cache.BackgroundTexture, rangeTotalOverlay, rangeOverlayColour); // main overlay
                                            spriteBatch.Draw(Cache.BackgroundTexture, new Rectangle(rangeTotalOverlay.Left, rangeTotalOverlay.Top + 2, 2, rangeTotalOverlay.Height - 4), rangeOverlayColour); // Left
                                            spriteBatch.Draw(Cache.BackgroundTexture, new Rectangle(rangeTotalOverlay.Right - 2, rangeTotalOverlay.Top + 2, 2, rangeTotalOverlay.Height - 4), rangeOverlayColour); // Right
                                            spriteBatch.Draw(Cache.BackgroundTexture, new Rectangle(rangeTotalOverlay.Left, rangeTotalOverlay.Top, rangeTotalOverlay.Width, 2), rangeOverlayColour); // Top
                                            spriteBatch.Draw(Cache.BackgroundTexture, new Rectangle(rangeTotalOverlay.Left, rangeTotalOverlay.Bottom - 2, rangeTotalOverlay.Width, 2), rangeOverlayColour); // Bottom
                                            rangeOverlayColour = rangeOverlayColour * 1.2f;
                                            spriteBatch.Draw(Cache.BackgroundTexture, new Rectangle(destinationX * 32, destinationY * 32, 32, 32), rangeOverlayColour);

                                            if (overlayColorMode)
                                            {
                                                colorMultiplier = 0.8f;
                                                for (int y = destinationY - spell.RangeY + 1; y < destinationY + spell.RangeY - 1; y++)
                                                    for (int x = destinationX - spell.RangeX + 1; x < destinationX + spell.RangeX - 1; x++)
                                                        switch (spell.Type)
                                                        {
                                                            default:
                                                                if (player.Map[y + Cache.MapView.PivotY + Globals.OffScreenCells][x + Cache.MapView.PivotX + Globals.OffScreenCells].IsOccupied)
                                                                {
                                                                    owner = player.Map[y + Cache.MapView.PivotY + Globals.OffScreenCells][x + Cache.MapView.PivotX + Globals.OffScreenCells].Owner;
                                                                    rangeOverlay = new Rectangle(x * 32, y * 32, 32, 32);
                                                                    rangeOverlay.Offset(owner.OffsetX, owner.OffsetY);
                                                                    color = (owner.Side == OwnerSide.Aresden) ? GameColor.Global : (owner.Side == OwnerSide.Elvine) ? GameColor.Town : (player.Side == OwnerSide.Aresden) ? GameColor.Global : (player.Side == OwnerSide.Elvine) ? GameColor.Town : GameColor.GameMaster;
                                                                    rangeOverlayColour = Cache.Colors[color] * colorMultiplier;
                                                                    spriteBatch.Draw(Cache.BackgroundTexture, new Rectangle(rangeOverlay.Left, rangeOverlay.Top + 2, 2, rangeOverlay.Height - 4), rangeOverlayColour); // Left
                                                                    spriteBatch.Draw(Cache.BackgroundTexture, new Rectangle(rangeOverlay.Right - 2, rangeOverlay.Top + 2, 2, rangeOverlay.Height - 4), rangeOverlayColour); // Right
                                                                    spriteBatch.Draw(Cache.BackgroundTexture, new Rectangle(rangeOverlay.Left, rangeOverlay.Top, rangeOverlay.Width, 2), rangeOverlayColour); // Top
                                                                    spriteBatch.Draw(Cache.BackgroundTexture, new Rectangle(rangeOverlay.Left, rangeOverlay.Bottom - 2, rangeOverlay.Width, 2), rangeOverlayColour); // Bottom                                            
                                                                    //spriteBatch.DrawItemPopup(Cache.BackgroundTexture, rangeOverlay, rangeOverlayColour); //Object inside range of overlay
                                                                }
                                                                break;
                                                            case MagicType.ExploitCorpse: // targets DeadOwner
                                                                if (player.Map[y + Cache.MapView.PivotY + Globals.OffScreenCells][x + Cache.MapView.PivotX + Globals.OffScreenCells].DeadOwner != null)
                                                                {
                                                                    owner = player.Map[y + Cache.MapView.PivotY + Globals.OffScreenCells][x + Cache.MapView.PivotX + Globals.OffScreenCells].DeadOwner;
                                                                    rangeOverlay = new Rectangle(x * 32, y * 32, 32, 32);
                                                                    rangeOverlay.Offset(owner.OffsetX, owner.OffsetY);
                                                                    color = (owner.Side == OwnerSide.Aresden) ? GameColor.Global : (owner.Side == OwnerSide.Elvine) ? GameColor.Town : (player.Side == OwnerSide.Aresden) ? GameColor.Global : (player.Side == OwnerSide.Elvine) ? GameColor.Town : GameColor.GameMaster;
                                                                    rangeOverlayColour = Cache.Colors[color] * colorMultiplier;
                                                                    spriteBatch.Draw(Cache.BackgroundTexture, new Rectangle(rangeOverlay.Left, rangeOverlay.Top + 2, 2, rangeOverlay.Height - 4), rangeOverlayColour); // Left
                                                                    spriteBatch.Draw(Cache.BackgroundTexture, new Rectangle(rangeOverlay.Right - 2, rangeOverlay.Top + 2, 2, rangeOverlay.Height - 4), rangeOverlayColour); // Right
                                                                    spriteBatch.Draw(Cache.BackgroundTexture, new Rectangle(rangeOverlay.Left, rangeOverlay.Top, rangeOverlay.Width, 2), rangeOverlayColour); // Top
                                                                    spriteBatch.Draw(Cache.BackgroundTexture, new Rectangle(rangeOverlay.Left, rangeOverlay.Bottom - 2, rangeOverlay.Width, 2), rangeOverlayColour); // Bottom                                            
                                                                    //spriteBatch.DrawItemPopup(Cache.BackgroundTexture, rangeOverlay, rangeOverlayColour); //Object inside range of overlay
                                                                }
                                                                break;
                                                        }
                                            }
                                        }
                                        break;
                                    case MagicType.Spiral:
                                    case MagicType.GhostSpiral:
                                        {
                                            // Hit 1: Spiral hit locations
                                            spiralHitLocations = Utility.GetMagicSpiralLocations(destinationX, destinationY);

                                            // Hit 2: Same as kinesis/spirit spells
                                            int width = (((spell.RangeX - 1) * 2) + 1) * 32;
                                            int height = (((spell.RangeY - 1) * 2) + 1) * 32;
                                            Rectangle rangeTotalOverlay = new Rectangle(((destinationX * 32) - width / 2) + 16, ((destinationY * 32) - height / 2) + 16, width, height);
                                            spriteBatch.Draw(Cache.BackgroundTexture, rangeTotalOverlay, rangeOverlayColour); // main overlay
                                            spriteBatch.Draw(Cache.BackgroundTexture, new Rectangle(rangeTotalOverlay.Left, rangeTotalOverlay.Top + 2, 2, rangeTotalOverlay.Height - 4), rangeOverlayColour); // Left
                                            spriteBatch.Draw(Cache.BackgroundTexture, new Rectangle(rangeTotalOverlay.Right - 2, rangeTotalOverlay.Top + 2, 2, rangeTotalOverlay.Height - 4), rangeOverlayColour); // Right
                                            spriteBatch.Draw(Cache.BackgroundTexture, new Rectangle(rangeTotalOverlay.Left, rangeTotalOverlay.Top, rangeTotalOverlay.Width, 2), rangeOverlayColour); // Top
                                            spriteBatch.Draw(Cache.BackgroundTexture, new Rectangle(rangeTotalOverlay.Left, rangeTotalOverlay.Bottom - 2, rangeTotalOverlay.Width, 2), rangeOverlayColour); // Bottom
                                            rangeOverlayColour = rangeOverlayColour * 1.2f;
                                            spriteBatch.Draw(Cache.BackgroundTexture, new Rectangle(destinationX * 32, destinationY * 32, 32, 32), rangeOverlayColour);


                                            foreach (int[] hitLocation in spiralHitLocations)
                                            {
                                                int x = (hitLocation[0]);
                                                int y = (hitLocation[1]);

                                                color = (player.Side == OwnerSide.Aresden) ? GameColor.Global : (player.Side == OwnerSide.Elvine) ? GameColor.Town : GameColor.GameMaster;
                                                colorMultiplier = 0.2f;
                                                rangeOverlayColour = Cache.Colors[color] * colorMultiplier;
                                                rangeOverlay = new Rectangle(x * 32, y * 32, 32, 32);
                                                spriteBatch.Draw(Cache.BackgroundTexture, rangeOverlay, rangeOverlayColour);

                                                if (overlayColorMode)
                                                {
                                                    if (player.Map[y + Cache.MapView.PivotY + Globals.OffScreenCells][x + Cache.MapView.PivotX + Globals.OffScreenCells].IsOccupied)
                                                    {
                                                        colorMultiplier = 0.8f;
                                                        owner = player.Map[y + Cache.MapView.PivotY + Globals.OffScreenCells][x + Cache.MapView.PivotX + Globals.OffScreenCells].Owner;
                                                        rangeOverlay.Offset(owner.OffsetX, owner.OffsetY);
                                                        color = (owner.Side == OwnerSide.Aresden) ? GameColor.Global : (owner.Side == OwnerSide.Elvine) ? GameColor.Town : (player.Side == OwnerSide.Aresden) ? GameColor.Global : (player.Side == OwnerSide.Elvine) ? GameColor.Town : GameColor.GameMaster;
                                                        rangeOverlayColour = Cache.Colors[color] * colorMultiplier;
                                                        spriteBatch.Draw(Cache.BackgroundTexture, new Rectangle(rangeOverlay.Left, rangeOverlay.Top + 2, 2, rangeOverlay.Height - 4), rangeOverlayColour); // Left
                                                        spriteBatch.Draw(Cache.BackgroundTexture, new Rectangle(rangeOverlay.Right - 2, rangeOverlay.Top + 2, 2, rangeOverlay.Height - 4), rangeOverlayColour); // Right
                                                        spriteBatch.Draw(Cache.BackgroundTexture, new Rectangle(rangeOverlay.Left, rangeOverlay.Top, rangeOverlay.Width, 2), rangeOverlayColour); // Top
                                                        spriteBatch.Draw(Cache.BackgroundTexture, new Rectangle(rangeOverlay.Left, rangeOverlay.Bottom - 2, rangeOverlay.Width, 2), rangeOverlayColour); // Bottom                                            
                                                        //spriteBatch.DrawItemPopup(Cache.BackgroundTexture, rangeOverlay, rangeOverlayColour); //Object inside range of overlay
                                                    }
                                                }
                                            }
                                        }
                                        break;
                                    case MagicType.CreateDynamic:
                                        switch ((DynamicObjectType)spell.Effect7)
                                        {
                                            case DynamicObjectType.PoisonCloudBegin:
                                            case DynamicObjectType.Fire:
                                            case DynamicObjectType.SpikeField:
                                            default:
                                                switch ((DynamicObjectShape)spell.Effect8)
                                                {
                                                    case DynamicObjectShape.Wall:
                                                        {
                                                            int rangeX, rangeY;
                                                            switch (Utility.GetNextDirection(Display.PlayerCenterX, Display.PlayerCenterY, destinationX, destinationY))
                                                            {
                                                                default:
                                                                case MotionDirection.North: rangeX = 1; rangeY = 0; break;
                                                                case MotionDirection.NorthEast: rangeX = 1; rangeY = 1; break;
                                                                case MotionDirection.East: rangeX = 0; rangeY = 1; break;
                                                                case MotionDirection.SouthEast: rangeX = -1; rangeY = 1; break;
                                                                case MotionDirection.South: rangeX = 1; rangeY = 0; break;
                                                                case MotionDirection.SouthWest: rangeX = -1; rangeY = -1; break;
                                                                case MotionDirection.West: rangeX = 0; rangeY = -1; break;
                                                                case MotionDirection.NorthWest: rangeX = 1; rangeY = -1; break;
                                                            }

                                                            //caster.CurrentMap.CreateDynamicObject((DynamicObjectType)spell.Effect7, destinationX, destinationY, spell.LastTime);
                                                            Rectangle rangeTotalOverlay = new Rectangle((destinationX) * 32, (destinationY) * 32, 32, 32);
                                                            spriteBatch.Draw(Cache.BackgroundTexture, rangeTotalOverlay, rangeOverlayColour); // main overlay

                                                            for (int i = 1; i < spell.RangeX; i++)
                                                            {
                                                                rangeTotalOverlay = new Rectangle((destinationX + (rangeX * i)) * 32, (destinationY + (rangeY * i)) * 32, 32, 32);
                                                                //caster.CurrentMap.CreateDynamicObject((DynamicObjectType)spell.Effect7, destinationX + (rangeX * i), destinationY + (rangeY * i), spell.LastTime);
                                                                spriteBatch.Draw(Cache.BackgroundTexture, rangeTotalOverlay, rangeOverlayColour); // main overlay

                                                                rangeTotalOverlay = new Rectangle((destinationX - (rangeX * i)) * 32, (destinationY - (rangeY * i)) * 32, 32, 32);
                                                                //caster.CurrentMap.CreateDynamicObject((DynamicObjectType)spell.Effect7, destinationX - (rangeX * i), destinationY - (rangeY * i), spell.LastTime);
                                                                spriteBatch.Draw(Cache.BackgroundTexture, rangeTotalOverlay, rangeOverlayColour); // main overlay
                                                            }
                                                        }
                                                        break;
                                                    case DynamicObjectShape.Field:
                                                        {
                                                            int width = (((spell.RangeX - 1) * 2) + 1) * 32;
                                                            int height = (((spell.RangeY - 1) * 2) + 1) * 32;
                                                            Rectangle rangeTotalOverlay = new Rectangle(((destinationX * 32) - width / 2) + 16, ((destinationY * 32) - height / 2) + 16, width, height);
                                                            spriteBatch.Draw(Cache.BackgroundTexture, rangeTotalOverlay, rangeOverlayColour); // main overlay
                                                            spriteBatch.Draw(Cache.BackgroundTexture, new Rectangle(rangeTotalOverlay.Left, rangeTotalOverlay.Top + 2, 2, rangeTotalOverlay.Height - 4), rangeOverlayColour); // Left
                                                            spriteBatch.Draw(Cache.BackgroundTexture, new Rectangle(rangeTotalOverlay.Right - 2, rangeTotalOverlay.Top + 2, 2, rangeTotalOverlay.Height - 4), rangeOverlayColour); // Right
                                                            spriteBatch.Draw(Cache.BackgroundTexture, new Rectangle(rangeTotalOverlay.Left, rangeTotalOverlay.Top, rangeTotalOverlay.Width, 2), rangeOverlayColour); // Top
                                                            spriteBatch.Draw(Cache.BackgroundTexture, new Rectangle(rangeTotalOverlay.Left, rangeTotalOverlay.Bottom - 2, rangeTotalOverlay.Width, 2), rangeOverlayColour); // Bottom

                                                            if (overlayColorMode)
                                                            {
                                                                colorMultiplier = 0.8f;
                                                                for (int y = destinationY - spell.Effect9; y <= destinationY + spell.Effect9; y++)
                                                                    for (int x = destinationX - spell.Effect9; x <= destinationX + spell.Effect9; x++)
                                                                    {
                                                                        if (player.Map[y + Cache.MapView.PivotY + Globals.OffScreenCells][x + Cache.MapView.PivotX + Globals.OffScreenCells].IsOccupied)
                                                                        {
                                                                            owner = player.Map[y + Cache.MapView.PivotY + Globals.OffScreenCells][x + Cache.MapView.PivotX + Globals.OffScreenCells].Owner;
                                                                            rangeOverlay = new Rectangle(x * 32, y * 32, 32, 32);
                                                                            rangeOverlay.Offset(owner.OffsetX, owner.OffsetY);
                                                                            color = (owner.Side == OwnerSide.Aresden) ? GameColor.Global : (owner.Side == OwnerSide.Elvine) ? GameColor.Town : (player.Side == OwnerSide.Aresden) ? GameColor.Global : (player.Side == OwnerSide.Elvine) ? GameColor.Town : GameColor.GameMaster;
                                                                            rangeOverlayColour = Cache.Colors[color] * colorMultiplier;
                                                                            spriteBatch.Draw(Cache.BackgroundTexture, new Rectangle(rangeOverlay.Left, rangeOverlay.Top + 2, 2, rangeOverlay.Height - 4), rangeOverlayColour); // Left
                                                                            spriteBatch.Draw(Cache.BackgroundTexture, new Rectangle(rangeOverlay.Right - 2, rangeOverlay.Top + 2, 2, rangeOverlay.Height - 4), rangeOverlayColour); // Right
                                                                            spriteBatch.Draw(Cache.BackgroundTexture, new Rectangle(rangeOverlay.Left, rangeOverlay.Top, rangeOverlay.Width, 2), rangeOverlayColour); // Top
                                                                            spriteBatch.Draw(Cache.BackgroundTexture, new Rectangle(rangeOverlay.Left, rangeOverlay.Bottom - 2, rangeOverlay.Width, 2), rangeOverlayColour); // Bottom                                            
                                                                            //spriteBatch.DrawItemPopup(Cache.BackgroundTexture, rangeOverlay, rangeOverlayColour); //Object inside range of overlay
                                                                        }
                                                                    }
                                                            }
                                                        }
                                                        break;
                                                }
                                                break;
                                            case DynamicObjectType.IceStorm:
                                                {
                                                    int width = ((spell.RangeX * 2) + 1) * 32;
                                                    int height = ((spell.RangeY * 2) + 1) * 32;
                                                    Rectangle rangeTotalOverlay = new Rectangle(((destinationX * 32) - width / 2) + 16, ((destinationY * 32) - height / 2) + 16, width, height);
                                                    spriteBatch.Draw(Cache.BackgroundTexture, rangeTotalOverlay, rangeOverlayColour); // main overlay
                                                    spriteBatch.Draw(Cache.BackgroundTexture, new Rectangle(rangeTotalOverlay.Left, rangeTotalOverlay.Top + 2, 2, rangeTotalOverlay.Height - 4), rangeOverlayColour); // Left
                                                    spriteBatch.Draw(Cache.BackgroundTexture, new Rectangle(rangeTotalOverlay.Right - 2, rangeTotalOverlay.Top + 2, 2, rangeTotalOverlay.Height - 4), rangeOverlayColour); // Right
                                                    spriteBatch.Draw(Cache.BackgroundTexture, new Rectangle(rangeTotalOverlay.Left, rangeTotalOverlay.Top, rangeTotalOverlay.Width, 2), rangeOverlayColour); // Top
                                                    spriteBatch.Draw(Cache.BackgroundTexture, new Rectangle(rangeTotalOverlay.Left, rangeTotalOverlay.Bottom - 2, rangeTotalOverlay.Width, 2), rangeOverlayColour); // Bottom

                                                    if (overlayColorMode)
                                                    {
                                                        colorMultiplier = 0.8f;
                                                        for (int y = destinationY - spell.Effect9; y <= destinationY + spell.Effect9; y++)
                                                            for (int x = destinationX - spell.Effect9; x <= destinationX + spell.Effect9; x++)
                                                            {
                                                                if (player.Map[y + Cache.MapView.PivotY + Globals.OffScreenCells][x + Cache.MapView.PivotX + Globals.OffScreenCells].IsOccupied)
                                                                {
                                                                    owner = player.Map[y + Cache.MapView.PivotY + Globals.OffScreenCells][x + Cache.MapView.PivotX + Globals.OffScreenCells].Owner;
                                                                    rangeOverlay = new Rectangle(x * 32, y * 32, 32, 32);
                                                                    rangeOverlay.Offset(owner.OffsetX, owner.OffsetY);
                                                                    color = (owner.Side == OwnerSide.Aresden) ? GameColor.Global : (owner.Side == OwnerSide.Elvine) ? GameColor.Town : (player.Side == OwnerSide.Aresden) ? GameColor.Global : (player.Side == OwnerSide.Elvine) ? GameColor.Town : GameColor.GameMaster;
                                                                    rangeOverlayColour = Cache.Colors[color] * colorMultiplier;
                                                                    spriteBatch.Draw(Cache.BackgroundTexture, new Rectangle(rangeOverlay.Left, rangeOverlay.Top + 2, 2, rangeOverlay.Height - 4), rangeOverlayColour); // Left
                                                                    spriteBatch.Draw(Cache.BackgroundTexture, new Rectangle(rangeOverlay.Right - 2, rangeOverlay.Top + 2, 2, rangeOverlay.Height - 4), rangeOverlayColour); // Right
                                                                    spriteBatch.Draw(Cache.BackgroundTexture, new Rectangle(rangeOverlay.Left, rangeOverlay.Top, rangeOverlay.Width, 2), rangeOverlayColour); // Top
                                                                    spriteBatch.Draw(Cache.BackgroundTexture, new Rectangle(rangeOverlay.Left, rangeOverlay.Bottom - 2, rangeOverlay.Width, 2), rangeOverlayColour); // Bottom                                            
                                                                    //spriteBatch.DrawItemPopup(Cache.BackgroundTexture, rangeOverlay, rangeOverlayColour); //Object inside range of overlay
                                                                }
                                                            }
                                                    }
                                                }
                                                break;
                                        }
                                        break;
                                }
                            }
                        }
                    }
                    if (overlayWeaponMode)
                    {
                        if (!player.IsCasting && !player.SpellReady)
                        {
                            int range = Utility.GetRange((player.Weapon != null ? player.Weapon.RelatedSkill : SkillType.Hand), player.CriticalMode, (player.Weapon != null ? player.Weapon.HasExtendedRange : false));
                            int width = ((range * 2) + 1) * 32;
                            Rectangle rangeTotalOverlay = new Rectangle((Display.PlayerCenterX - range) * 32, (Display.PlayerCenterY - range) * 32, width, width);
                            spriteBatch.Draw(Cache.BackgroundTexture, rangeTotalOverlay, rangeOverlayColour); // main overlay
                            spriteBatch.Draw(Cache.BackgroundTexture, new Rectangle(rangeTotalOverlay.Left, rangeTotalOverlay.Top + 2, 2, rangeTotalOverlay.Height - 4), rangeOverlayColour); // Left
                            spriteBatch.Draw(Cache.BackgroundTexture, new Rectangle(rangeTotalOverlay.Right - 2, rangeTotalOverlay.Top + 2, 2, rangeTotalOverlay.Height - 4), rangeOverlayColour); // Right
                            spriteBatch.Draw(Cache.BackgroundTexture, new Rectangle(rangeTotalOverlay.Left, rangeTotalOverlay.Top, rangeTotalOverlay.Width, 2), rangeOverlayColour); // Top
                            spriteBatch.Draw(Cache.BackgroundTexture, new Rectangle(rangeTotalOverlay.Left, rangeTotalOverlay.Bottom - 2, rangeTotalOverlay.Width, 2), rangeOverlayColour); // Bottom

                            //spriteBatch.DrawItemPopup(Cache.BackgroundTexture, new Rectangle((Display.PlayerCenterX - range + 1) * 32, (GameInterface.PlayerCenterY - range + 2) * 32, 2, lineLength), rangeOverlayLineColour); // Left
                            //spriteBatch.DrawItemPopup(Cache.BackgroundTexture, new Rectangle((Display.PlayerCenterX + range + 2) * 32, (GameInterface.PlayerCenterY - range + 2) * 32, 2, lineLength), rangeOverlayLineColour); // Right
                            //spriteBatch.DrawItemPopup(Cache.BackgroundTexture, new Rectangle((Display.PlayerCenterX - range + 2) * 32, (GameInterface.PlayerCenterY - range + 1) * 32, lineLength, 2), rangeOverlayLineColour); // Top
                            //spriteBatch.DrawItemPopup(Cache.BackgroundTexture, new Rectangle((Display.PlayerCenterX - range + 2) * 32, (GameInterface.PlayerCenterY + range + 2) * 32, lineLength, 2), rangeOverlayLineColour); // Bottom

                            if (overlayColorMode)
                            {
                                colorMultiplier = 0.8f;
                                for (int y = Display.PlayerCenterY - range; y <= Display.PlayerCenterY + range; y++)
                                    for (int x = Display.PlayerCenterX - range; x <= Display.PlayerCenterX + range; x++)
                                    {
                                        //if (y == Display.PlayerCenterY - range || y == Display.PlayerCenterY + range) <--Work for fixing drawing bug in overlay
                                        //if ( x == Display.PlayerCenterX - range || x == Display.PlayerCenterX + range)

                                        if (player.Map[y + Cache.MapView.PivotY + Globals.OffScreenCells][x + Cache.MapView.PivotX + Globals.OffScreenCells].IsOccupied)
                                        {
                                            owner = player.Map[y + Cache.MapView.PivotY + Globals.OffScreenCells][x + Cache.MapView.PivotX + Globals.OffScreenCells].Owner;
                                            if (owner != player)
                                            {
                                                rangeOverlay = new Rectangle(x * 32, y * 32, 32, 32);
                                                rangeOverlay.Offset(-player.OffsetX, -player.OffsetY);
                                                rangeOverlay.Offset(owner.OffsetX, owner.OffsetY);
                                                color = (owner.Side == OwnerSide.Aresden) ? GameColor.Global : (owner.Side == OwnerSide.Elvine) ? GameColor.Town : (player.Side == OwnerSide.Aresden) ? GameColor.Global : (player.Side == OwnerSide.Elvine) ? GameColor.Town : GameColor.GameMaster;
                                                rangeOverlayColour = Cache.Colors[color] * colorMultiplier;
                                                spriteBatch.Draw(Cache.BackgroundTexture, new Rectangle(rangeOverlay.Left, rangeOverlay.Top + 2, 2, rangeOverlay.Height - 4), rangeOverlayColour); // Left
                                                spriteBatch.Draw(Cache.BackgroundTexture, new Rectangle(rangeOverlay.Right - 2, rangeOverlay.Top + 2, 2, rangeOverlay.Height - 4), rangeOverlayColour); // Right
                                                spriteBatch.Draw(Cache.BackgroundTexture, new Rectangle(rangeOverlay.Left, rangeOverlay.Top, rangeOverlay.Width, 2), rangeOverlayColour); // Top
                                                spriteBatch.Draw(Cache.BackgroundTexture, new Rectangle(rangeOverlay.Left, rangeOverlay.Bottom - 2, rangeOverlay.Width, 2), rangeOverlayColour); // Bottom                                            
                                                //spriteBatch.DrawItemPopup(Cache.BackgroundTexture, rangeOverlay, rangeOverlayColour); //Object inside range of overlay
                                            }
                                        }
                                    }
                            }
                        }
                    }

                    //Overlay for player movement 
                    if (overlayMovePath && (player.DestinationX != -1 || player.DestinationY != -1))
                    {
                        List<int[]> path = GetMovePath(player.X, player.Y, player.DestinationX, player.DestinationY);

                        //Destination tile
                        int finalX = (player.DestinationX - Cache.MapView.PivotX) - Globals.OffScreenCells;
                        int finalY = (player.DestinationY - Cache.MapView.PivotY) - Globals.OffScreenCells;
                        rangeOverlay = new Rectangle(finalX * 32, finalY * 32, 32, 32);
                        rangeOverlay.Offset(-player.OffsetX, -player.OffsetY);
                        spriteBatch.Draw(Cache.BackgroundTexture, rangeOverlay, Cache.Colors[GameColor.Guild] * 0.6f);

                        //Path
                        foreach (int[] location in path)
                        {
                            int x = (location[0] - Cache.MapView.PivotX) - Globals.OffScreenCells;
                            int y = (location[1] - Cache.MapView.PivotY) - Globals.OffScreenCells;

                            rangeOverlay = new Rectangle(x * 32, y * 32, 32, 32);
                            rangeOverlay.Offset(-player.OffsetX, -player.OffsetY);
                            spriteBatch.Draw(Cache.BackgroundTexture, rangeOverlay, Cache.Colors[GameColor.Guild] * 0.3f);
                        }
                    }

                }

                /* LAYER 2 - ITEMS */ //TODO place all ground items into one large 2048x2048 texture
                indexY = Cache.MapView.PivotY;
                for (int iy = -(Globals.CellHeight * Globals.OffScreenCells); iy < display.GameBoardHeight + (Globals.CellHeight * Globals.OffScreenCells); iy += Globals.CellHeight)
                {
                    indexX = Cache.MapView.PivotX;
                    for (int ix = -(Globals.CellWidth * Globals.OffScreenCells); ix < display.GameBoardWidth + (Globals.CellWidth * Globals.OffScreenCells); ix += Globals.CellWidth)
                    {
                        if (Cache.MapView.IsWithinBounds(indexX, indexY))
                        {
                            sprite = Cache.MapView.Map[indexY][indexX].ItemSprite;
                            spriteFrame = Cache.MapView.Map[indexY][indexX].ItemSpriteFrame;
                            spriteColour = Cache.MapView.Map[indexY][indexX].ItemSpriteColour;
                            itemColorType = Cache.MapView.Map[indexY][indexX].ItemColorType;

                            if (sprite != -1 && Cache.Equipment.ContainsKey((int)SpriteId.ItemGround + sprite) && Cache.Equipment[(int)SpriteId.ItemGround + sprite].Frames[spriteFrame] != null)
                            {
                                Vector2 pos = new Vector2(ix + Cache.Equipment[(int)SpriteId.ItemGround + sprite].Frames[spriteFrame].PivotX + 16 + -player.OffsetX, iy + Cache.Equipment[(int)SpriteId.ItemGround + sprite].Frames[spriteFrame].PivotY + 16 + -player.OffsetY);
                                spriteBatch.Draw(Cache.Equipment[(int)SpriteId.ItemGround + sprite].Texture, pos, Cache.Equipment[(int)SpriteId.ItemGround + sprite].Frames[spriteFrame].GetRectangle(), (itemColorType == GameColor.Custom) ? SpriteHelper.ColourFromArgb(spriteColour) : Cache.Colors[itemColorType]);

                                if (Utility.IsSelected((int)Display.Mouse.X, (int)Display.Mouse.Y, (int)pos.X, (int)pos.Y, (int)pos.X + Cache.Equipment[(int)SpriteId.ItemGround + sprite].Frames[spriteFrame].Width, (int)pos.Y + Cache.Equipment[(int)SpriteId.ItemGround + sprite].Frames[spriteFrame].Height, Cache.DefaultState.Display.GameBoardShiftX, Cache.DefaultState.Display.GameBoardShiftY))
                                    if (highlightedDialogBox == GameDialogBoxType.None && !player.IsCasting && !player.SpellReady)
                                        Display.Mouse.State = GameMouseState.Pickup;
                            }
                        }
                        indexX++;
                    }
                    indexY++;
                }

                // checks mouse/object collisions for selected object
                selectedObjectId = -1; selectedObjectX = 0; selectedObjectY = 0;

                ///* LAYER 3 - EFFECT LIGHTING BEHIND OBJECTS */ //TODO possibly add to same texuture
                foreach (IGameEffect e in effects)
                    e.DrawLights(spriteBatch, display, player.OffsetX, player.OffsetY, Cache.MapView.PivotX, Cache.MapView.PivotY);

                /* LAYER 4 - NPCS, PLAYERS, ITEMS etc */
                indexY = Cache.MapView.PivotY;
                for (int iy = -(Globals.CellHeight * Globals.OffScreenCells); iy < display.GameBoardHeight + (Globals.CellHeight * Globals.OffScreenCells); iy += Globals.CellHeight)
                {
                    indexX = Cache.MapView.PivotX;
                    for (int ix = -(Globals.CellWidth * Globals.OffScreenCells); ix < display.GameBoardWidth + (Globals.CellWidth * Globals.OffScreenCells); ix += Globals.CellWidth)
                    {
                        if (Cache.MapView.IsWithinBounds(indexX, indexY))
                        {
                            if (Cache.MapView.Map[indexY][indexX].DeadOwner != null)
                                if (Cache.MapView.Map[indexY][indexX].DeadOwner.Draw(spriteBatch, display, ix + 16, iy + 16, -player.OffsetX, -player.OffsetY))
                                {
                                    selectedObjectId = Cache.MapView.Map[indexY][indexX].DeadOwner.ObjectId;
                                    selectedObjectType = ObjectType.DeadOwner;
                                    selectedObjectX = ix + 16; selectedObjectY = iy + 16;
                                }
                            if (Cache.MapView.Map[indexY][indexX].Owner != null)
                            {
                                //if (Cache.MapView.Map[indexY][indexX].Owner.OwnerType == OwnerType.Player)
                                //{
                                //    ////Team color 
                                //    //if (Cache.GameSettings.TeamColorOn && (Cache.GameSettings.TownColorOn || Cache.GameSettings.GuildColorOn || Cache.GameSettings.PartyColorOn)) // Maybe exclude the player? 
                                //    //{
                                //    //    if (string.IsNullOrEmpty(Cache.MapView.Map[indexY][indexX].Owner.GuildName))
                                //    //        GetGuildName(Cache.MapView.Map[indexY][indexX].Owner.ObjectId);
                                //    //}
                                //}

                                if (Cache.MapView.Map[indexY][indexX].Owner.Draw(spriteBatch, display, ix + 16, iy + 16, -player.OffsetX, -player.OffsetY))
                                {
                                    selectedObjectId = Cache.MapView.Map[indexY][indexX].Owner.ObjectId;
                                    selectedObjectType = ObjectType.Owner;
                                    selectedObjectX = ix + 16; selectedObjectY = iy + 16;
                                }
                                // get x,y,width,height bounds of the character sprite
                                if (Cache.MapView.Map[indexY][indexX].Owner.ObjectId == player.ObjectId)
                                    playerBounds = new Rectangle(ix + 16 + Cache.HumanAnimations[player.Animation].Frames[player.AnimationFrame].PivotX, iy + 16 + Cache.HumanAnimations[player.Animation].Frames[player.AnimationFrame].PivotY,
                                                                    Cache.HumanAnimations[player.Animation].Frames[player.AnimationFrame].Width, Cache.HumanAnimations[player.Animation].Frames[player.AnimationFrame].Height);
                            }
                            sprite = Cache.MapView.Map[indexY][indexX].ObjectSprite;  //MAP OBJECTS DRAWN HERE + Dynamic objects 
                            spriteFrame = Cache.MapView.Map[indexY][indexX].ObjectSpriteFrame;
                            float fade = 1.0f;

                            // trees low detail
                            if (sprite >= 100 && sprite < 200)
                            {
                                if (Cache.GameSettings.DetailMode == GraphicsDetail.Low)
                                {
                                    if (sprite < 111) sprite = 104;
                                    else if (sprite < 123) sprite = 109;
                                    else if (sprite < 132) sprite = 123;
                                    else sprite = 132;
                                }
                                else
                                {
                                    if (playerBounds != Rectangle.Empty)  // fade trees when player behind them
                                        if (Cache.Tiles.ContainsKey(sprite) && sprite > 0 && Cache.Tiles[sprite].Frames.Count > spriteFrame)
                                        {
                                            Rectangle treeBounds = new Rectangle(ix + 16 + Cache.Tiles[sprite].Frames[spriteFrame].PivotX, iy + 16 + Cache.Tiles[sprite].Frames[spriteFrame].PivotY,
                                                                                   Cache.Tiles[sprite].Frames[spriteFrame].Width, Cache.Tiles[sprite].Frames[spriteFrame].Height);
                                            if (treeBounds.Contains(playerBounds)) fade = 0.5f;
                                        }
                                }
                            }

                            if (Cache.Tiles.ContainsKey(sprite) && sprite > 0 && Cache.Tiles[sprite].Frames.Count > spriteFrame)
                            {
                                Vector2 pos = new Vector2(ix + Cache.Tiles[sprite].Frames[spriteFrame].PivotX + -player.OffsetX, iy + Cache.Tiles[sprite].Frames[spriteFrame].PivotY + -player.OffsetY);
                                spriteBatch.Draw(Cache.Tiles[sprite].Texture, pos, Cache.Tiles[sprite].Frames[spriteFrame].GetRectangle(), Color.White * fade);
                            }

                            if (Cache.MapView.Map[indexY][indexX].DynamicObject != null) //TODO dynamic objects stay on screen after being finished and don't show if cast off screen
                                if (Cache.MapView.Map[indexY][indexX].DynamicObject.Draw(spriteBatch, ix + 16, iy + 16, -player.OffsetX, -player.OffsetY))
                                {
                                    selectedObjectId = Cache.MapView.Map[indexY][indexX].DynamicObject.ObjectId;
                                    selectedObjectType = ObjectType.DynamicObject;
                                    selectedObjectX = ix + 16; selectedObjectY = iy + 16;
                                }
                        }
                        indexX++;
                    }
                    indexY++;
                }
                ///* LAYER 5 - EFFECTS */
                foreach (IGameEffect e in effects)
                    e.Draw(spriteBatch, display, player.OffsetX, player.OffsetY, Cache.MapView.PivotX, Cache.MapView.PivotY);

                /* LAYER 6 - WEATHER EFFECTS */
                if (weather != null)
                    weather.Draw(spriteBatch, player.OffsetX, player.OffsetY);


                /* HIGHLIGHTED OBJECT */ //TODO fix location, shorten info shown
                if (selectedObjectId != -1)
                    switch (selectedObjectType)
                    {
                        case ObjectType.DynamicObject:
                            Display.Mouse.State = GameMouseState.Highlight;
                            break;
                        case ObjectType.DeadOwner:
                        case ObjectType.Owner:
                            int textLine = 0;
                            if (Cache.OwnerCache.ContainsKey((ushort)selectedObjectId))
                            {
                                IOwner owner = Cache.OwnerCache[(ushort)selectedObjectId];
                                drawX = selectedObjectX + ((owner != player) ? owner.OffsetX - player.OffsetX : 0); //todo add resolution offsets for shifted pixels
                                drawY = selectedObjectY + ((owner != player) ? owner.OffsetY - player.OffsetY : 0); //todo add resolution offsets for shifted pixels

                                if (owner.OwnerType == OwnerType.Player)
                                {
                                    Color textColor = Color.White; //GameMaster > Party > Guild > Friendly = Enemy = Neutral
                                    //if (owner.isGameMaster) textColor = Cache.ChatColours[ChatType.GameMaster]; //TODO add this
                                    if (player.HasParty && ((owner.ObjectId == player.ObjectId) || (player.Party.Members.Contains(owner.ObjectId))))
                                        textColor = Cache.Colors[GameColor.Party];
                                    else if (player.GuildName != "NONE" && owner.GuildName == player.GuildName) textColor = Cache.Colors[GameColor.Guild];
                                    else
                                    {
                                        switch (player.GetRelationship(owner.Side))
                                        {
                                            case OwnerRelationShip.Enemy: textColor = Color.Red; break;
                                            case OwnerRelationShip.Friendly: textColor = Color.Green; break;
                                            case OwnerRelationShip.Neutral:
                                            default: break;
                                        }
                                    }


                                    //TODO show berserked/fronze/hasted another way?               
                                    string name = string.Format("{0}{1}{2}{3}", owner.Name, (owner.IsBerserked) ? " Berserked" : "", (owner.IsFrozen) ? " Frozen" : "", (owner.IsHaste) ? " Hasted" : "");

                                    SpriteHelper.DrawTextWithShadow(spriteBatch, name, FontType.DamageSmallSize11, new Vector2((int)(drawX + 1), (int)(drawY + 1)), textColor);

                                    if (string.IsNullOrEmpty(owner.GuildName))
                                        GetGuildName(owner.ObjectId);
                                    else if (!owner.GuildName.Equals("NONE"))
                                        SpriteHelper.DrawTextWithShadow(spriteBatch, string.Format("{0} {1}", owner.GuildName, (owner.GuildRank == 0) ? "Guildmaster" : "Guildsman"), FontType.GeneralSize10, new Vector2((int)(drawX), (int)(drawY + 4 + (++textLine * 14))), Color.DarkGray);
                                }
                                else if (owner.OwnerType == OwnerType.Npc)
                                {

                                    SpriteHelper.DrawTextWithShadow(spriteBatch, NpcHelper.GetNpcFriendlyName(Cache.NpcConfiguration, owner.Id, owner.Status), FontType.DamageSmallSize11, new Vector2((int)(drawX), (int)(drawY + (++textLine * 14))), Color.White);

                                    foreach (NpcPerk perk in owner.Perks)
                                        if (perk != NpcPerk.None)
                                            SpriteHelper.DrawTextWithShadow(spriteBatch, NpcHelper.GetNpcPerkName(perk), FontType.GeneralSize10, new Vector2((int)(drawX), (int)(drawY + (++textLine * 14))), Color.Yellow);
                                }

                                switch (owner.GetRelationship(player.Side))
                                {
                                    case OwnerRelationShip.Enemy:
                                        switch (owner.Side)
                                        {
                                            case OwnerSide.Aresden:
                                            case OwnerSide.Elvine:
                                                SpriteHelper.DrawTextWithShadow(spriteBatch, string.Format("{0} {1}", owner.Side.ToString(), owner.SideStatus.ToString()), FontType.GeneralSize10, new Vector2((int)(drawX), (int)(drawY + 4 + (++textLine * 14))), Color.Red);
                                                break;
                                            default:
                                                SpriteHelper.DrawTextWithShadow(spriteBatch, "(Enemy)", FontType.GeneralSize10, new Vector2((int)(drawX), (int)(drawY + 4 + (++textLine * 14))), Color.Red);
                                                break;
                                        }

                                        if (!player.IsCasting)
                                        {
                                            if (player.SpellReady) Display.Mouse.State = GameMouseState.MagicAggressive;
                                            else Display.Mouse.State = GameMouseState.Aggressive;
                                        }
                                        break;
                                    case OwnerRelationShip.Neutral:
                                        if (owner.Type < 10) // players TODO fix this hard code?
                                        {
                                            spriteBatch.DrawString(Cache.Fonts[FontType.GeneralSize10], "Traveller", new Vector2((int)(drawX + 1), (int)(drawY + 4 + (++textLine * 14) + 1)), Color.Black);
                                            spriteBatch.DrawString(Cache.Fonts[FontType.GeneralSize10], "Traveller", new Vector2((int)(drawX), (int)(drawY + 4 + (textLine * 14))), new Color(50, 50, 255));
                                        }
                                        else
                                        {
                                            spriteBatch.DrawString(Cache.Fonts[FontType.GeneralSize10], "Neutral", new Vector2((int)(drawX + 1), (int)(drawY + 4 + (++textLine * 14) + 1)), Color.Black);
                                            spriteBatch.DrawString(Cache.Fonts[FontType.GeneralSize10], "Neutral", new Vector2((int)(drawX), (int)(drawY + 4 + (textLine * 14))), new Color(50, 50, 255));
                                        }
                                        Display.Mouse.State = GameMouseState.Highlight;

                                        if (!player.IsCasting)
                                        {
                                            if (player.SpellReady) Display.Mouse.State = GameMouseState.Magic;
                                            else Display.Mouse.State = GameMouseState.Highlight;
                                        }
                                        break;
                                    case OwnerRelationShip.Friendly:
                                        spriteBatch.DrawString(Cache.Fonts[FontType.GeneralSize10], string.Format("{0} {1}", owner.Side.ToString(), owner.SideStatus.ToString()), new Vector2((int)(drawX + 1), (int)(drawY + 4 + (++textLine * 14) + 1)), Color.Black);
                                        spriteBatch.DrawString(Cache.Fonts[FontType.GeneralSize10], string.Format("{0} {1}", owner.Side.ToString(), owner.SideStatus.ToString()), new Vector2((int)(drawX), (int)(drawY + 4 + (textLine * 14))), new Color(30, 255, 30));

                                        if (!player.IsCasting)
                                        {
                                            if (player.SpellReady) Display.Mouse.State = GameMouseState.Magic;
                                            else Display.Mouse.State = GameMouseState.Highlight;
                                        }
                                        break;
                                }

                                if (display.DebugMode)
                                {
                                    spriteBatch.DrawString(Cache.Fonts[FontType.GeneralSize10], string.Format("ID: {0}   X: {1}   Y: {2} (" + owner.Motion.ToString() + ")", owner.ObjectId, owner.X, owner.Y), new Vector2((int)(drawX + 1), (int)(drawY + 4 + (++textLine * 14) + 1)), Color.Black);
                                    spriteBatch.DrawString(Cache.Fonts[FontType.GeneralSize10], string.Format("ID: {0}   X: {1}   Y: {2} (" + owner.Motion.ToString() + ")", owner.ObjectId, owner.X, owner.Y), new Vector2((int)(drawX), (int)(drawY + 4 + (textLine * 14))), Color.White);
                                }
                            }
                            break;
                    }

                //SET BACKBUFFER AS RENDER TARGET
                spriteBatch.End();
                Display.Device.SetRenderTarget(mainRenderTarget);
                Display.Device.Clear(Color.Black);

                spriteBatch.Begin(SpriteSortMode.Deferred, null, null, null, null, null);

                //TODO clean up code

                spriteBatch.Draw(gameBoardRenderTarget, new Vector2(-display.GameBoardShiftX, -display.GameBoardShiftY), Color.White);
                //Rectangle rec = new Rectangle(-display.GameBoardShiftX + display.Zoom.X, -display.GameBoardShiftY + display.Zoom.Y, display.Zoom.Width, display.Zoom.Height);
                //spriteBatch.DrawItemPopup(gameBoardRenderTarget, display.Device.Viewport.Bounds, display.Zoom, Color.White);

                ///* REGEN RING */
                ////Create a Better texture for this
                if (Cache.GameSettings.RegenRings && !player.IsDead && (Cache.GameSettings.HpRing || Cache.GameSettings.MpRing || Cache.GameSettings.SpRing || Cache.GameSettings.ExpRing))
                {
                    Rectangle source = Cache.Effects[(int)SpriteId.ScreenEffect].Frames[1].GetRectangle();
                    Vector2 position = new Vector2(display.Device.Viewport.Width / 2, (display.Device.Viewport.Height / 2) - 20);

                    if (Cache.GameSettings.HpRing)
                    {
                        if (player.TickHP) { spriteBatch.Draw(Cache.Effects[(int)SpriteId.ScreenEffect].Texture, position, source, Color.Red * 0.5f, regenRotation, new Vector2((float)(source.Width / 2), (float)(source.Height / 2)), 0.38f, SpriteEffects.None, 0); }
                    }

                    if (Cache.GameSettings.MpRing)
                    {
                        if (player.TickMP) { spriteBatch.Draw(Cache.Effects[(int)SpriteId.ScreenEffect].Texture, position, source, Color.Blue * 0.5f, -regenRotation, new Vector2((float)(source.Width / 2), (float)(source.Height / 2)), 0.36f, SpriteEffects.None, 0); }
                    }

                    if (Cache.GameSettings.ExpRing)
                    {
                        if (player.TickEXP) { spriteBatch.Draw(Cache.Effects[(int)SpriteId.ScreenEffect].Texture, position, source, Color.Gold * 0.5f, -regenRotation, new Vector2((float)(source.Width / 2), (float)(source.Height / 2)), 0.32f, SpriteEffects.None, 0); }
                    }

                    if (Cache.GameSettings.SpRing)
                    {
                        if (player.TickSP) { spriteBatch.Draw(Cache.Effects[(int)SpriteId.ScreenEffect].Texture, position, source, Color.Green * 0.5f, regenRotation, new Vector2((float)(source.Width / 2), (float)(source.Height / 2)), 0.34f, SpriteEffects.None, 0); }
                    }
                }

                /*LOW HEALTH INDICATIOR*/
                //Create a Better texture for this
                //Possibly add an float Intensity that the color should be * by instead of glowframe
                if (Cache.GameSettings.LowHealthIndicator && !player.IsDead && (player.HP < (float)player.MaxHP * (Cache.GameSettings.LowHealthFlashValue / 100f)))
                {
                    spriteBatch.Draw(Cache.Effects[(int)SpriteId.ScreenEffect].Texture, new Vector2(0, 0), new Rectangle(0, 0, display.Device.Viewport.Width, display.Device.Viewport.Height), Color.Red * Cache.TransparencyFaders.GlowFrame);
                }


                /*BLOOD SPLATER*/
                //if ((Cache.GameSettings.DetailMode == GraphicsDetail.High) && (bloodSplaters.Count > 0))
                //{
                //    for (int i = 0; i < bloodSplaters.Count - 1; i++)
                //    {
                //        //{ image = 0, bloodSplaterIntensity = 1, x = 2, y = 3, rotation = 4, texture = 5 };
                //        spriteBatch.DrawItemPopup(
                //            Cache.Effects[(int)bloodSplaters[i][5]].Texture, //Texture
                //            new Rectangle((int)bloodSplaters[i][2], (int)bloodSplaters[i][3], Cache.Effects[(int)SpriteId.Blood].Frames[(int)bloodSplaters[i][0]].Width, Cache.Effects[(int)SpriteId.Blood].Frames[(int)bloodSplaters[i][0]].Height),
                //            new Rectangle(Cache.Effects[(int)SpriteId.Blood].Frames[(int)bloodSplaters[i][0]].X, Cache.Effects[(int)SpriteId.Blood].Frames[(int)bloodSplaters[i][0]].Y, Cache.Effects[(int)SpriteId.Blood].Frames[(int)bloodSplaters[i][0]].Width, Cache.Effects[(int)SpriteId.Blood].Frames[(int)bloodSplaters[i][0]].Height),
                //            Color.White * bloodSplaters[i][1],
                //            bloodSplaters[i][4],  //Rotation
                //            new Vector2(Cache.Effects[(int)SpriteId.Blood].Texture.Bounds.Width / 2, Cache.Effects[(int)SpriteId.Blood].Texture.Bounds.Height / 2), //Origin
                //            SpriteEffects.None, //Effect
                //            0); //LayerDepth
                //    }
                //}

                /* PUBLIC EVENT */
                /*if (publicEventState == PublicEventState.Created || publicEventState == PublicEventState.Started)
                {
                    Vector2 origin = new Vector2(Cache.Interface[(int)SpriteId.DialogsV2 + 2].Frames[15].Width, Cache.Interface[(int)SpriteId.DialogsV2 + 2].Frames[15].Height);
                    Vector2 diff = (new Vector2(publicEventX, publicEventY) - new Vector2(player.X, player.Y));
                    double rotation = Math.Atan2(diff.Y, diff.X);

                    //SpriteHelper.DrawTextWithShadow(spriteBatch, string.Format("{0},{1} - {2}", diff.X,diff.Y,rotation), FontType.DisplayName, new Vector2((display.PlayerCenterX * 32)+25, (display.PlayerCenterY * 32) - 70), Cache.GameState.Display.FontColours[FontType.DialogsV2Orange]);
                    spriteBatch.DrawItemPopup(Cache.Interface[(int)SpriteId.DialogsV2 + 2].Texture, new Vector2(display.PlayerCenterX*32, display.PlayerCenterY*32 - 70), Cache.Interface[(int)SpriteId.DialogsV2 + 2].Frames[15].GetRectangle(), Color.White, (float)rotation, origin, 1, SpriteEffects.None, 0);
                }*/

                if (publicEventParticipation)
                {
                    spriteBatch.Draw(Cache.Interface[(int)SpriteId.DialogsV2 + 7].Texture, new Vector2(display.ResolutionWidth - 350, 35), Cache.Interface[(int)SpriteId.DialogsV2 + 7].Frames[0].GetRectangle(), Color.White);

                    SpriteHelper.DrawTextWithShadow(spriteBatch, "Public Event: " + publicEventType.ToString(), FontType.DisplayNameSize13Spacing1, new Vector2(display.ResolutionWidth - 330, 38), Cache.Colors[GameColor.Orange]);

                    switch (publicEventType)
                    {
                        case PublicEventType.Defence: SpriteHelper.DrawTextWithShadow(spriteBatch, "Defend the Grand Magic Generator", FontType.DialogsSmallSize8, new Vector2(display.ResolutionWidth - 330, 64), Color.White, Color.Black); break;
                        case PublicEventType.Ambush: SpriteHelper.DrawTextWithShadow(spriteBatch, "Ambush the Enemy Base", FontType.DialogsSmallSize8, new Vector2(display.ResolutionWidth - 330, 64), Color.White, Color.Black); break;
                        case PublicEventType.Constructor: SpriteHelper.DrawTextWithShadow(spriteBatch, "Construct the Grand Magic Generator", FontType.DialogsSmallSize8, new Vector2(display.ResolutionWidth - 330, 64), Color.White, Color.Black); break;
                        case PublicEventType.Elimination: SpriteHelper.DrawTextWithShadow(spriteBatch, "Eliminate the elite target", FontType.DialogsSmallSize8, new Vector2(display.ResolutionWidth - 330, 64), Color.White, Color.Black); break;
                        case PublicEventType.Escort: SpriteHelper.DrawTextWithShadow(spriteBatch, "Escort the VIP to his destination", FontType.DialogsSmallSize8, new Vector2(display.ResolutionWidth - 330, 64), Color.White, Color.Black); break;
                    }

                    string endTimeFormat = ((publicEventEndTime - DateTime.Now).TotalSeconds < 0
                                                ? "00:00"
                                                : string.Format("{0}:{1:00}", (publicEventEndTime - DateTime.Now).Minutes, (publicEventEndTime - DateTime.Now).Seconds));

                    SpriteHelper.DrawTextRightWithShadow(spriteBatch, string.Format("Remaining Time: {0}", endTimeFormat), FontType.DialogsSmallSize8, new Vector2(display.ResolutionWidth - 330, 64), 320, Color.White, Color.Black);
                }

                /* LEVEL UP */
                if (player.LevelUpPoints > 0)
                    SpriteHelper.DrawTextWithOutline(spriteBatch, "Level Up!", FontType.DisplayNameSize13Spacing1, new Vector2(levelUpRectangle.X, levelUpRectangle.Y), Cache.Colors[GameColor.Orange], Color.Black);

                /* DIALOG BOXES */
                highlightedDialogBox = GameDialogBoxType.None; // checks mouse/dialog collisions
                if (dialogBoxes.Count > 0)
                    foreach (GameDialogBoxType dialogType in dialogBoxDrawOrder)
                    {
                        IGameDialogBox box = dialogBoxes[dialogType];

                        // hidden boxes with alwaysvisible show as greyed out in locked mode
                        if ((box.Config.Visible || (box.Config.AlwaysVisible && !Cache.GameSettings.LockedDialogs)) && box.Draw(spriteBatch, gameTime))
                            highlightedDialogBox = (GameDialogBoxType)box.Type;
                    }

                /* CHAT HISTORY */
                if (chatHistory.Count > 0)
                    for (int i = 0; i < chatHistory.Count; i++)
                        if (chatHistory.Count > i && (DateTime.Now - chatHistory[i].MessageTime).Seconds < 5)
                            SpriteHelper.DrawTextWithShadow(spriteBatch, string.Format("{0}: {1}", chatHistory[i].OwnerName, chatHistory[i].Message), chatHistory[i].Font, new Vector2((int)(10 + 1), (int)(10 + (i * 15) + 1)), Cache.Colors[chatHistory[i].Color]);

                /* EVENT HISTORY */
                if (eventHistory.Count > 0)
                    for (int i = 0; i < Globals.MaximumEventHistory; i++)
                        if (eventHistory.Count > i && (DateTime.Now - eventHistory[i].MessageTime).Seconds < 5)
                        {
                            spriteBatch.DrawString(Cache.Fonts[FontType.GeneralSize10], eventHistory[i].Message, new Vector2((int)(10 + 1), (int)(display.ResolutionHeight - 160 + (i * 15) + 1)), Color.Black);
                            spriteBatch.DrawString(Cache.Fonts[FontType.GeneralSize10], eventHistory[i].Message, new Vector2((int)(10), (int)(display.ResolutionHeight - 160 + (i * 15))), new Color(180, 255, 180));
                        }

                /* CHATTING */
                if (chatMode)
                {
                    spriteBatch.Draw(Cache.BackgroundTexture, chatBoxBackground, Color.Black * 0.5f);

                    if (chatBox.Text.Length > 0)
                        switch (chatBox.Text[0])
                        {
                            case '!': chatBox.Draw(spriteBatch, gameTime, Cache.Colors[GameColor.Global]); break;
                            case '~': chatBox.Draw(spriteBatch, gameTime, Cache.Colors[GameColor.Town]); break;
                            case '@':
                            case '^': chatBox.Draw(spriteBatch, gameTime, Cache.Colors[GameColor.Guild]); break;
                            case '$': chatBox.Draw(spriteBatch, gameTime, Cache.Colors[GameColor.Party]); break;
                            case '/':
                            case '#':
                            default: chatBox.Draw(spriteBatch, gameTime, Cache.Colors[GameColor.Local]); break;
                        }
                    else chatBox.Draw(spriteBatch, gameTime);
                }

                /* DEBUG */
                if (display.DebugMode)
                {
                    spriteBatch.DrawString(Cache.Fonts[FontType.GeneralSize10], "Name: " + player.Name, new Vector2(5, 5), Color.White);
                    spriteBatch.DrawString(Cache.Fonts[FontType.GeneralSize10], "HP: " + player.HP, new Vector2(5, 20), Color.White);
                    spriteBatch.DrawString(Cache.Fonts[FontType.GeneralSize10], "MP: " + player.MP, new Vector2(5, 35), Color.White);
                    spriteBatch.DrawString(Cache.Fonts[FontType.GeneralSize10], "SP: " + player.SP, new Vector2(5, 50), Color.White);
                    spriteBatch.DrawString(Cache.Fonts[FontType.GeneralSize10], "Contribution: " + player.Contribution, new Vector2(5, 65), Color.White);
                    spriteBatch.DrawString(Cache.Fonts[FontType.GeneralSize10], "Reputation: " + player.Reputation, new Vector2(5, 80), Color.White);
                    spriteBatch.DrawString(Cache.Fonts[FontType.GeneralSize10], "Map: " + player.MapName, new Vector2(5, 95), Color.White);
                    spriteBatch.DrawString(Cache.Fonts[FontType.GeneralSize10], "X: " + player.X, new Vector2(5, 110), Color.White);
                    spriteBatch.DrawString(Cache.Fonts[FontType.GeneralSize10], "Y: " + player.Y, new Vector2(5, 125), Color.White);
                    spriteBatch.DrawString(Cache.Fonts[FontType.GeneralSize10], "Criticals: " + player.Criticals, new Vector2(5, 140), Color.White);
                    spriteBatch.DrawString(Cache.Fonts[FontType.GeneralSize10], "Majestics: " + player.Majestics, new Vector2(5, 155), Color.White);

                    int rightSide = Display.ResolutionWidth - 200;
                    spriteBatch.DrawString(Cache.Fonts[FontType.GeneralSize10], "Level: " + player.Level, new Vector2(rightSide, 5), Color.White);
                    spriteBatch.DrawString(Cache.Fonts[FontType.GeneralSize10], "Strength: " + player.Strength, new Vector2(rightSide, 20), Color.White);
                    spriteBatch.DrawString(Cache.Fonts[FontType.GeneralSize10], "Dexterity: " + player.Dexterity, new Vector2(rightSide, 35), Color.White);
                    spriteBatch.DrawString(Cache.Fonts[FontType.GeneralSize10], "Intelligence: " + player.Intelligence, new Vector2(rightSide, 50), Color.White);
                    spriteBatch.DrawString(Cache.Fonts[FontType.GeneralSize10], "Magic: " + player.Magic, new Vector2(rightSide, 65), Color.White);
                    spriteBatch.DrawString(Cache.Fonts[FontType.GeneralSize10], "Vitality: " + player.Vitality, new Vector2(rightSide, 80), Color.White);
                    spriteBatch.DrawString(Cache.Fonts[FontType.GeneralSize10], "Agility: " + player.Agility, new Vector2(rightSide, 95), Color.White);
                    spriteBatch.DrawString(Cache.Fonts[FontType.GeneralSize10], "Experience: " + player.Experience, new Vector2(rightSide, 110), Color.White);
                    spriteBatch.DrawString(Cache.Fonts[FontType.GeneralSize10], "Gold: " + player.Gold, new Vector2(rightSide, 125), Color.White);
                    if (player.CriticalMode)
                        spriteBatch.DrawString(Cache.Fonts[FontType.GeneralSize10], "Damage: " + player.MinMeleeDamageCritical + "~" + player.MaxMeleeDamageCritical + "  (" + (int)player.DPSCritical + "dps)", new Vector2(rightSide, 140), Color.White);
                    else spriteBatch.DrawString(Cache.Fonts[FontType.GeneralSize10], "Damage: " + player.MinMeleeDamage + "~" + player.MaxMeleeDamage + "  (" + (int)player.DPS + "dps)", new Vector2(rightSide, 140), Color.White);

                    spriteBatch.DrawString(Cache.Fonts[FontType.GeneralSize10], "Direction: " + player.Direction.ToString(), new Vector2(5, 185), Color.White);
                    spriteBatch.DrawString(Cache.Fonts[FontType.GeneralSize10], "Mouse X: " + Display.Mouse.CellX + " (" + Display.Mouse.X + ") - " + (Cache.MapView.PivotX + Display.Mouse.CellX + Globals.OffScreenCells), new Vector2(5, 200), Color.White);
                    spriteBatch.DrawString(Cache.Fonts[FontType.GeneralSize10], "Mouse Y: " + Display.Mouse.CellY + " (" + Display.Mouse.Y + ") - " + (Cache.MapView.PivotY + Display.Mouse.CellY + Globals.OffScreenCells), new Vector2(5, 215), Color.White);
                    spriteBatch.DrawString(Cache.Fonts[FontType.GeneralSize10], "Center X: " + Display.PlayerCenterX, new Vector2(5, 230), Color.White);
                    spriteBatch.DrawString(Cache.Fonts[FontType.GeneralSize10], "Center Y: " + Display.PlayerCenterY, new Vector2(5, 245), Color.White);
                    spriteBatch.DrawString(Cache.Fonts[FontType.GeneralSize10], "Pivot X: " + Cache.MapView.PivotX, new Vector2(5, 260), Color.White);
                    spriteBatch.DrawString(Cache.Fonts[FontType.GeneralSize10], "Pivot Y: " + Cache.MapView.PivotY, new Vector2(5, 275), Color.White);
                    spriteBatch.DrawString(Cache.Fonts[FontType.GeneralSize10], "Destination X: " + player.DestinationX, new Vector2(5, 290), Color.White);
                    spriteBatch.DrawString(Cache.Fonts[FontType.GeneralSize10], "Destination Y: " + player.DestinationY, new Vector2(5, 305), Color.White);
                    spriteBatch.DrawString(Cache.Fonts[FontType.GeneralSize10], "Running: " + player.IsRunning.ToString(), new Vector2(5, 320), Color.White);
                    spriteBatch.DrawString(Cache.Fonts[FontType.GeneralSize10], "Mouse Drag: " + Display.Mouse.IsDragging.ToString() + " - (" + Display.Mouse.DragX + ", " + Display.Mouse.DragY + ")", new Vector2(5, 335), Color.White);
                    spriteBatch.DrawString(Cache.Fonts[FontType.GeneralSize10], "Dialog Box: " + highlightedDialogBox.ToString() + " highlighted, " + clickedDialogBox + " clicked", new Vector2(5, 350), Color.White);
                    spriteBatch.DrawString(Cache.Fonts[FontType.GeneralSize10], string.Format("fps: {0}", frameRate), new Vector2(5, 365), Color.White);
                    spriteBatch.DrawString(Cache.Fonts[FontType.GeneralSize10], string.Format("ping: {0}ms", ping), new Vector2(5, 380), Color.White);
                    spriteBatch.DrawString(Cache.Fonts[FontType.GeneralSize10], string.Format("Double Click: {0}", DateTime.Now - Display.Mouse.LeftClickTime), new Vector2(5, 395), Color.White);
                    spriteBatch.DrawString(Cache.Fonts[FontType.GeneralSize10], "Ability Activated: " + player.IsAbilityActivated.ToString(), new Vector2(5, 415), Color.White);
                    spriteBatch.DrawString(Cache.Fonts[FontType.GeneralSize10], "Ability Ready: " + player.SpecialAbilityReady.ToString(), new Vector2(5, 430), Color.White);

                    int count;
                    string activeAbilityTypes = "Active Ability Types:";
                    if (player.SpecialAbilitiesActive.Count > 0)
                    {
                        count = 0;
                        foreach (KeyValuePair<ItemSpecialAbilityType, int> type in player.SpecialAbilitiesActive)
                        {
                            if (count == 0) { activeAbilityTypes += " " + type.Key.ToString(); }
                            else { activeAbilityTypes += ", " + type.Key.ToString(); }
                            count++;
                        }
                    }
                    else { activeAbilityTypes += "None"; }
                    spriteBatch.DrawString(Cache.Fonts[FontType.GeneralSize10], activeAbilityTypes, new Vector2(5, 445), Color.White);


                    string passiveAbilityTypes = "Passive Ability Types: ";
                    if (player.SpecialAbilitiesPassive.Count > 0)
                    {
                        count = 0;
                        foreach (ItemSpecialAbilityType type in player.SpecialAbilitiesPassive)
                        {
                            if (count == 0) { passiveAbilityTypes += " " + type.ToString(); }
                            else { passiveAbilityTypes += ", " + type.ToString(); }
                            count++;
                        }
                    }
                    else { passiveAbilityTypes += "None"; }
                    spriteBatch.DrawString(Cache.Fonts[FontType.GeneralSize10], passiveAbilityTypes, new Vector2(5, 460), Color.White);

                }
                else
                {
                    spriteBatch.DrawString(Cache.Fonts[FontType.GeneralSize10], string.Format("Fps: {0}", frameRate), new Vector2(5, 155), Color.White);
                    spriteBatch.DrawString(Cache.Fonts[FontType.GeneralSize10], string.Format("Ping: {0}ms", ping), new Vector2(5, 170), Color.White);
                    //spriteBatch.DrawString(Display.Fonts[FontType.General], string.Format("In: {0}kbs", dataInRate.ToString("F")), new Vector2(5, 185), Color.White);
                    //spriteBatch.DrawString(Display.Fonts[FontType.General], string.Format("Out: {0}kbs", dataOutRate.ToString("F")), new Vector2(5, 200), Color.White);

                    /*spriteBatch.DrawString(Display.Fonts[FontType.General], string.Format("Event: {0}", publicEventType.ToString()), new Vector2(5, 185), Color.White);
                    spriteBatch.DrawString(Display.Fonts[FontType.General], string.Format("Event Difficulty: {0}", publicEventDifficulty.ToString()), new Vector2(5, 200), Color.White);
                    if (publicEventType != PublicEventType.None)
                        if (publicEventStarted)
                            spriteBatch.DrawString(Display.Fonts[FontType.General], string.Format("Event Ends: {0}", (publicEventEndTime-DateTime.Now).ToString()), new Vector2(5, 215), Color.White);
                        else spriteBatch.DrawString(Display.Fonts[FontType.General], string.Format("Event Starts: {0}", (publicEventStartTime - DateTime.Now).ToString()), new Vector2(5, 215), Color.White);
                     */
                }
            }


            //LEVEL HISTORY //TODO add on/off options for both majestics and levelup 
            if (levelHistory.Count > 0)
            {
                for (int i = levelHistory.Count - 1; i >= 0; i--)
                {
                    ChatMessage message = levelHistory[i];
                    switch (message.Color)
                    {
                        case GameColor.MajesticUp:
                            spriteBatch.DrawString(Cache.Fonts[message.Font], message.Message, new Vector2((display.Device.Viewport.Width / 2) - (Cache.Fonts[message.Font].MeasureString(message.Message).X / 2) + 1, display.Device.Viewport.Height - 100 + 1), Color.Black * message.Transparency);

                            spriteBatch.DrawString(Cache.Fonts[message.Font], message.Message, new Vector2((display.Device.Viewport.Width / 2) - (Cache.Fonts[message.Font].MeasureString(message.Message).X / 2), display.Device.Viewport.Height - 100), Cache.Colors[message.Color] * message.Transparency);
                            break;
                        case GameColor.LevelUp:

                            spriteBatch.DrawString(Cache.Fonts[message.Font], message.Message, new Vector2((display.Device.Viewport.Width / 2) - (Cache.Fonts[message.Font].MeasureString(message.Message).X / 2) + 1, 100 + 1), Color.Black * message.Transparency);

                            spriteBatch.DrawString(Cache.Fonts[message.Font], message.Message, new Vector2((display.Device.Viewport.Width / 2) - (Cache.Fonts[message.Font].MeasureString(message.Message).X / 2), 100), Cache.Colors[message.Color] * message.Transparency);
                            break;
                        default: break;
                    }
                }
            }

            /* POPUP */
            //TODO - POPUP FOR MAGIC ICONS
            if (popupType != GameDialogBoxType.None && popupType == highlightedDialogBox)
            {
                switch (popupType)
                {
                    //case GameDialogBoxType.SpellBookV2:
                    case GameDialogBoxType.HotBar:
                    case GameDialogBoxType.HotBarMagic:
                        {
                            /* MAGIC POPUP */
                            ItemPopup.DrawMagicPopup(spriteBatch, popupMagicIndex, popupLocation, popupX, popupY);
                            break;
                        }
                    default:
                        {
                            /* ITEM POPUP */
                            ItemPopup.DrawItemPopup(spriteBatch, popupItem, popupLocation, popupX, popupY);
                            break;
                        }
                }

            }

            /* DRAGGED ITEM */ //Item small...
            //TODO fix Inventoryv1 to be in this
            if (draggedType != DraggedType.None)
            {
                //Items
                if (draggedType == DraggedType.Inventory || draggedType == DraggedType.Warehouse)
                {
                    draggedItem = null;
                    switch (draggedType)
                    {
                        case DraggedType.Warehouse: { draggedItem = player.Warehouse[draggedIndex]; break; }
                        case DraggedType.Inventory: { draggedItem = player.Inventory[draggedIndex]; break; }
                        default: return;
                    }

                    if (draggedItem != null)
                    {
                        // get sprite frame
                        Rectangle f = Cache.Equipment[(int)SpriteId.ItemGround + draggedItem.Sprite].Frames[draggedItem.SpriteFrame].GetRectangle();
                        // get draw location offset from center of the slot
                        Rectangle draw = new Rectangle(
                            (int)display.Mouse.X - (f.Width > 31 || f.Height > 31 ? 31 : f.Width) / 2,
                            (int)display.Mouse.Y - (f.Width > 31 || f.Height > 31 ? 31 : f.Height) / 2,
                            f.Width,
                            f.Height
                            );

                        if (f.Width > 31 || f.Height > 31)
                            spriteBatch.Draw(Cache.Equipment[(int)SpriteId.ItemGround + draggedItem.Sprite].Texture, new Rectangle(draw.X, draw.Y, 31, 31), f, (draggedItem.ColorType == GameColor.Custom) ? SpriteHelper.ColourFromArgb(draggedItem.Colour) : Cache.Colors[draggedItem.ColorType]);
                        else spriteBatch.Draw(Cache.Equipment[(int)SpriteId.ItemGround + draggedItem.Sprite].Texture, new Vector2(draw.X, draw.Y), f, (draggedItem.ColorType == GameColor.Custom) ? SpriteHelper.ColourFromArgb(draggedItem.Colour) : Cache.Colors[draggedItem.ColorType]);

                        if (draggedItem.Count > 1)
                            SpriteHelper.DrawTextRightWithShadow(spriteBatch, string.Format("x{0}", draggedItem.Count.ToString()), FontType.DialogsSmallerSize7, new Vector2(display.Mouse.X - 18, display.Mouse.Y + 2), 32, Cache.Colors[GameColor.Orange]);
                        if (draggedItem.SetNumber > 0)
                            SpriteHelper.DrawTextWithShadow(spriteBatch, string.Format("{0}", draggedItem.SetNumber.ToString()), FontType.DialogsSmallerSize7, new Vector2(display.Mouse.X - 12, display.Mouse.Y - 14), Cache.Colors[GameColor.Orange]);
                    }
                }

                //Magic Icon's
                if (draggedType == DraggedType.MagicIcon)
                {
                    spriteBatch.Draw(Cache.Interface[(int)SpriteId.DialogsV2 + 4].Texture, new Vector2(display.Mouse.X - 15, display.Mouse.Y - 15), Cache.Interface[(int)SpriteId.DialogsV2 + 4].Frames[Cache.MagicConfiguration[draggedIndex].SmallIconSpriteFrame].GetRectangle(), Color.White);
                }
            }

            /* MOUSE */
            Display.Mouse.Draw(spriteBatch);
        }


        /// <summary>
        /// Handles mouse clicks and scrolling.
        /// </summary>
        /// <param name="isActive">Specifies whether the game is focused or not. Unattended movement continues if not active.</param>
        public void HandleMouse(bool isActive)
        {
            /* LEFT CLICKED */
            if (Display.Mouse.LeftClick && isActive)
            {
                /* SET CLICKED DIALOGBOX & BRING TO FRONT & CHECK FOR CLICKED ITEM  */
                if (clickedDialogBox == GameDialogBoxType.None && highlightedDialogBox != GameDialogBoxType.None)
                {
                    clickedDialogBox = highlightedDialogBox;
                    BringToFront(clickedDialogBox);

                    if (dialogBoxes[clickedDialogBox].SelectedItemIndex != -1 && dialogBoxes[clickedDialogBox].ClickedItemIndex == -1)
                    {
                        dialogBoxes[clickedDialogBox].ClickedItemIndex = dialogBoxes[clickedDialogBox].SelectedItemIndex;
                         switch ((GameDialogBoxType)dialogBoxes[clickedDialogBox].Type)
                         {
                             case GameDialogBoxType.InventoryV1:
                                 player.InventoryDrawOrder.Remove(dialogBoxes[clickedDialogBox].ClickedItemIndex);
                                 player.InventoryDrawOrder.AddLast(dialogBoxes[clickedDialogBox].ClickedItemIndex);
                                 break;
                         }
                    }

                }

                if (clickedDialogBox != GameDialogBoxType.None)
                {
                    /*DOUBLE LEFT CLICK ACTION ON DIALOGBOX */
                    if (!display.Mouse.IsDragging && (DateTime.Now - Display.Mouse.LeftClickTime).TotalMilliseconds < 300)
                    {
                        dialogBoxes[clickedDialogBox].LeftDoubleClicked(ref selectionMode, ref selectionModeId);
                    }

                    /*LEFT CLICK ACTION ON DIALOGBOX */
                    dialogBoxes[clickedDialogBox].LeftClicked();
                    Display.Mouse.LeftClickTime = DateTime.Now;

                }
                /* CLICK NPC TO OPEN DIALOGS */
                else if (selectedObjectId != -1 && !player.IsDead)
                {
                    switch (selectedObjectType)
                    {
                        case ObjectType.Owner:
                        case ObjectType.DeadOwner:
                            if (Cache.OwnerCache.ContainsKey((UInt16)selectedObjectId) && !Cache.OwnerCache[(UInt16)selectedObjectId].Location.Equals(player.Location))
                            {
                                IOwner owner = Cache.OwnerCache[(UInt16)selectedObjectId];

                                if (owner.MerchantType != MerchantType.None)
                                {
                                    switch (owner.MerchantType)
                                    {
                                        case MerchantType.General:
                                        case MerchantType.Blacksmith:
                                            ((MerchantDialogBox)dialogBoxes[GameDialogBoxType.Merchant]).MerchantId = owner.MerchantId;
                                            if (dialogBoxes[GameDialogBoxType.Merchant].Config.Visible) { dialogBoxes[GameDialogBoxType.Merchant].Hide(); }
                                            else { dialogBoxes[GameDialogBoxType.Merchant].Show(); }
                                            break;
                                        case MerchantType.Warehouse:
                                            dialogBoxes[GameDialogBoxType.WarehouseV2].Toggle();
                                            break;
                                        case MerchantType.MagicShop:
                                            dialogBoxes[GameDialogBoxType.MagicShop].Toggle();
                                            break;
                                    }
                                }
                                break;
                            }
                            break;
                    }
                }
            }

            /* RIGHT CLICKED */
            if (Display.Mouse.RightClick && isActive)
            {
                /* CANCEL SELECTION MODE */
                if (selectionMode != SelectionMode.None)
                {
                    //clear selection mode for apply item
                    selectionMode = SelectionMode.None;
                    selectionModeId = -1;
                }
                /* CLOSE DIALOG */
                else if (highlightedDialogBox != GameDialogBoxType.None && !dialogBoxes[highlightedDialogBox].Config.AlwaysVisible)
                {
                    dialogBoxes[highlightedDialogBox].RightClicked();

                    if (dialogBoxes[(GameDialogBoxType)highlightedDialogBox].Config.AlwaysVisible) // handle hud dialogs that cant be removed (except the lock)
                    {
                        if (!Cache.GameSettings.LockedDialogs) // if HUD is unlocked, allow toggle (HUD items dont disappear, just go transparent
                            if (dialogBoxes[(GameDialogBoxType)highlightedDialogBox].Config.Visible)
                                dialogBoxes[(GameDialogBoxType)highlightedDialogBox].Hide();
                            else dialogBoxes[(GameDialogBoxType)highlightedDialogBox].Show();
                    }
                    else
                    {
                        // close dialog special cases
                        switch ((GameDialogBoxType)highlightedDialogBox)
                        {
                            case GameDialogBoxType.PartyRequest: PartyRequestConfirmation(GameDialogBoxType.PartyRequest, MessageBoxResponse.No); break;
                            case GameDialogBoxType.TradeV2Confirmation: TradeRequestConfirmation(GameDialogBoxType.TradeV2Confirmation, MessageBoxResponse.No); break;
                            case GameDialogBoxType.TradeV2: EndTrade(TradeState.Cancelled); break;
                            default: dialogBoxes[(GameDialogBoxType)highlightedDialogBox].Hide(); break; // handle all other dialogs
                        }
                    }
                }
                /* CANCEL SPELL CAST */
                else if (player.IsCasting)
                {
                    player.SpellReady = player.IsCasting = false;
                    player.Idle(player.Direction, false);
                    Idle(player.Direction);
                    player.SpellRequest = -1;
                    AddEvent("Cancelled.");
                }
            }

            /* LEFT HELD */
            if (Display.Mouse.LeftHeld && isActive)
            {
                bool move = false;
                if (clickedDialogBox != GameDialogBoxType.None)
                {

                    /* LEFT HELD ACTION */
                    dialogBoxes[clickedDialogBox].LeftHeld(); //TODO fix message dialog boxes

                    if (display.Mouse.IsDragging)
                    {
                        /*LEFT DRAG ACTION */
                        if (dialogBoxes[clickedDialogBox].ClickedItemIndex != -1)
                        {
                            dialogBoxes[clickedDialogBox].LeftDragged();
                        }

                        /* DRAGGING THE DIALOG BOX ITSELF */
                        // can only move "Moveable" dialogs, unless LockedDialogs is false and we aren't trying to move the padlock (firm bottom right)
                        else if ((dialogBoxes[clickedDialogBox].Config.Moveable || (!Cache.GameSettings.LockedDialogs && clickedDialogBox != GameDialogBoxType.LockIconPanel)) &&
                            display.Mouse.X >= 0 && display.Mouse.X <= display.ResolutionWidth && display.Mouse.Y >= 0 && display.Mouse.Y <= display.ResolutionHeight)
                        {
                            dialogBoxes[clickedDialogBox].OffsetLocation(display.Mouse.DragX, display.Mouse.DragY); //s
                        }
                    }
                }

                if (highlightedDialogBox == GameDialogBoxType.None && draggedType == DraggedType.None)
                {

                    if (player.SpellRequest != -1 && player.MoveReady && !player.IsMoving && !player.IsCasting && !player.SpellReady)
                    {
                        player.Idle(player.Direction, true);
                        Idle(player.Direction);

                        Cast(player.SpellRequest);
                        player.Cast(player.SpellRequest);
                        player.SpellRequest = -1;
                    }

                    /* MAGIC CASTING */
                    else if (player.IsCasting)
                    {
                        if (player.SpellReady && !player.IsDead)
                        {
                            if (!player.HasShield)
                            {
                                int destinationX, destinationY;
                                if (selectedObjectId != -1 && (selectedObjectType == ObjectType.Owner || selectedObjectType == ObjectType.DeadOwner)
                                    && Cache.OwnerCache.ContainsKey((UInt16)selectedObjectId))
                                {
                                    destinationX = Cache.OwnerCache[(UInt16)selectedObjectId].X;
                                    destinationY = Cache.OwnerCache[(UInt16)selectedObjectId].Y;
                                }
                                else
                                {
                                    destinationX = Cache.MapView.PivotX + Display.Mouse.CellX + Globals.OffScreenCells;
                                    destinationY = Cache.MapView.PivotY + Display.Mouse.CellY + Globals.OffScreenCells;
                                }

                                Magic(destinationX, destinationY);
                                //player.Idle(player.Direction, false); //switched
                                //Idle(player.Direction);

                                player.SpellReady = player.IsCasting = false;
                                player.SpellRequest = -1;

                            }
                            else
                            {
                                Cache.DefaultState.AddEvent("Your hands must be free to cast magic.");
                                player.SpellReady = player.IsCasting = false;
                                player.SpellRequest = -1;
                            }

                            draggedType = DraggedType.None;
                        }
                    }

                    /* PICK UP ITEM */
                    else if (selectedObjectType == ObjectType.Owner && selectedObjectId == player.ObjectId && player.MoveReady && !player.IsDead)
                    {
                        player.PickUp();
                        PickUp(player.Direction);
                        player.Commands++;
                    }
                    else if (selectedObjectId != -1 && player.MoveReady && !player.IsDead)
                    {
                        switch (selectedObjectType)
                        {
                            case ObjectType.DynamicObject:
                                if (Cache.DynamicObjects.ContainsKey(selectedObjectId))
                                {
                                    DynamicObject dynamic = Cache.DynamicObjects[selectedObjectId];
                                    switch (dynamic.Type)
                                    {
                                        case DynamicObjectType.Rock:
                                        case DynamicObjectType.Gem:
                                            MotionDirection direction = Utility.GetNextDirection(Display.PlayerCenterX, Display.PlayerCenterY, Display.PlayerCenterX + (dynamic.X - player.X), Display.PlayerCenterY + (dynamic.Y - player.Y));

                                            if ((display.Keyboard.LeftShift || player.IsRunning) && player.CanDash(dynamic.X, dynamic.Y) && player.CanMove(direction)  //hardcoded leftshift for dash??? needs fixing
                                                    && (!player.CriticalMode))
                                            {
                                                Dash(direction, player.AttackType, dynamic.X, dynamic.Y);
                                                player.Dash(direction, dynamic.X, dynamic.Y, (player.Weapon != null ? player.Weapon.ItemId : 0));
                                                Cache.MapView.Pan(direction);
                                            }
                                            else if (player.WithinRange(dynamic.X, dynamic.Y))
                                            {
                                                player.Direction = direction;

                                                if (player.IsCombatMode)
                                                    player.Attack(dynamic.X, dynamic.Y, (player.CriticalMode && player.Criticals > 0 ? AttackType.Critical : AttackType.Normal));
                                                else player.Bow(direction);
                                                Attack(direction, player.AttackType, dynamic.X, dynamic.Y);
                                                if (player.AttackType == AttackType.Critical) player.Criticals--;

                                                player.Commands++;
                                            }
                                            else move = true;
                                            break;
                                        case DynamicObjectType.Fish:
                                            if (GameUtility.WithinRange(player.X, player.Y, dynamic.X, dynamic.Y, 5))
                                            {
                                                Fish(dynamic.X, dynamic.Y);
                                                AudioHelper.PlaySound("E14");
                                            }
                                            else move = true;
                                            break;
                                        default: move = true; break;
                                    }
                                }
                                else selectedObjectId = -1;
                                break;
                            case ObjectType.Owner:
                            case ObjectType.DeadOwner:
                                if (Cache.OwnerCache.ContainsKey((UInt16)selectedObjectId) && !Cache.OwnerCache[(UInt16)selectedObjectId].Location.Equals(player.Location))
                                {
                                    IOwner owner = Cache.OwnerCache[(UInt16)selectedObjectId];

                                    if (owner.MerchantType != MerchantType.None) //TODO find best way to do this
                                    {
                                        player.DestinationX = player.X;
                                        player.DestinationY = player.Y;
                                    }
                                    else if (!owner.IsInvisible && (player.ForceAttack || (owner.GetRelationship(player.Side) == OwnerRelationShip.Enemy && !owner.IsDead)))
                                    {
                                        MotionDirection direction = Utility.GetNextDirection(Display.PlayerCenterX, Display.PlayerCenterY, Display.PlayerCenterX + (owner.X - player.X), Display.PlayerCenterY + (owner.Y - player.Y));

                                        if ((display.Keyboard.LeftShift || player.IsRunning) && player.CanDash(owner) && player.CanMove(direction)  //hardcoded leftshift for dash??? needs fixing
                                            && (!player.CriticalMode))
                                        {
                                            Dash(direction, player.AttackType, owner.X, owner.Y);
                                            player.Dash(direction, owner.X, owner.Y, (player.Weapon != null ? player.Weapon.ItemId : 0));
                                            Cache.MapView.Pan(direction);
                                        }
                                        else if (player.WithinRange(owner))
                                        {
                                            player.Direction = direction;

                                            {
                                                if (player.IsCombatMode)
                                                    player.Attack(owner.X, owner.Y, (player.CriticalMode && player.Criticals > 0 ? AttackType.Critical : AttackType.Normal));
                                                else player.Bow(direction);
                                                Attack(direction, player.AttackType, owner.X, owner.Y);
                                                if (player.AttackType == AttackType.Critical) player.Criticals--;
                                            }
                                            player.Commands++;
                                        }
                                        else move = true;
                                    }
                                    else move = true;
                                }
                                else selectedObjectId = -1;
                                break;
                        }
                    }
                    else move = true;

                    if (move && player.SpellRequest == -1)
                    {
                        if (!player.IsCasting && !player.IsDead && (player.LevelUpPoints == 0 || !levelUpRectangle.Contains(display.Mouse.Point)))
                        {
                            player.HaltMovement = false;

                            player.DestinationX = Cache.MapView.PivotX + (((int)Display.Mouse.Position.X + player.OffsetX + display.GameBoardShiftX) / Globals.CellWidth) + Globals.OffScreenCells;
                            player.DestinationY = Cache.MapView.PivotY + (((int)Display.Mouse.Position.Y + player.OffsetY + display.GameBoardShiftY) / Globals.CellHeight) + Globals.OffScreenCells;

                            if (player.DestinationX == player.X && player.DestinationY == player.Y)
                            {
                                player.DestinationX = -1;
                                player.DestinationY = -1;
                            }

                            if (player.MoveReady)
                            {

                                // fly during movement
                                if (player.Flying && player.CanMove(player.FlyDirection))
                                {
                                    Fly(player.FlyDirection);
                                    player.Fly(player.FlyDamage, player.FlyHitCount, player.FlyDirection, player.FlyDamageType);
                                    Cache.MapView.Pan(player.FlyDirection);
                                    player.SpellReady = player.IsCasting = false;
                                    player.SpellRequest = -1;
                                }
                                else if (player.Location.IsTeleport)
                                {
                                    player.Idle(player.Direction, true);
                                    Idle(player.Direction);
                                    Teleport();
                                }
                                else if (player.DestinationX != -1 && player.DestinationY != -1)
                                {
                                    MotionDirection direction = Utility.GetNextDirection(Display.PlayerCenterX, Display.PlayerCenterY, Display.Mouse.CellX, Display.Mouse.CellY);

                                    if (Display.Mouse.RightHeld)
                                    {
                                        if ((direction == player.Direction && (player.MoveReady && player.Motion != MotionType.Idle)) ||
                                                (direction != player.Direction && player.MoveReady) && direction != MotionDirection.None)
                                        {
                                            player.Idle(direction);
                                            Idle(player.Direction);
                                            player.Commands++;
                                        }
                                        //Display.Mouse.RightHeld = false;
                                    }

                                    MotionDirection newDirection = FindNextMovableDirection(direction);

                                    if (newDirection != MotionDirection.None)
                                    {
                                        Move(newDirection, player.IsRunning);

                                        if (player.IsRunning) player.Run(newDirection);
                                        else player.Walk(newDirection);
                                        Cache.MapView.Pan(newDirection);
                                    }
                                    else if (player.Motion != MotionType.Idle)
                                    {
                                        player.Idle(player.Direction);
                                        Idle(player.Direction);
                                    }
                                }
                            }
                        }
                    }
                }
            }

            /* RIGHT HELD */
            if (Display.Mouse.RightHeld && isActive)
            {
                if (highlightedDialogBox == GameDialogBoxType.None)
                {
                    bool idle = false;
                    /*  */
                    if (selectedObjectId != -1 && player.MoveReady && !player.IsDead)
                    {
                        switch (selectedObjectType)
                        {
                            case ObjectType.DynamicObject:
                                if (Cache.DynamicObjects.ContainsKey(selectedObjectId))
                                {
                                    DynamicObject dynamic = Cache.DynamicObjects[selectedObjectId];
                                    MotionDirection direction = Utility.GetNextDirection(Display.PlayerCenterX, Display.PlayerCenterY, Display.PlayerCenterX + (dynamic.X - player.X), Display.PlayerCenterY + (dynamic.Y - player.Y));

                                    if (player.WithinRange(dynamic.X, dynamic.Y))
                                    {
                                        player.Direction = direction;

                                        {
                                            if (player.IsCombatMode)
                                                player.Attack(dynamic.X, dynamic.Y, (player.CriticalMode && player.Criticals > 0 ? AttackType.Critical : AttackType.Normal));
                                            else player.Bow(direction);
                                            Attack(direction, player.AttackType, dynamic.X, dynamic.Y);
                                            if (player.AttackType == AttackType.Critical) player.Criticals--;
                                        }
                                        player.Commands++;
                                    }
                                    else idle = true;
                                }
                                else selectedObjectId = -1;
                                break;
                            case ObjectType.DeadOwner:
                            case ObjectType.Owner:
                                if (Cache.OwnerCache.ContainsKey((UInt16)selectedObjectId) && !Cache.OwnerCache[(UInt16)selectedObjectId].Location.Equals(player.Location))
                                {
                                    IOwner owner = Cache.OwnerCache[(UInt16)selectedObjectId];
                                    if (!owner.IsInvisible && (player.ForceAttack || (owner.GetRelationship(player.Side) == OwnerRelationShip.Enemy && !owner.IsDead)))
                                    {
                                        MotionDirection direction = Utility.GetNextDirection(Display.PlayerCenterX, Display.PlayerCenterY, Display.PlayerCenterX + (owner.X - player.X), Display.PlayerCenterY + (owner.Y - player.Y));

                                        if (player.WithinRange(owner))
                                            if (player.IsCombatMode)
                                            {
                                                player.Direction = direction;
                                                player.Attack(owner.X, owner.Y, (player.CriticalMode && player.Criticals > 0 ? AttackType.Critical : AttackType.Normal));
                                                Attack(direction, player.AttackType, owner.X, owner.Y);
                                                if (player.AttackType == AttackType.Critical) player.Criticals--;
                                                player.Commands++;
                                            }
                                            else
                                            {
                                                player.Bow(direction);
                                                Attack(direction, player.AttackType, owner.X, owner.Y);
                                                if (player.AttackType == AttackType.Critical) player.Criticals--;
                                                player.Commands++;
                                            }
                                        else idle = true;
                                    }
                                    else idle = true;
                                }
                                else selectedObjectId = -1;
                                break;
                        }
                    }
                    else idle = true;

                    if (idle)
                        if (!player.IsCasting && !player.IsDead)
                        {
                            MotionDirection direction = Utility.GetNextDirection(Display.PlayerCenterX, Display.PlayerCenterY, (int)Display.Mouse.CellX, (int)Display.Mouse.CellY);

                            if (Display.Mouse.LeftReleased && (player.LevelUpPoints == 0 || !levelUpRectangle.Contains(display.Mouse.Point)) && player.MoveReady && !player.IsMoving)
                            {

                                player.DestinationX = Cache.MapView.PivotX + (((int)Display.Mouse.Position.X + player.OffsetX + display.GameBoardShiftX) / Globals.CellWidth) + Globals.OffScreenCells;
                                player.DestinationY = Cache.MapView.PivotY + (((int)Display.Mouse.Position.Y + player.OffsetY + display.GameBoardShiftY) / Globals.CellHeight) + Globals.OffScreenCells;

                                if (player.DestinationX == player.X && player.DestinationY == player.Y)
                                {
                                    player.DestinationX = -1;
                                    player.DestinationY = -1;
                                }

                                // fly during movement
                                if (player.Flying && player.CanMove(player.FlyDirection))
                                {
                                    Fly(player.FlyDirection);
                                    player.Fly(player.FlyDamage, player.FlyHitCount, player.FlyDirection, player.FlyDamageType);
                                    Cache.MapView.Pan(player.FlyDirection);
                                    player.SpellReady = player.IsCasting = false;
                                    player.SpellRequest = -1;
                                }
                                else if (player.Location.IsTeleport)
                                {
                                    player.Idle(player.Direction, true);
                                    Idle(player.Direction);
                                    Teleport();
                                }
                                else if (player.DestinationX != -1 && player.DestinationY != -1)
                                {
                                    MotionDirection newDirection = FindNextMovableDirection(direction);

                                    if (newDirection != MotionDirection.None)
                                    {
                                        Move(newDirection, player.IsRunning);

                                        if (player.IsRunning) player.Run(newDirection);
                                        else player.Walk(newDirection);
                                        Cache.MapView.Pan(newDirection);
                                    }
                                    else if (player.Motion != MotionType.Idle)
                                    {
                                        player.Idle(player.Direction);
                                        Idle(player.Direction);
                                    }
                                }
                            }

                            else if ((direction == player.Direction && (player.MoveReady && player.Motion != MotionType.Idle)) ||
                                (direction != player.Direction && player.MoveReady) && direction != MotionDirection.None)
                            {
                                player.Idle(direction);
                                Idle(player.Direction);
                                player.Commands++;
                            }

                            else if (player.CanHalt)
                            {
                                player.HaltMovement = true;
                            }
                        }
                }
            }

            /* LEFT RELEASED */
            if (Display.Mouse.LeftReleased && isActive)
            {
                /* LEVEL UP CLICK */
                if (player.LevelUpPoints > 0)
                    if (levelUpRectangle.Contains(display.Mouse.Point))
                    {
                        if (dialogBoxes[GameDialogBoxType.LevelUpV1].Config.Visible)
                        {
                            dialogBoxes[GameDialogBoxType.LevelUpV1].Toggle();
                        }
                        else
                        {
                            player.RemainderLevelUpPoints = player.LevelUpPoints;
                            player.LevelUpStrength = player.LevelUpDexterity = player.LevelUpMagic =
                                player.LevelUpIntelligence = player.LevelUpVitality = player.LevelUpAgility = 0;

                            dialogBoxes[GameDialogBoxType.LevelUpV1].Show();
                            BringToFront(GameDialogBoxType.LevelUpV1);
                        }

                        /* SAVE CHANGES TO DIALOG LOCATIONS */
                        SaveDialogBoxConfiguration();
                    }

                if (clickedDialogBox != GameDialogBoxType.None)
                {
                    /* DROP THE DRAGGING ITEM */
                    if (dialogBoxes.ContainsKey(clickedDialogBox) &&
                        dialogBoxes[clickedDialogBox].ClickedItemIndex != -1 && !player.IsDead)
                    {
                        dialogBoxes[clickedDialogBox].LeftReleased(highlightedDialogBox, (highlightedDialogBox != GameDialogBoxType.None) ? dialogBoxes[highlightedDialogBox].HighlightedItemIndex : -1);
                        if (dialogBoxes.ContainsKey(clickedDialogBox)) dialogBoxes[clickedDialogBox].ClickedItemIndex = -1;
                        draggedType = DraggedType.None;                     
                    }
                    clickedDialogBox = GameDialogBoxType.None;
                }
            }

            /* RIGHT RELEASED */
            if (Display.Mouse.RightReleased && isActive)
            { 

            }

            /* MOUSE SCROLLED */
            if (Display.Mouse.Scroll != 0 && isActive)
            {
                if (highlightedDialogBox != GameDialogBoxType.None && dialogBoxes.ContainsKey(highlightedDialogBox))
                {
                    dialogBoxes[highlightedDialogBox].Scroll(Display.Mouse.Scroll);
                }
                else if (clickedDialogBox != GameDialogBoxType.None && dialogBoxes.ContainsKey(clickedDialogBox))
                {
                    dialogBoxes[clickedDialogBox].Scroll(Display.Mouse.Scroll);
                }
                else
                {
                    topdialogBox = dialogBoxDrawOrder.Last();
                    if (dialogBoxes.ContainsKey(topdialogBox) && dialogBoxes[topdialogBox].Config.Visible)
                        dialogBoxes[topdialogBox].Scroll(Display.Mouse.Scroll);
                }
            }
                        
            // FLY FROM IDLE
            if (player.Flying && player.CanMove(player.FlyDirection) && !player.IsMoving)
            {
                Fly(player.FlyDirection);
                player.Fly(player.FlyDamage, player.FlyHitCount, player.FlyDirection, player.FlyDamageType);
                Cache.MapView.Pan(player.FlyDirection);
                player.SpellReady = player.IsCasting = false;
                player.SpellRequest = -1;
            }

            // CAST FROM IDLE
            else if (player.SpellRequest != -1 && !player.IsCasting && !player.SpellReady && !player.IsMoving)
            {
                player.Idle(player.Direction, true);
                Idle(player.Direction);

                Cast(player.SpellRequest);
                player.Cast(player.SpellRequest);
                player.SpellRequest = -1;
                draggedType = DraggedType.None;
            }

            /* NO CLICK - UNATTENDED MOVEMENT */
            else if (player.IsMoving && player.MoveReady && !player.IsDead)
            {
                if (player.X == player.DestinationX && player.Y == player.DestinationY)
                {
                    player.DestinationX = -1;
                    player.DestinationY = -1;
                }
                // fly during unattended movement
                if (player.Flying && player.CanMove(player.FlyDirection))
                {
                    Fly(player.FlyDirection);
                    player.Fly(player.FlyDamage, player.FlyHitCount, player.FlyDirection, player.FlyDamageType);
                    Cache.MapView.Pan(player.FlyDirection);
                    player.SpellReady = player.IsCasting = false;
                    player.SpellRequest = -1;
                }
                else if (player.Location.IsTeleport)
                {
                    player.Idle(player.Direction, true);
                    Idle(player.Direction);
                    Teleport();
                }
                else if (player.SpellRequest != -1 && !player.IsCasting && !player.SpellReady)
                {
                    player.Idle(player.Direction, true);
                    Idle(player.Direction);

                    Cast(player.SpellRequest);
                    player.Cast(player.SpellRequest);
                    player.SpellRequest = -1;
                }
                else if (player.HaltMovement)
                {
                    MotionDirection direction = Utility.GetNextDirection(Display.PlayerCenterX, Display.PlayerCenterY, (int)Display.Mouse.CellX, (int)Display.Mouse.CellY);

                    if ((direction == player.Direction && (player.MoveReady && player.Motion != MotionType.Idle)) ||
                            (direction != player.Direction && player.MoveReady) && direction != MotionDirection.None)
                    {
                        player.Idle(direction);
                        Idle(player.Direction);
                        player.Commands++;
                    }
                    player.HaltMovement = false;
                }
                else if (player.DestinationX != -1 || player.DestinationY != -1) //If destination X/Y, move 
                {
                    if (player.X != player.DestinationX || player.Y != player.DestinationY)
                    {
                        MotionDirection newDirection = FindNextMovableDirection(Utility.GetNextDirection(player.X, player.Y, player.DestinationX, player.DestinationY));

                        if (newDirection != MotionDirection.None)
                        {
                            Move(newDirection, player.IsRunning);

                            if (player.IsRunning) player.Run(newDirection);
                            else player.Walk(newDirection);
                            Cache.MapView.Pan(newDirection);
                        }
                        else if (player.Motion != MotionType.Idle) //Set to idle if new direction is none
                        {
                            player.Idle(player.Direction);
                            Idle(player.Direction);
                        }
                    }
                }
                else if (player.Motion != MotionType.Idle)//Set to idle to finish unintended movement
                {
                    player.Idle(player.Direction);
                    Idle(player.Direction);
                }
            }
        }





        //LEFT HELD
        //bool move = false;





        //        if (player.SpellRequest != -1 && player.MoveReady && !player.IsMoving && !player.IsCasting && !player.SpellReady)
        //        {
        //            player.Idle(player.Direction, true);
        //            Idle(player.Direction);

        //            Cast(player.SpellRequest);
        //            player.Cast(player.SpellRequest);
        //            player.SpellRequest = -1;
        //        }

        //        /* MAGIC CASTING */
        //        else if (player.IsCasting)
        //        {
        //            if (player.SpellReady && !player.IsDead)
        //            {
        //                if (!player.HasShield)
        //                {
        //                    int destinationX, destinationY;
        //                    if (selectedObjectId != -1 && (selectedObjectType == ObjectType.Owner || selectedObjectType == ObjectType.DeadOwner)
        //                        && Cache.OwnerCache.ContainsKey((UInt16)selectedObjectId))
        //                    {
        //                        destinationX = Cache.OwnerCache[(UInt16)selectedObjectId].X;
        //                        destinationY = Cache.OwnerCache[(UInt16)selectedObjectId].Y;
        //                    }
        //                    else
        //                    {
        //                        destinationX = Cache.MapView.PivotX + Display.Mouse.CellX + Globals.OffScreenCells;
        //                        destinationY = Cache.MapView.PivotY + Display.Mouse.CellY + Globals.OffScreenCells;
        //                    }

        //                    Magic(destinationX, destinationY);
        //                    //player.Idle(player.Direction, false); //switched
        //                    //Idle(player.Direction);

        //                    player.SpellReady = player.IsCasting = false;
        //                    player.SpellRequest = -1;
        //                }
        //                else
        //                {
        //                    Cache.DefaultState.AddEvent("Your hands must be free to cast magic.");
        //                    player.SpellReady = player.IsCasting = false;
        //                    player.SpellRequest = -1;
        //                }
        //            }
        //        }
        //        /* PICK UP ITEM */
        //        else if (selectedObjectType == ObjectType.Owner && selectedObjectId == player.ObjectId && player.MoveReady && !player.IsDead)
        //        {
        //            player.PickUp();
        //            PickUp(player.Direction);
        //            player.Commands++;
        //        }
        //        else if (selectedObjectId != -1 && player.MoveReady && !player.IsDead)
        //        {
        //            switch (selectedObjectType)
        //            {
        //                case ObjectType.DynamicObject:
        //                    if (Cache.DynamicObjects.ContainsKey(selectedObjectId))
        //                    {
        //                        DynamicObject dynamic = Cache.DynamicObjects[selectedObjectId];
        //                        switch (dynamic.Type)
        //                        {
        //                            case DynamicObjectType.Rock:
        //                            case DynamicObjectType.Gem:
        //                                MotionDirection direction = Utility.GetNextDirection(Display.PlayerCenterX, Display.PlayerCenterY, Display.PlayerCenterX + (dynamic.X - player.X), Display.PlayerCenterY + (dynamic.Y - player.Y));

        //                                if ((display.Keyboard.LeftShift || player.IsRunning) && player.CanDash(dynamic.X, dynamic.Y) && player.CanMove(direction)  //hardcoded leftshift for dash??? needs fixing
        //                                        && (!player.CriticalMode))
        //                                {
        //                                    Dash(direction, player.AttackType, dynamic.X, dynamic.Y);
        //                                    player.Dash(direction, dynamic.X, dynamic.Y, (player.Weapon != null ? player.Weapon.ItemId : 0));
        //                                    Cache.MapView.Pan(direction);
        //                                }
        //                                else if (player.WithinRange(dynamic.X, dynamic.Y))
        //                                {
        //                                    player.Direction = direction;

        //                                    if (player.IsCombatMode)
        //                                        player.Attack(dynamic.X, dynamic.Y, (player.CriticalMode && player.Criticals > 0 ? AttackType.Critical : AttackType.Normal));
        //                                    else player.Bow(direction);
        //                                    Attack(direction, player.AttackType, dynamic.X, dynamic.Y);
        //                                    if (player.AttackType == AttackType.Critical) player.Criticals--;

        //                                    player.Commands++;
        //                                }
        //                                else move = true;
        //                                break;
        //                            case DynamicObjectType.Fish:
        //                                if (GameUtility.WithinRange(player.X, player.Y, dynamic.X, dynamic.Y, 5))
        //                                {
        //                                    Fish(dynamic.X, dynamic.Y);
        //                                    AudioHelper.PlaySound("E14");
        //                                }
        //                                else move = true;
        //                                break;
        //                            default: move = true; break;
        //                        }
        //                    }
        //                    else selectedObjectId = -1;
        //                    break;
        //                case ObjectType.Owner:
        //                case ObjectType.DeadOwner:
        //                    if (Cache.OwnerCache.ContainsKey((UInt16)selectedObjectId) && !Cache.OwnerCache[(UInt16)selectedObjectId].Location.Equals(player.Location))
        //                    {
        //                        IOwner owner = Cache.OwnerCache[(UInt16)selectedObjectId];

        //                        if (owner.MerchantType != MerchantType.None)
        //                        { } // DO nothing - check again on mouse up
        //                        else if (!owner.IsInvisible && (player.ForceAttack || (owner.GetRelationship(player.Side) == OwnerRelationShip.Enemy && !owner.IsDead)))
        //                        {
        //                            MotionDirection direction = Utility.GetNextDirection(Display.PlayerCenterX, Display.PlayerCenterY, Display.PlayerCenterX + (owner.X - player.X), Display.PlayerCenterY + (owner.Y - player.Y));

        //                            if ((display.Keyboard.LeftShift || player.IsRunning) && player.CanDash(owner) && player.CanMove(direction)  //hardcoded leftshift for dash??? needs fixing
        //                                && (!player.CriticalMode))
        //                            {
        //                                Dash(direction, player.AttackType, owner.X, owner.Y);
        //                                player.Dash(direction, owner.X, owner.Y, (player.Weapon != null ? player.Weapon.ItemId : 0));
        //                                Cache.MapView.Pan(direction);
        //                            }
        //                            else if (player.WithinRange(owner))
        //                            {
        //                                player.Direction = direction;

        //                                {
        //                                    if (player.IsCombatMode)
        //                                        player.Attack(owner.X, owner.Y, (player.CriticalMode && player.Criticals > 0 ? AttackType.Critical : AttackType.Normal));
        //                                    else player.Bow(direction);
        //                                    Attack(direction, player.AttackType, owner.X, owner.Y);
        //                                    if (player.AttackType == AttackType.Critical) player.Criticals--;
        //                                }
        //                                player.Commands++;
        //                            }
        //                            else move = true;
        //                        }
        //                        else move = true;
        //                    }
        //                    else selectedObjectId = -1;
        //                    break;
        //            }
        //        }
        //        else move = true;

        //        if (move && player.SpellRequest == -1)
        //        {
        //            if (!player.IsCasting && !player.IsDead && (player.LevelUpPoints == 0 || !levelUpRectangle.Contains(display.Mouse.Point)))
        //            {
        //                player.HaltMovement = false;

        //                player.DestinationX = Cache.MapView.PivotX + (((int)Display.Mouse.Position.X + player.OffsetX + display.GameBoardShiftX) / Globals.CellWidth) + Globals.OffScreenCells;
        //                player.DestinationY = Cache.MapView.PivotY + (((int)Display.Mouse.Position.Y + player.OffsetY + display.GameBoardShiftY) / Globals.CellHeight) + Globals.OffScreenCells;

        //                if (player.DestinationX == player.X && player.DestinationY == player.Y)
        //                {
        //                    player.DestinationX = -1;
        //                    player.DestinationY = -1;
        //                }

        //                if (player.MoveReady)
        //                {

        //                    // fly during movement
        //                    if (player.Flying && player.CanMove(player.FlyDirection))
        //                    {
        //                        Fly(player.FlyDirection);
        //                        player.Fly(player.FlyDamage, player.FlyDirection);
        //                        Cache.MapView.Pan(player.FlyDirection);
        //                        player.SpellReady = player.IsCasting = false;
        //                        player.SpellRequest = -1;
        //                    }
        //                    else if (player.Location.IsTeleport)
        //                    {
        //                        player.Idle(player.Direction, true);
        //                        Idle(player.Direction);
        //                        Teleport();
        //                    }
        //                    else if (player.DestinationX != -1 && player.DestinationY != -1)
        //                    {
        //                        MotionDirection direction = Utility.GetNextDirection(Display.PlayerCenterX, Display.PlayerCenterY, Display.Mouse.CellX, Display.Mouse.CellY);

        //                        if (Display.Mouse.RightHeld)
        //                        {
        //                            if ((direction == player.Direction && (player.MoveReady && player.Motion != MotionType.Idle)) ||
        //                                    (direction != player.Direction && player.MoveReady) && direction != MotionDirection.None)
        //                            {
        //                                player.Idle(direction);
        //                                Idle(player.Direction);
        //                                player.Commands++;
        //                            }
        //                            //Display.Mouse.RightHeld = false;
        //                        }

        //                        MotionDirection newDirection = FindNextMovableDirection(direction);

        //                        if (newDirection != MotionDirection.None)
        //                        {
        //                            Move(newDirection, player.IsRunning);

        //                            if (player.IsRunning) player.Run(newDirection);
        //                            else player.Walk(newDirection);
        //                            Cache.MapView.Pan(newDirection);
        //                        }
        //                        else if (player.Motion != MotionType.Idle)
        //                        {
        //                            player.Idle(player.Direction);
        //                            Idle(player.Direction);
        //                        }
        //                    }
        //                }
        //            }
        //        }


                ///RIGHT HELD
            //bool idle = false;
            //    /*  */
            //    if (selectedObjectId != -1 && player.MoveReady && !player.IsDead)
            //    {
            //        switch (selectedObjectType)
            //        {
            //            case ObjectType.DynamicObject:
            //                if (Cache.DynamicObjects.ContainsKey(selectedObjectId))
            //                {
            //                    DynamicObject dynamic = Cache.DynamicObjects[selectedObjectId];
            //                    MotionDirection direction = Utility.GetNextDirection(Display.PlayerCenterX, Display.PlayerCenterY, Display.PlayerCenterX + (dynamic.X - player.X), Display.PlayerCenterY + (dynamic.Y - player.Y));

            //                    if (player.WithinRange(dynamic.X, dynamic.Y))
            //                    {
            //                        player.Direction = direction;

            //                        {
            //                            if (player.IsCombatMode)
            //                                player.Attack(dynamic.X, dynamic.Y, (player.CriticalMode && player.Criticals > 0 ? AttackType.Critical : AttackType.Normal));
            //                            else player.Bow(direction);
            //                            Attack(direction, player.AttackType, dynamic.X, dynamic.Y);
            //                            if (player.AttackType == AttackType.Critical) player.Criticals--;
            //                        }
            //                        player.Commands++;
            //                    }
            //                    else idle = true;
            //                }
            //                else selectedObjectId = -1;
            //                break;
            //            case ObjectType.DeadOwner:
            //            case ObjectType.Owner:
            //                if (Cache.OwnerCache.ContainsKey((UInt16)selectedObjectId) && !Cache.OwnerCache[(UInt16)selectedObjectId].Location.Equals(player.Location))
            //                {
            //                    IOwner owner = Cache.OwnerCache[(UInt16)selectedObjectId];
            //                    if (!owner.IsInvisible && (player.ForceAttack || (owner.GetRelationship(player.Side) == OwnerRelationShip.Enemy && !owner.IsDead)))
            //                    {
            //                        MotionDirection direction = Utility.GetNextDirection(Display.PlayerCenterX, Display.PlayerCenterY, Display.PlayerCenterX + (owner.X - player.X), Display.PlayerCenterY + (owner.Y - player.Y));

            //                        if (player.WithinRange(owner))
            //                            if (player.IsCombatMode)
            //                            {
            //                                player.Direction = direction;
            //                                player.Attack(owner.X, owner.Y, (player.CriticalMode && player.Criticals > 0 ? AttackType.Critical : AttackType.Normal));
            //                                Attack(direction, player.AttackType, owner.X, owner.Y);
            //                                if (player.AttackType == AttackType.Critical) player.Criticals--;
            //                                player.Commands++;
            //                            }
            //                            else
            //                            {
            //                                player.Bow(direction);
            //                                Attack(direction, player.AttackType, owner.X, owner.Y);
            //                                if (player.AttackType == AttackType.Critical) player.Criticals--;
            //                                player.Commands++;
            //                            }
            //                        else idle = true;
            //                    }
            //                    else idle = true;
            //                }
            //                else selectedObjectId = -1;
            //                break;
            //        }
            //    }
            //    else idle = true;

            //    if (idle)
            //        if (!player.IsCasting && !player.IsDead)
            //        {
            //            MotionDirection direction = Utility.GetNextDirection(Display.PlayerCenterX, Display.PlayerCenterY, (int)Display.Mouse.CellX, (int)Display.Mouse.CellY);

            //            if (Display.Mouse.LeftReleased && (player.LevelUpPoints == 0 || !levelUpRectangle.Contains(display.Mouse.Point)) && player.MoveReady && !player.IsMoving)
            //            {

            //                player.DestinationX = Cache.MapView.PivotX + (((int)Display.Mouse.Position.X + player.OffsetX + display.GameBoardShiftX) / Globals.CellWidth) + Globals.OffScreenCells;
            //                player.DestinationY = Cache.MapView.PivotY + (((int)Display.Mouse.Position.Y + player.OffsetY + display.GameBoardShiftY) / Globals.CellHeight) + Globals.OffScreenCells;

            //                if (player.DestinationX == player.X && player.DestinationY == player.Y)
            //                {
            //                    player.DestinationX = -1;
            //                    player.DestinationY = -1;
            //                }

            //                // fly during movement
            //                if (player.Flying && player.CanMove(player.FlyDirection))
            //                {
            //                    Fly(player.FlyDirection);
            //                    player.Fly(player.FlyDamage, player.FlyDirection);
            //                    Cache.MapView.Pan(player.FlyDirection);
            //                    player.SpellReady = player.IsCasting = false;
            //                    player.SpellRequest = -1;
            //                }
            //                else if (player.Location.IsTeleport)
            //                {
            //                    player.Idle(player.Direction, true);
            //                    Idle(player.Direction);
            //                    Teleport();
            //                }
            //                else if (player.DestinationX != -1 && player.DestinationY != -1)
            //                {
            //                    MotionDirection newDirection = FindNextMovableDirection(direction);

            //                    if (newDirection != MotionDirection.None)
            //                    {
            //                        Move(newDirection, player.IsRunning);

            //                        if (player.IsRunning) player.Run(newDirection);
            //                        else player.Walk(newDirection);
            //                        Cache.MapView.Pan(newDirection);
            //                    }
            //                    else if (player.Motion != MotionType.Idle)
            //                    {
            //                        player.Idle(player.Direction);
            //                        Idle(player.Direction);
            //                    }
            //                }
            //            }

            //            else if ((direction == player.Direction && (player.MoveReady && player.Motion != MotionType.Idle)) ||
            //                (direction != player.Direction && player.MoveReady) && direction != MotionDirection.None)
            //            {
            //                player.Idle(direction);
            //                Idle(player.Direction);
            //                player.Commands++;
            //            }

            //            else if (player.CanHalt)
            //            {
            //                player.HaltMovement = true;
            //            }
            //        }


            //        // FLY FROM IDLE
            //if (player.Flying && player.CanMove(player.FlyDirection) && !player.IsMoving)
            //{
            //    Fly(player.FlyDirection);
            //    player.Fly(player.FlyDamage, player.FlyDirection);
            //    Cache.MapView.Pan(player.FlyDirection);
            //    player.SpellReady = player.IsCasting = false;
            //    player.SpellRequest = -1;
            //}

            //// CAST FROM IDLE
        //if ( player.MoveReady )
            //else if (player.SpellRequest != -1 && !player.IsCasting && !player.SpellReady && !player.IsMoving)
            //{
            //    player.Idle(player.Direction, true);
            //    Idle(player.Direction);

            //    Cast(player.SpellRequest);
            //    player.Cast(player.SpellRequest);
            //    player.SpellRequest = -1;
            //}

            ///* NO CLICK - UNATTENDED MOVEMENT */
            //else if (player.IsMoving && player.MoveReady && !player.IsDead)
            //{
            //    if (player.X == player.DestinationX && player.Y == player.DestinationY)
            //    {
            //        player.DestinationX = -1;
            //        player.DestinationY = -1;
            //    }
            //    // fly during unattended movement
            //    if (player.Flying && player.CanMove(player.FlyDirection))
            //    {
            //        Fly(player.FlyDirection);
            //        player.Fly(player.FlyDamage, player.FlyDirection);
            //        Cache.MapView.Pan(player.FlyDirection);
            //        player.SpellReady = player.IsCasting = false;
            //        player.SpellRequest = -1;
            //    }
            //    else if (player.Location.IsTeleport)
            //    {
            //        player.Idle(player.Direction, true);
            //        Idle(player.Direction);
            //        Teleport();
            //    }
            //    else if (player.SpellRequest != -1 && !player.IsCasting && !player.SpellReady) //CAST SPELL
            //    {
            //        player.Idle(player.Direction, true);
            //        Idle(player.Direction);

            //        Cast(player.SpellRequest);
            //        player.Cast(player.SpellRequest);
            //        player.SpellRequest = -1;
            //    }
            //    else if (player.HaltMovement)
            //    {
            //        MotionDirection direction = Utility.GetNextDirection(Display.PlayerCenterX, Display.PlayerCenterY, (int)Display.Mouse.CellX, (int)Display.Mouse.CellY);

            //        if ((direction == player.Direction && (player.MoveReady && player.Motion != MotionType.Idle)) ||
            //                (direction != player.Direction && player.MoveReady) && direction != MotionDirection.None)
            //        {
            //            player.Idle(direction);
            //            Idle(player.Direction);
            //            player.Commands++;
            //        }
            //        player.HaltMovement = false;
            //    }
            //    else if (player.DestinationX != -1 || player.DestinationY != -1)
            //    {
            //        if (player.X != player.DestinationX || player.Y != player.DestinationY)
            //        {
            //            MotionDirection newDirection = FindNextMovableDirection(Utility.GetNextDirection(player.X, player.Y, player.DestinationX, player.DestinationY));

            //            if (newDirection != MotionDirection.None)
            //            {
            //                Move(newDirection, player.IsRunning);

            //                if (player.IsRunning) player.Run(newDirection);
            //                else player.Walk(newDirection);
            //                Cache.MapView.Pan(newDirection);
            //            }
            //            else if (player.Motion != MotionType.Idle)
            //            {
            //                player.Idle(player.Direction);
            //                Idle(player.Direction);
            //            }
            //            else if (player.Motion != MotionType.Idle)
            //            {
            //                player.Idle(player.Direction);
            //                Idle(player.Direction);
            //            }

            //        }
            //    }
            //    else
            //    {
            //        player.Idle(player.Direction);
            //        Idle(player.Direction);
            //    }
            //}










        //OLD HANDLE MOUSE CODE
        ///// <summary>
        ///// Handles mouse clicks and scrolling.
        ///// </summary>
        ///// <param name="isActive">Specifies whether the game is focused or not. Unattended movement continues if not active.</param>
        //public void HandleMouse(bool isActive)
        //{
        //    /* LEFT MOUSE */
        //    if ((Display.Mouse.LeftClick || Display.Mouse.LeftHeld) && isActive)
        //    {
        //        bool move = false;

        //        /* DIALOG CLICK */
        //        if (clickedDialogBox != GameDialogBoxType.None)
        //        {
        //            /* START DRAGGING CLICKED DIALOG BOX */
        //            if (dialogBoxes[clickedDialogBox].SelectedItemIndex != -1 && dialogBoxes[clickedDialogBox].ClickedItemIndex == -1)
        //            {
        //                dialogBoxes[clickedDialogBox].ClickedItemIndex = dialogBoxes[clickedDialogBox].SelectedItemIndex;

        //                switch ((GameDialogBoxType)dialogBoxes[clickedDialogBox].Type)
        //                {
        //                    case GameDialogBoxType.InventoryV1:
        //                        player.InventoryDrawOrder.Remove(dialogBoxes[clickedDialogBox].ClickedItemIndex);
        //                        player.InventoryDrawOrder.AddLast(dialogBoxes[clickedDialogBox].ClickedItemIndex);
        //                        break;
        //                }
        //            }
        //            else if (display.Mouse.IsDragging)
        //            {
        //                /* DRAGGING OBJECT ON DIALOG BOX */
        //                if (dialogBoxes[clickedDialogBox].ClickedItemIndex != -1)
        //                {
        //                    switch (clickedDialogBox)
        //                    {
        //                        case GameDialogBoxType.InventoryV1:
        //                            if (player.Inventory[dialogBoxes[clickedDialogBox].ClickedItemIndex] != null)
        //                            {
        //                                Item item = player.Inventory[dialogBoxes[clickedDialogBox].ClickedItemIndex];
        //                                item.BagX += ((int)display.Mouse.X + (int)display.Mouse.DragX);
        //                                item.BagY += ((int)display.Mouse.Y + (int)display.Mouse.DragY);
        //                            }
        //                            break;
        //                        case GameDialogBoxType.WarehouseV2:
        //                            if (player.Warehouse[dialogBoxes[clickedDialogBox].ClickedItemIndex] != null)
        //                            {
        //                                dialogBoxes[clickedDialogBox].SelectedItemIndexOffsetX += ((int)display.Mouse.X + (int)display.Mouse.DragX);
        //                                dialogBoxes[clickedDialogBox].SelectedItemIndexOffsetY += ((int)display.Mouse.Y + (int)display.Mouse.DragY);
        //                            }
        //                            break;
        //                        case GameDialogBoxType.CharacterV2:
        //                            if (player.Inventory[dialogBoxes[clickedDialogBox].ClickedItemIndex] != null)
        //                            {
        //                                dialogBoxes[clickedDialogBox].SelectedItemIndexOffsetX += ((int)display.Mouse.X + (int)display.Mouse.DragX);
        //                                dialogBoxes[clickedDialogBox].SelectedItemIndexOffsetY += ((int)display.Mouse.Y + (int)display.Mouse.DragY);
        //                            }
        //                            break;
        //                        case GameDialogBoxType.HotBar:
        //                            if (Cache.HotBarConfiguration.ContainsKey(dialogBoxes[clickedDialogBox].ClickedItemIndex))
        //                            {
        //                                dialogBoxes[clickedDialogBox].SelectedItemIndexOffsetX += ((int)display.Mouse.X + (int)display.Mouse.DragX);
        //                                dialogBoxes[clickedDialogBox].SelectedItemIndexOffsetY += ((int)display.Mouse.Y + (int)display.Mouse.DragY);
        //                            }
        //                            break;
        //                        case GameDialogBoxType.UpgradeV2:
        //                            if (player.Inventory[dialogBoxes[clickedDialogBox].ClickedItemIndex] != null)
        //                            {
        //                                dialogBoxes[clickedDialogBox].SelectedItemIndexOffsetX += ((int)display.Mouse.X + (int)display.Mouse.DragX);
        //                                dialogBoxes[clickedDialogBox].SelectedItemIndexOffsetY += ((int)display.Mouse.Y + (int)display.Mouse.DragY);
        //                            }
        //                            break;
        //                    }

        //                    //display.Mouse.DragX = (int)display.Mouse.X;
        //                    //display.Mouse.DragY = (int)display.Mouse.Y;
        //                }
        //                /* DRAGGING THE DIALOG BOX ITSELF */
        //                // can only move "Moveable" dialogs, unless LockedDialogs is false and we aren't trying to move the padlock (firm bottom right)
        //                else if ((dialogBoxes[clickedDialogBox].Config.Moveable || (!Cache.GameSettings.LockedDialogs && clickedDialogBox != GameDialogBoxType.LockIconPanel)) &&
        //                    display.Mouse.X >= 0 && display.Mouse.X <= display.ResolutionWidth &&
        //                    display.Mouse.Y >= 0 && display.Mouse.Y <= display.ResolutionHeight)
        //                {
        //                    dialogBoxes[clickedDialogBox].OffsetLocation(display.Mouse.DragX, display.Mouse.DragY);

        //                    //display.Mouse.DragX = (int)display.Mouse.X;
        //                    //display.Mouse.DragY = (int)display.Mouse.Y;

        //                    //if ((GameDialogBoxType)dialogBoxes[clickedDialogBox].Type == GameDialogBoxType.InventoryV1)
        //                    //{
        //                    //    player.InventoryDrawOrder.Remove(dialogBoxes[clickedDialogBox].ClickedItemIndex);
        //                    //    player.InventoryDrawOrder.AddLast(dialogBoxes[clickedDialogBox].ClickedItemIndex);
        //                    //}
        //                }
        //            }
        //        }
        //        /* DIALOG CLICK - BRING TO FRONT */
        //        else if (highlightedDialogBox != GameDialogBoxType.None)
        //        {
        //            if (!Display.Mouse.IsDragging)
        //            {
        //                clickedDialogBox = highlightedDialogBox;
        //                BringToFront(clickedDialogBox);

        //                /* DIALOG ON MOUSE DOWN ACTION */
        //                //Only specific dialogboxes activate on mouse down
        //                switch (clickedDialogBox)
        //                {
        //                    case GameDialogBoxType.SpellBook:
        //                        dialogBoxes[GameDialogBoxType.SpellBook].LeftClick();
        //                        break;
        //                    case GameDialogBoxType.HotBar:
        //                        dialogBoxes[GameDialogBoxType.HotBar].LeftClick();
        //                        break;
        //                    default: break;
        //                }

        //            }

        //        }

        //        else if (player.SpellRequest != -1 && player.MoveReady && !player.IsMoving && !player.IsCasting && !player.SpellReady)
        //        {
        //            player.Idle(player.Direction, true);
        //            Idle(player.Direction);

        //            Cast(player.SpellRequest);
        //            player.Cast(player.SpellRequest);
        //            player.SpellRequest = -1;
        //        }

        //        /* MAGIC CASTING */
        //        else if (player.IsCasting)
        //        {
        //            if (player.SpellReady && !player.IsDead)
        //            {
        //                if (!player.HasShield)
        //                {
        //                    int destinationX, destinationY;
        //                    if (selectedObjectId != -1 && (selectedObjectType == ObjectType.Owner || selectedObjectType == ObjectType.DeadOwner)
        //                        && Cache.OwnerCache.ContainsKey((UInt16)selectedObjectId))
        //                    {
        //                        destinationX = Cache.OwnerCache[(UInt16)selectedObjectId].X;
        //                        destinationY = Cache.OwnerCache[(UInt16)selectedObjectId].Y;
        //                    }
        //                    else
        //                    {
        //                        destinationX = Cache.MapView.PivotX + Display.Mouse.CellX + Globals.OffScreenCells;
        //                        destinationY = Cache.MapView.PivotY + Display.Mouse.CellY + Globals.OffScreenCells;
        //                    }

        //                    Magic(destinationX, destinationY);
        //                    //player.Idle(player.Direction, false); //switched
        //                    //Idle(player.Direction);

        //                    player.SpellReady = player.IsCasting = false;
        //                    player.SpellRequest = -1;
        //                }
        //                else
        //                {
        //                    Cache.DefaultState.AddEvent("Your hands must be free to cast magic.");
        //                    player.SpellReady = player.IsCasting = false;
        //                    player.SpellRequest = -1;
        //                }
        //            }
        //        }
        //        /* PICK UP ITEM */
        //        else if (selectedObjectType == ObjectType.Owner && selectedObjectId == player.ObjectId && player.MoveReady && !player.IsDead)
        //        {
        //            player.PickUp();
        //            PickUp(player.Direction);
        //            player.Commands++;
        //        }
        //        else if (selectedObjectId != -1 && player.MoveReady && !player.IsDead)
        //        {
        //            switch (selectedObjectType)
        //            {
        //                case ObjectType.DynamicObject:
        //                    if (Cache.DynamicObjects.ContainsKey(selectedObjectId))
        //                    {
        //                        DynamicObject dynamic = Cache.DynamicObjects[selectedObjectId];
        //                        switch (dynamic.Type)
        //                        {
        //                            case DynamicObjectType.Rock:
        //                            case DynamicObjectType.Gem:
        //                                MotionDirection direction = Utility.GetNextDirection(Display.PlayerCenterX, Display.PlayerCenterY, Display.PlayerCenterX + (dynamic.X - player.X), Display.PlayerCenterY + (dynamic.Y - player.Y));

        //                                if ((display.Keyboard.LeftShift || player.IsRunning) && player.CanDash(dynamic.X, dynamic.Y) && player.CanMove(direction)  //hardcoded leftshift for dash??? needs fixing
        //                                        && (!player.CriticalMode))
        //                                {
        //                                    Dash(direction, player.AttackType, dynamic.X, dynamic.Y);
        //                                    player.Dash(direction, dynamic.X, dynamic.Y, (player.Weapon != null ? player.Weapon.ItemId : 0));
        //                                    Cache.MapView.Pan(direction);
        //                                }
        //                                else if (player.WithinRange(dynamic.X, dynamic.Y))
        //                                {
        //                                    player.Direction = direction;

        //                                    if (player.IsCombatMode)
        //                                        player.Attack(dynamic.X, dynamic.Y, (player.CriticalMode && player.Criticals > 0 ? AttackType.Critical : AttackType.Normal));
        //                                    else player.Bow(direction);
        //                                    Attack(direction, player.AttackType, dynamic.X, dynamic.Y);
        //                                    if (player.AttackType == AttackType.Critical) player.Criticals--;

        //                                    player.Commands++;
        //                                }
        //                                else move = true;
        //                                break;
        //                            case DynamicObjectType.Fish:
        //                                if (GameUtility.WithinRange(player.X, player.Y, dynamic.X, dynamic.Y, 5))
        //                                {
        //                                    Fish(dynamic.X, dynamic.Y);
        //                                    AudioHelper.PlaySound("E14");
        //                                }
        //                                else move = true;
        //                                break;
        //                            default: move = true; break;
        //                        }
        //                    }
        //                    else selectedObjectId = -1;
        //                    break;
        //                case ObjectType.Owner:
        //                case ObjectType.DeadOwner:
        //                    if (Cache.OwnerCache.ContainsKey((UInt16)selectedObjectId) && !Cache.OwnerCache[(UInt16)selectedObjectId].Location.Equals(player.Location))
        //                    {
        //                        IOwner owner = Cache.OwnerCache[(UInt16)selectedObjectId];

        //                        if (owner.MerchantType != MerchantType.None)
        //                        { } // DO nothing - check again on mouse up
        //                        else if (!owner.IsInvisible && (player.ForceAttack || (owner.GetRelationship(player.Side) == OwnerRelationShip.Enemy && !owner.IsDead)))
        //                        {
        //                            MotionDirection direction = Utility.GetNextDirection(Display.PlayerCenterX, Display.PlayerCenterY, Display.PlayerCenterX + (owner.X - player.X), Display.PlayerCenterY + (owner.Y - player.Y));

        //                            if ((display.Keyboard.LeftShift || player.IsRunning) && player.CanDash(owner) && player.CanMove(direction)  //hardcoded leftshift for dash??? needs fixing
        //                                && (!player.CriticalMode))
        //                            {
        //                                Dash(direction, player.AttackType, owner.X, owner.Y);
        //                                player.Dash(direction, owner.X, owner.Y, (player.Weapon != null ? player.Weapon.ItemId : 0));
        //                                Cache.MapView.Pan(direction);
        //                            }
        //                            else if (player.WithinRange(owner))
        //                            {
        //                                player.Direction = direction;

        //                                {
        //                                    if (player.IsCombatMode)
        //                                        player.Attack(owner.X, owner.Y, (player.CriticalMode && player.Criticals > 0 ? AttackType.Critical : AttackType.Normal));
        //                                    else player.Bow(direction);
        //                                    Attack(direction, player.AttackType, owner.X, owner.Y);
        //                                    if (player.AttackType == AttackType.Critical) player.Criticals--;
        //                                }
        //                                player.Commands++;
        //                            }
        //                            else move = true;
        //                        }
        //                        else move = true;
        //                    }
        //                    else selectedObjectId = -1;
        //                    break;
        //            }
        //        }
        //        else move = true;

        //        if (move && player.SpellRequest == -1)
        //        {
        //            if (!player.IsCasting && !player.IsDead && (player.LevelUpPoints == 0 || !levelUpRectangle.Contains(display.Mouse.Point)))
        //            {
        //                player.HaltMovement = false;

        //                player.DestinationX = Cache.MapView.PivotX + (((int)Display.Mouse.Position.X + player.OffsetX + display.GameBoardShiftX) / Globals.CellWidth) + Globals.OffScreenCells;
        //                player.DestinationY = Cache.MapView.PivotY + (((int)Display.Mouse.Position.Y + player.OffsetY + display.GameBoardShiftY) / Globals.CellHeight) + Globals.OffScreenCells;

        //                if (player.DestinationX == player.X && player.DestinationY == player.Y)
        //                {
        //                    player.DestinationX = -1;
        //                    player.DestinationY = -1;
        //                }

        //                if (player.MoveReady)
        //                {

        //                    // fly during movement
        //                    if (player.Flying && player.CanMove(player.FlyDirection))
        //                    {
        //                        Fly(player.FlyDirection);
        //                        player.Fly(player.FlyDamage, player.FlyDirection);
        //                        Cache.MapView.Pan(player.FlyDirection);
        //                        player.SpellReady = player.IsCasting = false;
        //                        player.SpellRequest = -1;
        //                    }
        //                    else if (player.Location.IsTeleport)
        //                    {
        //                        player.Idle(player.Direction, true);
        //                        Idle(player.Direction);
        //                        Teleport();
        //                    }
        //                    else if (player.DestinationX != -1 && player.DestinationY != -1)
        //                    {
        //                        MotionDirection direction = Utility.GetNextDirection(Display.PlayerCenterX, Display.PlayerCenterY, Display.Mouse.CellX, Display.Mouse.CellY);

        //                        if (Display.Mouse.RightHeld)
        //                        {
        //                            if ((direction == player.Direction && (player.MoveReady && player.Motion != MotionType.Idle)) ||
        //                                    (direction != player.Direction && player.MoveReady) && direction != MotionDirection.None)
        //                            {
        //                                player.Idle(direction);
        //                                Idle(player.Direction);
        //                                player.Commands++;
        //                            }
        //                            //Display.Mouse.RightHeld = false;
        //                        }

        //                        MotionDirection newDirection = FindNextMovableDirection(direction);

        //                        if (newDirection != MotionDirection.None)
        //                        {
        //                            Move(newDirection, player.IsRunning);

        //                            if (player.IsRunning) player.Run(newDirection);
        //                            else player.Walk(newDirection);
        //                            Cache.MapView.Pan(newDirection);
        //                        }
        //                        else if (player.Motion != MotionType.Idle)
        //                        {
        //                            player.Idle(player.Direction);
        //                            Idle(player.Direction);
        //                        }
        //                    }
        //                }
        //            }
        //        }
        //    }
        //    /* RIGHT MOUSE */
        //    else if ((Display.Mouse.RightClick || Display.Mouse.RightHeld) && isActive)
        //    {
        //        bool idle = false;

        //        if (selectionMode != SelectionMode.None)
        //        {
        //            //clear selection mode for apply item
        //            selectionMode = SelectionMode.None;
        //            selectionModeId = -1;
        //        }
        //        else if (highlightedDialogBox != GameDialogBoxType.None && highlightedDialogBox != GameDialogBoxType.LockIconPanel && Display.Mouse.RightClick)
        //        {
        //            if (dialogBoxes[(GameDialogBoxType)highlightedDialogBox].Config.AlwaysVisible) // handle hud dialogs that cant be removed (except the lock)
        //            {
        //                if (!Cache.GameSettings.LockedDialogs) // if HUD is unlocked, allow toggle (HUD items dont disappear, just go transparent
        //                    if (dialogBoxes[(GameDialogBoxType)highlightedDialogBox].Config.Visible)
        //                        dialogBoxes[(GameDialogBoxType)highlightedDialogBox].Hide();
        //                    else dialogBoxes[(GameDialogBoxType)highlightedDialogBox].Show();
        //            }
        //            else
        //            {
        //                // close dialog special cases
        //                switch ((GameDialogBoxType)highlightedDialogBox)
        //                {
        //                    case GameDialogBoxType.PartyRequest: PartyRequestConfirmation(GameDialogBoxType.PartyRequest, MessageBoxResponse.No); break;
        //                    case GameDialogBoxType.TradeV2Confirmation: TradeRequestConfirmation(GameDialogBoxType.TradeV2Confirmation, MessageBoxResponse.No); break;
        //                    case GameDialogBoxType.TradeV2: EndTrade(TradeState.Cancelled); break;
        //                    default: dialogBoxes[(GameDialogBoxType)highlightedDialogBox].Hide(); break; // handle all other dialogs
        //                }
        //            }
        //            //Display.Mouse.RightClick = false;

        //            /* SAVE CHANGES TO DIALOG LOCATIONS */
        //            SaveDialogBoxConfiguration();
        //        }

        //        if (player.IsCasting)
        //        {
        //            player.SpellReady = player.IsCasting = false;
        //            player.Idle(player.Direction, false);
        //            Idle(player.Direction);
        //            player.SpellRequest = -1;
        //            AddEvent("Cancelled.");
        //        }
        //        else if (selectedObjectId != -1 && player.MoveReady && !player.IsDead)
        //        {
        //            switch (selectedObjectType)
        //            {
        //                case ObjectType.DynamicObject:
        //                    if (Cache.DynamicObjects.ContainsKey(selectedObjectId))
        //                    {
        //                        DynamicObject dynamic = Cache.DynamicObjects[selectedObjectId];
        //                        MotionDirection direction = Utility.GetNextDirection(Display.PlayerCenterX, Display.PlayerCenterY, Display.PlayerCenterX + (dynamic.X - player.X), Display.PlayerCenterY + (dynamic.Y - player.Y));

        //                        if (player.WithinRange(dynamic.X, dynamic.Y))
        //                        {
        //                            player.Direction = direction;

        //                            {
        //                                if (player.IsCombatMode)
        //                                    player.Attack(dynamic.X, dynamic.Y, (player.CriticalMode && player.Criticals > 0 ? AttackType.Critical : AttackType.Normal));
        //                                else player.Bow(direction);
        //                                Attack(direction, player.AttackType, dynamic.X, dynamic.Y);
        //                                if (player.AttackType == AttackType.Critical) player.Criticals--;
        //                            }
        //                            player.Commands++;
        //                        }
        //                        else idle = true;
        //                    }
        //                    else selectedObjectId = -1;
        //                    break;
        //                case ObjectType.DeadOwner:
        //                case ObjectType.Owner:
        //                    if (Cache.OwnerCache.ContainsKey((UInt16)selectedObjectId) && !Cache.OwnerCache[(UInt16)selectedObjectId].Location.Equals(player.Location))
        //                    {
        //                        IOwner owner = Cache.OwnerCache[(UInt16)selectedObjectId];
        //                        if (!owner.IsInvisible && (player.ForceAttack || (owner.GetRelationship(player.Side) == OwnerRelationShip.Enemy && !owner.IsDead)))
        //                        {
        //                            MotionDirection direction = Utility.GetNextDirection(Display.PlayerCenterX, Display.PlayerCenterY, Display.PlayerCenterX + (owner.X - player.X), Display.PlayerCenterY + (owner.Y - player.Y));

        //                            if (player.WithinRange(owner))
        //                                if (player.IsCombatMode)
        //                                {
        //                                    player.Direction = direction;
        //                                    player.Attack(owner.X, owner.Y, (player.CriticalMode && player.Criticals > 0 ? AttackType.Critical : AttackType.Normal));
        //                                    Attack(direction, player.AttackType, owner.X, owner.Y);
        //                                    if (player.AttackType == AttackType.Critical) player.Criticals--;
        //                                    player.Commands++;
        //                                }
        //                                else
        //                                {
        //                                    player.Bow(direction);
        //                                    Attack(direction, player.AttackType, owner.X, owner.Y);
        //                                    if (player.AttackType == AttackType.Critical) player.Criticals--;
        //                                    player.Commands++;
        //                                }
        //                            else idle = true;
        //                        }
        //                        else idle = true;
        //                    }
        //                    else selectedObjectId = -1;
        //                    break;
        //            }
        //        }
        //        else idle = true;

        //        if (idle)
        //            if (!player.IsCasting && !player.IsDead)
        //            {
        //                MotionDirection direction = Utility.GetNextDirection(Display.PlayerCenterX, Display.PlayerCenterY, (int)Display.Mouse.CellX, (int)Display.Mouse.CellY);

        //                if (Display.Mouse.LeftReleased && (player.LevelUpPoints == 0 || !levelUpRectangle.Contains(display.Mouse.Point)) && player.MoveReady && !player.IsMoving)
        //                {

        //                    player.DestinationX = Cache.MapView.PivotX + (((int)Display.Mouse.Position.X + player.OffsetX + display.GameBoardShiftX) / Globals.CellWidth) + Globals.OffScreenCells;
        //                    player.DestinationY = Cache.MapView.PivotY + (((int)Display.Mouse.Position.Y + player.OffsetY + display.GameBoardShiftY) / Globals.CellHeight) + Globals.OffScreenCells;

        //                    if (player.DestinationX == player.X && player.DestinationY == player.Y)
        //                    {
        //                        player.DestinationX = -1;
        //                        player.DestinationY = -1;
        //                    }

        //                    // fly during movement
        //                    if (player.Flying && player.CanMove(player.FlyDirection))
        //                    {
        //                        Fly(player.FlyDirection);
        //                        player.Fly(player.FlyDamage, player.FlyDirection);
        //                        Cache.MapView.Pan(player.FlyDirection);
        //                        player.SpellReady = player.IsCasting = false;
        //                        player.SpellRequest = -1;
        //                    }
        //                    else if (player.Location.IsTeleport)
        //                    {
        //                        player.Idle(player.Direction, true);
        //                        Idle(player.Direction);
        //                        Teleport();
        //                    }
        //                    else if (player.DestinationX != -1 && player.DestinationY != -1)
        //                    {
        //                        MotionDirection newDirection = FindNextMovableDirection(direction);

        //                        if (newDirection != MotionDirection.None)
        //                        {
        //                            Move(newDirection, player.IsRunning);

        //                            if (player.IsRunning) player.Run(newDirection);
        //                            else player.Walk(newDirection);
        //                            Cache.MapView.Pan(newDirection);
        //                        }
        //                        else if (player.Motion != MotionType.Idle)
        //                        {
        //                            player.Idle(player.Direction);
        //                            Idle(player.Direction);
        //                        }
        //                    }
        //                }

        //                else if ((direction == player.Direction && (player.MoveReady && player.Motion != MotionType.Idle)) ||
        //                    (direction != player.Direction && player.MoveReady) && direction != MotionDirection.None)
        //                {
        //                    player.Idle(direction);
        //                    Idle(player.Direction);
        //                    player.Commands++;
        //                }

        //                else if (player.CanHalt)
        //                {
        //                    player.HaltMovement = true;
        //                }
        //            }

        //        //Display.Mouse.RightHeld = false;
        //    }

        //    // fly from idle
        //    if (player.Flying && player.CanMove(player.FlyDirection) && !player.IsMoving)
        //    {
        //        Fly(player.FlyDirection);
        //        player.Fly(player.FlyDamage, player.FlyDirection);
        //        Cache.MapView.Pan(player.FlyDirection);
        //        player.SpellReady = player.IsCasting = false;
        //        player.SpellRequest = -1;
        //    }

        //    // cast from idle
        //    else if (player.SpellRequest != -1 && !player.IsCasting && !player.SpellReady && !player.IsMoving)
        //    {
        //        player.Idle(player.Direction, true);
        //        Idle(player.Direction);

        //        Cast(player.SpellRequest);
        //        player.Cast(player.SpellRequest);
        //        player.SpellRequest = -1;
        //    }

        //    /* NO CLICK - UNATTENDED MOVEMENT */
        //    else if (player.IsMoving && player.MoveReady && !player.IsDead)
        //    {
        //        if (player.X == player.DestinationX && player.Y == player.DestinationY)
        //        {
        //            player.DestinationX = -1;
        //            player.DestinationY = -1;
        //        }
        //        // fly during unattended movement
        //        if (player.Flying && player.CanMove(player.FlyDirection))
        //        {
        //            Fly(player.FlyDirection);
        //            player.Fly(player.FlyDamage, player.FlyDirection);
        //            Cache.MapView.Pan(player.FlyDirection);
        //            player.SpellReady = player.IsCasting = false;
        //            player.SpellRequest = -1;
        //        }
        //        else if (player.Location.IsTeleport)
        //        {
        //            player.Idle(player.Direction, true);
        //            Idle(player.Direction);
        //            Teleport();
        //        }
        //        else if (player.SpellRequest != -1 && !player.IsCasting && !player.SpellReady)
        //        {
        //            player.Idle(player.Direction, true);
        //            Idle(player.Direction);

        //            Cast(player.SpellRequest);
        //            player.Cast(player.SpellRequest);
        //            player.SpellRequest = -1;
        //        }
        //        else if (player.HaltMovement)
        //        {
        //            MotionDirection direction = Utility.GetNextDirection(Display.PlayerCenterX, Display.PlayerCenterY, (int)Display.Mouse.CellX, (int)Display.Mouse.CellY);

        //            if ((direction == player.Direction && (player.MoveReady && player.Motion != MotionType.Idle)) ||
        //                    (direction != player.Direction && player.MoveReady) && direction != MotionDirection.None)
        //            {
        //                player.Idle(direction);
        //                Idle(player.Direction);
        //                player.Commands++;
        //            }
        //            player.HaltMovement = false;
        //        }
        //        else if (player.DestinationX != -1 || player.DestinationY != -1)
        //        {
        //            if (player.X != player.DestinationX || player.Y != player.DestinationY)
        //            {
        //                MotionDirection newDirection = FindNextMovableDirection(Utility.GetNextDirection(player.X, player.Y, player.DestinationX, player.DestinationY));

        //                if (newDirection != MotionDirection.None)
        //                {
        //                    Move(newDirection, player.IsRunning);

        //                    if (player.IsRunning) player.Run(newDirection);
        //                    else player.Walk(newDirection);
        //                    Cache.MapView.Pan(newDirection);
        //                }
        //                else if (player.Motion != MotionType.Idle)
        //                {
        //                    player.Idle(player.Direction);
        //                    Idle(player.Direction);
        //                }
        //                else if (player.Motion != MotionType.Idle)
        //                {
        //                    player.Idle(player.Direction);
        //                    Idle(player.Direction);
        //                }

        //            }
        //        }
        //        else
        //        {
        //            player.Idle(player.Direction);
        //            Idle(player.Direction);
        //        }
        //    }

        //    // MOUSE UP
        //    if (Display.Mouse.LeftReleased && isActive)
        //    {
        //        /* SINGLE CLICK DIALOG BOX */
        //        if (clickedDialogBox != GameDialogBoxType.None && dialogBoxes.ContainsKey(clickedDialogBox))
        //            switch (clickedDialogBox) //If dialogbox activates click() during mouse down event and do rest of code here else activate click() 
        //            {
        //                case GameDialogBoxType.SpellBook:
        //                    switch (highlightedDialogBox)
        //                    {
        //                        case GameDialogBoxType.HotBar:
        //                            if (dialogBoxes[clickedDialogBox].ClickedItemIndex != -1)
        //                            {
        //                                // clear existing hotkey at dropped location
        //                                if (Cache.HotBarConfiguration.ContainsKey(dialogBoxes[highlightedDialogBox].SelectedItemIndex))
        //                                    Cache.HotBarConfiguration.Remove(dialogBoxes[highlightedDialogBox].SelectedItemIndex);

        //                                HotBarIcon icon = new HotBarIcon(dialogBoxes[clickedDialogBox].ClickedItemIndex);
        //                                icon.Type = HotBarType.Spell;
        //                                Cache.HotBarConfiguration.Add(dialogBoxes[highlightedDialogBox].SelectedItemIndex, icon);

        //                                SaveHotKeyConfiguration();

        //                                dialogBoxes[highlightedDialogBox].ClickedItemIndex = -1;
        //                                dialogBoxes[clickedDialogBox].ClickedItemIndex = -1;
        //                                DoHotKeyAction(HotKeyActions.CancelAction);
        //                            }
        //                            break;
        //                        case GameDialogBoxType.SpellBook: //Dont hide when self drag
        //                            if (dialogBoxes[clickedDialogBox].ClickedItemIndex != -1)
        //                            {
        //                                dialogBoxes[clickedDialogBox].Hide();
        //                            }
        //                            break;
        //                        default: dialogBoxes[clickedDialogBox].Hide(); break;
        //                    }
        //                    break;
        //                case GameDialogBoxType.HotBar:
        //                    switch (highlightedDialogBox)
        //                    {
        //                        case GameDialogBoxType.HotBar:
        //                            int selectedItemIndex = dialogBoxes[highlightedDialogBox].HighlightedItemIndex;
        //                            int clickedItemIndex = dialogBoxes[clickedDialogBox].ClickedItemIndex;
        //                            if (selectedItemIndex != -1 && clickedItemIndex != -1) //Hover over a location
        //                            {
        //                                if (selectedItemIndex != clickedItemIndex) //Do action
        //                                {
        //                                    if (Cache.HotBarConfiguration.ContainsKey(dialogBoxes[highlightedDialogBox].ClickedItemIndex))
        //                                    {
        //                                        HotBarIcon tempIcon = null;
        //                                        // if hotbaricon at destination, remove
        //                                        if (Cache.HotBarConfiguration.ContainsKey(dialogBoxes[highlightedDialogBox].SelectedItemIndex))
        //                                        {
        //                                            tempIcon = Cache.HotBarConfiguration[dialogBoxes[highlightedDialogBox].SelectedItemIndex];
        //                                            Cache.HotBarConfiguration.Remove(dialogBoxes[highlightedDialogBox].SelectedItemIndex);
        //                                        }

        //                                        // set hotbar icon at destination
        //                                        //HotBarIcon icon = Cache.HotBarConfiguration[dialogBoxes[highlightedDialogBox].ClickedItemIndex];
        //                                        //Cache.HotBarConfiguration.Add(dialogBoxes[highlightedDialogBox].SelectedItemIndex, icon);

        //                                        // swap or remove hotbar icon from source
        //                                        if (tempIcon != null) { Cache.HotBarConfiguration[dialogBoxes[clickedDialogBox].ClickedItemIndex] = tempIcon; }
        //                                        else { Cache.HotBarConfiguration.Remove(dialogBoxes[clickedDialogBox].ClickedItemIndex); }

        //                                        SaveHotKeyConfiguration();
        //                                    }
        //                                }
        //                            }
        //                            break;
        //                        default:
        //                            if (Cache.HotBarConfiguration.ContainsKey(dialogBoxes[clickedDialogBox].ClickedItemIndex))
        //                            {
        //                                Cache.HotBarConfiguration.Remove(dialogBoxes[clickedDialogBox].ClickedItemIndex);
        //                                SaveHotKeyConfiguration();
        //                            }
        //                            break;
        //                    }
        //                    dialogBoxes[clickedDialogBox].ClickedItemIndex = -1;
        //                    break;
        //                default: dialogBoxes[clickedDialogBox].LeftClick(); break;
        //            }
        //        /* CLICK NPC TO OPEN DIALOGS */
        //        else if (selectedObjectId != -1 && player.MoveReady && !player.IsDead)
        //        {
        //            switch (selectedObjectType)
        //            {
        //                case ObjectType.Owner:
        //                case ObjectType.DeadOwner:
        //                    if (Cache.OwnerCache.ContainsKey((UInt16)selectedObjectId) && !Cache.OwnerCache[(UInt16)selectedObjectId].Location.Equals(player.Location))
        //                    {
        //                        IOwner owner = Cache.OwnerCache[(UInt16)selectedObjectId];

        //                        if (owner.MerchantType != MerchantType.None)
        //                        {
        //                            switch (owner.MerchantType)
        //                            {
        //                                case MerchantType.General:
        //                                case MerchantType.Blacksmith:
        //                                    ((MerchantDialogBox)dialogBoxes[GameDialogBoxType.Merchant]).MerchantId = owner.MerchantId;
        //                                    dialogBoxes[GameDialogBoxType.Merchant].Show();
        //                                    break;
        //                                case MerchantType.Warehouse:
        //                                    dialogBoxes[GameDialogBoxType.WarehouseV2].Show();
        //                                    break;
        //                                //case MerchantType.MagicShop:
        //                                //    dialogBoxes[GameDialogBoxType.MagicShop].Show();
        //                                //break;
        //                            }
        //                        }
        //                        break;
        //                    }
        //                    break;
        //            }
        //        }

        //        if (clickedDialogBox != GameDialogBoxType.None)
        //        {
        //            /* DOUBLE CLICK */
        //            if (!Display.Mouse.IsDragging)
        //            {
        //                if ((DateTime.Now - Display.Mouse.LeftClickTime).TotalMilliseconds < 300)
        //                    if (selectionMode == SelectionMode.ApplyItem)
        //                    {
        //                        switch (clickedDialogBox)
        //                        {
        //                            //case GameDialogBoxType.CharacterV1:
        //                            case GameDialogBoxType.InventoryV1:
        //                            case GameDialogBoxType.CharacterV2:
        //                                if (dialogBoxes[clickedDialogBox].SelectedItemIndex != -1)
        //                                    ApplyItem(selectionModeId, dialogBoxes[clickedDialogBox].SelectedItemIndex);
        //                                break;
        //                        }
        //                        //clear selection mode for apply item
        //                        selectionMode = SelectionMode.None;
        //                        selectionModeId = -1;
        //                    }
        //                    else
        //                    {
        //                        switch (clickedDialogBox)
        //                        {
        //                            //case GameDialogBoxType.CharacterV1:
        //                            case GameDialogBoxType.InventoryV1:
        //                            case GameDialogBoxType.CharacterV2:
        //                                // special case - add sell item if merchant window open + in sell state
        //                                if (dialogBoxes[clickedDialogBox].SelectedItemIndex != -1)
        //                                {
        //                                    if (dialogBoxes[GameDialogBoxType.Merchant].Config.Visible)
        //                                        switch (dialogBoxes[GameDialogBoxType.Merchant].Config.State)
        //                                        {
        //                                            case DialogBoxState.Sell: AddSellItem(dialogBoxes[clickedDialogBox].SelectedItemIndex); break;
        //                                            case DialogBoxState.Repair: AddRepairItem(dialogBoxes[clickedDialogBox].SelectedItemIndex); break;
        //                                        }
        //                                    else if (dialogBoxes[GameDialogBoxType.TradeV2].Config.Visible)
        //                                        AddTradeItem(dialogBoxes[clickedDialogBox].SelectedItemIndex);
        //                                    else if (dialogBoxes[GameDialogBoxType.WarehouseV2].Config.Visible)
        //                                        ItemToWarehouse(dialogBoxes[clickedDialogBox].SelectedItemIndex, -1);
        //                                    else if (dialogBoxes[GameDialogBoxType.UpgradeV2].Config.Visible)
        //                                        ChangeUpgradeItem(dialogBoxes[clickedDialogBox].SelectedItemIndex);
        //                                    else UseItem(dialogBoxes[clickedDialogBox].SelectedItemIndex);
        //                                }
        //                                break;
        //                            case GameDialogBoxType.UpgradeV2:
        //                                if (dialogBoxes[clickedDialogBox].SelectedItemIndex != -1) //Use selected here?
        //                                {
        //                                    switch (dialogBoxes[clickedDialogBox].Config.State)
        //                                    {
        //                                        case DialogBoxState.Success:
        //                                        case DialogBoxState.Fail: break;
        //                                        default:
        //                                            {
        //                                                ChangeUpgradeItem(dialogBoxes[clickedDialogBox].SelectedItemIndex);
        //                                                break;
        //                                            }
        //                                    }
        //                                }
        //                                break;
        //                            case GameDialogBoxType.MiniMap:

        //                                break;
        //                            case GameDialogBoxType.WarehouseV2:
        //                                if (dialogBoxes[clickedDialogBox].SelectedItemIndex != -1)
        //                                {
        //                                    if (dialogBoxes[GameDialogBoxType.CharacterV2].Config.Visible || dialogBoxes[GameDialogBoxType.InventoryV1].Config.Visible)
        //                                        ItemFromWarehouse(dialogBoxes[clickedDialogBox].SelectedItemIndex, -1);
        //                                }
        //                                break;
        //                        }
        //                    }
        //                else
        //                {
        //                    Display.Mouse.LeftClickTime = DateTime.Now;

        //                    /* SAVE CHANGES TO DIALOG LOCATIONS */
        //                    SaveDialogBoxConfiguration();
        //                }
        //            }


        //            /* DROP THE DRAGGING ITEM */
        //            if (dialogBoxes.ContainsKey(clickedDialogBox) &&
        //                dialogBoxes[clickedDialogBox].ClickedItemIndex != -1 && !player.IsDead)
        //            {
        //                switch (clickedDialogBox)
        //                {
        //                    case GameDialogBoxType.WarehouseV2:
        //                        switch (highlightedDialogBox)
        //                        {
        //                            case GameDialogBoxType.None: // TODO drop item from warehouse
        //                                break;
        //                            case GameDialogBoxType.InventoryV1:
        //                            case GameDialogBoxType.CharacterV2:
        //                                if (dialogBoxes[clickedDialogBox].ClickedItemIndex != -1)
        //                                    ItemFromWarehouse(dialogBoxes[clickedDialogBox].ClickedItemIndex, dialogBoxes[highlightedDialogBox].HighlightedItemIndex);
        //                                dialogBoxes[clickedDialogBox].HighlightedItemIndex = -1;
        //                                break;
        //                            case GameDialogBoxType.WarehouseV2:
        //                                if (dialogBoxes[clickedDialogBox].ClickedItemIndex != dialogBoxes[highlightedDialogBox].HighlightedItemIndex)
        //                                    SwapWarehouseItem(dialogBoxes[clickedDialogBox].ClickedItemIndex, dialogBoxes[highlightedDialogBox].HighlightedItemIndex);
        //                                dialogBoxes[clickedDialogBox].HighlightedItemIndex = -1;
        //                                break;
        //                        }

        //                        dialogBoxes[clickedDialogBox].SelectedItemIndexOffsetX = 0;
        //                        dialogBoxes[clickedDialogBox].SelectedItemIndexOffsetY = 0;
        //                        break;
        //                    case GameDialogBoxType.InventoryV1:
        //                    case GameDialogBoxType.CharacterV2:
        //                        switch (highlightedDialogBox)
        //                        {
        //                            case GameDialogBoxType.None: // no destination dialog box
        //                                // check for player highlight (not self)
        //                                if (Cache.OwnerCache.ContainsKey((UInt16)selectedObjectId) && !Cache.OwnerCache[(UInt16)selectedObjectId].Location.Equals(player.Location))
        //                                {
        //                                    IOwner owner = Cache.OwnerCache[(UInt16)selectedObjectId];
        //                                    if (owner.OwnerType == OwnerType.Player)
        //                                    {
        //                                        AddEvent(string.Format("Requesting trade with {0}...", owner.Name));
        //                                        RequestTrade(owner.ObjectId, dialogBoxes[clickedDialogBox].ClickedItemIndex);
        //                                    }
        //                                }
        //                                else DropItem(dialogBoxes[clickedDialogBox].ClickedItemIndex);
        //                                dialogBoxes[clickedDialogBox].HighlightedItemIndex = -1;
        //                                break;
        //                            case GameDialogBoxType.CharacterV2: // moving items around the dialog box //TODO - create a general equiplocation?
        //                                if (dialogBoxes[clickedDialogBox].ClickedItemIndex != dialogBoxes[highlightedDialogBox].HighlightedItemIndex)
        //                                    SwapItem(dialogBoxes[clickedDialogBox].ClickedItemIndex, dialogBoxes[highlightedDialogBox].HighlightedItemIndex);
        //                                dialogBoxes[clickedDialogBox].HighlightedItemIndex = -1;
        //                                break;
        //                            case GameDialogBoxType.Merchant:
        //                                if (dialogBoxes[clickedDialogBox].ClickedItemIndex != -1)
        //                                    switch (dialogBoxes[GameDialogBoxType.Merchant].Config.State)
        //                                    {
        //                                        case DialogBoxState.Sell: AddSellItem(dialogBoxes[clickedDialogBox].ClickedItemIndex); break;
        //                                        case DialogBoxState.Repair: AddRepairItem(dialogBoxes[clickedDialogBox].ClickedItemIndex); break;
        //                                    }
        //                                dialogBoxes[clickedDialogBox].HighlightedItemIndex = -1;
        //                                break;
        //                            case GameDialogBoxType.TradeV2:
        //                                if (dialogBoxes[clickedDialogBox].ClickedItemIndex != -1) AddTradeItem(dialogBoxes[clickedDialogBox].ClickedItemIndex);
        //                                dialogBoxes[clickedDialogBox].HighlightedItemIndex = -1;
        //                                break;
        //                            case GameDialogBoxType.UpgradeV2:
        //                                if (dialogBoxes[clickedDialogBox].ClickedItemIndex != -1)
        //                                {
        //                                    ChangeUpgradeItem(dialogBoxes[clickedDialogBox].ClickedItemIndex);
        //                                    //dialogBoxes[clickedDialogBox].ClickedItemIndex = -1;
        //                                }
        //                                dialogBoxes[clickedDialogBox].HighlightedItemIndex = -1;
        //                                break;
        //                            case GameDialogBoxType.WarehouseV2:
        //                                if (dialogBoxes[clickedDialogBox].ClickedItemIndex != -1)
        //                                    ItemToWarehouse(dialogBoxes[clickedDialogBox].ClickedItemIndex, dialogBoxes[highlightedDialogBox].HighlightedItemIndex);
        //                                dialogBoxes[clickedDialogBox].HighlightedItemIndex = -1;
        //                                break;
        //                        }

        //                        if (clickedDialogBox == GameDialogBoxType.InventoryV1)
        //                            if (highlightedDialogBox == GameDialogBoxType.InventoryV1)
        //                            {
        //                                // HOLD SHIFT TO SET LOCATION OF ALL RELATED ITEMS
        //                                if (display.Keyboard.LeftShift)
        //                                {
        //                                    for (int i = Globals.MaximumEquipment; i < Globals.MaximumTotalItems; i++)
        //                                        if (player.Inventory[i] != null && player.Inventory[i].ItemId == player.Inventory[dialogBoxes[clickedDialogBox].ClickedItemIndex].ItemId)
        //                                        {
        //                                            player.Inventory[i].BagX = player.Inventory[dialogBoxes[clickedDialogBox].ClickedItemIndex].BagX;
        //                                            player.Inventory[i].BagY = player.Inventory[dialogBoxes[clickedDialogBox].ClickedItemIndex].BagY;
        //                                            SetBagLocation(i); // update server
        //                                        }
        //                                }
        //                                else SetBagLocation(dialogBoxes[clickedDialogBox].ClickedItemIndex); // update server
        //                            }
        //                            else if (highlightedDialogBox != GameDialogBoxType.None)
        //                            {
        //                                Item movedItem = player.Inventory[dialogBoxes[clickedDialogBox].ClickedItemIndex];
        //                                movedItem.BagX = player.Inventory[dialogBoxes[clickedDialogBox].ClickedItemIndex].BagOldX;
        //                                movedItem.BagY = player.Inventory[dialogBoxes[clickedDialogBox].ClickedItemIndex].BagOldY;
        //                            }

        //                        dialogBoxes[clickedDialogBox].SelectedItemIndexOffsetX = 0;
        //                        dialogBoxes[clickedDialogBox].SelectedItemIndexOffsetY = 0;
        //                        break;
        //                    case GameDialogBoxType.UpgradeV2:
        //                        switch (highlightedDialogBox)
        //                        {
        //                            case GameDialogBoxType.UpgradeV2: { break; }
        //                            default: { ChangeUpgradeItem(dialogBoxes[clickedDialogBox].ClickedItemIndex); break; }
        //                        }

        //                        dialogBoxes[clickedDialogBox].SelectedItemIndexOffsetX = 0;
        //                        dialogBoxes[clickedDialogBox].SelectedItemIndexOffsetY = 0;
        //                        break;
        //                    //case GameDialogBoxType.HotBar:
        //                    //    switch (highlightedDialogBox)
        //                    //    {
        //                    //        case GameDialogBoxType.HotBar:
        //                    //            int selectedItemIndex = dialogBoxes[highlightedDialogBox].SelectedItemIndex;
        //                    //            int clickedItemIndex = dialogBoxes[clickedDialogBox].ClickedItemIndex;
        //                    //            if (selectedItemIndex != -1)
        //                    //            {
        //                    //                if (selectedItemIndex == clickedItemIndex) //Do action
        //                    //                {
        //                    //                    dialogBoxes[clickedDialogBox].LeftClick();
        //                    //                    clickedDialogBox = GameDialogBoxType.HotBar;
        //                    //                }
        //                    //                else //Swap icon locations
        //                    //                {
        //                    //                    if (Cache.HotBarConfiguration.ContainsKey(dialogBoxes[highlightedDialogBox].ClickedItemIndex))
        //                    //                    {
        //                    //                        HotBarIcon tempIcon = null;
        //                    //                        // if hotbaricon at destination, remove
        //                    //                        if (Cache.HotBarConfiguration.ContainsKey(dialogBoxes[highlightedDialogBox].SelectedItemIndex))
        //                    //                        {
        //                    //                            tempIcon = Cache.HotBarConfiguration[dialogBoxes[highlightedDialogBox].SelectedItemIndex];
        //                    //                            Cache.HotBarConfiguration.Remove(dialogBoxes[highlightedDialogBox].SelectedItemIndex);
        //                    //                        }

        //                    //                        // set hotbar icon at destination
        //                    //                        HotBarIcon icon = Cache.HotBarConfiguration[dialogBoxes[highlightedDialogBox].ClickedItemIndex];
        //                    //                        Cache.HotBarConfiguration.Add(dialogBoxes[highlightedDialogBox].SelectedItemIndex, icon);

        //                    //                        // swap or remove hotbar icon from source
        //                    //                        if (tempIcon != null) { Cache.HotBarConfiguration[dialogBoxes[clickedDialogBox].ClickedItemIndex] = tempIcon; }
        //                    //                        else { Cache.HotBarConfiguration.Remove(dialogBoxes[clickedDialogBox].ClickedItemIndex); }

        //                    //                        SaveHotKeyConfiguration();
        //                    //                    }
        //                    //                }
        //                    //            }
        //                    //            break;
        //                    //        default:
        //                    //        case GameDialogBoxType.None: // remove hotbaricon
        //                    //            if (Cache.HotBarConfiguration.ContainsKey(dialogBoxes[clickedDialogBox].ClickedItemIndex))
        //                    //            {
        //                    //                Cache.HotBarConfiguration.Remove(dialogBoxes[clickedDialogBox].ClickedItemIndex);
        //                    //                SaveHotKeyConfiguration();
        //                    //            }
        //                    //            break;
        //                    //    }

        //                    //    dialogBoxes[clickedDialogBox].SelectedItemIndex = -1;                             
        //                    //    dialogBoxes[clickedDialogBox].SelectedItemIndexOffsetX = 0;                           
        //                    //    dialogBoxes[clickedDialogBox].SelectedItemIndexOffsetY = 0;
        //                    //    break;
        //                }
        //                dialogBoxes[clickedDialogBox].ClickedItemIndex = -1;
        //            }
        //            clickedDialogBox = GameDialogBoxType.None;
        //        }

        //        /* LEVEL UP CLICK */
        //        if (player.LevelUpPoints > 0)
        //            if (levelUpRectangle.Contains(display.Mouse.Point))
        //            {
        //                if (dialogBoxes[GameDialogBoxType.LevelUpV1].Config.Visible)
        //                {
        //                    dialogBoxes[GameDialogBoxType.LevelUpV1].Toggle();
        //                }
        //                else
        //                {
        //                    player.RemainderLevelUpPoints = player.LevelUpPoints;
        //                    player.LevelUpStrength = player.LevelUpDexterity = player.LevelUpMagic =
        //                        player.LevelUpIntelligence = player.LevelUpVitality = player.LevelUpAgility = 0;

        //                    dialogBoxes[GameDialogBoxType.LevelUpV1].Show();
        //                    BringToFront(GameDialogBoxType.LevelUpV1);
        //                }

        //                /* SAVE CHANGES TO DIALOG LOCATIONS */
        //                SaveDialogBoxConfiguration();
        //            }

        //        //Display.Mouse.LeftClick = false;
        //    }

        //    /* MOUSE SCROLL */
        //    if (Display.Mouse.Scroll != 0 && isActive)
        //    {
        //        topdialogBox = dialogBoxDrawOrder.Last();
        //        if (dialogBoxes.ContainsKey(topdialogBox) && dialogBoxes[topdialogBox].Config.Visible)
        //            dialogBoxes[topdialogBox].Scroll(Display.Mouse.Scroll);
        //        //Display.Mouse.Scroll = 0;
        //    }
        //}

        public void HandleKeyboard(bool isActive)
        {
            if (!isActive) return;

            List<Keys> pressedKeysList = Display.Keyboard.NewlyPressedKeys.ToList(); //Only keys that have been pressed during the last update
            List<Keys> heldKeysList = Display.Keyboard.HeldKeys.ToList(); //Only keys that have been held down, not including new keys pressed this update

            if (pressedKeysList.Count > 0 || heldKeysList.Count > 0 || Display.Keyboard.HeldKeyCheck.Count > 0) // check there are 1 or more keys down or in HeldKeycheck
               
                if (pressedKeysList.Count == 0) { pressedKeysList.Add(Keys.None);}
           
                if (heldKeysList.Count == 0) { heldKeysList.Add(Keys.None); }

                foreach (KeyValuePair<Keys[], HotKeyActions> hotKeys in Hotkeys) // iterate through the hotkey configuration
                {
                    List<Keys> keyList = hotKeys.Key.ToList(); // we need to get a list to remove the "None" before the check
                    if (keyList[0] == Keys.None) //Held down actions should trigger twice, once for the button press and once for the button release
                    {
                        if (pressedKeysList.Contains(keyList[1]) && !Display.Keyboard.HeldKeyCheck.Contains(keyList[1])) //checks for key and if it has a keycheck 
                        {
                            DoHotKeyAction(hotKeys.Value); //does hotkeyaction
                            Display.Keyboard.HeldKeyCheck.Add(keyList[1]); //adds value to keycheck so no repeating calls
                        }

                        if (Display.Keyboard.IsNewKeyRelease(keyList[1])) //checks for keyrelease 
                        {
                            DoHotKeyAction(hotKeys.Value); //does hotkey action
                            Display.Keyboard.HeldKeyCheck.Remove(keyList[1]); //removes keycheck so it can be called again
                        }
                    }
                    else
                    if (pressedKeysList.Contains(keyList[0]) && heldKeysList.Contains(keyList[1])) // Handles Presses and allows for modifiers(heldkeys)
                    {
                        DoHotKeyAction(hotKeys.Value);
                    }               
                }
        }

        public void DoHotKeyAction(HotKeyActions action)
        {
            if (!chatMode || action == HotKeyActions.ChatMode)
            {
                int spellToCast = -1;
                switch (action)
                {
                    default: break;
                    case HotKeyActions.JoinParty:
                        if (selectedObjectId != -1 && selectedObjectType == ObjectType.Owner)
                        {
                            IOwner owner = Cache.OwnerCache[(UInt16)selectedObjectId];
                            if (owner.OwnerType == OwnerType.Player)
                            {
                                JoinParty();
                            }
                        }
                        break;
                    case HotKeyActions.InformationScreen: { dialogBoxes[GameDialogBoxType.Information].Toggle(); break; }
                    case HotKeyActions.ActivateAbility: //Use Activation on Item
                        if (player.SpecialAbilityReady)
                        {
                            ItemSpecialAbilityType activation = ItemSpecialAbilityType.None;

                            if (player.SpecialAbilitiesActive != null && player.SpecialAbilitiesActive.Count > 0)
                            {
                                if (player.SpecialAbilitiesActive.ContainsKey(ItemSpecialAbilityType.MedusaWeapon)) activation = ItemSpecialAbilityType.MedusaWeapon;
                                else if (player.SpecialAbilitiesActive.ContainsKey(ItemSpecialAbilityType.XelimaWeapon)) activation = ItemSpecialAbilityType.XelimaWeapon;
                                else if (player.SpecialAbilitiesActive.ContainsKey(ItemSpecialAbilityType.IceWeapon)) activation = ItemSpecialAbilityType.IceWeapon;
                                else if (player.SpecialAbilitiesActive.ContainsKey(ItemSpecialAbilityType.MerienShield)) activation = ItemSpecialAbilityType.MerienShield;
                                else if (player.SpecialAbilitiesActive.ContainsKey(ItemSpecialAbilityType.MerienArmour)) activation = ItemSpecialAbilityType.MerienArmour;

                                if (activation != ItemSpecialAbilityType.None)
                                {
                                    ActivateAbility(activation);
                                    AddEvent("Special" + activation.ToString() + "ability activated!");
                                    player.SpecialAbilityReady = false;
                                }

                                //foreach (KeyValuePair<ItemSpecialAbilityType, int> type in player.SpecialAbilitiesActive)
                                //{
                                //    if(!activated)
                                //    {
                                //        switch (type.Key)
                                //        {
                                //            case ItemSpecialAbilityType.None: break;
                                //            case ItemSpecialAbilityType.IceWeapon: //TODO sort in order of best to worst?
                                //            case ItemSpecialAbilityType.MedusaWeapon:
                                //            case ItemSpecialAbilityType.MerienArmour:
                                //            case ItemSpecialAbilityType.MerienShield:
                                //            case ItemSpecialAbilityType.XelimaWeapon:
                                //                {
                                //                    ActivateAbility(type.Key); 
                                //                    AddEvent("Special" + type.Key.ToString() + "ability activated!");
                                //                    player.SpecialAbilityReady = false;
                                //                    activated = true;
                                //                    break;
                                //                }
                                //        }
                                //    }
                                //}  
                            }                           
                            else AddEvent("No item with special ability is equipped.");
                        }
                        else AddEvent("Special ability is not ready yet");
                        break;
                    case HotKeyActions.ActivateIce:
                        {
                            if (player.SpecialAbilityReady)
                            {
                                if (player.SpecialAbilitiesActive != null && player.SpecialAbilitiesActive.Count > 0 && player.SpecialAbilitiesActive.ContainsKey(ItemSpecialAbilityType.IceWeapon))
                                {
                                    ActivateAbility(ItemSpecialAbilityType.IceWeapon);
                                    AddEvent("Ice weapon ability activated!");
                                    player.SpecialAbilityReady = false;
                                    break;
                                }
                                else AddEvent("No item with ice weapon ability is equipped.");
                            }
                            else AddEvent("Special ability is not ready yet");
                            break;
                        }
                    case HotKeyActions.ActivateMedusa:
                        {
                            if (player.SpecialAbilityReady)
                            {
                                if (player.SpecialAbilitiesActive != null && player.SpecialAbilitiesActive.Count > 0 && player.SpecialAbilitiesActive.ContainsKey(ItemSpecialAbilityType.MedusaWeapon))
                                {
                                    ActivateAbility(ItemSpecialAbilityType.MedusaWeapon);
                                    AddEvent("Medusa weapon ability activated!");
                                    player.SpecialAbilityReady = false;
                                    break;
                                }
                                else AddEvent("No item with medusa weapon ability is equipped.");
                            }
                            else AddEvent("Special ability is not ready yet");
                            break;
                        }
                    case HotKeyActions.ActivateXelima:
                        {
                            if (player.SpecialAbilityReady)
                            {
                                if (player.SpecialAbilitiesActive != null && player.SpecialAbilitiesActive.Count > 0 && player.SpecialAbilitiesActive.ContainsKey(ItemSpecialAbilityType.XelimaWeapon))
                                {
                                    ActivateAbility(ItemSpecialAbilityType.XelimaWeapon);
                                    AddEvent("Xelima weapon ability activated!");
                                    player.SpecialAbilityReady = false;
                                    break;
                                }
                                else AddEvent("No item with xelima weapon ability is equipped.");
                            }
                            else AddEvent("Special ability is not ready yet");
                            break;
                        }
                    case HotKeyActions.ActivateMerienArmour:
                        {
                            if (player.SpecialAbilityReady)
                            {
                                if (player.SpecialAbilitiesActive != null && player.SpecialAbilitiesActive.Count > 0 && player.SpecialAbilitiesActive.ContainsKey(ItemSpecialAbilityType.MerienArmour))
                                {
                                    ActivateAbility(ItemSpecialAbilityType.MerienArmour);
                                    AddEvent("Merien armour ability activated!");
                                    player.SpecialAbilityReady = false;
                                    break;
                                }
                                else AddEvent("No item with merien armour ability is equipped.");
                            }
                            else AddEvent("Special ability is not ready yet");
                            break;
                        }
                    case HotKeyActions.ActivateMerienShield:
                        {
                            if (player.SpecialAbilityReady)
                            {
                                if (player.SpecialAbilitiesActive != null && player.SpecialAbilitiesActive.Count > 0 && player.SpecialAbilitiesActive.ContainsKey(ItemSpecialAbilityType.MerienShield))
                                {
                                    ActivateAbility(ItemSpecialAbilityType.MerienShield);
                                    AddEvent("Merien shield ability activated!");
                                    player.SpecialAbilityReady = false;
                                    break;
                                }
                                else AddEvent("No item with merien shield ability is equipped.");
                            }
                            else AddEvent("Special ability is not ready yet");
                            break;
                        }
                    case HotKeyActions.Attack: //New* Doing a physical attack such as swing sword/weapons, alternative to left clicking on the mouse
                        {
                            if (selectedObjectId != -1)
                                switch (selectedObjectType)
                                {
                                    case ObjectType.Owner:
                                    case ObjectType.DeadOwner:
                                        if (Cache.OwnerCache.ContainsKey((UInt16)selectedObjectId) && !Cache.OwnerCache[(UInt16)selectedObjectId].Location.Equals(player.Location))
                                        {
                                            IOwner owner = Cache.OwnerCache[(UInt16)selectedObjectId];
                                            if (player.WithinRange(owner))
                                            {
                                                MotionDirection direction = Utility.GetNextDirection(Display.PlayerCenterX, Display.PlayerCenterY, (int)Display.Mouse.CellX, (int)Display.Mouse.CellY);
                                                player.Direction = direction;

                                                {
                                                    if (player.IsCombatMode)
                                                        player.Attack(owner.X, owner.Y, (player.CriticalMode && player.Criticals > 0 ? AttackType.Critical : AttackType.Normal));
                                                    else player.Bow(direction);
                                                    Attack(direction, player.AttackType, owner.X, owner.Y);
                                                    if (player.AttackType == AttackType.Critical) player.Criticals--;
                                                }
                                            }
                                        }
                                        break;
                                }
                            break;
                        }
                    case HotKeyActions.AttackMode: //Changes the Character between peace and attack mode
                        {
                            ToggleCombatMode();
                            break;
                        }
                    case HotKeyActions.AutoAttack: //Enables auto attack on friendly and neutral players and monsters. Must be disabled when talking to NPC’s. Also allows you to mine and farm with out having to hold down the control button.
                    case HotKeyActions.AutoAttackPlayer: //Ability to auto attack enemy players.
                    case HotKeyActions.AutoRun: //For switching between run and walk mode
                        {
                            if (!player.RunningMode)
                            {
                                if (player.SP > 0)
                                {
                                    player.RunningMode = player.IsRunning = true;
                                    AddEvent("Running Mode ON");
                                }
                            }
                            else
                            {
                                player.RunningMode = player.IsRunning = false;
                                AddEvent("Running Mode OFF");
                            }
                            break;
                        }
                    case HotKeyActions.CancelAction: //Cancel Spell/ChatMode/logout
                        {
                            if (player.IsCasting) //TODO make visual transition smoother when canceling from a spell
                            {
                                player.SpellReady = player.IsCasting = false;
                                player.Idle(player.Direction, false);
                                Idle(player.Direction);
                                player.SpellRequest = -1;
                                AddEvent("Cancelled.");
                            }
                            if (chatMode) chatMode = false;
                            if (Cache.GameSettings.IsLoggingOut)
                            {
                                AddEvent("Logout count stopped.");
                                Cache.GameSettings.IsLoggingOut = false;
                            }
                            break;
                        }
                    case HotKeyActions.CharacterScreen:
                        dialogBoxes[GameDialogBoxType.CharacterV2].Toggle();

                        /* SAVE CHANGES TO DIALOG LOCATIONS */
                        SaveDialogBoxConfiguration();
                        break;
                    case HotKeyActions.ChatHistoryScreen:
                        dialogBoxes[GameDialogBoxType.Chat].Toggle();

                        /* SAVE CHANGES TO DIALOG LOCATIONS */
                        SaveDialogBoxConfiguration();
                        break;
                    case HotKeyActions.ChatMode:
                        {
                            if (!chatMode)
                            {
                                chatMode = true;
                                chatBox.Text = "";
                                Display.Keyboard.Dispatcher.Subscriber = chatBox; //TODO resolution resize for chat box
                            }
                            else
                            {
                                chatMode = false;
                                if (chatBox.Text.Length > 0)
                                {
                                    Chat(chatBox.Text);
                                    if (chatBoxHistory.Count > 0) // check for existing chat history. dont store repeated chat                  
                                    {
                                        if (!chatBoxHistory[chatBoxHistory.Count - 1].Equals(chatBox.Text))
                                        {
                                            chatBoxHistory.Add(chatBox.Text);
                                            chatBoxHistoryIndex = chatBoxHistory.Count - 1;
                                        }
                                    }
                                    else
                                    {
                                        chatBoxHistory.Add(chatBox.Text);
                                        chatBoxHistoryIndex = chatBoxHistory.Count - 1;
                                    }
                                }
                            }
                            break;
                        }
                    case HotKeyActions.CriticalAttack: //TODO disable if reaches 0 crits while holding down
                        {
                            if (!player.CriticalMode && player.Criticals > 0)
                            {
                                player.CriticalMode = true;
                            }
                            else
                            {
                                player.CriticalMode = false;
                            }
                            break;
                        }
                    case HotKeyActions.DashAttack: break;
                    case HotKeyActions.DebugMode:
                        {
                            if (!display.DebugMode)
                            {
                                display.DebugMode = true;
                                AddEvent("Debug Mode ON");
                            }
                            else
                            {
                                display.DebugMode = false;
                                AddEvent("Debug Mode OFF");
                            }
                            break;
                        }
                    case HotKeyActions.DetailMode:
                        {
                            int mode = ((int)Cache.GameSettings.DetailMode) + 1;
                            if (mode > 2) mode = 0;

                            ChangeDetailMode((GraphicsDetail)mode);
                            break;
                        }
                    case HotKeyActions.ForceAttack:
                        {
                            if (!player.ForceAttack)
                            {
                                player.ForceAttack = true;
                            }
                            else
                            {
                                player.ForceAttack = false;
                            }
                            break;
                        }
                    case HotKeyActions.FullScreenMode:
                        {
                            if (!fullScreenMode)
                            {
                                fullScreenMode = true;
                                Display.ToggleFullScreen();
                                Cache.GameSettings.FullScreen = display.FullScreen;
                                AddEvent("Full Screen Mode ON");
                            }
                            else
                            {
                                fullScreenMode = false;
                                Display.ToggleFullScreen();
                                Cache.GameSettings.FullScreen = display.FullScreen;
                                AddEvent("Full Screen OFF");
                            }
                            break;
                        }
                    case HotKeyActions.HelpScreen: break;
                    case HotKeyActions.InventoryScreen:
                        if (Cache.GameSettings.InventoryV2)
                            dialogBoxes[GameDialogBoxType.CharacterV2].Toggle(DialogBoxState.Compact);
                        else dialogBoxes[GameDialogBoxType.InventoryV1].Toggle();

                        /* SAVE CHANGES TO DIALOG LOCATIONS */
                        SaveDialogBoxConfiguration();
                        break;
                    case HotKeyActions.Logout: break;
                    case HotKeyActions.MiniMap:
                        dialogBoxes[GameDialogBoxType.MiniMap].Toggle();

                        /* SAVE CHANGES TO DIALOG LOCATIONS */
                        SaveDialogBoxConfiguration();
                        break;
                    case HotKeyActions.MusicMode:
                        {
                            if (Cache.GameSettings.MusicOn)
                            {
                                AudioHelper.StopMusic();
                                Cache.GameSettings.MusicOn = false;
                                AddEvent("Music OFF");
                            }
                            else if (Cache.GameSettings.SoundsOn)
                            {
                                Cache.GameSettings.SoundsOn = false;
                                AddEvent("Sounds OFF");
                            }
                            else
                            {
                                AudioHelper.PlayMusic(player.Map.MusicName);
                                Cache.GameSettings.MusicOn = true;
                                AddEvent("Music ON");
                                Cache.GameSettings.SoundsOn = true;
                                AddEvent("Sounds ON");
                            }
                            break;
                        }
                    case HotKeyActions.OverlayMode: //TODO add to settings, seperate
                        {
                            if (!overlayMode)
                            {
                                overlayMode = true;
                                overlayMagicMode = true;
                                overlayWeaponMode = true;
                                overlayColorMode = true;
                                overlayMovePath = true;
                                AddEvent("Range Overlay ON");
                            }
                            else
                            {
                                overlayMode = false;
                                overlayMagicMode = false;
                                overlayWeaponMode = false;
                                overlayColorMode = false;
                                overlayMovePath = false;
                                AddEvent("Range Overlay OFF");
                            }
                            break;
                        }
                    case HotKeyActions.QuickSlotOne: break; //Hotkey button 1
                    case HotKeyActions.QuickSlotOneSet: break; //Set Hotkey button 1
                    case HotKeyActions.QuickSlotTwo: break; //Hotkey button 2
                    case HotKeyActions.QuickSlotTwoSet: break; //Set Hotkey button 2
                    case HotKeyActions.RepeatMessage: break; //repeat last chat message
                    case HotKeyActions.Run: //For character to run while button is held down
                        {
                            if (!player.RunningMode)
                            {
                                if (!player.IsRunning) { player.IsRunning = true; }
                                else { player.IsRunning = false; }
                            }
                            break;
                        }
                    case HotKeyActions.SafeMode: ToggleSafeMode(); break;
                    case HotKeyActions.ScreenShot: break;
                    case HotKeyActions.SkillScreen:
                        dialogBoxes[GameDialogBoxType.Skill].Toggle();

                        /* SAVE CHANGES TO DIALOG LOCATIONS */
                        SaveDialogBoxConfiguration();
                        break;
                    case HotKeyActions.MagicScreen:
                        //if (Cache.GameSettings.SpellBookV2)
                            dialogBoxes[GameDialogBoxType.SpellBook].Toggle();
                        //else dialogBoxes[GameDialogBoxType.SpellBook].Toggle();

                        /* SAVE CHANGES TO DIALOG LOCATIONS */
                        SaveDialogBoxConfiguration();
                        break;
                    case HotKeyActions.TransparencyMode: Cache.GameSettings.TransparentDialogs = !Cache.GameSettings.TransparentDialogs; break;
                    case HotKeyActions.ZoomIn: Cache.GameSettings.MiniMapZoomLevel = Math.Min(Cache.GameSettings.MiniMapZoomLevel + 1, 2); break;
                    case HotKeyActions.ZoomOut: Cache.GameSettings.MiniMapZoomLevel = Math.Max(Cache.GameSettings.MiniMapZoomLevel - 1, 0); break;
                    case HotKeyActions.UseHPPotion: break;
                    case HotKeyActions.UseMPPotion: break;
                    case HotKeyActions.WisperMode: break;
                    case HotKeyActions.MagicScreenEight: //Quick access to spell book page
                        {
                            if (dialogBoxes[GameDialogBoxType.SpellBook].Config.Visible)
                            {
                                dialogBoxes[GameDialogBoxType.SpellBook].Config.Page = 8;
                                BringToFront(GameDialogBoxType.SpellBook);
                            }
                            else
                            {
                                dialogBoxes[GameDialogBoxType.SpellBook].Config.Page = 8;
                                dialogBoxes[GameDialogBoxType.SpellBook].Show();
                                BringToFront(GameDialogBoxType.SpellBook);
                            }
                            break;
                        }
                    case HotKeyActions.MagicScreenFive: //Quick access to spell book page
                        {
                            if (dialogBoxes[GameDialogBoxType.SpellBook].Config.Visible)
                            {
                                dialogBoxes[GameDialogBoxType.SpellBook].Config.Page = 5;
                                BringToFront(GameDialogBoxType.SpellBook);
                            }
                            else
                            {
                                dialogBoxes[GameDialogBoxType.SpellBook].Config.Page = 5;
                                dialogBoxes[GameDialogBoxType.SpellBook].Show();
                                BringToFront(GameDialogBoxType.SpellBook);
                            }
                            break;
                        }
                    case HotKeyActions.MagicScreenFour: //Quick access to spell book page
                        {
                            if (dialogBoxes[GameDialogBoxType.SpellBook].Config.Visible)
                            {
                                dialogBoxes[GameDialogBoxType.SpellBook].Config.Page = 4;
                                BringToFront(GameDialogBoxType.SpellBook);
                            }
                            else
                            {
                                dialogBoxes[GameDialogBoxType.SpellBook].Config.Page = 4;
                                dialogBoxes[GameDialogBoxType.SpellBook].Show();
                                BringToFront(GameDialogBoxType.SpellBook);
                            }
                            break;
                        }
                    case HotKeyActions.MagicScreenNine: //Quick access to spell book page
                        {
                            if (dialogBoxes[GameDialogBoxType.SpellBook].Config.Visible)
                            {
                                dialogBoxes[GameDialogBoxType.SpellBook].Config.Page = 9;
                                BringToFront(GameDialogBoxType.SpellBook);
                            }
                            else
                            {
                                dialogBoxes[GameDialogBoxType.SpellBook].Config.Page = 9;
                                dialogBoxes[GameDialogBoxType.SpellBook].Show();
                                BringToFront(GameDialogBoxType.SpellBook);
                            }
                            break;
                        }
                    case HotKeyActions.MagicScreenOne: //Quick access to spell book page
                        {
                            if (dialogBoxes[GameDialogBoxType.SpellBook].Config.Visible)
                            {
                                dialogBoxes[GameDialogBoxType.SpellBook].Config.Page = 1;
                                BringToFront(GameDialogBoxType.SpellBook);
                            }
                            else
                            {
                                dialogBoxes[GameDialogBoxType.SpellBook].Config.Page = 1;
                                dialogBoxes[GameDialogBoxType.SpellBook].Show();
                                BringToFront(GameDialogBoxType.SpellBook);
                            }
                            break;
                        }
                    case HotKeyActions.MagicScreenSeven: //Quick access to spell book page
                        {
                            if (dialogBoxes[GameDialogBoxType.SpellBook].Config.Visible)
                            {
                                dialogBoxes[GameDialogBoxType.SpellBook].Config.Page = 7;
                                BringToFront(GameDialogBoxType.SpellBook);
                            }
                            else
                            {
                                dialogBoxes[GameDialogBoxType.SpellBook].Config.Page = 7;
                                dialogBoxes[GameDialogBoxType.SpellBook].Show();
                                BringToFront(GameDialogBoxType.SpellBook);
                            }
                            break;
                        }
                    case HotKeyActions.MagicScreenSix: //Quick access to spell book page
                        {
                            if (dialogBoxes[GameDialogBoxType.SpellBook].Config.Visible)
                            {
                                dialogBoxes[GameDialogBoxType.SpellBook].Config.Page = 6;
                                BringToFront(GameDialogBoxType.SpellBook);
                            }
                            else
                            {
                                dialogBoxes[GameDialogBoxType.SpellBook].Config.Page = 6;
                                dialogBoxes[GameDialogBoxType.SpellBook].Show();
                                BringToFront(GameDialogBoxType.SpellBook);
                            }
                            break;
                        }
                    case HotKeyActions.MagicScreenTen: //Quick access to spell book page
                        {
                            if (dialogBoxes[GameDialogBoxType.SpellBook].Config.Visible)
                            {
                                dialogBoxes[GameDialogBoxType.SpellBook].Config.Page = 10;
                                BringToFront(GameDialogBoxType.SpellBook);
                            }
                            else
                            {
                                dialogBoxes[GameDialogBoxType.SpellBook].Config.Page = 10;
                                dialogBoxes[GameDialogBoxType.SpellBook].Show();
                                BringToFront(GameDialogBoxType.SpellBook);
                            }
                            break;
                        }
                    case HotKeyActions.MagicScreenThree: //Quick access to spell book page
                        {
                            if (dialogBoxes[GameDialogBoxType.SpellBook].Config.Visible)
                            {
                                dialogBoxes[GameDialogBoxType.SpellBook].Config.Page = 3;
                                BringToFront(GameDialogBoxType.SpellBook);
                            }
                            else
                            {
                                dialogBoxes[GameDialogBoxType.SpellBook].Config.Page = 3;
                                dialogBoxes[GameDialogBoxType.SpellBook].Show();
                                BringToFront(GameDialogBoxType.SpellBook);
                            }
                            break;
                        }
                    case HotKeyActions.MagicScreenTwo: //Quick access to spell book page
                        {
                            if (dialogBoxes[GameDialogBoxType.SpellBook].Config.Visible)
                            {
                                dialogBoxes[GameDialogBoxType.SpellBook].Config.Page = 2;
                                BringToFront(GameDialogBoxType.SpellBook);
                            }
                            else
                            {
                                dialogBoxes[GameDialogBoxType.SpellBook].Config.Page = 2;
                                dialogBoxes[GameDialogBoxType.SpellBook].Show();
                                BringToFront(GameDialogBoxType.SpellBook);
                            }
                            break;
                        }
                    case HotKeyActions.SystemMenuScreen:
                        dialogBoxes[GameDialogBoxType.System].Toggle();

                        /* SAVE CHANGES TO DIALOG LOCATIONS */
                        SaveDialogBoxConfiguration();
                        break;
                    case HotKeyActions.TeamColorMode: Cache.GameSettings.TeamColorOn = !Cache.GameSettings.TeamColorOn; break; //Display players with colored capes + long boots that match their relation to the user for easy identifying. 
                    case HotKeyActions.TileOverLayMode: TileOverLayMode = !TileOverLayMode; break; //Used to display black borders around the tiles for players who want this view.                  
                    case HotKeyActions.QuickCast: spellToCast = player.SpellCharged; break;
                    case HotKeyActions.HotBar1: if (!chatMode) spellToCast = (Cache.HotBarConfiguration.ContainsKey(0) ? Cache.HotBarConfiguration[0].Index : -1); break;
                    case HotKeyActions.HotBar2: if (!chatMode) spellToCast = (Cache.HotBarConfiguration.ContainsKey(1) ? Cache.HotBarConfiguration[1].Index : -1); break;
                    case HotKeyActions.HotBar3: if (!chatMode) spellToCast = (Cache.HotBarConfiguration.ContainsKey(2) ? Cache.HotBarConfiguration[2].Index : -1); break;
                    case HotKeyActions.HotBar4: if (!chatMode) spellToCast = (Cache.HotBarConfiguration.ContainsKey(3) ? Cache.HotBarConfiguration[3].Index : -1); break;
                    case HotKeyActions.HotBar5: if (!chatMode) spellToCast = (Cache.HotBarConfiguration.ContainsKey(4) ? Cache.HotBarConfiguration[4].Index : -1); break;
                    case HotKeyActions.HotBar6: if (!chatMode) spellToCast = (Cache.HotBarConfiguration.ContainsKey(5) ? Cache.HotBarConfiguration[5].Index : -1); break;
                    case HotKeyActions.HotBar7: if (!chatMode) spellToCast = (Cache.HotBarConfiguration.ContainsKey(6) ? Cache.HotBarConfiguration[6].Index : -1); break;
                    case HotKeyActions.HotBar8: if (!chatMode) spellToCast = (Cache.HotBarConfiguration.ContainsKey(7) ? Cache.HotBarConfiguration[7].Index : -1); break;
                    case HotKeyActions.HotBar9: if (!chatMode) spellToCast = (Cache.HotBarConfiguration.ContainsKey(8) ? Cache.HotBarConfiguration[8].Index : -1); break;
                    case HotKeyActions.HotBar0: if (!chatMode) spellToCast = (Cache.HotBarConfiguration.ContainsKey(9) ? Cache.HotBarConfiguration[9].Index : -1); break;
                    case HotKeyActions.ZoomInScreen: ZoomIn(); break;
                    case HotKeyActions.ZoomOutScreen: ZoomOut(); break;
                    case HotKeyActions.Set1: UseItemSet(1); break;
                    case HotKeyActions.Set2: UseItemSet(2); break;
                    case HotKeyActions.Set3: UseItemSet(3); break;
                    case HotKeyActions.Set4: UseItemSet(4); break;
                    case HotKeyActions.Set5: UseItemSet(5); break;
                }

                if (spellToCast != -1)
                    if (player.HandsFree)
                    {
                        if (player.CanCast(spellToCast))
                        {
                            if (!player.MoveReady) player.SpellRequest = spellToCast;
                            else if (!player.IsCasting)
                            {
                                player.Idle(player.Direction, true);
                                ((MainGame)Cache.DefaultState).Idle(player.Direction);

                                player.Cast(spellToCast);
                                ((MainGame)Cache.DefaultState).Cast(spellToCast);
                            }
                            else Cache.DefaultState.AddEvent("Already casting a spell");
                        }
                        else Cache.DefaultState.AddEvent("Not enough mana or intelligence to cast."); //split this up
                    }
                    else Cache.DefaultState.AddEvent("Your hands must be free to cast magic.");
            }
        }

        public void SetMagicPopup(GameDialogBoxType type, int magicIndex, Vector2 location, int x, int y)
        {
            popupType = type;
            popupMagicIndex = magicIndex;
            popupLocation = location;
            popupX = x;
            popupY = y;
        }

        public void SetItemPopup(GameDialogBoxType type, Item item, Vector2 location, int x, int y)
        {
            popupType = type;
            popupItem = item;
            popupLocation = location;
            popupX = x;
            popupY = y;
        }

        public void SetDraggedItem(DraggedType type, int index)
        {
            draggedType = type;
            draggedIndex = index;
        }

        public void ZoomIn()
        {

            //Rectangle rec = new Rectangle(-display.GameBoardShiftX + display.Zoom.X, -display.GameBoardShiftY + display.Zoom.Y, display.Zoom.Width, display.Zoom.Height);


            //x, y, width, height
            //Todo maybe use player center?
            int height = display.Zoom.Height;
            int width = display.Zoom.Width;

            //shrink
            height -= (int)((float)height * 0.1f);
            width -= (int)((float)width * 0.1f);

            //find x,y cords
            int x = ((display.ResolutionWidth - width) / 2) + display.GameBoardShiftX;
            int y = ((display.ResolutionHeight - height) / 2) + display.GameBoardShiftY;

            Rectangle rectangle = new Rectangle(x ,y, width, height);
            display.Zoom = new Rectangle(x, y, width, height);
        }

        public void ZoomOut()
        {

            //x, y, width, height
            //Todo maybe use player center?
            int height = display.Zoom.Height;
            int width = display.Zoom.Width;

            //shrink
            height += (int)((float)height * 0.1f);
            width += (int)((float)width * 0.1f);

            //find x,y cords
            int x = ((display.ResolutionWidth - width) / 2) + display.GameBoardShiftX;
            int y = ((display.ResolutionHeight - height) / 2) + display.GameBoardShiftY;

            Rectangle rectangle = new Rectangle(x, y, width, height);
            if (rectangle.Height >= display.ResolutionHeight || rectangle.Width >= display.ResolutionWidth) { display.Zoom = new Rectangle(display.GameBoardShiftX, display.GameBoardShiftY, display.ResolutionWidth, display.ResolutionHeight); }
            else { display.Zoom = rectangle; } //Todo add, cannot zoom out?
        }


        public void BringToFront(GameDialogBoxType type)
        {
            dialogBoxDrawOrder.Remove(type);
            dialogBoxDrawOrder.AddLast(type);
        }

     

        private void ReadMessageFromGameServer(ClientInfo ci, byte[] data, int len)
        {
            
            try
            {
                // find out if there is more than 1 packet here, if there is we need to split them up
                // and process them individually. this is by nature of the way TCP works using streams
                bool allPacketsProcessed = false;
                int pointer = 0; // packet start pointer
                while (!allPacketsProcessed)
                {
                    int size = BitConverter.ToInt16(data, pointer + 1); // data[1] and data[2] - size of the packet
                    byte[] packetData = new byte[size];
                    Buffer.BlockCopy(data, pointer, packetData, 0, size);

                    if (packetData.Length > 2)
                    {
                        Command command = new Command(packetData, ci.ID);
                        lock (processQueueLock)
                        {
                            processQueue.Enqueue(command);
                        }
                    }

                    // check if another header can be found. if so, check that its size > 0. if not, all packets processed
                    pointer += size + 3; // set the pointer to the index start of the next packet
                                         //if ((len >= pointer) && BitConverter.ToInt16(data, pointer + 1) > 0)
                                         //{ }
                                         //else

                    if (len <= pointer || BitConverter.ToInt16(data, pointer + 1) <= 0)
                    {
                        allPacketsProcessed = true;
                    } // we're all good
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine("Error in readmessage: " + ex.ToString());
            }
        }

        private void ServerProcess(byte[] data, int identity, string ipAddress)
        {
            dataInCounter += data.Length;

            if (data.Length < 6) return;

            try
            {
                UInt16 objectId;
                int x,y,dx,dy;
                int typeTemp = (int)BitConverter.ToInt32(data, 0);
                int messageTypeTemp = (int)BitConverter.ToInt16(data, 4);
                CommandType type;
                CommandMessageType messageType;
                ChatMessage message;

                if (Enum.TryParse<CommandType>(typeTemp.ToString(), out type))
                {
                    switch (type)
                    {
                        case CommandType.CommonEvent:
                            if (Enum.TryParse<CommandMessageType>(messageTypeTemp.ToString(), out messageType))
                                switch (messageType)
                                {
                                    case CommandMessageType.DropItem:
                                    case CommandMessageType.SetItem:
                                        x = BitConverter.ToInt16(data, 6);
                                        y = BitConverter.ToInt16(data, 8);
                                        Cache.MapView.Map[y][x].ItemSprite = BitConverter.ToInt16(data, 10);
                                        Cache.MapView.Map[y][x].ItemSpriteFrame = BitConverter.ToInt16(data, 12);
                                        Cache.MapView.Map[y][x].ItemSpriteColour = BitConverter.ToInt32(data, 14);
                                        Cache.MapView.Map[y][x].ItemColorType = (GameColor)BitConverter.ToInt16(data, 18);

                                        if (messageType == CommandMessageType.DropItem)
                                        {
                                            Cache.DefaultState.AddEffect(new GameEffect(DrawEffectType.DustKick, new Location(x - Cache.MapView.PivotX - Globals.OffScreenCells, y - Cache.MapView.PivotY - Globals.OffScreenCells), null));
                                            if (Cache.MapView.Map[y][x].ItemSprite == 6 && Cache.MapView.Map[y][x].ItemSpriteFrame == 0)
                                                Cache.DefaultState.AddEffect(new GameEffect(DrawEffectType.GoldDrop, new Location(x - Cache.MapView.PivotX - Globals.OffScreenCells, y - Cache.MapView.PivotY - Globals.OffScreenCells), null));
                                            else AudioHelper.PlaySound("E11"); // TODO - gold hardcoding
                                        }
                                        break;
                                    case CommandMessageType.CastMagic:
                                        x = BitConverter.ToInt16(data, 6);
                                        y = BitConverter.ToInt16(data, 8);
                                        dx = BitConverter.ToInt16(data, 10);
                                        dy = BitConverter.ToInt16(data, 12);
                                        int magicType = (BitConverter.ToInt32(data, 14) - 100);

                                        DrawEffectType effectType = Cache.MagicConfiguration[magicType].DrawEffect;
                                        if (effectType != DrawEffectType.None)
                                        {
                                            IGameEffect e = new GameEffect(effectType, new Location(x - Cache.MapView.PivotX - Globals.OffScreenCells, y - Cache.MapView.PivotY - Globals.OffScreenCells), new Location(dx - Cache.MapView.PivotX - Globals.OffScreenCells, dy - Cache.MapView.PivotY - Globals.OffScreenCells));
                                            e.Pivot = new Location(Cache.MapView.PivotX, Cache.MapView.PivotY);
                                            effects.Add(e);
                                        }
                                        break;
                                    case CommandMessageType.ShootArrow:
                                        x = BitConverter.ToInt16(data, 6);
                                        y = BitConverter.ToInt16(data, 8);
                                        dx = BitConverter.ToInt16(data, 10);
                                        dy = BitConverter.ToInt16(data, 12);

                                        Cache.DefaultState.AddEffect(new GameEffect(DrawEffectType.Arrow,
                                            new Location(x - (Cache.MapView.PivotX + Globals.OffScreenCells), y - (Cache.MapView.PivotY + Globals.OffScreenCells)),
                                            new Location(dx - (Cache.MapView.PivotX + Globals.OffScreenCells), dy - (Cache.MapView.PivotY + Globals.OffScreenCells))));
                                        break;
                                    case CommandMessageType.TradeRequest:
                                        player.TradePartnerID = BitConverter.ToInt32(data, 6);
                                        string tradePartnerName = Encoding.ASCII.GetString(data, 10, 10).Trim('\0');
                                        AddEvent(string.Format("{0} would like to trade...", tradePartnerName));
                                        ((TradeV2DialogBox)dialogBoxes[GameDialogBoxType.TradeV2]).TradePartnerName = tradePartnerName;

                                        int statCount = (int)data[54];
                                        byte[] itemData = new byte[36 + (statCount)*2];
                                        Buffer.BlockCopy(data, 20, itemData, 0, itemData.Length);
                                        int id = BitConverter.ToInt32(itemData, 2);
                                        Item item = Cache.ItemConfiguration[id].Copy();
                                        item.ParseData(itemData);
                                        AddTradePartnerItem(item);

                                        GameMessageBox box = new GameMessageBox(GameDialogBoxType.TradeV2Confirmation, string.Format("Trade with {0}?", tradePartnerName), "Yes", "No");
                                        box.ResponseSelected += TradeRequestConfirmation;
                                        ShowMessageBox(box);
                                        break;
                                    case CommandMessageType.TradeAddItem:
                                        int statCount2 = (int)data[44];
                                        byte[] itemData2 = new byte[36 + (statCount2)*2];
                                        Buffer.BlockCopy(data, 10, itemData2, 0, itemData2.Length);
                                        int id2 = BitConverter.ToInt32(itemData2, 2);
                                        Item item2 = Cache.ItemConfiguration[id2].Copy();
                                        item2.ParseData(itemData2);
                                        AddTradePartnerItem(item2);
                                        break;
                                    case CommandMessageType.TradeRemoveItem:
                                        int removeItemId = BitConverter.ToInt16(data, 10);
                                        RemoveTradePartnerItem(removeItemId);
                                        break;
                                    case CommandMessageType.TradeResponse:
                                        if ((int)data[20] == 1)
                                        {
                                            ((TradeV2DialogBox)dialogBoxes[GameDialogBoxType.TradeV2]).TradePartnerName = Encoding.ASCII.GetString(data, 10, 10).Trim('\0');
                                            dialogBoxes[GameDialogBoxType.TradeV2].Show();
                                        }
                                        else
                                        {
                                            ((TradeV2DialogBox)dialogBoxes[GameDialogBoxType.TradeV2]).TradePartnerName = string.Empty;
                                            player.TradePartnerID = -1;
                                            AddEvent("Partner declined the trade request.");
                                        }
                                        break;
                                    case CommandMessageType.TradeCancel: EndTrade(TradeState.Cancelled); break;
                                    case CommandMessageType.TradeAccept:
                                        if ((int)data[6] == 1)
                                            ((TradeV2DialogBox)dialogBoxes[GameDialogBoxType.TradeV2]).TradeAcceptedPartner = true;
                                        else ((TradeV2DialogBox)dialogBoxes[GameDialogBoxType.TradeV2]).TradeAcceptedPartner = false;
                                        break;
                                    case CommandMessageType.TradeComplete: EndTrade(TradeState.Completed); break;
                                }
                            break;
                        case CommandType.ResponseInitPlayer:
                            if (Enum.TryParse<CommandMessageType>(messageTypeTemp.ToString(), out messageType))
                                switch (messageType)
                                {
                                    case CommandMessageType.Confirm: InitData(); break;
                                    case CommandMessageType.Reject: break;
                                }
                            break;
                        case CommandType.ResponseInitData:
                            if (Enum.TryParse<CommandMessageType>(messageTypeTemp.ToString(), out messageType))
                                switch (messageType)
                                {
                                    case CommandMessageType.Confirm:
                                        player.Init();
                                        player.ObjectId = BitConverter.ToUInt16(data, 6);
                                        x = BitConverter.ToInt16(data, 8);
                                        y = BitConverter.ToInt16(data, 10);
                                        player.Type = BitConverter.ToInt16(data, 12);
                                        player.Appearance1 = BitConverter.ToInt16(data, 14);
                                        player.Appearance2 = BitConverter.ToInt16(data, 16);
                                        player.Appearance3 = BitConverter.ToInt16(data, 18);
                                        player.Appearance4 = BitConverter.ToInt16(data, 20);
                                        player.AppearanceColour = BitConverter.ToInt32(data, 22);
                                        player.Status = BitConverter.ToInt32(data, 26);
                                        player.MapName = Encoding.ASCII.GetString(data, 30, 10).Trim('\0');
                                        player.FriendlyMapName = Encoding.ASCII.GetString(data, 40, 30).Trim('\0');
                                        timeOfDay = (TimeOfDay)data[80];
                                        WeatherType weatherType = (WeatherType)data[81];
                                        player.Contribution = BitConverter.ToInt32(data, 82);
                                        //isObserver = Boolean.Parse(Int32.Parse(data[86].ToString()))
                                        player.Hunger = (int)data[86];
                                        player.Reputation = BitConverter.ToInt32(data, 87);
                                        player.HP = BitConverter.ToInt32(data, 91);
                                        player.PlayType = (PlayType)(int)data[95];

                                        Cache.OwnerCache.Clear(); // clears cache of owners
                                        Cache.DynamicObjects.Clear(); // clears cache of dynamic objects
                                        effects.Clear(); // clears local cache of effects

                                        Map map = new Map();
                                        map.Load("Maps\\" + player.MapName + ".amd");
                                        map.MusicName = Encoding.ASCII.GetString(data, 70, 10).Trim('\0');
                                        MapHelper.LoadMiniMap(player.MapName, map);

                                        player.Idle(MotionDirection.South);
                                        player.SetLocation(map, x, y);
                                        Cache.MapView.Map = map;
                                        Cache.MapView.SetPivot(player.X, player.Y);                                        
                                        
                                        //int header;
                                        //int count = 96;
                                        //int cells = BitConverter.ToInt16(data, count);
                                        //count += 2;

                                        //for (int i = 1; i <= cells; i++)
                                        //{
                                        //    x = Cache.MapView.PivotX + Globals.OffScreenCells + BitConverter.ToInt16(data, count);
                                        //    count += 2;
                                        //    y = Cache.MapView.PivotY + Globals.OffScreenCells + BitConverter.ToInt16(data, count);
                                        //    count += 2;
                                        //    header = (int)data[count];
                                        //    count++;
                                        //    if ((header & 0x01) == 0 ? false : true)
                                        //    {
                                        //        objectId = BitConverter.ToUInt16(data, count);
                                        //        count += 2;
                                        //        IOwner owner = (objectId == player.ObjectId) ? (IOwner)player : new Owner(objectId);
                                        //        owner.Parse(data, count, out count);

                                        //        if (!Cache.OwnerCache.ContainsKey(owner.ObjectId))
                                        //        {
                                        //            owner.Idle(owner.Direction);
                                        //            owner.SetLocation(Cache.MapView.Map, x, y);
                                        //            Cache.OwnerCache.Add(owner.ObjectId, owner);
                                        //        }
                                        //    }
                                        //    if ((header & 0x02) == 0 ? false : true)
                                        //    {
                                        //        objectId = BitConverter.ToUInt16(data, count);
                                        //        count += 2;
                                        //        IOwner owner = (objectId == player.ObjectId) ? (IOwner)player : new Owner(objectId);
                                        //        owner.Parse(data, count, out count);

                                        //        if (!Cache.OwnerCache.ContainsKey(owner.ObjectId))
                                        //        {
                                        //            owner.Idle(owner.Direction);
                                        //            owner.SetLocation(Cache.MapView.Map, x, y);
                                        //            Cache.OwnerCache.Add(owner.ObjectId, owner);
                                        //        }
                                        //    }
                                        //    if ((header & 0x04) == 0 ? false : true)
                                        //    {
                                        //        Cache.MapView.Map[y][x].ItemSprite = BitConverter.ToInt16(data, count);
                                        //        count += 2;
                                        //        Cache.MapView.Map[y][x].ItemSpriteFrame = BitConverter.ToInt16(data, count);
                                        //        count += 2;
                                        //        Cache.MapView.Map[y][x].ItemSpriteColour = BitConverter.ToInt32(data, count);
                                        //        count += 4;
                                        //    }
                                        //    if ((header & 0x08) == 0 ? false : true)
                                        //    {
                                        //        int id = BitConverter.ToInt16(data, count);
                                        //        count += 2;
                                        //        int objectType = BitConverter.ToInt16(data, count);
                                        //        count += 2;

                                        //        if (!Cache.DynamicObjects.ContainsKey(id))
                                        //        {
                                        //            DynamicObject dynamicObject = new DynamicObject(id, (DynamicObjectType)objectType);
                                        //            dynamicObject.Map = map;
                                        //            dynamicObject.Init(x, y);
                                        //        }
                                        //    }
                                        //}

                                        Cache.MapView.Init(map, player);
                                        AudioHelper.PlayMusic(map.MusicName);

                                        if (weatherType != WeatherType.Clear)
                                        {
                                            weather = new Weather(weatherType);
                                            weather.Init();
                                        }
                                        else weather = null;

                                        // close non static dialog boxes
                                        dialogBoxes[GameDialogBoxType.Merchant].Hide();
                                        dialogBoxes[GameDialogBoxType.TradeV2].Hide();
                                        dialogBoxes[GameDialogBoxType.WarehouseV2].Hide();
                                        if (dialogBoxes.ContainsKey(GameDialogBoxType.TradeV2Confirmation))
                                            TradeRequestConfirmation(GameDialogBoxType.TradeV2Confirmation, MessageBoxResponse.No);

                                        // clear public event details
                                        publicEventState = PublicEventState.None;
                                        publicEventParticipation = false;
                                        publicEventType = PublicEventType.None;
                                        publicEventX = publicEventY = -1;
                                        publicEventDifficulty = PublicEventDifficulty.None;

                                        break;
                                    case CommandMessageType.Reject: break;
                                }
                            break;
                        case CommandType.ResponseInitMapData:
                            if (Enum.TryParse<CommandMessageType>(messageTypeTemp.ToString(), out messageType))
                                switch (messageType)
                                {
                                    case CommandMessageType.Confirm: 
                                        {
                                            int header;
                                            int count = 6;
                                            int cells = BitConverter.ToInt16(data, count);
                                            count += 2;

                                            for (int i = 1; i <= cells; i++)
                                            {
                                                x = Cache.MapView.PivotX + Globals.OffScreenCells + BitConverter.ToInt16(data, count);
                                                count += 2;
                                                y = Cache.MapView.PivotY + Globals.OffScreenCells + BitConverter.ToInt16(data, count);
                                                count += 2;
                                                header = (int)data[count];
                                                count++;
                                                if ((header & 0x01) == 0 ? false : true)
                                                {
                                                    objectId = BitConverter.ToUInt16(data, count);
                                                    count += 2;
                                                    IOwner owner = (objectId == player.ObjectId) ? (IOwner)player : new Owner(objectId);
                                                    owner.Parse(data, count, out count);

                                                    if (!Cache.OwnerCache.ContainsKey(owner.ObjectId))
                                                    {
                                                        owner.Idle(owner.Direction);
                                                        owner.SetLocation(Cache.MapView.Map, x, y);
                                                        Cache.OwnerCache.Add(owner.ObjectId, owner);
                                                    }
                                                }
                                                if ((header & 0x02) == 0 ? false : true)
                                                {
                                                    objectId = BitConverter.ToUInt16(data, count);
                                                    count += 2;
                                                    IOwner owner = (objectId == player.ObjectId) ? (IOwner)player : new Owner(objectId);
                                                    owner.Parse(data, count, out count);

                                                    if (!Cache.OwnerCache.ContainsKey(owner.ObjectId))
                                                    {
                                                        owner.Idle(owner.Direction);
                                                        owner.SetLocation(Cache.MapView.Map, x, y);
                                                        Cache.OwnerCache.Add(owner.ObjectId, owner);
                                                    }
                                                }
                                                if ((header & 0x04) == 0 ? false : true)
                                                {
                                                    Cache.MapView.Map[y][x].ItemSprite = BitConverter.ToInt16(data, count);
                                                    count += 2;
                                                    Cache.MapView.Map[y][x].ItemSpriteFrame = BitConverter.ToInt16(data, count);
                                                    count += 2;
                                                    Cache.MapView.Map[y][x].ItemSpriteColour = BitConverter.ToInt32(data, count);
                                                    count += 4;
                                                    Cache.MapView.Map[y][x].ItemColorType = (GameColor)BitConverter.ToInt16(data, count);
                                                    count += 2;
                                                }
                                                if ((header & 0x08) == 0 ? false : true)
                                                {
                                                    int id = BitConverter.ToInt16(data, count);
                                                    count += 2;
                                                    int objectType = BitConverter.ToInt16(data, count);
                                                    count += 2;

                                                    if (!Cache.DynamicObjects.ContainsKey(id))
                                                    {
                                                        DynamicObject dynamicObject = new DynamicObject(id, (DynamicObjectType)objectType);
                                                        dynamicObject.Map = Cache.MapView.Map;
                                                        dynamicObject.Init(x, y);
                                                    }
                                                }
                                            }

                                            break; 
                                        }
                                    case CommandMessageType.Reject: break;                                       
                                }
                            break;
                        case CommandType.ResponseInitDataPlayerStats:
                            if (Enum.TryParse<CommandMessageType>(messageTypeTemp.ToString(), out messageType))
                                switch (messageType)
                                {
                                    case CommandMessageType.Confirm:
                                        player.HP = BitConverter.ToInt32(data, 6);
                                        player.MP = BitConverter.ToInt32(data, 10);
                                        player.SP = BitConverter.ToInt32(data, 14);
                                        if (player.PlayType == PlayType.PvE)
                                            player.Gold = BitConverter.ToInt32(data, 18); // 18-21 used to be defence ratio - deprecated
                                        else if (player.PlayType == PlayType.PvP)
                                            player.GladiatorPoints = BitConverter.ToInt32(data, 18);
                                        player.Criticals = BitConverter.ToInt16(data, 22); // 22-25 used to be hit prob - deprecated
                                        player.Majestics = BitConverter.ToInt16(data, 24);
                                        player.Level = BitConverter.ToInt32(data, 26);
                                        player.Strength = BitConverter.ToInt32(data, 30);
                                        player.Intelligence = BitConverter.ToInt32(data, 34);
                                        player.Vitality = BitConverter.ToInt32(data, 38);
                                        player.Dexterity = BitConverter.ToInt32(data, 42);
                                        player.Magic = BitConverter.ToInt32(data, 46);
                                        player.Agility = BitConverter.ToInt32(data, 50);
                                        player.LevelUpPoints = BitConverter.ToInt32(data, 54);
                                        player.SideStatus = (OwnerSideStatus)(int)data[58];
                                        player.RebirthLevel = (int)data[59];
                                        // 60 unused
                                        player.Experience = BitConverter.ToInt32(data, 61);
                                        player.EnemyKills = BitConverter.ToInt32(data, 65);
                                        player.CriminalCount = BitConverter.ToInt32(data, 69);
                                        player.RewardGold = BitConverter.ToInt32(data, 73);
                                        OwnerSide sideParser = OwnerSide.None;
                                        if (Enum.TryParse<OwnerSide>(Encoding.ASCII.GetString(data, 77, 10).Trim('\0'), true, out sideParser))
                                            player.Side = sideParser;
                                        player.GuildName = Encoding.ASCII.GetString(data, 87, 20).Trim('\0').Replace('_', ' ');
                                        player.GuildRank = BitConverter.ToInt32(data, 107);
                                        
                                        // 111 used to be critical
                                        // 111 (1 byte) TODO - fightzone number
                                        player.MinMeleeDamage = BitConverter.ToInt16(data, 112);
                                        player.MaxMeleeDamage = BitConverter.ToInt16(data, 114);
                                        player.MinMeleeDamageCritical = BitConverter.ToInt16(data, 116);
                                        player.MaxMeleeDamageCritical = BitConverter.ToInt16(data, 118);
                                        player.Titles[TitleType.DistanceTravelled] = BitConverter.ToInt32(data, 120);
                                        player.Titles[TitleType.ItemsCollected] = BitConverter.ToInt32(data, 124);
                                        player.Titles[TitleType.RareItemsCollected] = BitConverter.ToInt32(data, 128);
                                        player.Titles[TitleType.MonstersKilled] = BitConverter.ToInt32(data, 132);
                                        player.Titles[TitleType.PlayersKilled] = BitConverter.ToInt32(data, 136);
                                        player.Titles[TitleType.MonsterDeaths] = BitConverter.ToInt32(data, 140);
                                        player.Titles[TitleType.PlayerDeaths] = BitConverter.ToInt32(data, 144);
                                        player.Titles[TitleType.MonsterDamageDealt] = BitConverter.ToInt32(data, 148);
                                        player.Titles[TitleType.PlayerDamageDealt] = BitConverter.ToInt32(data, 152);
                                        player.Titles[TitleType.QuestsCompleted] = BitConverter.ToInt32(data, 156);
                                        player.Titles[TitleType.HighestMeleeDamage] = BitConverter.ToInt32(data, 160);
                                        player.Titles[TitleType.HighestMagicDamage] = BitConverter.ToInt32(data, 164);
                                        player.Titles[TitleType.HighestValueItem] = BitConverter.ToInt32(data, 168);
                                        player.Titles[TitleType.PublicEventsGold] = BitConverter.ToInt32(data, 172);
                                        player.Titles[TitleType.PublicEventsSilver] = BitConverter.ToInt32(data, 176);
                                        player.Titles[TitleType.PublicEventsBronze] = BitConverter.ToInt32(data, 180);
                                        break;
                                }
                            break;
                        case CommandType.ResponseInitDataPlayerItems:
                            if (Enum.TryParse<CommandMessageType>(messageTypeTemp.ToString(), out messageType))
                                switch (messageType)
                                {
                                    case CommandMessageType.Confirm:
                                        int itemCount = (int)data[6];
                                        int index = 7;

                                        for (int i = 0; i < itemCount; i++)
                                        {
                                            int statCount = (int)data[index + 34];
                                            byte[] itemData = new byte[37 + (statCount)*2];
                                            Buffer.BlockCopy(data, index, itemData, 0, itemData.Length);
                                            int id = BitConverter.ToInt32(itemData, 2);
                                            Item item = Cache.ItemConfiguration[id].Copy();
                                            item.ParseData(itemData);
                                            index += itemData.Length;
                                            player.Inventory[item.SlotId] = item;
                                            player.InventoryDrawOrder.AddLast(item.SlotId); // v1
                                        }
                                        player.Inventory.Update(); // update stat bonuses 

                                        int warehouseCount = (int)data[index];
                                        index++;
                                        for (int i = 0; i < warehouseCount; i++)
                                        {
                                            int statCount = (int)data[index + 34];
                                            byte[] itemData = new byte[37 + (statCount)*2];
                                            Buffer.BlockCopy(data, index, itemData, 0, itemData.Length);
                                            int id = BitConverter.ToInt32(itemData, 2);
                                            Item item = Cache.ItemConfiguration[id].Copy();
                                            item.ParseData(itemData);
                                            index += itemData.Length;
                                            player.Warehouse[item.SlotId] = item;
                                        }

                                        for (int i = 0; i < Globals.MaximumSpells; i++)
                                        {
                                            player.MagicLearned[i] = ((int)data[index] == 1) ? true : false;
                                            index++;
                                        }

                                        for (int i = 0; i < Globals.MaximumSkills; i++)
                                        {
                                            player.Skills[i] = ((int)data[index]);
                                            index++;
                                        }
                                        break;
                                }
                            break;
                        case CommandType.ResponseCitizenship:
                            switch ((int)data[4])
                            {
                                case 0: // failed
                                    AddEvent("You do not meet the requirements to become a citizen.");
                                    break;
                                case 1: // success
                                    OwnerSide side;
                                    if (Enum.TryParse<OwnerSide>(((int)data[5]).ToString(), out side))
                                    {
                                        player.Side = side;
                                        AddEvent(string.Format("You have become a citizen of {0}!", side.ToString()));
                                    }
                                    break;
                            }
                            break;
                        case CommandType.ResponseMerchantData: dialogBoxes[GameDialogBoxType.Merchant].SetData(data); break;
                        case CommandType.DynamicObjectEvent:
                            if (Enum.TryParse<CommandMessageType>(messageTypeTemp.ToString(), out messageType))
                            {
                                x = BitConverter.ToInt16(data, 6);
                                y = BitConverter.ToInt16(data, 8);
                                DynamicObjectType objectType = (DynamicObjectType)BitConverter.ToInt16(data, 10);
                                int id = BitConverter.ToInt16(data, 12);
                                switch (messageType)
                                {
                                    case CommandMessageType.Confirm:
                                        if (!Cache.DynamicObjects.ContainsKey(id))
                                        {
                                            DynamicObject dynamicObject = new DynamicObject(id, (DynamicObjectType)objectType);
                                            dynamicObject.Map = player.Map;
                                            dynamicObject.Init(x, y);
                                        }
                                        break;
                                    case CommandMessageType.Reject:
                                        Cache.DynamicObjects[id].Remove();
                                        break;
                                }
                            }
                            break;
                        case CommandType.Notify:
                            if (Enum.TryParse<CommandMessageType>(messageTypeTemp.ToString(), out messageType))
                                switch (messageType)
                                {
                                    case CommandMessageType.NotifySafeMode:
                                        {
                                            int safemode = BitConverter.ToInt32(data, 6);
                                            if (safemode == 1)
                                            {
                                                AddEvent("Safe mode activated.");
                                                player.SafeMode = true;
                                            }
                                            else
                                            {
                                                AddEvent("Safe mode deactivated.");
                                                player.SafeMode = false;
                                            }
                                            break;
                                        }
                                    case CommandMessageType.NotifyTickHP: player.TickHP = true; break;
                                    case CommandMessageType.NotifyTickMP: player.TickMP = true; break;
                                    case CommandMessageType.NotifyTickSP: player.TickSP = true; break;
                                    case CommandMessageType.NotifyTickEXP: player.TickEXP = true; break;
                                    case CommandMessageType.NotifyTickStopHP: player.TickHP = false; break;
                                    case CommandMessageType.NotifyTickStopMP: player.TickMP = false; break;
                                    case CommandMessageType.NotifyTickStopSP: player.TickSP = false; break;
                                    case CommandMessageType.NotifyTickStopEXP: player.TickEXP = false; break;
                                    case CommandMessageType.NotifyCriticals: player.Criticals = BitConverter.ToInt16(data, 6); break;
                                    case CommandMessageType.NotifyMajestics: 
                                        player.Majestics = BitConverter.ToInt16(data, 6);
                                        int gainedMajestics = BitConverter.ToInt32(data, 8);
                                        switch(gainedMajestics)
                                        {
                                            case 0: break;
                                            case 1: 
                                                AddEvent("You gained a Majestic Point!");
                                                switch (player.Gender)
                                                {
                                                    case GenderType.Male: AudioHelper.PlaySound("C21", 0.7f); break;
                                                    case GenderType.Female: AudioHelper.PlaySound("C22", 0.7f); break;
                                                }

                                                //TODO add settings to turn this on or off
                                                message = new ChatMessage(DateTime.Now);
                                                message.Message = "Majestic Points + 1!";
                                                message.Font = FontType.DamageLargeSize14Bold;
                                                message.Color = GameColor.MajesticUp;

                                                levelHistory.Add(message);
                                                break;
                                            default:
                                                AddEvent(string.Format("You gained {0} Majestic Points!", gainedMajestics));
                                                switch (player.Gender)
                                                {
                                                    case GenderType.Male: AudioHelper.PlaySound("C21", 0.7f); break;
                                                    case GenderType.Female: AudioHelper.PlaySound("C22", 0.7f); break;
                                                }
                                               
                                                //TODO add settings to turn this on or off
                                                message = new ChatMessage(DateTime.Now);
                                                message.Message = string.Format("Majestic Points +{0}!", gainedMajestics);
                                                message.Font = FontType.DamageLargeSize14Bold;
                                                message.Color = GameColor.MajesticUp;
                                               
                                                levelHistory.Add(message);
                                                break;
                                        }
                                        break;
                                    case CommandMessageType.NotifyTitle:
                                        TitleType title = (TitleType)BitConverter.ToInt32(data, 6);
                                        player.Titles[title] = BitConverter.ToInt32(data, 10);
                                        break;
                                    case CommandMessageType.NotifyItemToUpgrade: //TODO clean this up, split up functionality
                                        {
                                            int upgradeIndex = BitConverter.ToInt32(data, 6);

                                            if (player.Inventory[upgradeIndex] != null)
                                            {
                                                Item upgradeItem = player.Inventory[upgradeIndex];

                                                switch (upgradeItem.UpgradeType)
                                                {
                                                    case ItemUpgradeType.Ingredient:
                                                        if (player.Inventory.UpgradeIngredientIndex != -1)
                                                            AddEvent(player.Inventory[player.Inventory.UpgradeIngredientIndex].FriendlyName + " removed from upgrade.");
                                                        break;
                                                    case ItemUpgradeType.Merien:
                                                    case ItemUpgradeType.Xelima:
                                                        if (player.Inventory.UpgradeItemIndex != -1)
                                                            AddEvent(player.Inventory[player.Inventory.UpgradeItemIndex].FriendlyName + " removed from upgrade.");
                                                        break;
                                                    case ItemUpgradeType.Majestic:
                                                        if (player.Inventory.UpgradeIngredientIndex != -1)
                                                            AddEvent(player.Inventory[player.Inventory.UpgradeIngredientIndex].FriendlyName + " removed from upgrade.");
                                                        if (player.Inventory.UpgradeItemIndex != -1)
                                                            AddEvent(player.Inventory[player.Inventory.UpgradeItemIndex].FriendlyName + " removed from upgrade.");
                                                        break;
                                                }

                                                player.Inventory.ChangeUpgradeItem(upgradeIndex);

                                                if (player.Inventory[upgradeIndex].IsUpgradeIngredient || player.Inventory[upgradeIndex].IsUpgradeItem)
                                                {
                                                    switch (player.Inventory[upgradeIndex].UpgradeType)
                                                    {
                                                        case ItemUpgradeType.Majestic:
                                                            {
                                                                dialogBoxes[GameDialogBoxType.UpgradeV2].Config.State = DialogBoxState.MajesticUpgrade;
                                                                break;
                                                            }
                                                        case ItemUpgradeType.Merien:
                                                        case ItemUpgradeType.Xelima:
                                                        case ItemUpgradeType.Ingredient:
                                                            {
                                                                dialogBoxes[GameDialogBoxType.UpgradeV2].Config.State = DialogBoxState.IngredientUpgrade;
                                                                break;
                                                            }
                                                        default: break;
                                                    }
                                                    AddEvent(player.Inventory[upgradeIndex].FriendlyName + " added to upgrade.");
                                                }

                                                DialogBoxState state =  dialogBoxes[GameDialogBoxType.UpgradeV2].Config.State;
                                                if (player.Inventory.UpgradeItemIndex == -1 && player.Inventory.UpgradeIngredientIndex == -1 &&
                                                    state != DialogBoxState.Fail && state != DialogBoxState.Success)
                                                    dialogBoxes[GameDialogBoxType.UpgradeV2].Toggle(DialogBoxState.Normal);
                                            }                                            
                                            break;
                                        }
                                    case CommandMessageType.NotifyItemUpgrade:
                                        {
                                            int upgradeIndex = BitConverter.ToInt32(data, 6);
                                            if (player.Inventory[upgradeIndex] != null)
                                            {
                                                Item itemToUpgrade = player.Inventory[upgradeIndex];
                                                player.Inventory[upgradeIndex].Level = BitConverter.ToInt32(data, 10);
                                                AddEvent("Item upgrade succeeded!");

                                                if (itemToUpgrade.IsEquipped)
                                                {
                                                    switch (itemToUpgrade.EquipType)
                                                    {
                                                        case EquipType.DualHand: player.Inventory[(int)EquipType.RightHand] = player.Inventory[upgradeIndex]; break;
                                                        case EquipType.FullBody: player.Inventory[(int)EquipType.Body] = player.Inventory[upgradeIndex]; break;
                                                        default: player.Inventory[(int)itemToUpgrade.EquipType] = player.Inventory[upgradeIndex]; break;
                                                    }
                                                }

                                                dialogBoxes[GameDialogBoxType.UpgradeV2].Toggle(DialogBoxState.Success);
                                            }
                                            break;
                                        }
                                    case CommandMessageType.NotifyItemUpgradeFailed:
                                        UpgradeFailReason failReason = (UpgradeFailReason)BitConverter.ToInt32(data, 6);
                                        dialogBoxes[GameDialogBoxType.UpgradeV2].Toggle(DialogBoxState.Fail); 
                                        switch (failReason)
                                        {
                                            case UpgradeFailReason.Incompatible: AddEvent("Upgrade failed, incorrect combination."); break;
                                            case UpgradeFailReason.MaxLevel: AddEvent("Upgrade failed, item max level."); break;
                                            case UpgradeFailReason.ItemBroken: AddEvent("Upgrade failed, item is broken."); break;
                                            case UpgradeFailReason.ChanceFailed: AddEvent("Item upgrade failed."); break; // TODO fail sound
                                            case UpgradeFailReason.ToFewMajestics: AddEvent("Upgrade failed, to few majestics."); break;
                                            case UpgradeFailReason.NotUpgradable: AddEvent("Upgrade failed, item not upgradable."); break;
                                            case UpgradeFailReason.NoIngredient: AddEvent("Upgrade failed, no upgrade ingredient present."); break;
                                            case UpgradeFailReason.NoItem: AddEvent("Upgrade failed, no item to upgrade."); break;
                                            case UpgradeFailReason.EquippedItem: AddEvent("Can not upgrade equipped Item"); break;
                                            case UpgradeFailReason.NotAnUpgradeIngredient: AddEvent("Ingredient is not for upgrading"); break;
                                            case UpgradeFailReason.NotImplemented: AddEvent("Upgrade type hasn't been implemented"); break;
                                        }

                                        player.Inventory.ClearUpgradeItems();

                                        break;
                                    case CommandMessageType.NotifyGold:
                                        int previousGold = player.Gold;
                                        player.Gold = BitConverter.ToInt32(data, 6);
                                        if (previousGold < player.Gold)
                                        {
                                            AddEvent(string.Format("You got {0} Gold.", player.Gold-previousGold));
                                            AudioHelper.PlaySound("E12");
                                        }
                                        break;
                                    case CommandMessageType.NotifyNotEnoughGold: AddEvent("Not enough Gold."); break;
                                    case CommandMessageType.NotifyHP:
                                        int previousHP = player.HP;
                                        player.HP = BitConverter.ToInt32(data, 6); 
                                        player.MP = BitConverter.ToInt32(data, 10);
                                        if ((player.HP > previousHP) && (player.HP - previousHP > 10))
                                        {
                                            AddEvent(string.Format("HP increased by {0}pts.", player.HP - previousHP));
                                            AudioHelper.PlaySound("E21");
                                        }
                                        else if ((player.HP < previousHP) && (previousHP - player.HP > 10)) AddEvent(string.Format("HP decreased by {0}pts.", previousHP - player.HP));
                                        break;
                                    case CommandMessageType.NotifySP:
                                        int previousSP = player.SP;
                                        player.SP = BitConverter.ToInt32(data, 6); if (player.SP <= 0) player.RunChange = true;
                                        if ((player.SP > previousSP) && (player.SP - previousSP > 10))
                                        {
                                            AddEvent(string.Format("SP increased by {0}pts.", player.SP - previousSP));
                                            AudioHelper.PlaySound("E21");
                                        }
                                        else if ((player.SP < previousSP) && (previousSP - player.SP > 10)) AddEvent(string.Format("SP decreased by {0}pts.", previousSP - player.SP));
                                        break;
                                    case CommandMessageType.NotifyMP:
                                        int previousMP = player.MP;
                                        player.MP = BitConverter.ToInt32(data, 6);
                                        if ((player.MP > previousMP) && (player.MP - previousMP > 10))
                                        {
                                            AddEvent(string.Format("MP increased by {0}pts.", player.MP - previousMP));
                                            AudioHelper.PlaySound("E21");
                                        }
                                        else if ((player.MP < previousMP) && (previousMP - player.MP > 75)) AddEvent(string.Format("MP decreased by {0}pts.", previousMP - player.MP));
                                        break;
                                    case CommandMessageType.NotifyGuildName:
                                        UInt16 id = BitConverter.ToUInt16(data, 8);
                                        if (Cache.OwnerCache.ContainsKey(id))
                                        {
                                            Cache.OwnerCache[id].GuildName = Encoding.ASCII.GetString(data, 10, 20).Trim('\0').Replace('_', ' ');
                                            Cache.OwnerCache[id].GuildRank = BitConverter.ToInt16(data, 6);
                                        }
                                        break;
                                    case CommandMessageType.NotifyHunger:
                                        player.Hunger = BitConverter.ToInt16(data, 6);
                                        if (player.Hunger <= 25 && player.Hunger > 20) AddEvent("You are hungry...");
                                        else if (player.Hunger <= 20 && player.Hunger > 15) AddEvent("You are very hungry...");
                                        else if (player.Hunger <= 15 && player.Hunger > 10) AddEvent("You are starving...");
                                        else if (player.Hunger <= 10 && player.Hunger >= 0) AddEvent("You are starving to death!");
                                        break;
                                    case CommandMessageType.NotifyItemRepaired:
                                        int repairIndex = BitConverter.ToInt32(data, 6);
                                        int repairEndurance = BitConverter.ToInt32(data, 10);

                                        if (player.Inventory[repairIndex] != null)
                                        {
                                            player.Inventory[repairIndex].Endurance = repairEndurance;
                                            AddEvent(player.Inventory[repairIndex].FriendlyName + " has been repaired.");
                                        }
                                        break;
                                    case CommandMessageType.NotifyItemEndurance:
                                        int iIndex = BitConverter.ToInt32(data, 6);
                                        int iEndurance = BitConverter.ToInt32(data, 10);

                                        if (player.Inventory[iIndex] != null)
                                            player.Inventory[iIndex].Endurance = iEndurance;
                                        break;
                                    case CommandMessageType.NotifyItemSet:
                                        int itemId = BitConverter.ToInt32(data, 6);
                                        int setNumber = BitConverter.ToInt32(data, 10);

                                        if (player.Inventory[itemId] != null)
                                            player.Inventory[itemId].SetNumber = setNumber;
                                        break;
                                    case CommandMessageType.NotifyItemSetUsed:
                                        int itemSetUsed = BitConverter.ToInt32(data, 6);                             
                                        AddEvent(string.Format("Set {0} equipped!", itemSetUsed)); 
                                        break;
                                    case CommandMessageType.NotifyItemSold:
                                        //AddEvent("Item sold.");
                                        break;
                                    case CommandMessageType.NotifyItemObtained:
                                        int count = BitConverter.ToInt16(data, 6);
                                        int statCount = (int)data[42];
                                        byte[] itemData = new byte[37 + (statCount)*2];
                                        Buffer.BlockCopy(data, 8, itemData, 0, itemData.Length);
                                        int itid = BitConverter.ToInt32(itemData, 2);
                                        Item item = Cache.ItemConfiguration[itid].Copy();
                                        item.ParseData(itemData);

                                        player.Inventory[item.SlotId] = item;
                                        player.InventoryDrawOrder.AddLast(item.SlotId); // v1

                                        if (count == 1) AddEvent(string.Format("You got a {0}{1}.", (item.Quality != ItemQuality.None ? item.Quality.ToString() + " " : ""), item.FriendlyName).Trim());
                                        else AddEvent(string.Format("You got {0} {1}s.", count, Cache.ItemConfiguration[item.ItemId].FriendlyName));
                                        AudioHelper.PlaySound("E20");
                                        break;
                                    case CommandMessageType.NotifyItemBagPositions:
                                        int index = 6;
                                        for (int i = Globals.MaximumEquipment; i < Globals.MaximumTotalItems; i++)
                                            if (player.Inventory[i] != null)
                                            {
                                                Item bagItem = player.Inventory[i];
                                                bagItem.BagX = BitConverter.ToInt16(data, index);
                                                index += 2;
                                                bagItem.BagY = BitConverter.ToInt16(data, index);
                                                index += 2;
                                            }
                                        break;
                                    case CommandMessageType.NotifyItemEquipped:
                                        {
                                            int itemIndex = BitConverter.ToInt16(data, 6);
                                            AddEvent(string.Format("{0} has been equipped.", Cache.ItemConfiguration[player.Inventory[itemIndex].ItemId].FriendlyName));

                                            player.EquipItem(itemIndex);
                                            AudioHelper.PlaySound("E28");
                                            break;
                                        }
                                    case CommandMessageType.NotifyItemUnequipped:
                                        {
                                            int unequipItemIndex = BitConverter.ToInt16(data, 6);
                                            int originalItemIndex = BitConverter.ToInt16(data, 8);
                                            int destinationIndex = BitConverter.ToInt16(data, 10);
                                           
                                            AddEvent(string.Format("{0} has been unequipped.", Cache.ItemConfiguration[player.Inventory[unequipItemIndex].ItemId].FriendlyName));
                                            player.UnEquipItem(unequipItemIndex, originalItemIndex, destinationIndex);



                                            //if (player.Inventory[unequippedIndex] != null)
                                            //{
                                            //    Item unequippedItem = player.Inventory[unequippedIndex];
                                            //    unequippedItem.IsEquipped = false;
                                            //    AddEvent(string.Format("{0} has been unequipped.", Cache.ItemConfiguration[unequippedItem.ItemId].FriendlyName));
                                            //}

                                            break;
                                        }
                                    case CommandMessageType.NotifyItemBroken:
                                        int brokenIndex = BitConverter.ToInt16(data, 6);

                                        if (player.Inventory[brokenIndex] != null)
                                        {
                                            player.Inventory[brokenIndex].Endurance = 0;
                                            AddEvent(string.Format("{0} {1} is broken!", player.Inventory[brokenIndex].Quality.ToString(), player.Inventory[brokenIndex].FriendlyName).Trim());
                                            UnEquipItem(brokenIndex);
                                            AudioHelper.PlaySound("E10");
                                        }
                                        break;
                                    case CommandMessageType.NotifyItemDepleted:
                                        int depletedIndex = BitConverter.ToInt16(data, 6);
                                        bool useItemResult = (BitConverter.ToInt16(data, 8) == 1) ? true : false;

                                        if (player.Inventory[depletedIndex] != null)
                                        {
                                            Item depletedItem = player.Inventory[depletedIndex];
                                            if (player.Inventory[depletedIndex].IsEquipped)
                                            {
                                                AddEvent(string.Format("{0} has been unequipped.", Cache.ItemConfiguration[depletedItem.ItemId].FriendlyName));
                                                player.Inventory[depletedIndex].IsEquipped = false;
                                            }

                                            switch (player.Inventory[depletedIndex].Type)
                                            {
                                                case ItemType.Consume:
                                                case ItemType.Arrow:
                                                        AddEvent(string.Format("{0} is exhausted.", Cache.ItemConfiguration[depletedItem.ItemId].FriendlyName));
                                                    break;
                                                case ItemType.DepleteDestination:
                                                case ItemType.Deplete:
                                                    if (useItemResult)
                                                        AddEvent(string.Format("You used {0}.", Cache.ItemConfiguration[depletedItem.ItemId].FriendlyName));
                                                    break;
                                                case ItemType.Eat:
                                                    if (useItemResult)
                                                    {
                                                        AddEvent(string.Format("You ate {0}.", Cache.ItemConfiguration[depletedItem.ItemId].FriendlyName));
                                                        switch (player.Gender)
                                                        {
                                                            case GenderType.Male: AudioHelper.PlaySound("C19"); break;
                                                            case GenderType.Female: AudioHelper.PlaySound("C20"); break;
                                                        }
                                                    }
                                                    break;
                                                default:
                                                    if (useItemResult)
                                                    {
                                                        AddEvent(string.Format("{0} is broken.", Cache.ItemConfiguration[depletedItem.ItemId].FriendlyName));
                                                        AudioHelper.PlaySound("E10");
                                                    }
                                                    break;
                                            }
                                            player.RemoveFromInventory(depletedIndex);
                                        }
                                        break;
                                    case CommandMessageType.NotifyFly:
                                        player.FlyDirection = (MotionDirection)(int)data[6];
                                        player.FlyDamage = BitConverter.ToInt32(data, 7);
                                        player.FlyDamageType = (DamageType)((int)data[11]);
                                        player.FlyHitCount = (int)data[12];
                                        player.Flying = true;
                                        break;
                                    case CommandMessageType.NotifyDamageStatistics:
                                        player.MinMeleeDamage = BitConverter.ToInt16(data, 6);
                                        player.MaxMeleeDamage = BitConverter.ToInt16(data, 8);
                                        player.MinMeleeDamageCritical = BitConverter.ToInt16(data, 10);
                                        player.MaxMeleeDamageCritical = BitConverter.ToInt16(data, 12);
                                        break;
                                    case CommandMessageType.NotifyParty:
                                        PartyMode mode = PartyMode.Hunting;
                                        PartyAction action = PartyAction.None;
                                        if (Enum.TryParse<PartyAction>(((int)data[6]).ToString(), out action))
                                            switch (action)
                                            {
                                                case PartyAction.Create:
                                                    player.Party = new Party(player.ObjectId);
                                                    player.Party.MemberInfo.Add(player.ObjectId, new PartyMember(player.Name));
                                                    player.Party.MemberInfo[player.ObjectId].X = player.X;
                                                    player.Party.MemberInfo[player.ObjectId].Y = player.Y;
                                                    player.Party.MemberInfo[player.ObjectId].MapName = player.MapName;
                                                    player.Party.MemberInfo[player.ObjectId].HP = player.HP;
                                                    player.Party.MemberInfo[player.ObjectId].MP = player.MP;
                                                    //player.Party.MemberInfo[memberId].SP = BitConverter.ToInt16(data, 38);
                                                    //player.Party.MemberInfo[player.ObjectId].Poison = ((int)data[38] == 1);
                                                    player.Party.MemberInfo[player.ObjectId].MaxHP = player.MaxHP;
                                                    player.Party.MemberInfo[player.ObjectId].MaxMP = player.MaxMP;
                                                    AddEvent("Party created.");
                                                    break;
                                                case PartyAction.Reject:
                                                    switch ((PartyRejectReason)(int)data[7])
                                                    {
                                                        case PartyRejectReason.InParty: AddEvent("Cannot join this party, you are already in a party."); break;
                                                        case PartyRejectReason.InCombatMode: AddEvent("Cannot join the party, combat mode is active."); break;
                                                        case PartyRejectReason.InWrongTown: AddEvent("Cannot join the party, you are in a different nation."); break;
                                                        case PartyRejectReason.RequestPending: AddEvent("Cannot join the party, another party request is pending."); break;
                                                        case PartyRejectReason.PartyFull: AddEvent("Cannot join the party as it is full."); break;
                                                        case PartyRejectReason.Rejected: AddEvent("Party request rejected."); break;
                                                        default: AddEvent("Cannot join the party."); break;
                                                    }
                                                    break;
                                                case PartyAction.Join:
                                                    if (Enum.TryParse<PartyMode>(((int)data[7]).ToString(), out mode))
                                                    {
                                                        player.Party = new Party(BitConverter.ToInt16(data, 8));
                                                        player.Party.Mode = mode;

                                                        int membercount = (int)data[10];
                                                        for (int i = 0; i < membercount; i++)
                                                        {
                                                            int memberId = BitConverter.ToInt16(data, 11 + (i * 35));
                                                            string memberName = Encoding.ASCII.GetString(data, 13 + (i * 35), 10).Trim('\0');
                                                            if (memberId != player.Party.LeaderId) player.Party.AddMember(memberId);
                                                            player.Party.MemberInfo.Add(memberId, new PartyMember(memberName));
                                                            player.Party.MemberInfo[memberId].X = BitConverter.ToInt16(data, 23 + (i * 35));
                                                            player.Party.MemberInfo[memberId].Y = BitConverter.ToInt16(data, 25 + (i * 35));
                                                            player.Party.MemberInfo[memberId].MapName = Encoding.ASCII.GetString(data, 27 + (i*35), 10).Trim('\0');
                                                            player.Party.MemberInfo[memberId].HP = BitConverter.ToInt16(data, 37 + (i * 35));
                                                            player.Party.MemberInfo[memberId].MP = BitConverter.ToInt16(data, 39 + (i * 35));
                                                            //player.Party.MemberInfo[memberId].SP = BitConverter.ToInt16(data, 41 + (i * 35));
                                                            player.Party.MemberInfo[memberId].Poison = ((int)data[41 + (i*35)] == 1);
                                                            player.Party.MemberInfo[memberId].MaxHP = BitConverter.ToInt16(data, 43 + (i * 35));
                                                            player.Party.MemberInfo[memberId].MaxMP = BitConverter.ToInt16(data, 45 + (i * 35));
                                                        }

                                                        AddEvent("You have joined a party. The current party mode is " + mode.ToString() +".");
                                                    }
                                                    break;
                                                case PartyAction.Leave:
                                                    player.Party = null;
                                                    AddEvent("You have left the party.");
                                                    break;
                                                case PartyAction.ChangeMode:
                                                    if (Enum.TryParse<PartyMode>(((int)data[7]).ToString(), out mode))
                                                        if (player.Party != null)
                                                        {
                                                            player.Party.Mode = mode;
                                                            AddEvent("Party mode has changed to " + mode.ToString() + ".");
                                                        }
                                                    break;
                                                case PartyAction.Update:
                                                    PartyAction memberAction = PartyAction.None;
                                                    if (Enum.TryParse<PartyAction>(((int)data[7]).ToString(), out memberAction))
                                                    {
                                                        int memberId = BitConverter.ToInt16(data, 8);
                                                        string memberName;
                                                        switch (memberAction)
                                                        {
                                                            case PartyAction.Join:
                                                                memberName = Encoding.ASCII.GetString(data, 10, 10).Trim('\0');
                                                                player.Party.AddMember(memberId);
                                                                player.Party.MemberInfo.Add(memberId, new PartyMember(memberName));
                                                                AddEvent(memberName + " has joined the party.");
                                                                player.Party.MemberInfo[memberId].X = BitConverter.ToInt16(data, 20);
                                                                player.Party.MemberInfo[memberId].Y = BitConverter.ToInt16(data, 22);
                                                                player.Party.MemberInfo[memberId].MapName = Encoding.ASCII.GetString(data, 24, 10).Trim('\0');
                                                                player.Party.MemberInfo[memberId].HP = BitConverter.ToInt16(data, 34);
                                                                player.Party.MemberInfo[memberId].MP = BitConverter.ToInt16(data, 36);
                                                                //player.Party.MemberInfo[memberId].SP = BitConverter.ToInt16(data, 38);
                                                                player.Party.MemberInfo[memberId].Poison = ((int)data[38] == 1);
                                                                player.Party.MemberInfo[memberId].MaxHP = BitConverter.ToInt16(data, 40);
                                                                player.Party.MemberInfo[memberId].MaxMP = BitConverter.ToInt16(data, 42);
                                                                break;
                                                            case PartyAction.Leave:
                                                                player.Party.RemoveMember(memberId);
                                                                AddEvent(player.Party.MemberInfo[memberId].Name + " has left the party.");
                                                                player.Party.MemberInfo.Remove(memberId);
                                                                break;
                                                            case PartyAction.Info:
                                                                player.Party.MemberInfo[memberId].X = BitConverter.ToInt16(data, 10);
                                                                player.Party.MemberInfo[memberId].Y = BitConverter.ToInt16(data, 12);
                                                                player.Party.MemberInfo[memberId].MapName = Encoding.ASCII.GetString(data, 14, 10).Trim('\0');
                                                                player.Party.MemberInfo[memberId].HP = BitConverter.ToInt16(data, 24);
                                                                player.Party.MemberInfo[memberId].MP = BitConverter.ToInt16(data, 26);
                                                                //player.Party.MemberInfo[memberId].SP = BitConverter.ToInt16(data, 28);
                                                                player.Party.MemberInfo[memberId].Poison = ((int)data[28] == 1);
                                                                player.Party.MemberInfo[memberId].MaxHP = BitConverter.ToInt16(data, 30);
                                                                player.Party.MemberInfo[memberId].MaxMP = BitConverter.ToInt16(data, 32);
                                                                break;
                                                        }
                                                    }
                                                    break;
                                                case PartyAction.Request:
                                                    string requestName = Encoding.ASCII.GetString(data, 8, 10).Trim('\0');
                                                    GameMessageBox box = new GameMessageBox(GameDialogBoxType.PartyRequest, string.Format("{0} wants to join the Party", requestName), "Ok", "No");
                                                    box.ResponseSelected += PartyRequestConfirmation;
                                                    ShowMessageBox(box);
                                                    break;
                                            }
                                        break;
                                    case CommandMessageType.NotifyLevelUp:
                                        player.Level = BitConverter.ToInt32(data, 6);
                                        player.LevelUpPoints = BitConverter.ToInt32(data, 10);

                                        message = new ChatMessage(DateTime.Now);                                  
                                        message.Font = FontType.DamageLargeSize14Bold;
                                        message.Color = GameColor.LevelUp;

                                        if (player.Level == Globals.MaximumLevel)
                                        {
                                            AddEvent("Max level reached!");
                                            message.Message = "Max level reached!";
                                        }
                                        else
                                        {
                                            AddEvent(string.Format("Level {0} reached!", player.Level));
                                            message.Message = string.Format("Level {0} reached!", player.Level);
                                        }

                                        switch (player.Gender)
                                        {
                                            case GenderType.Male: AudioHelper.PlaySound("C21", 0.7f); break;
                                            case GenderType.Female: AudioHelper.PlaySound("C22", 0.7f); break;
                                        }

                                        levelHistory.Add(message);
                                        break;
                                    case CommandMessageType.NotifyStatChangeLevelUpSuccess:
                                        player.Level = BitConverter.ToInt32(data, 6);
                                        player.LevelUpPoints = BitConverter.ToInt32(data, 10);
                                        player.Strength += player.LevelUpStrength;
                                        player.Dexterity += player.LevelUpDexterity;
                                        player.Vitality += player.LevelUpVitality;
                                        player.Intelligence += player.LevelUpIntelligence;
                                        player.Magic += player.LevelUpMagic;
                                        player.Agility += player.LevelUpAgility;

                                        AddEvent("Your stats have been changed.");
                                        break;
                                    case CommandMessageType.NotifyStatChangeLevelUpFailed:
                                        AddEvent("Your stats have not been changed.");
                                        break;
                                    case CommandMessageType.NotifyMagicEffectOn:
                                        {
                                            int magicType = BitConverter.ToInt16(data, 6);
                                            int magicValue = BitConverter.ToInt32(data, 8);

                                            switch ((MagicType)magicType)
                                            {
                                                case MagicType.Protect:
                                                    switch (magicValue)
                                                    {
                                                        case 1: AddEvent("You are completely protected from Arrows!"); break;
                                                        case 2: AddEvent("You are protected from Magic!"); break;
                                                        case 3:
                                                        case 4: AddEvent("Defence ratio increased by a Magic Shield!"); break;
                                                        case 5: AddEvent("You are completely protected from Magic!"); break;
                                                    }
                                                    break;
                                                case MagicType.Paralyze:
                                                    switch (magicValue)
                                                    {
                                                        case 1: AddEvent("You are bound by a Hold Person spell! Unable to move!"); player.IsHeld = true; break;
                                                        case 2: AddEvent("You are bound by a Paralysis spell! Unable to move!"); player.IsParalyzed = true; break;
                                                    }
                                                    break;
                                                case MagicType.Invisibility:
                                                    switch (magicValue)
                                                    {
                                                        case 1: AddEvent("You are now Invisible, no one can see you!"); break;
                                                    }
                                                    break;
                                                case MagicType.Confuse:
                                                    switch (magicValue)
                                                    {
                                                        case 1: AddEvent("No one understand you because of language Confusion Magic!"); break;
                                                        case 2: AddEvent("Confusion magic casted, impossible to determine player allegience!"); break;
                                                        case 3: AddEvent("Illusion magic casted, impossible to tell who is who!"); break;
                                                        case 4: AddEvent("You are thrown into confusion, and are flustered!"); break;
                                                    }
                                                    break;
                                                case MagicType.Poison:
                                                    AddEvent("You have been poisoned!");
                                                    player.IsPoisoned = true;
                                                    break;
                                                case MagicType.Berserk:
                                                    switch (magicValue)
                                                    {
                                                        case 1: AddEvent("Berserk Magic casted!"); break;
                                                    }
                                                    break;
                                                case MagicType.Barbs:
                                                    switch (magicValue)
                                                    {
                                                        case 1: AddEvent("Barbs Magic casted!"); break;
                                                    }
                                                    break;
                                                case MagicType.Ice: AddEvent("You have been frozen! Your movement decreases to 50%."); break;
                                            }
                                        }
                                        break;
                                    case CommandMessageType.NotifyMagicEffectOff: //TODO flush out all of these?
                                        {
                                            int magicType = BitConverter.ToInt16(data, 6);
                                            int magicValue = BitConverter.ToInt32(data, 8);

                                            switch ((MagicType)magicType)
                                            {
                                                case MagicType.Protect:
                                                    switch (magicValue)
                                                    {
                                                        case 1: AddEvent("Protection from arrows as vanished."); break;
                                                        case 2: AddEvent("Protection from magic has vanished."); break;
                                                        case 3:
                                                        case 4: AddEvent("Defence shield effect has vanished."); break;
                                                        case 5: AddEvent("Absolute Magic Protection has vanished."); break;
                                                    }
                                                    break;
                                                case MagicType.Paralyze:
                                                    switch (magicValue)
                                                    {
                                                        case 1: AddEvent("Hold person magic effect has vanished."); player.IsHeld = false; break;
                                                        case 2: AddEvent("Paralysis magic effect has vanished."); player.IsParalyzed = false; break;
                                                    }
                                                    break;
                                                case MagicType.Invisibility:
                                                    switch (magicValue)
                                                    {
                                                        case 1: AddEvent("Invisibility magic effect has vanished."); break;
                                                    }
                                                    break;
                                                case MagicType.Confuse:
                                                    switch (magicValue)
                                                    {
                                                        case 1: AddEvent("Language confuse magic effect has vanished."); break;
                                                        case 2: AddEvent("Confusion magic has vanished."); break;
                                                        case 3: AddEvent("Illusion magic has vanished."); break;
                                                        case 4: AddEvent("At last, you gather your senses."); break;
                                                    }
                                                    break;
                                                case MagicType.Poison:
                                                    AddEvent("Poison cured.");
                                                    player.IsPoisoned = false;
                                                    break;
                                                case MagicType.Berserk:
                                                    switch (magicValue)
                                                    {
                                                        case 1: AddEvent("Berserk mode released."); break;
                                                    }
                                                    break;
                                                case MagicType.Barbs:
                                                    switch (magicValue)
                                                    {
                                                        case 1: AddEvent("Barbs mode released."); break;
                                                    }
                                                    break;
                                                case MagicType.Ice: AddEvent("Freeze effect released."); break;
                                            }
                                            break;
                                        }
                                    case CommandMessageType.NotifySpellUnlearned:
                                        {
                                            int magicIndex = BitConverter.ToInt32(data, 6);
                                            Magic magic = Cache.MagicConfiguration[magicIndex];
                                            if (magicIndex >= 0 && magicIndex < player.MagicLearned.Length && magic != null)
                                            {
                                                if (Cache.MagicConfiguration[magicIndex] != null)
                                                {
                                                    player.MagicLearned[magicIndex] = false;
                                                    AddEvent(string.Format("Magic unlearned: {0}", magic.Name));
                                                }
                                            }
                                            break;
                                        }
                                    case CommandMessageType.NotifyStudySpellFailed:
                                        {

                                            int magicIndex = BitConverter.ToInt32(data, 6);
                                            Magic magic = Cache.MagicConfiguration[magicIndex];
                                            if (magicIndex >= 0 && magicIndex < player.MagicLearned.Length && magic != null)
                                            {
                                                if (Cache.MagicConfiguration[magicIndex] != null)
                                                AddEvent(string.Format("Failed to learn magic: {0}", magic.Name));
                                            }
                                            break;
                                        }
                                    case CommandMessageType.NotifyStudySpellSuccess:
                                        {

                                            int magicIndex = BitConverter.ToInt32(data, 6);
                                            Magic magic = Cache.MagicConfiguration[magicIndex];
                                            if (magicIndex >= 0 && magicIndex < player.MagicLearned.Length && magic != null)
                                            {
                                                if (Cache.MagicConfiguration[magicIndex] != null)
                                                {
                                                    player.MagicLearned[magicIndex] = true;
                                                    AddEvent(string.Format("Magic learned: {0}", magic.Name));
                                                    AudioHelper.PlaySound("E23");
                                                }
                                            }
                                            break;
                                        }
                                    case CommandMessageType.NotifyWeatherChange:
                                        WeatherType weatherType = (WeatherType)data[6];
                                        if (weatherType != WeatherType.Clear)
                                        {
                                            weather = new Weather(weatherType);
                                            weather.Init();
                                        }
                                        else weather = null;
                                        break;
                                    case CommandMessageType.NotifyItemColourChange:
                                        {
                                            int itemIndex = BitConverter.ToInt16(data, 6);
                                            int colour = BitConverter.ToInt32(data, 8);
                                            GameColor colorType = (GameColor)BitConverter.ToInt16(data, 12);

                                            if (colour != -1)
                                            {
                                                if (player.Inventory[itemIndex] != null)
                                                {
                                                    player.Inventory[itemIndex].Colour = colour;
                                                    player.Inventory[itemIndex].ColorType = colorType;
                                                    AddEvent(player.Inventory[itemIndex].FriendlyName + " has been dyed.");
                                                }
                                            }
                                            else AddEvent("That item cannot be dyed.");
                                        }
                                        break;
                                    case CommandMessageType.NotifyExperience: player.Experience = BitConverter.ToInt32(data, 6); break;
                                    case CommandMessageType.NotifySpecialAbilityEnabled:
                                        AddEvent("You can now use a special ability!");
                                        player.SpecialAbilityReady = true;
                                        AudioHelper.PlaySound("E30");
                                        break;
                                    case CommandMessageType.NotifyInventoryFull: AddEvent("Inventory full!"); break;
                                    case CommandMessageType.NotifyWarehouseFull: AddEvent("Warehouse full!"); break;
                                    case CommandMessageType.NotifySpecialAbilityStatus:
                                        {
                                            ItemSpecialAbilityStatus status = (ItemSpecialAbilityStatus)BitConverter.ToInt16(data, 6);
                                            ItemSpecialAbilityType saType; int seconds;

                                            switch (status)
                                            {
                                                case ItemSpecialAbilityStatus.Activated: //TODO -FIX, Doesn't show in game
                                                    saType = (ItemSpecialAbilityType)BitConverter.ToInt16(data, 8);
                                                    seconds = BitConverter.ToInt16(data, 10);

                                                    AddEvent("Special Ability Activated!");
                                                    switch (saType)
                                                    {
                                                        case ItemSpecialAbilityType.IceWeapon: AddEvent(string.Format("Freeze enemy! Lasts {0} {1}!", (seconds >= 60 ? seconds / 60 : seconds), (seconds >= 60 ? "minutes" : "seconds"))); break;
                                                        case ItemSpecialAbilityType.XelimaWeapon: AddEvent(string.Format("Decrease enemy's HP by 50%! Lasts {0} {1}!", (seconds >= 60 ? seconds / 60 : seconds), (seconds >= 60 ? "minutes" : "seconds"))); break;
                                                        case ItemSpecialAbilityType.MedusaWeapon: AddEvent(string.Format("Paralyze enemy! Lasts {0} {1}!", (seconds >= 60 ? seconds / 60 : seconds), (seconds >= 60 ? "minutes" : "seconds"))); break;
                                                        case ItemSpecialAbilityType.MerienArmour: AddEvent(string.Format("Break enemy's weapon! Lasts {0} {1}!", (seconds >= 60 ? seconds / 60 : seconds), (seconds >= 60 ? "minutes" : "seconds"))); break;
                                                        case ItemSpecialAbilityType.MerienShield: AddEvent(string.Format("You are untouchable for {0} {1}!", (seconds >= 60 ? seconds / 60 : seconds), (seconds >= 60 ? "minutes" : "seconds"))); break;
                                                    }

                                                    AudioHelper.PlaySound("E35");
                                                    break;
                                                case ItemSpecialAbilityStatus.Equipped:
                                                    saType = (ItemSpecialAbilityType)BitConverter.ToInt16(data, 8);
                                                    seconds = BitConverter.ToInt16(data, 10);

                                                    AddEvent("Special Ability Equipped!");
                                                    switch (saType)
                                                    {
                                                        case ItemSpecialAbilityType.IceWeapon:    AddEvent(string.Format("Ability that freezes enemy: Can be used after {0} {1}.", (seconds>= 60 ? seconds/60 : seconds), (seconds >=60 ? "minutes" : "seconds"))); break;
                                                        case ItemSpecialAbilityType.XelimaWeapon: AddEvent(string.Format("Ability that decreases enemy's HP by 50%: Can be used after {0} {1}.", (seconds>= 60 ? seconds/60 : seconds), (seconds >=60 ? "minutes" : "seconds"))); break;
                                                        case ItemSpecialAbilityType.MedusaWeapon: AddEvent(string.Format("Ability that paralyzes enemy: Can be used after {0} {1}.", (seconds>= 60 ? seconds/60 : seconds), (seconds >=60 ? "minutes" : "seconds"))); break;
                                                        case ItemSpecialAbilityType.MerienArmour: AddEvent(string.Format("Ability that breaks enemy's weapon: Can be used after {0} {1}.", (seconds>= 60 ? seconds/60 : seconds), (seconds >=60 ? "minutes" : "seconds"))); break;
                                                        case ItemSpecialAbilityType.MerienShield: AddEvent(string.Format("Ability that makes character untouchable: Can be used after {0} {1}.", (seconds>= 60 ? seconds/60 : seconds), (seconds >=60 ? "minutes" : "seconds"))); break;
                                                    }

                                                    AudioHelper.PlaySound("E34");
                                                    break;
                                                case ItemSpecialAbilityStatus.Unequipped:
                                                    AddEvent("Special ability has been released.");
                                                    //player.SpecialAbilityReady = false;
                                                    break;
                                                case ItemSpecialAbilityStatus.Expired:
                                                    AddEvent(string.Format("Special ability has run out! Will be available in {0} minutes.", Globals.SpecialAbilityTime / 60));
                                                    player.SpecialAbilityReady = false;
                                                    break;
                                            }
                                        }
                                        break;
                                    case CommandMessageType.NotifyItemCount:
                                        {
                                            int itemIndex = BitConverter.ToInt16(data, 6);
                                            int itemCount = BitConverter.ToInt32(data, 8);

                                            if (player.Inventory[itemIndex] != null)
                                                player.Inventory[itemIndex].Count = itemCount;
                                        }
                                        break;
                                    case CommandMessageType.NotifyDead:
                                        AddEvent("You have died!  Click the restart button in system menu ");
                                        AddEvent("to start again, or click the Log Out button to exit.");
                                        AddEvent("Your equipment has lost 10% endurance!");
                                        break;
                                    case CommandMessageType.NotifyCrusade:
                                        CrusadeMode crusadeMode = (CrusadeMode)BitConverter.ToInt16(data, 6);
                                        CrusadeDuty crusadeDuty = (CrusadeDuty)BitConverter.ToInt16(data, 8);
                                        switch (crusadeMode) // TODO finish
                                        {
                                            case CrusadeMode.Start:
                                                switch (crusadeDuty)
                                                {
                                                    case CrusadeDuty.None:
                                                        // show duty selection dialog
                                                        break;
                                                    default: break; //
                                                }
                                                break;
                                            case CrusadeMode.End:

                                                break;
                                        }
                                        break;
                                    case CommandMessageType.NotifyResolutionChange: AddEvent("Resolution Changed to " + Cache.DefaultState.Display.Resolution.ToString()); break;
                                    case CommandMessageType.NotifyWhisperOn: AddEvent("Whispering " + Encoding.ASCII.GetString(data, 6, 10).Trim('\0') + "."); break;
                                    case CommandMessageType.NotifyWhisperOff: AddEvent("Whispering mode off."); break;
                                    case CommandMessageType.NotifyReputationFailed:
                                        int repTime = BitConverter.ToInt32(data, 6);

                                        if (repTime <= 0) AddEvent("Must be friendly to give this player reputation.");
                                        else AddEvent(string.Format("You can give reputation in {0} {1}.", (repTime >= 60 ? repTime / 60 : repTime), (repTime >= 60 ? "minutes" : "seconds")));
                                        break;
                                    case CommandMessageType.NotifyReputationSuccess:
                                        bool positive = ((int)data[6] == 1 ? true : false);
                                        string repName = Encoding.ASCII.GetString(data, 7, 10).Trim('\0');
                                        player.Reputation = BitConverter.ToInt32(data, 17);

                                        if (repName.Equals(player.Name))
                                        {
                                            if (positive)
                                            {
                                                AddEvent("A player has given you good reputation!");
                                                AudioHelper.PlaySound("E23");
                                            }
                                            else AddEvent("A player has given you bad reputation...");
                                        }
                                        else
                                        {
                                            if (positive) AddEvent(string.Format("You gave good reputation to {0}!", repName));
                                            else AddEvent(string.Format("You gave bad reputation to {1}...", repName));
                                        }
                                        break;
                                    case CommandMessageType.NotifyItemToWarehouse:
                                        int index1 = BitConverter.ToInt32(data, 6);
                                        int index2 = BitConverter.ToInt32(data, 10);

                                        Item moveItem1 = player.Inventory[index1];
                                        player.Inventory[index1] = null;

                                        if (index2 != -1)
                                        {
                                            if (player.Warehouse[index2] != null)
                                            {
                                                Item moveItem2 = player.Warehouse[index2];
                                                player.Warehouse[index2] = null;

                                                player.Inventory[index1] = moveItem2;
                                            }
                                            player.Warehouse[index2] = moveItem1;
                                        }
                                        else player.AddWarehouse(moveItem1);
                                        break;
                                    case CommandMessageType.NotifyItemFromWarehouse:
                                        int index3 = BitConverter.ToInt32(data, 6);
                                        int index4 = BitConverter.ToInt32(data, 10);

                                        Item moveItem3 = player.Warehouse[index3];
                                        player.Warehouse[index3] = null;

                                        if (index4 != -1)
                                        {
                                            if (player.Inventory[index4] != null)
                                            {
                                                Item moveItem4 = player.Inventory[index4];
                                                player.Inventory[index4] = null;

                                                player.Warehouse[index3] = moveItem4;
                                            }
                                            player.Inventory[index4] = moveItem3;
                                        }
                                        break;
                                    case CommandMessageType.NotifyPublicEventCreated:
                                        publicEventState = PublicEventState.Created;
                                        publicEventParticipation = false;
                                        publicEventType = (PublicEventType)BitConverter.ToInt32(data, 6);
                                        publicEventX = BitConverter.ToInt32(data, 10);
                                        publicEventY = BitConverter.ToInt32(data, 14);
                                        publicEventDifficulty = (PublicEventDifficulty)BitConverter.ToInt32(data, 18);
                                        publicEventStartTime = DateTime.Now.AddSeconds(BitConverter.ToInt32(data, 22));

                                        //AddEvent(publicEventType.ToString() + " event created at " + publicEventX + "," + publicEventY + ". Start time: " + publicEventStartTime.ToString());
                                        AddEvent("A public event has started nearby.");
                                        break;
                                    case CommandMessageType.NotifyPublicEventStarted:
                                        publicEventState = PublicEventState.Started;
                                        publicEventType = (PublicEventType)BitConverter.ToInt32(data, 6);
                                        publicEventX = BitConverter.ToInt32(data, 10);
                                        publicEventY = BitConverter.ToInt32(data, 14);
                                        publicEventDifficulty = (PublicEventDifficulty)BitConverter.ToInt32(data, 18);
                                        publicEventEndTime = DateTime.Now.AddSeconds(BitConverter.ToInt32(data, 22));
                                        publicEventParticipation = (BitConverter.ToInt32(data, 26) == 1);

                                        if (publicEventParticipation)
                                            AddEvent("You have joined the public event.");
                                        /*else AddEvent("Event started. End time: " + publicEventEndTime.ToString());*/
                                        break;
                                    case CommandMessageType.NotifyPublicEventEnded:
                                        publicEventState = PublicEventState.None;
                                        publicEventParticipation = false;
                                        publicEventType = PublicEventType.None;
                                        publicEventX = publicEventY = -1;
                                        publicEventDifficulty = PublicEventDifficulty.None;

                                        AddEvent("The public event ended.");
                                        break;
                                    default:
                                        if ((int)type != 0)
                                        {
                                            CommandType test = type;
                                            CommandMessageType test2 = messageType;
                                        }
                                        break;
                                }
                            break;
                        case CommandType.ResponseMotion:
                            if (Enum.TryParse<CommandMessageType>(messageTypeTemp.ToString(), out messageType))
                                switch (messageType)
                                {
                                    case CommandMessageType.ObjectConfirmMove:
                                        player.Titles[TitleType.DistanceTravelled]++;

                                        x = BitConverter.ToInt16(data, 6);
                                        y = BitConverter.ToInt16(data, 8);
                                        // direction 10 - should be for pannning?
                                        // spchange 11
                                        // occupystatus 12
                                        // hp 13,14,15,16

                                        int count = 17; int header;
                                        int cells = BitConverter.ToInt16(data, count);
                                        count += 2;

                                        for (int i = 1; i <= cells; i++)
                                        {
                                            x = Cache.MapView.PivotX + Globals.OffScreenCells + BitConverter.ToInt16(data, count);
                                            count += 2;
                                            y = Cache.MapView.PivotY + Globals.OffScreenCells + BitConverter.ToInt16(data, count);
                                            count += 2;
                                            header = (int)data[count];
                                            count++;

                                            if ((header & 0x01) == 0 ? false : true)
                                            {
                                                objectId = BitConverter.ToUInt16(data, count);
                                                count += 2;
                                                IOwner owner = (objectId == player.ObjectId) ? (IOwner)player : ((Cache.OwnerCache.ContainsKey(objectId)) ? (Owner)Cache.OwnerCache[objectId] : new Owner(objectId));
                                                owner.Parse(data, count, out count);
                                                owner.Map = Cache.MapView.Map;

                                                if (!Cache.OwnerCache.ContainsKey(objectId))
                                                    owner.Init(x, y);
                                                else
                                                {
                                                    //owner.Idle(direction);
                                                    owner.SetLocation(owner.Map, x, y);
                                                }
                                            }
                                            if ((header & 0x02) == 0 ? false : true)
                                            {
                                                objectId = BitConverter.ToUInt16(data, count);
                                                count += 2;
                                                IOwner owner = (objectId == player.ObjectId) ? (IOwner)player : ((Cache.OwnerCache.ContainsKey(objectId)) ? (Owner)Cache.OwnerCache[objectId] : new Owner(objectId));
                                                owner.Parse(data, count, out count);
                                                owner.Map = Cache.MapView.Map;

                                                if (!Cache.OwnerCache.ContainsKey(objectId))
                                                    owner.Init(x, y);
                                                else
                                                {
                                                    //owner.Idle(direction);
                                                    owner.SetLocation(owner.Map, x, y);
                                                }
                                            }
                                            if ((header & 0x04) == 0 ? false : true)
                                            {
                                                Cache.MapView.Map[y][x].ItemSprite = BitConverter.ToInt16(data, count);
                                                count += 2;
                                                Cache.MapView.Map[y][x].ItemSpriteFrame = BitConverter.ToInt16(data, count);
                                                count += 2;
                                                Cache.MapView.Map[y][x].ItemSpriteColour = BitConverter.ToInt32(data, count);
                                                count += 4;
                                                Cache.MapView.Map[y][x].ItemColorType = (GameColor)BitConverter.ToInt16(data, count);
                                                count += 2;
                                            }
                                            if ((header & 0x08) == 0 ? false : true)
                                            {
                                                int id = BitConverter.ToInt16(data, count);
                                                count += 2;
                                                int objectType = BitConverter.ToInt16(data, count);
                                                count += 2;

                                                if (!Cache.DynamicObjects.ContainsKey(id))
                                                {
                                                    DynamicObject dynamicObject = new DynamicObject(id, (DynamicObjectType)objectType);
                                                    dynamicObject.Map = player.Map;
                                                    dynamicObject.Init(x, y);
                                                }
                                            }
                                        }

                                        
                                        break;
                                    case CommandMessageType.ObjectRejectMove: 
                                        // bump / rubberband
                                        player.ObjectId = BitConverter.ToUInt16(data, 6);
                                        x = BitConverter.ToInt16(data, 8);
                                        y = BitConverter.ToInt16(data, 10);
                                        player.Type = BitConverter.ToInt16(data, 12);
                                        player.Direction = (MotionDirection)(int)data[14];
                                        player.Name = Encoding.ASCII.GetString(data, 15, 10).Trim('\0');
                                        player.Appearance1 = BitConverter.ToInt16(data, 25);
                                        player.Appearance2 = BitConverter.ToInt16(data, 27);
                                        player.Appearance3 = BitConverter.ToInt16(data, 29);
                                        player.Appearance4 = BitConverter.ToInt16(data, 31);
                                        player.AppearanceColour = BitConverter.ToInt32(data, 33);
                                        player.Status = BitConverter.ToInt32(data, 37);

                                        player.Idle(player.Direction, true);
                                        player.SetLocation(player.Map, x, y);
                                        Cache.MapView.SetPivot(x, y);

                                        switch (player.Gender)
                                        {
                                            case GenderType.Male: AudioHelper.PlaySound("C12"); break;
                                            case GenderType.Female: AudioHelper.PlaySound("C13"); break;
                                        }
                                        break;
                                }
                            player.Commands--;
                            break;
                        case CommandType.LogEvent:
                        case CommandType.Motion:
                            if (Enum.TryParse<CommandMessageType>(messageTypeTemp.ToString(), out messageType))
                            {
                                UInt16 id = BitConverter.ToUInt16(data, 6);
                                OwnerType ownerType = (OwnerType)(int)data[10];
                                if (ownerType == OwnerType.Npc) id += 10000; // local id store
                                IOwner owner;

                                //clear mobs with same ids
                                if (messageType == CommandMessageType.Confirm && Cache.OwnerCache.ContainsKey(id))
                                    Cache.OwnerCache[id].Remove();

                                if (Cache.OwnerCache.ContainsKey(id))
                                    owner = Cache.OwnerCache[id];
                                else owner = new Owner(id);

                                owner.Map = Cache.MapView.Map;
                                owner.ParseMotion(messageType, data);
                            }
                            break;
                        case CommandType.Chat:
                            {
                                message = new ChatMessage(DateTime.Now);
                                message.OwnerId = BitConverter.ToUInt16(data, 4);
                                message.OwnerX = BitConverter.ToInt16(data, 6);
                                message.OwnerY = BitConverter.ToInt16(data, 8);
                                message.OwnerName = Encoding.ASCII.GetString(data, 10, 10).Trim('\0');
                                message.Color = (GameColor)((int)data[20]);
                                message.Font = FontType.GeneralSize10;
                                message.Message = Encoding.ASCII.GetString(data, 21, data.Length - 21).Trim('\0');

                                // if player cached, then add the message (as they are nearby)
                                //if (message.Type == ChatType.Local)
                                //{
                                    if (Cache.OwnerCache.ContainsKey(message.OwnerId))
                                        Cache.OwnerCache[message.OwnerId].ChatHistory = message; 
                                //}
                                
                                if (message.Color == GameColor.GameMaster && message.OwnerName.Equals("Server"))
                                    AddEvent(message.Message);
                                else if (message.Color != GameColor.Local) chatHistory.Add(message);

                                chatMessages.Add(message);
                                if (chatMessages.Count > Globals.MaximumChatHistory) chatMessages.RemoveAt(0);
                            }
                            break;
                        default:
                            if ((int)type != 0)
                            {
                                CommandType test3 = type;
                            }
                            break;
                    }
                }
            }
            catch (Exception ex) { 
                Console.WriteLine(ex.ToString()); 
            }
        }

        public void PartyRequestConfirmation(GameDialogBoxType type, MessageBoxResponse response)
        {
            byte[] command = new byte[27];
            Buffer.BlockCopy(Utility.GetCommandTypeBytes(CommandType.Common), 0, command, 0, 4);
            Buffer.BlockCopy(Utility.GetCommandTypeBytes(CommandMessageType.HandlePartyRequest), 0, command, 4, 2);
            command[6] = (byte)(response == MessageBoxResponse.Yes ? 1 : 0);

            byte[] data = Utility.Encrypt(command, false);
            if (gameServer != null) gameServer.Send(data);

            HideMessageBox(type);
        }

        public void JoinParty()
        {
            byte[] command = new byte[15];
            Buffer.BlockCopy(Utility.GetCommandTypeBytes(CommandType.Common), 0, command, 0, 4);
            Buffer.BlockCopy(Utility.GetCommandTypeBytes(CommandMessageType.PartyRequest), 0, command, 4, 2);
            command[6] = (byte)(int)PartyRequestType.Join;
            Buffer.BlockCopy(selectedObjectId.GetBytes(), 0, command, 7, 4);

            byte[] data = Utility.Encrypt(command, false);
            if (gameServer != null) gameServer.Send(data);
        }

        public void LeaveParty()
        {
            byte[] command = new byte[15];
            Buffer.BlockCopy(Utility.GetCommandTypeBytes(CommandType.Common), 0, command, 0, 4);
            Buffer.BlockCopy(Utility.GetCommandTypeBytes(CommandMessageType.PartyRequest), 0, command, 4, 2);
            command[6] = (byte)(int)PartyRequestType.Withdraw;
            //Buffer.BlockCopy(selectedObjectId.GetBytes(), 0, command, 7, 4);

            byte[] data = Utility.Encrypt(command, false);
            if (gameServer != null) gameServer.Send(data);
        }

        public void TradeRequestConfirmation(GameDialogBoxType type, MessageBoxResponse response)
        {
            byte[] command = new byte[20];
            Buffer.BlockCopy(Utility.GetCommandTypeBytes(CommandType.Common), 0, command, 0, 4);
            Buffer.BlockCopy(Utility.GetCommandTypeBytes(CommandMessageType.TradeResponse), 0, command, 4, 2);
            Buffer.BlockCopy(player.TradePartnerID.GetBytes(), 0, command, 6, 4);
            command[10] = (byte)(response == MessageBoxResponse.Yes ? 1 : 0);

            byte[] data = Utility.Encrypt(command, false);
            if (gameServer != null) gameServer.Send(data);


            switch (response)
            {
                case MessageBoxResponse.Yes:
                    dialogBoxes[GameDialogBoxType.TradeV2].Show();
                    break;
                case MessageBoxResponse.No: 
                    EndTrade(TradeState.Rejected); 
                    break;
            }

            HideMessageBox(type);
        }

        public List<int[]> GetMovePath(int sourceX, int sourceY, int destinationX, int destinationY)
        {

            List<int[]> moveLocations = new List<int[]>();
            //Return if no path
            if (sourceX == destinationX && sourceY == destinationY)
            {
                return moveLocations;
            }

            //Order is X, Y in int[]
            int[] tempLocation = new int[2] { sourceX, sourceY };
            //A way to do it without using while?
            while (tempLocation[0] != destinationX || tempLocation[1] != destinationY)
            {
                //Get a direction
                MotionDirection direction = Utility.GetNextDirection(tempLocation[0], tempLocation[1], destinationX, destinationY);

                //Check if tile is blocked
                MotionDirection newDirection = FindNextPathFindingDirection(direction, tempLocation);

                switch (newDirection)
                {
                    //TODO make more readable
                    case MotionDirection.North: tempLocation[1] -= 1; break;
                    case MotionDirection.NorthEast: tempLocation[0] += 1; tempLocation[1] -= 1; break;
                    case MotionDirection.East: tempLocation[0] += 1; break;
                    case MotionDirection.SouthEast: tempLocation[0] += 1; tempLocation[1] += 1; break;
                    case MotionDirection.South: tempLocation[1] += 1; break;
                    case MotionDirection.SouthWest: tempLocation[0] -= 1; tempLocation[1] += 1; break;
                    case MotionDirection.West: tempLocation[0] -= 1; break;
                    case MotionDirection.NorthWest: tempLocation[0] -= 1; tempLocation[1] -= 1; break;
                    case MotionDirection.None: return moveLocations;
                }

                //Check for duplication
                foreach (int[] location in moveLocations)
                {
                    if (location[0] == tempLocation[0] && location[1] == tempLocation[1])  return moveLocations; 
                }

                if (newDirection != MotionDirection.None) moveLocations.Add(new int[2] { tempLocation[0], tempLocation[1] });
            }
            return moveLocations;
        }

        //Used for pathfinding overlay
        //TODO, change to return an cords of next tile?
        //TODO find multiple paths, compare length, return shortest?
        public MotionDirection FindNextPathFindingDirection(MotionDirection direction, int[] mapTileCords)
        {
            int x = mapTileCords[0];
            int y = mapTileCords[1];

            if (player.Map.NextLocationIsDestination(direction, x, y, player.DestinationX, player.DestinationY) && !player.Map.LocationCanMove(direction, x, y)) { return MotionDirection.None; }
            else if (player.Map.LocationCanMove(direction, x, y)) { return direction; }
            else if (direction != MotionDirection.North && player.Map.LocationCanMove(direction - 1, x, y)) { return direction - 1; }
            else if (direction == MotionDirection.North && player.Map.LocationCanMove(MotionDirection.NorthWest, x, y)) { return MotionDirection.NorthWest; }
            else if (direction != MotionDirection.NorthWest && player.Map.LocationCanMove(direction + 1, x, y)) { return direction + 1; }
            else if (direction == MotionDirection.NorthWest && player.Map.LocationCanMove(MotionDirection.North, x, y)) { return MotionDirection.North; }

            //Check left and right for movable tile
            //else if ((direction != MotionDirection.North || direction != MotionDirection.NorthEast) && player.Map.LocationCanMove(direction - 2, x, y)) { return direction - 2; }
            //else if (direction == MotionDirection.North && player.Map.LocationCanMove(MotionDirection.West, x, y)) { return MotionDirection.West; }
            //else if (direction == MotionDirection.NorthEast && player.Map.LocationCanMove(MotionDirection.NorthWest, x, y)) { return MotionDirection.NorthWest; }

            //else if ((direction != MotionDirection.NorthWest || direction != MotionDirection.West) && player.Map.LocationCanMove(direction + 2, x, y)) { return direction + 2; }
            //else if (direction == MotionDirection.West && player.Map.LocationCanMove(MotionDirection.North, x, y)) { return MotionDirection.North; }
            //else if (direction == MotionDirection.NorthWest && player.Map.LocationCanMove(MotionDirection.NorthEast, x, y)) { return MotionDirection.NorthEast; }

            else return MotionDirection.None;
        }
       
        //Used for player movement
        public MotionDirection FindNextMovableDirection(MotionDirection direction)
        {
            if (player.Map.NextLocationIsDestination(direction, player.X, player.Y, player.DestinationX, player.DestinationY) && !player.CanMove(direction)) { return MotionDirection.None; }
            else if (player.CanMove(direction)) { return direction; }
            else if (direction != MotionDirection.North && player.CanMove(direction - 1)) { return direction - 1; }
            else if (direction == MotionDirection.North && player.CanMove(MotionDirection.NorthWest)) { return MotionDirection.NorthWest; }
            else if (direction != MotionDirection.NorthWest && player.CanMove(direction + 1)) { return direction + 1; }
            else if (direction == MotionDirection.NorthWest && player.CanMove(MotionDirection.North)) { return MotionDirection.North; }

            //Check left and right for moveable tiles
            //else if ((direction != MotionDirection.North || direction != MotionDirection.NorthEast) && player.CanMove(direction - 2)) { return direction - 2; }
            //else if (direction == MotionDirection.North && player.CanMove(MotionDirection.West)) { return MotionDirection.West; }
            //else if (direction == MotionDirection.NorthEast && player.CanMove(MotionDirection.NorthWest)) { return MotionDirection.NorthWest; }

            //else if ((direction != MotionDirection.NorthWest || direction != MotionDirection.West) && player.CanMove(direction + 2)) { return direction + 2; }          
            //else if (direction == MotionDirection.West && player.CanMove(MotionDirection.North)) { return MotionDirection.North; }
            //else if (direction == MotionDirection.NorthWest && player.CanMove(MotionDirection.NorthEast)) { return MotionDirection.NorthEast; }

            else return MotionDirection.None;
        }

        public void HUDResetConfirmation(GameDialogBoxType type, MessageBoxResponse response)
        {
            switch (response)
            {
                case MessageBoxResponse.Yes:
                    /* UPDATE DIALOGS */
                    foreach (KeyValuePair<GameDialogBoxType, IGameDialogBox> box in dialogBoxes)
                    {
                        box.Value.Config.DefaultDialogValues(box.Key, display.Resolution);
                    }

                    //foreach (IGameDialogBox box in dialogBoxes.Values)
                    //    box.Config.DefaultDialogValues(display.Resolution);

                    /* UPDATE CHAT BOX */
                    switch (display.Resolution)
                    {
                        case Resolution.Classic:
                            chatBox.Y = 400;
                            chatBoxBackground = new Rectangle(0, 400, Display.ResolutionWidth, 20);
                            break;
                        case Resolution.Standard:
                            chatBox.Y = 520;
                            chatBoxBackground = new Rectangle(0, 520, Display.ResolutionWidth, 20);
                            break;
                        case Resolution.Large:
                            chatBox.Y = 668;
                            chatBoxBackground = new Rectangle(0, 668, Display.ResolutionWidth, 20);
                            break;
                    }
                    break;
                case MessageBoxResponse.No:

                    break;
            }

            //Force lock?
            Cache.GameSettings.LockedDialogs = true;
            SaveDialogBoxConfiguration();

            HideMessageBox(type);
        }

        private bool LoadHotKeyConfiguration()
        {
            try
            {
                string configPath = Path.GetDirectoryName(Assembly.GetExecutingAssembly().GetName().CodeBase).Replace(@"file:\", "") + @"\Configs\hotkey.xml";

                Cache.HotKeyConfiguration = ConfigurationHelper.LoadHotKeys(configPath);
                Cache.HotBarConfiguration = ConfigurationHelper.LoadHotBar(configPath);

                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public void SaveHotKeyConfiguration()
        {
            string configPath = Path.GetDirectoryName(Assembly.GetExecutingAssembly().GetName().CodeBase).Replace(@"file:\", "") + @"\Configs\hotkey.xml";
            ConfigurationHelper.SaveHotKeys(configPath, Cache.HotKeyConfiguration);
        }

        //TODO look over this
        private void SaveDialogBoxConfiguration()
        {
            if (!resolutionChange)
            {
                if (dialogBoxes != null)
                {
                    Dictionary<GameDialogBoxType, GameDialogBoxConfiguration> configDictionary = new Dictionary<GameDialogBoxType, GameDialogBoxConfiguration>();
                    foreach (KeyValuePair<GameDialogBoxType, IGameDialogBox> box in dialogBoxes)
                    {
                        switch (box.Key)
                        {
                            case GameDialogBoxType.None:
                            case GameDialogBoxType.TradeV2Confirmation:
                            case GameDialogBoxType.PartyRequest:
                            case GameDialogBoxType.Informational:
                            case GameDialogBoxType.HUDResetConfirmation: break;
                            default: configDictionary.Add(box.Key, box.Value.Config); break;
                        }
                    }

                    Cache.DialogBoxStates[(int)Display.Resolution] = configDictionary;

                    if (Cache.LoadedDialogBoxStatesArray.ContainsKey(player.Name))
                    {
                        Cache.LoadedDialogBoxStatesArray[player.Name] = Cache.DialogBoxStates;
                    }
                    else
                    {
                        Cache.LoadedDialogBoxStatesArray.Add(player.Name, Cache.DialogBoxStates);
                    }
                }
                string configPath = Path.GetDirectoryName(Assembly.GetExecutingAssembly().GetName().CodeBase).Replace(@"file:\", "") + @"\Configs\dialogs.xml";
                ConfigurationHelper.SaveDialogLocations(configPath, Cache.LoadedDialogBoxStatesArray);
            }
        }

        public void SaveGameSettings()
        {
            string configPath = Path.GetDirectoryName(Assembly.GetExecutingAssembly().GetName().CodeBase).Replace(@"file:\", "") + @"\Configs\gamesettings.xml";
            ConfigurationHelper.SaveGameSettings(configPath);
        }

        private RenderTarget2D GameBoardRenderTargetSetUp(RenderTarget2D renderTarget)
        {
            /* Set Game Board Render Target Size */

            //Get the current presentation parameters
            PresentationParameters pp = display.Device.PresentationParameters;
            renderTarget = new RenderTarget2D(display.Device, display.GameBoardWidth, display.GameBoardHeight, false, pp.BackBufferFormat, pp.DepthStencilFormat, pp.MultiSampleCount, RenderTargetUsage.PreserveContents);
            return renderTarget;
        }

        private void LoadDefaultDialogBoxConfigurations() 
        {
            for (int i = 0; i < Enum.GetValues(typeof(Resolution)).Length; i++) //for all resolutions
            {
                GameDialogBoxConfiguration config;
                foreach (GameDialogBoxType type in Enum.GetValues(typeof(GameDialogBoxType))) //for all dialogbox types
                {
                    switch (type)
                    {
                        case GameDialogBoxType.None:
                        case GameDialogBoxType.HUDResetConfirmation:
                        case GameDialogBoxType.Informational:
                        case GameDialogBoxType.PartyRequest:
                        case GameDialogBoxType.TradeV2Confirmation: break;
                        default:
                            {
                                config = new GameDialogBoxConfiguration(type, (Resolution)i);
                                Cache.DialogBoxStates[i].Add(type, config);
                                break; //create default values break;
                            }
                    }
     
                }
            }
        }

        private void LoadSavedDialogBoxLocations()
        {
            /* LOAD SAVED DIALOG LOCATIONS */
            string configPath = Path.GetDirectoryName(Assembly.GetExecutingAssembly().GetName().CodeBase).Replace(@"file:\", "") + @"\Configs\dialogs.xml";
            //Dictionary<DialogBoxType, GameDialogBoxConfiguration> dialogLocations = ConfigurationHelper.LoadDialogLocations(configPath);
            Cache.LoadedDialogBoxStatesArray = ConfigurationHelper.LoadDialogLocations(configPath);
            if (Cache.LoadedDialogBoxStatesArray.ContainsKey(player.Name)) //if has, overwrite any dialog configs with new data
            {
                Dictionary<GameDialogBoxType, GameDialogBoxConfiguration>[] loadedConfigurationStates = Cache.LoadedDialogBoxStatesArray[player.Name];

                for (int i = 0; i < loadedConfigurationStates.Length; i++) //Go through all resolutions
                {
                    foreach (KeyValuePair<GameDialogBoxType, GameDialogBoxConfiguration> config in loadedConfigurationStates[i]) //try to load for all default dialogboxes
                    {
                        if (Cache.DialogBoxStates[i].ContainsKey(config.Key)) //Has dialogbox type
                        {
                            GameDialogBoxConfiguration loadedValue;

                            if (loadedConfigurationStates[i].TryGetValue(config.Key, out loadedValue))
                            {
                                //Forced defaults of saved dialogboxes
                                if (config.Key == GameDialogBoxType.UpgradeV2)
                                    loadedValue.State = DialogBoxState.Normal;

                                Cache.DialogBoxStates[i][config.Key] = loadedValue;
                            }
                        } 
                    }
                }
            }

            Cache.LoadedDialogBoxStatesArray[player.Name] = Cache.DialogBoxStates;
            SaveDialogBoxConfiguration();
        }

        private void InitializeDialogBoxes()
        {
            //Visiblity problems on loaded dialogboxes. Check if saving correctly first, it should be.       

            highlightedDialogBox = clickedDialogBox = GameDialogBoxType.None;

            IGameDialogBox dialogBox;

            dialogBoxes = new Dictionary<GameDialogBoxType, IGameDialogBox>();
            dialogBoxDrawOrder = new LinkedList<GameDialogBoxType>();  

            //dialogBoxes = new Dictionary<GameDialogBoxType, IGameDialogBox>(); //current game dialogboxes
            //dialogBoxDrawOrder = new LinkedList<GameDialogBoxType>();   //draw order of current game dialogboxes

            //if order matters (arrange the enum in order of bottom to top visual shown?)
            //for (int i = 0; i < Enum.GetValues(typeof(GameDialogBoxType)).Length; i++)
            //{
            //    GameDialogBoxType type = (GameDialogBoxType)i;
            //    dialogBox = Cache.DialogBoxStates[(int)display.Resolution][type].DialogBoxFactory(type);
            //    dialogBoxes.Add(type, dialogBox);
            //    dialogBoxDrawOrder.AddFirst(type);
            //}

            //if order doesnt matter
            foreach (KeyValuePair<GameDialogBoxType, GameDialogBoxConfiguration> config in Cache.DialogBoxStates[(int)Display.Resolution]) //current configs for all gameDialogBoxTypes
            {
                dialogBox = config.Value.DialogBoxFactory(config.Key); //Make correct Dialog
                dialogBoxes.Add(config.Key, dialogBox); //Add to current game dialogboxes
                dialogBoxDrawOrder.AddFirst(config.Key); //Add to draworder 
            }
        }

        private void UpdateDialogBoxes()
        {
            foreach (KeyValuePair<GameDialogBoxType, GameDialogBoxConfiguration> config in Cache.DialogBoxStates[(int)Display.Resolution])
            {
                if (dialogBoxes[config.Key] != null)
                {
                    dialogBoxes[config.Key].Config = config.Value;
                }
            }
        }

        public void AddEvent(string message)
        {
            if (eventHistory.Count + 1 > Globals.MaximumEventHistory)
                eventHistory.RemoveAt(0);

            eventHistory.Add(new EventMessage(message, DateTime.Now));
        }

        public void AddEffect(IGameEffect e) { AddEffect(e, Cache.MapView.PivotX, Cache.MapView.PivotY); }
        public void AddEffect(IGameEffect e, int pivotX, int pivotY)
        {
            e.Pivot = new Location(pivotX, pivotY);
            Cache.DefaultState.Effects.Add(e);
        }

        public bool IsComplete { get { return isComplete; } }
        public bool Back { get { return back; } }
        public DefaultState State { get { return DefaultState.Playing; } }
        public GameDisplay Display { get { return display; } set { display = value; } }
        public Player Player { get { return player; } set { player = value; } }
        public Weather Weather { get { return weather; } }
        public List<ChatMessage> ChatMessages { get { return chatMessages; } }
        public Dictionary<GameDialogBoxType,IGameDialogBox> DialogBoxes { get { return dialogBoxes; } }
        public List<IGameEffect> Effects { get { return effects; } set { effects = value; } }
        public bool TileOverLayMode { get { return tileoverlaymode; } set { tileoverlaymode = value; } }
        public LinkedList<GameDialogBoxType> DialogBoxDrawOrder { get { return dialogBoxDrawOrder; } set { dialogBoxDrawOrder = value; } }
        public bool ResolutionChange { get { return resolutionChange; } set { resolutionChange = value; } }  
        public LinkedList<BackGroundImage> BackgroundImages { get { return backgroundImages; } set { backgroundImages = value; } }
        public RenderTarget2D MainRenderTarget { get { return mainRenderTarget; } set { mainRenderTarget = value; } }
        public RenderTarget2D RenderTargetObjects { get { return renderTargetObjects; } set { renderTargetObjects = value; } }
        public PublicEventState PublicEventState { get { return publicEventState; } }
        public int PublicEventX { get { return publicEventX; } }
        public int PublicEventY { get { return publicEventY; } }
        public GameDialogBoxType ClickedDialogBox { get { return clickedDialogBox; } set { clickedDialogBox = value; } }
        public GameDialogBoxType HighlightedDialogBox { get { return highlightedDialogBox; } }
        public int SelectedObjectId { get { return selectedObjectId; } }
        public DraggedType DraggedType { get { return draggedType; } }
    }
}
