﻿using System.Collections.Generic;
using Client.Assets.UI;
using Helbreath.Common;

namespace Client.Assets.State
{
    public interface IMenuState
    {
        Dictionary<MenuDialogBoxType, IMenuDialogBox> DialogBoxes { get; }
        LinkedList<MenuDialogBoxType> DialogBoxDrawOrder { get; set; }
        void BringToFront(MenuDialogBoxType type);
        void HideMessageBox(MenuDialogBoxType type);
        MenuDialogBoxType ClickedDialogBox { get; set; }
        void ShowMessageBox(MenuMessageBox box);
    }
}
