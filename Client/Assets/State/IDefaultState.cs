﻿using System.Collections.Generic;
using Client.Assets.Effects;
using Helbreath.Common;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;



namespace Client.Assets.State
{
    public interface IDefaultState
    {
        void Init(GameWindow window);
        void Update(GameTime gameTime, Microsoft.Xna.Framework.Game game);
        void Draw(SpriteBatch spriteBatch, GameTime gameTime);
        void AddEvent(string message);
        DefaultState State { get; }
        GameDisplay Display { get; set; }
        bool IsComplete { get; }
        bool ResolutionChange { get; set; }
        bool Back { get; }
        void AddEffect(IGameEffect e);
        void AddEffect(IGameEffect e, int pivotX, int pivotY);
        List<IGameEffect> Effects { get; set; }
        RenderTarget2D MainRenderTarget { get; set; }
    }
}
