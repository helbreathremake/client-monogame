﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Client.Assets
{
    public class EventMessage
    {
        private string message;
        private DateTime messageTime;

        public EventMessage(string message, DateTime messageTime)
        {
            this.message = message;
            this.messageTime = messageTime;
        }

        public string Message { get { return message; } }
        public DateTime MessageTime { get { return messageTime; } }
    }
}
