﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;

using Microsoft.Xna.Framework.Graphics;

using Client.Assets.State;

namespace Client.Assets
{
    public static class MapHelper
    {
        public static readonly int MiniMapTempScaleFactor = 8; // resizes tiles to this factor to improve minimap generation performance

        public static void LoadMiniMap(string mapName, Map map)
        {
            bool miniMap = false; bool zoomMap = false;
            if (Cache.MiniMaps.ContainsKey(mapName)) { map.MiniMap = Cache.MiniMaps[mapName]; miniMap = true;} // pull from cache
            if (Cache.ZoomMaps.ContainsKey(mapName)) { map.ZoomMap = Cache.ZoomMaps[mapName]; zoomMap = true; }

            if (!miniMap || !zoomMap) // no need to regenerate
            {   // generate
                GenerateMiniMapAndZoomMap(new object[] { mapName, map, 128, 128 });
                //ThreadPool.QueueUserWorkItem(new WaitCallback(GenerateMiniMapAndZoomMap), new object[] { mapName, map, 128, 128 });
            }
        }

        // resize at the end of each iteration - slow on big maps
        private static void GenerateMiniMapAndZoomMap(object o) { object[] objects = (object[])o; GenerateMiniMapAndZoomMap(objects[0].ToString(), (Map)objects[1], (int)objects[2], (int)objects[3]); }
        private static void GenerateMiniMapAndZoomMap(string mapName, Map map, int imageWidth, int imageHeight)
        {
            try
            {
                Bitmap fullMap = new Bitmap(map.Width * (32 / MapHelper.MiniMapTempScaleFactor), map.Height * (32 / MapHelper.MiniMapTempScaleFactor));
                int rowsPerIteration = 5;

                for (int rowIndex = 0; rowIndex < map.Height; rowIndex += rowsPerIteration)
                {
                    using (Graphics g = Graphics.FromImage(fullMap))
                    {
                        for (int y = rowIndex; y < rowIndex + rowsPerIteration; y++)
                            for (int x = 0; x < map.Width; x++)
                            {
                                if (x < map.Width && y < map.Height &&
                                    Cache.Tiles.ContainsKey(map[y][x].Sprite) &&
                                    Cache.Tiles[map[y][x].Sprite].MiniMapImage != null)
                                {
                                    AnimationFrame frame = Cache.Tiles[map[y][x].Sprite].Frames[map[y][x].SpriteFrame];
                                    Rectangle frameBox = new Rectangle(frame.X / MapHelper.MiniMapTempScaleFactor, frame.Y / MapHelper.MiniMapTempScaleFactor, frame.Width / MapHelper.MiniMapTempScaleFactor, frame.Height / MapHelper.MiniMapTempScaleFactor);
                                    g.DrawImage(Cache.Tiles[map[y][x].Sprite].MiniMapImage, x * (32 / MapHelper.MiniMapTempScaleFactor), y * (32 / MapHelper.MiniMapTempScaleFactor), frameBox, GraphicsUnit.Pixel);
                                }
                            }

                        g.Flush();
                    }

                    Bitmap miniMap = ResizeImage(fullMap, imageWidth, imageHeight); // minimap is usally always 128x128
                    Bitmap zoomMap = ResizeImage(fullMap, map.Width, map.Height); // zoom map is 1x1 pixel per tile

                    // convert minimap to a texture2d
                    using (MemoryStream stream = new MemoryStream())
                    {
                        miniMap.Save(stream, System.Drawing.Imaging.ImageFormat.Png);
                        stream.Seek(0, SeekOrigin.Begin);

                        Texture2D mm = Texture2D.FromStream(Cache.DefaultState.Display.Device, stream);
                        if (Cache.MiniMaps.ContainsKey(mapName)) // store in cache
                            Cache.MiniMaps[mapName] = mm;
                        else Cache.MiniMaps.Add(mapName, mm);
                        map.MiniMap = mm;
                    }

                    // convert zoommap to a texture2d
                    using (MemoryStream stream = new MemoryStream())
                    {
                        zoomMap.Save(stream, System.Drawing.Imaging.ImageFormat.Png);
                        stream.Seek(0, SeekOrigin.Begin);

                        Texture2D zm = Texture2D.FromStream(Cache.DefaultState.Display.Device, stream);
                        if (Cache.ZoomMaps.ContainsKey(mapName)) // store in cache
                            Cache.ZoomMaps[mapName] = zm;
                        else Cache.ZoomMaps.Add(mapName, zm);
                        map.ZoomMap = zm;
                    }
                }

                for (int rowIndex = 0; rowIndex < map.Height; rowIndex += rowsPerIteration)
                {
                    using (Graphics g = Graphics.FromImage(fullMap))
                    {
                        for (int y = rowIndex; y < rowIndex + rowsPerIteration; y++)
                            for (int x = 0; x < map.Width; x++)
                            {
                                if (x < map.Width && y < map.Height &&
                                    map[y][x].ObjectSprite > 0 &&
                                    Cache.Tiles.ContainsKey(map[y][x].ObjectSprite) &&
                                    Cache.Tiles[map[y][x].ObjectSprite].MiniMapImage != null &&
                                    Cache.Tiles[map[y][x].ObjectSprite].Frames.Count > map[y][x].ObjectSpriteFrame)
                                {
                                    AnimationFrame frame = Cache.Tiles[map[y][x].ObjectSprite].Frames[map[y][x].ObjectSpriteFrame];
                                    Rectangle frameBox = new Rectangle(frame.X / MapHelper.MiniMapTempScaleFactor, frame.Y / MapHelper.MiniMapTempScaleFactor, frame.Width / MapHelper.MiniMapTempScaleFactor, frame.Height / MapHelper.MiniMapTempScaleFactor);

                                    g.DrawImage(Cache.Tiles[map[y][x].ObjectSprite].MiniMapImage, x * (32 / MapHelper.MiniMapTempScaleFactor), y * (32 / MapHelper.MiniMapTempScaleFactor), frameBox, GraphicsUnit.Pixel);
                                }
                            }

                        g.Flush();
                    }

                    Bitmap miniMap = ResizeImage(fullMap, imageWidth, imageHeight); // minimap is usally always 128x128
                    Bitmap zoomMap = ResizeImage(fullMap, map.Width, map.Height); // zoom map is 1x1 pixel per tile

                    // convert minimap to a texture2d
                    using (MemoryStream stream = new MemoryStream())
                    {
                        miniMap.Save(stream, System.Drawing.Imaging.ImageFormat.Png);
                        stream.Seek(0, SeekOrigin.Begin);

                        Texture2D mm = Texture2D.FromStream(Cache.DefaultState.Display.Device, stream);
                        if (Cache.MiniMaps.ContainsKey(mapName)) // store in cache
                            Cache.MiniMaps[mapName] = mm;
                        else Cache.MiniMaps.Add(mapName, mm);
                        map.MiniMap = mm;
                    }

                    // convert zoommap to a texture2d
                    using (MemoryStream stream = new MemoryStream())
                    {
                        zoomMap.Save(stream, System.Drawing.Imaging.ImageFormat.Png);
                        stream.Seek(0, SeekOrigin.Begin);

                        Texture2D zm = Texture2D.FromStream(Cache.DefaultState.Display.Device, stream);
                        if (Cache.ZoomMaps.ContainsKey(mapName)) // store in cache
                            Cache.ZoomMaps[mapName] = zm;
                        else Cache.ZoomMaps.Add(mapName, zm);
                        map.ZoomMap = zm;
                    }
                }

                map.MiniMapComplete = true;
            }
            catch 
            { 
                map.MiniMapComplete = false;
                map.MiniMap = null;
                map.ZoomMap = null;
                Cache.MiniMaps.Remove(mapName);
                Cache.ZoomMaps.Remove(mapName);
            }
        }

        public static Bitmap ResizeImage(Bitmap sourceBMP, int width, int height)
        {
            Bitmap result = new Bitmap(width, height);
            using (Graphics g = Graphics.FromImage(result))
                g.DrawImage(sourceBMP, 0, 0, width, height);
            return result;
        }
    }
}
