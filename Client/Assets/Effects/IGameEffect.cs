﻿using Microsoft.Xna.Framework.Graphics;
using Helbreath.Common.Assets;

namespace Client.Assets.Effects
{
    public interface IGameEffect
    {
        Location Pivot { get; set; }
        bool Draw(SpriteBatch spriteBatch, GameDisplay display, int x, int y, int pivotX, int pivotY);
        bool DrawLights(SpriteBatch spriteBatch, GameDisplay display, int x, int y, int pivotX, int pivotY);
        void Update();
        bool IsComplete { get; }
    }
}
