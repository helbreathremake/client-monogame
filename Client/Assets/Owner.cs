﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Client.Assets.Effects;
using Client.Assets.State;
using Helbreath.Common;
using Helbreath.Common.Assets;
using Client.Assets.Game;

namespace Client.Assets
{
    public class Owner : IOwner
    {
        private UInt16 id;
        private bool isMoving;
        private int prevX;
        private int prevY;
        private int x;
        private int y;
        private int offsetX = 0;
        private int offsetY = 0; // object is moving, animate between cell. note: humans have 8 walk/run frames, mobs have only 4
        private List<int[]> offsetPoints; // dash/fly effect
        private int attackDestinationX;
        private int attackDestinationY;
        private int type;
        private MotionDirection direction;
        private MotionType motionType;
        private DateTime animationTime;
        private OwnerSide side;
        private OwnerSideStatus sideStatus;
        private int animationFrame;
        private int effectFrame; // internal frame counter for condition sprites
        private int tempFrame; // for dash/fly
        private float fadeValue; // for dead fadeout
        private bool criticalAttack;
        private Dictionary<EquipType, int> equipment;
        private Dictionary<EquipType, int> equipmentColours;
        private Dictionary<EquipType, GameColor> equipmentColorType;
        private List<ChatMessage> damageHistoryStageTwo;
        //private List<ChatMessage> vitalHistory;
        private ChatMessage vitalHistory;
        private ChatMessage damageHistory;
        private ChatMessage spellHistory;
        private ChatMessage chatHistory;

        private Map currentMap;

        private string name;
        private List<NpcPerk> perks;
        private int appearance1;
        private int appearance2;
        private int appearance3;
        private int appearance4;
        private int appearanceColour;
        private int status;
        private bool isCombatMode;
        private bool isDead;
        private bool isCorpseExploited;
        private string guildName;
        private int guildRank;
        private int merchantId;
        private MerchantType merchantType;
        

        public Owner(UInt16 objectId)
        {
            this.id = objectId;
            this.name = string.Empty;
            this.equipment = new Dictionary<EquipType, int>();
            this.equipmentColours = new Dictionary<EquipType, int>();
            this.equipmentColorType = new Dictionary<EquipType, GameColor>();
            this.perks = new List<NpcPerk>();

            fadeValue = 255;

            damageHistoryStageTwo = new List<ChatMessage>();
            //vitalHistory = new List<ChatMessage>();
            vitalHistory = new ChatMessage();
            damageHistory = new ChatMessage();
            spellHistory = new ChatMessage();
            chatHistory = new ChatMessage();
        }

        public void Parse(byte[] data, int countIn, out int count)
        {
            count = countIn;
            type = BitConverter.ToInt16(data, count);
            count += 2;
            direction = (MotionDirection)(int)data[count];
            count++;
            side = (OwnerSide)(int)data[count];
            count++;
            sideStatus = (OwnerSideStatus)(int)data[count];
            count++;

            equipment[EquipType.Head] = BitConverter.ToInt16(data, count);
            count += 2;
            equipmentColours[EquipType.Head] = BitConverter.ToInt32(data, count);
            count += 4;
            equipmentColorType[EquipType.Head] = (GameColor)((int)data[count]);
            count++;

            equipment[EquipType.Body] = BitConverter.ToInt16(data, count);
            count += 2;
            equipmentColours[EquipType.Body] = BitConverter.ToInt32(data, count);
            count += 4;
            equipmentColorType[EquipType.Body] = (GameColor)((int)data[count]);
            count++;

            equipment[EquipType.Arms] = BitConverter.ToInt16(data, count);
            count += 2;
            equipmentColours[EquipType.Arms] = BitConverter.ToInt32(data, count);
            count += 4;
            equipmentColorType[EquipType.Arms] = (GameColor)((int)data[count]);
            count++;

            equipment[EquipType.Legs] = BitConverter.ToInt16(data, count);
            count += 2;
            equipmentColours[EquipType.Legs] = BitConverter.ToInt32(data, count);
            count += 4;
            equipmentColorType[EquipType.Legs] = (GameColor)((int)data[count]);
            count++;

            equipment[EquipType.Back] = BitConverter.ToInt16(data, count);
            count += 2;
            equipmentColours[EquipType.Back] = BitConverter.ToInt32(data, count);
            count += 4;
            equipmentColorType[EquipType.Back] = (GameColor)((int)data[count]);
            count++;

            equipment[EquipType.RightHand] = BitConverter.ToInt16(data, count);
            count += 2;
            equipmentColours[EquipType.RightHand] = BitConverter.ToInt32(data, count);
            count += 4;
            equipmentColorType[EquipType.RightHand] = (GameColor)((int)data[count]);
            count++;

            equipment[EquipType.LeftHand] = BitConverter.ToInt16(data, count);
            count += 2;
            equipmentColours[EquipType.LeftHand] = BitConverter.ToInt32(data, count);
            count += 4;
            equipmentColorType[EquipType.LeftHand] = (GameColor)((int)data[count]);
            count++;

            equipment[EquipType.Feet] = BitConverter.ToInt16(data, count);
            count += 2;
            equipmentColours[EquipType.Feet] = BitConverter.ToInt32(data, count);
            count += 4;
            equipmentColorType[EquipType.Feet] = (GameColor)((int)data[count]);
            count++;

            equipment[EquipType.Utility] = (int)data[count];
            count++;
            isCombatMode = ((int)data[count] == 1);
            count++;
            status = BitConverter.ToInt32(data, count);
            count += 4;

            if (OwnerType == OwnerType.Player)
            {
                name = Encoding.ASCII.GetString(data, count, 10).Trim('\0');
                count += 10;
            }
            else
            {
                Id = BitConverter.ToInt32(data, count);
                count += 4;
                merchantId = (int)data[count];
                count++;
                merchantType = (MerchantType)(int)data[count];
                count++;

                perks.Clear();
                for (int i = 0; i < Globals.MaximumNpcPerks; i++)
                {
                    perks.Add((NpcPerk)data[count]);
                    count++;
                }
            }
        }

        public void ParseMotion(CommandMessageType messageType, byte[] data)
        {
            int x = BitConverter.ToInt16(data, 11);
            int y = BitConverter.ToInt16(data, 13);
            MotionDirection direction = (MotionDirection)(int)data[15];

            switch (messageType)
            {
                case CommandMessageType.ObjectVitalsChanged:
                case CommandMessageType.ObjectNullAction:
                case CommandMessageType.ObjectTakeDamage: break;
                default:
                    this.direction = direction; 
                    break;
            }

            side = (OwnerSide)(int)data[16];
            status = BitConverter.ToInt32(data, 17);
            type = BitConverter.ToInt16(data, 21);
            if (OwnerType == OwnerType.Player)
                name = Encoding.ASCII.GetString(data, 23, 10).Trim('\0');
            else
            {
                Id = BitConverter.ToInt32(data, 23); // was 35

                perks.Clear();
                for (int i = 0; i < Globals.MaximumNpcPerks; i++)
                    perks.Add((NpcPerk)data[27 + i]);
            }

            equipment[EquipType.Head] = BitConverter.ToInt16(data, 33);
            equipmentColours[EquipType.Head] = BitConverter.ToInt32(data, 35);
            equipmentColorType[EquipType.Head] = (GameColor)((int)data[39]);

            equipment[EquipType.Body] = BitConverter.ToInt16(data, 40);
            equipmentColours[EquipType.Body] = BitConverter.ToInt32(data, 42);
            equipmentColorType[EquipType.Body] = (GameColor)((int)data[46]);

            equipment[EquipType.Arms] = BitConverter.ToInt16(data, 47);
            equipmentColours[EquipType.Arms] = BitConverter.ToInt32(data, 49);
            equipmentColorType[EquipType.Arms] = (GameColor)((int)data[53]);

            equipment[EquipType.Legs] = BitConverter.ToInt16(data, 54);
            equipmentColours[EquipType.Legs] = BitConverter.ToInt32(data, 56);
            equipmentColorType[EquipType.Legs] = (GameColor)((int)data[60]);

            equipment[EquipType.Back] = BitConverter.ToInt16(data, 61);
            equipmentColours[EquipType.Back] = BitConverter.ToInt32(data, 53);
            equipmentColorType[EquipType.Back] = (GameColor)((int)data[67]);

            equipment[EquipType.RightHand] = BitConverter.ToInt16(data, 68);
            equipmentColours[EquipType.RightHand] = BitConverter.ToInt32(data, 70);
            equipmentColorType[EquipType.RightHand] = (GameColor)((int)data[74]);

            equipment[EquipType.LeftHand] = BitConverter.ToInt16(data, 75);
            equipmentColours[EquipType.LeftHand] = BitConverter.ToInt32(data, 77);
            equipmentColorType[EquipType.LeftHand] = (GameColor)((int)data[81]);

            equipment[EquipType.Feet] = BitConverter.ToInt16(data, 82);
            equipmentColours[EquipType.Feet] = BitConverter.ToInt32(data, 84);
            equipmentColorType[EquipType.Feet] = (GameColor)((int)data[88]);

            equipment[EquipType.Utility] = (int)data[89]; //+8
            isCombatMode = ((int)data[90] == 1);
            // 83 = special ability
            // 84 = item glow
            // 85 = misc - build points etc
            sideStatus = (OwnerSideStatus)(int)data[94];

            AttackType attackType = AttackType.Normal; DamageType damageType = DamageType.Melee;
            int damage = 0; bool isStun = false; int weaponId = 0; bool corpseExploited = false;
            int destinationX = 0; int destinationY = 0; int spellId = -1; int hitCount = 0;
            int vitalAmount = 0; VitalType vitalType = VitalType.None;
            switch (messageType)
            {
                case CommandMessageType.ObjectTakeDamage:
                case CommandMessageType.ObjectTakeDamageAndFly:
                case CommandMessageType.ObjectDying:
                    damage = BitConverter.ToInt32(data, 95);
                    isStun = ((int)data[99] == 1) ? true : false; // new, we dont stun on every hit now
                    damageType = (DamageType)((int)data[100]);
                    hitCount = (int)data[101];
                    break;
                case CommandMessageType.ObjectVitalsChanged:
                     vitalAmount = BitConverter.ToInt32(data, 95);          
                     vitalType = (VitalType)((int)data[99]); 
                    break;
                case CommandMessageType.ObjectAttack:
                    destinationX = BitConverter.ToInt16(data, 95);
                    destinationY = BitConverter.ToInt16(data, 97);
                    attackType = (AttackType)((int)data[99]);
                    break;
                case CommandMessageType.ObjectDead:
                    corpseExploited = ((int)data[95] == 1 ? true : false);
                    break;
                case CommandMessageType.ObjectMagic:
                    spellId = BitConverter.ToInt16(data, 95);
                    break;
            }

            if (!Cache.OwnerCache.ContainsKey(id))
                Init(x, y); // add new object to map
            else
            {
                switch (messageType) // handle movement and actions
                {
                    case CommandMessageType.ObjectRun: Run(direction); break;
                    case CommandMessageType.ObjectMove: Walk(direction); break;
                    case CommandMessageType.ObjectAttack: Attack(destinationX, destinationY, attackType); break;
                    case CommandMessageType.ObjectTakeDamage: TakeDamage(damage, damageType, hitCount, isStun); break;
                    case CommandMessageType.ObjectTakeDamageAndFly: Fly(damage, hitCount, direction, damageType); break;
                    case CommandMessageType.ObjectVitalsChanged: VitalChanged(vitalAmount, vitalType); break;
                    case CommandMessageType.ObjectStop: Idle(direction); break;
                    case CommandMessageType.ObjectPickUp: PickUp(); break;
                    case CommandMessageType.ObjectDying: Dying(damage, damageType, hitCount); break;
                    case CommandMessageType.ObjectMagic: Cast(spellId); break;
                    case CommandMessageType.ObjectAttackDash: Dash(direction, destinationX, destinationY, weaponId); break;
                    case CommandMessageType.ObjectDead: IsCorpseExploited = corpseExploited; break;
                }

                switch (messageType)
                {
                    case CommandMessageType.ObjectRun:
                    case CommandMessageType.ObjectMove:
                    case CommandMessageType.ObjectTakeDamageAndFly:
                    case CommandMessageType.ObjectAttackDash:
                        SetLocation(Map, x, y);
                        break;
                }
            }

            // remove owner from screen
            if (messageType == CommandMessageType.Reject) 
            {

                FadeOut();
                //if (IsDead) FadeOut(); // fade out if dead
                //else Remove();
            }
        }

        public void Init(int x, int y)
        {
            this.x = x;
            this.y = y;

            if (isDead) { Map[y][x].DeadOwner = this; }
            else Map[y][x].Owner = this;       

            Cache.OwnerCache.Add(this.id, this);
        }

        public void Remove()
        {
            if (Location != null)
            {
                if (Location.DeadOwner == this) Location.DeadOwner = null;
                if (Location.Owner == this) Location.Owner = null;
            }

            Cache.OwnerCache.Remove(this.id);
        }

        public void Cast(int magicId)
        {
            if (isDead) return;

            motionType = MotionType.Magic;
            animationFrame = 0;

            string spellName = (Cache.MagicConfiguration.ContainsKey(magicId)) ? Cache.MagicConfiguration[magicId].Name : "Unknown-Spell";

            ChatMessage message = new ChatMessage(DateTime.Now);
            message.Message = string.Format("{0}!", spellName);
            message.Font = FontType.MagicMedieval18;
            message.Color = (((MainGame)Cache.DefaultState).Player.IsAlly(this)) ? GameColor.Friendly : GameColor.Enemy; //TODO use Cached colors, show green spells for friendlies, red spells for enemies.

            spellHistory = message;

            if (HeroMage || HeroBattleMage)
            {
                Cache.DefaultState.AddEffect(new GameEffect(DrawEffectType.HeroMage, new Location(x - (Cache.MapView.PivotX + Globals.OffScreenCells), y - (Cache.MapView.PivotY + Globals.OffScreenCells)), null));
            }

            if (magicId >= 70)
                Cache.DefaultState.AddEffect(new GameEffect(DrawEffectType.AdvancedMagic, new Location(x - (Cache.MapView.PivotX + Globals.OffScreenCells), y - (Cache.MapView.PivotY + Globals.OffScreenCells)), null));
        }

        public bool IsAlly(IOwner owner)
        {
            if (owner.SideStatus == OwnerSideStatus.Criminal) { return false; }
            else return (side == owner.Side);
        }

        public void Attack(int destinationX, int destinationY, AttackType attackType)
        {
            if (isDead) return;

            criticalAttack = (attackType == AttackType.Critical);
            if (IsCombatMode || OwnerType == OwnerType.Npc)
            {
                this.attackDestinationX = destinationX;
                this.attackDestinationY = destinationY;
                SkillType skillType = (Weapon == null ? SkillType.Hand : Weapon.RelatedSkill);

                if (skillType == SkillType.Archery)
                    BowAttack();
                else
                {
                    motionType = MotionType.Attack;
                    animationFrame = 0;

                    if (Weapon != null && Weapon.DrawEffect != DrawEffectType.None)
                        Cache.DefaultState.AddEffect(new GameEffect(Weapon.DrawEffect, new Location(x - Cache.MapView.PivotX - Globals.OffScreenCells, y - Cache.MapView.PivotY - Globals.OffScreenCells),
                                                                                         new Location(destinationX - Cache.MapView.PivotX - Globals.OffScreenCells, destinationY - Cache.MapView.PivotY - Globals.OffScreenCells)));
                }
            }
            else Bow(direction);

            if (criticalAttack && (HeroWarrior || HeroArcher))
                Cache.DefaultState.AddEffect(new GameEffect(DrawEffectType.HeroWarrior, new Location(x - (Cache.MapView.PivotX + Globals.OffScreenCells), y - (Cache.MapView.PivotY + Globals.OffScreenCells)), null));

            switch (type)
            {
                case 29: AudioHelper.PlaySound("M52"); break;
            }
        }

        public void BowAttack()
        {
            if (isDead) return;

            motionType = MotionType.BowAttack;
            animationFrame = 0;
        }

        public void Bow(MotionDirection direction)
        {
            if (isDead) return;

            this.direction = direction;
            motionType = MotionType.Bow;
            animationFrame = 0;
        }

        public void PickUp()
        {
            if (isDead) return;

            motionType = MotionType.PickUp;
            animationFrame = 0;
        }

        public void Idle(MotionDirection direction)
        {
            if (isDead) return;

            this.direction = direction;
            motionType = MotionType.Idle;
            animationFrame = 0;
        }
        public void Run(MotionDirection direction)
        {
            if (isDead) return;

            this.direction = direction;
            motionType = MotionType.Run;
            animationFrame = 0;
            isMoving = true;
        }

        public void SetLocation(Map map, int x, int y)
        {
            Map prevMap = currentMap;
            prevX = this.x; prevY = this.y;

            //if (currentMap[y][x].IsMoveAllowed)
            //{
                this.x = x;
                this.y = y;
                this.currentMap = map;

                if (isDead)
                {
                    if (prevMap != null) prevMap[prevY][prevX].DeadOwner = null;
                    map[y][x].DeadOwner = this;
                }
                else
                {
                    if (prevMap != null) prevMap[prevY][prevX].Owner = null;
                    map[y][x].Owner = this;
                }
            /*}
            else
            {
                this.x = prevX; this.y = prevY;
            }*/
        }

        public void Walk(MotionDirection direction)
        {
            if (isDead) return;

            if (Cache.DefaultState.Display.DebugMode)
                Debug.WriteLine(string.Format("Object: {0}  moving in direction {1}", ObjectId, direction.ToString()));

            this.direction = direction;
            motionType = MotionType.Move;
            animationFrame = 0;
            isMoving = true;
        }

        public void Dash(MotionDirection direction, int destinationX, int destinationY, int weaponId)
        {
            if (isDead) return;

            prevX = x; prevY = y;

            switch (direction)
            {
                case MotionDirection.North: y--; break;
                case MotionDirection.NorthEast: y--; x++; break;
                case MotionDirection.East: x++; break;
                case MotionDirection.SouthEast: x++; y++; break;
                case MotionDirection.South: y++; break;
                case MotionDirection.SouthWest: y++; x--; break;
                case MotionDirection.West: x--; break;
                case MotionDirection.NorthWest: x--; y--; break;
                default: return;
            }

            this.direction = direction;
            currentMap[prevY][prevX].Owner = null;
            currentMap[y][x].Owner = this;

            motionType = MotionType.Dash;
            animationFrame = tempFrame = 0;
            isMoving = true;

            Item weapon = (Cache.ItemConfiguration.ContainsKey(weaponId) ? Cache.ItemConfiguration[weaponId] : null);

            if (weapon != null && weapon.DrawEffect != DrawEffectType.None)
                Cache.DefaultState.AddEffect(new GameEffect(weapon.DrawEffect, new Location(x - Cache.MapView.PivotX - Globals.OffScreenCells, y - Cache.MapView.PivotY - Globals.OffScreenCells),
                                                                                 new Location(destinationX - Cache.MapView.PivotX - Globals.OffScreenCells, destinationY - Cache.MapView.PivotY - Globals.OffScreenCells)));
        }

        public void VitalChanged(int amount, VitalType vitalType)
        {
            if (amount == 0) return;
            ChatMessage message = new ChatMessage(DateTime.Now);

            if (amount > 0) { message.Message = string.Format("+{0}", amount); }
            else { message.Message = string.Format("{0}", amount); }

            //message.FontType = (amount >= Globals.LargeDamage) ? FontType.DamageLarge : (amount >= Globals.MediumDamage) ? FontType.DamageMedium : FontType.DamageSmall;
            message.Font = FontType.DialogsSmallerSize7;

            switch (vitalType)
            {
                case VitalType.Health: message.Color = GameColor.HP; break;
                case VitalType.Hunger: message.Color = GameColor.Hunger;  break;
                case VitalType.Mana: message.Color = GameColor.Mana; break;
                case VitalType.Stamina: message.Color = GameColor.SP; break;
                default: message.Color = GameColor.None; break;
            }


            //vitalHistory.Add(message);
            vitalHistory = message;
        }

        public void TakeDamage(int damage, DamageType damageType, int hitCount, bool stun = true)
        {
            if (isDead) return;

            if (stun)
            {
                motionType = MotionType.TakeDamage;
                animationFrame = 0;
            }

            ChatMessage message = new ChatMessage(DateTime.Now);
            //if (hitCount > 1) { message.Message = (damage > 0 && damage < Globals.CriticalDamage) ? string.Format("-{0} {1}x", damage, hitCount) : "Critical"; }
            //else { message.Message = (damage > 0 && damage < Globals.CriticalDamage) ? string.Format("-{0}", damage) : "Critical"; }

            message.Message = (damage > 0 && damage < Globals.CriticalDamage) ? string.Format("-{0}", damage) : "Critical";

            message.Font = (damage >= Globals.LargeDamage) ? FontType.DamageLargeSize14Bold : (damage >= Globals.MediumDamage) ? FontType.DamageMediumSize13 : FontType.DamageSmallSize11;

            switch (damageType)
            {
                case DamageType.Environment: message.Color = GameColor.DamageEnvironmental; break;
                case DamageType.Poison: message.Color = GameColor.DamagePoison; break;
                case DamageType.Spirit: message.Color = GameColor.DamageSpirit; break;
                case DamageType.Magic: message.Color = GameColor.DamageMagic; break;
                case DamageType.Melee: message.Color = GameColor.DamageMelee; break; //Yellow
                case DamageType.Ranged: message.Color = GameColor.DamageRanged; break; //Purple
                default: message.Color = GameColor.None; break;
            }

            //if (damageHistory.Type != ChatType.None) { damageHistory.StageTwo(); damageHistoryStageTwo.Add(damageHistory); damageHistory = message; }
            //else { damageHistory = message; }

            damageHistory = message;

            if (type >= 0 && type <= 6)
                Cache.DefaultState.AddEffect(new GameEffect(DrawEffectType.Hit, new Location(x - (Cache.MapView.PivotX + Globals.OffScreenCells), y - (Cache.MapView.PivotY + Globals.OffScreenCells)), null));
        }

        public void Fly(int damage, int hitCount, MotionDirection direction, DamageType damageType)
        {
            if (isDead) return;

            prevX = x; prevY = y;

            switch (direction)
            {
                case MotionDirection.North: y--; break;
                case MotionDirection.NorthEast: y--; x++; break;
                case MotionDirection.East: x++; break;
                case MotionDirection.SouthEast: x++; y++; break;
                case MotionDirection.South: y++; break;
                case MotionDirection.SouthWest: y++; x--; break;
                case MotionDirection.West: x--; break;
                case MotionDirection.NorthWest: x--; y--; break;
                default: return;
            }

            currentMap[prevY][prevX].Owner = null;
            currentMap[y][x].Owner = this;

            this.direction = direction;
            isMoving = true;

            motionType = MotionType.Fly;
            animationFrame = 0;

            if (damage > 0) // dont show fly damage if 0 or lower (some kinesis does 0 damage) //TODO add damage type to dictate color?
            {
                ChatMessage message = new ChatMessage(DateTime.Now);
                //if (hitCount > 1) { message.Message = (damage < Globals.CriticalDamage) ? string.Format("-{0} {1}x", damage, hitCount) : "Critical"; }
                //else { message.Message = (damage < Globals.CriticalDamage) ? string.Format("-{0}", damage) : "Critical"; }

                message.Message = (damage < Globals.CriticalDamage) ? string.Format("-{0}", damage) : "Critical";
                message.Font = (damage >= Globals.LargeDamage) ? FontType.DamageLargeSize14Bold : (damage >= Globals.MediumDamage) ? FontType.DamageMediumSize13 : FontType.DamageSmallSize11;

                switch (damageType)
                {
                    case DamageType.Environment: message.Color = GameColor.DamageEnvironmental; break;
                    case DamageType.Poison: message.Color = GameColor.DamagePoison; break;
                    case DamageType.Spirit: message.Color = GameColor.DamageSpirit; break;
                    case DamageType.Magic: message.Color = GameColor.DamageMagic; break;
                    case DamageType.Melee: message.Color = GameColor.DamageMelee; break; //Yellow
                    case DamageType.Ranged: message.Color = GameColor.DamageRanged; break; //Purple
                    default: message.Color = GameColor.Ancient; break;
                }

                //if (damageHistory.Type != ChatType.None) { damageHistory.StageTwo(); damageHistoryStageTwo.Add(damageHistory); damageHistory = message; }
                //else { damageHistory = message; }

                damageHistory = message;
            }
        }

        public void Dying(int damage, DamageType damageType, int hitCount)
        {
            if (isDead) return;

            motionType = MotionType.Die;
            animationFrame = 0;

            if (currentMap[y][x].Owner == this)
                currentMap[y][x].Owner = null;
            currentMap[y][x].DeadOwner = this;
            isDead = true;

            // last hit
            ChatMessage message = new ChatMessage(DateTime.Now);
            //if (hitCount > 1) { message.Message = (damage > 0 && damage < Globals.CriticalDamage) ? string.Format("-{0}! {1}x!", damage, hitCount) : "Critical!"; }
            //else { message.Message = (damage > 0 && damage < Globals.CriticalDamage) ? string.Format("-{0}!", damage) : "Critical!"; }
            message.Message = (damage > 0 && damage < Globals.CriticalDamage) ? string.Format("-{0}!", damage) : "Critical!";

            message.Font = (damage >= Globals.LargeDamage) ? FontType.DamageLargeSize14Bold : (damage >= Globals.MediumDamage) ? FontType.DamageMediumSize13 : FontType.DamageSmallSize11;

            switch (damageType)
            {
                case DamageType.Environment: message.Color = GameColor.DamageEnvironmental; break;
                case DamageType.Poison: message.Color = GameColor.DamagePoison; break;
                case DamageType.Spirit: message.Color = GameColor.DamageSpirit; break;
                case DamageType.Magic: message.Color = GameColor.DamageMagic; break;
                case DamageType.Melee: message.Color = GameColor.DamageMelee; break; //Yellow
                case DamageType.Ranged: message.Color = GameColor.DamageRanged; break; //Purple
                default: message.Color = GameColor.None; break;
            }

            //if (damageHistory.Type != ChatType.None) { damageHistory.StageTwo(); damageHistoryStageTwo.Add(damageHistory); damageHistory = message; }
            //else { damageHistory = message; }

            damageHistory = message;
        }

        public void FadeOut()
        {
            motionType = MotionType.DeadFadeOut;
            //animationFrame = 0;
        }

        public bool Draw(SpriteBatch spriteBatch, GameDisplay display, int x, int y, int playerOffsetX, int playerOffsetY)
        {
            if (direction == MotionDirection.None) return false;

            bool selected = false;
            bool showAura = (!isDead);
            
            bool sameSide = false;
            Player player = ((State.MainGame)Cache.DefaultState).Player;
            if (player.Side == Side) sameSide = true;

            float fade = fadeValue / 255.0f;
            if (!isDead && IsInvisible && !sameSide) return false;
            if (!isDead && IsInvisible && sameSide) fade = 0.5f;
            Color colour = Cache.Colors[GameColor.Normal];
            if (isDead && isCorpseExploited) colour = Cache.Colors[GameColor.Exploited];
            else
            {
                // TODO - fix for new colour set up
                /*if (Type >= 10 && Cache.NpcConfiguration[Id].SpriteColour > 0)
                    colour = SpriteHelper.GetItemColour(ItemEffectType.Attack, Cache.NpcConfiguration[Id].SpriteColour) * fade;
                else colour = Color.White * fade;*/
            }

            bool drawWeapons = (!isDead && motionType != MotionType.PickUp && motionType != MotionType.Bow && motionType != MotionType.Magic);

            if (Type <= 6) //Players
            {
                GameColor teamColor = GameColor.None; //No color
                if (Cache.GameSettings.TeamColorOn)
                {
                    //if (Cache.GameSettings.PartyColorOn) 
                    if (Cache.GameSettings.GuildColorOn && guildName == player.GuildName) { teamColor = GameColor.Guild; }
                    else if (Cache.GameSettings.TownColorOn)
                    {
                        switch (side)
                        {
                            case OwnerSide.Aresden: teamColor = GameColor.Aresden; break;
                            case OwnerSide.Elvine: teamColor = GameColor.Elvine; break;
                        }
                    }
                }


                //DrawItemPopup Shadow
                if (!isDead && Cache.GameSettings.DetailMode != GraphicsDetail.Low)SpriteHelper.DrawBodyShadow(spriteBatch, Type, Animation, AnimationFrame,
                            x, y, playerOffsetX, playerOffsetY, offsetX, offsetY, Cache.Colors[GameColor.Shadow], fade, out selected);

                if (showAura)
                {
                    // Defence shield
                    if ((status & 0x02000000) != 0)
                        SpriteHelper.DrawEffect(spriteBatch, 80, effectFrame % 17, x + 75, y + 107, offsetX + playerOffsetX, offsetY + playerOffsetY, Color.White * 0.5f);

                    // Protection From Magic
                    if ((status & 0x04000000) != 0)
                        SpriteHelper.DrawEffect(spriteBatch, 79, effectFrame % 15, x + 101, y + 135, offsetX + playerOffsetX, offsetY + playerOffsetY, Color.White * 0.7f);

                    // Protection From Arrow
                    if ((status & 0x08000000) != 0)
                        SpriteHelper.DrawEffect(spriteBatch, 72, effectFrame % 30, x, y + 35, offsetX + playerOffsetX, offsetY + playerOffsetY, Color.White * 0.7f);

                    // Illusion movement
                    if ((status & 0x00200000) != 0)
                        SpriteHelper.DrawEffect(spriteBatch, 151, effectFrame % 24, x + 90, y + 90, offsetX + playerOffsetX, offsetY + playerOffsetY, Color.White * 0.7f);

                    // HP Slate
                    if ((status & 0x00400000) != 0)
                        SpriteHelper.DrawEffect(spriteBatch, 149, effectFrame % 15, x + 90, y + 120, offsetX + playerOffsetX, offsetY + playerOffsetY, Color.White * 0.7f);

                    // Mana Slate
                    if ((status & 0x00800000) != 0)
                        SpriteHelper.DrawEffect(spriteBatch, 150, effectFrame % 15, x + 1, y + 26, offsetX + playerOffsetX, offsetY + playerOffsetY, Color.White * 0.7f);

                    // XP Slate
                    if ((status & 0x00010000) != 0)
                        SpriteHelper.DrawEffect(spriteBatch, 148, effectFrame % 23, x, y + 32, offsetX + playerOffsetX, offsetY + playerOffsetY, Color.White * 0.7f);

                    // Hero Flag (Heldenian)
                    if ((status & 0x00020000) != 0)
                        SpriteHelper.DrawEffect(spriteBatch, 87, effectFrame % 29, x + 53, y + 54, offsetX + playerOffsetX, offsetY + playerOffsetY, Color.White * 0.7f);

                    if (IsAbilityActivated)
                        switch ((appearance4 & 0x00F0) >> 4)
                        {
                            case 1: SpriteHelper.DrawEffect(spriteBatch, 26, effectFrame % 14, x, y, offsetX + playerOffsetX, offsetY + playerOffsetY, Color.White * 0.7f); break;
                            case 2: SpriteHelper.DrawEffect(spriteBatch, 27, effectFrame % 12, x, y, offsetX + playerOffsetX, offsetY + playerOffsetY, Color.White * 0.7f); break;
                        }
                }

                SpriteHelper.DrawBody(spriteBatch, Type, Animation, AnimationFrame,
                                        x, y, playerOffsetX, playerOffsetY, offsetX, offsetY, colour, fade, out selected);

                switch (Type)
                {
                    case 1:
                    case 2:
                    case 3: // men
                        {
                            int undiesIndex = (int)SpriteId.MaleUndies + ((Appearance1 & 0x000F) * 15) + AnimationMotionOffset;
                            int undiesFrame = (((int)Direction - 1) * AnimationFrameCount) + AnimationFrame;
                            SpriteHelper.DrawEquipment(spriteBatch, undiesIndex, undiesFrame, x, y, playerOffsetX, playerOffsetY, offsetX, offsetY, colour, fade);

                            /* DRAW CAPE BEHIND BODY */
                            if (!AnimationDrawCapeFront)
                            {
                                if (Cache.GameSettings.TeamColorOn && teamColor != GameColor.None)
                                {
                                    int capeIndex = (int)SpriteId.MaleCape + 45 + AnimationMotionOffset;
                                    int capeFrame = (((int)Direction - 1) * AnimationFrameCount) + AnimationFrame;
                                    SpriteHelper.DrawEquipment(spriteBatch, capeIndex, capeFrame, x, y, playerOffsetX, playerOffsetY, offsetX, offsetY, Cache.Colors[teamColor], fade);
                                }
                                else if (Cape != null)
                                {
                                    int capeIndex = ((int)SpriteId.MaleCape + (Cape.Appearance * 15)) + AnimationMotionOffset;
                                    int capeFrame = (((int)Direction - 1) * AnimationFrameCount) + AnimationFrame;
                                    SpriteHelper.DrawEquipment(spriteBatch, capeIndex, capeFrame, x, y, playerOffsetX, playerOffsetY, offsetX, offsetY, (equipmentColorType[EquipType.Back] == GameColor.Custom) ?  SpriteHelper.ColourFromArgb(equipmentColours[EquipType.Back]) : Cache.Colors[equipmentColorType[EquipType.Back]], fade, Cape.SpriteFileMale);
                                }
                            }

                            /* DRAW WEAPONS AND SHIELDS BEHIND BODY */
                            if (drawWeapons)
                            {
                                /* HARD CODED EXCEPTION FOR NORTH EAST */
                                if (direction == MotionDirection.NorthEast)
                                {
                                    if (!AnimationDrawShieldFront && Shield != null) // shield
                                    {
                                        int shieldIndex = (int)SpriteId.MaleShield + Shield.Appearance * 8 + AnimationMotionOffsetWeapon;
                                        int shieldFrame = (((int)Direction - 1) * AnimationFrameCount) + AnimationFrame;
                                        SpriteHelper.DrawEquipment(spriteBatch, shieldIndex, shieldFrame, x, y, playerOffsetX, playerOffsetY, offsetX, offsetY, (equipmentColorType[EquipType.LeftHand] == GameColor.Custom) ? SpriteHelper.ColourFromArgb(equipmentColours[EquipType.LeftHand]) : Cache.Colors[equipmentColorType[EquipType.LeftHand]], fade, Shield.SpriteFileMale);
                                    }
                                    if (!AnimationDrawWeaponFront && Weapon != null) // weapon
                                    {
                                        int weaponIndex = ((int)SpriteId.MaleWeapon + Weapon.Appearance * 64 + 8 * AnimationMotionOffsetWeapon) + ((int)Direction - 1);
                                        int weaponFrame = AnimationFrame;
                                        SpriteHelper.DrawEquipment(spriteBatch, weaponIndex, weaponFrame, x, y, playerOffsetX, playerOffsetY, offsetX, offsetY, (equipmentColorType[EquipType.RightHand] == GameColor.Custom) ? SpriteHelper.ColourFromArgb(equipmentColours[EquipType.RightHand]) : Cache.Colors[equipmentColorType[EquipType.RightHand]], fade, Weapon.SpriteFileMale);
                                    }
                                }
                                else
                                {
                                    if (!AnimationDrawWeaponFront && Weapon != null) // weapon
                                    {
                                        int weaponIndex = ((int)SpriteId.MaleWeapon + Weapon.Appearance * 64 + 8 * AnimationMotionOffsetWeapon) + ((int)Direction - 1);
                                        int weaponFrame = AnimationFrame;
                                        SpriteHelper.DrawEquipment(spriteBatch, weaponIndex, weaponFrame, x, y, playerOffsetX, playerOffsetY, offsetX, offsetY, (equipmentColorType[EquipType.RightHand] == GameColor.Custom) ? SpriteHelper.ColourFromArgb(equipmentColours[EquipType.RightHand]) : Cache.Colors[equipmentColorType[EquipType.RightHand]], fade, Weapon.SpriteFileMale);
                                    }
                                    if (!AnimationDrawShieldFront && Shield != null) // shield
                                    {
                                        int shieldIndex = (int)SpriteId.MaleShield + Shield.Appearance * 8 + AnimationMotionOffsetWeapon;
                                        int shieldFrame = (((int)Direction - 1) * AnimationFrameCount) + AnimationFrame;
                                        SpriteHelper.DrawEquipment(spriteBatch, shieldIndex, shieldFrame, x, y, playerOffsetX, playerOffsetY, offsetX, offsetY, (equipmentColorType[EquipType.LeftHand] == GameColor.Custom) ? SpriteHelper.ColourFromArgb(equipmentColours[EquipType.LeftHand]) : Cache.Colors[equipmentColorType[EquipType.LeftHand]], fade, Shield.SpriteFileMale);
                                    }
                                }
                            }

                            /* DRAW LEGS */
                            if (Leggings != null) // legs
                            {
                                int legsIndex = ((int)SpriteId.MaleLegs + Leggings.Appearance * 15) + AnimationMotionOffset;
                                int legsFrame = (((int)Direction - 1) * AnimationFrameCount) + AnimationFrame;
                                SpriteHelper.DrawEquipment(spriteBatch, legsIndex, legsFrame, x, y, playerOffsetX, playerOffsetY, offsetX, offsetY, (equipmentColorType[EquipType.Legs] == GameColor.Custom) ? SpriteHelper.ColourFromArgb(equipmentColours[EquipType.Legs]) : Cache.Colors[equipmentColorType[EquipType.Legs]], fade, Leggings.SpriteFileMale);
                            }

                            /* DRAW BOOTS */
                            if (Cache.GameSettings.TeamColorOn && teamColor != GameColor.None)
                            {
                                int bootsIndex = (int)SpriteId.MaleBoots + 30 + AnimationMotionOffset;
                                int bootsFrame = (((int)Direction - 1) * AnimationFrameCount) + AnimationFrame;
                                SpriteHelper.DrawEquipment(spriteBatch, bootsIndex, bootsFrame, x, y, playerOffsetX, playerOffsetY, offsetX, offsetY, Cache.Colors[teamColor], fade);
                            }
                            else if (Boots != null) // boots
                            {
                                int bootsIndex = ((int)SpriteId.MaleBoots + Boots.Appearance * 15) + AnimationMotionOffset;
                                int bootsFrame = (((int)Direction - 1) * AnimationFrameCount) + AnimationFrame;
                                SpriteHelper.DrawEquipment(spriteBatch, bootsIndex, bootsFrame, x, y, playerOffsetX, playerOffsetY, offsetX, offsetY, (equipmentColorType[EquipType.Feet] == GameColor.Custom) ? SpriteHelper.ColourFromArgb(equipmentColours[EquipType.Feet]) : Cache.Colors[equipmentColorType[EquipType.Feet]], fade, Boots.SpriteFileMale);
                            }

                            /* DRAW ARMS */
                            if (Hauberk != null) // arms
                            {
                                int armsIndex = ((int)SpriteId.MaleArms + Hauberk.Appearance * 15) + AnimationMotionOffset;
                                int armsFrame = (((int)Direction - 1) * AnimationFrameCount) + AnimationFrame;
                                SpriteHelper.DrawEquipment(spriteBatch, armsIndex, armsFrame, x, y, playerOffsetX, playerOffsetY, offsetX, offsetY, (equipmentColorType[EquipType.Arms] == GameColor.Custom) ? SpriteHelper.ColourFromArgb(equipmentColours[EquipType.Arms]) : Cache.Colors[equipmentColorType[EquipType.Arms]], fade, Hauberk.SpriteFileMale);
                            }

                            /* DRAW BODY */
                            if (BodyArmour != null) // body
                            {
                                int bodyIndex = ((int)SpriteId.MaleBody + BodyArmour.Appearance * 15) + AnimationMotionOffset;
                                int bodyFrame = (((int)Direction - 1) * AnimationFrameCount) + AnimationFrame;
                                SpriteHelper.DrawEquipment(spriteBatch, bodyIndex, bodyFrame, x, y, playerOffsetX, playerOffsetY, offsetX, offsetY, (equipmentColorType[EquipType.Body] == GameColor.Custom) ? SpriteHelper.ColourFromArgb(equipmentColours[EquipType.Body]) : Cache.Colors[equipmentColorType[EquipType.Body]], fade, BodyArmour.SpriteFileMale);
                            }

                            /* DRAW HELM OR HAIR */
                            if (Helmet != null) // helm
                            {
                                int headIndex = ((int)SpriteId.MaleHead + Helmet.Appearance * 15) + AnimationMotionOffset;
                                int headFrame = (((int)Direction - 1) * AnimationFrameCount) + AnimationFrame;
                                SpriteHelper.DrawEquipment(spriteBatch, headIndex, headFrame, x, y, playerOffsetX, playerOffsetY, offsetX, offsetY, (equipmentColorType[EquipType.Head] == GameColor.Custom) ? SpriteHelper.ColourFromArgb(equipmentColours[EquipType.Head]) : Cache.Colors[equipmentColorType[EquipType.Head]], fade, Helmet.SpriteFileMale);
                            }
                            else
                            {
                                int hairIndex = ((int)SpriteId.MaleHair + ((Appearance1 & 0x0F00) >> 8) * 15) + AnimationMotionOffset;
                                int hairFrame = (((int)Direction - 1) * AnimationFrameCount) + AnimationFrame;
                                SpriteHelper.DrawEquipment(spriteBatch, hairIndex, hairFrame, x, y, playerOffsetX, playerOffsetY, offsetX, offsetY, colour, fade);
                            }

                            /* DRAW WEAPONS AND SHIELDS IN FRONT OF BODY */
                            if (drawWeapons)
                            {
                                /* HARD CODED EXCEPTION FOR NORTH EAST */
                                if (direction == MotionDirection.NorthEast)
                                {
                                    if (AnimationDrawShieldFront && Shield != null) // shield
                                    {
                                        int shieldIndex = (int)SpriteId.MaleShield + Shield.Appearance * 8 + AnimationMotionOffsetWeapon;
                                        int shieldFrame = (((int)Direction - 1) * AnimationFrameCount) + AnimationFrame;
                                        SpriteHelper.DrawEquipment(spriteBatch, shieldIndex, shieldFrame, x, y, playerOffsetX, playerOffsetY, offsetX, offsetY, (equipmentColorType[EquipType.LeftHand] == GameColor.Custom) ? SpriteHelper.ColourFromArgb(equipmentColours[EquipType.LeftHand]) : Cache.Colors[equipmentColorType[EquipType.LeftHand]], fade, Shield.SpriteFileMale);
                                    }
                                    if (AnimationDrawWeaponFront && Weapon != null) // weapon
                                    {
                                        int weaponIndex = ((int)SpriteId.MaleWeapon + Weapon.Appearance * 64 + 8 * AnimationMotionOffsetWeapon) + ((int)Direction - 1);
                                        int weaponFrame = AnimationFrame;
                                        SpriteHelper.DrawEquipment(spriteBatch, weaponIndex, weaponFrame, x, y, playerOffsetX, playerOffsetY, offsetX, offsetY, (equipmentColorType[EquipType.RightHand] == GameColor.Custom) ? SpriteHelper.ColourFromArgb(equipmentColours[EquipType.RightHand]) : Cache.Colors[equipmentColorType[EquipType.RightHand]], fade, Weapon.SpriteFileMale);
                                    }
                                }
                                else
                                {
                                    if (AnimationDrawWeaponFront && Weapon != null) // weapon
                                    {
                                        int weaponIndex = ((int)SpriteId.MaleWeapon + Weapon.Appearance * 64 + 8 * AnimationMotionOffsetWeapon) + ((int)Direction - 1);
                                        int weaponFrame = AnimationFrame;
                                        SpriteHelper.DrawEquipment(spriteBatch, weaponIndex, weaponFrame, x, y, playerOffsetX, playerOffsetY, offsetX, offsetY, (equipmentColorType[EquipType.RightHand] == GameColor.Custom) ? SpriteHelper.ColourFromArgb(equipmentColours[EquipType.RightHand]) : Cache.Colors[equipmentColorType[EquipType.RightHand]], fade, Weapon.SpriteFileMale);
                                    }
                                    if (AnimationDrawShieldFront && Shield != null) // shield
                                    {
                                        int shieldIndex = (int)SpriteId.MaleShield + Shield.Appearance * 8 + AnimationMotionOffsetWeapon;
                                        int shieldFrame = (((int)Direction - 1) * AnimationFrameCount) + AnimationFrame;
                                        SpriteHelper.DrawEquipment(spriteBatch, shieldIndex, shieldFrame, x, y, playerOffsetX, playerOffsetY, offsetX, offsetY, (equipmentColorType[EquipType.LeftHand] == GameColor.Custom) ? SpriteHelper.ColourFromArgb(equipmentColours[EquipType.LeftHand]) : Cache.Colors[equipmentColorType[EquipType.LeftHand]], fade, Shield.SpriteFileMale);
                                    }
                                }
                            }

                            /* DRAW CAPE IN FRONT OF BODY */
                            if (AnimationDrawCapeFront)
                            {
                                if (Cache.GameSettings.TeamColorOn && teamColor != GameColor.None)
                                {
                                    int capeIndex = (int)SpriteId.MaleCape + 45 + AnimationMotionOffset;
                                    int capeFrame = (((int)Direction - 1) * AnimationFrameCount) + AnimationFrame;
                                    SpriteHelper.DrawEquipment(spriteBatch, capeIndex, capeFrame, x, y, playerOffsetX, playerOffsetY, offsetX, offsetY, Cache.Colors[teamColor], fade);

                                }
                                else if (Cape != null) // cape
                                {
                                    int capeIndex = ((int)SpriteId.MaleCape + Cape.Appearance * 15) + AnimationMotionOffset;
                                    int capeFrame = (((int)Direction - 1) * AnimationFrameCount) + AnimationFrame;
                                    SpriteHelper.DrawEquipment(spriteBatch, capeIndex, capeFrame, x, y, playerOffsetX, playerOffsetY, offsetX, offsetY, (equipmentColorType[EquipType.Back] == GameColor.Custom) ? SpriteHelper.ColourFromArgb(equipmentColours[EquipType.Back]) : Cache.Colors[equipmentColorType[EquipType.Back]], fade, Cape.SpriteFileMale);
                                }
                            }
                        }
                        break;
                    case 4:
                    case 5:
                    case 6: // woman
                        {
                            int undiesIndex = (int)SpriteId.FemaleUndies + ((Appearance1 & 0x000F) * 15) + AnimationMotionOffset;
                            int undiesFrame = (((int)Direction - 1) * AnimationFrameCount) + AnimationFrame;
                            SpriteHelper.DrawEquipment(spriteBatch, undiesIndex, undiesFrame, x, y, playerOffsetX, playerOffsetY, offsetX, offsetY, Cache.Colors[GameColor.Normal], fade);

                            /* DRAW CAPE BEHIND BODY */
                            if (!AnimationDrawCapeFront)
                            {
                                if (Cache.GameSettings.TeamColorOn && teamColor != GameColor.None)
                                {
                                    int capeIndex = (int)SpriteId.FemaleCape + 45 + AnimationMotionOffset;
                                    int capeFrame = (((int)Direction - 1) * AnimationFrameCount) + AnimationFrame;
                                    SpriteHelper.DrawEquipment(spriteBatch, capeIndex, capeFrame, x, y, playerOffsetX, playerOffsetY, offsetX, offsetY, Cache.Colors[teamColor], fade);
                                }
                                else if (Cape != null) // cape
                                {
                                    int capeIndex = ((int)SpriteId.FemaleCape + Cape.Appearance * 15) + AnimationMotionOffset;
                                    int capeFrame = (((int)Direction - 1) * AnimationFrameCount) + AnimationFrame;
                                    SpriteHelper.DrawEquipment(spriteBatch, capeIndex, capeFrame, x, y, playerOffsetX, playerOffsetY, offsetX, offsetY, (equipmentColorType[EquipType.Back] == GameColor.Custom) ? SpriteHelper.ColourFromArgb(equipmentColours[EquipType.Back]) : Cache.Colors[equipmentColorType[EquipType.Back]], fade, Cape.SpriteFileFemale);
                                }
                            }

                            /* DRAW WEAPONS AND SHIELDS BEHIND BODY */
                            if (drawWeapons)
                            {
                                /* HARD CODED EXCEPTION FOR NORTH EAST */
                                if (direction == MotionDirection.NorthEast)
                                {
                                    if (!AnimationDrawShieldFront && Shield != null) // shield
                                    {
                                        int shieldIndex = (int)SpriteId.FemaleShield + Shield.Appearance * 8 + AnimationMotionOffsetWeapon;
                                        int shieldFrame = (((int)Direction - 1) * AnimationFrameCount) + AnimationFrame;
                                        SpriteHelper.DrawEquipment(spriteBatch, shieldIndex, shieldFrame, x, y, playerOffsetX, playerOffsetY, offsetX, offsetY, (equipmentColorType[EquipType.LeftHand] == GameColor.Custom) ? SpriteHelper.ColourFromArgb(equipmentColours[EquipType.LeftHand]) : Cache.Colors[equipmentColorType[EquipType.LeftHand]], fade, Shield.SpriteFileFemale);
                                    }
                                    if (!AnimationDrawWeaponFront && Weapon != null) // weapon
                                    {
                                        int weaponIndex = ((int)SpriteId.FemaleWeapon + Weapon.Appearance * 64 + 8 * AnimationMotionOffsetWeapon) + ((int)Direction - 1);
                                        int weaponFrame = AnimationFrame;
                                        SpriteHelper.DrawEquipment(spriteBatch, weaponIndex, weaponFrame, x, y, playerOffsetX, playerOffsetY, offsetX, offsetY, (equipmentColorType[EquipType.RightHand] == GameColor.Custom) ? SpriteHelper.ColourFromArgb(equipmentColours[EquipType.RightHand]) : Cache.Colors[equipmentColorType[EquipType.RightHand]], fade, Weapon.SpriteFileFemale);
                                    }
                                }
                                else
                                {
                                    if (!AnimationDrawWeaponFront && Weapon != null) // weapon
                                    {
                                        int weaponIndex = ((int)SpriteId.FemaleWeapon + Weapon.Appearance * 64 + 8 * AnimationMotionOffsetWeapon) + ((int)Direction - 1);
                                        int weaponFrame = AnimationFrame;
                                        SpriteHelper.DrawEquipment(spriteBatch, weaponIndex, weaponFrame, x, y, playerOffsetX, playerOffsetY, offsetX, offsetY, (equipmentColorType[EquipType.RightHand] == GameColor.Custom) ? SpriteHelper.ColourFromArgb(equipmentColours[EquipType.RightHand]) : Cache.Colors[equipmentColorType[EquipType.RightHand]], fade, Weapon.SpriteFileFemale);
                                    }
                                    if (!AnimationDrawShieldFront && Shield != null) // shield
                                    {
                                        int shieldIndex = (int)SpriteId.FemaleShield + Shield.Appearance * 8 + AnimationMotionOffsetWeapon;
                                        int shieldFrame = (((int)Direction - 1) * AnimationFrameCount) + AnimationFrame;
                                        SpriteHelper.DrawEquipment(spriteBatch, shieldIndex, shieldFrame, x, y, playerOffsetX, playerOffsetY, offsetX, offsetY, (equipmentColorType[EquipType.LeftHand] == GameColor.Custom) ? SpriteHelper.ColourFromArgb(equipmentColours[EquipType.LeftHand]) : Cache.Colors[equipmentColorType[EquipType.LeftHand]], fade, Shield.SpriteFileFemale);
                                    }
                                }
                            }

                            /* DRAW LEGS */
                            if (Leggings != null) // legs
                            {
                                int legsIndex = ((int)SpriteId.FemaleLegs + Leggings.Appearance * 15) + AnimationMotionOffset;
                                int legsFrame = (((int)Direction - 1) * AnimationFrameCount) + AnimationFrame;
                                SpriteHelper.DrawEquipment(spriteBatch, legsIndex, legsFrame, x, y, playerOffsetX, playerOffsetY, offsetX, offsetY, (equipmentColorType[EquipType.Legs] == GameColor.Custom) ? SpriteHelper.ColourFromArgb(equipmentColours[EquipType.Legs]) : Cache.Colors[equipmentColorType[EquipType.Legs]], fade, Leggings.SpriteFileFemale);
                            }

                            /* DRAW BOOTS */
                            if (Cache.GameSettings.TeamColorOn && teamColor != GameColor.None)
                            {
                                int bootsIndex = (int)SpriteId.FemaleBoots + 30 + AnimationMotionOffset;
                                int bootsFrame = (((int)Direction - 1) * AnimationFrameCount) + AnimationFrame;
                                SpriteHelper.DrawEquipment(spriteBatch, bootsIndex, bootsFrame, x, y, playerOffsetX, playerOffsetY, offsetX, offsetY, Cache.Colors[teamColor], fade);
                            }
                            else if (Boots != null) // boots
                            {
                                int bootsIndex = ((int)SpriteId.FemaleBoots + Boots.Appearance * 15) + AnimationMotionOffset;
                                int bootsFrame = (((int)Direction - 1) * AnimationFrameCount) + AnimationFrame;
                                SpriteHelper.DrawEquipment(spriteBatch, bootsIndex, bootsFrame, x, y, playerOffsetX, playerOffsetY, offsetX, offsetY, (equipmentColorType[EquipType.Feet] == GameColor.Custom) ? SpriteHelper.ColourFromArgb(equipmentColours[EquipType.Feet]) : Cache.Colors[equipmentColorType[EquipType.Feet]], fade, Boots.SpriteFileFemale);
                            }

                            /* DRAW ARMS */
                            if (Hauberk != null) // arms
                            {
                                int armsIndex = ((int)SpriteId.FemaleArms + Hauberk.Appearance * 15) + AnimationMotionOffset;
                                int armsFrame = (((int)Direction - 1) * AnimationFrameCount) + AnimationFrame;
                                SpriteHelper.DrawEquipment(spriteBatch, armsIndex, armsFrame, x, y, playerOffsetX, playerOffsetY, offsetX, offsetY, (equipmentColorType[EquipType.Arms] == GameColor.Custom) ? SpriteHelper.ColourFromArgb(equipmentColours[EquipType.Arms]) : Cache.Colors[equipmentColorType[EquipType.Arms]], fade, Hauberk.SpriteFileFemale);
                            }

                            /* DRAW BODY */
                            if (BodyArmour != null) // body
                            {
                                int bodyIndex = ((int)SpriteId.FemaleBody + BodyArmour.Appearance * 15) + AnimationMotionOffset;
                                int bodyFrame = (((int)Direction - 1) * AnimationFrameCount) + AnimationFrame;
                                SpriteHelper.DrawEquipment(spriteBatch, bodyIndex, bodyFrame, x, y, playerOffsetX, playerOffsetY, offsetX, offsetY, (equipmentColorType[EquipType.Body] == GameColor.Custom) ? SpriteHelper.ColourFromArgb(equipmentColours[EquipType.Body]) : Cache.Colors[equipmentColorType[EquipType.Body]], fade, BodyArmour.SpriteFileFemale);
                            }

                            /* DRAW HELM OR HAIR */
                            if (Helmet != null) // helm
                            {
                                int headIndex = ((int)SpriteId.FemaleHead + Helmet.Appearance * 15) + AnimationMotionOffset;
                                int headFrame = (((int)Direction - 1) * AnimationFrameCount) + AnimationFrame;
                                SpriteHelper.DrawEquipment(spriteBatch, headIndex, headFrame, x, y, playerOffsetX, playerOffsetY, offsetX, offsetY, (equipmentColorType[EquipType.Head] == GameColor.Custom) ? SpriteHelper.ColourFromArgb(equipmentColours[EquipType.Head]) : Cache.Colors[equipmentColorType[EquipType.Head]], fade, Helmet.SpriteFileFemale);
                            }
                            else
                            {
                                int hairIndex = ((int)SpriteId.FemaleHair + ((Appearance1 & 0x0F00) >> 8) * 15) + AnimationMotionOffset;
                                int hairFrame = (((int)Direction - 1) * AnimationFrameCount) + AnimationFrame;
                                SpriteHelper.DrawEquipment(spriteBatch, hairIndex, hairFrame, x, y, playerOffsetX, playerOffsetY, offsetX, offsetY, colour, fade);
                            }

                            /* DRAW WEAPONS AND SHIELDS IN FRONT OF BODY */
                            if (drawWeapons)
                            {
                                /* HARD CODED EXCEPTION FOR NORTH EAST */
                                if (direction == MotionDirection.NorthEast)
                                {
                                    if (AnimationDrawShieldFront && Shield != null) // shield
                                    {
                                        int shieldIndex = (int)SpriteId.FemaleShield + Shield.Appearance * 8 + AnimationMotionOffsetWeapon;
                                        int shieldFrame = (((int)Direction - 1) * AnimationFrameCount) + AnimationFrame;
                                        SpriteHelper.DrawEquipment(spriteBatch, shieldIndex, shieldFrame, x, y, playerOffsetX, playerOffsetY, offsetX, offsetY, (equipmentColorType[EquipType.LeftHand] == GameColor.Custom) ? SpriteHelper.ColourFromArgb(equipmentColours[EquipType.LeftHand]) : Cache.Colors[equipmentColorType[EquipType.LeftHand]], fade, Shield.SpriteFileFemale);
                                    }
                                    if (AnimationDrawWeaponFront && Weapon != null) // weapon
                                    {
                                        int weaponIndex = ((int)SpriteId.FemaleWeapon + Weapon.Appearance * 64 + 8 * AnimationMotionOffsetWeapon) + ((int)Direction - 1);
                                        int weaponFrame = AnimationFrame;
                                        SpriteHelper.DrawEquipment(spriteBatch, weaponIndex, weaponFrame, x, y, playerOffsetX, playerOffsetY, offsetX, offsetY, (equipmentColorType[EquipType.RightHand] == GameColor.Custom) ? SpriteHelper.ColourFromArgb(equipmentColours[EquipType.RightHand]) : Cache.Colors[equipmentColorType[EquipType.RightHand]], fade, Weapon.SpriteFileFemale);
                                    }
                                }
                                else
                                {
                                    if (AnimationDrawWeaponFront && Weapon != null) // weapon
                                    {
                                        int weaponIndex = ((int)SpriteId.FemaleWeapon + Weapon.Appearance * 64 + 8 * AnimationMotionOffsetWeapon) + ((int)Direction - 1);
                                        int weaponFrame = AnimationFrame;
                                        SpriteHelper.DrawEquipment(spriteBatch, weaponIndex, weaponFrame, x, y, playerOffsetX, playerOffsetY, offsetX, offsetY, (equipmentColorType[EquipType.RightHand] == GameColor.Custom) ? SpriteHelper.ColourFromArgb(equipmentColours[EquipType.RightHand]) : Cache.Colors[equipmentColorType[EquipType.RightHand]], fade, Weapon.SpriteFileFemale);
                                    }
                                    if (AnimationDrawShieldFront && Shield != null) // shield
                                    {
                                        int shieldIndex = (int)SpriteId.FemaleShield + Shield.Appearance * 8 + AnimationMotionOffsetWeapon;
                                        int shieldFrame = (((int)Direction - 1) * AnimationFrameCount) + AnimationFrame;
                                        SpriteHelper.DrawEquipment(spriteBatch, shieldIndex, shieldFrame, x, y, playerOffsetX, playerOffsetY, offsetX, offsetY, (equipmentColorType[EquipType.LeftHand] == GameColor.Custom) ? SpriteHelper.ColourFromArgb(equipmentColours[EquipType.LeftHand]) : Cache.Colors[equipmentColorType[EquipType.LeftHand]], fade, Shield.SpriteFileFemale);
                                    }
                                }
                            }

                            /* DRAW CAPE IN FRONT OF BODY */
                            if (AnimationDrawCapeFront)
                            {
                                if (Cache.GameSettings.TeamColorOn && teamColor != GameColor.None)
                                {
                                    int capeIndex = (int)SpriteId.FemaleCape + 45 + AnimationMotionOffset;
                                    int capeFrame = (((int)Direction - 1) * AnimationFrameCount) + AnimationFrame;
                                    SpriteHelper.DrawEquipment(spriteBatch, capeIndex, capeFrame, x, y, playerOffsetX, playerOffsetY, offsetX, offsetY, Cache.Colors[teamColor], fade);
                                }
                                else if (Cape != null) // cape
                                {
                                    int capeIndex = ((int)SpriteId.FemaleCape + Cape.Appearance * 15) + AnimationMotionOffset;
                                    int capeFrame = (((int)Direction - 1) * AnimationFrameCount) + AnimationFrame;
                                    SpriteHelper.DrawEquipment(spriteBatch, capeIndex, capeFrame, x, y, playerOffsetX, playerOffsetY, offsetX, offsetY, (equipmentColorType[EquipType.Back] == GameColor.Custom) ? SpriteHelper.ColourFromArgb(equipmentColours[EquipType.Back]) : Cache.Colors[equipmentColorType[EquipType.Back]], fade, Cape.SpriteFileFemale);
                                }
                            }
                        }
                        break;
                }

                // Dash/Flying
                if (Cache.GameSettings.DetailMode != GraphicsDetail.Low && offsetPoints != null &&
                        (motionType == MotionType.Dash || motionType == MotionType.Fly)) // dash/fly shadow
                {
                    foreach (int[] offset in offsetPoints)
                        SpriteHelper.DrawBody(spriteBatch, Type, Animation, AnimationFrame,
                                        x, y, playerOffsetX, playerOffsetY, offset[0], offset[1], Cache.Colors[GameColor.Shadow], 0.2f, out selected);

                    SpriteHelper.DrawBody(spriteBatch, Type, Animation, AnimationFrame,
                                        x, y, playerOffsetX, playerOffsetY, offsetX, offsetY, Cache.Colors[GameColor.Shadow], 0.2f, out selected);
                }

                if (showAura)
                {
                    if (IsBerserked)
                    {
                        if (Cache.GameSettings.DetailMode == GraphicsDetail.High)
                        {
                            int skintype = (Gender == GenderType.Male ? skintype = 2 : skintype = 4);
                            spriteBatch.End();
                            spriteBatch.Begin(SpriteSortMode.Deferred, BlendState.NonPremultiplied, null, null, null, Cache.PixelShaders[(int)PixelShader.Berserk]);
                            SpriteHelper.DrawBody(spriteBatch, skintype, Animation, AnimationFrame, x, y, playerOffsetX, playerOffsetY, offsetX, offsetY, Cache.Colors[GameColor.Normal], 0.5f, out selected);
                            spriteBatch.End();
                            spriteBatch.Begin();
                        }
                        else
                        {
                            SpriteHelper.DrawBody(spriteBatch, Type, Animation, AnimationFrame,
                                 x, y, playerOffsetX, playerOffsetY, offsetX, offsetY, Cache.Colors[GameColor.Berserk], 0.5f, out selected);
                        }
                    }
                    if (IsHaste)
                        SpriteHelper.DrawBody(spriteBatch, Type, Animation, AnimationFrame,
                                            x, y, playerOffsetX, playerOffsetY, offsetX, offsetY, Cache.Colors[GameColor.Haste], 0.5f, out selected);

                    if (IsPoisoned)
                        SpriteHelper.DrawEffect(spriteBatch, 81, effectFrame % 21, x + 115, y + 120 - 40, offsetX + playerOffsetX, offsetY + playerOffsetY, Color.White * 0.7f);

                    bool angelSelected = false;
                    if (equipment.ContainsKey(EquipType.Utility))
                        switch ((AngelType)equipment[EquipType.Utility])
                        {
                            case AngelType.Str: SpriteHelper.DrawAngel(spriteBatch, motionType, direction, 0, animationFrame, x, y, offsetX + playerOffsetX, offsetY + playerOffsetY, out angelSelected, "TutelarAngel1.spr"); break; // TODO fix hard coding
                            case AngelType.Dex: SpriteHelper.DrawAngel(spriteBatch, motionType, direction, 1, animationFrame, x, y, offsetX + playerOffsetX, offsetY + playerOffsetY, out angelSelected, "TutelarAngel2.spr"); break; // TODO fix hard coding
                            case AngelType.Int: SpriteHelper.DrawAngel(spriteBatch, motionType, direction, 2, animationFrame, x, y - 20, offsetX + playerOffsetX, offsetY + playerOffsetY, out angelSelected, "TutelarAngel3.spr"); break; // TODO fix hard coding
                            case AngelType.Mag: SpriteHelper.DrawAngel(spriteBatch, motionType, direction, 3, animationFrame, x, y - 20, offsetX + playerOffsetX, offsetY + playerOffsetY, out angelSelected, "TutelarAngel4.spr"); break; // TODO fix hard coding
                            case AngelType.Vit: SpriteHelper.DrawAngel(spriteBatch, motionType, direction, 0, animationFrame, x, y, offsetX + playerOffsetX, offsetY + playerOffsetY, out angelSelected, Angel.SpriteFile); break;// TODO fix hard coding
                            case AngelType.Agi: SpriteHelper.DrawAngel(spriteBatch, motionType, direction, 1, animationFrame, x, y, offsetX + playerOffsetX, offsetY + playerOffsetY, out angelSelected, Angel.SpriteFile); break;// TODO fix hard coding
                        }                       

                    if (!isDead && IsInvisible)
                        SpriteHelper.DrawBody(spriteBatch, Type, Animation, AnimationFrame,
                                            x, y, playerOffsetX, playerOffsetY, offsetX, offsetY, Cache.Colors[GameColor.Normal], (-0.3f + Cache.TransparencyFaders.GlowFrame), out selected);
                }
            }
            else //monsters
            {
                //DrawItemPopup Shadow
                if (!isDead && Cache.GameSettings.DetailMode != GraphicsDetail.Low) SpriteHelper.DrawMonsterShadow(spriteBatch, Type, Animation, AnimationFrame,
                            x, y, playerOffsetX, playerOffsetY, offsetX, offsetY, Cache.Colors[GameColor.Shadow], fade, out selected);

                if (showAura)
                {
                    // Defence shield
                    if ((status & 0x02000000) != 0)
                        SpriteHelper.DrawEffect(spriteBatch, 80, effectFrame % 17, x + 75, y + 107, offsetX + playerOffsetX, offsetY + playerOffsetY, Color.White * 0.5f);

                    // Protection From Magic
                    if ((status & 0x04000000) != 0)
                        SpriteHelper.DrawEffect(spriteBatch, 79, effectFrame % 15, x + 101, y + 135, offsetX + playerOffsetX, offsetY + playerOffsetY, Color.White * 0.7f);

                    // Protection From Arrow
                    if ((status & 0x08000000) != 0)
                        SpriteHelper.DrawEffect(spriteBatch, 72, effectFrame % 30, x, y + 35, offsetX + playerOffsetX, offsetY + playerOffsetY, Color.White * 0.7f);
                }

                // Dash/Flying
                if (Cache.GameSettings.DetailMode != GraphicsDetail.Low && offsetPoints != null &&
                        (motionType == MotionType.Dash || motionType == MotionType.Fly)) // dash/fly shadow
                {
                    foreach (int[] offset in offsetPoints)
                        SpriteHelper.DrawBody(spriteBatch, Type, Animation, AnimationFrame,
                                        x, y, playerOffsetX, playerOffsetY, offset[0], offset[1], Cache.Colors[GameColor.Shadow], 0.1f, out selected);
                }

                SpriteHelper.DrawMonster(spriteBatch, Id, Animation, AnimationFrame,
                                    x, y, playerOffsetX, playerOffsetY, offsetX, offsetY, colour, fade, out selected);

                if (showAura)
                {
                    if (IsBerserked)
                    {
                        if (Cache.GameSettings.DetailMode == GraphicsDetail.High)
                        {
                            spriteBatch.End();
                            spriteBatch.Begin(SpriteSortMode.Deferred, null, null, null, null, Cache.PixelShaders[(int)PixelShader.Berserk]);
                            SpriteHelper.DrawMonster(spriteBatch, Id, Animation, AnimationFrame,
                                x, y, playerOffsetX, playerOffsetY, offsetX, offsetY, Cache.Colors[GameColor.Normal], 0.5f, out selected);
                            spriteBatch.End();
                            spriteBatch.Begin();
                        }
                        else
                        {
                            SpriteHelper.DrawMonster(spriteBatch, Id, Animation, AnimationFrame,
                                x, y, playerOffsetX, playerOffsetY, offsetX, offsetY, Cache.Colors[GameColor.Berserk], 0.5f, out selected);
                        }
                    }
                    if (IsHaste)
                        SpriteHelper.DrawMonster(spriteBatch, Id, Animation, AnimationFrame,
                                                x, y, playerOffsetX, playerOffsetY, offsetX, offsetY, Cache.Colors[GameColor.Haste], 0.5f, out selected);
                    if (IsPoisoned)
                        SpriteHelper.DrawEffect(spriteBatch, 81, effectFrame % 21, x + 115, y + 120 - 40, offsetX + playerOffsetX, offsetY + playerOffsetY, Color.White * 0.7f);
                }
            }

            // Abaddon effects
            if (type == 81)
            {
                /*int randFrame = animationFrame % 12;
                SpriteHelper.DrawEffect(spriteBatch, 154, randFrame, x - 50, y - 50, offsetX + playerOffsetX, offsetY + playerOffsetY, Color.White * 0.7f);
                SpriteHelper.DrawEffect(spriteBatch, 155, randFrame, x - 20, y - 80, offsetX + playerOffsetX, offsetY + playerOffsetY, Color.White * 0.7f);
                SpriteHelper.DrawEffect(spriteBatch, 156, randFrame, x + 70, y - 50, offsetX + playerOffsetX, offsetY + playerOffsetY, Color.White * 0.7f);
                SpriteHelper.DrawEffect(spriteBatch, 157, randFrame, x - 30, y, offsetX + playerOffsetX, offsetY + playerOffsetY, Color.White * 0.7f);
                SpriteHelper.DrawEffect(spriteBatch, 158, randFrame, x - 60, y + 90, offsetX + playerOffsetX, offsetY + playerOffsetY, Color.White * 0.7f);
                SpriteHelper.DrawEffect(spriteBatch, 159, randFrame, x + 65, y + 85, offsetX + playerOffsetX, offsetY + playerOffsetY, Color.White * 0.7f);

                switch (direction)
                {
                    case MotionDirection.North:
                        SpriteHelper.DrawEffect(spriteBatch, 153, effectFrame % 28, x, y + 108, offsetX + playerOffsetX, offsetY + playerOffsetY, Color.White * 0.7f);
                        SpriteHelper.DrawEffect(spriteBatch, 164, effectFrame % 15, x - 50, y + 10, offsetX + playerOffsetX, offsetY + playerOffsetY, Color.White * 0.7f);
                        break;
                    case MotionDirection.NorthEast:
                        SpriteHelper.DrawEffect(spriteBatch, 153, effectFrame % 28, x, y + 95, offsetX + playerOffsetX, offsetY + playerOffsetY, Color.White * 0.7f);
                        SpriteHelper.DrawEffect(spriteBatch, 164, effectFrame % 15, x - 70, y + 10, offsetX + playerOffsetX, offsetY + playerOffsetY, Color.White * 0.7f);
                        break;
                    case MotionDirection.East:
                        SpriteHelper.DrawEffect(spriteBatch, 153, effectFrame % 28, x, y + 105, offsetX + playerOffsetX, offsetY + playerOffsetY, Color.White * 0.7f);
                        SpriteHelper.DrawEffect(spriteBatch, 164, effectFrame % 15, x - 90, y + 10, offsetX + playerOffsetX, offsetY + playerOffsetY, Color.White * 0.7f);
                        break;
                    case MotionDirection.SouthEast:
                        SpriteHelper.DrawEffect(spriteBatch, 153, effectFrame % 28, x -35, y + 100, offsetX + playerOffsetX, offsetY + playerOffsetY, Color.White * 0.7f);
                        SpriteHelper.DrawEffect(spriteBatch, 164, effectFrame % 15, x - 80, y + 10, offsetX + playerOffsetX, offsetY + playerOffsetY, Color.White * 0.7f);
                        break;
                    case MotionDirection.South:
                        SpriteHelper.DrawEffect(spriteBatch, 153, effectFrame % 28, x, y + 95, offsetX + playerOffsetX, offsetY + playerOffsetY, Color.White * 0.7f);
                        SpriteHelper.DrawEffect(spriteBatch, 164, effectFrame % 15, x - 65, y - 5, offsetX + playerOffsetX, offsetY + playerOffsetY, Color.White * 0.7f);
                        break;
                    case MotionDirection.SouthWest:
                        SpriteHelper.DrawEffect(spriteBatch, 153, effectFrame % 28, x + 45, y + 95, offsetX + playerOffsetX, offsetY + playerOffsetY, Color.White * 0.7f);
                        SpriteHelper.DrawEffect(spriteBatch, 164, effectFrame % 15, x - 31, y + 10, offsetX + playerOffsetX, offsetY + playerOffsetY, Color.White * 0.7f);
                        break;
                    case MotionDirection.West:
                        SpriteHelper.DrawEffect(spriteBatch, 153, effectFrame % 28, x + 40, y + 118, offsetX + playerOffsetX, offsetY + playerOffsetY, Color.White * 0.7f);
                        SpriteHelper.DrawEffect(spriteBatch, 164, effectFrame % 15, x - 30, y + 10, offsetX + playerOffsetX, offsetY + playerOffsetY, Color.White * 0.7f);
                        break;
                    case MotionDirection.NorthWest:
                        SpriteHelper.DrawEffect(spriteBatch, 153, effectFrame % 28, x + 20, y + 110, offsetX + playerOffsetX, offsetY + playerOffsetY, Color.White * 0.7f);
                        SpriteHelper.DrawEffect(spriteBatch, 164, effectFrame % 15, x - 20, y + 16, offsetX + playerOffsetX, offsetY + playerOffsetY, Color.White * 0.7f);
                        break;
                }*/
            }

            //Damage History
            if (damageHistory.Color != GameColor.None)
            {
                //Outline
                spriteBatch.DrawString(Cache.Fonts[damageHistory.Font], damageHistory.Message, new Vector2((int)(x - (Cache.Fonts[damageHistory.Font].MeasureString(damageHistory.Message).X / 2) + 10 + playerOffsetX + offsetX + 1 + damageHistory.X), (int)(y - (Cache.Fonts[damageHistory.Font].MeasureString(damageHistory.Message).Y / 2) - 45 + playerOffsetY + offsetY + 1 + damageHistory.Y)), Color.Black * damageHistory.Transparency, 0.0f, new Vector2((int)Cache.Fonts[damageHistory.Font].MeasureString(damageHistory.Message).X / 2, (int)Cache.Fonts[damageHistory.Font].MeasureString(damageHistory.Message).Y / 2), damageHistory.Scale, SpriteEffects.None, 0.0f);
                //spriteBatch.DrawString(display.Fonts[damageHistory.FontType], damageHistory.Message, new Vector2((int)(x - (display.Fonts[damageHistory.FontType].MeasureString(damageHistory.Message).X / 2) + 10 + playerOffsetX + offsetX - 1 + damageHistory.X), (int)(y - (display.Fonts[damageHistory.FontType].MeasureString(damageHistory.Message).Y / 2) - 45 + playerOffsetY + offsetY - 1 + damageHistory.Y)), Color.Black * damageHistory.Transparency, 0.0f, new Vector2((int)display.Fonts[damageHistory.FontType].MeasureString(damageHistory.Message).X / 2, (int)display.Fonts[damageHistory.FontType].MeasureString(damageHistory.Message).Y / 2), damageHistory.Scale, SpriteEffects.None, 0.0f);
                //spriteBatch.DrawString(display.Fonts[damageHistory.FontType], damageHistory.Message, new Vector2((int)(x - (display.Fonts[damageHistory.FontType].MeasureString(damageHistory.Message).X / 2) + 10 + playerOffsetX + offsetX + 1 + damageHistory.X), (int)(y - (display.Fonts[damageHistory.FontType].MeasureString(damageHistory.Message).Y / 2) - 45 + playerOffsetY + offsetY - 1 + damageHistory.Y)), Color.Black * damageHistory.Transparency, 0.0f, new Vector2((int)display.Fonts[damageHistory.FontType].MeasureString(damageHistory.Message).X / 2, (int)display.Fonts[damageHistory.FontType].MeasureString(damageHistory.Message).Y / 2), damageHistory.Scale, SpriteEffects.None, 0.0f);
                //spriteBatch.DrawString(display.Fonts[damageHistory.FontType], damageHistory.Message, new Vector2((int)(x - (display.Fonts[damageHistory.FontType].MeasureString(damageHistory.Message).X / 2) + 10 + playerOffsetX + offsetX - 1 + damageHistory.X), (int)(y - (display.Fonts[damageHistory.FontType].MeasureString(damageHistory.Message).Y / 2) - 45 + playerOffsetY + offsetY + 1 + damageHistory.Y)), Color.Black * damageHistory.Transparency, 0.0f, new Vector2((int)display.Fonts[damageHistory.FontType].MeasureString(damageHistory.Message).X / 2, (int)display.Fonts[damageHistory.FontType].MeasureString(damageHistory.Message).Y / 2), damageHistory.Scale, SpriteEffects.None, 0.0f);

                //Text
                spriteBatch.DrawString(Cache.Fonts[damageHistory.Font], damageHistory.Message, new Vector2((int)(x - (Cache.Fonts[damageHistory.Font].MeasureString(damageHistory.Message).X / 2) + 10 + playerOffsetX + offsetX + damageHistory.X), (int)(y - (Cache.Fonts[damageHistory.Font].MeasureString(damageHistory.Message).Y / 2) - 45 + playerOffsetY + offsetY + damageHistory.Y)), Cache.Colors[damageHistory.Color] * damageHistory.Transparency, 0.0f, new Vector2((int)Cache.Fonts[damageHistory.Font].MeasureString(damageHistory.Message).X / 2, (int)Cache.Fonts[damageHistory.Font].MeasureString(damageHistory.Message).Y / 2), damageHistory.Scale, SpriteEffects.None, 0.0f);
            }

            //Vital History
            if (vitalHistory.Color != GameColor.None)
            {
                spriteBatch.DrawString(Cache.Fonts[vitalHistory.Font], vitalHistory.Message, new Vector2((int)(x - (Cache.Fonts[vitalHistory.Font].MeasureString(vitalHistory.Message).X / 2) + playerOffsetX + offsetX + 1 + vitalHistory.X), (int)(y - (Cache.Fonts[vitalHistory.Font].MeasureString(vitalHistory.Message).Y / 2) - 30 + playerOffsetY + offsetY + 1 + vitalHistory.Y)), Color.Black * vitalHistory.Transparency, 0.0f, new Vector2((int)Cache.Fonts[vitalHistory.Font].MeasureString(vitalHistory.Message).X / 2, (int)Cache.Fonts[vitalHistory.Font].MeasureString(vitalHistory.Message).Y / 2), vitalHistory.Scale, SpriteEffects.None, 0.0f);
                spriteBatch.DrawString(Cache.Fonts[vitalHistory.Font], vitalHistory.Message, new Vector2((int)(x - (Cache.Fonts[vitalHistory.Font].MeasureString(vitalHistory.Message).X / 2) + playerOffsetX + offsetX + vitalHistory.X), (int)(y - (Cache.Fonts[vitalHistory.Font].MeasureString(vitalHistory.Message).Y / 2) - 30 + playerOffsetY + offsetY + vitalHistory.Y)), Cache.Colors[vitalHistory.Color] * vitalHistory.Transparency, 0.0f, new Vector2((int)Cache.Fonts[vitalHistory.Font].MeasureString(vitalHistory.Message).X / 2, (int)Cache.Fonts[vitalHistory.Font].MeasureString(vitalHistory.Message).Y / 2), vitalHistory.Scale, SpriteEffects.None, 0.0f);
            }

            /* Vital History Show Multiple*/
            //int index = 0;
            //if (vitalHistory.Count > 0)
            //{
            //    for (int i = vitalHistory.Count - 1; i >= 0; i--)
            //    {
            //        Color textColor = vitalHistory[i].Color;
            //        if (vitalHistory[i].Message.Contains("+")) { textColor = SpriteHelper.Lighten(textColor.R, textColor.G, textColor.B); }

            //        spriteBatch.DrawString(display.Fonts[vitalHistory[i].FontType], vitalHistory[i].Message, new Vector2((int)(x - (display.Fonts[vitalHistory[i].FontType].MeasureString(vitalHistory[i].Message).X / 2) + playerOffsetX + offsetX + 1), (int)(y - (display.Fonts[vitalHistory[i].FontType].MeasureString(vitalHistory[i].Message).Y / 2) - 36 + (9 * index) + playerOffsetY + offsetY + 1)), Color.Black * (vitalHistory[i].Transparency - (index * 0.1f)));
            //        //spriteBatch.DrawString(display.Fonts[vitalHistory[i].FontType], vitalHistory[i].Message, new Vector2((int)(x - (display.Fonts[vitalHistory[i].FontType].MeasureString(vitalHistory[i].Message).X / 2) + playerOffsetX + offsetX - 1), (int)(y - (display.Fonts[vitalHistory[i].FontType].MeasureString(vitalHistory[i].Message).Y / 2) - 36 + (9 * index) + playerOffsetY + offsetY - 1)), Color.Black * (vitalHistory[i].Transparency - (index * 0.1f)));
            //        //spriteBatch.DrawString(display.Fonts[vitalHistory[i].FontType], vitalHistory[i].Message, new Vector2((int)(x - (display.Fonts[vitalHistory[i].FontType].MeasureString(vitalHistory[i].Message).X / 2) + playerOffsetX + offsetX + 1), (int)(y - (display.Fonts[vitalHistory[i].FontType].MeasureString(vitalHistory[i].Message).Y / 2) - 36 + (9 * index) + playerOffsetY + offsetY - 1)), Color.Black * (vitalHistory[i].Transparency - (index * 0.1f)));
            //        //spriteBatch.DrawString(display.Fonts[vitalHistory[i].FontType], vitalHistory[i].Message, new Vector2((int)(x - (display.Fonts[vitalHistory[i].FontType].MeasureString(vitalHistory[i].Message).X / 2) + playerOffsetX + offsetX - 1), (int)(y - (display.Fonts[vitalHistory[i].FontType].MeasureString(vitalHistory[i].Message).Y / 2) - 36 + (9 * index) + playerOffsetY + offsetY + 1)), Color.Black * (vitalHistory[i].Transparency - (index * 0.1f)));

            //        spriteBatch.DrawString(display.Fonts[vitalHistory[i].FontType], vitalHistory[i].Message, new Vector2((int)(x - (display.Fonts[vitalHistory[i].FontType].MeasureString(vitalHistory[i].Message).X / 2) + playerOffsetX + offsetX), (int)(y - (display.Fonts[vitalHistory[i].FontType].MeasureString(vitalHistory[i].Message).Y / 2) - 36 + (9 * index) + playerOffsetY + offsetY)), textColor * (vitalHistory[i].Transparency - (index * 0.1f)));
            //        index++;
            //    }
            //}

            //Spell History
            if (spellHistory.Color != GameColor.None)
            {
                spriteBatch.DrawString(Cache.Fonts[spellHistory.Font], spellHistory.Message, new Vector2((int)(x - (Cache.Fonts[spellHistory.Font].MeasureString(spellHistory.Message).X / 2) + playerOffsetX + offsetX + 1), (int)(y - (Cache.Fonts[spellHistory.Font].MeasureString(spellHistory.Message).Y / 2) - 65 - spellHistory.Y + playerOffsetY + offsetY + 1)), Color.Black * spellHistory.Transparency);
                spriteBatch.DrawString(Cache.Fonts[spellHistory.Font], spellHistory.Message, new Vector2((int)(x - (Cache.Fonts[spellHistory.Font].MeasureString(spellHistory.Message).X / 2) + playerOffsetX + offsetX), (int)(y - (Cache.Fonts[spellHistory.Font].MeasureString(spellHistory.Message).Y / 2) - 65 - spellHistory.Y + playerOffsetY + offsetY)), Cache.Colors[spellHistory.Color] * spellHistory.Transparency);
            }

            //Chat History
            if (chatHistory.Color != GameColor.None)
            {
                spriteBatch.DrawString(Cache.Fonts[chatHistory.Font], chatHistory.Message, new Vector2((int)(x - (Cache.Fonts[chatHistory.Font].MeasureString(chatHistory.Message).X / 2) + playerOffsetX + offsetX + 1), (int)(y - (Cache.Fonts[chatHistory.Font].MeasureString(chatHistory.Message).Y / 2) - 45 - chatHistory.Y + playerOffsetY + offsetY + 1)), Color.Black * chatHistory.Transparency);
                spriteBatch.DrawString(Cache.Fonts[chatHistory.Font], chatHistory.Message, new Vector2((int)(x - (Cache.Fonts[chatHistory.Font].MeasureString(chatHistory.Message).X / 2) + playerOffsetX + offsetX), (int)(y - (Cache.Fonts[chatHistory.Font].MeasureString(chatHistory.Message).Y / 2) - 45 - chatHistory.Y + playerOffsetY + offsetY)), Cache.Colors[chatHistory.Color] * chatHistory.Transparency);
            }  

            return selected;
        }

        public OwnerRelationShip GetRelationship(OwnerSide playerSide)
        {
            //unchecked
            //{
            /*bool criminal = ((status & (int)0x80000000) == 0) ? false : true;
            bool citizen = ((status & (int)0x40000000) == 0) ? false : true;
            bool hunter = ((status & (int)0x10000000) == 0) ? false : true;

            if (!citizen)
            {
                if (playerSide == OwnerSide.Neutral) return OwnerRelationShip.Neutral;
                else return OwnerRelationShip.Enemy;
            }
            else */
            if (side == OwnerSide.Neutral) return OwnerRelationShip.Neutral;
            else if (side == playerSide)
            {
                if (sideStatus == OwnerSideStatus.Criminal) return OwnerRelationShip.Enemy;
                else
                {
                    if (playerSide == OwnerSide.Neutral) return OwnerRelationShip.Neutral;
                    else return OwnerRelationShip.Friendly;
                }
            }
            else return OwnerRelationShip.Enemy;
            //}
        }

        public bool Draw(SpriteBatch spriteBatch, GameDisplay display)
        {
            return false;
        }

        public void Update()
        {
            if (type == 0 || type > 200 || direction == MotionDirection.None) return;

            int frameSpeed = Cache.AnimationSpeeds[type, (int)motionType]; // speed of current frame in milliseconds
            int frameCount = (type >= 0 && type <= 6) ? (Cache.HumanAnimations.ContainsKey(Animation) ? Cache.HumanAnimations[Animation].Frames.Count : 1) : (Cache.MonsterAnimations.ContainsKey(Animation) ? Cache.MonsterAnimations[Animation].Frames.Count : 1); // number of frames in current animation
            int step = Globals.CellWidth / frameCount; // number of pixels to step per frame
            int distanceX = (x - Globals.OffScreenCells - Cache.MapView.PivotX) - Cache.DefaultState.Display.PlayerCenterX;
            int distanceY = (y - Globals.OffScreenCells - Cache.MapView.PivotY) - Cache.DefaultState.Display.PlayerCenterY;

            WeaponType weaponType = Utility.GetWeaponType(appearance2);
            if (frameSpeed <= 0) frameSpeed = (motionType == MotionType.Move || motionType == MotionType.Run || motionType == MotionType.DeadFadeOut) ? 50 : 100;
            // frame offsets
            switch (motionType)
            {
                case MotionType.Magic:
                    //if (skills[(int)SkillType.Magic] >= 100) frameSpeed -= 13; //HB has never downloaded owner skills for monsters - TODO
                    if (IsHaste) frameSpeed -= (frameSpeed / 5); // +20% faster
                    break;
                case MotionType.Attack:
                case MotionType.AttackStationary:
                case MotionType.BowAttack:
                case MotionType.Dash:
                    if (weaponType != WeaponType.Hand)
                        frameSpeed += (status & 0x000F) * 12; // swing speed
                    if (IsHaste) frameSpeed -= (frameSpeed / 5); // +20% faster
                    break;
            }
            if (IsFrozen) frameSpeed += (frameSpeed / 4); // +25% slower
            frameSpeed = (int)MathHelper.Clamp(frameSpeed, 10, 300);
            
            // dash/fly shadow
            if (motionType == MotionType.Dash)
            {
                if (tempFrame == 0) offsetPoints = new List<int[]>();
                else if (tempFrame >= 1 && tempFrame <= 8) offsetPoints.Add(new int[] { offsetX, offsetY });
                else if (tempFrame >= 13) offsetPoints.RemoveAt(0);
            }
            else if (motionType == MotionType.Fly)
            {
                if (animationFrame == 0) offsetPoints = new List<int[]>();
                else if (animationFrame >= 4) offsetPoints.RemoveAt(0); //TODO some bug here
                else if (animationFrame >= 1) offsetPoints.Add(new int[] { offsetX, offsetY });
            }

            // object is moving, animate between cell. note: humans have 8 walk/run frames, mobs have only 4
            offsetX = 0; offsetY = 0;
            switch (motionType)
            {
                case MotionType.Move:
                case MotionType.Run:
                    switch (direction)
                    {
                        case MotionDirection.North: offsetY -= (animationFrame - frameCount) * step; break;
                        case MotionDirection.NorthEast: offsetX = (animationFrame - frameCount) * step; offsetY -= (animationFrame - frameCount) * step; break;
                        case MotionDirection.East: offsetX = (animationFrame - frameCount) * step; break;
                        case MotionDirection.SouthEast: offsetX = (animationFrame - frameCount) * step; offsetY = (animationFrame - frameCount) * step; break;
                        case MotionDirection.South: offsetY = (animationFrame - frameCount) * step; break;
                        case MotionDirection.SouthWest: offsetX -= (animationFrame - frameCount) * step; offsetY = (animationFrame - frameCount) * step; break;
                        case MotionDirection.West: offsetX -= (animationFrame - frameCount) * step; break;
                        case MotionDirection.NorthWest: offsetX -= (animationFrame - frameCount) * step; offsetY -= (animationFrame - frameCount) * step; break;
                    }
                    break;
                case MotionType.Fly:
                    switch (direction)
                    {
                        case MotionDirection.North: offsetY -= (animationFrame - 4) * 8; break;
                        case MotionDirection.NorthEast: offsetX = (animationFrame - 4) * 8; offsetY -= (animationFrame - 4) * 8; break;
                        case MotionDirection.East: offsetX = (animationFrame - 4) * 8; break;
                        case MotionDirection.SouthEast: offsetX = (animationFrame - 4) * 8; offsetY = (animationFrame - 4) * 8; break;
                        case MotionDirection.South: offsetY = (animationFrame - 4) * 8; break;
                        case MotionDirection.SouthWest: offsetX -= (animationFrame - 4) * 8; offsetY = (animationFrame - 4) * 8; break;
                        case MotionDirection.West: offsetX -= (animationFrame - 4) * 8; break;
                        case MotionDirection.NorthWest: offsetX -= (animationFrame - 4) * 8; offsetY -= (animationFrame - 4) * 8; break;
                    }
                    break;
                case MotionType.Dash:
                    switch (direction)
                    {
                        case MotionDirection.North: offsetY -= (Math.Min(tempFrame, 8) - 8) * 4; break;
                        case MotionDirection.NorthEast: offsetX = (Math.Min(tempFrame, 8) - 8) * 4; offsetY -= (Math.Min(tempFrame, 8) - 8) * 4; break;
                        case MotionDirection.East: offsetX = (Math.Min(tempFrame, 8) - 8) * 4; break;
                        case MotionDirection.SouthEast: offsetX = (Math.Min(tempFrame, 8) - 8) * 4; offsetY = (Math.Min(tempFrame, 8) - 8) * 4; break;
                        case MotionDirection.South: offsetY = (Math.Min(tempFrame, 8) - 8) * 4; break;
                        case MotionDirection.SouthWest: offsetX -= (Math.Min(tempFrame, 8) - 8) * 4; offsetY = (Math.Min(tempFrame, 8) - 8) * 4; break;
                        case MotionDirection.West: offsetX -= (Math.Min(tempFrame, 8) - 8) * 4; break;
                        case MotionDirection.NorthWest: offsetX -= (Math.Min(tempFrame, 8) - 8) * 4; offsetY -= (Math.Min(tempFrame, 8) - 8) * 4; break;
                    }
                    break;
            }

            switch (type)
            {
                case 1:
                case 2:
                case 3:
                case 4:
                case 5:
                case 6:
                    if (SpriteHelper.SpriteExists(Cache.HumanAnimations, Animation, AnimationFrame))
                        if ((DateTime.Now - animationTime).Milliseconds > frameSpeed)
                        {
                            // internal frame counter for condition sprites
                            effectFrame = (effectFrame > 100 ? 0 : effectFrame + 1);

                            switch (motionType)
                            {
                                case MotionType.Dead:
                                    animationFrame = frameCount - 1;
                                    break;
                                case MotionType.DeadFadeOut:
                                    //animationFrame = frameCount - 1;

                                    if (fadeValue <= 0) Remove();
                                    else fadeValue -= 5;
                                    break;
                                case MotionType.Dash:
                                    if (tempFrame == 2) AudioHelper.PlaySound("C4");
                                    if (tempFrame == 4) AudioHelper.PlaySound("C11");
                                    if (tempFrame == 5)
                                    {
                                        switch (weaponType)
                                        {
                                            case WeaponType.Hand:
                                            case WeaponType.ShortSword: AudioHelper.PlaySound("C1"); break;
                                            case WeaponType.Fencing:
                                            case WeaponType.StormBlade:
                                            case WeaponType.LongSword: AudioHelper.PlaySound("C2"); break;
                                            case WeaponType.Hammer:
                                            case WeaponType.Wand:
                                            case WeaponType.BattleStaff:
                                            case WeaponType.Axe: AudioHelper.PlaySound("C18"); break;
                                            case WeaponType.ShortBow:
                                            case WeaponType.LongBow: AudioHelper.PlaySound("C3"); break;
                                        }
                                    }

                                    if (tempFrame > 16)
                                    {
                                        animationFrame = 0;
                                        tempFrame = 0;
                                        motionType = MotionType.Idle;
                                    }
                                    else if (tempFrame >= 4 && tempFrame <= 13) animationFrame = 4;
                                    else if (tempFrame == 14) animationFrame = 3;
                                    else if (tempFrame == 15) animationFrame = 2;
                                    else if (tempFrame == 16) animationFrame = 1;
                                    else animationFrame++;

                                    tempFrame++;
                                    break;
                                default:
                                    if (animationFrame + 1 < frameCount)
                                    {
                                        // twinkle twinkle little stars
                                        if (StarTwinkle && Cache.GameSettings.DetailMode == GraphicsDetail.High &&
                                            motionType != MotionType.Dead && (animationFrame == 1 || animationFrame == 5) && Cache.MapView != null)
                                        {
                                            IGameEffect twinkle = new GameEffect(DrawEffectType.Twinkle,
                                                             new Location(x - (Cache.MapView.PivotX + Globals.OffScreenCells), y - (Cache.MapView.PivotY + Globals.OffScreenCells)), null);
                                            //twinkle.Color = Cache.ChatColours[ChatType.GameMaster];
                                            Cache.DefaultState.AddEffect(twinkle);
                                        }

                                        switch (motionType)
                                        {
                                            case MotionType.Move:
                                                if (animationFrame == 1 || animationFrame == 5) AudioHelper.PlaySound("C8");
                                                break;
                                            case MotionType.Run:
                                                if (animationFrame == 1 || animationFrame == 5) AudioHelper.PlaySound("C10");
                                                break;
                                            case MotionType.BowAttack:
                                            case MotionType.Attack:
                                                if (animationFrame == 2)
                                                {
                                                    if (criticalAttack)
                                                        switch (Gender)
                                                        {
                                                            case GenderType.Male: AudioHelper.PlaySound("C23"); break;
                                                            case GenderType.Female: AudioHelper.PlaySound("C24"); break;
                                                        }

                                                    /*if (motionType == MotionType.BowAttack)
                                                        Cache.GameState.AddEffect(new GameEffect(DrawEffectType.Arrow,
                                                            new Location(x - (Cache.MapView.PivotX + Globals.OffScreenCells), y - (Cache.MapView.PivotY + Globals.OffScreenCells)),
                                                            new Location(attackDestinationX - (Cache.MapView.PivotX + Globals.OffScreenCells), attackDestinationY - (Cache.MapView.PivotY + Globals.OffScreenCells))));
                                                     */
                                                }
                                                if (animationFrame == 5)
                                                {
                                                    switch (weaponType)
                                                    {
                                                        case WeaponType.Hand:
                                                        case WeaponType.ShortSword: AudioHelper.PlaySound("C1"); break;
                                                        case WeaponType.Fencing:
                                                        case WeaponType.StormBlade:
                                                        case WeaponType.LongSword: AudioHelper.PlaySound("C2"); break;
                                                        case WeaponType.Hammer:
                                                        case WeaponType.Wand:
                                                        case WeaponType.BattleStaff:
                                                        case WeaponType.Axe: AudioHelper.PlaySound("C18"); break;
                                                        case WeaponType.ShortBow:
                                                        case WeaponType.LongBow: AudioHelper.PlaySound("C3"); break;
                                                    }
                                                    if (criticalAttack)
                                                        Cache.DefaultState.AddEffect(new GameEffect(DrawEffectType.Critical,
                                                            new Location(x - (Cache.MapView.PivotX + Globals.OffScreenCells), y - (Cache.MapView.PivotY + Globals.OffScreenCells)),
                                                            new Location(attackDestinationX - (Cache.MapView.PivotX + Globals.OffScreenCells), attackDestinationY - (Cache.MapView.PivotY + Globals.OffScreenCells))));
                                                }
                                                break;
                                            case MotionType.Magic:
                                                if (animationFrame == 1) AudioHelper.PlaySound("C16");
                                                break;
                                            case MotionType.TakeDamage:
                                                if (animationFrame == 2)
                                                    switch (Gender)
                                                    {
                                                        case GenderType.Male: AudioHelper.PlaySound("C12"); break;
                                                        case GenderType.Female: AudioHelper.PlaySound("C13"); break;
                                                    }
                                                break;
                                            case MotionType.Die:
                                                if (animationFrame == 2)
                                                    switch (Gender)
                                                    {
                                                        case GenderType.Male: AudioHelper.PlaySound("C14"); break;
                                                        case GenderType.Female: AudioHelper.PlaySound("C15"); break;
                                                    }
                                                break;
                                        }

                                        animationFrame++;
                                        animationTime = DateTime.Now;
                                    }
                                    else
                                    {
                                        switch (motionType)
                                        {
                                            case MotionType.Attack:
                                            case MotionType.BowAttack:
                                            case MotionType.PickUp:
                                            case MotionType.AttackStationary:
                                            case MotionType.Bow:
                                            case MotionType.TakeDamage:
                                            case MotionType.Magic:
                                            case MotionType.Move:
                                            case MotionType.Run:
                                            case MotionType.Idle:
                                            case MotionType.Fly:
                                                motionType = MotionType.Idle;
                                                animationFrame = 0;
                                                criticalAttack = false;
                                                break;
                                            case MotionType.Die:
                                                motionType = MotionType.Dead;
                                                animationFrame = frameCount - 1;
                                                break;
                                        }
                                    }
                                    break;
                            }
                        }
                    break;
                default:
                    if (SpriteHelper.SpriteExists(Cache.MonsterAnimations, Animation, AnimationFrame))
                        if ((DateTime.Now - animationTime).Milliseconds > frameSpeed)
                        {
                            // internal frame counter for condition sprites
                            effectFrame = (effectFrame > 100 ? 0 : effectFrame + 1);

                            if (motionType == MotionType.Dead)
                                animationFrame = frameCount - 1;
                            else if (motionType == MotionType.DeadFadeOut)
                            {
                                animationFrame = frameCount - 1;
                                if (fadeValue <= 0) Remove();
                                else fadeValue -= 5;
                            }
                            else if (animationFrame + 1 < frameCount)
                            {
                                switch (motionType)
                                {
                                    case MotionType.Move:
                                        if (animationFrame == 1)
                                            AudioHelper.PlaySound(Cache.AnimationSounds[type, (int)MotionType.Move], distanceX, distanceY);
                                        break;
                                    case MotionType.TakeDamage:
                                        if (animationFrame == 5)
                                            AudioHelper.PlaySound(Cache.AnimationSounds[type, (int)MotionType.TakeDamage], distanceX, distanceY);
                                        break;
                                    case MotionType.Attack:
                                        if (animationFrame == 1)
                                            AudioHelper.PlaySound(Cache.AnimationSounds[type, (int)MotionType.Attack], distanceX, distanceY);
                                        break;
                                    case MotionType.Die:
                                        if (animationFrame == 1)
                                            AudioHelper.PlaySound(Cache.AnimationSounds[type, (int)MotionType.Die], distanceX, distanceY);
                                        break;

                                }
                                animationFrame++;
                                animationTime = DateTime.Now;
                            }
                            else
                            {
                                switch (motionType)
                                {
                                    case MotionType.Attack:
                                    case MotionType.PickUp:
                                    case MotionType.AttackStationary:
                                    case MotionType.Bow:
                                    case MotionType.TakeDamage:
                                    case MotionType.Fly:
                                    case MotionType.Magic:
                                    case MotionType.Move:
                                    case MotionType.Run:
                                    case MotionType.Idle:
                                        motionType = MotionType.Idle;
                                        animationFrame = tempFrame = 0;
                                        break;
                                    case MotionType.Die:
                                        motionType = MotionType.Dead;
                                        animationFrame = frameCount - 1;
                                        break;
                                }
                            }
                        }
                    break;
            }

            /* DAMAGE HISTORY */
            if (damageHistory.Color != GameColor.None)
            {
                //Remove
                if ((DateTime.Now - damageHistory.MessageTime).Seconds >= 3) { damageHistory.Clear(); }

                if (((int)(DateTime.Now - damageHistory.MessageTime).TotalMilliseconds / 20) > damageHistory.AnimationTime)
                {
                    damageHistory.AnimationTime += 1;

                    //Handle Scale
                    if (damageHistory.AnimationTime > 5 && damageHistory.AnimationTime < 15) { damageHistory.Scale -= 0.05f; }
                    else if (damageHistory.AnimationTime > 15 && damageHistory.Scale != 1.0f) { damageHistory.Scale = 1.0f; }

                    //Transparency
                    if (damageHistory.AnimationTime > 140) { damageHistory.Transparency -= 0.1f; }
                }
            }

            /* Vital HISTORY */
            if (vitalHistory.Color != GameColor.None)
            {
                //Remove
                if ((DateTime.Now - vitalHistory.MessageTime).Seconds >= 3) { vitalHistory.Clear(); }

                if (((int)(DateTime.Now - vitalHistory.MessageTime).TotalMilliseconds / 20) > vitalHistory.AnimationTime)
                {
                    vitalHistory.AnimationTime += 1;

                    //Handle Scale
                    if (vitalHistory.AnimationTime > 5 && vitalHistory.AnimationTime < 15) { vitalHistory.Scale -= 0.05f; }
                    else if (vitalHistory.AnimationTime > 15 && vitalHistory.Scale != 1.0f) { vitalHistory.Scale = 1.0f; }

                    //Transparency
                    if (vitalHistory.AnimationTime > 140) { vitalHistory.Transparency -= 0.1f; }
                }
            }

            ///* VITAL HISTORY Show multple*/
            //if (vitalHistory.Count > 0)
            //{
            //    //Remove expired
            //    for (int i = 0; i < vitalHistory.Count; i++)
            //    {
            //        if ((DateTime.Now - vitalHistory[i].MessageTime).Seconds >= 3) { vitalHistory.RemoveAt(i); }
            //    }

            //    //trim if needed
            //    if (vitalHistory.Count > 5)
            //    {
            //        int removeAmount = vitalHistory.Count - 5;
            //        vitalHistory.RemoveRange(0, removeAmount);
            //    }

            //    for (int i = 0; i < vitalHistory.Count; i++)
            //    {
            //        if (((int)(DateTime.Now - vitalHistory[i].MessageTime).TotalMilliseconds / 20) > vitalHistory[i].AnimationTime)
            //        {
            //            vitalHistory[i].AnimationTime += 1;
            //            if (vitalHistory[i].AnimationTime > 100) { vitalHistory[i].Transparency -= 0.02f; }
            //        }
            //    }
            //}

            /*Spell History */
            if (spellHistory.Color != GameColor.None)
            {
                if (((int)(DateTime.Now - spellHistory.MessageTime).TotalMilliseconds / 20) > spellHistory.AnimationTime)
                { 
                    spellHistory.AnimationTime += 1; 
                    if (spellHistory.AnimationTime < 10) { spellHistory.Y += 1; }
                    if (spellHistory.AnimationTime > 100) { spellHistory.Transparency -= 0.02f; }
                }

                //Remove
                if ((DateTime.Now - spellHistory.MessageTime).Seconds >= 3) { spellHistory.Clear(); }
            }

            /*CHAT HISTORY*/
            if (chatHistory.Color != GameColor.None)
            {
                if (((int)(DateTime.Now - chatHistory.MessageTime).TotalMilliseconds / 20) > chatHistory.AnimationTime)
                {
                    chatHistory.AnimationTime += 1;
                    if (chatHistory.AnimationTime < 10) { chatHistory.Y += 1; }
                    if (chatHistory.AnimationTime > 200) { chatHistory.Transparency -= 0.02f; }
                }

                //Remove
                if ((DateTime.Now - chatHistory.MessageTime).Seconds >= 5) { chatHistory.Clear(); }
            }
        }

        public int Id { get; set; }
        public UInt16 ObjectId { get { return id; } set { id = value; } }
        public bool IsMoving { get { return isMoving; } set { isMoving = value; } }
        public int X { get { return x; } }
        public int Y { get { return y; } }
        public int Type { get { return type; } set { type = value; } }
        public OwnerType OwnerType { get { return (id < 10000) ? OwnerType.Player : OwnerType.Npc; } }
        public OwnerSide Side { get { return side; } set { side = value; } }
        public OwnerSideStatus SideStatus { get { return sideStatus; } set { sideStatus = value; } }
        public MotionDirection Direction { get { return direction; } set { direction = value; } }
        public MotionType Motion { get { return motionType; } set { motionType = value; } }
        public Map Map { get { return currentMap; } set { currentMap = value; } }
        public MapTile Location { get { return (Map.LocationExists(x, y) ? Map[y][x] : null); } }
        public string Name { get { return name; } set { name = value; } }
        public List<NpcPerk> Perks { get { return perks; } }
        public int Appearance1 { get { return appearance1; } set { appearance1 = value; } }
        public int Appearance2 { get { return appearance2; } set { appearance2 = value; } }
        public int Appearance3 { get { return appearance3; } set { appearance3 = value; } }
        public int Appearance4 { get { return appearance4; } set { appearance4 = value; } }
        public int AppearanceColour { get { return appearanceColour; } set { appearanceColour = value; } }
        public Item Helmet { get { return (equipment.ContainsKey(EquipType.Head) && Cache.ItemConfiguration.ContainsKey(equipment[EquipType.Head]) ? Cache.ItemConfiguration[equipment[EquipType.Head]] : null); } }
        public Item BodyArmour { get { return (equipment.ContainsKey(EquipType.Body) && Cache.ItemConfiguration.ContainsKey(equipment[EquipType.Body]) ? Cache.ItemConfiguration[equipment[EquipType.Body]] : null); } }
        public Item Hauberk { get { return (equipment.ContainsKey(EquipType.Arms) && Cache.ItemConfiguration.ContainsKey(equipment[EquipType.Arms]) ? Cache.ItemConfiguration[equipment[EquipType.Arms]] : null); } }
        public Item Leggings { get { return (equipment.ContainsKey(EquipType.Legs) && Cache.ItemConfiguration.ContainsKey(equipment[EquipType.Legs]) ? Cache.ItemConfiguration[equipment[EquipType.Legs]] : null); } }
        public Item Boots { get { return (equipment.ContainsKey(EquipType.Feet) && Cache.ItemConfiguration.ContainsKey(equipment[EquipType.Feet]) ? Cache.ItemConfiguration[equipment[EquipType.Feet]] : null); } }
        public Item Cape { get { return (equipment.ContainsKey(EquipType.Back) && Cache.ItemConfiguration.ContainsKey(equipment[EquipType.Back]) ? Cache.ItemConfiguration[equipment[EquipType.Back]] : null); } }
        public Item Weapon { get { return (equipment.ContainsKey(EquipType.RightHand) && Cache.ItemConfiguration.ContainsKey(equipment[EquipType.RightHand]) ? Cache.ItemConfiguration[equipment[EquipType.RightHand]] : null); } }
        public Item Shield { get { return (equipment.ContainsKey(EquipType.LeftHand) && Cache.ItemConfiguration.ContainsKey(equipment[EquipType.LeftHand]) ? Cache.ItemConfiguration[equipment[EquipType.LeftHand]] : null); } }
        public Item Angel { get { return (equipment.ContainsKey(EquipType.Utility) && Cache.ItemConfiguration.ContainsKey(equipment[EquipType.Utility]) ? Cache.ItemConfiguration[equipment[EquipType.Utility]] : null); } }
        public bool StarTwinkle
        {
            get
            {
                // shields
                if (Shield != null)
                    switch (Shield.SpecialAbilityType)
                    {
                        case ItemSpecialAbilityType.MerienShield: return true;
                    }

                // weapons
                if (Weapon != null)
                    switch (Weapon.SpecialAbilityType)
                    {
                        case ItemSpecialAbilityType.IceWeapon:
                        case ItemSpecialAbilityType.MedusaWeapon:
                        case ItemSpecialAbilityType.XelimaWeapon: return true;
                    }

                // armours
                if (BodyArmour != null)
                    switch (BodyArmour.SpecialAbilityType)
                    {
                        case ItemSpecialAbilityType.MerienArmour: return true;
                    }

                return false;
            }
        }
        public bool AngelTwinkle { get { return false; } } // TODO

        public bool Hero
        {
            get
            {
                if (Cape.Set == ItemSet.HeroCape) return true;
                return false;
            }
        }

        public bool HeroWarrior
        {
            get
            {
                if (Helmet.Set == ItemSet.HeroHelm &&
                    Hauberk.Set == ItemSet.HeroHauberk &&
                    BodyArmour.Set == ItemSet.HeroPlate &&
                    Leggings.Set == ItemSet.HeroLeggings) return true;
                return false;
            }
        }

        public bool HeroMage
        {
            get
            {
                if (Helmet.Set == ItemSet.HeroCap &&
                    Hauberk.Set == ItemSet.HeroHauberk &&
                    BodyArmour.Set == ItemSet.HeroRobe &&
                    Leggings.Set == ItemSet.HeroLeggings) return true;
                return false;
            }
        }

        public bool HeroBattleMage
        {
            get
            {
                if (Helmet.Set == ItemSet.HeroBattleCap &&
                    Hauberk.Set == ItemSet.HeroHauberk &&
                    BodyArmour.Set == ItemSet.HeroBattlePlate &&
                    Leggings.Set == ItemSet.HeroLeggings) return true;
                return false;
            }
        }

        public bool HeroArcher
        {
            get
            {
                if (Helmet.Set == ItemSet.HeroHood &&
                    Hauberk.Set == ItemSet.HeroHauberk &&
                    BodyArmour.Set == ItemSet.HeroLeather &&
                    Leggings.Set == ItemSet.HeroLeggings) return true;
                return false;
            }
        }

        public bool Godly
        {
            get
            {
                if (Cape.Set == ItemSet.GodlyCape) return true;
                return false;
            }
        }

        public bool GodlyWarrior
        {
            get
            {
                if (Helmet.Set == ItemSet.GodlyHelm &&
                    Hauberk.Set == ItemSet.GodlyHauberk &&
                    BodyArmour.Set == ItemSet.GodlyPlate &&
                    Leggings.Set == ItemSet.GodlyLeggings) return true;
                return false;
            }
        }

        public bool GodlyMage
        {
            get
            {
                if (Helmet.Set == ItemSet.GodlyCap &&
                    Hauberk.Set == ItemSet.GodlyHauberk &&
                    BodyArmour.Set == ItemSet.GodlyRobe &&
                    Leggings.Set == ItemSet.GodlyLeggings) return true;
                return false;
            }
        }

        public bool GodlyBattleMage
        {
            get
            {
                if (Helmet.Set == ItemSet.GodlyBattleCap &&
                    Hauberk.Set == ItemSet.GodlyHauberk &&
                    BodyArmour.Set == ItemSet.GodlyBattlePlate &&
                    Leggings.Set == ItemSet.GodlyLeggings) return true;
                return false;
            }
        }

        public bool GodlyArcher
        {
            get
            {
                if (Helmet.Set == ItemSet.GodlyHood &&
                    Hauberk.Set == ItemSet.GodlyHauberk &&
                    BodyArmour.Set == ItemSet.GodlyLeather &&
                    Leggings.Set == ItemSet.GodlyLeggings) return true;
                return false;
            }
        }

        public int Status { get { return status; } set { status = value; } }
        public DateTime AnimationTime { get { return animationTime; } set { animationTime = value; } }
        public int AnimationFrame { get { return animationFrame; } set { animationFrame = value; } }
        public int Animation
        {
            get
            {
                int temp;
                if (type <= 6)
                {
                    temp = ((int)type * 100);
                    switch (motionType)
                    {
                        case MotionType.Idle: temp += ((IsCombatMode) ? 8 : 0); break;
                        case MotionType.Move: temp += ((IsCombatMode) ? 24 : 16); break;
                        case MotionType.Run: temp += 32; break;
                        case MotionType.Bow: temp += 40; break;
                        case MotionType.Dash:
                        case MotionType.Attack: temp += 48; break;
                        case MotionType.BowAttack: temp += 56; break;
                        case MotionType.Magic: temp += 64; break;
                        case MotionType.PickUp: temp += 72; break;
                        case MotionType.Fly:
                        case MotionType.TakeDamage: temp += 80; break;
                        case MotionType.Dead:
                        case MotionType.DeadFadeOut:
                        case MotionType.Die: temp += 88; break;
                    }
                }
                else
                {
                    temp = ((int)Id * 100);

                    // TODO - hard coded for wyvern/fw/abby
                    switch (type)
                    {
                        case 66: // iw
                        case 73: // fw - both only have 3 animations
                            switch (motionType)
                            {
                                case MotionType.Move:
                                case MotionType.Fly: temp += 8; break;
                                case MotionType.Idle:
                                case MotionType.TakeDamage:
                                case MotionType.Attack: temp += 0; break;
                                case MotionType.Dead:
                                case MotionType.DeadFadeOut:
                                case MotionType.Die: temp += 16; break;
                            }
                            break;
                        default:
                            switch (motionType)
                            {
                                case MotionType.Idle: temp += 0; break;
                                case MotionType.Move: temp += 8; break;
                                case MotionType.Attack: temp += 16; break;
                                case MotionType.Fly:
                                case MotionType.TakeDamage: temp += 24; break;
                                case MotionType.Dead:
                                case MotionType.DeadFadeOut:
                                case MotionType.Die: temp += 32; break;
                            }
                            break;
                    }
                }
                return temp + ((int)direction - 1);
            }
        }
        public int AnimationMotionOffset
        {
            get
            {
                int index = 0;
                switch (Motion)
                {
                    case MotionType.Idle: index = (IsCombatMode) ? 1 : 0; break;
                    case MotionType.Move: index = (IsCombatMode) ? 3 : 2; break;
                    case MotionType.Run: index = 4; break;
                    case MotionType.Bow: index = 5; break;
                    case MotionType.Dash:
                    case MotionType.Attack: index = 6; break;
                    case MotionType.BowAttack: index = 7; break;
                    case MotionType.Magic: index = 8; break;
                    case MotionType.PickUp: index = 9; break;
                    case MotionType.Fly:
                    case MotionType.TakeDamage: index = 10; break;
                    case MotionType.Die: index = 11; break;
                    case MotionType.DeadFadeOut:
                    case MotionType.Dead: index = 11; break;
                }

                return index;
            }
        }
        public int AnimationMotionOffsetWeapon
        {
            get
            {
                int index = 0;
                switch (Motion)
                {
                    case MotionType.Idle: index = (IsCombatMode) ? 1 : 0; break;
                    case MotionType.Move: index = (IsCombatMode) ? 3 : 2; break;
                    case MotionType.Dash:
                    case MotionType.BowAttack:
                    case MotionType.Attack: index = 4; break;
                    case MotionType.Fly:
                    case MotionType.TakeDamage: index = 5; break;
                    case MotionType.Run: index = 6; break;
                }

                return index;
            }
        }
        public bool AnimationDrawCapeFront
        {
            get
            {
                if (isDead) return true;
                if (motionType == MotionType.PickUp) return true;

                switch (direction)
                {
                    case MotionDirection.West:
                    case MotionDirection.NorthWest:
                    case MotionDirection.North:
                    case MotionDirection.NorthEast:
                    case MotionDirection.East:
                        return true;
                    case MotionDirection.SouthEast:
                    case MotionDirection.South:
                    case MotionDirection.SouthWest:
                        return false;
                    default: return false;
                }
            }
        }
        public bool AnimationDrawShieldFront
        {
            get
            {

                if (IsCombatMode)
                {
                    switch (direction)
                    {
                        case MotionDirection.North:
                        case MotionDirection.NorthEast:
                        case MotionDirection.NorthWest:
                            return false;
                        case MotionDirection.East:
                        case MotionDirection.South:
                        case MotionDirection.SouthEast:
                        case MotionDirection.West:
                        case MotionDirection.SouthWest:
                            return true;
                        default: return false;
                    }
                }
                else
                {
                    switch (direction)
                    {
                        case MotionDirection.North:
                        case MotionDirection.NorthEast:
                        case MotionDirection.East:
                        case MotionDirection.SouthEast:
                            return false;
                        case MotionDirection.South:
                        case MotionDirection.NorthWest:
                        case MotionDirection.West:
                        case MotionDirection.SouthWest:
                            return true;
                        default: return false;
                    }
                }
            }
        }
        public bool AnimationDrawWeaponFront
        {
            get
            {
                switch (direction)
                {
                    case MotionDirection.North:
                    case MotionDirection.NorthEast:
                    case MotionDirection.NorthWest:
                    case MotionDirection.West:
                        return false;
                    case MotionDirection.SouthWest:
                    case MotionDirection.South:
                    case MotionDirection.SouthEast:
                    case MotionDirection.East:
                        return true;
                    default: return false;
                }
            }
        }
        public int AnimationFrameCount
        {
            get
            {
                switch (Motion)
                {
                    case MotionType.Idle:
                    case MotionType.Move:
                    case MotionType.Run:
                    case MotionType.Bow:
                    case MotionType.Dash:
                    case MotionType.Attack:
                    case MotionType.BowAttack:
                    case MotionType.Die:
                    default:
                        return 8;
                    case MotionType.Fly:
                    case MotionType.TakeDamage:
                    case MotionType.PickUp:
                        return 4;
                    case MotionType.Dead:
                        return 8; // fixed from 1
                    case MotionType.Magic:
                        return 16;
                }
            }
        }

        public bool IsCombatMode { get { return isCombatMode; } }
        public bool IsDead { get { return isDead; } }
        public bool IsCorpseExploited { get { return isCorpseExploited; } set { isCorpseExploited = value; } }
        public bool IsAbilityActivated { get { return (appearance4 & 0x00F0) != 0; } }
        public bool IsInvisible { get { return (status & 0x10) != 0; } }
        public bool IsBerserked { get { return (status & 0x20) != 0; } }
        public bool IsPoisoned { get { return (status & 0x80) != 0; } }
        public bool IsHaste { get { return false; } }
        public bool IsFrozen { get { return (status & 0x40) != 0; } }
        public int OffsetX { get { return offsetX; } }
        public int OffsetY { get { return offsetY; } }
        public string GuildName { get { return guildName; } set { guildName = value; } }
        public int GuildRank { get { return guildRank; } set { guildRank = value; } }
        public int MerchantId { get { return merchantId; } set { merchantId = value; } }
        public MerchantType MerchantType { get { return merchantType; } set { merchantType = value; } }
        public bool IsMerchant { get { return MerchantType != MerchantType.None; } }
        public List<ChatMessage> DamageHistoryStageTwo { get { return damageHistoryStageTwo; } set { damageHistoryStageTwo = value; } }
        //public List<ChatMessage> VitalHistory { get { return vitalHistory; } set { vitalHistory = value; } }
        public ChatMessage VitalHistory {  get { return vitalHistory; } set { vitalHistory = value; } }
        public ChatMessage DamageHistory { get { return damageHistory; } set { damageHistory = value; } }
        public ChatMessage SpellHistory { get { return spellHistory; } set { spellHistory = value; } }
        public ChatMessage ChatHistory { get { return chatHistory; } set { chatHistory = value; } }
        public GenderType Gender
        {
            get
            {
                switch (type)
                {
                    case 1:
                    case 2:
                    case 3:
                        return GenderType.Male;
                    case 4:
                    case 5:
                    case 6:
                        return GenderType.Female;
                    default: return GenderType.None;
                }
            }
        }
    }
}
