﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Helbreath.Common;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace Client.Assets
{
    public class GameDisplay
    {
        private GraphicsDetail detailmode;
        private bool debugMoge;
        private bool fullScreen;

        private GraphicsDevice device;
        private GraphicsDeviceManager deviceManager;
        private GameMouse mouse;
        private GameKeyboard keyboard;
        private Rectangle zoom;


        public GameDisplay(Resolution resolution, bool fullScreen)
        {
            this.Resolution = resolution;
            this.fullScreen = fullScreen;
            zoom = new Rectangle(GameBoardShiftX, GameBoardShiftY, ResolutionWidth, ResolutionHeight);
        }

        public void ToggleFullScreen()
        {
            deviceManager.IsFullScreen = !deviceManager.IsFullScreen;
            deviceManager.ApplyChanges();
        }

        public Resolution Resolution;
        public int ResolutionWidth { get { return Globals.Resolutions[(int)Resolution, (int)ResolutionSetting.Width]; } }
        public int ResolutionHeight { get { return Globals.Resolutions[(int)Resolution, (int)ResolutionSetting.Height]; } }
        public int GameBoardWidth { get { return Globals.Resolutions[(int)Resolution, (int)ResolutionSetting.GameBoardWidth]; } }
        public int GameBoardHeight { get { return Globals.Resolutions[(int)Resolution, (int)ResolutionSetting.GameBoardHeight]; } }
        public int GameBoardShiftX { get { return Globals.Resolutions[(int)Resolution, (int)ResolutionSetting.GameBoardShiftX]; } }
        public int GameBoardShiftY { get { return Globals.Resolutions[(int)Resolution, (int)ResolutionSetting.GameBoardShiftY]; } }
        public int PlayerCenterX { get { return Globals.Resolutions[(int)Resolution, (int)ResolutionSetting.CenterX]; } }
        public int PlayerCenterY { get { return Globals.Resolutions[(int)Resolution, (int)ResolutionSetting.CenterY]; } }
        public bool DebugMode { get { return debugMoge; } set { debugMoge = value; } }
        public GraphicsDevice Device { get { return device; } set { device = value; } }
        public GraphicsDeviceManager DeviceManager { get { return deviceManager; } set { deviceManager = value; } }
        public bool FullScreen { get { return fullScreen; } }
        public GameMouse Mouse { get { return mouse; } set { mouse = value; } }
        public GameKeyboard Keyboard { get { return keyboard; } set { keyboard = value; } }
        public Rectangle Zoom { get { return zoom; } set { zoom = value; } }
    }
}
