﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Helbreath.Common;

namespace Client.Assets.UI
{
    public class BackGroundImage
    {
        private Texture2D texture;
        private Vector2 drawposition;
        private BackgroundType type;
        private Microsoft.Xna.Framework.Color color;
        private float scale;
        private int counter;
        private float rotation;
        private Vector2 origin;
        private Microsoft.Xna.Framework.Rectangle source;

        public BackGroundImage()
        {
            this.texture = null;
            this.drawposition = new Vector2(0, 0);
            this.color = Microsoft.Xna.Framework.Color.White;
            this.type = BackgroundType.Background;
            this.scale = 1.0f;
            this.counter = 0;
            this.rotation = 0;
            this.origin = new Vector2(0, 0);
            if (this.texture == null)
            {
                this.source = new Microsoft.Xna.Framework.Rectangle(0, 0, 0, 0);
            }
            else
            {
                this.source = new Microsoft.Xna.Framework.Rectangle(0, 0, texture.Bounds.Width, texture.Bounds.Height);
            }
        }

        public BackGroundImage(BackgroundType background)
        {
            this.texture = null;
            this.drawposition = new Vector2(0, 0);
            this.color = Microsoft.Xna.Framework.Color.White;
            this.type = background;
            this.scale = 1.0f;
            this.counter = 0;
            this.rotation = 0;
            this.origin = new Vector2(0, 0);
            if (this.texture == null)
            {
                this.source = new Microsoft.Xna.Framework.Rectangle(0, 0, 0, 0);
            }
            else
            {
                this.source = new Microsoft.Xna.Framework.Rectangle(0, 0, texture.Bounds.Width, texture.Bounds.Height);
            }
        }

        public BackGroundImage(BackgroundType background, int y)
        {
            this.texture = null;
            this.drawposition = new Vector2(0, y);
            this.color = Microsoft.Xna.Framework.Color.White;
            this.type = background;
            this.scale = 1.0f;
            this.counter = 0;
            this.rotation = 0;
            this.origin = new Vector2(0, 0);
            if (this.texture == null)
            {
                this.source = new Microsoft.Xna.Framework.Rectangle(0, 0, 0, 0);
            }
            else
            {
                this.source = new Microsoft.Xna.Framework.Rectangle(0, 0, texture.Bounds.Width, texture.Bounds.Height);
            }
        }

        public BackGroundImage(BackgroundType background, int y, float scale)
        {
            this.texture = null;
            this.drawposition = new Vector2(0, y);
            this.color = Microsoft.Xna.Framework.Color.White;
            this.type = background;
            this.scale = scale;
            this.counter = 0;
            this.rotation = 0;
            this.origin = new Vector2(0, 0);
            if (this.texture == null)
            {
                this.source = new Microsoft.Xna.Framework.Rectangle(0, 0, 0, 0);
            }
            else
            {
                this.source = new Microsoft.Xna.Framework.Rectangle(0, 0, texture.Bounds.Width, texture.Bounds.Height);
            }
        }

        public BackGroundImage(BackgroundType background, int x, int y)
        {
            this.texture = null;
            this.drawposition = new Vector2(x, y);
            this.color = Microsoft.Xna.Framework.Color.White;
            this.type = background;
            this.scale = 1.0f;
            this.counter = 0;
            this.rotation = 0;
            this.origin = new Vector2(0, 0);
            if (this.texture == null)
            {
                this.source = new Microsoft.Xna.Framework.Rectangle(0, 0, 0, 0);
            }
            else
            {
                this.source = new Microsoft.Xna.Framework.Rectangle(0, 0, texture.Bounds.Width, texture.Bounds.Height);
            }
        }

        public BackGroundImage(BackgroundType background, int x, int y, int counter)
        {
            this.texture = null;
            this.drawposition = new Vector2(x, y);
            this.color = Microsoft.Xna.Framework.Color.White;
            this.type = background;
            this.scale = 1.0f;
            this.counter = counter;
            this.rotation = 0;
            this.origin = new Vector2(0, 0);
            if (this.texture == null)
            {
                this.source = new Microsoft.Xna.Framework.Rectangle(0, 0, 0, 0);
            }
            else
            {
                this.source = new Microsoft.Xna.Framework.Rectangle(0, 0, texture.Bounds.Width, texture.Bounds.Height);
            }
        }

        public BackGroundImage(BackgroundType background, int x, int y, float scale)
        {
            this.texture = null;
            this.drawposition = new Vector2(x, y);
            this.color = Microsoft.Xna.Framework.Color.White;
            this.type = background;
            this.scale = scale;
            this.counter = 0;
            this.rotation = 0;
            this.origin = new Vector2(0, 0);
            if (this.texture == null)
            {
                this.source = new Microsoft.Xna.Framework.Rectangle(0, 0, 0, 0);
            }
            else
            {
                this.source = new Microsoft.Xna.Framework.Rectangle(0, 0, texture.Bounds.Width, texture.Bounds.Height);
            }
        }

        public BackGroundImage(BackgroundType background, int x, int y, Vector2 origin)
        {
            this.texture = null;
            this.drawposition = new Vector2(x, y);
            this.color = Microsoft.Xna.Framework.Color.White;
            this.type = background;
            this.scale = 1.0f;
            this.counter = 0;
            this.rotation = 0;
            this.origin = origin;
            if (this.texture == null)
            {
                this.source = new Microsoft.Xna.Framework.Rectangle(0, 0, 0, 0);
            }
            else
            {
                this.source = new Microsoft.Xna.Framework.Rectangle(0, 0, texture.Bounds.Width, texture.Bounds.Height);
            }
        }

        public BackGroundImage(BackgroundType background, int x, int y, Vector2 origin, float scale)
        {
            this.texture = null;
            this.drawposition = new Vector2(x, y);
            this.color = Microsoft.Xna.Framework.Color.White;
            this.type = background;
            this.scale = scale;
            this.counter = 0;
            this.rotation = 0;
            this.origin = origin;
            if (this.texture == null)
            {
                this.source = new Microsoft.Xna.Framework.Rectangle(0, 0, 0, 0);
            }
            else
            {
                this.source = new Microsoft.Xna.Framework.Rectangle(0, 0, texture.Bounds.Width, texture.Bounds.Height);
            }
        }

        public BackGroundImage(BackgroundType background, int x, int y, float scale, Microsoft.Xna.Framework.Rectangle source)
        {
            this.texture = null;
            this.drawposition = new Vector2(x, y);
            this.color = Microsoft.Xna.Framework.Color.White;
            this.type = background;
            this.scale = scale;
            this.counter = 0;
            this.rotation = 0;
            this.origin = new Vector2(0, 0);
            this.source = source;
        }

        public void UpdateSource()
        {
            if (this.texture == null)
            {
                this.source = new Microsoft.Xna.Framework.Rectangle(0, 0, 0, 0);
            }
            else
            {
                this.source = new Microsoft.Xna.Framework.Rectangle(0, 0, texture.Bounds.Width, texture.Bounds.Height);
            }
        }

        public void ShiftLeft(BackGroundImage image)
        {
            float x = image.drawposition.X;
            switch (image.type)
            {
                default: break;
                case BackgroundType.Foreground: x = x - 3; break;
                case BackgroundType.Midground: x = x - 2; break;
                case BackgroundType.Background: x = x - 1; break;
                case BackgroundType.Stationary: break;
                case BackgroundType.Smoke: x = x - 1; break;
                case BackgroundType.Smokebehind: x = x - 2; break;
                case BackgroundType.Body: x = x - 0.05f; break;
                case BackgroundType.Grass: x = x - 0.1f; break;
            }
            image.drawposition.X = x;
        }

        public void ShiftRight(BackGroundImage image)
        {
            float x = image.drawposition.X;
            switch (image.type)
            {
                default: break;
                case BackgroundType.Foreground: x = x + 3; break;
                case BackgroundType.Midground: x = x + 2; break;
                case BackgroundType.Background: x = x + 1; break;
                case BackgroundType.Stationary: break;
                case BackgroundType.Smoke: x = x + 1; break;
                case BackgroundType.Smokebehind: x = x + 2; break;
                case BackgroundType.Body: x = x + 0.05f; break;
                case BackgroundType.Grass: x = x + 0.1f; break;
            }
            image.drawposition.X = x;
        }

        public void ShiftUp(BackGroundImage image, float shift)
        {
            image.drawposition.Y = image.drawposition.Y - shift;          
        }

        public void ShiftDown(BackGroundImage image, float shift)
        {
            image.drawposition.Y = image.drawposition.Y + shift;
        }

        public void ScaleUp(BackGroundImage image, float scale)
        {
            image.scale = image.scale + scale;
        }

        public void ScaleDown(BackGroundImage image, float scale)
        {
            image.scale = image.scale - scale;
        }

        public void Rotate(BackGroundImage image, float rotation)
        {
            image.rotation = image.rotation + rotation;
        }

        public Texture2D Texture { get { return texture; } set { texture = value; } }
        public Vector2 Drawposition { get { return drawposition; } set { drawposition = value; } }
        public BackgroundType Type { get { return type; } set { type = value; } }
        public Microsoft.Xna.Framework.Color Color { get { return color; } set { color = value; } }
        public float Scale { get { return scale; } set { scale = value; } }
        public int Counter { get { return counter; } set { counter = value; } }
        public float Rotation { get { return rotation; } set { rotation = value; } }
        public Vector2 Origin { get { return origin; } set { origin = value; } }
        public Microsoft.Xna.Framework.Rectangle Source { get { return source; } set { source = value; } }
    }
}

