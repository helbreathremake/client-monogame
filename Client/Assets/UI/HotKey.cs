﻿using System;
using System.Xml;
using Helbreath.Common;
using Microsoft.Xna.Framework.Input;

namespace Client.Assets
{
    public class HotKey
    {
        private int index;
        private Keys press;
        private Keys hold;
        private HotKeyActions action;

        public static HotKey ParseXml(XmlReader r)
        {
            HotKey hotkey = new HotKey(Int32.Parse(r["Index"]));
            HotKeyActions action1st;
            if (Enum.TryParse<HotKeyActions>(r["Action"], out action1st))
                hotkey.Action = action1st;
            Keys press1st;
            if (r["Press"] != null && Enum.TryParse<Keys>(r["Press"], out press1st))
                hotkey.Press = press1st;
            Keys hold1st;
            if (r["Hold"] != null && Enum.TryParse<Keys>(r["Hold"], out hold1st))
                hotkey.Hold = hold1st;
            return hotkey;
        }

        public HotKey(int index)
        {
            this.index = index;
        }

        public HotKey(HotKeyActions action)
        {
            this.action = action;
        }

        public HotKey(Keys press, Keys hold, HotKeyActions action)
        {
            this.press = press;
            this.hold = hold;
            this.action = action;
        }

        public HotKey(Keys press, Keys hold, HotKeyActions action, int index)
        {
            this.press = press;
            this.hold = hold;
            this.action = action;
            this.index = index;
        }

        public int Index
        {
            get { return index; }
            set { index = value; }
        }

        public HotKeyActions Action
        {
            get { return action; }
            set { action = value; }
        }

        public Keys Press
        {
            get { return press; }
            set { press = value; }
        }

        public Keys Hold
        {
            get { return hold; }
            set { hold = value; }
        }
    }
}
