﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Reflection;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Helbreath.Common;

namespace Client.Assets.UI
{
    public class HotKeyMenuItem
    {
        private bool selectable;
        private HotKey hotkey;
        private GameDisplay display;
        private List<MenuItem> menuItems;
        private Dictionary<int, HotKey> hotKeyConfiguration;
        private Texture2D backgroundTexture;
        private bool delete = false;
        private bool press = false;
        private bool hold = false;
        private MenuItemState state = MenuItemState.Normal;
        private bool selected;
        private int menuScroll = 0;
        private bool dropMenuActive;
        private ConfigurationState configState = ConfigurationState.Normal;

        private List<MenuItem> hotKeyActionsList;

        public HotKeyMenuItem(HotKeyMenuItem item)
        {
            selectable = item.selectable;
            hotkey = item.hotkey;
            display = item.display;
            menuItems = item.menuItems;
            hotKeyConfiguration = item.hotKeyConfiguration;
            backgroundTexture = item.backgroundTexture;
            delete = item.delete;
            press = item.press;
            hold = item.hold;
            state = item.state;
            selected = item.selected;
            menuScroll = item.menuScroll;
            hotKeyActionsList = item.hotKeyActionsList;
        }

        public HotKeyMenuItem(GameDisplay display, HotKey hotkey, Dictionary<int,HotKey> hotKeyConfiguration, int startingPlace)
        {
            this.display = display;
            this.hotkey = hotkey;
            this.menuItems = new List<MenuItem>();
            this.hotKeyActionsList = new List<MenuItem>();
            this.hotKeyConfiguration = hotKeyConfiguration;
            selectable = true;
            this.Init(startingPlace);
        }

        public HotKeyMenuItem(GameDisplay display, string text, int startingPlace, MenuItemType itemType)
        {
            this.display = display;
            this.menuItems = new List<MenuItem>();

            Viewport viewport = display.Device.Viewport;
            Vector2 viewportSize = new Vector2(viewport.Width, viewport.Height);
            int width = viewport.Width;
            int height = viewport.Height;
            int mouseX = (int)display.Mouse.X;
            int mouseY = (int)display.Mouse.Y;

            backgroundTexture = new Texture2D(display.Device, 1, 1);
            backgroundTexture.SetData(new[] { Color.White * 0.3f });

            //Background
            MenuItem backGround = new MenuItem(MenuItemType.Image, "background");
            backGround.SetPosition(new Vector2((int)(viewport.Width / 2 - 123 - 19), (int)((viewport.Height / 2) - 29 + (startingPlace * 60))));
            backGround.Height = 44;
            backGround.Width = 282;
            backGround.IsScrollable = true;
            backGround.Texture = backgroundTexture;
            backGround.ItemState = MenuItemState.Normal;
            menuItems.Add(backGround);

            if (itemType == MenuItemType.CreateHotKey)
            {
                MenuItem itemLabel = new MenuItem(MenuItemType.SmallButton, text);
                itemLabel.SetPosition(new Vector2((int)((viewport.Width / 2) - (Cache.Fonts[FontType.MagicMedieval14].MeasureString(itemLabel.Text).X / 2)), (int)((viewport.Height / 2) - 15 + (startingPlace * 60))));
                itemLabel.IsScrollable = true;
                itemLabel.SettingType = SettingsType.CreateButton;
                itemLabel.ItemState = MenuItemState.Normal;
                selectable = true;
                menuItems.Add(itemLabel);
            }

            if (itemType == MenuItemType.Text)
            {
                selectable = true;

                //NORMAL STATE
                MenuItem itemLabel = new MenuItem(itemType, text);
                itemLabel.SetPosition(new Vector2((int)((viewport.Width / 2) - (Cache.Fonts[FontType.MagicMedieval14].MeasureString(itemLabel.Text).X / 2)), (int)((viewport.Height / 2) - 25 + (startingPlace * 60))));
                itemLabel.IsScrollable = true;
                itemLabel.ItemState = MenuItemState.Normal;
                //itemLabel.SettingType = SettingsType.Action;
                menuItems.Add(itemLabel);

                MenuItem resetLabel = new MenuItem(MenuItemType.Text, "Default Hotkey Settings:");
                resetLabel.SetPosition(new Vector2((int)((viewport.Width / 2) - 30 - (Cache.Fonts[FontType.MagicMedieval14].MeasureString(resetLabel.Text).X / 2)), (int)((viewport.Height / 2) - 6 + (startingPlace * 60))));
                resetLabel.IsScrollable = true;
                resetLabel.ItemState = MenuItemState.Normal;
                menuItems.Add(resetLabel);

                MenuItem resetLabelButton = new MenuItem(MenuItemType.SmallButton, "Restore");
                resetLabelButton.SetPosition(new Vector2((int)((viewport.Width / 2) + 70), (int)((viewport.Height / 2) - 6 + (startingPlace * 60))));
                resetLabelButton.IsScrollable = true;
                resetLabelButton.ItemState = MenuItemState.Normal;
                resetLabelButton.SettingType = SettingsType.Reset;
                menuItems.Add(resetLabelButton);

                //RESET STATE
                MenuItem resetBackGround = new MenuItem(MenuItemType.Image, "background");
                resetBackGround.SetPosition(new Vector2((int)(viewport.Width / 2 - 123 - 19), (int)((viewport.Height / 2) - 29 + (startingPlace * 60))));
                resetBackGround.Height = 44;
                resetBackGround.Width = 282;
                resetBackGround.IsScrollable = true;
                resetBackGround.Texture = backgroundTexture;
                resetBackGround.ItemState = MenuItemState.Reset;
                menuItems.Add(resetBackGround);

                MenuItem resetStateLabel = new MenuItem(itemType, "Restore Default Settings");
                resetStateLabel.SetPosition(new Vector2((int)((viewport.Width / 2) - (Cache.Fonts[FontType.MagicMedieval14].MeasureString(itemLabel.Text).X / 2)), (int)((viewport.Height / 2) - 25 + (startingPlace * 60))));
                resetStateLabel.IsScrollable = true;
                resetStateLabel.ItemState = MenuItemState.Reset;
                menuItems.Add(resetStateLabel);

                //Reset Yes Button
                MenuItem resetYesButton = new MenuItem(MenuItemType.SmallButton, "Yes");
                resetYesButton.SetPosition(new Vector2((int)(viewport.Width / 2 - Cache.Fonts[FontType.MagicMedieval14].MeasureString(resetYesButton.Text).X - 10), (int)((viewport.Height / 2) - 6) + (startingPlace * 60)));
                resetYesButton.SettingType = SettingsType.Reset;
                resetYesButton.IsScrollable = true;
                resetYesButton.ItemState = MenuItemState.Reset;
                menuItems.Add(resetYesButton);

                //Reset No Button
                MenuItem resetNoButton = new MenuItem(MenuItemType.SmallButton, "No");
                resetNoButton.SetPosition(new Vector2((int)(viewport.Width / 2 + 10), (int)((viewport.Height / 2) - 6) + (startingPlace * 60)));
                resetNoButton.SettingType = SettingsType.Reset;
                resetNoButton.IsScrollable = true;
                resetNoButton.ItemState = MenuItemState.Reset;
                menuItems.Add(resetNoButton);
            }

        }

        public void Init(int startingPlace)
        {
            Viewport viewport = display.Device.Viewport;
            Vector2 viewportSize = new Vector2(viewport.Width, viewport.Height);
            int width = viewport.Width;
            int height = viewport.Height;
            int mouseX = (int)display.Mouse.X;
            int mouseY = (int)display.Mouse.Y;

            backgroundTexture = new Texture2D(display.Device, 1, 1);
            backgroundTexture.SetData(new[] { Color.White * 0.3f });

            //Background
            MenuItem backGround = new MenuItem(MenuItemType.Image, "background");
            backGround.SetPosition(new Vector2((int)(viewport.Width / 2 - 123 - 19), (int)((viewport.Height / 2) - 29 + (startingPlace * 60))));
            backGround.Height = 44;
            backGround.Width = 282;
            backGround.IsScrollable = true;
            backGround.Texture = backgroundTexture;
            backGround.ItemState = MenuItemState.Normal;
            menuItems.Add(backGround);

            //Index Number
            string index = hotkey.Index.ToString();
            MenuItem indexNumber = new MenuItem(MenuItemType.Text, index);
            indexNumber.SetPosition(new Vector2((int)(viewport.Width / 2 - 123 - 6), (int)((viewport.Height / 2) - 29 + (startingPlace * 60))));
            indexNumber.IsScrollable = true;
            indexNumber.SettingType = SettingsType.Numbers;
            indexNumber.ItemState = MenuItemState.Normal;
            menuItems.Add(indexNumber);

            //Action Label
            MenuItem actionLabel = new MenuItem(MenuItemType.Text, "Action:");
            actionLabel.SetPosition(new Vector2((int)(viewport.Width / 2 - 123 + 14), (int)((viewport.Height / 2) - 25 + (startingPlace * 60))));
            actionLabel.IsScrollable = true;
            actionLabel.ItemState = MenuItemState.Normal;
            menuItems.Add(actionLabel);

            //HotkeyAction Name
            string hotkeyaction = hotkey.Action.ToString();
            MenuItem hotKeyAction = new MenuItem(MenuItemType.SmallButton, hotkeyaction);
            hotKeyAction.SetPosition(new Vector2((int)(viewport.Width / 2 - 123 + 80), (int)((viewport.Height / 2) - 25 + (startingPlace * 60))));
            hotKeyAction.IsScrollable = true;
            hotKeyAction.SettingType = SettingsType.Action;
            hotKeyAction.ItemState = MenuItemState.Normal;
            menuItems.Add(hotKeyAction);

            //Delete Button
            MenuItem deleteButton = new MenuItem(MenuItemType.SmallButton, "X");
            deleteButton.SetPosition(new Vector2((int)(viewport.Width / 2 - 123 + 245 + 4), (int)((viewport.Height / 2) - 22 + (startingPlace * 60))));
            deleteButton.SettingType = SettingsType.DeleteButton;
            deleteButton.IsScrollable = true;
            deleteButton.ItemState = MenuItemState.Normal;
            menuItems.Add(deleteButton);

            //Hold
            MenuItem holdLabel = new MenuItem(MenuItemType.Text, "Hold:");
            holdLabel.SetPosition(new Vector2((int)(viewport.Width / 2 - 123 - 6), (int)((viewport.Height / 2) - 5) + (startingPlace * 60)));
            holdLabel.IsScrollable = true;
            holdLabel.ItemState = MenuItemState.Normal;
            menuItems.Add(holdLabel);

            //HoldButton
            string hold = hotkey.Hold.ToString();
            MenuItem holdButton = new MenuItem(MenuItemType.SmallButton, hold);
            holdButton.SetPosition(new Vector2((int)(viewport.Width / 2 - 123 + 40), (int)((viewport.Height / 2) - 7) + (startingPlace * 60)));
            holdButton.IsScrollable = true;
            holdButton.SettingType = SettingsType.Hold;
            holdButton.ItemState = MenuItemState.Normal;
            menuItems.Add(holdButton);

            //Press
            MenuItem pressLabel = new MenuItem(MenuItemType.Text, "Press:");
            pressLabel.SetPosition(new Vector2((int)(viewport.Width / 2 - 123 + 120), (int)((viewport.Height / 2) - 5) + (startingPlace * 60)));
            pressLabel.IsScrollable = true;
            pressLabel.ItemState = MenuItemState.Normal;
            menuItems.Add(pressLabel);

            //PressButton
            string press = hotkey.Press.ToString();
            MenuItem pressButton = new MenuItem(MenuItemType.SmallButton, press);
            pressButton.SetPosition(new Vector2((int)(viewport.Width / 2 - 123 + 170), (int)((viewport.Height / 2) - 7) + (startingPlace * 60)));
            pressButton.IsScrollable = true;
            pressButton.SettingType = SettingsType.Press;
            pressButton.ItemState = MenuItemState.Normal;
            menuItems.Add(pressButton);

            //Delete Background
            MenuItem deletebackGround = new MenuItem(MenuItemType.Image, "background");
            deletebackGround.SetPosition(new Vector2((int)(viewport.Width / 2 - 123 - 18), (int)((viewport.Height / 2) - 29 + (startingPlace * 60))));
            deletebackGround.SettingType = SettingsType.Delete;
            deletebackGround.Height = 44;
            deletebackGround.Width = 280;
            deletebackGround.IsScrollable = true;
            deletebackGround.Texture = backgroundTexture;
            deletebackGround.ItemState = MenuItemState.Delete;
            menuItems.Add(deletebackGround);

            //Delete Label
            MenuItem deleteLabel = new MenuItem(MenuItemType.Text, "Do you want to delete this hotkey?");
            deleteLabel.SetPosition(new Vector2((int)(viewport.Width / 2 - 123 - 6), (int)((viewport.Height / 2) - 25 + (startingPlace * 60))));
            deleteLabel.SettingType = SettingsType.Delete;
            deleteLabel.IsScrollable = true;
            deleteLabel.ItemState = MenuItemState.Delete;
            menuItems.Add(deleteLabel);

            //Delete Yes Button
            MenuItem deleteYesButton = new MenuItem(MenuItemType.SmallButton, "Yes");
            deleteYesButton.SetPosition(new Vector2((int)(viewport.Width / 2 - Cache.Fonts[FontType.DamageSmallSize11].MeasureString(deleteYesButton.Text).X - 10), (int)((viewport.Height / 2) - 7) + (startingPlace * 60)));
            deleteYesButton.SettingType = SettingsType.Delete;
            deleteYesButton.IsScrollable = true;
            deleteYesButton.ItemState = MenuItemState.Delete;
            menuItems.Add(deleteYesButton);

            //Delete No Button
            MenuItem deleteNoButton = new MenuItem(MenuItemType.SmallButton, "No");
            deleteNoButton.SetPosition(new Vector2((int)(viewport.Width / 2 + 10), (int)((viewport.Height / 2) - 7) + (startingPlace * 60)));
            deleteNoButton.SettingType = SettingsType.Delete;
            deleteNoButton.IsScrollable = true;
            deleteNoButton.ItemState = MenuItemState.Delete;
            menuItems.Add(deleteNoButton);

            //PressBackground
            MenuItem pressbackGround = new MenuItem(MenuItemType.Image, "background");
            pressbackGround.SetPosition(new Vector2((int)(viewport.Width / 2 - 123 - 18), (int)((viewport.Height / 2) - 29 + (startingPlace * 60))));
            pressbackGround.SettingType = SettingsType.Press;
            pressbackGround.Height = 44;
            pressbackGround.Width = 280;
            pressbackGround.IsScrollable = true;
            pressbackGround.Texture = backgroundTexture;
            pressbackGround.ItemState = MenuItemState.Press;
            menuItems.Add(pressbackGround);

            //Press Label
            MenuItem pressbackgroundLabel = new MenuItem(MenuItemType.Title, "Change Press button");
            pressbackgroundLabel.SetPosition(new Vector2((int)(viewport.Width / 2 - 123 - 6), (int)((viewport.Height / 2) - 25 + (startingPlace * 60))));
            pressbackgroundLabel.SettingType = SettingsType.Press;
            pressbackgroundLabel.IsScrollable = true;
            pressbackgroundLabel.ItemState = MenuItemState.Press;
            menuItems.Add(pressbackgroundLabel);

            //Press Label2
            MenuItem pressbackgroundLabel2 = new MenuItem(MenuItemType.Title, "Set as:");
            pressbackgroundLabel2.SetPosition(new Vector2((int)(viewport.Width / 2 - 129), (int)((viewport.Height / 2) - 7 + (startingPlace * 60))));
            pressbackgroundLabel2.SettingType = SettingsType.Press;
            pressbackgroundLabel2.IsScrollable = true;
            pressbackgroundLabel2.ItemState = MenuItemState.Press;
            menuItems.Add(pressbackgroundLabel2);

            //Press Label3
            MenuItem pressbackgroundLabel3 = new MenuItem(MenuItemType.SmallButton, "None");
            pressbackgroundLabel3.SetPosition(new Vector2((int)(viewport.Width / 2 - 69), (int)((viewport.Height / 2) - 7 + (startingPlace * 60))));
            pressbackgroundLabel3.SettingType = SettingsType.Press;
            pressbackgroundLabel3.IsScrollable = true;
            pressbackgroundLabel3.ItemState = MenuItemState.Press;
            menuItems.Add(pressbackgroundLabel3);

            //Press Label4
            MenuItem pressbackgroundLabel4 = new MenuItem(MenuItemType.Title, "or");
            pressbackgroundLabel4.SetPosition(new Vector2((int)(viewport.Width / 2 - 20), (int)((viewport.Height / 2) - 7 + (startingPlace * 60))));
            pressbackgroundLabel4.SettingType = SettingsType.Press;
            pressbackgroundLabel4.IsScrollable = true;
            pressbackgroundLabel4.ItemState = MenuItemState.Press;
            menuItems.Add(pressbackgroundLabel4);

            //Press Label5
            MenuItem pressbackgroundLabel5 = new MenuItem(MenuItemType.Text, "press any Key");
            pressbackgroundLabel5.SetPosition(new Vector2((int)(viewport.Width / 2), (int)((viewport.Height / 2) - 7 + (startingPlace * 60))));
            pressbackgroundLabel5.SettingType = SettingsType.Press;
            pressbackgroundLabel5.IsScrollable = true;
            pressbackgroundLabel5.ItemState = MenuItemState.Press;
            menuItems.Add(pressbackgroundLabel5);

            //Press Cancel Button
            MenuItem pressCancelButton = new MenuItem(MenuItemType.SmallButton, "Cancel");
            pressCancelButton.SetPosition(new Vector2((int)(viewport.Width / 2 - 123 + 205), (int)((viewport.Height / 2) - 25 + (startingPlace * 60))));
            pressCancelButton.SettingType = SettingsType.DeleteButton;
            pressCancelButton.IsScrollable = true;
            pressCancelButton.ItemState = MenuItemState.Press;
            menuItems.Add(pressCancelButton);

            //HoldBackground
            MenuItem holdbackGround = new MenuItem(MenuItemType.Image, "background");
            holdbackGround.SetPosition(new Vector2((int)(viewport.Width / 2 - 123 - 18), (int)((viewport.Height / 2) - 29 + (startingPlace * 60))));
            holdbackGround.SettingType = SettingsType.Hold;
            holdbackGround.Height = 44;
            holdbackGround.Width = 280;
            holdbackGround.IsScrollable = true;
            holdbackGround.Texture = backgroundTexture;
            holdbackGround.ItemState = MenuItemState.Hold;
            menuItems.Add(holdbackGround);

            //Hold Label
            MenuItem holdbackgroundLabel = new MenuItem(MenuItemType.Title, "Change Hold button");
            holdbackgroundLabel.SetPosition(new Vector2((int)(viewport.Width / 2 - 123 - 6), (int)((viewport.Height / 2) - 25 + (startingPlace * 60))));
            holdbackgroundLabel.SettingType = SettingsType.Hold;
            holdbackgroundLabel.IsScrollable = true;
            holdbackgroundLabel.ItemState = MenuItemState.Hold;
            menuItems.Add(holdbackgroundLabel);

            //Hold Label2
            MenuItem holdbackgroundLabel2 = new MenuItem(MenuItemType.Title, "Set as:");
            holdbackgroundLabel2.SetPosition(new Vector2((int)(viewport.Width / 2 - 129), (int)((viewport.Height / 2) - 7 + (startingPlace * 60))));
            holdbackgroundLabel2.SettingType = SettingsType.Hold;
            holdbackgroundLabel2.IsScrollable = true;
            holdbackgroundLabel2.ItemState = MenuItemState.Hold;
            menuItems.Add(holdbackgroundLabel2);

            //Hold Label3
            MenuItem holdbackgroundLabel3 = new MenuItem(MenuItemType.SmallButton, "None");
            holdbackgroundLabel3.SetPosition(new Vector2((int)(viewport.Width / 2 - 69), (int)((viewport.Height / 2) - 7 + (startingPlace * 60))));
            holdbackgroundLabel3.SettingType = SettingsType.Hold;
            holdbackgroundLabel3.IsScrollable = true;
            holdbackgroundLabel3.ItemState = MenuItemState.Hold;
            menuItems.Add(holdbackgroundLabel3);

            //Hold Label 4
            MenuItem holdbackgroundLabel4 = new MenuItem(MenuItemType.Title, "or");
            holdbackgroundLabel4.SetPosition(new Vector2((int)(viewport.Width / 2 - 20), (int)((viewport.Height / 2) - 7 + (startingPlace * 60))));
            holdbackgroundLabel4.SettingType = SettingsType.Hold;
            holdbackgroundLabel4.IsScrollable = true;
            holdbackgroundLabel4.ItemState = MenuItemState.Hold;
            menuItems.Add(holdbackgroundLabel4);

            //Hold Label 5
            MenuItem holdbackgroundLabel5 = new MenuItem(MenuItemType.Text, "press any Key");
            holdbackgroundLabel5.SetPosition(new Vector2((int)(viewport.Width / 2), (int)((viewport.Height / 2) - 7 + (startingPlace * 60))));
            holdbackgroundLabel5.SettingType = SettingsType.Hold;
            holdbackgroundLabel5.IsScrollable = true;
            holdbackgroundLabel5.ItemState = MenuItemState.Hold;
            menuItems.Add(holdbackgroundLabel5);

            //Hold Cancel Button
            MenuItem holdCancelButton = new MenuItem(MenuItemType.SmallButton, "Cancel");
            holdCancelButton.SetPosition(new Vector2((int)(viewport.Width / 2 - 123 + 205), (int)((viewport.Height / 2) - 25 + (startingPlace * 60))));
            holdCancelButton.SettingType = SettingsType.DeleteButton;
            holdCancelButton.IsScrollable = true;
            holdCancelButton.ItemState = MenuItemState.Hold;
            menuItems.Add(holdCancelButton);

            //ActionBackground
            MenuItem actionbackGround = new MenuItem(MenuItemType.Image, "background");
            actionbackGround.SetPosition(new Vector2((int)(viewport.Width / 2 - 123 - 19), (int)((viewport.Height / 2) - 29 + (startingPlace * 60))));
            actionbackGround.Height = 44;
            actionbackGround.Width = 282;
            actionbackGround.IsScrollable = true;
            actionbackGround.Texture = backgroundTexture;
            actionbackGround.ItemState = MenuItemState.Action;
            menuItems.Add(actionbackGround);

            //Action Number
            MenuItem actionIndexNumber = new MenuItem(MenuItemType.Text, index);
            actionIndexNumber.SetPosition(new Vector2((int)(viewport.Width / 2 - 123 - 6), (int)((viewport.Height / 2) - 29 + (startingPlace * 60))));
            actionIndexNumber.IsScrollable = true;
            actionIndexNumber.SettingType = SettingsType.Numbers;
            actionIndexNumber.ItemState = MenuItemState.Action;
            menuItems.Add(actionIndexNumber);

            //Action Label
            MenuItem actionStateLabel = new MenuItem(MenuItemType.Text, "Action:");
            actionStateLabel.SetPosition(new Vector2((int)(viewport.Width / 2 - 123 + 14), (int)((viewport.Height / 2) - 25 + (startingPlace * 60))));
            actionStateLabel.IsScrollable = true;
            actionStateLabel.ItemState = MenuItemState.Action;
            menuItems.Add(actionStateLabel);

            //Action Name
            MenuItem hotKeyActionName = new MenuItem(MenuItemType.SmallButton, hotkeyaction);
            hotKeyActionName.SetPosition(new Vector2((int)(viewport.Width / 2 - 123 + 80), (int)((viewport.Height / 2) - 25 + (startingPlace * 60))));
            hotKeyActionName.IsScrollable = true;
            hotKeyActionName.SettingType = SettingsType.Action;
            hotKeyActionName.ItemState = MenuItemState.Action;
            menuItems.Add(hotKeyActionName);

            //Delete Button
            MenuItem actionCancelButton = new MenuItem(MenuItemType.SmallButton, "X");
            actionCancelButton.SetPosition(new Vector2((int)(viewport.Width / 2 - 123 + 245 + 4), (int)((viewport.Height / 2) - 22 + (startingPlace * 60))));
            actionCancelButton.SettingType = SettingsType.DeleteButton;
            actionCancelButton.IsScrollable = true;
            actionCancelButton.ItemState = MenuItemState.Action;
            menuItems.Add(actionCancelButton);

            //HotKey ScrollBar
            MenuItem scrollBarHotKeyAction = new MenuItem(MenuItemType.ScrollBar, "");
            scrollBarHotKeyAction.SetPosition(new Vector2((int)(viewport.Width / 2 + 114), (int)((viewport.Height / 2 - 26)) + (startingPlace * 60)));
            scrollBarHotKeyAction.Width = 2;
            scrollBarHotKeyAction.Height = 141;
            scrollBarHotKeyAction.ItemState = MenuItemState.Action;
            menuItems.Add(scrollBarHotKeyAction);

            //Drop down list of hotkeyactions
            string[] hotKeyActionsString = Enum.GetNames(typeof(HotKeyActions));
            foreach (string s in hotKeyActionsString)
            {
                MenuItem hotKeyItem = new MenuItem(MenuItemType.Text, s);
                hotKeyItem.SetPosition(new Vector2((int)(viewport.Width / 2 - 123 + 80), (int)((viewport.Height / 2) - 25 + (startingPlace * 60))));
                hotKeyItem.IsScrollable = true;
                hotKeyActionsList.Add(hotKeyItem);
            }
        }

        public ConfigurationState Update()
        {
            if (selectable)
            {
                MenuItem background = menuItems.Find(item => item.Type == MenuItemType.Image);

                if (background.Position.Y + background.ScrollPosition > (int)((display.Device.Viewport.Height / 2) - 37) && (background.Position.Y + background.ScrollPosition < (int)((display.Device.Viewport.Height / 2) + 120)))
                {
                    if (state == MenuItemState.Action)
                    {
                        if (((display.Mouse.Y > background.Position.Y + background.ScrollPosition - 3) && (display.Mouse.Y < (background.Position.Y + background.ScrollPosition + background.Height + 106))) && ((display.Mouse.X > background.Position.X + 94) && (display.Mouse.X < (background.Position.X + background.Width - 20))))
                        {
                            selected = true;
                        }
                        else if (((display.Mouse.Y > background.Position.Y + background.ScrollPosition) && (display.Mouse.Y < (background.Position.Y + background.ScrollPosition + background.Height))) && ((display.Mouse.X > background.Position.X) && (display.Mouse.X < (background.Position.X + background.Width))))
                        {
                            selected = true;
                        }
                        else selected = false;
                        if (selected) { dropMenuActive = true; }
                        if (!selected) { state = MenuItemState.Normal; dropMenuActive = false; }
                    }
                    else
                    {
                        if (((display.Mouse.Y > background.Position.Y + background.ScrollPosition) && (display.Mouse.Y < (background.Position.Y + background.ScrollPosition + background.Height))) && ((display.Mouse.X > background.Position.X) && (display.Mouse.X < (background.Position.X + background.Width))))
                        {
                            selected = true;
                        }
                        else selected = false;

                        if (!selected) { state = MenuItemState.Normal; }
                    }
                }
                else
                {
                    selected = false;
                    state = MenuItemState.Normal;
                }
            }
            else selected = false;
            return configState;
        }


        public void Draw(SpriteBatch spriteBatch, GameTime gameTime, float glowFrame)
        {
            foreach (MenuItem item in menuItems)
            {
                if (item.Position.Y + item.ScrollPosition > (int)((display.Device.Viewport.Height / 2) - 37) && (item.Position.Y + item.ScrollPosition < (int)((display.Device.Viewport.Height / 2) + 120)))
                {

                    switch (state)
                    {
                        default: break;
                        case MenuItemState.Reset:
                            {
                                if (item.ItemState == MenuItemState.Reset)
                                {
                                    if (item.Type == MenuItemType.Image)
                                    {
                                        spriteBatch.Draw(item.Texture, new Microsoft.Xna.Framework.Rectangle((int)(item.Position.X), (int)(item.Position.Y + item.ScrollPosition), item.Width, item.Height), Microsoft.Xna.Framework.Color.Black);
                                        if (selected)
                                        {
                                            spriteBatch.Draw(item.Texture, new Microsoft.Xna.Framework.Rectangle((int)(item.Position.X), (int)(item.Position.Y + item.ScrollPosition), item.Width, item.Height), Microsoft.Xna.Framework.Color.Black);
                                            spriteBatch.Draw(item.Texture, new Microsoft.Xna.Framework.Rectangle((int)(item.Position.X), (int)(item.Position.Y + item.ScrollPosition), item.Width, item.Height), Microsoft.Xna.Framework.Color.Black);
                                        }
                                    }
                                    if (item.Type == MenuItemType.SmallButton)
                                    {
                                        spriteBatch.DrawString(Cache.Fonts[FontType.MagicMedieval14], item.Text, new Vector2((int)(item.Position.X + 1), (int)(item.Position.Y + item.ScrollPosition + 1)), Color.Black);
                                        spriteBatch.DrawString(Cache.Fonts[FontType.MagicMedieval14], item.Text, new Vector2((int)(item.Position.X), (int)(item.Position.Y + item.ScrollPosition)), Color.DarkOrange);
                                        //HIGHLIGHT
                                        if (selected)
                                        {
                                            if ((display.Mouse.Y > item.Position.Y + item.ScrollPosition) && (display.Mouse.Y < (item.Position.Y + item.ScrollPosition + (int)(Cache.Fonts[FontType.MagicMedieval14].MeasureString(item.Text).Y))))
                                            {
                                                if ((display.Mouse.X > item.Position.X) && (display.Mouse.X < (item.Position.X + (int)(Cache.Fonts[FontType.MagicMedieval14].MeasureString(item.Text).X))))
                                                {
                                                    if (!item.Selected) { display.Mouse.State = GameMouseState.Pickup; }
                                                    spriteBatch.DrawString(Cache.Fonts[FontType.MagicMedieval14], item.Text, new Vector2((int)(item.Position.X) + 1, (int)(item.Position.Y + item.ScrollPosition) + 1), Color.Black * glowFrame);
                                                    spriteBatch.DrawString(Cache.Fonts[FontType.MagicMedieval14], item.Text, new Vector2((int)(item.Position.X), (int)(item.Position.Y + item.ScrollPosition)), Color.DarkRed * glowFrame);
                                                }
                                            }
                                        }
                                    }
                                    if (item.Type == MenuItemType.Text)
                                    {
                                        spriteBatch.DrawString(Cache.Fonts[FontType.MagicMedieval14], item.Text, new Vector2((int)(item.Position.X + 1), (int)(item.Position.Y + item.ScrollPosition + 1)), Color.Black);
                                        spriteBatch.DrawString(Cache.Fonts[FontType.MagicMedieval14], item.Text, new Vector2((int)(item.Position.X), (int)(item.Position.Y + item.ScrollPosition)), Color.White);
                                    }
                                }
                                break;
                            }
                        case MenuItemState.Normal:
                            {
                                if (item.ItemState == MenuItemState.Normal)
                                {
                                    if (item.Type == MenuItemType.Image)
                                    {
                                        spriteBatch.Draw(item.Texture, new Microsoft.Xna.Framework.Rectangle((int)(item.Position.X), (int)(item.Position.Y + item.ScrollPosition), item.Width, item.Height), Microsoft.Xna.Framework.Color.Black);
                                        if (selected)
                                        {
                                            spriteBatch.Draw(item.Texture, new Microsoft.Xna.Framework.Rectangle((int)(item.Position.X), (int)(item.Position.Y + item.ScrollPosition), item.Width, item.Height), Microsoft.Xna.Framework.Color.Black);
                                            spriteBatch.Draw(item.Texture, new Microsoft.Xna.Framework.Rectangle((int)(item.Position.X), (int)(item.Position.Y + item.ScrollPosition), item.Width, item.Height), Microsoft.Xna.Framework.Color.Black);
                                        }
                                    }
                                    if (item.Type == MenuItemType.SmallButton)
                                    {
                                        if (item.SettingType == SettingsType.DeleteButton)
                                        {
                                            spriteBatch.DrawString(Cache.Fonts[FontType.MagicMedieval14], item.Text, new Vector2((int)(item.Position.X + 1), (int)(item.Position.Y + item.ScrollPosition + 1)), Color.Black);
                                            spriteBatch.DrawString(Cache.Fonts[FontType.MagicMedieval14], item.Text, new Vector2((int)(item.Position.X), (int)(item.Position.Y + item.ScrollPosition)), Color.Red);
                                        }
                                        else if (item.SettingType == SettingsType.Action || item.SettingType == SettingsType.Reset)
                                        {
                                            spriteBatch.DrawString(Cache.Fonts[FontType.MagicMedieval14], item.Text, new Vector2((int)(item.Position.X + 1), (int)(item.Position.Y + item.ScrollPosition + 1)), Color.Black);
                                            spriteBatch.DrawString(Cache.Fonts[FontType.MagicMedieval14], item.Text, new Vector2((int)(item.Position.X), (int)(item.Position.Y + item.ScrollPosition)), Color.DarkOrange);
                                        }
                                        else
                                        {
                                            spriteBatch.DrawString(Cache.Fonts[FontType.DamageSmallSize11], item.Text, new Vector2((int)(item.Position.X + 1), (int)(item.Position.Y + item.ScrollPosition + 1)), Color.Black);
                                            spriteBatch.DrawString(Cache.Fonts[FontType.DamageSmallSize11], item.Text, new Vector2((int)(item.Position.X), (int)(item.Position.Y + item.ScrollPosition)), Color.DarkOrange);
                                        }

                                        //HIGHLIGHT
                                        if (selected)
                                        {
                                            if ((display.Mouse.Y > item.Position.Y + item.ScrollPosition) && (display.Mouse.Y < (item.Position.Y + item.ScrollPosition + (int)(Cache.Fonts[FontType.DamageSmallSize11].MeasureString(item.Text).Y))))
                                            {
                                                if ((display.Mouse.X > item.Position.X) && (display.Mouse.X < (item.Position.X + (int)(Cache.Fonts[FontType.DamageSmallSize11].MeasureString(item.Text).X))))
                                                {
                                                    if (!item.Selected) { display.Mouse.State = GameMouseState.Pickup; }
                                                    if (item.SettingType == SettingsType.DeleteButton)
                                                    {
                                                        spriteBatch.DrawString(Cache.Fonts[FontType.MagicMedieval14], item.Text, new Vector2((int)(item.Position.X + 1), (int)(item.Position.Y + item.ScrollPosition + 1)), Color.Black * glowFrame);
                                                        spriteBatch.DrawString(Cache.Fonts[FontType.MagicMedieval14], item.Text, new Vector2((int)(item.Position.X), (int)(item.Position.Y + item.ScrollPosition)), Color.DarkRed * glowFrame);
                                                    }
                                                    else if (item.SettingType == SettingsType.Action || item.SettingType == SettingsType.Reset)
                                                    {
                                                        spriteBatch.DrawString(Cache.Fonts[FontType.MagicMedieval14], item.Text, new Vector2((int)(item.Position.X + 1), (int)(item.Position.Y + item.ScrollPosition + 1)), Color.Black * glowFrame);
                                                        spriteBatch.DrawString(Cache.Fonts[FontType.MagicMedieval14], item.Text, new Vector2((int)(item.Position.X), (int)(item.Position.Y + item.ScrollPosition)), Color.DarkRed * glowFrame);
                                                    }
                                                    else
                                                    {
                                                        spriteBatch.DrawString(Cache.Fonts[FontType.DamageSmallSize11], item.Text, new Vector2((int)(item.Position.X) + 1, (int)(item.Position.Y + item.ScrollPosition) + 1), Color.Black * glowFrame);
                                                        spriteBatch.DrawString(Cache.Fonts[FontType.DamageSmallSize11], item.Text, new Vector2((int)(item.Position.X), (int)(item.Position.Y + item.ScrollPosition)), Color.DarkRed * glowFrame);
                                                    }

                                                }
                                            }
                                        }
                                    }
                                    if (item.Type == MenuItemType.Text)
                                    {
                                        if (item.SettingType == SettingsType.Numbers)
                                        {
                                            spriteBatch.DrawString(Cache.Fonts[FontType.DamageMediumSize13], item.Text, new Vector2((int)(item.Position.X + 1), (int)(item.Position.Y + item.ScrollPosition + 1)), Color.Black);
                                            spriteBatch.DrawString(Cache.Fonts[FontType.DamageMediumSize13], item.Text, new Vector2((int)(item.Position.X), (int)(item.Position.Y + item.ScrollPosition)), Color.White);
                                        }
                                        else if (item.SettingType == SettingsType.Action)
                                        {
                                            spriteBatch.DrawString(Cache.Fonts[FontType.MagicMedieval14], item.Text, new Vector2((int)(item.Position.X + 1), (int)(item.Position.Y + item.ScrollPosition + 1)), Color.Black);
                                            spriteBatch.DrawString(Cache.Fonts[FontType.MagicMedieval14], item.Text, new Vector2((int)(item.Position.X), (int)(item.Position.Y + item.ScrollPosition)), Color.DarkRed);
                                        }
                                        else
                                        {
                                            spriteBatch.DrawString(Cache.Fonts[FontType.MagicMedieval14], item.Text, new Vector2((int)(item.Position.X + 1), (int)(item.Position.Y + item.ScrollPosition + 1)), Color.Black);
                                            spriteBatch.DrawString(Cache.Fonts[FontType.MagicMedieval14], item.Text, new Vector2((int)(item.Position.X), (int)(item.Position.Y + item.ScrollPosition)), Color.White);
                                        }
                                    }
                                }
                                break;
                            }
                        case MenuItemState.Press:
                            {
                                if (item.ItemState == MenuItemState.Press)
                                {
                                    if (item.Type == MenuItemType.Image)
                                    {
                                        spriteBatch.Draw(item.Texture, new Microsoft.Xna.Framework.Rectangle((int)(item.Position.X), (int)(item.Position.Y + item.ScrollPosition), item.Width, item.Height), Microsoft.Xna.Framework.Color.Black);
                                        if (selected)
                                        {
                                            spriteBatch.Draw(item.Texture, new Rectangle((int)(item.Position.X), (int)(item.Position.Y + item.ScrollPosition), item.Width, item.Height), Microsoft.Xna.Framework.Color.Black);
                                            spriteBatch.Draw(item.Texture, new Rectangle((int)(item.Position.X), (int)(item.Position.Y + item.ScrollPosition), item.Width, item.Height), Microsoft.Xna.Framework.Color.Black);
                                        }
                                    }
                                    if (item.Type == MenuItemType.SmallButton)
                                    {
                                        spriteBatch.DrawString(Cache.Fonts[FontType.MagicMedieval14], item.Text, new Vector2((int)(item.Position.X + 1), (int)(item.Position.Y + item.ScrollPosition + 1)), Color.Black); 
                                        spriteBatch.DrawString(Cache.Fonts[FontType.MagicMedieval14], item.Text, new Vector2((int)(item.Position.X), (int)(item.Position.Y + item.ScrollPosition)), Color.Red);

                                        //HIGHLIGHT
                                        if (selected)
                                        {
                                            if ((display.Mouse.Y > item.Position.Y + item.ScrollPosition) && (display.Mouse.Y < (item.Position.Y + item.ScrollPosition + (int)(Cache.Fonts[FontType.DamageSmallSize11].MeasureString(item.Text).Y))))
                                            {
                                                if ((display.Mouse.X > item.Position.X) && (display.Mouse.X < (item.Position.X + (int)(Cache.Fonts[FontType.MagicMedieval14].MeasureString(item.Text).X))))
                                                {
                                                    if (!item.Selected) { display.Mouse.State = GameMouseState.Pickup; }
                                                    spriteBatch.DrawString(Cache.Fonts[FontType.MagicMedieval14], item.Text, new Vector2((int)(item.Position.X + 1), (int)(item.Position.Y + item.ScrollPosition + 1)), Color.Black * glowFrame);
                                                    spriteBatch.DrawString(Cache.Fonts[FontType.MagicMedieval14], item.Text, new Vector2((int)(item.Position.X), (int)(item.Position.Y + item.ScrollPosition)), Color.DarkRed * glowFrame);
                                                }
                                            }
                                        }
                                    }
                                    if (item.Type == MenuItemType.Title)
                                    {
                                        spriteBatch.DrawString(Cache.Fonts[FontType.MagicMedieval14], item.Text, new Vector2((int)(item.Position.X + 1), (int)(item.Position.Y + item.ScrollPosition + 1)), Color.Black);
                                        spriteBatch.DrawString(Cache.Fonts[FontType.MagicMedieval14], item.Text, new Vector2((int)(item.Position.X), (int)(item.Position.Y + item.ScrollPosition)), Color.White);
                                    }
                                    if (item.Type == MenuItemType.Text)
                                    {
                                        spriteBatch.DrawString(Cache.Fonts[FontType.MagicMedieval14], item.Text, new Vector2((int)(item.Position.X + 1), (int)(item.Position.Y + item.ScrollPosition + 1)), Color.Black);
                                        spriteBatch.DrawString(Cache.Fonts[FontType.MagicMedieval14], item.Text, new Vector2((int)(item.Position.X), (int)(item.Position.Y + item.ScrollPosition)), Color.DarkOrange * glowFrame);
                                    }
                                }
                                break;
                            }
                        case MenuItemState.Hold:
                            {
                                if (item.ItemState == MenuItemState.Hold)
                                {
                                    if (item.Type == MenuItemType.Image)
                                    {
                                        spriteBatch.Draw(item.Texture, new Microsoft.Xna.Framework.Rectangle((int)(item.Position.X), (int)(item.Position.Y + item.ScrollPosition), item.Width, item.Height), Microsoft.Xna.Framework.Color.Black);
                                        if (selected)
                                        {
                                            spriteBatch.Draw(item.Texture, new Microsoft.Xna.Framework.Rectangle((int)(item.Position.X), (int)(item.Position.Y + item.ScrollPosition), item.Width, item.Height), Microsoft.Xna.Framework.Color.Black);
                                            spriteBatch.Draw(item.Texture, new Microsoft.Xna.Framework.Rectangle((int)(item.Position.X), (int)(item.Position.Y + item.ScrollPosition), item.Width, item.Height), Microsoft.Xna.Framework.Color.Black);
                                        }
                                    }
                                    if (item.Type == MenuItemType.SmallButton)
                                    {
                                        spriteBatch.DrawString(Cache.Fonts[FontType.MagicMedieval14], item.Text, new Vector2((int)(item.Position.X + 1), (int)(item.Position.Y + item.ScrollPosition + 1)), Color.Black);
                                        spriteBatch.DrawString(Cache.Fonts[FontType.MagicMedieval14], item.Text, new Vector2((int)(item.Position.X), (int)(item.Position.Y + item.ScrollPosition)), Color.Red);

                                        //HIGHLIGHT
                                        if (selected)
                                        {
                                            if ((display.Mouse.Y > item.Position.Y + item.ScrollPosition) && (display.Mouse.Y < (item.Position.Y + item.ScrollPosition + (int)(Cache.Fonts[FontType.DamageSmallSize11].MeasureString(item.Text).Y))))
                                            {
                                                if ((display.Mouse.X > item.Position.X) && (display.Mouse.X < (item.Position.X + (int)(Cache.Fonts[FontType.DamageSmallSize11].MeasureString(item.Text).X))))
                                                {
                                                    if (!item.Selected) { display.Mouse.State = GameMouseState.Pickup; }
                                                    spriteBatch.DrawString(Cache.Fonts[FontType.MagicMedieval14], item.Text, new Vector2((int)(item.Position.X + 1), (int)(item.Position.Y + item.ScrollPosition + 1)), Color.Black * glowFrame);
                                                    spriteBatch.DrawString(Cache.Fonts[FontType.MagicMedieval14], item.Text, new Vector2((int)(item.Position.X), (int)(item.Position.Y + item.ScrollPosition)), Color.DarkRed * glowFrame);
                                                }
                                            }
                                        }
                                    }
                                    if (item.Type == MenuItemType.Title)
                                    {
                                        spriteBatch.DrawString(Cache.Fonts[FontType.MagicMedieval14], item.Text, new Vector2((int)(item.Position.X + 1), (int)(item.Position.Y + item.ScrollPosition + 1)), Color.Black);
                                        spriteBatch.DrawString(Cache.Fonts[FontType.MagicMedieval14], item.Text, new Vector2((int)(item.Position.X), (int)(item.Position.Y + item.ScrollPosition)), Color.White);
                                    }
                                    if (item.Type == MenuItemType.Text)
                                    {
                                        spriteBatch.DrawString(Cache.Fonts[FontType.DamageSmallSize11], item.Text, new Vector2((int)(item.Position.X + 1), (int)(item.Position.Y + item.ScrollPosition + 1)), Color.Black);
                                        spriteBatch.DrawString(Cache.Fonts[FontType.DamageSmallSize11], item.Text, new Vector2((int)(item.Position.X), (int)(item.Position.Y + item.ScrollPosition)), Color.DarkOrange * glowFrame);
                                    }
                                }
                                break;
                            }
                        case MenuItemState.Action:
                            {
                                if (item.ItemState == MenuItemState.Action)
                                {
                                    if (item.Type == MenuItemType.Image)
                                    {
                                        Texture2D texture = new Texture2D(display.Device, 1, 1);
                                        texture.SetData(new[] {Color.White});
                                        spriteBatch.Draw(texture, new Microsoft.Xna.Framework.Rectangle((int)(item.Position.X + 97), (int)(item.Position.Y + item.ScrollPosition), item.Width - 120, item.Height + 103), Microsoft.Xna.Framework.Color.Black);
                                        spriteBatch.Draw(item.Texture, new Microsoft.Xna.Framework.Rectangle((int)(item.Position.X), (int)(item.Position.Y + item.ScrollPosition), item.Width, item.Height), Microsoft.Xna.Framework.Color.Black);
                                        spriteBatch.Draw(item.Texture, new Microsoft.Xna.Framework.Rectangle((int)(item.Position.X), (int)(item.Position.Y + item.ScrollPosition), item.Width, item.Height), Microsoft.Xna.Framework.Color.Black);
                                        spriteBatch.Draw(item.Texture, new Microsoft.Xna.Framework.Rectangle((int)(item.Position.X), (int)(item.Position.Y + item.ScrollPosition), item.Width, item.Height), Microsoft.Xna.Framework.Color.Black);
                                    }

                                    //ADD hover over effect
                                    if (item.Type == MenuItemType.ScrollBar)
                                    {
                                        Texture2D texture = new Texture2D(display.Device, 1, 1);
                                        texture.SetData(new[] { Color.White });
                                        spriteBatch.Draw(texture, new Rectangle((int)(item.Position.X), (int)(item.Position.Y + item.ScrollPosition), item.Width, item.Height), Color.DarkOrange);
                                        SpriteHelper.DrawScrollBarMainMenu(spriteBatch, (int)(item.Position.X - 6), (int)(item.Position.Y + item.ScrollPosition), (int)((float)(126) * ((float)(menuScroll) / (float)(hotKeyActionsList.Count - 7))));

                                        if ((display.Mouse.Y > item.Position.Y - 4 + item.ScrollPosition) && (display.Mouse.Y < (item.Position.Y + item.Height + 4 + item.ScrollPosition)))
                                        {
                                            if ((display.Mouse.X > item.Position.X - 8) && (display.Mouse.X < (item.Position.X + item.Width + 8)))
                                            {
                                                display.Mouse.State = GameMouseState.Pickup;
                                                spriteBatch.Draw(texture, new Rectangle((int)(item.Position.X), (int)(item.Position.Y + item.ScrollPosition), item.Width, item.Height), Color.DarkRed * glowFrame);
                                                SpriteHelper.DrawScrollBarMainMenu(spriteBatch, (int)(item.Position.X - 6), (int)(item.Position.Y + item.ScrollPosition), (int)((float)(126) * ((float)(menuScroll) / (float)(hotKeyActionsList.Count - 7))), Color.DarkRed * glowFrame);
                                            }
                                        }
                                    }

                                    if (item.Type == MenuItemType.SmallButton)
                                    {
                                        if (item.SettingType == SettingsType.DeleteButton)
                                        {
                                            spriteBatch.DrawString(Cache.Fonts[FontType.MagicMedieval14], item.Text, new Vector2((int)(item.Position.X + 1), (int)(item.Position.Y + item.ScrollPosition + 1)), Color.Black);
                                            spriteBatch.DrawString(Cache.Fonts[FontType.MagicMedieval14], item.Text, new Vector2((int)(item.Position.X), (int)(item.Position.Y + item.ScrollPosition)), Color.Red);
                                        }
                                        else if (item.SettingType == SettingsType.Action)
                                        {

                                            HotKeyActions actionName = (HotKeyActions)Enum.Parse(typeof(HotKeyActions), item.Text);
                                            foreach (MenuItem hotkey in hotKeyActionsList)
                                            {
                                                HotKeyActions hotKeyActionName = (HotKeyActions)Enum.Parse(typeof(HotKeyActions), hotkey.Text);
                                                if ((menuScroll <= (int)hotKeyActionName) && ((int)hotKeyActionName < menuScroll + 7))
                                                {
                                                    if (item.Text == hotkey.Text)
                                                    {
                                                        spriteBatch.DrawString(Cache.Fonts[FontType.MagicMedieval14], hotkey.Text, new Vector2((int)(item.Position.X + 1), (int)(item.Position.Y + item.ScrollPosition + (((int)hotKeyActionName - menuScroll) * 20) + 1)), Color.Black);
                                                        spriteBatch.DrawString(Cache.Fonts[FontType.MagicMedieval14], hotkey.Text, new Vector2((int)(item.Position.X), (int)(item.Position.Y + item.ScrollPosition + (((int)hotKeyActionName - menuScroll) * 20))), Color.DarkGray);
                                                    }
                                                    else
                                                    {
                                                        spriteBatch.DrawString(Cache.Fonts[FontType.MagicMedieval14], hotkey.Text, new Vector2((int)(item.Position.X + 1), (int)(item.Position.Y + item.ScrollPosition + (((int)hotKeyActionName - menuScroll) * 20) + 1)), Color.Black);
                                                        spriteBatch.DrawString(Cache.Fonts[FontType.MagicMedieval14], hotkey.Text, new Vector2((int)(item.Position.X), (int)(item.Position.Y + item.ScrollPosition + (((int)hotKeyActionName - menuScroll) * 20))), Color.DarkOrange);
                                                    }
                                                    if ((display.Mouse.Y > item.Position.Y + item.ScrollPosition + (((int)hotKeyActionName - menuScroll) * 20)) && (display.Mouse.Y < (item.Position.Y + item.ScrollPosition + (((int)hotKeyActionName - menuScroll) * 20) + (int)(Cache.Fonts[FontType.MagicMedieval14].MeasureString(hotkey.Text).Y))))
                                                    {
                                                        if ((display.Mouse.X > item.Position.X) && (display.Mouse.X < (item.Position.X + (int)(Cache.Fonts[FontType.MagicMedieval14].MeasureString(hotkey.Text).X)))) //If only for mouse over text
                                                        //if ((display.Mouse.X > item.Position.X) && (display.Mouse.X < (item.Position.X + 150))) //if for mouse over any area on X axis of drop box 
                                                        {
                                                            if (item.Text != hotkey.Text)
                                                            {
                                                                display.Mouse.State = GameMouseState.Pickup;
                                                                spriteBatch.DrawString(Cache.Fonts[FontType.MagicMedieval14], hotkey.Text, new Vector2((int)(item.Position.X), (int)(item.Position.Y + item.ScrollPosition + (((int)hotKeyActionName - menuScroll) * 20))), Color.DarkRed * glowFrame);
                                                            }
                                                        }
                                                    }
                                                }
                                            }                                       
                                        }

                                        //HIGHLIGHT
                                        if (selected)
                                        {
                                            if ((display.Mouse.Y > item.Position.Y + item.ScrollPosition) && (display.Mouse.Y < (item.Position.Y + item.ScrollPosition + (int)(Cache.Fonts[FontType.MagicMedieval14].MeasureString(item.Text).Y))))
                                            {
                                                if ((display.Mouse.X > item.Position.X) && (display.Mouse.X < (item.Position.X + (int)(Cache.Fonts[FontType.MagicMedieval14].MeasureString(item.Text).X))))
                                                {                                          
                                                    if (item.SettingType == SettingsType.DeleteButton)
                                                    {
                                                        if (!item.Selected) { display.Mouse.State = GameMouseState.Pickup; }
                                                        spriteBatch.DrawString(Cache.Fonts[FontType.MagicMedieval14], item.Text, new Vector2((int)(item.Position.X + 1), (int)(item.Position.Y + item.ScrollPosition + 1)), Color.Black * glowFrame);
                                                        spriteBatch.DrawString(Cache.Fonts[FontType.MagicMedieval14], item.Text, new Vector2((int)(item.Position.X), (int)(item.Position.Y + item.ScrollPosition)), Color.DarkRed * glowFrame);
                                                    }

                                                }
                                            }
                                        }
                                    }
                                    if (item.Type == MenuItemType.Text)
                                    {
                                        if (item.SettingType == SettingsType.Numbers)
                                        {
                                            spriteBatch.DrawString(Cache.Fonts[FontType.DamageMediumSize13], item.Text, new Vector2((int)(item.Position.X + 1), (int)(item.Position.Y + item.ScrollPosition + 1)), Color.Black);
                                            spriteBatch.DrawString(Cache.Fonts[FontType.DamageMediumSize13], item.Text, new Vector2((int)(item.Position.X), (int)(item.Position.Y + item.ScrollPosition)), Color.White);
                                        }
                                        else
                                        {
                                            spriteBatch.DrawString(Cache.Fonts[FontType.MagicMedieval14], item.Text, new Vector2((int)(item.Position.X + 1), (int)(item.Position.Y + item.ScrollPosition + 1)), Color.Black);
                                            spriteBatch.DrawString(Cache.Fonts[FontType.MagicMedieval14], item.Text, new Vector2((int)(item.Position.X), (int)(item.Position.Y + item.ScrollPosition)), Color.White);
                                        }
                                    }
                                }
                                break;
                            }
                        case MenuItemState.Delete:
                            {
                                if (item.ItemState == MenuItemState.Delete)
                                {
                                    if (item.Type == MenuItemType.Image)
                                    {
                                        spriteBatch.Draw(item.Texture, new Rectangle((int)(item.Position.X), (int)(item.Position.Y + item.ScrollPosition), item.Width, item.Height), Microsoft.Xna.Framework.Color.Black);
                                        if (selected)
                                        {
                                            spriteBatch.Draw(item.Texture, new Rectangle((int)(item.Position.X), (int)(item.Position.Y + item.ScrollPosition), item.Width, item.Height), Microsoft.Xna.Framework.Color.Black);
                                            spriteBatch.Draw(item.Texture, new Rectangle((int)(item.Position.X), (int)(item.Position.Y + item.ScrollPosition), item.Width, item.Height), Microsoft.Xna.Framework.Color.Black);
                                        }
                                    }

                                    if (item.Type == MenuItemType.Text && item.SettingType == SettingsType.Delete)
                                    {
                                        spriteBatch.DrawString(Cache.Fonts[FontType.MagicMedieval14], item.Text, new Vector2((int)(item.Position.X + 1), (int)(item.Position.Y + item.ScrollPosition + 1)), Color.Black);
                                        spriteBatch.DrawString(Cache.Fonts[FontType.MagicMedieval14], item.Text, new Vector2((int)(item.Position.X), (int)(item.Position.Y + item.ScrollPosition)), Color.White);
                                    }

                                    if (item.Type == MenuItemType.SmallButton && item.SettingType == SettingsType.Delete)
                                    {
                                        spriteBatch.DrawString(Cache.Fonts[FontType.DamageSmallSize11], item.Text, new Vector2((int)(item.Position.X + 1), (int)(item.Position.Y + item.ScrollPosition + 1)), Color.Black);
                                        spriteBatch.DrawString(Cache.Fonts[FontType.DamageSmallSize11], item.Text, new Vector2((int)(item.Position.X), (int)(item.Position.Y + item.ScrollPosition)), Color.DarkOrange);

                                        //Highlight
                                        if (selected)
                                        {
                                            if ((display.Mouse.Y > item.Position.Y + item.ScrollPosition) && (display.Mouse.Y < (item.Position.Y + item.ScrollPosition + (int)(Cache.Fonts[FontType.DamageSmallSize11].MeasureString(item.Text).Y))))
                                            {
                                                if ((display.Mouse.X > item.Position.X) && (display.Mouse.X < (item.Position.X + (int)(Cache.Fonts[FontType.DamageSmallSize11].MeasureString(item.Text).X))))
                                                {
                                                    if (!item.Selected) { display.Mouse.State = GameMouseState.Pickup; }
                                                    spriteBatch.DrawString(Cache.Fonts[FontType.DamageSmallSize11], item.Text, new Vector2((int)(item.Position.X) + 1, (int)(item.Position.Y + item.ScrollPosition) + 1), Color.Black * glowFrame);
                                                    spriteBatch.DrawString(Cache.Fonts[FontType.DamageSmallSize11], item.Text, new Vector2((int)(item.Position.X), (int)(item.Position.Y + item.ScrollPosition)), Color.DarkRed * glowFrame);

                                                }
                                            }
                                        }
                                    }
                                }
                                break;
                            }
                    }
                }
            }
        }

        public bool HandleMouse(bool LeftClickCheck)
        {
            if (LeftClickCheck)
            {
                foreach (MenuItem item in menuItems)
                {
                    if (selected)
                    {
                        if (item.Position.Y + item.ScrollPosition > (int)((display.Device.Viewport.Height / 2) - 37) && (item.Position.Y + item.ScrollPosition < (int)((display.Device.Viewport.Height / 2) + 120)))
                        {
                            if (item.ItemState == state)
                            {
                                if (item.Type == MenuItemType.ScrollBar)
                                {
                                    if ((display.Mouse.Y > item.Position.Y - 4 + item.ScrollPosition) && (display.Mouse.Y < (item.Position.Y + item.Height + 4 + item.ScrollPosition)))
                                    {
                                        if ((display.Mouse.X > item.Position.X - 8) && (display.Mouse.X < (item.Position.X + item.Width + 8)))
                                        {
                                            if (display.Mouse.LeftClick)
                                            {
                                                menuScroll = (int)(MathHelper.Clamp((float)(display.Mouse.Y - (item.Position.Y + (float)(item.ScrollPosition))) / ((float)(141) / (float)(hotKeyActionsList.Count - 7)), 0, hotKeyActionsList.Count - 7)); 
                                                //display.Mouse.LeftClick = false;
                                            }
                                            else if (!display.Mouse.LeftClick && display.Mouse.IsDragging)
                                            {
                                                menuScroll = (int)(MathHelper.Clamp((float)(display.Mouse.Y - (item.Position.Y + (float)(item.ScrollPosition))) / ((float)141 / (float)(hotKeyActionsList.Count - 7)), 0, hotKeyActionsList.Count - 7));
                                            }
                                        }
                                    }
                                }

                                if (item.Type == MenuItemType.SmallButton)
                                {
                                    if ((display.Mouse.Y > item.Position.Y + item.ScrollPosition) && (display.Mouse.Y < (item.Position.Y + item.ScrollPosition + (int)(Cache.Fonts[FontType.MagicMedieval14].MeasureString(item.Text).Y))))
                                    {
                                        if ((display.Mouse.X > item.Position.X) && (display.Mouse.X < (item.Position.X + (int)(Cache.Fonts[FontType.MagicMedieval14].MeasureString(item.Text).X))))
                                        {
                                            switch (item.SettingType)
                                            {
                                                default: break;
                                                case SettingsType.Reset:
                                                    {
                                                        if (item.ItemState == MenuItemState.Reset)
                                                        {
                                                            switch (item.Text)
                                                            {
                                                                default: break;
                                                                case "No":
                                                                    {
                                                                        LeftClickCheck = false;
                                                                        state = MenuItemState.Normal;
                                                                        break;
                                                                    }
                                                                case "Yes":
                                                                    {
                                                                        string configPath = Path.GetDirectoryName(Assembly.GetExecutingAssembly().GetName().CodeBase).Replace(@"file:\", "") + @"\Configs\hotkey.xml";
                                                                        ConfigurationHelper.ResetHotKeys(configPath);
                                                                        LeftClickCheck = false;
                                                                        configState = ConfigurationState.Reload;
                                                                        break;
                                                                    }                                                                  
                                                            }
                                                            break;
                                                        }
                                                        if (item.ItemState == MenuItemState.Normal)
                                                        {
                                                            LeftClickCheck = false;
                                                            state = MenuItemState.Reset;
                                                        }
                                                        break;
                                                    }
                                                case SettingsType.Action:
                                                    {
                                                        if (!dropMenuActive)
                                                        {
                                                            LeftClickCheck = false;
                                                            state = MenuItemState.Action;
                                                            HotKeyActions actionName = (HotKeyActions)Enum.Parse(typeof(HotKeyActions), item.Text);
                                                            if ((int)actionName <= hotKeyActionsList.Count - 7)
                                                            {
                                                                menuScroll = (int)actionName;
                                                            }
                                                            else
                                                            {
                                                                menuScroll = hotKeyActionsList.Count - 7;
                                                            }
                                                            dropMenuActive = true;
                                                        }
                                                        break;
                                                    }
                                                case SettingsType.Press:
                                                    {
                                                        if (state == MenuItemState.Press)
                                                        {
                                                            LeftClickCheck = false;
                                                            hotkey.Press = Keys.None;
                                                            Cache.HotKeyConfiguration[hotkey.Index] = hotkey;
                                                            configState = ConfigurationState.Changed;
                                                            break;
                                                        }
                                                        else
                                                        {
                                                            LeftClickCheck = false;
                                                            state = MenuItemState.Press;
                                                            break;
                                                        }
                                                    }
                                                case SettingsType.Hold:
                                                    {
                                                        if (state == MenuItemState.Hold)
                                                        {
                                                            LeftClickCheck = false;
                                                            hotkey.Hold = Keys.None;
                                                            Cache.HotKeyConfiguration[hotkey.Index] = hotkey;
                                                            configState = ConfigurationState.Changed;
                                                            break;
                                                        }
                                                        else
                                                        {
                                                            LeftClickCheck = false;
                                                            state = MenuItemState.Hold;
                                                            break;
                                                        }
                                                    }
                                                case SettingsType.DeleteButton:
                                                    {
                                                        LeftClickCheck = false;
                                                        switch (state)
                                                        {
                                                            case MenuItemState.Delete:
                                                                {
                                                                    state = MenuItemState.Normal;
                                                                    break;
                                                                }
                                                            case MenuItemState.Normal:
                                                                {
                                                                    state = MenuItemState.Delete;
                                                                    break;
                                                                }
                                                            case MenuItemState.Press:
                                                                {
                                                                    state = MenuItemState.Normal;
                                                                    break;
                                                                }
                                                            case MenuItemState.Hold:
                                                                {
                                                                    state = MenuItemState.Normal;
                                                                    break;
                                                                }
                                                            case MenuItemState.Action:
                                                                {
                                                                    state = MenuItemState.Normal;
                                                                    break;
                                                                }
                                                        }
                                                        break;
                                                    }
                                                case SettingsType.CreateButton:
                                                    {
                                                        int newIndex;
                                                        if (Cache.HotKeyConfiguration.Count >= 1)
                                                        {
                                                            KeyValuePair<int, HotKey> lastKey = Cache.HotKeyConfiguration.ElementAt<KeyValuePair<int, HotKey>>(Cache.HotKeyConfiguration.Count - 1);
                                                            newIndex = lastKey.Value.Index + 1;
                                                        }
                                                        else
                                                        {
                                                            newIndex = 1;
                                                        }

                                                        HotKey newKey = new HotKey(Keys.None, Keys.None, HotKeyActions.None, newIndex);
                                                        Cache.HotKeyConfiguration.Add(newKey.Index, newKey);
                                                        LeftClickCheck = false;
                                                        configState = ConfigurationState.Changed;
                                                        break;
                                                    }
                                                case SettingsType.Delete:
                                                    {
                                                        if (state == MenuItemState.Delete)
                                                        {
                                                            switch (item.Text)
                                                            {
                                                                default: break;
                                                                case "No":
                                                                    {
                                                                        LeftClickCheck = false;
                                                                        state = MenuItemState.Normal;
                                                                        break;
                                                                    }
                                                                case "Yes":
                                                                    {
                                                                        LeftClickCheck = false;
                                                                        if (hotkey.Index != Cache.HotKeyConfiguration.Count)
                                                                        {
                                                                            if (hotkey.Index == Cache.HotKeyConfiguration.Count - 1)
                                                                            {
                                                                                Cache.HotKeyConfiguration.Remove(hotkey.Index);
                                                                                Cache.HotKeyConfiguration[hotkey.Index + 1].Index = hotkey.Index;
                                                                            }
                                                                            else
                                                                            {
                                                                                Cache.HotKeyConfiguration.Remove(hotkey.Index);
                                                                                for (int i = hotkey.Index + 1; i < Cache.HotKeyConfiguration.Count + 1; i++)
                                                                                {
                                                                                    HotKey changeIndex = Cache.HotKeyConfiguration[i];
                                                                                    changeIndex.Index = changeIndex.Index - 1;
                                                                                    Cache.HotKeyConfiguration[changeIndex.Index] = changeIndex;
                                                                                }
                                                                                Cache.HotKeyConfiguration.Remove(Cache.HotKeyConfiguration.Count - 1);
                                                                            }
                                                                        }
                                                                        else
                                                                        {
                                                                            Cache.HotKeyConfiguration.Remove(hotkey.Index);
                                                                        }

                                                                        configState = ConfigurationState.Changed;
                                                                        break;
                                                                    }
                                                            }
                                                        }
                                                        break;
                                                    }
                                            }
                                        }
                                    }

                                    if (item.SettingType == SettingsType.Action && item.ItemState == MenuItemState.Action && LeftClickCheck)
                                    {
                                        foreach (MenuItem hotKeyAction in hotKeyActionsList)
                                        {
                                            HotKeyActions hotKeyActionName = (HotKeyActions)Enum.Parse(typeof(HotKeyActions), hotKeyAction.Text);
                                            if ((menuScroll <= (int)hotKeyActionName) && ((int)hotKeyActionName < menuScroll + 7))
                                            {
                                                if ((display.Mouse.Y > item.Position.Y + item.ScrollPosition + (((int)hotKeyActionName - menuScroll) * 20)) && (display.Mouse.Y < (item.Position.Y + item.ScrollPosition + (((int)hotKeyActionName - menuScroll) * 20) + (int)(Cache.Fonts[FontType.MagicMedieval14].MeasureString(hotKeyAction.Text).Y))))
                                                {
                                                    if ((display.Mouse.X > item.Position.X) && (display.Mouse.X < (item.Position.X + (int)(Cache.Fonts[FontType.MagicMedieval14].MeasureString(hotKeyAction.Text).X))))
                                                    {
                                                        if (item.Text != hotKeyAction.Text)
                                                        {
                                                            LeftClickCheck = false;
                                                            hotkey.Action = hotKeyActionName;
                                                            Cache.HotKeyConfiguration[hotkey.Index] = hotkey;
                                                            configState = ConfigurationState.Changed;
                                                            break;
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                    if (LeftClickCheck == false) { break; }
                }
            }
            return LeftClickCheck;
        }


        public void HandleKeyboard()
        {
            switch (state)
            {
                case MenuItemState.Hold:
                    {
                        Keys[] pressedkeys = display.Keyboard.NewlyPressedKeys.ToArray();
                        if (pressedkeys.Length > 0)
                        {
                            bool keyComboExists = false;
                            HotKey testHotKey = new HotKey(hotkey.Press, hotkey.Hold, hotkey.Action);
                            testHotKey.Hold = pressedkeys[0];
                            foreach (KeyValuePair<int, HotKey> currentHotKey in Cache.HotKeyConfiguration)
                            {
                                if ((currentHotKey.Value.Press == testHotKey.Press && currentHotKey.Value.Hold == testHotKey.Hold) && (currentHotKey.Value.Press != Keys.None && currentHotKey.Value.Hold != Keys.None))
                                {
                                    keyComboExists = true;
                                    break;
                                }
                            }

                            if (!keyComboExists)
                            {
                                hotkey.Hold = pressedkeys[0];
                                Cache.HotKeyConfiguration[hotkey.Index] = hotkey;
                                configState = ConfigurationState.Changed;
                            }
                            else
                            {
                                state = MenuItemState.Normal;
                            }
                        }
                        break;
                    }
                case MenuItemState.Press:
                    {
                        Keys[] pressedkeys = display.Keyboard.NewlyPressedKeys.ToArray();
                        if (pressedkeys.Length > 0)
                        {
                            bool keyComboExists = false;
                            HotKey testHotKey = new HotKey(hotkey.Press, hotkey.Hold, hotkey.Action);
                            testHotKey.Press = pressedkeys[0];
                            foreach (KeyValuePair<int, HotKey> currentHotKey in Cache.HotKeyConfiguration)
                            {
                                if ((currentHotKey.Value.Press == testHotKey.Press && currentHotKey.Value.Hold == testHotKey.Hold) && (currentHotKey.Value.Press != Keys.None && currentHotKey.Value.Hold != Keys.None))
                                {
                                    keyComboExists = true;
                                    break;
                                }
                            }

                            if (!keyComboExists)
                            {
                                hotkey.Press = pressedkeys[0];
                                Cache.HotKeyConfiguration[hotkey.Index] = hotkey;
                                configState = ConfigurationState.Changed;
                            }
                            else
                            {
                                state = MenuItemState.Normal;
                            }
                        }
                        break;
                    }
                case MenuItemState.Action:
                case MenuItemState.Delete:
                case MenuItemState.Normal:
                    {
                        break;
                    }
            }
        }

        public void Scroll(int scrollDistance)
        {
            foreach (MenuItem scrollItem in menuItems)
            {
                scrollItem.ScrollPosition = -scrollDistance * 60;
            }
        }

        public void ScrollAction()
        {
            if (display.Mouse.Scroll == -1 && menuScroll <  hotKeyActionsList.Count - 7)
            {
                menuScroll = menuScroll + 1;
            }
            else if (display.Mouse.Scroll == 1 && menuScroll > 0)
            {
                menuScroll = menuScroll - 1;
            }
        }

        public HotKey HotKey { get { return hotkey; } set { hotkey = value; } }
        public bool Delete { get { return delete; } set { delete = value; } }
        public bool Press { get { return press; } set { press = value; } }
        public bool Hold { get { return hold; } set { hold = value; } }
        public bool Selected { get { return selected; } set { selected = value; } }
        public MenuItemState State { get { return state; } set { state = value; } }
    }
}
