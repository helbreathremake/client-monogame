﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

using Helbreath.Common;
using Client.Assets.State;
using Client.Assets.Game;

namespace Client.Assets.UI
{
    public class StatsDialogBox : IGameDialogBox
    {
        int selectedItemIndex = -1;
        int clickedItemIndex = -1;
        int highlightedItemIndex = -1;
        int selectedItemIndexOffsetX; //Dragging items
        int selectedItemIndexOffsetY; //Dragging items
              
        GameDialogBoxConfiguration config;

        //Base graphic info
        int sprite = (int)SpriteId.DialogsV2 + 2; //Main texture
        int spriteFrame = 17; //rectangle in texutre
        AnimationFrame frame;

        public int SelectedItemIndex { get { return selectedItemIndex; } set { selectedItemIndex = value; } }
        public int ClickedItemIndex { get { return clickedItemIndex; } set { clickedItemIndex = value; } }
        public int HighlightedItemIndex { get { return highlightedItemIndex; } set { highlightedItemIndex = value; } }
        public int SelectedItemIndexOffsetX { get { return selectedItemIndexOffsetX; } set { selectedItemIndexOffsetX = value; } }
        public int SelectedItemIndexOffsetY { get { return selectedItemIndexOffsetY; } set { selectedItemIndexOffsetY = value; } }
        public GameDialogBoxConfiguration Config { get { return config; } set { config = value; } }
        public GameDialogBoxType Type { get { return GameDialogBoxType.Stats; } }

        public StatsDialogBox()
        {    
            config = new GameDialogBoxConfiguration(Type, Cache.GameSettings.Resolution);
            frame = Cache.Interface[sprite].Frames[spriteFrame];
        }


        public StatsDialogBox(GameDialogBoxConfiguration config)
        {    
            this.config = config;
            frame = Cache.Interface[sprite].Frames[spriteFrame];
        }


        public bool Draw(SpriteBatch spriteBatch, GameTime gameTime)
        {       
            if (config.Hidden)
                if (!config.AlwaysVisible) return false; // dont draw dialogs if not a HUD item
                else if (Cache.GameSettings.LockedDialogs) return false;  // show HUD items when HUD is unlocked (transparent)

            if (((MainGame)Cache.DefaultState).Player.IsCasting && Cache.GameSettings.HideDialogDuringCasting) return false;

            Player player = ((MainGame)Cache.DefaultState).Player;
            int mouseX = (int)Cache.DefaultState.Display.Mouse.X;
            int mouseY = (int)Cache.DefaultState.Display.Mouse.Y;
            int X = config.X;
            int Y = config.Y;
            float transparency = ((Cache.GameSettings.TransparentDialogs || config.Hidden) ? 0.5F : 1F);  
            highlightedItemIndex = clickedItemIndex = selectedItemIndex = -1;
                   
            spriteBatch.Draw(Cache.Interface[sprite].Texture, new Vector2(X, Y), frame.GetRectangle(), Color.White * transparency);  //draw the base texture

            //draw extra 
            /* PAGINATION */
            spriteBatch.Draw(Cache.Interface[sprite].Texture, new Vector2(X + 215, Y + 459), Cache.Interface[sprite].Frames[9].GetRectangle(), Color.White * transparency);
            SpriteHelper.DrawTextRightWithShadow(spriteBatch, (config.Page) + "/" + (config.MaxPages), FontType.DialogsSmallerSize7, new Vector2(X + 189, Y + 458), 25, Cache.Colors[GameColor.Orange]);

            /* DATA ON EACH PAGE */
            int yLoc = Y;
            switch (config.Page)
            {
                default:
                case 1:
                    yLoc += 65;
                    #region General
                    SpriteHelper.DrawTextWithShadow(spriteBatch, "General", FontType.DialogsSmallSize8, new Vector2(X + 30, yLoc), Cache.Colors[GameColor.Orange]);
                    yLoc += 20;

                    // strength
                    SpriteHelper.DrawTextRightWithShadow(spriteBatch, player.HP + "/" + player.MaxHP, FontType.DialogsSmallSize8, new Vector2(X + 20, yLoc), 50, Color.White);
                    SpriteHelper.DrawTextWithShadow(spriteBatch, "Health Points (HP)", FontType.DialogsSmallSize8, new Vector2(X + 80, yLoc), Cache.Colors[GameColor.Orange]);
                    yLoc += 15;
                    // strength
                    SpriteHelper.DrawTextRightWithShadow(spriteBatch, player.MP + "/" + player.MaxMP, FontType.DialogsSmallSize8, new Vector2(X + 20, yLoc), 50, Color.White);
                    SpriteHelper.DrawTextWithShadow(spriteBatch, "Mana Points (MP)", FontType.DialogsSmallSize8, new Vector2(X + 80, yLoc), Cache.Colors[GameColor.Orange]);
                    yLoc += 15;
                    // strength
                    SpriteHelper.DrawTextRightWithShadow(spriteBatch, player.SP + "/" + player.MaxSP, FontType.DialogsSmallSize8, new Vector2(X + 20, yLoc), 50, Color.White);
                    SpriteHelper.DrawTextWithShadow(spriteBatch, "Stamina Points (SP)", FontType.DialogsSmallSize8, new Vector2(X + 80, yLoc), Cache.Colors[GameColor.Orange]);
                    yLoc += 15;
                    // strength
                    SpriteHelper.DrawTextRightWithShadow(spriteBatch, string.Format("{0} {1}", player.Strength, (player.Inventory.StrengthBonus > 0 ? "(+" + player.Inventory.StrengthBonus + ")" : "")).Trim(), FontType.DialogsSmallSize8, new Vector2(X + 20, yLoc), 50, Color.White);
                    SpriteHelper.DrawTextWithShadow(spriteBatch, "Strength", FontType.DialogsSmallSize8, new Vector2(X + 80, yLoc), Cache.Colors[GameColor.Orange]);
                    yLoc += 15;
                    // dexterity
                    SpriteHelper.DrawTextRightWithShadow(spriteBatch, string.Format("{0} {1}", player.Dexterity, (player.Inventory.DexterityBonus > 0 ? "(+" + player.Inventory.DexterityBonus + ")" : "")).Trim(), FontType.DialogsSmallSize8, new Vector2(X + 20, yLoc), 50, Color.White);
                    SpriteHelper.DrawTextWithShadow(spriteBatch, "Dexterity", FontType.DialogsSmallSize8, new Vector2(X + 80, yLoc), Cache.Colors[GameColor.Orange]);
                    yLoc += 15;
                    // vitality
                    SpriteHelper.DrawTextRightWithShadow(spriteBatch, string.Format("{0} {1}", player.Vitality, (player.Inventory.VitalityBonus > 0 ? "(+" + player.Inventory.VitalityBonus + ")" : "")).Trim(), FontType.DialogsSmallSize8, new Vector2(X + 20, yLoc), 50, Color.White);
                    SpriteHelper.DrawTextWithShadow(spriteBatch, "Vitality", FontType.DialogsSmallSize8, new Vector2(X + 80, yLoc), Cache.Colors[GameColor.Orange]);
                    yLoc += 15;
                    // int
                    SpriteHelper.DrawTextRightWithShadow(spriteBatch, string.Format("{0} {1}", player.Intelligence, (player.Inventory.IntelligenceBonus > 0 ? "(+" + player.Inventory.IntelligenceBonus + ")" : "")).Trim(), FontType.DialogsSmallSize8, new Vector2(X + 20, yLoc), 50, Color.White);
                    SpriteHelper.DrawTextWithShadow(spriteBatch, "Intelligence", FontType.DialogsSmallSize8, new Vector2(X + 80, yLoc), Cache.Colors[GameColor.Orange]);
                    yLoc += 15;
                    // magic
                    SpriteHelper.DrawTextRightWithShadow(spriteBatch, string.Format("{0} {1}", player.Magic, (player.Inventory.MagicBonus > 0 ? "(+" + player.Inventory.MagicBonus + ")" : "")).Trim(), FontType.DialogsSmallSize8, new Vector2(X + 20, yLoc), 50, Color.White);
                    SpriteHelper.DrawTextWithShadow(spriteBatch, "Magic", FontType.DialogsSmallSize8, new Vector2(X + 80, yLoc), Cache.Colors[GameColor.Orange]);
                    yLoc += 15;
                    // agility
                    SpriteHelper.DrawTextRightWithShadow(spriteBatch, string.Format("{0} {1}", player.Agility, (player.Inventory.AgilityBonus > 0 ? "(+" + player.Inventory.AgilityBonus + ")" : "")).Trim(), FontType.DialogsSmallSize8, new Vector2(X + 20, yLoc), 50, Color.White);
                    SpriteHelper.DrawTextWithShadow(spriteBatch, "Agility", FontType.DialogsSmallSize8, new Vector2(X + 80, yLoc), Cache.Colors[GameColor.Orange]);
                    #endregion
                    yLoc += 20;
                    #region Miscellaneous
                    SpriteHelper.DrawTextWithShadow(spriteBatch, "Miscellaneous", FontType.DialogsSmallSize8, new Vector2(X + 30, yLoc), Cache.Colors[GameColor.Orange]);
                    yLoc += 20;
                    // hunger
                    SpriteHelper.DrawTextRightWithShadow(spriteBatch, player.Hunger.ToString() + "/100", FontType.DialogsSmallSize8, new Vector2(X + 20, yLoc), 50, Color.White);
                    SpriteHelper.DrawTextWithShadow(spriteBatch, "Hunger", FontType.DialogsSmallSize8, new Vector2(X + 80, yLoc), Cache.Colors[GameColor.Orange]);
                    yLoc += 15;
                    // contribution
                    SpriteHelper.DrawTextRightWithShadow(spriteBatch, player.Contribution.ToString(), FontType.DialogsSmallSize8, new Vector2(X + 20, yLoc), 50, Color.White);
                    SpriteHelper.DrawTextWithShadow(spriteBatch, "Contribution", FontType.DialogsSmallSize8, new Vector2(X + 80, yLoc), Cache.Colors[GameColor.Orange]);
                    yLoc += 15;
                    // reputation
                    SpriteHelper.DrawTextRightWithShadow(spriteBatch, player.Reputation.ToString(), FontType.DialogsSmallSize8, new Vector2(X + 20, yLoc), 50, Color.White);
                    SpriteHelper.DrawTextWithShadow(spriteBatch, "Reputation", FontType.DialogsSmallSize8, new Vector2(X + 80, yLoc), Cache.Colors[GameColor.Orange]);
                    yLoc += 15;
                    // majestic points
                    SpriteHelper.DrawTextRightWithShadow(spriteBatch, player.Majestics.ToString(), FontType.DialogsSmallSize8, new Vector2(X + 20, yLoc), 50, Color.White);
                    SpriteHelper.DrawTextWithShadow(spriteBatch, "Majestic Points", FontType.DialogsSmallSize8, new Vector2(X + 80, yLoc), Cache.Colors[GameColor.Orange]);
                    yLoc += 15;
                    // eks
                    SpriteHelper.DrawTextRightWithShadow(spriteBatch, player.EnemyKills.ToString(), FontType.DialogsSmallSize8, new Vector2(X + 20, yLoc), 50, Color.White);
                    SpriteHelper.DrawTextWithShadow(spriteBatch, "Enemy Kills (EK)", FontType.DialogsSmallSize8, new Vector2(X + 80, yLoc), Cache.Colors[GameColor.Orange]);
                    yLoc += 15;
                    // pks
                    SpriteHelper.DrawTextRightWithShadow(spriteBatch, player.CriminalCount.ToString(), FontType.DialogsSmallSize8, new Vector2(X + 20, yLoc), 50, Color.White);
                    SpriteHelper.DrawTextWithShadow(spriteBatch, "Criminal Level", FontType.DialogsSmallSize8, new Vector2(X + 80, yLoc), Cache.Colors[GameColor.Orange]);
                    yLoc += 15;
                    //Luck
                    SpriteHelper.DrawTextRightWithShadow(spriteBatch, player.MaxLuck.ToString(), FontType.DialogsSmallSize8, new Vector2(X + 20, yLoc), 50, Color.White);
                    SpriteHelper.DrawTextWithShadow(spriteBatch, "Luck", FontType.DialogsSmallSize8, new Vector2(X + 80, yLoc), Cache.Colors[GameColor.Orange]);
                    yLoc += 15;
                    //InventoryV1 Count
                    SpriteHelper.DrawTextRightWithShadow(spriteBatch, player.Inventory.InventoryCount.ToString(), FontType.DialogsSmallSize8, new Vector2(X + 20, yLoc), 50, Color.White);
                    SpriteHelper.DrawTextWithShadow(spriteBatch, "Inventory Count", FontType.DialogsSmallSize8, new Vector2(X + 80, yLoc), Cache.Colors[GameColor.Orange]);
                    yLoc += 15;
                    //Equiped Item Count
                    SpriteHelper.DrawTextRightWithShadow(spriteBatch, player.Inventory.InventoryEquipedCount.ToString(), FontType.DialogsSmallSize8, new Vector2(X + 20, yLoc), 50, Color.White);
                    SpriteHelper.DrawTextWithShadow(spriteBatch, "Inventory Equiped Count", FontType.DialogsSmallSize8, new Vector2(X + 80, yLoc), Cache.Colors[GameColor.Orange]);
                    yLoc += 15;
                    //Equipment is Equiped count
                    SpriteHelper.DrawTextRightWithShadow(spriteBatch, player.Inventory.isEquipedCount.ToString(), FontType.DialogsSmallSize8, new Vector2(X + 20, yLoc), 50, Color.White);
                    SpriteHelper.DrawTextWithShadow(spriteBatch, "Equipment Is Equiped Count", FontType.DialogsSmallSize8, new Vector2(X + 80, yLoc), Cache.Colors[GameColor.Orange]);
                    yLoc += 15;
                    //Equiped Item Count
                    SpriteHelper.DrawTextRightWithShadow(spriteBatch, player.Inventory.EquipmentCount.ToString(), FontType.DialogsSmallSize8, new Vector2(X + 20, yLoc), 50, Color.White);
                    SpriteHelper.DrawTextWithShadow(spriteBatch, "Equiped Item Count", FontType.DialogsSmallSize8, new Vector2(X + 80, yLoc), Cache.Colors[GameColor.Orange]);
                    #endregion
                    break;
                case 2:
                    yLoc += 65;
                    #region Offence
                    SpriteHelper.DrawTextWithShadow(spriteBatch, "Offence", FontType.DialogsSmallSize8, new Vector2(X + 30, yLoc), Cache.Colors[GameColor.Orange]);
                    yLoc += 20;
                    // dps
                    if (!player.CriticalMode)
                    {
                        SpriteHelper.DrawTextRightWithShadow(spriteBatch, ((int)player.DPS).ToString(), FontType.DialogsSmallSize8, new Vector2(X + 20, yLoc), 50, Color.White);
                        SpriteHelper.DrawTextWithShadow(spriteBatch, "DPS", FontType.DialogsSmallSize8, new Vector2(X + 80, yLoc), Cache.Colors[GameColor.Orange]);
                    }
                    else // dps critical
                    {
                        if (player.Weapon != null)
                            switch (player.Weapon.Category)
                            {
                                case ItemCategory.Bows:
                                    SpriteHelper.DrawTextRightWithShadow(spriteBatch, ((int)(player.DPSCritical / 3)).ToString(), FontType.DialogsSmallSize8, new Vector2(X + 20, yLoc), 50, Color.White);
                                    SpriteHelper.DrawTextWithShadow(spriteBatch, "DPS Critical (x3)", FontType.DialogsSmallSize8, new Vector2(X + 80, yLoc), Cache.Colors[GameColor.Orange]);
                                    break;
                                default:
                                    SpriteHelper.DrawTextRightWithShadow(spriteBatch, ((int)player.DPSCritical).ToString(), FontType.DialogsSmallSize8, new Vector2(X + 20, yLoc), 50, Color.White);
                                    SpriteHelper.DrawTextWithShadow(spriteBatch, "DPS Critical", FontType.DialogsSmallSize8, new Vector2(X + 80, yLoc), Cache.Colors[GameColor.Orange]);
                                    break;
                            }
                        else
                        {
                            SpriteHelper.DrawTextRightWithShadow(spriteBatch, ((int)player.DPSCritical).ToString(), FontType.DialogsSmallSize8, new Vector2(X + 20, yLoc), 50, Color.White);
                            SpriteHelper.DrawTextWithShadow(spriteBatch, "DPS Critical", FontType.DialogsSmallSize8, new Vector2(X + 80, yLoc), Cache.Colors[GameColor.Orange]);
                        }

                    }
                    yLoc += 15;
                    // melee damage
                    if (!player.CriticalMode)
                    {

                        if (player.Weapon != null)
                            switch (player.Weapon.Category)
                            {
                                case ItemCategory.Bows: //bows do multiple hits when critical
                                    SpriteHelper.DrawTextRightWithShadow(spriteBatch, player.MinMeleeDamage + "~" + player.MaxMeleeDamage, FontType.DialogsSmallSize8, new Vector2(X + 20, yLoc), 50, Color.White);
                                    SpriteHelper.DrawTextWithShadow(spriteBatch, "Ranged Damage", FontType.DialogsSmallSize8, new Vector2(X + 80, yLoc), Cache.Colors[GameColor.Orange]);
                                    break;
                                default:
                                    SpriteHelper.DrawTextRightWithShadow(spriteBatch, player.MinMeleeDamage + "~" + player.MaxMeleeDamage, FontType.DialogsSmallSize8, new Vector2(X + 20, yLoc), 50, Color.White);
                                    SpriteHelper.DrawTextWithShadow(spriteBatch, "Melee Damage", FontType.DialogsSmallSize8, new Vector2(X + 80, yLoc), Cache.Colors[GameColor.Orange]);
                                    break;
                            }
                        else
                        {
                            SpriteHelper.DrawTextRightWithShadow(spriteBatch, player.MinMeleeDamage + "~" + player.MaxMeleeDamage, FontType.DialogsSmallSize8, new Vector2(X + 20, yLoc), 50, Color.White);
                            SpriteHelper.DrawTextWithShadow(spriteBatch, "Fist Damage", FontType.DialogsSmallSize8, new Vector2(X + 80, yLoc), Cache.Colors[GameColor.Orange]);
                        }
                    }
                    else // melee damage critical
                    {
                        if (player.Weapon != null)
                            switch (player.Weapon.Category)
                            {
                                case ItemCategory.Bows: //bows do multiple hits when critical
                                    SpriteHelper.DrawTextRightWithShadow(spriteBatch, player.MinMeleeDamageCritical / 3 + "~" + player.MaxMeleeDamageCritical / 3, FontType.DialogsSmallSize8, new Vector2(X + 20, yLoc), 50, Color.White);
                                    SpriteHelper.DrawTextWithShadow(spriteBatch, "Ranged Damage Critical (x3)", FontType.DialogsSmallSize8, new Vector2(X + 80, yLoc), Cache.Colors[GameColor.Orange]);
                                    break;
                                default:
                                    SpriteHelper.DrawTextRightWithShadow(spriteBatch, player.MinMeleeDamageCritical + "~" + player.MaxMeleeDamageCritical, FontType.DialogsSmallSize8, new Vector2(X + 20, yLoc), 50, Color.White);
                                    SpriteHelper.DrawTextWithShadow(spriteBatch, "Melee Damage Critical", FontType.DialogsSmallSize8, new Vector2(X + 80, yLoc), Cache.Colors[GameColor.Orange]);
                                    break;
                            }
                        else
                        {
                            SpriteHelper.DrawTextRightWithShadow(spriteBatch, player.MinMeleeDamageCritical + "~" + player.MaxMeleeDamageCritical, FontType.DialogsSmallSize8, new Vector2(X + 20, yLoc), 50, Color.White);
                            SpriteHelper.DrawTextWithShadow(spriteBatch, "Fist Damage Critical", FontType.DialogsSmallSize8, new Vector2(X + 80, yLoc), Cache.Colors[GameColor.Orange]);
                        }

                    }
                    yLoc += 15;
                    //Hitting Probability //TODO split Hitting prob into SpellBook/Physical + add hitting prob from stats to them
                    SpriteHelper.DrawTextRightWithShadow(spriteBatch, player.Inventory.HitChanceBonus + "%", FontType.DialogsSmallSize8, new Vector2(X + 20, yLoc), 50, Color.White);
                    SpriteHelper.DrawTextWithShadow(spriteBatch, "Bonus Hit Probability", FontType.DialogsSmallSize8, new Vector2(X + 80, yLoc), Cache.Colors[GameColor.Orange]);
                    yLoc += 15;
                    //CP
                    SpriteHelper.DrawTextRightWithShadow(spriteBatch, player.Inventory.CastProbabilityBonus + "%", FontType.DialogsSmallSize8, new Vector2(X + 20, yLoc), 50, Color.White);
                    SpriteHelper.DrawTextWithShadow(spriteBatch, "Bonus Casting Probability", FontType.DialogsSmallSize8, new Vector2(X + 80, yLoc), Cache.Colors[GameColor.Orange]);
                    yLoc += 15;
                    //MS //TODO show when global max is reached!
                    SpriteHelper.DrawTextRightWithShadow(spriteBatch, player.Inventory.ManaSaveBonus + "%", FontType.DialogsSmallSize8, new Vector2(X + 20, yLoc), 50, Color.White);
                    SpriteHelper.DrawTextWithShadow(spriteBatch, "Mana Save", FontType.DialogsSmallSize8, new Vector2(X + 80, yLoc), Cache.Colors[GameColor.Orange]);
                    yLoc += 15;
                    //Bonus melee
                    SpriteHelper.DrawTextRightWithShadow(spriteBatch, player.Inventory.PhysicalDamageBonus.ToString(), FontType.DialogsSmallSize8, new Vector2(X + 20, yLoc), 50, Color.White);
                    SpriteHelper.DrawTextWithShadow(spriteBatch, "Melee Damage Bonus", FontType.DialogsSmallSize8, new Vector2(X + 80, yLoc), Cache.Colors[GameColor.Orange]);
                    yLoc += 15;
                    //Bonus magic
                    SpriteHelper.DrawTextRightWithShadow(spriteBatch, player.Inventory.MagicDamageBonus.ToString(), FontType.DialogsSmallSize8, new Vector2(X + 20, yLoc), 50, Color.White);
                    SpriteHelper.DrawTextWithShadow(spriteBatch, "Magic Damage Bonus", FontType.DialogsSmallSize8, new Vector2(X + 80, yLoc), Cache.Colors[GameColor.Orange]);
                    yLoc += 15;
                    //Vampiric
                    SpriteHelper.DrawTextRightWithShadow(spriteBatch, player.Inventory.VampiricBonus + "%", FontType.DialogsSmallSize8, new Vector2(X + 20, yLoc), 50, Color.White);
                    SpriteHelper.DrawTextWithShadow(spriteBatch, "HP Steal", FontType.DialogsSmallSize8, new Vector2(X + 80, yLoc), Cache.Colors[GameColor.Orange]);
                    yLoc += 15;
                    //Zealous
                    SpriteHelper.DrawTextRightWithShadow(spriteBatch, player.Inventory.ZealousBonus + "%", FontType.DialogsSmallSize8, new Vector2(X + 20, yLoc), 50, Color.White);
                    SpriteHelper.DrawTextWithShadow(spriteBatch, "MP Steal", FontType.DialogsSmallSize8, new Vector2(X + 80, yLoc), Cache.Colors[GameColor.Orange]);
                    yLoc += 15;
                    //Piercing
                    SpriteHelper.DrawTextRightWithShadow(spriteBatch, player.Inventory.PiercingBonus + "%", FontType.DialogsSmallSize8, new Vector2(X + 20, yLoc), 50, Color.White);
                    SpriteHelper.DrawTextWithShadow(spriteBatch, "Piercing", FontType.DialogsSmallSize8, new Vector2(X + 80, yLoc), Cache.Colors[GameColor.Orange]);
                    yLoc += 15;
                    //Max Critical Attacks
                    SpriteHelper.DrawTextRightWithShadow(spriteBatch, player.Inventory.MaxCritsBonus.ToString(), FontType.DialogsSmallSize8, new Vector2(X + 20, yLoc), 50, Color.White);
                    SpriteHelper.DrawTextWithShadow(spriteBatch, "Bonus Critical Attacks", FontType.DialogsSmallSize8, new Vector2(X + 80, yLoc), Cache.Colors[GameColor.Orange]);
                    yLoc += 15;
                    //Critical Damage Bonus
                    SpriteHelper.DrawTextRightWithShadow(spriteBatch, player.Inventory.CriticalDamageBonus.ToString(), FontType.DialogsSmallSize8, new Vector2(X + 20, yLoc), 50, Color.White);
                    SpriteHelper.DrawTextWithShadow(spriteBatch, "Bonus Critical Damage", FontType.DialogsSmallSize8, new Vector2(X + 80, yLoc), Cache.Colors[GameColor.Orange]);
                    yLoc += 15;
                    //Critical Increase Chance
                    SpriteHelper.DrawTextRightWithShadow(spriteBatch, player.Inventory.CriticalIncreaseChanceBonus + "%", FontType.DialogsSmallSize8, new Vector2(X + 20, yLoc), 50, Color.White);
                    SpriteHelper.DrawTextWithShadow(spriteBatch, "Bonus Critical Increase", FontType.DialogsSmallSize8, new Vector2(X + 80, yLoc), Cache.Colors[GameColor.Orange]);
                    yLoc += 15;
                    //Poisonous Damage
                    SpriteHelper.DrawTextRightWithShadow(spriteBatch, player.Inventory.PoisonousBonus.ToString(), FontType.DialogsSmallSize8, new Vector2(X + 20, yLoc), 50, Color.White);
                    SpriteHelper.DrawTextWithShadow(spriteBatch, "Bonus Poison Damage", FontType.DialogsSmallSize8, new Vector2(X + 80, yLoc), Cache.Colors[GameColor.Orange]);
                    yLoc += 15;
                    //Combo Damage
                    SpriteHelper.DrawTextRightWithShadow(spriteBatch, player.Inventory.ComboDamageBonus.ToString(), FontType.DialogsSmallSize8, new Vector2(X + 20, yLoc), 50, Color.White);
                    SpriteHelper.DrawTextWithShadow(spriteBatch, "Bonus Combo Damage", FontType.DialogsSmallSize8, new Vector2(X + 80, yLoc), Cache.Colors[GameColor.Orange]);
                    yLoc += 15;
                    //Exhaustive (SP Reduction)
                    SpriteHelper.DrawTextRightWithShadow(spriteBatch, player.Inventory.ExhaustiveBonus.ToString(), FontType.DialogsSmallSize8, new Vector2(X + 20, yLoc), 50, Color.White);
                    SpriteHelper.DrawTextWithShadow(spriteBatch, "Bonus Stamina Drain Damage", FontType.DialogsSmallSize8, new Vector2(X + 80, yLoc), Cache.Colors[GameColor.Orange]);
                    yLoc += 15;
                    // Sharp
                    if (player.Inventory.SharpDamageBonus)
                    {
                        SpriteHelper.DrawTextRightWithShadow(spriteBatch, "Yes", FontType.DialogsSmallSize8, new Vector2(X + 20, yLoc), 50, Color.White);
                    }
                    else
                    {
                        SpriteHelper.DrawTextRightWithShadow(spriteBatch, "No", FontType.DialogsSmallSize8, new Vector2(X + 20, yLoc), 50, Color.White);
                    }
                    SpriteHelper.DrawTextWithShadow(spriteBatch, "Sharp (+1 Dice Damage)", FontType.DialogsSmallSize8, new Vector2(X + 80, yLoc), Cache.Colors[GameColor.Orange]);
                    yLoc += 15;
                    // Ancient
                    if (player.Inventory.AncientDamageBonus)
                    {
                        SpriteHelper.DrawTextRightWithShadow(spriteBatch, "Yes", FontType.DialogsSmallSize8, new Vector2(X + 20, yLoc), 50, Color.White);
                    }
                    else
                    {
                        SpriteHelper.DrawTextRightWithShadow(spriteBatch, "No", FontType.DialogsSmallSize8, new Vector2(X + 20, yLoc), 50, Color.White);
                    }
                    SpriteHelper.DrawTextWithShadow(spriteBatch, "Ancient (+2 Dice Damage)", FontType.DialogsSmallSize8, new Vector2(X + 80, yLoc), Cache.Colors[GameColor.Orange]);
                    yLoc += 15;
                    // Legendary
                    if (player.Inventory.LegendaryBonus)
                    {
                        SpriteHelper.DrawTextRightWithShadow(spriteBatch, "Yes", FontType.DialogsSmallSize8, new Vector2(X + 20, yLoc), 50, Color.White);
                    }
                    else
                    {
                        SpriteHelper.DrawTextRightWithShadow(spriteBatch, "No", FontType.DialogsSmallSize8, new Vector2(X + 20, yLoc), 50, Color.White);
                    }
                    SpriteHelper.DrawTextWithShadow(spriteBatch, "Legendary (+3 Dice Damage)", FontType.DialogsSmallSize8, new Vector2(X + 80, yLoc), Cache.Colors[GameColor.Orange]);
                    yLoc += 15;
                    //Righteous
                    if (player.Inventory.RighteousBonus)
                    {
                        SpriteHelper.DrawTextRightWithShadow(spriteBatch, "Yes", FontType.DialogsSmallSize8, new Vector2(X + 20, yLoc), 50, Color.White);
                    }
                    else
                    {
                        SpriteHelper.DrawTextRightWithShadow(spriteBatch, "No", FontType.DialogsSmallSize8, new Vector2(X + 20, yLoc), 50, Color.White);
                    }
                    SpriteHelper.DrawTextWithShadow(spriteBatch, "Righteous Bonus Damage", FontType.DialogsSmallSize8, new Vector2(X + 80, yLoc), Cache.Colors[GameColor.Orange]);
                    yLoc += 15;
                    //Kloness
                    if (player.Inventory.Kloness)
                    {
                        SpriteHelper.DrawTextRightWithShadow(spriteBatch, "Yes", FontType.DialogsSmallSize8, new Vector2(X + 20, yLoc), 50, Color.White);
                    }
                    else
                    {
                        SpriteHelper.DrawTextRightWithShadow(spriteBatch, "No", FontType.DialogsSmallSize8, new Vector2(X + 20, yLoc), 50, Color.White);
                    }
                    SpriteHelper.DrawTextWithShadow(spriteBatch, "Kloness Bonus Damage", FontType.DialogsSmallSize8, new Vector2(X + 80, yLoc), Cache.Colors[GameColor.Orange]);
                    yLoc += 15;
                    //Agile
                    if (player.Inventory.AgileBonus != 0)
                    {
                        SpriteHelper.DrawTextRightWithShadow(spriteBatch, "Yes", FontType.DialogsSmallSize8, new Vector2(X + 20, yLoc), 50, Color.White);
                    }
                    else
                    {
                        SpriteHelper.DrawTextRightWithShadow(spriteBatch, "No", FontType.DialogsSmallSize8, new Vector2(X + 20, yLoc), 50, Color.White);
                    }
                    SpriteHelper.DrawTextWithShadow(spriteBatch, "Agile Weapon Swing", FontType.DialogsSmallSize8, new Vector2(X + 80, yLoc), Cache.Colors[GameColor.Orange]);
                    yLoc += 15;
                    #endregion
                    break;
                case 3:
                    yLoc += 65;
                    #region Defence
                    SpriteHelper.DrawTextWithShadow(spriteBatch, "Defence", FontType.DialogsSmallSize8, new Vector2(X + 30, yLoc), Cache.Colors[GameColor.Orange]);
                    yLoc += 20;
                    //DR -//TODO - Add bonus from dex ect! 
                    SpriteHelper.DrawTextRightWithShadow(spriteBatch, player.Inventory.PhysicalResistance.ToString(), FontType.DialogsSmallSize8, new Vector2(X + 20, yLoc), 50, Color.White);
                    SpriteHelper.DrawTextWithShadow(spriteBatch, "Physical Resistance (DR)", FontType.DialogsSmallSize8, new Vector2(X + 80, yLoc), Cache.Colors[GameColor.Orange]);
                    yLoc += 15;

                    //head PA - Includes General PA (Rings/Gem ect.)
                    SpriteHelper.DrawTextRightWithShadow(spriteBatch, player.Inventory.TotalPhysicalAbsorptionGeneral + player.Inventory.TotalPhysicalAbsorptionHead + "%", FontType.DialogsSmallSize8, new Vector2(X + 20, yLoc), 50, Color.White);
                    SpriteHelper.DrawTextWithShadow(spriteBatch, "Physical Absorption (Head)", FontType.DialogsSmallSize8, new Vector2(X + 80, yLoc), Cache.Colors[GameColor.Orange]);
                    yLoc += 15;

                    //arms PA- Includes General PA (Rings/Gem ect.)
                    SpriteHelper.DrawTextRightWithShadow(spriteBatch, player.Inventory.TotalPhysicalAbsorptionGeneral + player.Inventory.TotalPhysicalAbsorptionArms + "%", FontType.DialogsSmallSize8, new Vector2(X + 20, yLoc), 50, Color.White);
                    SpriteHelper.DrawTextWithShadow(spriteBatch, "Physical Absorption (Arms)", FontType.DialogsSmallSize8, new Vector2(X + 80, yLoc), Cache.Colors[GameColor.Orange]);
                    yLoc += 15;

                    //body PA- Includes General PA (Rings/Gem ect.)
                    SpriteHelper.DrawTextRightWithShadow(spriteBatch, player.Inventory.TotalPhysicalAbsorptionGeneral + player.Inventory.TotalPhysicalAbsorptionBody + "%", FontType.DialogsSmallSize8, new Vector2(X + 20, yLoc), 50, Color.White);
                    SpriteHelper.DrawTextWithShadow(spriteBatch, "Physical Absorption (Body)", FontType.DialogsSmallSize8, new Vector2(X + 80, yLoc), Cache.Colors[GameColor.Orange]);
                    yLoc += 15;

                    //legs PA- Includes General PA (Rings/Gem ect.)
                    SpriteHelper.DrawTextRightWithShadow(spriteBatch, player.Inventory.TotalPhysicalAbsorptionGeneral + player.Inventory.TotalPhysicalAbsorptionLegs + "%", FontType.DialogsSmallSize8, new Vector2(X + 20, yLoc), 50, Color.White);
                    SpriteHelper.DrawTextWithShadow(spriteBatch, "Physical Absorption (Legs)", FontType.DialogsSmallSize8, new Vector2(X + 80, yLoc), Cache.Colors[GameColor.Orange]);
                    yLoc += 15;

                    // shield PA
                    SpriteHelper.DrawTextRightWithShadow(spriteBatch, player.Inventory.TotalPhysicalAbsorptionShield + "%", FontType.DialogsSmallSize8, new Vector2(X + 20, yLoc), 50, Color.White);
                    SpriteHelper.DrawTextWithShadow(spriteBatch, "Physical Absorption (Shield)", FontType.DialogsSmallSize8, new Vector2(X + 80, yLoc), Cache.Colors[GameColor.Orange]);
                    yLoc += 15;

                    //MR //TODO - Add mr from mag stat and possible base mr from items
                    SpriteHelper.DrawTextRightWithShadow(spriteBatch, player.Inventory.MagicResistanceBonus.ToString(), FontType.DialogsSmallSize8, new Vector2(X + 20, yLoc), 50, Color.White);
                    SpriteHelper.DrawTextWithShadow(spriteBatch, "Magic Resistance (MR)", FontType.DialogsSmallSize8, new Vector2(X + 80, yLoc), Cache.Colors[GameColor.Orange]);
                    yLoc += 15;
                    //earth MA
                    SpriteHelper.DrawTextRightWithShadow(spriteBatch, player.AbsorptionValue(MagicAttribute.Earth), FontType.DialogsSmallSize8, new Vector2(X + 20, yLoc), 50, Color.White);
                    SpriteHelper.DrawTextWithShadow(spriteBatch, "Earth Absorption", FontType.DialogsSmallSize8, new Vector2(X + 80, yLoc), Cache.Colors[GameColor.Orange]);
                    yLoc += 15;
                    // air MA
                    SpriteHelper.DrawTextRightWithShadow(spriteBatch, player.AbsorptionValue(MagicAttribute.Air), FontType.DialogsSmallSize8, new Vector2(X + 20, yLoc), 50, Color.White);
                    SpriteHelper.DrawTextWithShadow(spriteBatch, "Air Absorption", FontType.DialogsSmallSize8, new Vector2(X + 80, yLoc), Cache.Colors[GameColor.Orange]);
                    yLoc += 15;
                    // fire MA
                    SpriteHelper.DrawTextRightWithShadow(spriteBatch, player.AbsorptionValue(MagicAttribute.Fire), FontType.DialogsSmallSize8, new Vector2(X + 20, yLoc), 50, Color.White);
                    SpriteHelper.DrawTextWithShadow(spriteBatch, "Fire Absorption", FontType.DialogsSmallSize8, new Vector2(X + 80, yLoc), Cache.Colors[GameColor.Orange]);
                    yLoc += 15;
                    // water MA                             
                    SpriteHelper.DrawTextRightWithShadow(spriteBatch, player.AbsorptionValue(MagicAttribute.Water), FontType.DialogsSmallSize8, new Vector2(X + 20, yLoc), 50, Color.White);
                    SpriteHelper.DrawTextWithShadow(spriteBatch, "Water Absorption", FontType.DialogsSmallSize8, new Vector2(X + 80, yLoc), Cache.Colors[GameColor.Orange]);
                    yLoc += 15;
                    // unholy MA                             
                    SpriteHelper.DrawTextRightWithShadow(spriteBatch, player.AbsorptionValue(MagicAttribute.Unholy), FontType.DialogsSmallSize8, new Vector2(X + 20, yLoc), 50, Color.White);
                    SpriteHelper.DrawTextWithShadow(spriteBatch, "Unholy Absorption", FontType.DialogsSmallSize8, new Vector2(X + 80, yLoc), Cache.Colors[GameColor.Orange]);
                    yLoc += 15;
                    // spirit MA                             
                    SpriteHelper.DrawTextRightWithShadow(spriteBatch, player.AbsorptionValue(MagicAttribute.Spirit), FontType.DialogsSmallSize8, new Vector2(X + 20, yLoc), 50, Color.White);
                    SpriteHelper.DrawTextWithShadow(spriteBatch, "Spirit Absorption", FontType.DialogsSmallSize8, new Vector2(X + 80, yLoc), Cache.Colors[GameColor.Orange]);
                    yLoc += 15;
                    // kinesis MA                             
                    SpriteHelper.DrawTextRightWithShadow(spriteBatch, player.AbsorptionValue(MagicAttribute.Kinesis), FontType.DialogsSmallSize8, new Vector2(X + 20, yLoc), 50, Color.White);
                    SpriteHelper.DrawTextWithShadow(spriteBatch, "Kinesis Absorption", FontType.DialogsSmallSize8, new Vector2(X + 80, yLoc), Cache.Colors[GameColor.Orange]);
                    yLoc += 15;
                    //// MA bonus - Not needed, added non-elemental MA to all other ma values
                    //SpriteHelper.DrawTextRightWithShadow(spriteBatch, player.AbsorptionValue(MagicAttribute.None), FontType.DialogsV2Small, new Vector2(x + 20, yLoc), 50, Color.White);
                    //SpriteHelper.DrawTextWithShadow(spriteBatch, "SpellBook Absorption Bonus (MA)", FontType.DialogsV2Small, new Vector2(x + 80, yLoc), Cache.GameState.Display.FontColours[FontType.DialogsV2Orange]);
                    //yLoc += 15;
                    // PR bonus
                    SpriteHelper.DrawTextRightWithShadow(spriteBatch, player.Inventory.PoisonResistanceBonus + "%", FontType.DialogsSmallSize8, new Vector2(X + 20, yLoc), 50, Color.White);
                    SpriteHelper.DrawTextWithShadow(spriteBatch, "Poison Evade Chance (PR)", FontType.DialogsSmallSize8, new Vector2(X + 80, yLoc), Cache.Colors[GameColor.Orange]);
                    yLoc += 15;
                    // HP recovery bonus
                    SpriteHelper.DrawTextRightWithShadow(spriteBatch, player.Inventory.HPRecoveryBonus + "%", FontType.DialogsSmallSize8, new Vector2(X + 20, yLoc), 50, Color.White);
                    SpriteHelper.DrawTextWithShadow(spriteBatch, "Bonus HP Recovery", FontType.DialogsSmallSize8, new Vector2(X + 80, yLoc), Cache.Colors[GameColor.Orange]);
                    yLoc += 15;
                    // MP recovery bonus
                    SpriteHelper.DrawTextRightWithShadow(spriteBatch, player.Inventory.MPRecoveryBonus + "%", FontType.DialogsSmallSize8, new Vector2(X + 20, yLoc), 50, Color.White);
                    SpriteHelper.DrawTextWithShadow(spriteBatch, "Bonus MP Recovery", FontType.DialogsSmallSize8, new Vector2(X + 80, yLoc), Cache.Colors[GameColor.Orange]);
                    yLoc += 15;
                    // SP recovery bonus
                    SpriteHelper.DrawTextRightWithShadow(spriteBatch, player.Inventory.SPRecoveryBonus + "%", FontType.DialogsSmallSize8, new Vector2(X + 20, yLoc), 50, Color.White);
                    SpriteHelper.DrawTextWithShadow(spriteBatch, "Bonus SP Recovery", FontType.DialogsSmallSize8, new Vector2(X + 80, yLoc), Cache.Colors[GameColor.Orange]);
                    yLoc += 15;
                    // Mana Convert bonus
                    SpriteHelper.DrawTextRightWithShadow(spriteBatch, player.Inventory.ManaConvertingBonus + "%", FontType.DialogsSmallSize8, new Vector2(X + 20, yLoc), 50, Color.White);
                    SpriteHelper.DrawTextWithShadow(spriteBatch, "Convert Damage to Mana", FontType.DialogsSmallSize8, new Vector2(X + 80, yLoc), Cache.Colors[GameColor.Orange]);
                    yLoc += 15;
                    // Max HP bonus
                    SpriteHelper.DrawTextRightWithShadow(spriteBatch, player.Inventory.MaxHPBonus + "%", FontType.DialogsSmallSize8, new Vector2(X + 20, yLoc), 50, Color.White);
                    SpriteHelper.DrawTextWithShadow(spriteBatch, "Bonus Max HP", FontType.DialogsSmallSize8, new Vector2(X + 80, yLoc), Cache.Colors[GameColor.Orange]);
                    yLoc += 15;
                    // Max MP bonus
                    SpriteHelper.DrawTextRightWithShadow(spriteBatch, player.Inventory.MaxMPBonus + "%", FontType.DialogsSmallSize8, new Vector2(X + 20, yLoc), 50, Color.White);
                    SpriteHelper.DrawTextWithShadow(spriteBatch, "Bonus Max MP", FontType.DialogsSmallSize8, new Vector2(X + 80, yLoc), Cache.Colors[GameColor.Orange]);
                    yLoc += 15;
                    // Max SP bonus
                    SpriteHelper.DrawTextRightWithShadow(spriteBatch, player.Inventory.MaxSPBonus + "%", FontType.DialogsSmallSize8, new Vector2(X + 20, yLoc), 50, Color.White);
                    SpriteHelper.DrawTextWithShadow(spriteBatch, "Bonus Max SP", FontType.DialogsSmallSize8, new Vector2(X + 80, yLoc), Cache.Colors[GameColor.Orange]);
                    yLoc += 15;
                    #endregion
                    break;
                case 4:
                    yLoc += 65;
                    #region Bonuses
                    SpriteHelper.DrawTextWithShadow(spriteBatch, "Bonuses", FontType.DialogsSmallSize8, new Vector2(X + 30, yLoc), Cache.Colors[GameColor.Orange]);
                    yLoc += 20;
                    //Gold
                    SpriteHelper.DrawTextRightWithShadow(spriteBatch, player.Inventory.GoldBonus + "%", FontType.DialogsSmallSize8, new Vector2(X + 20, yLoc), 50, Color.White);
                    SpriteHelper.DrawTextWithShadow(spriteBatch, "Gold Drop Bonus", FontType.DialogsSmallSize8, new Vector2(X + 80, yLoc), Cache.Colors[GameColor.Orange]);
                    yLoc += 15;
                    //Experience
                    SpriteHelper.DrawTextRightWithShadow(spriteBatch, player.Inventory.ExperienceBonus + "%", FontType.DialogsSmallSize8, new Vector2(X + 20, yLoc), 50, Color.White);
                    SpriteHelper.DrawTextWithShadow(spriteBatch, "Experience Bonus", FontType.DialogsSmallSize8, new Vector2(X + 80, yLoc), Cache.Colors[GameColor.Orange]);

                    yLoc += 15;
                    //Blood
                    if (player.Inventory.Blood)
                    {
                        SpriteHelper.DrawTextRightWithShadow(spriteBatch, "Yes", FontType.DialogsSmallSize8, new Vector2(X + 20, yLoc), 50, Color.White);
                    }
                    else
                    {
                        SpriteHelper.DrawTextRightWithShadow(spriteBatch, "No", FontType.DialogsSmallSize8, new Vector2(X + 20, yLoc), 50, Color.White);
                    }
                    SpriteHelper.DrawTextWithShadow(spriteBatch, "Blood", FontType.DialogsSmallSize8, new Vector2(X + 80, yLoc), Cache.Colors[GameColor.Orange]);

                    yLoc += 15;
                    //Berserk
                    if (player.Inventory.Berserk)
                    {
                        SpriteHelper.DrawTextRightWithShadow(spriteBatch, "Yes", FontType.DialogsSmallSize8, new Vector2(X + 20, yLoc), 50, Color.White);
                    }
                    else
                    {
                        SpriteHelper.DrawTextRightWithShadow(spriteBatch, "No", FontType.DialogsSmallSize8, new Vector2(X + 20, yLoc), 50, Color.White);
                    }
                    SpriteHelper.DrawTextWithShadow(spriteBatch, "Berserk", FontType.DialogsSmallSize8, new Vector2(X + 80, yLoc), Cache.Colors[GameColor.Orange]);

                    yLoc += 15;
                    //Dark
                    if (player.Inventory.Dark)
                    {
                        SpriteHelper.DrawTextRightWithShadow(spriteBatch, "Yes", FontType.DialogsSmallSize8, new Vector2(X + 20, yLoc), 50, Color.White);
                    }
                    else
                    {
                        SpriteHelper.DrawTextRightWithShadow(spriteBatch, "No", FontType.DialogsSmallSize8, new Vector2(X + 20, yLoc), 50, Color.White);
                    }
                    SpriteHelper.DrawTextWithShadow(spriteBatch, "Dark", FontType.DialogsSmallSize8, new Vector2(X + 80, yLoc), Cache.Colors[GameColor.Orange]);

                    yLoc += 15;
                    //Light
                    if (player.Inventory.Light)
                    {
                        SpriteHelper.DrawTextRightWithShadow(spriteBatch, "Yes", FontType.DialogsSmallSize8, new Vector2(X + 20, yLoc), 50, Color.White);
                    }
                    else
                    {
                        SpriteHelper.DrawTextRightWithShadow(spriteBatch, "No", FontType.DialogsSmallSize8, new Vector2(X + 20, yLoc), 50, Color.White);
                    }
                    SpriteHelper.DrawTextWithShadow(spriteBatch, "Light", FontType.DialogsSmallSize8, new Vector2(X + 80, yLoc), Cache.Colors[GameColor.Orange]);

                    yLoc += 15;
                    //DemonSlayer
                    if (player.Inventory.DemonSlayer)
                    {
                        SpriteHelper.DrawTextRightWithShadow(spriteBatch, "Yes", FontType.DialogsSmallSize8, new Vector2(X + 20, yLoc), 50, Color.White);
                    }
                    else
                    {
                        SpriteHelper.DrawTextRightWithShadow(spriteBatch, "No", FontType.DialogsSmallSize8, new Vector2(X + 20, yLoc), 50, Color.White);
                    }
                    SpriteHelper.DrawTextWithShadow(spriteBatch, "Demon Slayer", FontType.DialogsSmallSize8, new Vector2(X + 80, yLoc), Cache.Colors[GameColor.Orange]);

                    yLoc += 15;
                    //Kloness
                    if (player.Inventory.Kloness)
                    {
                        SpriteHelper.DrawTextRightWithShadow(spriteBatch, "Yes", FontType.DialogsSmallSize8, new Vector2(X + 20, yLoc), 50, Color.White);
                    }
                    else
                    {
                        SpriteHelper.DrawTextRightWithShadow(spriteBatch, "No", FontType.DialogsSmallSize8, new Vector2(X + 20, yLoc), 50, Color.White);
                    }
                    SpriteHelper.DrawTextWithShadow(spriteBatch, "Kloness", FontType.DialogsSmallSize8, new Vector2(X + 80, yLoc), Cache.Colors[GameColor.Orange]);

                    yLoc += 15;
                    //IceWeapon
                    if (player.Inventory.IceWeapon)
                    {
                        SpriteHelper.DrawTextRightWithShadow(spriteBatch, "Yes", FontType.DialogsSmallSize8, new Vector2(X + 20, yLoc), 50, Color.White);
                    }
                    else
                    {
                        SpriteHelper.DrawTextRightWithShadow(spriteBatch, "No", FontType.DialogsSmallSize8, new Vector2(X + 20, yLoc), 50, Color.White);
                    }
                    SpriteHelper.DrawTextWithShadow(spriteBatch, "Ice Weapon", FontType.DialogsSmallSize8, new Vector2(X + 80, yLoc), Cache.Colors[GameColor.Orange]);

                    yLoc += 15;
                    //MedusaWeapon
                    if (player.Inventory.MedusaWeapon)
                    {
                        SpriteHelper.DrawTextRightWithShadow(spriteBatch, "Yes", FontType.DialogsSmallSize8, new Vector2(X + 20, yLoc), 50, Color.White);
                    }
                    else
                    {
                        SpriteHelper.DrawTextRightWithShadow(spriteBatch, "No", FontType.DialogsSmallSize8, new Vector2(X + 20, yLoc), 50, Color.White);
                    }
                    SpriteHelper.DrawTextWithShadow(spriteBatch, "Medusa Weapon", FontType.DialogsSmallSize8, new Vector2(X + 80, yLoc), Cache.Colors[GameColor.Orange]);


                    yLoc += 15;
                    //XelimaWeapon
                    if (player.Inventory.XelimaWeapon)
                    {
                        SpriteHelper.DrawTextRightWithShadow(spriteBatch, "Yes", FontType.DialogsSmallSize8, new Vector2(X + 20, yLoc), 50, Color.White);
                    }
                    else
                    {
                        SpriteHelper.DrawTextRightWithShadow(spriteBatch, "No", FontType.DialogsSmallSize8, new Vector2(X + 20, yLoc), 50, Color.White);
                    }
                    SpriteHelper.DrawTextWithShadow(spriteBatch, "Xelima Weapon", FontType.DialogsSmallSize8, new Vector2(X + 80, yLoc), Cache.Colors[GameColor.Orange]);

                    yLoc += 15;
                    //BowLine
                    if (player.Inventory.BowLine)
                    {
                        SpriteHelper.DrawTextRightWithShadow(spriteBatch, "Yes", FontType.DialogsSmallSize8, new Vector2(X + 20, yLoc), 50, Color.White);
                    }
                    else
                    {
                        SpriteHelper.DrawTextRightWithShadow(spriteBatch, "No", FontType.DialogsSmallSize8, new Vector2(X + 20, yLoc), 50, Color.White);
                    }
                    SpriteHelper.DrawTextWithShadow(spriteBatch, "Bow Line", FontType.DialogsSmallSize8, new Vector2(X + 80, yLoc), Cache.Colors[GameColor.Orange]);

                    yLoc += 15;
                    //MerienArmour
                    if (player.Inventory.MerienArmour)
                    {
                        SpriteHelper.DrawTextRightWithShadow(spriteBatch, "Yes", FontType.DialogsSmallSize8, new Vector2(X + 20, yLoc), 50, Color.White);
                    }
                    else
                    {
                        SpriteHelper.DrawTextRightWithShadow(spriteBatch, "No", FontType.DialogsSmallSize8, new Vector2(X + 20, yLoc), 50, Color.White);
                    }
                    SpriteHelper.DrawTextWithShadow(spriteBatch, "Merien Armour", FontType.DialogsSmallSize8, new Vector2(X + 80, yLoc), Cache.Colors[GameColor.Orange]);

                    yLoc += 15;
                    //MerienShield
                    if (player.Inventory.MerienShield)
                    {
                        SpriteHelper.DrawTextRightWithShadow(spriteBatch, "Yes", FontType.DialogsSmallSize8, new Vector2(X + 20, yLoc), 50, Color.White);
                    }
                    else
                    {
                        SpriteHelper.DrawTextRightWithShadow(spriteBatch, "No", FontType.DialogsSmallSize8, new Vector2(X + 20, yLoc), 50, Color.White);
                    }
                    SpriteHelper.DrawTextWithShadow(spriteBatch, "Merien Shield", FontType.DialogsSmallSize8, new Vector2(X + 80, yLoc), Cache.Colors[GameColor.Orange]);

                    yLoc += 15;
                    //Storm
                    if (player.Inventory.Storm)
                    {
                        SpriteHelper.DrawTextRightWithShadow(spriteBatch, "Yes", FontType.DialogsSmallSize8, new Vector2(X + 20, yLoc), 50, Color.White);
                    }
                    else
                    {
                        SpriteHelper.DrawTextRightWithShadow(spriteBatch, "No", FontType.DialogsSmallSize8, new Vector2(X + 20, yLoc), 50, Color.White);
                    }
                    SpriteHelper.DrawTextWithShadow(spriteBatch, "Storm", FontType.DialogsSmallSize8, new Vector2(X + 80, yLoc), Cache.Colors[GameColor.Orange]);

                    yLoc += 15;
                    //Hero
                    if (player.Inventory.Hero)
                    {
                        SpriteHelper.DrawTextRightWithShadow(spriteBatch, "Yes", FontType.DialogsSmallSize8, new Vector2(X + 20, yLoc), 50, Color.White);
                    }
                    else
                    {
                        SpriteHelper.DrawTextRightWithShadow(spriteBatch, "No", FontType.DialogsSmallSize8, new Vector2(X + 20, yLoc), 50, Color.White);
                    }
                    SpriteHelper.DrawTextWithShadow(spriteBatch, "Hero", FontType.DialogsSmallSize8, new Vector2(X + 80, yLoc), Cache.Colors[GameColor.Orange]);

                    yLoc += 15;
                    //HeroWarrior
                    if (player.Inventory.HeroWarrior)
                    {
                        SpriteHelper.DrawTextRightWithShadow(spriteBatch, "Yes", FontType.DialogsSmallSize8, new Vector2(X + 20, yLoc), 50, Color.White);
                    }
                    else
                    {
                        SpriteHelper.DrawTextRightWithShadow(spriteBatch, "No", FontType.DialogsSmallSize8, new Vector2(X + 20, yLoc), 50, Color.White);
                    }
                    SpriteHelper.DrawTextWithShadow(spriteBatch, "Hero Warrior", FontType.DialogsSmallSize8, new Vector2(X + 80, yLoc), Cache.Colors[GameColor.Orange]);

                    yLoc += 15;
                    //HeroMage
                    if (player.Inventory.HeroMage)
                    {
                        SpriteHelper.DrawTextRightWithShadow(spriteBatch, "Yes", FontType.DialogsSmallSize8, new Vector2(X + 20, yLoc), 50, Color.White);
                    }
                    else
                    {
                        SpriteHelper.DrawTextRightWithShadow(spriteBatch, "No", FontType.DialogsSmallSize8, new Vector2(X + 20, yLoc), 50, Color.White);
                    }
                    SpriteHelper.DrawTextWithShadow(spriteBatch, "Hero Mage", FontType.DialogsSmallSize8, new Vector2(X + 80, yLoc), Cache.Colors[GameColor.Orange]);
                    yLoc += 15;
                    //HeroBattleMage
                    if (player.Inventory.HeroBattleMage)
                    {
                        SpriteHelper.DrawTextRightWithShadow(spriteBatch, "Yes", FontType.DialogsSmallSize8, new Vector2(X + 20, yLoc), 50, Color.White);
                    }
                    else
                    {
                        SpriteHelper.DrawTextRightWithShadow(spriteBatch, "No", FontType.DialogsSmallSize8, new Vector2(X + 20, yLoc), 50, Color.White);
                    }
                    SpriteHelper.DrawTextWithShadow(spriteBatch, "Hero Battle Mage", FontType.DialogsSmallSize8, new Vector2(X + 80, yLoc), Cache.Colors[GameColor.Orange]);
                    yLoc += 15;
                    //HeroArcher
                    if (player.Inventory.HeroArcher)
                    {
                        SpriteHelper.DrawTextRightWithShadow(spriteBatch, "Yes", FontType.DialogsSmallSize8, new Vector2(X + 20, yLoc), 50, Color.White);
                    }
                    else
                    {
                        SpriteHelper.DrawTextRightWithShadow(spriteBatch, "No", FontType.DialogsSmallSize8, new Vector2(X + 20, yLoc), 50, Color.White);
                    }
                    SpriteHelper.DrawTextWithShadow(spriteBatch, "Hero Archer", FontType.DialogsSmallSize8, new Vector2(X + 80, yLoc), Cache.Colors[GameColor.Orange]);
                    yLoc += 15;
                    //Godly
                    if (player.Inventory.Godly)
                    {
                        SpriteHelper.DrawTextRightWithShadow(spriteBatch, "Yes", FontType.DialogsSmallSize8, new Vector2(X + 20, yLoc), 50, Color.White);
                    }
                    else
                    {
                        SpriteHelper.DrawTextRightWithShadow(spriteBatch, "No", FontType.DialogsSmallSize8, new Vector2(X + 20, yLoc), 50, Color.White);
                    }
                    SpriteHelper.DrawTextWithShadow(spriteBatch, "Godly", FontType.DialogsSmallSize8, new Vector2(X + 80, yLoc), Cache.Colors[GameColor.Orange]);
                    yLoc += 15;
                    //GodlyWarrior
                    if (player.Inventory.GodlyWarrior)
                    {
                        SpriteHelper.DrawTextRightWithShadow(spriteBatch, "Yes", FontType.DialogsSmallSize8, new Vector2(X + 20, yLoc), 50, Color.White);
                    }
                    else
                    {
                        SpriteHelper.DrawTextRightWithShadow(spriteBatch, "No", FontType.DialogsSmallSize8, new Vector2(X + 20, yLoc), 50, Color.White);
                    }
                    SpriteHelper.DrawTextWithShadow(spriteBatch, "Godly Warrior", FontType.DialogsSmallSize8, new Vector2(X + 80, yLoc), Cache.Colors[GameColor.Orange]);
                    yLoc += 15;
                    //GodlyMage
                    if (player.Inventory.GodlyMage)
                    {
                        SpriteHelper.DrawTextRightWithShadow(spriteBatch, "Yes", FontType.DialogsSmallSize8, new Vector2(X + 20, yLoc), 50, Color.White);
                    }
                    else
                    {
                        SpriteHelper.DrawTextRightWithShadow(spriteBatch, "No", FontType.DialogsSmallSize8, new Vector2(X + 20, yLoc), 50, Color.White);
                    }
                    SpriteHelper.DrawTextWithShadow(spriteBatch, "Godly Mage", FontType.DialogsSmallSize8, new Vector2(X + 80, yLoc), Cache.Colors[GameColor.Orange]);
                    yLoc += 15;
                    //GodlyBattleMage
                    if (player.Inventory.GodlyBattleMage)
                    {
                        SpriteHelper.DrawTextRightWithShadow(spriteBatch, "Yes", FontType.DialogsSmallSize8, new Vector2(X + 20, yLoc), 50, Color.White);
                    }
                    else
                    {
                        SpriteHelper.DrawTextRightWithShadow(spriteBatch, "No", FontType.DialogsSmallSize8, new Vector2(X + 20, yLoc), 50, Color.White);
                    }
                    SpriteHelper.DrawTextWithShadow(spriteBatch, "Godly Battle Mage", FontType.DialogsSmallSize8, new Vector2(X + 80, yLoc), Cache.Colors[GameColor.Orange]);
                    yLoc += 15;
                    //GodlyArcher
                    if (player.Inventory.GodlyArcher)
                    {
                        SpriteHelper.DrawTextRightWithShadow(spriteBatch, "Yes", FontType.DialogsSmallSize8, new Vector2(X + 20, yLoc), 50, Color.White);
                    }
                    else
                    {
                        SpriteHelper.DrawTextRightWithShadow(spriteBatch, "No", FontType.DialogsSmallSize8, new Vector2(X + 20, yLoc), 50, Color.White);
                    }
                    SpriteHelper.DrawTextWithShadow(spriteBatch, "Godly Archer", FontType.DialogsSmallSize8, new Vector2(X + 80, yLoc), Cache.Colors[GameColor.Orange]);
                    #endregion
                    break;
                case 5:
                    yLoc += 65;
                    #region Statistics
                    SpriteHelper.DrawTextWithShadow(spriteBatch, "Statistics", FontType.DialogsSmallSize8, new Vector2(X + 30, yLoc), Cache.Colors[GameColor.Orange]);
                    yLoc += 15;
                    SpriteHelper.DrawTextRightWithShadow(spriteBatch, String.Format("{0:0.00}", ((double)player.Titles[TitleType.DistanceTravelled] * 0.000621371192)), FontType.DialogsSmallSize8, new Vector2(X + 20, yLoc), 50, Color.White);
                    SpriteHelper.DrawTextWithShadow(spriteBatch, "Distance Travelled (miles)", FontType.DialogsSmallSize8, new Vector2(X + 80, yLoc), Cache.Colors[GameColor.Orange]);
                    yLoc += 15;
                    SpriteHelper.DrawTextRightWithShadow(spriteBatch, GameUtility.FormatStatistic(player.Titles[TitleType.ItemsCollected]), FontType.DialogsSmallSize8, new Vector2(X + 20, yLoc), 50, Color.White);
                    SpriteHelper.DrawTextWithShadow(spriteBatch, "Items Collected", FontType.DialogsSmallSize8, new Vector2(X + 80, yLoc), Cache.Colors[GameColor.Orange]);
                    yLoc += 15;
                    SpriteHelper.DrawTextRightWithShadow(spriteBatch, GameUtility.FormatStatistic(player.Titles[TitleType.RareItemsCollected]), FontType.DialogsSmallSize8, new Vector2(X + 20, yLoc), 50, Color.White);
                    SpriteHelper.DrawTextWithShadow(spriteBatch, "Rare Items Collected", FontType.DialogsSmallSize8, new Vector2(X + 80, yLoc), Cache.Colors[GameColor.Orange]);
                    yLoc += 15;
                    SpriteHelper.DrawTextRightWithShadow(spriteBatch, GameUtility.FormatStatistic(player.Titles[TitleType.HighestValueItem]), FontType.DialogsSmallSize8, new Vector2(X + 20, yLoc), 50, Color.White);
                    SpriteHelper.DrawTextWithShadow(spriteBatch, "Highest Valued Item (gold)", FontType.DialogsSmallSize8, new Vector2(X + 80, yLoc), Cache.Colors[GameColor.Orange]);
                    yLoc += 15;
                    SpriteHelper.DrawTextRightWithShadow(spriteBatch, GameUtility.FormatStatistic(player.Titles[TitleType.MonsterDamageDealt]), FontType.DialogsSmallSize8, new Vector2(X + 20, yLoc), 50, Color.White);
                    SpriteHelper.DrawTextWithShadow(spriteBatch, "Monster Damage Dealt", FontType.DialogsSmallSize8, new Vector2(X + 80, yLoc), Cache.Colors[GameColor.Orange]);
                    yLoc += 15;
                    SpriteHelper.DrawTextRightWithShadow(spriteBatch, GameUtility.FormatStatistic(player.Titles[TitleType.PlayerDamageDealt]), FontType.DialogsSmallSize8, new Vector2(X + 20, yLoc), 50, Color.White);
                    SpriteHelper.DrawTextWithShadow(spriteBatch, "Player Damage Dealt", FontType.DialogsSmallSize8, new Vector2(X + 80, yLoc), Cache.Colors[GameColor.Orange]);
                    yLoc += 15;
                    SpriteHelper.DrawTextRightWithShadow(spriteBatch, GameUtility.FormatStatistic(player.Titles[TitleType.MonstersKilled]), FontType.DialogsSmallSize8, new Vector2(X + 20, yLoc), 50, Color.White);
                    SpriteHelper.DrawTextWithShadow(spriteBatch, "Monster Kills", FontType.DialogsSmallSize8, new Vector2(X + 80, yLoc), Cache.Colors[GameColor.Orange]);
                    yLoc += 15;
                    SpriteHelper.DrawTextRightWithShadow(spriteBatch, GameUtility.FormatStatistic(player.Titles[TitleType.PlayersKilled]), FontType.DialogsSmallSize8, new Vector2(X + 20, yLoc), 50, Color.White);
                    SpriteHelper.DrawTextWithShadow(spriteBatch, "Player Kills", FontType.DialogsSmallSize8, new Vector2(X + 80, yLoc), Cache.Colors[GameColor.Orange]);
                    yLoc += 15;
                    SpriteHelper.DrawTextRightWithShadow(spriteBatch, GameUtility.FormatStatistic(player.Titles[TitleType.MonsterDeaths]), FontType.DialogsSmallSize8, new Vector2(X + 20, yLoc), 50, Color.White);
                    SpriteHelper.DrawTextWithShadow(spriteBatch, "Monster Deaths", FontType.DialogsSmallSize8, new Vector2(X + 80, yLoc), Cache.Colors[GameColor.Orange]);
                    yLoc += 15;
                    SpriteHelper.DrawTextRightWithShadow(spriteBatch, GameUtility.FormatStatistic(player.Titles[TitleType.PlayerDeaths]), FontType.DialogsSmallSize8, new Vector2(X + 20, yLoc), 50, Color.White);
                    SpriteHelper.DrawTextWithShadow(spriteBatch, "Player Deaths", FontType.DialogsSmallSize8, new Vector2(X + 80, yLoc), Cache.Colors[GameColor.Orange]);
                    yLoc += 15;
                    SpriteHelper.DrawTextRightWithShadow(spriteBatch, ((double)Math.Max(player.Titles[TitleType.MonstersKilled], 1) / Math.Max((double)player.Titles[TitleType.MonsterDeaths], 1)).ToString("0.##"), FontType.DialogsSmallSize8, new Vector2(X + 20, yLoc), 50, Color.White);
                    SpriteHelper.DrawTextWithShadow(spriteBatch, "Monster K/D Ratio", FontType.DialogsSmallSize8, new Vector2(X + 80, yLoc), Cache.Colors[GameColor.Orange]);
                    yLoc += 15;
                    SpriteHelper.DrawTextRightWithShadow(spriteBatch, ((double)Math.Max(player.Titles[TitleType.PlayersKilled], 1) / Math.Max((double)player.Titles[TitleType.PlayerDeaths], 1)).ToString("0.##"), FontType.DialogsSmallSize8, new Vector2(X + 20, yLoc), 50, Color.White);
                    SpriteHelper.DrawTextWithShadow(spriteBatch, "Player K/D Ratio", FontType.DialogsSmallSize8, new Vector2(X + 80, yLoc), Cache.Colors[GameColor.Orange]);
                    yLoc += 15;
                    SpriteHelper.DrawTextRightWithShadow(spriteBatch, GameUtility.FormatStatistic(player.Titles[TitleType.HighestMeleeDamage]), FontType.DialogsSmallSize8, new Vector2(X + 20, yLoc), 50, Color.White);
                    SpriteHelper.DrawTextWithShadow(spriteBatch, "Highest Melee Damage", FontType.DialogsSmallSize8, new Vector2(X + 80, yLoc), Cache.Colors[GameColor.Orange]);
                    yLoc += 15;
                    SpriteHelper.DrawTextRightWithShadow(spriteBatch, GameUtility.FormatStatistic(player.Titles[TitleType.HighestMagicDamage]), FontType.DialogsSmallSize8, new Vector2(X + 20, yLoc), 50, Color.White);
                    SpriteHelper.DrawTextWithShadow(spriteBatch, "Highest Magic Damage", FontType.DialogsSmallSize8, new Vector2(X + 80, yLoc), Cache.Colors[GameColor.Orange]);
                    yLoc += 15;
                    SpriteHelper.DrawTextRightWithShadow(spriteBatch, GameUtility.FormatStatistic(player.Titles[TitleType.QuestsCompleted]), FontType.DialogsSmallSize8, new Vector2(X + 20, yLoc), 50, Color.White);
                    SpriteHelper.DrawTextWithShadow(spriteBatch, "Quests Completed", FontType.DialogsSmallSize8, new Vector2(X + 80, yLoc), Cache.Colors[GameColor.Orange]);
                    yLoc += 15;
                    SpriteHelper.DrawTextRightWithShadow(spriteBatch, GameUtility.FormatStatistic(player.Titles[TitleType.PublicEventsGold]), FontType.DialogsSmallSize8, new Vector2(X + 20, yLoc), 50, Color.White);
                    SpriteHelper.DrawTextWithShadow(spriteBatch, "Gold Public Events", FontType.DialogsSmallSize8, new Vector2(X + 80, yLoc), Cache.Colors[GameColor.Orange]);
                    yLoc += 15;
                    SpriteHelper.DrawTextRightWithShadow(spriteBatch, GameUtility.FormatStatistic(player.Titles[TitleType.PublicEventsSilver]), FontType.DialogsSmallSize8, new Vector2(X + 20, yLoc), 50, Color.White);
                    SpriteHelper.DrawTextWithShadow(spriteBatch, "Silver Public Events", FontType.DialogsSmallSize8, new Vector2(X + 80, yLoc), Cache.Colors[GameColor.Orange]);
                    yLoc += 15;
                    SpriteHelper.DrawTextRightWithShadow(spriteBatch, GameUtility.FormatStatistic(player.Titles[TitleType.PublicEventsBronze]), FontType.DialogsSmallSize8, new Vector2(X + 20, yLoc), 50, Color.White);
                    SpriteHelper.DrawTextWithShadow(spriteBatch, "Bronze Public Events", FontType.DialogsSmallSize8, new Vector2(X + 80, yLoc), Cache.Colors[GameColor.Orange]);
                    #endregion
                    break;
            }

            /* TITLE */
            spriteBatch.Draw(Cache.Interface[(int)SpriteId.DialogsV2].Texture, new Vector2(X + 48, Y - 16), Cache.Interface[(int)SpriteId.DialogsV2].Frames[28].GetRectangle(), Color.White * transparency);
            SpriteHelper.DrawTextCenteredWithShadow(spriteBatch, "Statistics", FontType.DisplayNameSize13Spacing1, new Vector2(X + 58, Y - 9), 140, Cache.Colors[GameColor.Orange]);
                  
            //figure out what this is used for
            if (frame != null && Utility.IsSelected(mouseX, mouseY, X, Y, X + frame.Width, Y + frame.Height))
                return true;
            else return false;                                                         
        }

        public void Update(GameTime gameTime)
        {

        }

        public void LeftClicked()
        {
            //GameKeyboard keyboard = Cache.DefaultState.Display.Keyboard;
            int mouseX = (int)Cache.DefaultState.Display.Mouse.X;
            int mouseY = (int)Cache.DefaultState.Display.Mouse.Y;
            int X = config.X;
            int Y = config.Y;

            switch (config.State)
            {
                case DialogBoxState.Compact:
                    {
                        /* MINIMIZE BUTTON */
                        if ((X + 222 < mouseX) && (X + 233 > mouseX) && (Y + 14 < mouseY) && (Y + 22 > mouseY))
                        {
                            config.State = DialogBoxState.Normal;
                            //highlightedItemIndex = -1;
                            return;
                        }

                        //// SETS 1-5
                        //if ((Y + 10 < mouseY) && (Y + 24 > mouseY))
                        //{
                        //    // SET 1
                        //    if ((X + 35 < mouseX) && (X + 70 > mouseX)) { ((MainGame)Cache.DefaultState).UseItemSet(1); }
                        //    // SET 2
                        //    if ((X + 70 < mouseX) && (X + 105 > mouseX)) { ((MainGame)Cache.DefaultState).UseItemSet(2); }
                        //    // SET 3
                        //    if ((X + 105 < mouseX) && (X + 140 > mouseX)) { ((MainGame)Cache.DefaultState).UseItemSet(3); }
                        //    // SET 4
                        //    if ((X + 140 < mouseX) && (X + 175 > mouseX)) { ((MainGame)Cache.DefaultState).UseItemSet(4); }
                        //    // SET 5
                        //    if ((X + 175 < mouseX) && (X + 210 > mouseX)) { ((MainGame)Cache.DefaultState).UseItemSet(5); }
                        //}
                        break;
                    }
                default:
                    {
                        if (config.State == DialogBoxState.Normal)
                        {
                            /* MAXIMIZE BUTTON */
                            if ((X + 222 < mouseX) && (X + 233 > mouseX) && (Y + 14 < mouseY) && (Y + 22 > mouseY))
                            {
                                config.State = DialogBoxState.Compact;
                                //SetPagination();           
                                return;
                            }
                        }
                        break;
                        //    // SETS 1-5
                        //    if ((Y + 254 < mouseY) && (Y + 268 > mouseY))
                        //    {
                        //        // SET 1
                        //        if ((X + 17 < mouseX) && (X + 52 > mouseX)) { ((MainGame)Cache.DefaultState).UseItemSet(1); }
                        //        // SET 2
                        //        if ((X + 60 < mouseX) && (X + 95 > mouseX)) { ((MainGame)Cache.DefaultState).UseItemSet(2); }
                        //        // SET 3
                        //        if ((X + 103 < mouseX) && (X + 138 > mouseX)) { ((MainGame)Cache.DefaultState).UseItemSet(3); }
                        //        // SET 4
                        //        if ((X + 146 < mouseX) && (X + 181 > mouseX)) { ((MainGame)Cache.DefaultState).UseItemSet(4); }
                        //        // SET 5
                        //        if ((X + 189 < mouseX) && (X + 224 > mouseX)) { ((MainGame)Cache.DefaultState).UseItemSet(5); }
                        //    }

                        //    //CREATE SET
                        //    if ((Y + 225 < mouseY) && (Y + 247 > mouseY))
                        //    {
                        //        if ((X + 59 < mouseX) && (X + 116 > mouseX))
                        //        {
                        //            //Hide clear set dialog box
                        //            ((MainGame)Cache.DefaultState).DialogBoxes[GameDialogBoxType.ClearSet].Hide();

                        //            //Update x & y then show
                        //            ((MainGame)Cache.DefaultState).DialogBoxes[GameDialogBoxType.CreateSet].Config.X = X + 35;
                        //            ((MainGame)Cache.DefaultState).DialogBoxes[GameDialogBoxType.CreateSet].Config.Y = Y + 110;
                        //            ((MainGame)Cache.DefaultState).DialogBoxes[GameDialogBoxType.CreateSet].Toggle();
                        //        }

                        //        //CLEAR SET
                        //        if ((X + 123 < mouseX) && (X + 182 > mouseX))
                        //        {
                        //            //Hide create set dialog box
                        //            ((MainGame)Cache.DefaultState).DialogBoxes[GameDialogBoxType.CreateSet].Hide();

                        //            //Update x & y, then show
                        //            ((MainGame)Cache.DefaultState).DialogBoxes[GameDialogBoxType.ClearSet].Config.X = X + 35;
                        //            ((MainGame)Cache.DefaultState).DialogBoxes[GameDialogBoxType.ClearSet].Config.Y = Y + 110;
                        //            ((MainGame)Cache.DefaultState).DialogBoxes[GameDialogBoxType.ClearSet].Toggle();
                        //        }
                        //    }
                        //}

                        ///* CHARACTER BUTTON */
                        //if ((X + 32 < mouseX) && (X + 86 > mouseX) && (Y + 473 < mouseY) && (Y + 491 > mouseY))
                        //{
                        //    //config.State = DialogBoxState.Normal;
                        //    //SetPagination();
                        //    //highlightedItemIndex = -1;
                        //    return;
                        //}

                        ///* STATS BUTTON */
                        //if ((X + 93 < mouseX) && (X + 147 > mouseX) && (Y + 473 < mouseY) && (Y + 491 > mouseY))
                        //{
                        //    //((MainGame)Cache.DefaultState).DialogBoxes[GameDialogBoxType.Stats].Toggle();
                        //    ////config.State = DialogBoxState.Statistics;
                        //    ////SetPagination();
                        //    ////highlightedItemIndex = -1;
                        //    return;
                        //}

                        ///* QUEST BUTTON */
                        //if ((X + 154 < mouseX) && (X + 208 > mouseX) && (Y + 473 < mouseY) && (Y + 491 > mouseY))
                        //{
                        //    //((MainGame)Cache.DefaultState).DialogBoxes[GameDialogBoxType.Quest].Toggle();
                        //    ////config.State = DialogBoxState.Quest;
                        //    ////SetPagination();
                        //    ////highlightedItemIndex = -1;
                        //    return;
                        //}
                    }
            }
        }

        public void LeftDoubleClicked(ref SelectionMode selectionMode, ref int selectionModeId)
        {
        }

        public void LeftHeld()
        {
        }

        public void LeftDragged()
        {
        }

        public void LeftReleased(GameDialogBoxType highlightedDialogBox, int highlightedDialogBoxItemIndex)
        {
        }

        public void RightClicked() { }

        public void RightHeld() { }

        public void RightReleased() { }

        public void Scroll(int direction)
        {
            if (config.Page - direction > config.MaxPages) config.Page = 1;
            else if (config.Page - direction < 1) config.Page = config.MaxPages;
            else config.Page -= direction;
        }

        public void OffsetLocation(int x, int y)
        {      
            config.X += x;
            config.Y += y;
        }

        public void Show()
        {
                       
            if (!config.AlwaysVisible && Cache.DefaultState != null)
                ((IGameState)Cache.DefaultState).BringToFront(Type);
            highlightedItemIndex = -1;
            config.Hidden = false;
        }

        public void Hide()
        {                   
            if (!config.AlwaysVisible && Cache.DefaultState != null)
            {
                ((IGameState)Cache.DefaultState).DialogBoxDrawOrder.Remove(Type);
                ((IGameState)Cache.DefaultState).ClickedDialogBox = GameDialogBoxType.None;
            }
            config.Hidden = true;
        }

        public void Toggle(DialogBoxState state = DialogBoxState.Normal)
        {
                      
            if (config.Visible && state == config.State) Hide();
            else
            {
                config.State = state;
                Show();
            }
        }

        public void SetData(byte[] data)
        {

        }
    }
}
