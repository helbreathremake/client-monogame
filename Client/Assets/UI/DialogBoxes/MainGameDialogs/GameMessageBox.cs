﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

using Helbreath.Common;
using Helbreath.Common.Assets;
using Client.Assets.State;
using Client.Assets.Game;

namespace Client.Assets.UI
{
    public class GameMessageBox : IGameDialogBox
    {
        public event GameMessageBoxHandler ResponseSelected;

        // IDIALOG
        private GameDialogBoxType type;
        private int clickedItemIndex; // clicked item
        private int selectedItemIndex; // mouse over item
        private Vector2 selectedItemLocation;
        private int selectedItemIndexOffsetX;
        private int selectedItemIndexOffsetY;
        private int highlightedItemIndex; // chosen item (non volatile)

        private GameDialogBoxConfiguration config;

        public int ClickedItemIndex { get { return clickedItemIndex; } set { clickedItemIndex = value; } }
        public int SelectedItemIndex { get { return selectedItemIndex; } set { selectedItemIndex = value; } }
        public int HighlightedItemIndex { get { return highlightedItemIndex; } set { highlightedItemIndex = value; } }
        public int SelectedItemIndexOffsetX { get { return selectedItemIndexOffsetX; } set { selectedItemIndexOffsetX = value; } }
        public int SelectedItemIndexOffsetY { get { return selectedItemIndexOffsetY; } set { selectedItemIndexOffsetY = value; } }
        public GameDialogBoxType Type { get { return type; } }
        public GameDialogBoxConfiguration Config { get { return config; } set { config = value; } }

        // MESSAGE BOX
        private string text;
        private List<string> textLines;
        private string button1;
        private string button2;

        public GameMessageBox(GameDialogBoxType type, string text, string button1, string button2)
        {
            this.type = type;
            config = new GameDialogBoxConfiguration(type, Cache.GameSettings.Resolution);

            this.highlightedItemIndex = selectedItemIndex = clickedItemIndex = -1;    
            this.button1 = button1;
            this.button2 = button2;

            this.textLines = new List<string>();
            this.text = text;

            // split the text in to separate lines so it doesn't overlfow the dialog sprite
            string[] textWords = text.Split(' ');
            StringBuilder lineBuilder = new StringBuilder();
            for (int j = 0; j < textWords.Length; j++)
            {
                int lineLength = (int)Cache.Fonts[FontType.GeneralSize10].MeasureString(lineBuilder.ToString() + textWords[j]).X;
                if (lineLength > 120)
                {
                    lineBuilder.Append(textWords[j]);
                    textLines.Add(lineBuilder.ToString().Trim());
                    lineBuilder.Clear();
                }
                else
                {
                    lineBuilder.Append(textWords[j]);
                    lineBuilder.Append(" ");
                }
            }
            if (lineBuilder.Length > 0) textLines.Add(lineBuilder.ToString().Trim());
            if (textLines.Count <= 0) textLines.Add(text);
            


            Hide();
        }

        public bool Draw(SpriteBatch spriteBatch, GameTime gameTime)
        {
            if (config.Hidden)
                if (!config.AlwaysVisible) return false; // dont draw dialogs if not a HUD item
                else if (Cache.GameSettings.LockedDialogs) return false;  // show HUD items when HUD is unlocked (transparent)

            if (((MainGame)Cache.DefaultState).Player.IsCasting && Cache.GameSettings.HideDialogDuringCasting) return false;

            int x = config.X;
            int y = config.Y;
            int sprite = (int)SpriteId.DialogsV2 + 2;
            int mouseX = (int)Cache.DefaultState.Display.Mouse.X;
            int mouseY = (int)Cache.DefaultState.Display.Mouse.Y;
            float transparency = ((Cache.GameSettings.TransparentDialogs || config.Hidden) ? 0.5F : 1F);
            Player player = ((MainGame)Cache.DefaultState).Player;
            highlightedItemIndex = selectedItemIndex = -1;

            AnimationFrame frame = Cache.Interface[sprite].Frames[10];
            spriteBatch.Draw(Cache.Interface[sprite].Texture, new Vector2(x, y), frame.GetRectangle(), Color.White * transparency);

            switch (config.State)
            {
                case DialogBoxState.Normal:

                    // text
                    for (int line = 0; line < textLines.Count; line++)
                    {
                        SpriteHelper.DrawTextCenteredWithShadow(spriteBatch, textLines[line], FontType.GeneralSize10, new Vector2(x + 13, y + 20 + (line*15)), 155, Cache.Colors[GameColor.Orange]);
                    }

                    // button 1
                    if (!string.IsNullOrEmpty(button1))
                    {
                        if (Utility.IsSelected(mouseX, mouseY, x + 13, y + 62, x + 13 + 65, y + 62 + 33))
                        {
                            spriteBatch.Draw(Cache.Interface[(int)SpriteId.DialogsV2 + 5].Texture, new Vector2(x + 13, y + 62), Cache.Interface[(int)SpriteId.DialogsV2 + 5].Frames[16].GetRectangle(), Color.White * transparency);
                            SpriteHelper.DrawTextCenteredWithShadow(spriteBatch, button1, FontType.GeneralSize10, new Vector2(x + 13, y + 65), 65, Color.White * transparency);
                            selectedItemIndex = highlightedItemIndex;
                        }
                        else
                        {
                            spriteBatch.Draw(Cache.Interface[(int)SpriteId.DialogsV2 + 5].Texture, new Vector2(x + 13, y + 62), Cache.Interface[(int)SpriteId.DialogsV2 + 5].Frames[15].GetRectangle(), Color.White * transparency);
                            SpriteHelper.DrawTextCenteredWithShadow(spriteBatch, button1, FontType.GeneralSize10, new Vector2(x + 13, y + 65), 65, Color.White * transparency);
                        }
                    }

                    // button 2
                    if (!string.IsNullOrEmpty(button2))
                    {
                        if (Utility.IsSelected(mouseX, mouseY, x + 97, y + 62, x + 97 + 65, y + 62 + 33))
                        {
                            spriteBatch.Draw(Cache.Interface[(int)SpriteId.DialogsV2 + 5].Texture, new Vector2(x + 97, y + 62), Cache.Interface[(int)SpriteId.DialogsV2 + 5].Frames[16].GetRectangle(), Color.White * transparency);
                            SpriteHelper.DrawTextCenteredWithShadow(spriteBatch, button2, FontType.GeneralSize10, new Vector2(x + 97, y + 65), 65, Color.White * transparency);
                            selectedItemIndex = highlightedItemIndex;
                        }
                        else
                        {
                            spriteBatch.Draw(Cache.Interface[(int)SpriteId.DialogsV2 + 5].Texture, new Vector2(x + 97, y + 62), Cache.Interface[(int)SpriteId.DialogsV2 + 5].Frames[15].GetRectangle(), Color.White * transparency);
                            SpriteHelper.DrawTextCenteredWithShadow(spriteBatch, button2, FontType.GeneralSize10, new Vector2(x + 97, y + 65), 65, Color.White * transparency);
                        }
                    }
                    break;
            }

            if (frame != null && Utility.IsSelected(mouseX, mouseY, x, y, x + frame.Width, y + frame.Height))
                return true;
            else return false;
        }

        public void Update(GameTime gameTime)
        {

        }

        public void LeftClicked()
        {
            int mouseX = (int)Cache.DefaultState.Display.Mouse.X;
            int mouseY = (int)Cache.DefaultState.Display.Mouse.Y;
            int X = config.X;
            int Y = config.Y;

            // button 1 //YES
            if (!string.IsNullOrEmpty(button1))
            {
                if (Utility.IsSelected(mouseX, mouseY, X + 13, Y + 62, X + 13 + 65, Y + 62 + 33))
                    if (ResponseSelected != null) ResponseSelected(type, MessageBoxResponse.Yes);
                    else ((IGameState)Cache.DefaultState).HideMessageBox(type);
            }

            // button 2 //NO
            if (!string.IsNullOrEmpty(button2))
            {
                if (Utility.IsSelected(mouseX, mouseY, X + 97, Y + 62, X + 97 + 65, Y + 62 + 33))
                    if (ResponseSelected != null) ResponseSelected(type, MessageBoxResponse.No);
                    else ((IGameState)Cache.DefaultState).HideMessageBox(type);
            }
        }

        public void LeftDoubleClicked(ref SelectionMode selectionMode, ref int selectionModeId)
        {
        }

        public void LeftHeld()
        {

        }

        public void LeftDragged()
        {
        }

        public void LeftReleased(GameDialogBoxType highlightedDialogBox, int highlightedDialogBoxItemIndex)
        {
        }

        public void RightClicked() { }

        public void RightHeld() { }

        public void RightReleased() { }

        public void Scroll(int direction)
        {
            if (config.Page - direction > config.MaxPages) config.Page = 1;
            else if (config.Page - direction < 1) config.Page = config.MaxPages;
            else config.Page -= direction;

            highlightedItemIndex = -1; // clear popup
        }

        public void OffsetLocation(int x, int y)
        {
            config.X += x;
            config.Y += y;
        }

        public void Toggle(DialogBoxState state = DialogBoxState.Normal)
        {
            if (config.Visible && state == config.State) Hide();
            else
            {
                config.State = state;
                SetPagination();
                Show();
            }
        }

        public void Show()
        {
            if (!config.AlwaysVisible && Cache.DefaultState != null)
                ((IGameState)Cache.DefaultState).BringToFront(Type);
            highlightedItemIndex = -1;
            config.Hidden = false;
        }

        public void Hide()
        {
            if (!config.AlwaysVisible && Cache.DefaultState != null)
            {
                ((MainGame)Cache.DefaultState).DialogBoxDrawOrder.Remove(Type);
                ((IGameState)Cache.DefaultState).ClickedDialogBox = GameDialogBoxType.None;
            }
            config.Hidden = true;
        }

        private void SetPagination()
        {

        }

        public void SetData(byte[] data)
        {

        }
    }
}
