﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

using Helbreath.Common;
using Helbreath.Common.Assets;
using Client.Assets.State;
using Client.Assets.Game;

namespace Client.Assets.UI
{
    public class TradeV2DialogBox : IGameDialogBox
    {
        // IDIALOG
        private int clickedItemIndex; // clicked item
        private int selectedItemIndex; // mouse over item
        private Vector2 selectedItemLocation;
        private int selectedItemIndexOffsetX;
        private int selectedItemIndexOffsetY;
        private int highlightedItemIndex; // chosen item (non volatile)
        private int highlightedItemIndex2; // chosen item (non volatile)
        private Vector2 highlightedItemLocation; // chose item location (non volatile)
        private Vector2 highlightedItemLocation2; // chose item location (non volatile)

        private GameDialogBoxConfiguration config;

        //Base graphic info
        int sprite = (int)SpriteId.DialogsV2 + 5; //Main texture
        int spriteFrame; //rectangle in texutre
        int spriteFrameHover = -1; //frame to display when hovering mouse over
        int width = -1; //frame width
        int height = -1; //frame height
        AnimationFrame frame;

        public int ClickedItemIndex { get { return clickedItemIndex; } set { clickedItemIndex = value; } }
        public int SelectedItemIndex { get { return selectedItemIndex; } set { selectedItemIndex = value; } }
        public int HighlightedItemIndex { get { return highlightedItemIndex; } set { highlightedItemIndex = value; } }
        public int SelectedItemIndexOffsetX { get { return selectedItemIndexOffsetX; } set { selectedItemIndexOffsetX = value; } }
        public int SelectedItemIndexOffsetY { get { return selectedItemIndexOffsetY; } set { selectedItemIndexOffsetY = value; } }

        public GameDialogBoxType Type { get { return GameDialogBoxType.TradeV2; } }
        public GameDialogBoxConfiguration Config { get { return config; } set { config = value; } }

        // TRADE DIALOG
        public string TradePartnerName;
        public List<int> TradeList;
        public List<Item> TradeListPartner;
        public bool TradeAccepted;
        public bool TradeAcceptedPartner;

        public TradeV2DialogBox() //Default settings
        {
            config = new GameDialogBoxConfiguration(Type, Cache.GameSettings.Resolution);
            this.highlightedItemIndex = selectedItemIndex = clickedItemIndex = -1;
            this.TradeList = new List<int>();
            this.TradeListPartner = new List<Item>();
            frame = Cache.Interface[sprite].Frames[0];
            //Hide();
        }

        public TradeV2DialogBox(GameDialogBoxConfiguration config) 
        {
            this.config = config;
            this.highlightedItemIndex = selectedItemIndex = clickedItemIndex = -1;
            this.TradeList = new List<int>();
            this.TradeListPartner = new List<Item>();
            frame = Cache.Interface[sprite].Frames[0];
            //Hide();
        }

        public bool Draw(SpriteBatch spriteBatch, GameTime gameTime)
        {
            if (config.Hidden)
                if (!config.AlwaysVisible) return false; // dont draw dialogs if not a HUD item
                else if (Cache.GameSettings.LockedDialogs) return false;  // show HUD items when HUD is unlocked (transparent)

            if (((MainGame)Cache.DefaultState).Player.IsCasting && Cache.GameSettings.HideDialogDuringCasting) return false;

            int mouseX = (int)Cache.DefaultState.Display.Mouse.X;
            int mouseY = (int)Cache.DefaultState.Display.Mouse.Y;
            int X = config.X;
            int Y = config.Y;
            float transparency = ((Cache.GameSettings.TransparentDialogs || config.Hidden) ? 0.5F : 1F);
            Player player = ((MainGame)Cache.DefaultState).Player;
            highlightedItemIndex = highlightedItemIndex2 = selectedItemIndex = -1;

            spriteBatch.Draw(Cache.Interface[sprite].Texture, new Vector2(X, Y), frame.GetRectangle(), Color.White * transparency);

            SpriteHelper.DrawTextCenteredWithShadow(spriteBatch, player.Name, FontType.DisplayNameSize13Spacing1, new Vector2(X + 45, Y + 40), 265, Cache.Colors[GameColor.Orange]);
            SpriteHelper.DrawTextCenteredWithShadow(spriteBatch, TradePartnerName, FontType.DisplayNameSize13Spacing1, new Vector2(X + 345, Y + 40), 265, Cache.Colors[GameColor.Orange]);

            switch (config.State)
            {
                case DialogBoxState.Normal:
                    for (int i = 0; i < 7; i++)
                    {
                        int listIndex = Math.Max(0, ((config.Page - 1) * 7) + i);
                        // PLAYER TRADE LIST
                        if (listIndex < TradeList.Count && TradeList[listIndex] >= 0)
                        {
                            spriteBatch.Draw(Cache.Interface[sprite].Texture, new Vector2(X + 45, Y + 81 + (i * 42)), Cache.Interface[sprite].Frames[17].GetRectangle(), Color.White * transparency);

                            if (Utility.IsSelected(mouseX, mouseY, X + 45, Y + 81 + (i * 42), X + 45 + 266, Y + 81 + (i * 42) + 42))
                            {
                                highlightedItemIndex = listIndex;
                                highlightedItemLocation = new Vector2(48, 86 + (i * 42));
                            }

                            Color displayColour = (highlightedItemIndex == listIndex ? Cache.Colors[GameColor.Orange] : Color.White);

                            Item item = player.Inventory[TradeList[listIndex]];

                            if (item != null)
                            {
                                // get sprite frame
                                Rectangle f = Cache.Equipment[(int)SpriteId.ItemGround + item.Sprite].Frames[item.SpriteFrame].GetRectangle();
                                // get draw location offset from center of the slot
                                Rectangle draw = new Rectangle(X + 50 + 18 - (f.Width > 31 || f.Height > 31 ? 31 : f.Width) / 2,
                                                               Y + 86 + (i * 42) + 17 - (f.Width > 31 || f.Height > 31 ? 31 : f.Height) / 2,
                                                               f.Width,
                                                               f.Height
                                                               );

                                if (f.Width > 31 || f.Height > 31)
                                    spriteBatch.Draw(Cache.Equipment[(int)SpriteId.ItemGround + item.Sprite].Texture, new Rectangle(draw.X, draw.Y, 31, 31), f, (item.ColorType == GameColor.Custom) ? SpriteHelper.ColourFromArgb(item.Colour) : Cache.Colors[item.ColorType]);
                                else spriteBatch.Draw(Cache.Equipment[(int)SpriteId.ItemGround + item.Sprite].Texture, new Vector2(draw.X, draw.Y), f, (item.ColorType == GameColor.Custom) ? SpriteHelper.ColourFromArgb(item.Colour) : Cache.Colors[item.ColorType]);

                                // name
                                SpriteHelper.DrawTextWithShadow(spriteBatch, string.Format("{0} {1}", (item.Quality != ItemQuality.None ? item.Quality.ToString() : ""), item.FriendlyName).Trim(), FontType.GeneralSize10, new Vector2(X + 105, Y + 93 + (i * 42)), displayColour * transparency);

                                // endurance as a percentage over the top of the item image
                                double endurance = (((double)item.Endurance / (double)item.MaximumEndurance) * 100);
                                if (item.IsBroken)
                                    SpriteHelper.DrawTextCenteredWithShadow(spriteBatch, "Broken", FontType.DialogsSmallerSize7, new Vector2(X + 50, Y + 108 + (i * 42)), 40, (endurance < 100 ? Cache.Colors[GameColor.Enemy] : displayColour) * transparency);
                                else SpriteHelper.DrawTextCenteredWithShadow(spriteBatch, ((int)Math.Max(1, endurance)).ToString() + "%", FontType.DialogsSmallerSize7, new Vector2(X + 50, Y + 108 + (i * 42)), 40, (endurance < 100 ? Cache.Colors[GameColor.Enemy] : displayColour) * transparency);

                                // Remove button
                                if (Utility.IsSelected(mouseX, mouseY, X + 240, Y + 87 + (i * 42), X + 240 + 65, Y + 87 + (i * 42) + 33))
                                {
                                    spriteBatch.Draw(Cache.Interface[sprite].Texture, new Vector2(X + 240, Y + 87 + (i * 42)), Cache.Interface[sprite].Frames[16].GetRectangle(), Color.White * transparency);
                                    SpriteHelper.DrawTextCenteredWithShadow(spriteBatch, "Remove", FontType.GeneralSize10, new Vector2(X + 240, Y + 90 + (i * 42)), 65, Color.White * transparency);
                                }
                                else
                                {
                                    spriteBatch.Draw(Cache.Interface[sprite].Texture, new Vector2(X + 240, Y + 87 + (i * 42)), Cache.Interface[sprite].Frames[15].GetRectangle(), Color.White * transparency);
                                    SpriteHelper.DrawTextCenteredWithShadow(spriteBatch, "Remove", FontType.GeneralSize10, new Vector2(X + 240, Y + 90 + (i * 42)), 65, Color.White * transparency);
                                }
                            }
                        }
                        // PARTNER TRADE LIST
                        if (listIndex < TradeListPartner.Count && TradeListPartner[listIndex] != null)
                        {
                            spriteBatch.Draw(Cache.Interface[sprite].Texture, new Vector2(X + 345, Y + 81 + (i * 42)), Cache.Interface[sprite].Frames[17].GetRectangle(), Color.White * transparency);

                            if (Utility.IsSelected(mouseX, mouseY, X + 345, Y + 81 + (i * 42), X + 345 + 266, Y + 81 + (i * 42) + 42))
                            {
                                highlightedItemIndex2 = listIndex;
                                highlightedItemLocation2 = new Vector2(348, 86 + (i * 42));
                            }

                            Color displayColour = (highlightedItemIndex2 == listIndex ? Cache.Colors[GameColor.Orange] : Color.White);

                            Item item = TradeListPartner[listIndex];

                            if (item != null)
                            {
                                // get sprite frame
                                Rectangle f = Cache.Equipment[(int)SpriteId.ItemGround + item.Sprite].Frames[item.SpriteFrame].GetRectangle();
                                // get draw location offset from center of the slot
                                Rectangle draw = new Rectangle(X + 350 + 18 - (f.Width > 31 || f.Height > 31 ? 31 : f.Width) / 2,
                                                               Y + 86 + (i * 42) + 17 - (f.Width > 31 || f.Height > 31 ? 31 : f.Height) / 2,
                                                               f.Width,
                                                               f.Height
                                                               );

                                if (f.Width > 31 || f.Height > 31)
                                    spriteBatch.Draw(Cache.Equipment[(int)SpriteId.ItemGround + item.Sprite].Texture, new Rectangle(draw.X, draw.Y, 31, 31), f, (item.ColorType == GameColor.Custom) ? SpriteHelper.ColourFromArgb(item.Colour) : Cache.Colors[item.ColorType]);
                                else spriteBatch.Draw(Cache.Equipment[(int)SpriteId.ItemGround + item.Sprite].Texture, new Vector2(draw.X, draw.Y), f, (item.ColorType == GameColor.Custom) ? SpriteHelper.ColourFromArgb(item.Colour) : Cache.Colors[item.ColorType]);

                                // name
                                SpriteHelper.DrawTextWithShadow(spriteBatch, string.Format("{0} {1}", (item.Quality != ItemQuality.None ? item.Quality.ToString() : ""), item.FriendlyName).Trim(), FontType.GeneralSize10, new Vector2(X + 405, Y + 93 + (i * 42)), displayColour * transparency);

                                // endurance as a percentage over the top of the item image
                                double endurance = (((double)item.Endurance / (double)item.MaximumEndurance) * 100);
                                if (item.IsBroken)
                                    SpriteHelper.DrawTextCenteredWithShadow(spriteBatch, "Broken", FontType.DialogsSmallerSize7, new Vector2(X + 350, Y + 108 + (i * 42)), 40, (endurance < 100 ? Cache.Colors[GameColor.Enemy] : displayColour) * transparency);
                                else SpriteHelper.DrawTextCenteredWithShadow(spriteBatch, ((int)Math.Max(1, endurance)).ToString() + "%", FontType.DialogsSmallerSize7, new Vector2(X + 350, Y + 108 + (i * 42)), 40, (endurance < 100 ? Cache.Colors[GameColor.Enemy] : displayColour) * transparency);
                            }
                        }
                    }

                    /* PLAYER ACCEPT */
                    if (TradeAccepted)
                        spriteBatch.Draw(Cache.Interface[sprite].Texture, new Vector2(X + 105, Y + 81 + (6 * 42)), Cache.Interface[sprite].Frames[18].GetRectangle(), Color.White * transparency);
                    else if (Utility.IsSelected(mouseX, mouseY, X + 105, Y + 81 + (6 * 42), X + 105 + 120, Y + 81 + (6 * 42) + 33))
                        spriteBatch.Draw(Cache.Interface[sprite].Texture, new Vector2(X + 105, Y + 81 + (6 * 42)), Cache.Interface[sprite].Frames[8].GetRectangle(), Color.White * transparency);
                    else spriteBatch.Draw(Cache.Interface[sprite].Texture, new Vector2(X + 105, Y + 81 + (6 * 42)), Cache.Interface[sprite].Frames[7].GetRectangle(), Color.White * transparency);
                    SpriteHelper.DrawTextCenteredWithShadow(spriteBatch, "Accept", FontType.GeneralSize10, new Vector2(X + 105, Y + 87 + (6 * 42)), 120, Color.White * transparency);
                    
                    /* PARTNER ACCEPT */
                    if (TradeAcceptedPartner)
                         spriteBatch.Draw(Cache.Interface[sprite].Texture, new Vector2(X + 405, Y + 81 + (6 * 42)), Cache.Interface[sprite].Frames[18].GetRectangle(), Color.White * transparency);
                    else spriteBatch.Draw(Cache.Interface[sprite].Texture, new Vector2(X + 405, Y + 81 + (6 * 42)), Cache.Interface[sprite].Frames[7].GetRectangle(), Color.White * transparency);
                    SpriteHelper.DrawTextCenteredWithShadow(spriteBatch, "Accept", FontType.GeneralSize10, new Vector2(X + 405, Y + 87 + (6 * 42)), 120, Color.White * transparency);

                    /* TRADE LIST POPUP */
                    if (highlightedItemIndex >= 0 && TradeList[highlightedItemIndex] >= 0 && TradeList[highlightedItemIndex] < Globals.MaximumTotalItems)
                    {
                        /* ITEM DETAILS */
                        if (player.Inventory[TradeList[highlightedItemIndex]] != null)
                            ((MainGame)Cache.DefaultState).SetItemPopup(Type, player.Inventory[TradeList[highlightedItemIndex]], highlightedItemLocation, X, Y);
                            //ItemPopup.DrawItemPopup(spriteBatch, player.Inventory[TradeList[highlightedItemIndex]], highlightedItemLocation, X, Y);
                    }

                    /* TRADE PARTNER LIST POPUP */
                    if (highlightedItemIndex2 >= 0 && highlightedItemIndex2 < Globals.MaximumTotalItems)
                    {
                        /* ITEM DETAILS */
                        if (TradeListPartner[highlightedItemIndex2] != null)
                            ((MainGame)Cache.DefaultState).SetItemPopup(Type, TradeListPartner[highlightedItemIndex2], highlightedItemLocation, X, Y);
                            //ItemPopup.DrawItemPopup(spriteBatch, TradeListPartner[highlightedItemIndex2], highlightedItemLocation2, X, Y);
                    }
                    break;
            }

            if (frame != null && Utility.IsSelected(mouseX, mouseY, X, Y, X + frame.Width, Y + frame.Height))
                return true;
            else return false;
        }

        public void Update(GameTime gameTime)
        {

        }

        public void LeftClicked()
        {
            int mouseX = (int)Cache.DefaultState.Display.Mouse.X;
            int mouseY = (int)Cache.DefaultState.Display.Mouse.Y;
            Player player = ((MainGame)Cache.DefaultState).Player;
            int X = config.X;
            int Y = config.Y;

            switch (config.State)
            {
                case DialogBoxState.Normal:
                    for (int i = 0; i < 7; i++)
                    {
                        int listIndex = Math.Max(0, ((config.Page - 1) * 7) + i);
                        // PLAYER TRADE LIST
                        if (listIndex < TradeList.Count && TradeList[listIndex] >= 0)
                            if (Utility.IsSelected(mouseX, mouseY, X + 240, Y + 87 + (i * 42), X + 240 + 65, Y + 87 + (i * 42) + 33))
                            {
                                ((MainGame)Cache.DefaultState).RemoveTradeItem(TradeList[listIndex]);
                                break;
                            }
                    }

                    /* PLAYER ACCEPTED */
                    if (Utility.IsSelected(mouseX, mouseY, X + 105, Y + 81 + (6 * 42), X + 105 + 120, Y + 81 + (6 * 42) + 33))
                    {
                        TradeAccepted = !TradeAccepted;
                        ((MainGame)Cache.DefaultState).AcceptTrade(TradeAccepted);
                    }
                    break;
            }
        }

        public void LeftDoubleClicked(ref SelectionMode selectionMode, ref int selectionModeId)
        {
        }

        public void LeftHeld()
        {
        }

        public void LeftDragged()
        {
        }

        public void LeftReleased(GameDialogBoxType highlightedDialogBox, int highlightedDialogBoxItemIndex)
        {
        }

        public void RightClicked() { }

        public void RightHeld() { }

        public void RightReleased() { }

        public void Scroll(int direction)
        {
            if (config.Page - direction > config.MaxPages) config.Page = 1;
            else if (config.Page - direction < 1) config.Page = config.MaxPages;
            else config.Page -= direction;

            highlightedItemIndex = -1; // clear popup
        }

        public void OffsetLocation(int x, int y)
        {
            config.X += x;
            config.Y += y;
        }

        public bool AddTradeItem(int itemId)
        {
            if (TradeList.Count + 1 > 6) return false;
            if (!TradeList.Contains(itemId)) TradeList.Add(itemId);

            // reset accepted state
            TradeAccepted = TradeAcceptedPartner = false;

            return true;
        }

        public void RemoveTradeItem(int itemId)
        {
            if (TradeList.Contains(itemId)) TradeList.Remove(itemId);

            // reset accepted state
            TradeAccepted = TradeAcceptedPartner = false;
        }

        public bool AddTradePartnerItem(Item item)
        {
            if (TradeListPartner.Count + 1 > 6) return false;
            if (!TradeListPartner.Contains(item)) TradeListPartner.Add(item);

            // reset accepted state
            TradeAccepted = TradeAcceptedPartner = false;

            return true;
        }

        public void RemoveTradePartnerItem(int itemIndex)
        {
            for (int i = 0; i < TradeListPartner.Count; i++)
                if (TradeListPartner[i].SlotId == itemIndex)
                {
                    TradeListPartner.RemoveAt(i);
                    break;
                }

            // reset accepted state
            TradeAccepted = TradeAcceptedPartner = false;
        }

        private void ClearTrade()
        {
            TradeList.Clear();
            TradeListPartner.Clear();

            // reset accepted state
            TradeAccepted = TradeAcceptedPartner = false;
        }

        public void Toggle(DialogBoxState state = DialogBoxState.Normal)
        {
            if (config.Visible && state == config.State) Hide();
            else
            {
                config.State = state;
                SetPagination();
                Show();
            }
        }

        public void Show()
        {
            if (!config.AlwaysVisible && Cache.DefaultState != null)
                ((IGameState)Cache.DefaultState).BringToFront(Type);
            highlightedItemIndex = -1;
            config.Hidden = false;
        }

        public void Hide()
        {
            if (!config.AlwaysVisible && Cache.DefaultState != null)
            {
                ((MainGame)Cache.DefaultState).DialogBoxDrawOrder.Remove(Type);
                ((IGameState)Cache.DefaultState).ClickedDialogBox = GameDialogBoxType.None;
            }
            config.Hidden = true;

            ClearTrade();
        }

        private void SetPagination()
        {
            
        }

        public void SetData(byte[] data)
        {
            
        }
    }
}