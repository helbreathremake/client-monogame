﻿using System;
using System.Collections.Generic;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

using Helbreath.Common.Assets;
using Client.Assets.State;
using Helbreath.Common;
using Client.Assets.Game;

namespace Client.Assets.UI
{
    public class UpgradeV2DialogBox : IGameDialogBox
    {
        // IDIALOG
        private int clickedItemIndex = -1; // clicked item
        private int selectedItemIndex = -1; // mouse over item
        private Vector2 selectedItemLocation;
        private int selectedItemIndexOffsetX;
        private int selectedItemIndexOffsetY;
        private int highlightedItemIndex = -1; // chosen item (non volatile)
        private Vector2 highlightedItemLocation; // chose item location (non volatile)
        private GameDialogBoxConfiguration config;

        //Base graphic info
        int sprite = (int)SpriteId.DialogsV2 + 6; //Main texture
        int spriteFrame = 2; //rectangle in texutre
        int spriteFrameHover = -1; //frame to display when hovering mouse over
        int width = -1; //frame width
        int height = -1; //frame height 

        AnimationFrame frame;
        private float itemTransparency;

        public int SelectedItemIndex { get { return selectedItemIndex; } set { selectedItemIndex = value; } }
        public int ClickedItemIndex { get { return clickedItemIndex; } set { clickedItemIndex = value; } }    
        public int HighlightedItemIndex { get { return highlightedItemIndex; } set { highlightedItemIndex = value; } }
        public int SelectedItemIndexOffsetX { get { return selectedItemIndexOffsetX; } set { selectedItemIndexOffsetX = value; } }
        public int SelectedItemIndexOffsetY { get { return selectedItemIndexOffsetY; } set { selectedItemIndexOffsetY = value; } }
        //public Vector2 SelectedItemLocation { get { return selectedItemLocation; } set { selectedItemLocation = value; } }

        public GameDialogBoxType Type { get { return GameDialogBoxType.UpgradeV2; } }
        public GameDialogBoxConfiguration Config { get { return config; } set { config = value; } }

        // upgrade item
        public int ItemToUpgradeIndex;
        public int UpgradeIngredientIndex;
        Player player;
        Item itemToUpgrade;
        Item upgradedItem = null;
        public int upgradedItemIndex;
        int mouseX;
        int mouseY;
        int X ;
        int Y;
        float transparency;

        public UpgradeV2DialogBox()
        {
            config = new GameDialogBoxConfiguration(Type, Cache.GameSettings.Resolution);
            frame = Cache.Interface[sprite].Frames[spriteFrame];
            width = frame.Width;
            height = frame.Height;
            player = ((MainGame)Cache.DefaultState).Player;
            ItemToUpgradeIndex = player.Inventory.UpgradeItemIndex;
            UpgradeIngredientIndex = player.Inventory.UpgradeIngredientIndex;
            mouseX = (int)Cache.DefaultState.Display.Mouse.X;
            mouseY = (int)Cache.DefaultState.Display.Mouse.Y;
            X = config.X;
            Y = config.Y;
            transparency = ((Cache.GameSettings.TransparentDialogs || config.Hidden) ? 0.5F : 1F);
        }

        public UpgradeV2DialogBox(GameDialogBoxConfiguration config)
        {
            this.config = config;
            frame = Cache.Interface[sprite].Frames[spriteFrame];
            width = frame.Width;
            height = frame.Height;
            player = ((MainGame)Cache.DefaultState).Player;
            ItemToUpgradeIndex = player.Inventory.UpgradeItemIndex;
            UpgradeIngredientIndex = player.Inventory.UpgradeIngredientIndex;
            mouseX = (int)Cache.DefaultState.Display.Mouse.X;
            mouseY = (int)Cache.DefaultState.Display.Mouse.Y;
            X = config.X;
            Y = config.Y;
            transparency = ((Cache.GameSettings.TransparentDialogs || config.Hidden) ? 0.5F : 1F);
        }

        public bool Draw(SpriteBatch spriteBatch, GameTime gameTime)
        {
            if (config.Hidden)
                if (!config.AlwaysVisible) return false; // dont draw dialogs if not a HUD item
                else if (Cache.GameSettings.LockedDialogs) return false;  // show HUD items when HUD is unlocked (transparent)

            if (((MainGame)Cache.DefaultState).Player.IsCasting && Cache.GameSettings.HideDialogDuringCasting) return false;

            highlightedItemIndex = -1;

            spriteBatch.Draw(Cache.Interface[sprite].Texture, new Vector2(X, Y), frame.GetRectangle(), Color.White * transparency);

            switch (config.State)
            {
                case DialogBoxState.Normal:
                    {
                        SpriteHelper.DrawTextCenteredWithShadow(spriteBatch, "Add item or ingredient to upgrade", FontType.DialogsSmallSize8, new Vector2(X, Y + 87), width, Cache.Colors[GameColor.Orange]);

                        //Majestic -71x
                        if (Utility.IsSelected(mouseX, mouseY, X + 19, Y + 157, X + 19 + 65, Y + 157 + 33))
                        {
                            spriteBatch.Draw(Cache.Interface[(int)SpriteId.DialogsV2 + 5].Texture, new Vector2(X + 19, Y + 157), Cache.Interface[(int)SpriteId.DialogsV2 + 5].Frames[16].GetRectangle(), Color.White * transparency);
                            SpriteHelper.DrawTextCenteredWithShadow(spriteBatch, "Majestic", FontType.GeneralSize10, new Vector2(X + 19, Y + 160), 65, Color.White);
                        }
                        else
                        {
                            spriteBatch.Draw(Cache.Interface[(int)SpriteId.DialogsV2 + 5].Texture, new Vector2(X + 19, Y + 157), Cache.Interface[(int)SpriteId.DialogsV2 + 5].Frames[15].GetRectangle(), Color.White * transparency);
                            SpriteHelper.DrawTextCenteredWithShadow(spriteBatch, "Majestic", FontType.GeneralSize10, new Vector2(X + 19, Y + 160), 65, Color.White);
                        }

                        //Armour +0x
                        if (Utility.IsSelected(mouseX, mouseY, X + 90, Y + 157, X + 90 + 65, Y + 157 + 33))
                        {
                            spriteBatch.Draw(Cache.Interface[(int)SpriteId.DialogsV2 + 5].Texture, new Vector2(X + 90, Y + 157), Cache.Interface[(int)SpriteId.DialogsV2 + 5].Frames[16].GetRectangle(), Color.White * transparency);
                            SpriteHelper.DrawTextCenteredWithShadow(spriteBatch, "Normal", FontType.GeneralSize10, new Vector2(X + 90, Y + 160), 65, Color.White);
                        }
                        else
                        {
                            spriteBatch.Draw(Cache.Interface[(int)SpriteId.DialogsV2 + 5].Texture, new Vector2(X + 90, Y + 157), Cache.Interface[(int)SpriteId.DialogsV2 + 5].Frames[15].GetRectangle(), Color.White * transparency);
                            SpriteHelper.DrawTextCenteredWithShadow(spriteBatch, "Normal", FontType.GeneralSize10, new Vector2(X + 90, Y + 160), 65, Color.White);
                        }

                        //Hero +71x
                        if (Utility.IsSelected(mouseX, mouseY, X + 161, Y + 157, X + 161 + 65, Y + 157 + 33))
                        {
                            spriteBatch.Draw(Cache.Interface[(int)SpriteId.DialogsV2 + 5].Texture, new Vector2(X + 161, Y + 157), Cache.Interface[(int)SpriteId.DialogsV2 + 5].Frames[16].GetRectangle(), Color.White * transparency);
                            SpriteHelper.DrawTextCenteredWithShadow(spriteBatch, "Hero", FontType.GeneralSize10, new Vector2(X + 161, Y + 160), 65, Color.White);
                        }
                        else
                        {
                            spriteBatch.Draw(Cache.Interface[(int)SpriteId.DialogsV2 + 5].Texture, new Vector2(X + 161, Y + 157), Cache.Interface[(int)SpriteId.DialogsV2 + 5].Frames[15].GetRectangle(), Color.White * transparency);
                            SpriteHelper.DrawTextCenteredWithShadow(spriteBatch, "Hero", FontType.GeneralSize10, new Vector2(X + 161, Y + 160), 65, Color.White);
                        }

                        selectedItemIndex = -1;

                        /* TITLE */
                        spriteBatch.Draw(Cache.Interface[(int)SpriteId.DialogsV2].Texture, new Vector2(X + 48, Y - 16), Cache.Interface[(int)SpriteId.DialogsV2].Frames[28].GetRectangle(), Color.White * transparency);
                        SpriteHelper.DrawTextCenteredWithShadow(spriteBatch, "Upgrade", FontType.DisplayNameSize13Spacing1, new Vector2(X, Y - 9), width, Cache.Colors[GameColor.Orange]);
                        break;
                    }
                case DialogBoxState.HeroUpgrade:
                    {
                        SpriteHelper.DrawTextCenteredWithShadow(spriteBatch, "Not implemented", FontType.DialogsSmallSize8, new Vector2(X, Y + 87), width, Cache.Colors[GameColor.Orange]);

                        //Upgrade
                        if (Utility.IsSelected(mouseX, mouseY, X + 50, Y + 157, X + 50 + 65, Y + 157 + 33))
                        {
                            spriteBatch.Draw(Cache.Interface[(int)SpriteId.DialogsV2 + 5].Texture, new Vector2(X + 50, Y + 157), Cache.Interface[(int)SpriteId.DialogsV2 + 5].Frames[16].GetRectangle(), Color.White * transparency);
                            SpriteHelper.DrawTextCenteredWithShadow(spriteBatch, "Upgrade", FontType.GeneralSize10, new Vector2(X + 50, Y + 160), 65, Color.White);
                        }
                        else
                        {
                            spriteBatch.Draw(Cache.Interface[(int)SpriteId.DialogsV2 + 5].Texture, new Vector2(X + 50, Y + 157), Cache.Interface[(int)SpriteId.DialogsV2 + 5].Frames[15].GetRectangle(), Color.White * transparency);
                            SpriteHelper.DrawTextCenteredWithShadow(spriteBatch, "Upgrade", FontType.GeneralSize10, new Vector2(X + 50, Y + 160), 65, Color.White);
                        }

                        //Cancel
                        if (Utility.IsSelected(mouseX, mouseY, X + 130, Y + 157, X + 130 + 65, Y + 157 + 33))
                        {
                            spriteBatch.Draw(Cache.Interface[(int)SpriteId.DialogsV2 + 5].Texture, new Vector2(X + 130, Y + 157), Cache.Interface[(int)SpriteId.DialogsV2 + 5].Frames[16].GetRectangle(), Color.White * transparency);
                            SpriteHelper.DrawTextCenteredWithShadow(spriteBatch, "Cancel", FontType.GeneralSize10, new Vector2(X + 130, Y + 160), 65, Color.White);
                        }
                        else
                        {
                            spriteBatch.Draw(Cache.Interface[(int)SpriteId.DialogsV2 + 5].Texture, new Vector2(X + 130, Y + 157), Cache.Interface[(int)SpriteId.DialogsV2 + 5].Frames[15].GetRectangle(), Color.White * transparency);
                            SpriteHelper.DrawTextCenteredWithShadow(spriteBatch, "Cancel", FontType.GeneralSize10, new Vector2(X + 130, Y + 160), 65, Color.White);
                        }

                        /* TITLE */
                        spriteBatch.Draw(Cache.Interface[(int)SpriteId.DialogsV2].Texture, new Vector2(X + 48, Y - 16), Cache.Interface[(int)SpriteId.DialogsV2].Frames[28].GetRectangle(), Color.White * transparency);
                        SpriteHelper.DrawTextCenteredWithShadow(spriteBatch, "Hero", FontType.DisplayNameSize13Spacing1, new Vector2(X, Y - 9), width, Cache.Colors[GameColor.Orange]);

                        if (highlightedItemIndex != -1 && player.Inventory[highlightedItemIndex] != null)
                        {
                            selectedItemIndex = highlightedItemIndex;
                            if (clickedItemIndex == -1) selectedItemLocation = highlightedItemLocation;
                        }
                        else selectedItemIndex = -1;
                        break;
                    }
                case DialogBoxState.IngredientUpgrade:
                    {
                        itemToUpgrade = null;
                        if (ItemToUpgradeIndex != -1 && player.Inventory[ItemToUpgradeIndex] != null)
                            itemToUpgrade = player.Inventory[ItemToUpgradeIndex];

                        if (itemToUpgrade == null)
                        {
                            if ((X + 51 < mouseX) && (X + 51 + 37 > mouseX) && (Y + 64 < mouseY) && (Y + 64 + 34 > mouseY))
                                spriteBatch.Draw(Cache.Interface[(int)SpriteId.DialogsV2 + 2].Texture, new Vector2(X + 48, Y + 58), Cache.Interface[(int)SpriteId.DialogsV2 + 2].Frames[2].GetRectangle(), Color.White * transparency);
                            else spriteBatch.Draw(Cache.Interface[(int)SpriteId.DialogsV2 + 2].Texture, new Vector2(X + 48, Y + 58), Cache.Interface[(int)SpriteId.DialogsV2 + 2].Frames[7].GetRectangle(), Color.White * transparency);
                        }
                        else
                        {
                            GameColor itemTextColor = GameColor.None;
                            switch (itemToUpgrade.Quality)
                            {
                                default:
                                case ItemQuality.None: itemTextColor = GameColor.Normal; break;//Color Orage //TODO expand on this for items that don't get quality level
                                case ItemQuality.Flimsy: itemTextColor = GameColor.Flimsy; break;//Color White
                                case ItemQuality.Sturdy: itemTextColor = GameColor.Sturdy; break;//Color Green
                                case ItemQuality.Reinforced: itemTextColor = GameColor.Reinforced; break;//Color Blue
                                case ItemQuality.Studded: itemTextColor = GameColor.Studded; break;//Color Purple
                            }

                            //TODO add custom color values?
                            SpriteHelper.DrawTextCenteredWithShadow(spriteBatch, itemToUpgrade.FriendlyName, FontType.DialogsSmallSize8, new Vector2(X, Y + 33), width, Cache.Colors[itemTextColor]);

                            SpriteHelper.DrawTextCenteredWithShadow(spriteBatch, string.Format("Lvl.{0}", itemToUpgrade.Level), FontType.DialogsSmallSize8, new Vector2(X + 51, Y + 103), 40, Color.White);

                            if ((X + 51 < mouseX) && (X + 51 + 37 > mouseX) && (Y + 64 < mouseY) && (Y + 64 + 34 > mouseY))
                            {
                                highlightedItemIndex = ItemToUpgradeIndex;
                                highlightedItemLocation = new Vector2(54, 59);
                            }

                            if (itemToUpgrade.UpgradeType == ItemUpgradeType.Majestic && player.Majestics < itemToUpgrade.MajesticUpgradeCost())
                                spriteBatch.Draw(Cache.Interface[(int)SpriteId.DialogsV2 + 2].Texture, new Vector2(X + 48, Y + 58), Cache.Interface[(int)SpriteId.DialogsV2 + 2].Frames[2].GetRectangle(), Color.White * transparency); //Orange box
                            spriteBatch.Draw(Cache.Interface[(int)SpriteId.DialogsV2 + 2].Texture, new Vector2(X + 48, Y + 58), Cache.Interface[(int)SpriteId.DialogsV2 + 2].Frames[11].GetRectangle(), Color.White * transparency); //Green box

                            // get sprite frame
                            Rectangle f = Cache.Equipment[(int)SpriteId.ItemGround + itemToUpgrade.Sprite].Frames[itemToUpgrade.SpriteFrame].GetRectangle();
                            // get draw location offset from center of the slot
                            Rectangle draw = new Rectangle(X + 51 + 18 - (f.Width > 31 || f.Height > 31 ? 31 : f.Width) / 2,
                                                           Y + 62 + 17 - (f.Width > 31 || f.Height > 31 ? 31 : f.Height) / 2,
                                                           f.Width,
                                                           f.Height
                                                           );

                            itemTransparency = 1.0f;
                            if (Cache.DefaultState.Display.Mouse.IsDragging && clickedItemIndex == ItemToUpgradeIndex)
                            {
                                itemTransparency = 0.6f;
                                spriteBatch.Draw(Cache.BackgroundTexture, new Rectangle(X + 51 + 5, Y + 62 + 5, 27, 24), f, Color.Black * 0.5f);
                            }


                            if (f.Width > 31 || f.Height > 31)
                                spriteBatch.Draw(Cache.Equipment[(int)SpriteId.ItemGround + itemToUpgrade.Sprite].Texture, new Rectangle(draw.X, draw.Y, 31, 31), f, ((itemToUpgrade.ColorType == GameColor.Custom) ? SpriteHelper.ColourFromArgb(itemToUpgrade.Colour) : Cache.Colors[itemToUpgrade.ColorType]) * itemTransparency);
                            else spriteBatch.Draw(Cache.Equipment[(int)SpriteId.ItemGround + itemToUpgrade.Sprite].Texture, new Vector2(draw.X, draw.Y), f, ((itemToUpgrade.ColorType == GameColor.Custom) ? SpriteHelper.ColourFromArgb(itemToUpgrade.Colour) : Cache.Colors[itemToUpgrade.ColorType]) * itemTransparency);

                            // endurance as a percentage over the top of the item image
                            double endurance = (((double)itemToUpgrade.Endurance / (double)itemToUpgrade.MaximumEndurance) * 100);
                            if (itemToUpgrade.IsBroken)
                                SpriteHelper.DrawTextCenteredWithShadow(spriteBatch, "Broken", FontType.DialogsSmallerSize7, new Vector2(X + 51, Y + 84), 40, Cache.Colors[GameColor.Enemy]);
                            else SpriteHelper.DrawTextCenteredWithShadow(spriteBatch, ((int)Math.Max(1, endurance)).ToString() + "%", FontType.DialogsSmallerSize7, new Vector2(X + 51, Y + 84), 40, (endurance < 100 ? Cache.Colors[GameColor.Enemy] : Color.White));
                        }

                        Item itemIngredient = null;
                        if (UpgradeIngredientIndex != -1 && player.Inventory[UpgradeIngredientIndex] != null)
                            itemIngredient = player.Inventory[UpgradeIngredientIndex];

                        if (itemIngredient == null)
                        {
                            if (itemToUpgrade == null || itemToUpgrade.UpgradeType == ItemUpgradeType.Merien || itemToUpgrade.UpgradeType == ItemUpgradeType.Xelima)
                            {
                                if ((X + 155 < mouseX) && (X + 155 + 37 > mouseX) && (Y + 64 < mouseY) && (Y + 64 + 34 > mouseY))
                                    spriteBatch.Draw(Cache.Interface[(int)SpriteId.DialogsV2 + 2].Texture, new Vector2(X + 152, Y + 58), Cache.Interface[(int)SpriteId.DialogsV2 + 2].Frames[2].GetRectangle(), Color.White * transparency);
                                else spriteBatch.Draw(Cache.Interface[(int)SpriteId.DialogsV2 + 2].Texture, new Vector2(X + 152, Y + 58), Cache.Interface[(int)SpriteId.DialogsV2 + 2].Frames[7].GetRectangle(), Color.White * transparency);
                            }
                        }
                        else
                        {
                            SpriteHelper.DrawTextCenteredWithShadow(spriteBatch, "Lvl+1", FontType.DialogsSmallSize8, new Vector2(X + 155, Y + 103), 40, Color.White);

                            if ((X + 155 < mouseX) && (X + 155 + 37 > mouseX) && (Y + 64 < mouseY) && (Y + 64 + 34 > mouseY))
                            {
                                highlightedItemIndex = UpgradeIngredientIndex;
                                highlightedItemLocation = new Vector2(158, 59);
                            }

                            if (itemToUpgrade != null && itemToUpgrade.CanUpgrade(itemIngredient))
                                spriteBatch.Draw(Cache.Interface[(int)SpriteId.DialogsV2 + 2].Texture, new Vector2(X + 152, Y + 58), Cache.Interface[(int)SpriteId.DialogsV2 + 2].Frames[11].GetRectangle(), Color.White * transparency);
                            else spriteBatch.Draw(Cache.Interface[(int)SpriteId.DialogsV2 + 2].Texture, new Vector2(X + 152, Y + 58), Cache.Interface[(int)SpriteId.DialogsV2 + 2].Frames[2].GetRectangle(), Color.White * transparency);

                            // get sprite frame
                            Rectangle f = Cache.Equipment[(int)SpriteId.ItemGround + itemIngredient.Sprite].Frames[itemIngredient.SpriteFrame].GetRectangle();
                            // get draw location offset from center of the slot
                            Rectangle draw = new Rectangle(X + 155 + 18 - (f.Width > 31 || f.Height > 31 ? 31 : f.Width) / 2,
                                                           Y + 62 + 17 - (f.Width > 31 || f.Height > 31 ? 31 : f.Height) / 2,
                                                           f.Width,
                                                           f.Height
                                                           );

                            itemTransparency = 1.0f;
                            if (Cache.DefaultState.Display.Mouse.IsDragging && clickedItemIndex == UpgradeIngredientIndex)
                            {
                                itemTransparency = 0.6f;
                                spriteBatch.Draw(Cache.BackgroundTexture, new Rectangle(X + 155 + 5, Y + 62 + 5, 27, 24), f, Color.Black * 0.5f);
                            }

                            if (f.Width > 31 || f.Height > 31)
                                spriteBatch.Draw(Cache.Equipment[(int)SpriteId.ItemGround + itemIngredient.Sprite].Texture, new Rectangle(draw.X, draw.Y, 31, 31), f, ((itemIngredient.ColorType == GameColor.Custom) ? SpriteHelper.ColourFromArgb(itemIngredient.Colour) : Cache.Colors[itemIngredient.ColorType]) * itemTransparency);
                            else spriteBatch.Draw(Cache.Equipment[(int)SpriteId.ItemGround + itemIngredient.Sprite].Texture, new Vector2(draw.X, draw.Y), f, ((itemIngredient.ColorType == GameColor.Custom) ? SpriteHelper.ColourFromArgb(itemIngredient.Colour) : Cache.Colors[itemIngredient.ColorType]) * itemTransparency);
                        }

                        if (itemToUpgrade != null)
                        {
                            bool ready = true;

                            if (itemIngredient != null)
                            {
                                if (!itemToUpgrade.CanUpgrade(itemIngredient))
                                {
                                    SpriteHelper.DrawTextCenteredWithShadow(spriteBatch, "Incorrect ingredient", FontType.DialogsSmallSize8, new Vector2(X, Y + 122), width, Cache.Colors[GameColor.Orange]);
                                    ready = false;
                                }
                            }
                            else
                            {
                                if (UpgradeIngredientIndex == -1)
                                {
                                    SpriteHelper.DrawTextCenteredWithShadow(spriteBatch, "Add ingredient", FontType.DialogsSmallSize8, new Vector2(X, Y + 122), width, Cache.Colors[GameColor.Orange]);
                                    ready = false;
                                }
                            }


                            if (ready)
                            {
                                SpriteHelper.DrawTextCenteredWithShadow(spriteBatch, "Item is ready to upgrade", FontType.DialogsSmallSize8, new Vector2(X, Y + 123), width, Cache.Colors[GameColor.Friendly]);
                                SpriteHelper.DrawTextCenteredWithShadow(spriteBatch, string.Format("Chance: {0}%", itemToUpgrade.UpgradeChance().ToString("##.##")), FontType.DialogsSmallSize8, new Vector2(X, Y + 137), width, Color.White);
                            }
                        }
                        else
                        {

                            if (UpgradeIngredientIndex == -1)
                            {
                                SpriteHelper.DrawTextCenteredWithShadow(spriteBatch, "Item: None", FontType.DialogsSmallSize8, new Vector2(X, Y + 33), width, Cache.Colors[GameColor.Orange]);
                                SpriteHelper.DrawTextCenteredWithShadow(spriteBatch, "Add item or ingredient", FontType.DialogsSmallSize8, new Vector2(X, Y + 122), width, Cache.Colors[GameColor.Orange]);
                            }
                            else
                            {
                                string type = "";
                                switch (itemIngredient.EffectType)
                                {
                                    case  ItemEffectType.Merien: type = "Armour"; break;
                                    case ItemEffectType.Xelima: type = "Weapon"; break;
                                    default: break;
                                }
                                SpriteHelper.DrawTextCenteredWithShadow(spriteBatch, string.Format("{0} ingredient", type), FontType.DialogsSmallSize8, new Vector2(X, Y + 33), width, Cache.Colors[GameColor.Orange]);
                                SpriteHelper.DrawTextCenteredWithShadow(spriteBatch, "Add item", FontType.DialogsSmallSize8, new Vector2(X, Y + 122), width, Cache.Colors[GameColor.Orange]);
                            }

                        }

                        //Upgrade
                        if (Utility.IsSelected(mouseX, mouseY, X + 50, Y + 157, X + 50 + 65, Y + 157 + 33))
                        {
                            spriteBatch.Draw(Cache.Interface[(int)SpriteId.DialogsV2 + 5].Texture, new Vector2(X + 50, Y + 157), Cache.Interface[(int)SpriteId.DialogsV2 + 5].Frames[16].GetRectangle(), Color.White * transparency);
                            SpriteHelper.DrawTextCenteredWithShadow(spriteBatch, "Upgrade", FontType.GeneralSize10, new Vector2(X + 50, Y + 160), 65, Color.White);
                        }
                        else
                        {
                            spriteBatch.Draw(Cache.Interface[(int)SpriteId.DialogsV2 + 5].Texture, new Vector2(X + 50, Y + 157), Cache.Interface[(int)SpriteId.DialogsV2 + 5].Frames[15].GetRectangle(), Color.White * transparency);
                            SpriteHelper.DrawTextCenteredWithShadow(spriteBatch, "Upgrade", FontType.GeneralSize10, new Vector2(X + 50, Y + 160), 65, Color.White);
                        }

                        //Cancel
                        if (Utility.IsSelected(mouseX, mouseY, X + 130, Y + 157, X + 130 + 65, Y + 157 + 33))
                        {
                            spriteBatch.Draw(Cache.Interface[(int)SpriteId.DialogsV2 + 5].Texture, new Vector2(X + 130, Y + 157), Cache.Interface[(int)SpriteId.DialogsV2 + 5].Frames[16].GetRectangle(), Color.White * transparency);
                            SpriteHelper.DrawTextCenteredWithShadow(spriteBatch, "Cancel", FontType.GeneralSize10, new Vector2(X + 130, Y + 160), 65, Color.White);
                        }
                        else
                        {
                            spriteBatch.Draw(Cache.Interface[(int)SpriteId.DialogsV2 + 5].Texture, new Vector2(X + 130, Y + 157), Cache.Interface[(int)SpriteId.DialogsV2 + 5].Frames[15].GetRectangle(), Color.White * transparency);
                            SpriteHelper.DrawTextCenteredWithShadow(spriteBatch, "Cancel", FontType.GeneralSize10, new Vector2(X + 130, Y + 160), 65, Color.White);
                        }

                        /* TITLE */
                        spriteBatch.Draw(Cache.Interface[(int)SpriteId.DialogsV2].Texture, new Vector2(X + 48, Y - 16), Cache.Interface[(int)SpriteId.DialogsV2].Frames[28].GetRectangle(), Color.White * transparency);
                        SpriteHelper.DrawTextCenteredWithShadow(spriteBatch, "Normal", FontType.DisplayNameSize13Spacing1, new Vector2(X, Y - 9), width, Cache.Colors[GameColor.Orange]);

                        if (highlightedItemIndex != -1 && player.Inventory[highlightedItemIndex] != null)
                        { 
                            selectedItemIndex = highlightedItemIndex; 
                            if (clickedItemIndex == -1) selectedItemLocation = highlightedItemLocation; 
                        }
                        else selectedItemIndex = -1;
                        break;
                    }
                case DialogBoxState.MajesticUpgrade:
                    {
                        itemToUpgrade = null;
                        if (ItemToUpgradeIndex != -1 && player.Inventory[ItemToUpgradeIndex] != null)
                            itemToUpgrade = player.Inventory[ItemToUpgradeIndex];

                        if (itemToUpgrade == null)
                        {
                            if ((X + 51 < mouseX) && (X + 51 + 37 > mouseX) && (Y + 64 < mouseY) && (Y + 64 + 34 > mouseY))
                                spriteBatch.Draw(Cache.Interface[(int)SpriteId.DialogsV2 + 2].Texture, new Vector2(X + 48, Y + 58), Cache.Interface[(int)SpriteId.DialogsV2 + 2].Frames[2].GetRectangle(), Color.White * transparency);
                            else spriteBatch.Draw(Cache.Interface[(int)SpriteId.DialogsV2 + 2].Texture, new Vector2(X + 48, Y + 58), Cache.Interface[(int)SpriteId.DialogsV2 + 2].Frames[7].GetRectangle(), Color.White * transparency);
                        }
                        else
                        {

                            GameColor itemTextColor = GameColor.None;
                            switch (itemToUpgrade.Quality)
                            {
                                default:
                                case ItemQuality.None: itemTextColor = GameColor.Normal; break;//Color Orage //TODO expand on this for items that don't get quality level
                                case ItemQuality.Flimsy: itemTextColor = GameColor.Flimsy; break;//Color White
                                case ItemQuality.Sturdy: itemTextColor = GameColor.Sturdy; break;//Color Green
                                case ItemQuality.Reinforced: itemTextColor = GameColor.Reinforced; break;//Color Blue
                                case ItemQuality.Studded: itemTextColor = GameColor.Studded; break;//Color Purple
                            }
                            SpriteHelper.DrawTextCenteredWithShadow(spriteBatch, itemToUpgrade.FriendlyName, FontType.DialogsSmallSize8, new Vector2(X, Y + 33), width, Cache.Colors[itemTextColor]);

                            SpriteHelper.DrawTextCenteredWithShadow(spriteBatch, string.Format("Lvl.{0}", itemToUpgrade.Level), FontType.DialogsSmallSize8, new Vector2(X + 51, Y + 103), 40, Color.White);

                            if ((X + 51 < mouseX) && (X + 51 + 37 > mouseX) && (Y + 64 < mouseY) && (Y + 64 + 34 > mouseY))
                            {
                                highlightedItemIndex = ItemToUpgradeIndex;
                                highlightedItemLocation = new Vector2(54, 59);
                            }

                            if (itemToUpgrade.UpgradeType == ItemUpgradeType.Majestic && player.Majestics < itemToUpgrade.MajesticUpgradeCost())
                                spriteBatch.Draw(Cache.Interface[(int)SpriteId.DialogsV2 + 2].Texture, new Vector2(X + 48, Y + 58), Cache.Interface[(int)SpriteId.DialogsV2 + 2].Frames[2].GetRectangle(), Color.White * transparency); //Orange box
                            spriteBatch.Draw(Cache.Interface[(int)SpriteId.DialogsV2 + 2].Texture, new Vector2(X + 48, Y + 58), Cache.Interface[(int)SpriteId.DialogsV2 + 2].Frames[11].GetRectangle(), Color.White * transparency); //Green box

                            // get sprite frame
                            Rectangle f = Cache.Equipment[(int)SpriteId.ItemGround + itemToUpgrade.Sprite].Frames[itemToUpgrade.SpriteFrame].GetRectangle();
                            // get draw location offset from center of the slot
                            Rectangle draw = new Rectangle(X + 51 + 18 - (f.Width > 31 || f.Height > 31 ? 31 : f.Width) / 2,
                                                           Y + 62 + 17 - (f.Width > 31 || f.Height > 31 ? 31 : f.Height) / 2,
                                                           f.Width,
                                                           f.Height
                                                           );

                            itemTransparency = 1.0f;
                            if (Cache.DefaultState.Display.Mouse.IsDragging && clickedItemIndex == ItemToUpgradeIndex)
                            {
                                itemTransparency = 0.6f;
                                spriteBatch.Draw(Cache.BackgroundTexture, new Rectangle(X + 51 + 5, Y + 62 + 5, 27, 24), f, Color.Black * 0.5f);
                            }


                            if (f.Width > 31 || f.Height > 31)
                                spriteBatch.Draw(Cache.Equipment[(int)SpriteId.ItemGround + itemToUpgrade.Sprite].Texture, new Rectangle(draw.X, draw.Y, 31, 31), f, ((itemToUpgrade.ColorType == GameColor.Custom) ? SpriteHelper.ColourFromArgb(itemToUpgrade.Colour) : Cache.Colors[itemToUpgrade.ColorType]) * itemTransparency);
                            else spriteBatch.Draw(Cache.Equipment[(int)SpriteId.ItemGround + itemToUpgrade.Sprite].Texture, new Vector2(draw.X, draw.Y), f, ((itemToUpgrade.ColorType == GameColor.Custom) ? SpriteHelper.ColourFromArgb(itemToUpgrade.Colour) : Cache.Colors[itemToUpgrade.ColorType]) * itemTransparency);

                            // endurance as a percentage over the top of the item image
                            double endurance = (((double)itemToUpgrade.Endurance / (double)itemToUpgrade.MaximumEndurance) * 100);
                            if (itemToUpgrade.IsBroken)
                                SpriteHelper.DrawTextCenteredWithShadow(spriteBatch, "Broken", FontType.DialogsSmallerSize7, new Vector2(X + 51, Y + 84), 40, Cache.Colors[GameColor.Enemy]);
                            else SpriteHelper.DrawTextCenteredWithShadow(spriteBatch, ((int)Math.Max(1, endurance)).ToString() + "%", FontType.DialogsSmallerSize7, new Vector2(X + 51, Y + 84), 40, (endurance < 100 ? Cache.Colors[GameColor.Enemy] : Color.White));
                        }

                        //Majestic count
                        SpriteHelper.DrawTextWithShadow(spriteBatch, player.Majestics.ToString(), FontType.DialogsSmallSize8, new Vector2(X + 182, Y + 65), Color.White * transparency);

                        SpriteHelper.DrawTextWithShadow(spriteBatch, "Total Majestics: ", FontType.DialogsSmallSize8, new Vector2(X + 104, Y + 65), Cache.Colors[GameColor.Orange]);
                        SpriteHelper.DrawTextWithShadow(spriteBatch, "Upgrade Cost: ", FontType.DialogsSmallSize8, new Vector2(X + 104, Y + 80), Cache.Colors[GameColor.Orange]);

                        if (itemToUpgrade != null)
                        {
                            bool ready = true;
                            
                            //Item majestic cost
                            if (player.Majestics < itemToUpgrade.MajesticUpgradeCost())
                            {
                                SpriteHelper.DrawTextWithShadow(spriteBatch, itemToUpgrade.MajesticUpgradeCost().ToString(), FontType.DialogsSmallSize8, new Vector2(X + 182, Y + 80), Cache.Colors[GameColor.Enemy]);
                            }
                            else
                            {
                                SpriteHelper.DrawTextWithShadow(spriteBatch, itemToUpgrade.MajesticUpgradeCost().ToString(), FontType.DialogsSmallSize8, new Vector2(X + 182, Y + 80), Cache.Colors[GameColor.Friendly]);
                            }

                            if (player.Majestics < itemToUpgrade.MajesticUpgradeCost())
                            {
                                SpriteHelper.DrawTextCenteredWithShadow(spriteBatch, string.Format("Majestics Needed: +{0}", itemToUpgrade.MajesticUpgradeCost() - player.Majestics), FontType.DialogsSmallSize8, new Vector2(X, Y + 129), width, Cache.Colors[GameColor.Enemy]);
                                ready = false;
                            }

                            if (ready)
                            {
                                SpriteHelper.DrawTextCenteredWithShadow(spriteBatch, "Item is ready to upgrade", FontType.DialogsSmallSize8, new Vector2(X, Y + 123), width, Cache.Colors[GameColor.Friendly]);
                                SpriteHelper.DrawTextCenteredWithShadow(spriteBatch, string.Format("Chance: {0}%", itemToUpgrade.UpgradeChance().ToString("##.##")), FontType.DialogsSmallSize8, new Vector2(X, Y + 137), width, Color.White);
                            }
                        }
                        else
                        {
                            SpriteHelper.DrawTextWithShadow(spriteBatch, "None", FontType.DialogsSmallSize8, new Vector2(X + 182, Y + 80), Cache.Colors[GameColor.Enemy]);
                            SpriteHelper.DrawTextCenteredWithShadow(spriteBatch, "Item: None", FontType.DialogsSmallSize8, new Vector2(X, Y + 33), width, Cache.Colors[GameColor.Orange]);
                            SpriteHelper.DrawTextCenteredWithShadow(spriteBatch, "Add item", FontType.DialogsSmallSize8, new Vector2(X, Y + 122), width, Cache.Colors[GameColor.Orange]);
                        }

                        //Upgrade
                        if (Utility.IsSelected(mouseX, mouseY, X + 50, Y + 157, X + 50 + 65, Y + 157 + 33))
                        {
                            spriteBatch.Draw(Cache.Interface[(int)SpriteId.DialogsV2 + 5].Texture, new Vector2(X + 50, Y + 157), Cache.Interface[(int)SpriteId.DialogsV2 + 5].Frames[16].GetRectangle(), Color.White * transparency);
                            SpriteHelper.DrawTextCenteredWithShadow(spriteBatch, "Upgrade", FontType.GeneralSize10, new Vector2(X + 50, Y + 160), 65, Color.White);
                        }
                        else
                        {
                            spriteBatch.Draw(Cache.Interface[(int)SpriteId.DialogsV2 + 5].Texture, new Vector2(X + 50, Y + 157), Cache.Interface[(int)SpriteId.DialogsV2 + 5].Frames[15].GetRectangle(), Color.White * transparency);
                            SpriteHelper.DrawTextCenteredWithShadow(spriteBatch, "Upgrade", FontType.GeneralSize10, new Vector2(X + 50, Y + 160), 65, Color.White);
                        }

                        //Cancel
                        if (Utility.IsSelected(mouseX, mouseY, X + 130, Y + 157, X + 130 + 65, Y + 157 + 33))
                        {
                            spriteBatch.Draw(Cache.Interface[(int)SpriteId.DialogsV2 + 5].Texture, new Vector2(X + 130, Y + 157), Cache.Interface[(int)SpriteId.DialogsV2 + 5].Frames[16].GetRectangle(), Color.White * transparency);
                            SpriteHelper.DrawTextCenteredWithShadow(spriteBatch, "Cancel", FontType.GeneralSize10, new Vector2(X + 130, Y + 160), 65, Color.White);
                        }
                        else
                        {
                            spriteBatch.Draw(Cache.Interface[(int)SpriteId.DialogsV2 + 5].Texture, new Vector2(X + 130, Y + 157), Cache.Interface[(int)SpriteId.DialogsV2 + 5].Frames[15].GetRectangle(), Color.White * transparency);
                            SpriteHelper.DrawTextCenteredWithShadow(spriteBatch, "Cancel", FontType.GeneralSize10, new Vector2(X + 130, Y + 160), 65, Color.White);
                        }

                        /* TITLE */
                        spriteBatch.Draw(Cache.Interface[(int)SpriteId.DialogsV2].Texture, new Vector2(X + 48, Y - 16), Cache.Interface[(int)SpriteId.DialogsV2].Frames[28].GetRectangle(), Color.White * transparency);
                        SpriteHelper.DrawTextCenteredWithShadow(spriteBatch, "Majestic", FontType.DisplayNameSize13Spacing1, new Vector2(X, Y - 9), width, Cache.Colors[GameColor.Orange]);

                        if (highlightedItemIndex != -1 && player.Inventory[highlightedItemIndex] != null)
                        {
                            selectedItemIndex = highlightedItemIndex;
                            if (clickedItemIndex == -1) selectedItemLocation = highlightedItemLocation;
                        }
                        else selectedItemIndex = -1;
                        break;
                    }
                case DialogBoxState.Success:
                    {
                        upgradedItem = null;
                        if (upgradedItemIndex != -1 && player.Inventory[upgradedItemIndex] != null)
                            upgradedItem = player.Inventory[upgradedItemIndex];

                        if (upgradedItem == null)
                        {
                            if ((X + 51 < mouseX) && (X + 51 + 37 > mouseX) && (Y + 64 < mouseY) && (Y + 64 + 34 > mouseY))
                                spriteBatch.Draw(Cache.Interface[(int)SpriteId.DialogsV2 + 2].Texture, new Vector2(X + 48, Y + 58), Cache.Interface[(int)SpriteId.DialogsV2 + 2].Frames[2].GetRectangle(), Color.White * transparency);
                            else spriteBatch.Draw(Cache.Interface[(int)SpriteId.DialogsV2 + 2].Texture, new Vector2(X + 48, Y + 58), Cache.Interface[(int)SpriteId.DialogsV2 + 2].Frames[7].GetRectangle(), Color.White * transparency);
                        }
                        else
                        {

                            if ((X + 51 < mouseX) && (X + 51 + 37 > mouseX) && (Y + 64 < mouseY) && (Y + 64 + 34 > mouseY))
                            {
                                highlightedItemIndex = upgradedItemIndex;
                                highlightedItemLocation = new Vector2(54, 59);
                            }

                            spriteBatch.Draw(Cache.Interface[(int)SpriteId.DialogsV2 + 2].Texture, new Vector2(X + 48, Y + 58), Cache.Interface[(int)SpriteId.DialogsV2 + 2].Frames[11].GetRectangle(), Color.White * transparency); //Green box

                            // get sprite frame
                            Rectangle f = Cache.Equipment[(int)SpriteId.ItemGround + upgradedItem.Sprite].Frames[upgradedItem.SpriteFrame].GetRectangle();
                            // get draw location offset from center of the slot
                            Rectangle draw = new Rectangle(X + 51 + 18 - (f.Width > 31 || f.Height > 31 ? 31 : f.Width) / 2,
                                                           Y + 62 + 17 - (f.Width > 31 || f.Height > 31 ? 31 : f.Height) / 2,
                                                           f.Width,
                                                           f.Height
                                                           );

                            spriteBatch.Draw(Cache.BackgroundTexture, new Rectangle(X + 48 + 8, Y + 58 + 10, 27, 24), f, Cache.Colors[GameColor.Friendly] * 0.1f);

                            if (f.Width > 31 || f.Height > 31)
                                spriteBatch.Draw(Cache.Equipment[(int)SpriteId.ItemGround + upgradedItem.Sprite].Texture, new Rectangle(draw.X, draw.Y, 31, 31), f, SpriteHelper.ColourFromArgb(upgradedItem.Colour));
                            else spriteBatch.Draw(Cache.Equipment[(int)SpriteId.ItemGround + upgradedItem.Sprite].Texture, new Vector2(draw.X, draw.Y), f, SpriteHelper.ColourFromArgb(upgradedItem.Colour));

                            SpriteHelper.DrawTextCenteredWithShadow(spriteBatch, "Upgrade succeeded!", FontType.DialogsSmallSize8, new Vector2(X + 50, Y + 65), width, Cache.Colors[GameColor.Friendly]);


                            GameColor itemTextColor = GameColor.None;
                            switch (upgradedItem.Quality)
                            {
                                default:
                                case ItemQuality.None: itemTextColor = GameColor.Normal; break;//Color Orage //TODO expand on this for items that don't get quality level
                                case ItemQuality.Flimsy: itemTextColor = GameColor.Flimsy; break;//Color White
                                case ItemQuality.Sturdy: itemTextColor = GameColor.Sturdy; break;//Color Green
                                case ItemQuality.Reinforced: itemTextColor = GameColor.Reinforced; break;//Color Blue
                                case ItemQuality.Studded: itemTextColor = GameColor.Studded; break;//Color Purple
                            }
                            SpriteHelper.DrawTextCenteredWithShadow(spriteBatch, upgradedItem.FriendlyName, FontType.DialogsSmallSize8, new Vector2(X, Y + 33), width, Cache.Colors[itemTextColor]);

                            if (upgradedItem.Level == Globals.MaximumItemLevel)
                            {
                                SpriteHelper.DrawTextCenteredWithShadow(spriteBatch, "Item is max level", FontType.DialogsSmallSize8, new Vector2(X + 50, Y + 80), width, Cache.Colors[GameColor.Orange]);
                            }
                            else
                            {
                                SpriteHelper.DrawTextCenteredWithShadow(spriteBatch, string.Format("Item is now level {0}", upgradedItem.Level).Trim(), FontType.DialogsSmallSize8, new Vector2(X + 50, Y + 80), width, Cache.Colors[GameColor.Orange]);
                            }
                        }

                        if (Utility.IsSelected(mouseX, mouseY, X + 90, Y + 157, X + 90 + 65, Y + 157 + 33))
                        {
                            spriteBatch.Draw(Cache.Interface[(int)SpriteId.DialogsV2 + 5].Texture, new Vector2(X + 90, Y + 157), Cache.Interface[(int)SpriteId.DialogsV2 + 5].Frames[16].GetRectangle(), Color.White * transparency);
                            SpriteHelper.DrawTextCenteredWithShadow(spriteBatch, "Ok", FontType.GeneralSize10, new Vector2(X + 90, Y + 160), 65, Color.White);
                        }
                        else
                        {
                            spriteBatch.Draw(Cache.Interface[(int)SpriteId.DialogsV2 + 5].Texture, new Vector2(X + 90, Y + 157), Cache.Interface[(int)SpriteId.DialogsV2 + 5].Frames[15].GetRectangle(), Color.White * transparency);
                            SpriteHelper.DrawTextCenteredWithShadow(spriteBatch, "Ok", FontType.GeneralSize10, new Vector2(X + 90, Y + 160), 65, Color.White);
                        }

                        selectedItemIndex = -1;

                        /* TITLE */
                        spriteBatch.Draw(Cache.Interface[(int)SpriteId.DialogsV2].Texture, new Vector2(X + 48, Y - 16), Cache.Interface[(int)SpriteId.DialogsV2].Frames[28].GetRectangle(), Color.White * transparency);
                        SpriteHelper.DrawTextCenteredWithShadow(spriteBatch, "Upgrade", FontType.DisplayNameSize13Spacing1, new Vector2(X, Y - 9), width, Cache.Colors[GameColor.Orange]);
                        break;
                    }
                case DialogBoxState.Fail: //TODO gets skipped for some reason randomly?
                    {                                            
                        upgradedItem = null;
                        if (upgradedItemIndex != -1 && player.Inventory[upgradedItemIndex] != null)
                            upgradedItem = player.Inventory[upgradedItemIndex];

                        if (upgradedItem == null)
                        {
                            if ((X + 51 < mouseX) && (X + 51 + 37 > mouseX) && (Y + 64 < mouseY) && (Y + 64 + 34 > mouseY))
                                spriteBatch.Draw(Cache.Interface[(int)SpriteId.DialogsV2 + 2].Texture, new Vector2(X + 48, Y + 58), Cache.Interface[(int)SpriteId.DialogsV2 + 2].Frames[2].GetRectangle(), Color.White * transparency);
                            else spriteBatch.Draw(Cache.Interface[(int)SpriteId.DialogsV2 + 2].Texture, new Vector2(X + 48, Y + 58), Cache.Interface[(int)SpriteId.DialogsV2 + 2].Frames[7].GetRectangle(), Color.White * transparency);
                        }
                        else
                        {

                            if ((X + 51 < mouseX) && (X + 51 + 37 > mouseX) && (Y + 64 < mouseY) && (Y + 64 + 34 > mouseY))
                            {
                                highlightedItemIndex = upgradedItemIndex;
                                highlightedItemLocation = new Vector2(54, 59);
                            }

                            spriteBatch.Draw(Cache.Interface[(int)SpriteId.DialogsV2 + 2].Texture, new Vector2(X + 48, Y + 58), Cache.Interface[(int)SpriteId.DialogsV2 + 2].Frames[2].GetRectangle(), Color.White * transparency); //Red box = 22

                            // get sprite frame
                            Rectangle f = Cache.Equipment[(int)SpriteId.ItemGround + upgradedItem.Sprite].Frames[upgradedItem.SpriteFrame].GetRectangle();
                            // get draw location offset from center of the slot
                            Rectangle draw = new Rectangle(X + 51 + 18 - (f.Width > 31 || f.Height > 31 ? 31 : f.Width) / 2,
                                                           Y + 62 + 17 - (f.Width > 31 || f.Height > 31 ? 31 : f.Height) / 2,
                                                           f.Width,
                                                           f.Height
                                                           );

                            spriteBatch.Draw(Cache.BackgroundTexture, new Rectangle(X + 48 + 8, Y + 58 + 10, 27, 24), f, Cache.Colors[GameColor.Enemy] * 0.1f);

                            if (f.Width > 31 || f.Height > 31)
                                spriteBatch.Draw(Cache.Equipment[(int)SpriteId.ItemGround + upgradedItem.Sprite].Texture, new Rectangle(draw.X, draw.Y, 31, 31), f, ((upgradedItem.ColorType == GameColor.Custom) ? SpriteHelper.ColourFromArgb(upgradedItem.Colour) : Cache.Colors[upgradedItem.ColorType]) * transparency);
                            else spriteBatch.Draw(Cache.Equipment[(int)SpriteId.ItemGround + upgradedItem.Sprite].Texture, new Vector2(draw.X, draw.Y), f, ((upgradedItem.ColorType == GameColor.Custom) ? SpriteHelper.ColourFromArgb(upgradedItem.Colour) : Cache.Colors[upgradedItem.ColorType]) * transparency);


                            GameColor itemTextColor = GameColor.None;
                            switch (upgradedItem.Quality)
                            {
                                default:
                                case ItemQuality.None: itemTextColor = GameColor.Normal; break;//Color Orage //TODO expand on this for items that don't get quality level
                                case ItemQuality.Flimsy: itemTextColor = GameColor.Flimsy; break;//Color White
                                case ItemQuality.Sturdy: itemTextColor = GameColor.Sturdy; break;//Color Green
                                case ItemQuality.Reinforced: itemTextColor = GameColor.Reinforced; break;//Color Blue
                                case ItemQuality.Studded: itemTextColor = GameColor.Studded; break;//Color Purple
                            }

                            SpriteHelper.DrawTextCenteredWithShadow(spriteBatch, itemToUpgrade.FriendlyName, FontType.DialogsSmallSize8, new Vector2(X, Y + 33), width, Cache.Colors[itemTextColor]);

                            SpriteHelper.DrawTextCenteredWithShadow(spriteBatch, "Upgrade failed!", FontType.DialogsSmallSize8, new Vector2(X + 50, Y + 65), width, Cache.Colors[GameColor.Enemy]);
                            SpriteHelper.DrawTextCenteredWithShadow(spriteBatch, upgradedItem.FriendlyName, FontType.DialogsSmallSize8, new Vector2(X, Y + 33), width, Cache.Colors[itemTextColor]);
                            SpriteHelper.DrawTextCenteredWithShadow(spriteBatch, "Item is now broken.", FontType.DialogsSmallSize8, new Vector2(X + 50, Y + 80), width, Cache.Colors[GameColor.Orange]);
                        }

                        if (Utility.IsSelected(mouseX, mouseY, X + 90, Y + 157, X + 90 + 65, Y + 157 + 33))
                        {
                            spriteBatch.Draw(Cache.Interface[(int)SpriteId.DialogsV2 + 5].Texture, new Vector2(X + 90, Y + 157), Cache.Interface[(int)SpriteId.DialogsV2 + 5].Frames[16].GetRectangle(), Color.White * transparency);
                            SpriteHelper.DrawTextCenteredWithShadow(spriteBatch, "Ok", FontType.GeneralSize10, new Vector2(X + 90, Y + 160), 65, Color.White);
                        }
                        else
                        {
                            spriteBatch.Draw(Cache.Interface[(int)SpriteId.DialogsV2 + 5].Texture, new Vector2(X + 90, Y + 157), Cache.Interface[(int)SpriteId.DialogsV2 + 5].Frames[15].GetRectangle(), Color.White * transparency);
                            SpriteHelper.DrawTextCenteredWithShadow(spriteBatch, "Ok", FontType.GeneralSize10, new Vector2(X + 90, Y + 160), 65, Color.White);
                        }

                        selectedItemIndex = -1;

                        /* TITLE */
                        spriteBatch.Draw(Cache.Interface[(int)SpriteId.DialogsV2].Texture, new Vector2(X + 48, Y - 16), Cache.Interface[(int)SpriteId.DialogsV2].Frames[28].GetRectangle(), Color.White * transparency);
                        SpriteHelper.DrawTextCenteredWithShadow(spriteBatch, "Upgrade", FontType.DisplayNameSize13Spacing1, new Vector2(X, Y - 9), width, Cache.Colors[GameColor.Orange]);
                        break;
                    }
            }
                              
            ///* DRAGGED ITEM */
            //if (Cache.DefaultState.Display.Mouse.IsDragging && clickedItemIndex != -1 && player.Inventory[clickedItemIndex] != null)
            //    ((MainGame)Cache.DefaultState).SetDraggedItem(DraggedType.Inventory, clickedItemIndex);

            /* POPUP */
            if (highlightedItemIndex >= Globals.MaximumEquipment && highlightedItemIndex < Globals.MaximumTotalItems)
            {
                /* ITEM DETAILS */
                if (player.Inventory[highlightedItemIndex] != null)
                    ((MainGame)Cache.DefaultState).SetItemPopup(Type, player.Inventory[highlightedItemIndex], highlightedItemLocation, X, Y);
            }

            if (frame != null && Utility.IsSelected(mouseX, mouseY, X, Y, X + frame.Width, Y + frame.Height))
                return true;
            else return false;
        }

        public void Update(GameTime gameTime)
        {
            player = ((MainGame)Cache.DefaultState).Player;
            ItemToUpgradeIndex = player.Inventory.UpgradeItemIndex;
            UpgradeIngredientIndex = player.Inventory.UpgradeIngredientIndex;

            mouseX = (int)Cache.DefaultState.Display.Mouse.X;
            mouseY = (int)Cache.DefaultState.Display.Mouse.Y;
            X = config.X;
            Y = config.Y;
            transparency = ((Cache.GameSettings.TransparentDialogs || config.Hidden) ? 0.5F : 1F);
        }

        public void LeftClicked()
        {
            switch (config.State)
            {
                case DialogBoxState.Normal:
                    {
                        //Majestic
                        if (Utility.IsSelected(mouseX, mouseY, X + 19, Y + 157, X + 19 + 65, Y + 157 + 33))
                        {
                            Toggle(DialogBoxState.MajesticUpgrade);
                        }
                        //Normal
                        if (Utility.IsSelected(mouseX, mouseY, X + 90, Y + 157, X + 90 + 65, Y + 157 + 33))
                        {
                            Toggle(DialogBoxState.IngredientUpgrade);
                        }
                        //Hero
                        if (Utility.IsSelected(mouseX, mouseY, X + 161, Y + 157, X + 161 + 65, Y + 157 + 33))
                        {
                            Toggle(DialogBoxState.HeroUpgrade);
                        }
                        break;
                    }
                case DialogBoxState.HeroUpgrade:
                case DialogBoxState.MajesticUpgrade:
                case DialogBoxState.IngredientUpgrade:
                    {
                        //Upgrade
                        if (Utility.IsSelected(mouseX, mouseY, X + 50, Y + 157, X + 50 + 65, Y + 157 + 33))
                        {
                            upgradedItemIndex = ItemToUpgradeIndex;
                            ((MainGame)Cache.DefaultState).UpgradeItem();
                        }

                        //Cancel
                        if (Utility.IsSelected(mouseX, mouseY, X + 130, Y + 157, X + 130 + 65, Y + 157 + 33))
                        {
                            ((MainGame)Cache.DefaultState).ClearUpgradeItems();
                            Toggle(DialogBoxState.Normal);
                        }
                        break;
                    }
                case DialogBoxState.Success:
                    {
                        if (Utility.IsSelected(mouseX, mouseY, X + 90, Y + 157, X + 90 + 65, Y + 157 + 33))
                        {
                            if (ItemToUpgradeIndex != -1 && player.Inventory[ItemToUpgradeIndex] != null)
                            {
                                switch (player.Inventory[ItemToUpgradeIndex].UpgradeType)
                                {
                                    case ItemUpgradeType.Hero: Toggle(DialogBoxState.HeroUpgrade); break;
                                    case ItemUpgradeType.Majestic: Toggle(DialogBoxState.MajesticUpgrade); break;
                                    case ItemUpgradeType.Merien: 
                                    case ItemUpgradeType.Xelima: Toggle(DialogBoxState.IngredientUpgrade); break;
                                    default: Config.State = DialogBoxState.Normal; break;
                                }
                            }
                            else { Toggle(DialogBoxState.Normal); }
                            upgradedItemIndex = -1;
                        }
                        break;
                    }
                case DialogBoxState.Fail:
                    {
                        if (Utility.IsSelected(mouseX, mouseY, X + 90, Y + 157, X + 90 + 65, Y + 157 + 33))
                        {
                            Toggle(DialogBoxState.Normal);
                            upgradedItemIndex = -1;
                        }
                        break;
                    }
                default: break;
            }

            if (clickedItemIndex != -1 && player.Inventory[clickedItemIndex] != null)
                ((MainGame)Cache.DefaultState).SetDraggedItem(DraggedType.Inventory, clickedItemIndex);
        }

        public void LeftDoubleClicked(ref SelectionMode selectionMode, ref int selectionModeId)
        {
            if (SelectedItemIndex != -1)
            {
                switch(config.State)
                {
                    case DialogBoxState.Success:
                    case DialogBoxState.Fail: break;
                    default: { ((MainGame)Cache.DefaultState).ChangeUpgradeItem(selectedItemIndex); break; }
                }
            }
        }

        public void LeftHeld()
        {
        }

        public void LeftDragged()
        {
        }

        public void LeftReleased(GameDialogBoxType highlightedDialogBox, int highlightedDialogBoxItemIndex)
        {
            switch (highlightedDialogBox)
            {
                case GameDialogBoxType.UpgradeV2: { break; }
                default: { ((MainGame)Cache.DefaultState).ChangeUpgradeItem(clickedItemIndex); break; }
            }

            selectedItemIndexOffsetX = 0;
            selectedItemIndexOffsetY = 0;
        }

        public void RightClicked() { }

        public void RightHeld() { }

        public void RightReleased() { }

        public void Scroll(int direction)
        {
            if (config.Page - direction > config.MaxPages) config.Page = 1;
            else if (config.Page - direction < 1) config.Page = config.MaxPages;
            else config.Page -= direction;

            highlightedItemIndex = -1; // clear popup
        }

        public void OffsetLocation(int x, int y)
        {
            config.X += x;
            config.Y += y;
        }

        public void Toggle(DialogBoxState state = DialogBoxState.Normal)
        {
            if (config.Visible && state == config.State) Hide();
            else
            {
                config.State = state;
                Show();
            }
        }

        public void Show()
        {
            if (!config.AlwaysVisible && Cache.DefaultState != null)
                ((IGameState)Cache.DefaultState).BringToFront(Type);
            highlightedItemIndex = -1;
            config.Hidden = false;
            //((MainGame)Cache.DefaultState).ClearUpgradeItems();
        }

        public void Hide()
        {
            if (!config.AlwaysVisible && Cache.DefaultState != null)
            {
                ((MainGame)Cache.DefaultState).DialogBoxDrawOrder.Remove(Type);
                ((IGameState)Cache.DefaultState).ClickedDialogBox = GameDialogBoxType.None;
            }
            config.Hidden = true;

            ((MainGame)Cache.DefaultState).ClearUpgradeItems();
        }

        private void SetPagination()
        {

        }

        public void SetData(byte[] data)
        {

        }
    }
}
