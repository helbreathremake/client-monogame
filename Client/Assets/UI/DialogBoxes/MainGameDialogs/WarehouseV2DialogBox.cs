﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

using Helbreath.Common;
using Helbreath.Common.Assets;
using Client.Assets.State;
using Client.Assets.Game;

namespace Client.Assets.UI
{
    public class WarehouseV2DialogBox : IGameDialogBox
    {
        // IDIALOG
        private int clickedItemIndex = -1; // clicked item
        private int selectedItemIndex = -1; // mouse over item
        private int highlightedItemIndex = -1; // chosen item (non volatile)
        private Vector2 selectedLocation;
        private Vector2 highlightedItemLocation; // chose item location (non volatile)
        private int selectedItemIndexOffsetX;
        private int selectedItemIndexOffsetY;

        private GameDialogBoxConfiguration config;

        //Base graphic info
        private int sprite = (int)SpriteId.DialogsV2 + 6; //Main texture
        private int spriteFrame = 3; //rectangle in texutre
        AnimationFrame frame;

        public int SelectedItemIndex { get { return selectedItemIndex; } set { selectedItemIndex = value; } }
        public int ClickedItemIndex { get { return clickedItemIndex; } set { clickedItemIndex = value; } }
        public int HighlightedItemIndex { get { return highlightedItemIndex; } set { highlightedItemIndex = value; } }
        public int SelectedItemIndexOffsetX { get { return selectedItemIndexOffsetX; } set { selectedItemIndexOffsetX = value; } }
        public int SelectedItemIndexOffsetY { get { return selectedItemIndexOffsetY; } set { selectedItemIndexOffsetY = value; } }
        public GameDialogBoxConfiguration Config { get { return config; } set { config = value; } }
        public GameDialogBoxType Type { get { return GameDialogBoxType.WarehouseV2; } }

        int mouseX;
        int mouseY;
        int X;
        int Y;
        float transparency;
        Player player;

        public WarehouseV2DialogBox()
        {
            config = new GameDialogBoxConfiguration(Type, Cache.GameSettings.Resolution);
            this.highlightedItemIndex = selectedItemIndex = clickedItemIndex = -1;
            frame = Cache.Interface[sprite].Frames[spriteFrame];
            mouseX = (int)Cache.DefaultState.Display.Mouse.X;
            mouseY = (int)Cache.DefaultState.Display.Mouse.Y;
            X = config.X;
            Y = config.Y;
            transparency = ((Cache.GameSettings.TransparentDialogs || config.Hidden) ? 0.5F : 1F);
            player = ((MainGame)Cache.DefaultState).Player;
        }

        public WarehouseV2DialogBox(GameDialogBoxConfiguration config)
        {
            this.config = config;
            this.highlightedItemIndex = selectedItemIndex = clickedItemIndex = -1;
            frame = Cache.Interface[sprite].Frames[spriteFrame];
            mouseX = (int)Cache.DefaultState.Display.Mouse.X;
            mouseY = (int)Cache.DefaultState.Display.Mouse.Y;
            X = config.X;
            Y = config.Y;
            transparency = ((Cache.GameSettings.TransparentDialogs || config.Hidden) ? 0.5F : 1F);
            player = ((MainGame)Cache.DefaultState).Player;
        }

        public bool Draw(SpriteBatch spriteBatch, GameTime gameTime)
        {
            if (config.Hidden)
                if (!config.AlwaysVisible) return false; // dont draw dialogs if not a HUD item
                else if (Cache.GameSettings.LockedDialogs) return false;  // show HUD items when HUD is unlocked (transparent)

            if (((MainGame)Cache.DefaultState).Player.IsCasting && Cache.GameSettings.HideDialogDuringCasting) return false;

            highlightedItemIndex = selectedItemIndex = -1;

            switch (config.State)
            {
                case DialogBoxState.Normal: //TODO fix dragging item
                    spriteBatch.Draw(Cache.Interface[sprite].Texture, new Vector2(X, Y), frame.GetRectangle(), Color.White * transparency);

                    /* GRID HIGHLIGHTS */
                    int slotId = 0;
                    for (int slotY = 0; slotY < 9; slotY++)
                        for (int slotX = 0; slotX < 11; slotX++)
                        {
                            if ((X + 39 + (slotX * 38) < mouseX) && (X + 39 + (slotX * 38) + 37 > mouseX) && (Y + 64 + (slotY * 35) < mouseY) && (Y + 64 + (slotY * 35) + 34 > mouseY))
                            {
                                spriteBatch.Draw(Cache.Interface[(int)SpriteId.DialogsV2 + 2].Texture, new Vector2(X + 39 + (slotX * 38), Y + 64 + (slotY * 35)), Cache.Interface[(int)SpriteId.DialogsV2 + 2].Frames[7].GetRectangle(), Color.White * transparency);
                                highlightedItemIndex = slotId;
                                highlightedItemLocation = new Vector2(42 + (slotX * 38), 69 + (slotY * 35));
                            }
                            slotId++;
                        }
                    if (highlightedItemIndex != -1 && player.Warehouse[highlightedItemIndex] != null) { selectedItemIndex = highlightedItemIndex; selectedLocation = highlightedItemLocation; }
                    else selectedItemIndex = -1;

                    if (player.Warehouse.Count > 0)
                    {
                        int slotX = 0; int slotY = 0;
                        for (int itemIndex = 0; itemIndex < Globals.MaximumWarehouseTabItems; itemIndex++)
                        {
                            if (player.Warehouse[itemIndex] != null)
                            {
                                Item item = player.Warehouse[itemIndex];

                                // get sprite frame
                                Rectangle f = Cache.Equipment[(int)SpriteId.ItemGround + item.Sprite].Frames[item.SpriteFrame].GetRectangle();
                                // get draw location offset from center of the slot
                                Rectangle draw = new Rectangle(((clickedItemIndex == itemIndex) ? selectedItemIndexOffsetX : 0) + X + 42 + (slotX * 38) + 18 - (f.Width > 31 || f.Height > 31 ? 31 : f.Width) / 2,
                                                               ((clickedItemIndex == itemIndex) ? selectedItemIndexOffsetY : 0) + Y + 69 + (slotY * 35) + 17 - (f.Width > 31 || f.Height > 31 ? 31 : f.Height) / 2,
                                                               f.Width,
                                                               f.Height
                                                               );

                                if (f.Width > 31 || f.Height > 31)
                                    spriteBatch.Draw(Cache.Equipment[(int)SpriteId.ItemGround + item.Sprite].Texture, new Rectangle(draw.X, draw.Y, 31, 31), f, (item.ColorType == GameColor.Custom) ? SpriteHelper.ColourFromArgb(item.Colour) : Cache.Colors[item.ColorType]);
                                else spriteBatch.Draw(Cache.Equipment[(int)SpriteId.ItemGround + item.Sprite].Texture, new Vector2(draw.X, draw.Y), f, (item.ColorType == GameColor.Custom) ? SpriteHelper.ColourFromArgb(item.Colour) : Cache.Colors[item.ColorType]);

                                if (item.Count > 1)
                                    SpriteHelper.DrawTextRightWithShadow(spriteBatch, string.Format("x{0}", item.Count.ToString()), FontType.DialogsSmallerSize7, new Vector2(((clickedItemIndex == itemIndex) ? selectedItemIndexOffsetX : 0) + X + 42 + (slotX * 38), ((clickedItemIndex == itemIndex) ? selectedItemIndexOffsetY : 0) + Y + 64 + (slotY * 35) + 24), 32, Cache.Colors[GameColor.Orange]);
                                else if (item.Level > 1)
                                    SpriteHelper.DrawTextRightWithShadow(spriteBatch, string.Format("Lvl.{0}", item.Level), FontType.DialogsSmallerSize7, new Vector2(((clickedItemIndex == itemIndex) ? selectedItemIndexOffsetX : 0) + X + 42 + (slotX * 38), ((clickedItemIndex == itemIndex) ? selectedItemIndexOffsetY : 0) + Y + 64 + (slotY * 35) + 24), 32, Cache.Colors[GameColor.Orange]);
                            }
                            slotX++;
                            if (slotX > 10) { slotX = 0; slotY++; }
                        }
                    }
                    break;
            }

            /* TITLE */
            spriteBatch.Draw(Cache.Interface[(int)SpriteId.DialogsV2].Texture, new Vector2(X + 175, Y - 16), Cache.Interface[(int)SpriteId.DialogsV2].Frames[28].GetRectangle(), Color.White * transparency);
            SpriteHelper.DrawTextCenteredWithShadow(spriteBatch, "Warehouse", FontType.DisplayNameSize13Spacing1, new Vector2(X + 185, Y - 9), 140, Cache.Colors[GameColor.Orange]);

            ///* DRAGGED ITEM */
            //if (Cache.DefaultState.Display.Mouse.IsDragging && clickedItemIndex != -1 && player.Inventory[clickedItemIndex] != null)
            //    ((MainGame)Cache.DefaultState).SetDraggedItem(DraggedType.Warehouse, clickedItemIndex);
            //{
            //    Item clickedItem = player.Inventory[clickedItemIndex];

            //    // get sprite frame
            //    Rectangle f = Cache.Equipment[(int)SpriteId.ItemGround + clickedItem.Sprite].Frames[clickedItem.SpriteFrame].GetRectangle();
            //    // get draw location offset from center of the slot
            //    Rectangle draw = new Rectangle(
            //        mouseX - (f.Width > 31 || f.Height > 31 ? 31 : f.Width) / 2,
            //        mouseY - (f.Width > 31 || f.Height > 31 ? 31 : f.Height) / 2,
            //        f.Width,
            //        f.Height
            //        );

            //    if (f.Width > 31 || f.Height > 31)
            //        spriteBatch.DrawItemPopup(Cache.Equipment[(int)SpriteId.ItemGround + clickedItem.Sprite].Texture, new Rectangle(draw.X, draw.Y, 31, 31), f, SpriteHelper.ColourFromArgb(clickedItem.Colour));
            //    else spriteBatch.DrawItemPopup(Cache.Equipment[(int)SpriteId.ItemGround + clickedItem.Sprite].Texture, new Vector2(draw.X, draw.Y), f, SpriteHelper.ColourFromArgb(clickedItem.Colour));

            //    if (clickedItem.Count > 1)
            //        SpriteHelper.DrawTextRightWithShadow(spriteBatch, string.Format("x{0}", clickedItem.Count.ToString()), FontType.DialogsV2Smaller, new Vector2(mouseX - 18, mouseY + 2), 32, Cache.DefaultState.Display.FontColours[FontType.DialogsV2Orange]);
            //    if (clickedItem.SetNumber > 0)
            //        SpriteHelper.DrawTextWithShadow(spriteBatch, string.Format("{0}", clickedItem.SetNumber.ToString()), FontType.DialogsV2Smaller, new Vector2(mouseX - 12, mouseY - 14), Cache.DefaultState.Display.FontColours[FontType.DialogsV2Orange]);
            //}

            /* POPUP */
            if (highlightedItemIndex >= 0 && highlightedItemIndex < Globals.MaximumWarehouseTabItems)
            {
                /* ITEM DETAILS */
                if (player.Warehouse[highlightedItemIndex] != null)
                    ((MainGame)Cache.DefaultState).SetItemPopup(Type, player.Warehouse[highlightedItemIndex], highlightedItemLocation, X, Y);
                    //ItemPopup.DrawItemPopup(spriteBatch, player.Warehouse[highlightedItemIndex], highlightedItemLocation, X, Y);
            }

            if (frame != null && Utility.IsSelected(mouseX, mouseY, X, Y, X + frame.Width, Y + frame.Height))
                return true;
            else return false;
        }

        public void Update(GameTime gameTime)
        {
            mouseX = (int)Cache.DefaultState.Display.Mouse.X;
            mouseY = (int)Cache.DefaultState.Display.Mouse.Y;
            X = config.X;
            Y = config.Y;
            transparency = ((Cache.GameSettings.TransparentDialogs || config.Hidden) ? 0.5F : 1F);
            player = ((MainGame)Cache.DefaultState).Player;
        }

        public void LeftClicked()
        {
            if (clickedItemIndex != -1 && player.Inventory[clickedItemIndex] != null)
                ((MainGame)Cache.DefaultState).SetDraggedItem(DraggedType.Warehouse, clickedItemIndex);
        }

        public void LeftDoubleClicked(ref SelectionMode selectionMode, ref int selectionModeId)
        {
            if (SelectedItemIndex != -1)
            {
                if (((MainGame)Cache.DefaultState).DialogBoxes[GameDialogBoxType.CharacterV2].Config.Visible || ((MainGame)Cache.DefaultState).DialogBoxes[GameDialogBoxType.InventoryV1].Config.Visible)
                    ((MainGame)Cache.DefaultState).ItemFromWarehouse(SelectedItemIndex, -1);
            }
        }

        public void LeftHeld()
        {
        }

        public void LeftDragged()
        {

        }

        public void LeftReleased(GameDialogBoxType highlightedDialogBox, int highlightedDialogBoxItemIndex)
        {
            switch (highlightedDialogBox)
            {
                case GameDialogBoxType.None: // TODO drop item from warehouse
                    break;
                case GameDialogBoxType.InventoryV1:
                case GameDialogBoxType.CharacterV2:
                    if (clickedItemIndex != -1)
                        ((MainGame)Cache.DefaultState).ItemFromWarehouse(clickedItemIndex, highlightedDialogBoxItemIndex);
                    highlightedItemIndex = -1;
                    break;
                case GameDialogBoxType.WarehouseV2:
                    if (clickedItemIndex != highlightedDialogBoxItemIndex)
                        ((MainGame)Cache.DefaultState).SwapWarehouseItem(clickedItemIndex, highlightedDialogBoxItemIndex);
                    highlightedItemIndex = -1;
                    break;
            }
        }

        public void RightClicked() { }

        public void RightHeld() { }

        public void RightReleased() { }

        public void Scroll(int direction)
        {
            if (config.Page - direction > config.MaxPages) config.Page = 1;
            else if (config.Page - direction < 1) config.Page = config.MaxPages;
            else config.Page -= direction;

            highlightedItemIndex = -1; // clear popup
        }

        public void OffsetLocation(int x, int y)
        {
            config.X += x;
            config.Y += y;
        }

        public void Toggle(DialogBoxState state = DialogBoxState.Normal)
        {
            if (config.Visible && state == config.State) Hide();
            else
            {
                config.State = state;
                Show();
            }
        }

        public void Show()
        {
            if (!config.AlwaysVisible && Cache.DefaultState != null)
                ((IGameState)Cache.DefaultState).BringToFront(Type);
            highlightedItemIndex = -1;
            config.Hidden = false;
        }

        public void Hide()
        {
            if (!config.AlwaysVisible && Cache.DefaultState != null)
            {
                ((IGameState)Cache.DefaultState).DialogBoxDrawOrder.Remove(Type);
                ((IGameState)Cache.DefaultState).ClickedDialogBox = GameDialogBoxType.None;
            }
            config.Hidden = true;
        }

        private void SetPagination()
        {

        }

        public void SetData(byte[] data)
        {

        }
    }
}
