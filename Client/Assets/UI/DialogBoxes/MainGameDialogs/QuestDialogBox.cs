﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

using Helbreath.Common;
using Helbreath.Common.Assets;
using Client.Assets.State;
using Client.Assets.Game;

namespace Client.Assets.UI
{
    public class QuestDialogBox : IGameDialogBox
    {
        int selectedItemIndex = -1;
        int clickedItemIndex = -1;
        int highlightedItemIndex = -1;
        int selectedItemIndexOffsetX; //Dragging items
        int selectedItemIndexOffsetY; //Dragging items
              
        GameDialogBoxConfiguration config;

        //Base graphic info
        int sprite = (int)SpriteId.DialogsV2 + 2; //Main texture
        int spriteFrame = 17; //rectangle in texutre
        AnimationFrame frame;

        public int SelectedItemIndex { get { return selectedItemIndex; } set { selectedItemIndex = value; } }
        public int ClickedItemIndex { get { return clickedItemIndex; } set { clickedItemIndex = value; } }
        public int HighlightedItemIndex { get { return highlightedItemIndex; } set { highlightedItemIndex = value; } }
        public int SelectedItemIndexOffsetX { get { return selectedItemIndexOffsetX; } set { selectedItemIndexOffsetX = value; } }
        public int SelectedItemIndexOffsetY { get { return selectedItemIndexOffsetY; } set { selectedItemIndexOffsetY = value; } }
        public GameDialogBoxConfiguration Config { get { return config; } set { config = value; } }
        public GameDialogBoxType Type { get { return GameDialogBoxType.Quest; } }

        public QuestDialogBox()
        {    
            config = new GameDialogBoxConfiguration(Type, Cache.GameSettings.Resolution);
            frame = Cache.Interface[sprite].Frames[spriteFrame];
        }


        public QuestDialogBox(GameDialogBoxConfiguration config)
        {    
            this.config = config;
            frame = Cache.Interface[sprite].Frames[spriteFrame];
        }


        public bool Draw(SpriteBatch spriteBatch, GameTime gameTime)
        {       
            if (config.Hidden)
                if (!config.AlwaysVisible) return false; // dont draw dialogs if not a HUD item
                else if (Cache.GameSettings.LockedDialogs) return false;  // show HUD items when HUD is unlocked (transparent)

            if (((MainGame)Cache.DefaultState).Player.IsCasting && Cache.GameSettings.HideDialogDuringCasting) return false;

            int mouseX = (int)Cache.DefaultState.Display.Mouse.X;
            int mouseY = (int)Cache.DefaultState.Display.Mouse.Y;
            int X = config.X;
            int Y = config.Y;
            float transparency = ((Cache.GameSettings.TransparentDialogs || config.Hidden) ? 0.5F : 1F);  
            highlightedItemIndex = clickedItemIndex = selectedItemIndex = -1;
                   
            spriteBatch.Draw(Cache.Interface[sprite].Texture, new Vector2(X, Y), frame.GetRectangle(), Color.White * transparency);  //draw the base texture

            //draw extra 
            SpriteHelper.DrawTextCenteredWithShadow(spriteBatch, "No Active Quests", FontType.GeneralSize10, new Vector2(X + 20, Y + 70), 205, Color.White);

            /* TITLE */
            spriteBatch.Draw(Cache.Interface[(int)SpriteId.DialogsV2].Texture, new Vector2(X + 48, Y - 16), Cache.Interface[(int)SpriteId.DialogsV2].Frames[28].GetRectangle(), Color.White * transparency);
            SpriteHelper.DrawTextCenteredWithShadow(spriteBatch, "Quest", FontType.DisplayNameSize13Spacing1, new Vector2(X + 58, Y - 9), 140, Cache.Colors[GameColor.Orange]);
                  
            //figure out what this is used for
            if (frame != null && Utility.IsSelected(mouseX, mouseY, X, Y, X + frame.Width, Y + frame.Height))
                return true;
            else return false;                                                         
        }

        public void Update(GameTime gameTime)
        {

        }

        public void LeftClicked()
        {
            //GameKeyboard keyboard = Cache.DefaultState.Display.Keyboard;
            int mouseX = (int)Cache.DefaultState.Display.Mouse.X;
            int mouseY = (int)Cache.DefaultState.Display.Mouse.Y;
            int X = config.X;
            int Y = config.Y;

            switch (config.State)
            {
                case DialogBoxState.Compact:
                    {
                        /* MINIMIZE BUTTON */
                        if ((X + 222 < mouseX) && (X + 233 > mouseX) && (Y + 14 < mouseY) && (Y + 22 > mouseY))
                        {
                            config.State = DialogBoxState.Normal;
                            //highlightedItemIndex = -1;
                            return;
                        }

                        //// SETS 1-5
                        //if ((Y + 10 < mouseY) && (Y + 24 > mouseY))
                        //{
                        //    // SET 1
                        //    if ((X + 35 < mouseX) && (X + 70 > mouseX)) { ((MainGame)Cache.DefaultState).UseItemSet(1); }
                        //    // SET 2
                        //    if ((X + 70 < mouseX) && (X + 105 > mouseX)) { ((MainGame)Cache.DefaultState).UseItemSet(2); }
                        //    // SET 3
                        //    if ((X + 105 < mouseX) && (X + 140 > mouseX)) { ((MainGame)Cache.DefaultState).UseItemSet(3); }
                        //    // SET 4
                        //    if ((X + 140 < mouseX) && (X + 175 > mouseX)) { ((MainGame)Cache.DefaultState).UseItemSet(4); }
                        //    // SET 5
                        //    if ((X + 175 < mouseX) && (X + 210 > mouseX)) { ((MainGame)Cache.DefaultState).UseItemSet(5); }
                        //}
                        break;
                    }
                default:
                    {
                        if (config.State == DialogBoxState.Normal)
                        {
                            /* MAXIMIZE BUTTON */
                            if ((X + 222 < mouseX) && (X + 233 > mouseX) && (Y + 14 < mouseY) && (Y + 22 > mouseY))
                            {
                                config.State = DialogBoxState.Compact;
                                //SetPagination();           
                                return;
                            }
                        }
                        break;
                        //    // SETS 1-5
                        //    if ((Y + 254 < mouseY) && (Y + 268 > mouseY))
                        //    {
                        //        // SET 1
                        //        if ((X + 17 < mouseX) && (X + 52 > mouseX)) { ((MainGame)Cache.DefaultState).UseItemSet(1); }
                        //        // SET 2
                        //        if ((X + 60 < mouseX) && (X + 95 > mouseX)) { ((MainGame)Cache.DefaultState).UseItemSet(2); }
                        //        // SET 3
                        //        if ((X + 103 < mouseX) && (X + 138 > mouseX)) { ((MainGame)Cache.DefaultState).UseItemSet(3); }
                        //        // SET 4
                        //        if ((X + 146 < mouseX) && (X + 181 > mouseX)) { ((MainGame)Cache.DefaultState).UseItemSet(4); }
                        //        // SET 5
                        //        if ((X + 189 < mouseX) && (X + 224 > mouseX)) { ((MainGame)Cache.DefaultState).UseItemSet(5); }
                        //    }

                        //    //CREATE SET
                        //    if ((Y + 225 < mouseY) && (Y + 247 > mouseY))
                        //    {
                        //        if ((X + 59 < mouseX) && (X + 116 > mouseX))
                        //        {
                        //            //Hide clear set dialog box
                        //            ((MainGame)Cache.DefaultState).DialogBoxes[GameDialogBoxType.ClearSet].Hide();

                        //            //Update x & y then show
                        //            ((MainGame)Cache.DefaultState).DialogBoxes[GameDialogBoxType.CreateSet].Config.X = X + 35;
                        //            ((MainGame)Cache.DefaultState).DialogBoxes[GameDialogBoxType.CreateSet].Config.Y = Y + 110;
                        //            ((MainGame)Cache.DefaultState).DialogBoxes[GameDialogBoxType.CreateSet].Toggle();
                        //        }

                        //        //CLEAR SET
                        //        if ((X + 123 < mouseX) && (X + 182 > mouseX))
                        //        {
                        //            //Hide create set dialog box
                        //            ((MainGame)Cache.DefaultState).DialogBoxes[GameDialogBoxType.CreateSet].Hide();

                        //            //Update x & y, then show
                        //            ((MainGame)Cache.DefaultState).DialogBoxes[GameDialogBoxType.ClearSet].Config.X = X + 35;
                        //            ((MainGame)Cache.DefaultState).DialogBoxes[GameDialogBoxType.ClearSet].Config.Y = Y + 110;
                        //            ((MainGame)Cache.DefaultState).DialogBoxes[GameDialogBoxType.ClearSet].Toggle();
                        //        }
                        //    }
                        //}

                        ///* CHARACTER BUTTON */
                        //if ((X + 32 < mouseX) && (X + 86 > mouseX) && (Y + 473 < mouseY) && (Y + 491 > mouseY))
                        //{
                        //    //config.State = DialogBoxState.Normal;
                        //    //SetPagination();
                        //    //highlightedItemIndex = -1;
                        //    return;
                        //}

                        ///* STATS BUTTON */
                        //if ((X + 93 < mouseX) && (X + 147 > mouseX) && (Y + 473 < mouseY) && (Y + 491 > mouseY))
                        //{
                        //    //((MainGame)Cache.DefaultState).DialogBoxes[GameDialogBoxType.Stats].Toggle();
                        //    ////config.State = DialogBoxState.Statistics;
                        //    ////SetPagination();
                        //    ////highlightedItemIndex = -1;
                        //    return;
                        //}

                        ///* QUEST BUTTON */
                        //if ((X + 154 < mouseX) && (X + 208 > mouseX) && (Y + 473 < mouseY) && (Y + 491 > mouseY))
                        //{
                        //    //((MainGame)Cache.DefaultState).DialogBoxes[GameDialogBoxType.Quest].Toggle();
                        //    ////config.State = DialogBoxState.Quest;
                        //    ////SetPagination();
                        //    ////highlightedItemIndex = -1;
                        //    return;
                        //}
                    }
            }
        }

        public void LeftDoubleClicked(ref SelectionMode selectionMode, ref int selectionModeId)
        {
        }

        public void LeftHeld()
        {
        }

        public void LeftDragged()
        {
        }

        public void LeftReleased(GameDialogBoxType highlightedDialogBox, int highlightedDialogBoxItemIndex)
        {
        }

        public void RightClicked() { }

        public void RightHeld() { }

        public void RightReleased() { }

        public void Scroll(int direction)
        {
            if (config.Page - direction > config.MaxPages) config.Page = 1;
            else if (config.Page - direction < 1) config.Page = config.MaxPages;
            else config.Page -= direction;
        }

        public void OffsetLocation(int x, int y)
        {      
            config.X += x;
            config.Y += y;
        }

        public void Show()
        {
                       
            if (!config.AlwaysVisible && Cache.DefaultState != null)
                ((IGameState)Cache.DefaultState).BringToFront(Type);
            highlightedItemIndex = -1;
            config.Hidden = false;
        }

        public void Hide()
        {                   
            if (!config.AlwaysVisible && Cache.DefaultState != null)
            {
                ((IGameState)Cache.DefaultState).DialogBoxDrawOrder.Remove(Type);
                ((IGameState)Cache.DefaultState).ClickedDialogBox = GameDialogBoxType.None;
            }
            config.Hidden = true;
        }

        public void Toggle(DialogBoxState state = DialogBoxState.Normal)
        {
                      
            if (config.Visible && state == config.State) Hide();
            else
            {
                config.State = state;
                Show();
            }
        }

        public void SetData(byte[] data)
        {

        }
    }
}
