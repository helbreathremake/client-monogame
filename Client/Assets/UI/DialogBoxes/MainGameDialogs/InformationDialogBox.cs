﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

using Helbreath.Common;
using Helbreath.Common.Assets;
using Client.Assets.State;
using Client.Assets.Game;

namespace Client.Assets.UI
{
    public class InformationDialogBox : IGameDialogBox
    {
        int selectedItemIndex = -1;
        int clickedItemIndex = -1;
        int highlightedItemIndex = -1;
        int selectedItemIndexOffsetX; //Dragging items
        int selectedItemIndexOffsetY; //Dragging items
              
        GameDialogBoxConfiguration config;

        //Base graphic info
        int sprite = (int)SpriteId.DialogsV2 + 2; //Main texture
        int spriteFrame = 17; //rectangle in texutre
        AnimationFrame frame;

        public int SelectedItemIndex { get { return selectedItemIndex; } set { selectedItemIndex = value; } }
        public int ClickedItemIndex { get { return clickedItemIndex; } set { clickedItemIndex = value; } }
        public int HighlightedItemIndex { get { return highlightedItemIndex; } set { highlightedItemIndex = value; } }
        public int SelectedItemIndexOffsetX { get { return selectedItemIndexOffsetX; } set { selectedItemIndexOffsetX = value; } }
        public int SelectedItemIndexOffsetY { get { return selectedItemIndexOffsetY; } set { selectedItemIndexOffsetY = value; } }
        public GameDialogBoxConfiguration Config { get { return config; } set { config = value; } }
        public GameDialogBoxType Type { get { return GameDialogBoxType.Information; } }


        int mouseX;
        int mouseY;
        int X;
        int Y;
        float transparency;  

        public InformationDialogBox()
        {    
            config = new GameDialogBoxConfiguration(Type, Cache.GameSettings.Resolution);
            frame = Cache.Interface[sprite].Frames[spriteFrame];
            mouseX = (int)Cache.DefaultState.Display.Mouse.X;
            mouseY = (int)Cache.DefaultState.Display.Mouse.Y;
            X = config.X;
            Y = config.Y;
            transparency = ((Cache.GameSettings.TransparentDialogs || config.Hidden) ? 0.5F : 1F);  
        }


        public InformationDialogBox(GameDialogBoxConfiguration config)
        {    
            this.config = config;
            frame = Cache.Interface[sprite].Frames[spriteFrame];
            mouseX = (int)Cache.DefaultState.Display.Mouse.X;
            mouseY = (int)Cache.DefaultState.Display.Mouse.Y;
            X = config.X;
            Y = config.Y;
            transparency = ((Cache.GameSettings.TransparentDialogs || config.Hidden) ? 0.5F : 1F);  
        }


        public bool Draw(SpriteBatch spriteBatch, GameTime gameTime)
        {       
            if (config.Hidden)
                if (!config.AlwaysVisible) return false; // dont draw dialogs if not a HUD item
                else if (Cache.GameSettings.LockedDialogs) return false;  // show HUD items when HUD is unlocked (transparent)

            if (((MainGame)Cache.DefaultState).Player.IsCasting && Cache.GameSettings.HideDialogDuringCasting) return false;

            highlightedItemIndex = clickedItemIndex = selectedItemIndex = -1;
                   
            spriteBatch.Draw(Cache.Interface[sprite].Texture, new Vector2(X, Y), frame.GetRectangle(), Color.White * transparency);  //draw the base texture

            int index = 0;
            //DATA
            //Recieved
            SpriteHelper.DrawTextWithShadow(spriteBatch, string.Format("In: {0}kbs", ((MainGame)Cache.DefaultState).dataInRate.ToString("F")), FontType.DialogsSmallSize8, new Vector2(X + 20, Y + 30 + (index * 15)), Cache.Colors[GameColor.Orange]);
            index++;

            //Sent
            SpriteHelper.DrawTextWithShadow(spriteBatch, string.Format("Out: {0}kbs", ((MainGame)Cache.DefaultState).dataOutRate.ToString("F")), FontType.DialogsSmallSize8, new Vector2(X + 20, Y + 30 + (index * 15)), Cache.Colors[GameColor.Orange]);
            index += 2;

            foreach (KeyValuePair<GameColor, Color> color in Cache.Colors)
            {
                if (index < 29) { SpriteHelper.DrawTextWithShadow(spriteBatch, color.Key.ToString(), FontType.GeneralSize10Bold, new Vector2(X + 20, Y + 30 + (index * 15)), color.Value); }
                else if (index < 54) { SpriteHelper.DrawTextWithShadow(spriteBatch, color.Key.ToString(), FontType.GeneralSize10Bold, new Vector2(X + 20 + 100, Y + 30 + ((index - 25) * 15)), color.Value); }
                else { SpriteHelper.DrawTextWithShadow(spriteBatch, color.Key.ToString(), FontType.GeneralSize10Bold, new Vector2(X + 20 + 100 + 100, Y + 30 + ((index - 50) * 15)), color.Value); }

                index += 1;
            }

            //MOUSE
            ////LeftHeld
            //SpriteHelper.DrawTextWithShadow(spriteBatch, string.Format("Drag: {0}", Cache.DefaultState.Display.Mouse.Drag.ToString()), FontType.DialogsSmallSize8, new Vector2(X + 20, Y + 30 + (index * 15)), Cache.Colors[GameColor.Orange]);
            //index++;

            ////Dragging
            //SpriteHelper.DrawTextWithShadow(spriteBatch, string.Format("Dragging: {0}", Cache.DefaultState.Display.Mouse.IsDragging.ToString()), FontType.DialogsSmallSize8, new Vector2(X + 20, Y + 30 + (index * 15)), Cache.Colors[GameColor.Orange]);
            //index += 2;


            ////LEFT BUTTON
            ////Clicked
            //SpriteHelper.DrawTextWithShadow(spriteBatch, string.Format("Left Clicked: {0}", Cache.DefaultState.Display.Mouse.LeftClick.ToString()), FontType.DialogsSmallSize8, new Vector2(X + 20, Y + 30 + (index * 15)), Cache.Colors[GameColor.Orange]);
            //index++;

            ////Held
            //SpriteHelper.DrawTextWithShadow(spriteBatch, string.Format("Left Down: {0}", Cache.DefaultState.Display.Mouse.LeftHeld.ToString()), FontType.DialogsSmallSize8, new Vector2(X + 20, Y + 30 + (index * 15)), Cache.Colors[GameColor.Orange]);
            //index++;

            ////Released
            //SpriteHelper.DrawTextWithShadow(spriteBatch, string.Format("Left Released: {0}", Cache.DefaultState.Display.Mouse.LeftReleased.ToString()), FontType.DialogsSmallSize8, new Vector2(X + 20, Y + 30 + (index * 15)), Cache.Colors[GameColor.Orange]);
            //index += 2;



            ////RIGHT BUTTON
            ////Clicked
            //SpriteHelper.DrawTextWithShadow(spriteBatch, string.Format("Right Clicked: {0}", Cache.DefaultState.Display.Mouse.RightClick.ToString()), FontType.DialogsSmallSize8, new Vector2(X + 20, Y + 30 + (index * 15)), Cache.Colors[GameColor.Orange]);
            //index++;

            ////Held
            //SpriteHelper.DrawTextWithShadow(spriteBatch, string.Format("Right Down: {0}", Cache.DefaultState.Display.Mouse.RightHeld.ToString()), FontType.DialogsSmallSize8, new Vector2(X + 20, Y + 30 + (index * 15)), Cache.Colors[GameColor.Orange]);
            //index++;

            ////Released
            //SpriteHelper.DrawTextWithShadow(spriteBatch, string.Format("Right Released: {0}", Cache.DefaultState.Display.Mouse.RightReleased.ToString()), FontType.DialogsSmallSize8, new Vector2(X + 20, Y + 30 + (index * 15)), Cache.Colors[GameColor.Orange]);
            //index += 2;

            ////Scroll
            //SpriteHelper.DrawTextWithShadow(spriteBatch, string.Format("Scroll: {0}", Cache.DefaultState.Display.Mouse.Scroll.ToString()), FontType.DialogsSmallSize8, new Vector2(X + 20, Y + 30 + (index * 15)), Cache.Colors[GameColor.Orange]);
            //index++;
                

            /* TITLE */
            spriteBatch.Draw(Cache.Interface[(int)SpriteId.DialogsV2].Texture, new Vector2(X + 48, Y - 16), Cache.Interface[(int)SpriteId.DialogsV2].Frames[28].GetRectangle(), Color.White * transparency);
            SpriteHelper.DrawTextCenteredWithShadow(spriteBatch, "Information", FontType.DisplayNameSize13Spacing1, new Vector2(X + 58, Y - 9), 140, Cache.Colors[GameColor.Orange]);
                  
            //figure out what this is used for
            if (frame != null && Utility.IsSelected(mouseX, mouseY, X, Y, X + frame.Width, Y + frame.Height))
                return true;
            else return false;                                                         
        }

        public void Update(GameTime gameTime)
        {
            transparency = ((Cache.GameSettings.TransparentDialogs || config.Hidden) ? 0.5F : 1F);  
            mouseX = (int)Cache.DefaultState.Display.Mouse.X;
            mouseY = (int)Cache.DefaultState.Display.Mouse.Y;
            X = config.X;
            Y = config.Y;
        }

        public void LeftClicked()
        {

        }

        public void LeftDoubleClicked(ref SelectionMode selectionMode, ref int selectionModeId)
        {
        }

        public void LeftHeld()
        {
        }

        public void LeftDragged()
        {
        }

        public void LeftReleased(GameDialogBoxType highlightedDialogBox, int highlightedDialogBoxItemIndex)
        {
        }

        public void RightClicked() { }

        public void RightHeld() { }

        public void RightReleased() { }

        public void Scroll(int direction)
        {
            if (config.Page - direction > config.MaxPages) config.Page = 1;
            else if (config.Page - direction < 1) config.Page = config.MaxPages;
            else config.Page -= direction;
        }

        public void OffsetLocation(int x, int y)
        {      
            config.X += x;
            config.Y += y;
        }

        public void Show()
        {
                       
            if (!config.AlwaysVisible && Cache.DefaultState != null)
                ((IGameState)Cache.DefaultState).BringToFront(Type);
            highlightedItemIndex = -1;
            config.Hidden = false;
        }

        public void Hide()
        {                   
            if (!config.AlwaysVisible && Cache.DefaultState != null)
            {
                ((IGameState)Cache.DefaultState).DialogBoxDrawOrder.Remove(Type);
                ((IGameState)Cache.DefaultState).ClickedDialogBox = GameDialogBoxType.None;
            }
            config.Hidden = true;
        }

        public void Toggle(DialogBoxState state = DialogBoxState.Normal)
        {
                      
            if (config.Visible && state == config.State) Hide();
            else
            {
                config.State = state;
                Show();
            }
        }

        public void SetData(byte[] data)
        {

        }
    }
}
