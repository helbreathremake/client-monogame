﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

using Helbreath.Common;
using Client.Assets.State;
using Client.Assets.Game;

namespace Client.Assets.UI
{
    public class SPIconPanelDialogBox : IGameDialogBox
    { 
        int selectedItemIndex = -1;
        int clickedItemIndex = -1;
        int highlightedItemIndex = -1;
        int selectedItemIndexOffsetX; //Dragging items
        int selectedItemIndexOffsetY; //Dragging items
              
        GameDialogBoxConfiguration config;

        //Base graphic info
        int sprite = (int)SpriteId.DialogsV2; //Main texture
        int spriteFrame = 2; //rectangle in texutre
        AnimationFrame frame;

        public int SelectedItemIndex { get { return selectedItemIndex; } set { selectedItemIndex = value; } }
        public int ClickedItemIndex { get { return clickedItemIndex; } set { clickedItemIndex = value; } }
        public int HighlightedItemIndex { get { return highlightedItemIndex; } set { highlightedItemIndex = value; } }
        public int SelectedItemIndexOffsetX { get { return selectedItemIndexOffsetX; } set { selectedItemIndexOffsetX = value; } }
        public int SelectedItemIndexOffsetY { get { return selectedItemIndexOffsetY; } set { selectedItemIndexOffsetY = value; } }
        public GameDialogBoxConfiguration Config { get { return config; } set { config = value; } }
        public GameDialogBoxType Type { get { return GameDialogBoxType.SPIconPanel; } }

        public SPIconPanelDialogBox()
        {    
            config = new GameDialogBoxConfiguration(Type, Cache.GameSettings.Resolution);
            frame = Cache.Interface[sprite].Frames[spriteFrame];
        }

        public SPIconPanelDialogBox(GameDialogBoxConfiguration config)
        {    
            this.config = config;
            frame = Cache.Interface[sprite].Frames[spriteFrame];
        }

        public bool Draw(SpriteBatch spriteBatch, GameTime gameTime)
        {       
            if (config.Hidden)
                if (!config.AlwaysVisible) return false; // dont draw dialogs if not a HUD item
                else if (Cache.GameSettings.LockedDialogs) return false;  // show HUD items when HUD is unlocked (transparent)

            if (((MainGame)Cache.DefaultState).Player.IsCasting && Cache.GameSettings.HideDialogDuringCasting) return false;

            Player player = ((MainGame)Cache.DefaultState).Player;
            int mouseX = (int)Cache.DefaultState.Display.Mouse.X;
            int mouseY = (int)Cache.DefaultState.Display.Mouse.Y;
            int x = config.X;
            int y = config.Y;
            float transparency = ((Cache.GameSettings.TransparentDialogs || config.Hidden) ? 0.5F : 1F);  
                 
            spriteBatch.Draw(Cache.Interface[sprite].Texture, new Vector2(x, y), frame.GetRectangle(), Color.White * transparency);  //draw the base texture //TODO check what this looks like without since we draw this 2x
              
            if (player.MaxSP > 0)
            {
                int spWidth = (int)MathHelper.Clamp(166 - (player.SP * 166) / player.MaxSP, 0, 166);
                spriteBatch.Draw(Cache.Interface[sprite].Texture, new Vector2(x + 3, y + 2), Cache.Interface[sprite].Frames[5].GetRectangle(167 - spWidth), Color.Green * transparency);
                if (player.SP < player.MaxSP / 4) { spriteBatch.Draw(Cache.Interface[sprite].Texture, new Vector2(x + 3, y + 2), Cache.Interface[sprite].Frames[5].GetRectangle(167 - spWidth), Color.Black * Cache.TransparencyFaders.BlinkFrame); }
                if (player.TickSP) { spriteBatch.Draw(Cache.Interface[sprite].Texture, new Vector2(x + 3, y + 2), Cache.Interface[sprite].Frames[5].GetRectangle(), Color.Green * Cache.TransparencyFaders.PulseFrame); }
            }

            spriteBatch.Draw(Cache.Interface[sprite].Texture, new Vector2(x, y), frame.GetRectangle(), Color.White * transparency);

            string spText = player.SP + "/" + player.MaxSP;
            spriteBatch.DrawString(Cache.Fonts[FontType.DialogsSmallSize8], spText, new Vector2((int)(x + (frame.Width / 2 - Cache.Fonts[FontType.DialogsSmallSize8].MeasureString(spText).X / 2)), (int)(y - 1)), Color.White);
                  
            if (Utility.IsSelected(mouseX, mouseY, x + 1, y + 1, x + frame.Width - 2, y + frame.Height - 2))
                return true;
            else return false;                                                         
        }

        public void Update(GameTime gameTime)
        {

        }

        public void LeftClicked()
        {
            int mouseX = (int)Cache.DefaultState.Display.Mouse.X;
            int mouseY = (int)Cache.DefaultState.Display.Mouse.Y;
            Player player = ((MainGame)Cache.DefaultState).Player;
        }

        public void LeftDoubleClicked(ref SelectionMode selectionMode, ref int selectionModeId)
        {
        }

        public void LeftHeld()
        {
        }

        public void LeftDragged()
        {
        }

        public void LeftReleased(GameDialogBoxType highlightedDialogBox, int highlightedDialogBoxItemIndex)
        {
        }

        public void RightClicked() { }

        public void RightHeld() { }

        public void RightReleased() { }

        public void Scroll(int direction)
        {
            if (config.Page - direction > config.MaxPages) config.Page = 1;
            else if (config.Page - direction < 1) config.Page = config.MaxPages;
            else config.Page -= direction;
        }

        public void OffsetLocation(int x, int y)
        {      
            config.X += x;
            config.Y += y;
        }

        public void Show()
        {
                       
            if (!config.AlwaysVisible && Cache.DefaultState != null)
                ((IGameState)Cache.DefaultState).BringToFront(Type);
            highlightedItemIndex = -1;
            config.Hidden = false;
        }

        public void Hide()
        {                   
            if (!config.AlwaysVisible && Cache.DefaultState != null)
            {
                ((IGameState)Cache.DefaultState).DialogBoxDrawOrder.Remove(Type);
                ((IGameState)Cache.DefaultState).ClickedDialogBox = GameDialogBoxType.None;
            }
            config.Hidden = true;
        }

        public void Toggle(DialogBoxState state = DialogBoxState.Normal)
        {
                      
            if (config.Visible && state == config.State) Hide();
            else
            {
                config.State = state;
                Show();
            }
        }

        public void SetData(byte[] data)
        {

        }
    }
}
