﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

using Helbreath.Common.Assets;
using Client.Assets.State;
using Helbreath.Common;
using Client.Assets.Game;

namespace Client.Assets.UI
{
    public class MagicShopDialogBox : IGameDialogBox
    {
        //Base graphic info
        int sprite = (int)SpriteId.DialogsV2 + 1; //Main texture
        int spriteFrame = 7; //rectangle in texutre
        AnimationFrame frame;
        int width;
        int height;

        // selected items
        private int clickedItemIndex; // clicked item
        private int selectedItemIndex; // mouse over item
        private int selectedItemIndexOffsetX;
        private int selectedItemIndexOffsetY;
        private int highlightedItemIndex; // chosen item (non volatile)
        private GameDialogBoxConfiguration config;

        public int ClickedItemIndex { get { return clickedItemIndex; } set { clickedItemIndex = value; } }
        public int SelectedItemIndex { get { return selectedItemIndex; } set { selectedItemIndex = value; } }
        public int HighlightedItemIndex { get { return highlightedItemIndex; } set { highlightedItemIndex = value; } }
        public int SelectedItemIndexOffsetX { get { return selectedItemIndexOffsetX; } set { selectedItemIndexOffsetX = value; } }
        public int SelectedItemIndexOffsetY { get { return selectedItemIndexOffsetY; } set { selectedItemIndexOffsetY = value; } }

        public GameDialogBoxType Type { get { return GameDialogBoxType.MagicShop; } }
        public GameDialogBoxConfiguration Config { get { return config; } set { config = value; } }

        int mouseX;
        int mouseY;
        int X;
        int Y;
        float transparency;
        Player player;

        public MagicShopDialogBox()
        {
            config = new GameDialogBoxConfiguration(Type, Cache.GameSettings.Resolution);
            highlightedItemIndex = selectedItemIndex = clickedItemIndex = -1;
            frame = Cache.Interface[sprite].Frames[spriteFrame];
            width = frame.Width;
            height = frame.Height;
            mouseX = (int)Cache.DefaultState.Display.Mouse.X;
            mouseY = (int)Cache.DefaultState.Display.Mouse.Y;
            X = config.X;
            Y = config.Y;
            transparency = (Cache.GameSettings.TransparentDialogs ? 0.5F : 1F);
            player = ((MainGame)Cache.DefaultState).Player;
        }

        public MagicShopDialogBox(GameDialogBoxConfiguration config)
        {
            this.config = config;
            highlightedItemIndex = selectedItemIndex = clickedItemIndex = -1;
            frame = Cache.Interface[sprite].Frames[spriteFrame];
            width = frame.Width;
            height = frame.Height;
            mouseX = (int)Cache.DefaultState.Display.Mouse.X;
            mouseY = (int)Cache.DefaultState.Display.Mouse.Y;
            X = config.X;
            Y = config.Y;
            transparency = (Cache.GameSettings.TransparentDialogs ? 0.5F : 1F);
            player = ((MainGame)Cache.DefaultState).Player;
        }

        public bool Draw(SpriteBatch spriteBatch, GameTime gameTime)
        {
            if (config.Hidden)
                if (!config.AlwaysVisible) return false; // dont draw dialogs if not a HUD item
                else if (Cache.GameSettings.LockedDialogs) return false;  // show HUD items when HUD is unlocked (transparent)

            if (((MainGame)Cache.DefaultState).Player.IsCasting && Cache.GameSettings.HideDialogDuringCasting) return false;

            highlightedItemIndex = -1;

            //Main Texture
            spriteBatch.Draw(Cache.Interface[sprite].Texture, new Vector2(X, Y), frame.GetRectangle(), Color.White * transparency);

            /* TITLE */
            spriteBatch.Draw(Cache.Interface[(int)SpriteId.DialogsV2].Texture, new Vector2(X + (width / 2) - (Cache.Interface[(int)SpriteId.DialogsV2].Frames[28].GetRectangle().Width / 2), Y - 16), Cache.Interface[(int)SpriteId.DialogsV2].Frames[28].GetRectangle(), Color.White * transparency);
            SpriteHelper.DrawTextCenteredWithShadow(spriteBatch, "Magic Study", FontType.DisplayNameSize13Spacing1, new Vector2(X, Y - 9),width, Cache.Colors[GameColor.Orange]);

            //Display Circle # in text at top of dialog box
            int pagenumber = config.Page;
            string circlenumber = "Circle: " + pagenumber.ToString();           
            SpriteHelper.DrawTextCenteredWithShadow(spriteBatch, circlenumber, FontType.DialogSize8Bold, new Vector2(X, Y + 40), width, Color.White);

            //Show casting probability
            int probability = Math.Min(100, Utility.GetCastingProbability(player.Skills[(int)SkillType.Magic], config.Page, player.Level, player.Intelligence, (((MainGame)Cache.DefaultState).Weather != null ? ((MainGame)Cache.DefaultState).Weather.Type : WeatherType.Clear), player.Inventory.CastProbabilityBonus, player.SP));
            string castprob = "Casting Chance: " + probability + "%";
            //SpriteHelper.DrawTextCenteredWithShadow(spriteBatch, castprob, FontType.DialogsV2Small, new Vector2(X, Y + 234), width, Color.White);
            SpriteHelper.DrawTextCenteredWithShadow(spriteBatch, castprob, FontType.DialogsSmallerSize7, new Vector2(X, Y + 57), width, Color.White);

            /* PAGINATION */
            spriteBatch.Draw(Cache.Interface[sprite + 1].Texture, new Vector2(X + 155 + 140, Y + 358 - 61), Cache.Interface[sprite + 1].Frames[9].GetRectangle(), Color.White * transparency);
            SpriteHelper.DrawTextRightWithShadow(spriteBatch, config.Page + "/" + config.MaxPages, FontType.DialogsSmallerSize7, new Vector2(X + 128 + 140, Y + 356 - 61), 25, Cache.Colors[GameColor.Orange]);

            /* PAGINATION SCROLL HIGHLIGHT*/
            if ((Y + 358 - 61  < mouseY) && (Y + 358 - 61  + 7 > mouseY))
            {
                if ((X + 155 + 140 < mouseX) && (X + 155 + 140 + 11 > mouseX)) { spriteBatch.Draw(Cache.Interface[sprite + 1].Texture, new Vector2(X + 155 + 140 - 3, Y + 358 - 61 - 3), Cache.Interface[sprite + 1].Frames[25].GetRectangle(), Color.White * transparency); }
                if ((X + 155 + 140 + 11 < mouseX) && (X + 155 + 140 + 11 + 11 > mouseX)) { spriteBatch.Draw(Cache.Interface[sprite + 1].Texture, new Vector2(X + 155 + 140 - 3 + 11, Y + 358 - 61 - 3), Cache.Interface[sprite + 1].Frames[25].GetRectangle(), Color.White * transparency); }
            }

            //Spells
            int index = 0; int indexLeft = 0; int indexRight = 0;
            for (int i = (config.Page * 10) - 10; i < (config.Page * 10); i++)
                if (Cache.MagicConfiguration.ContainsKey(i))
                {
                    bool cantCast = player.IsCasting || !player.CanCast(i) || !player.HandsFree;
                    Color color;

                    Magic magic = Cache.MagicConfiguration[i];
                    if (indexLeft < 5)
                    {
                        bool hover = Utility.IsSelected(mouseX, mouseY, X + 45, Y + 41 + (indexLeft * 45) - 5, X + 45 + 150, Y + 41 + (indexLeft * 45) - 5 + 45);

                        if (player.MagicLearned[i])
                        {
                            if (hover) { color = Color.Gray; }
                            else { color = Cache.Colors[GameColor.Orange]; }

                            spriteBatch.Draw(Cache.Interface[sprite + 3].Texture, new Vector2(X + 55, Y + 41 + (indexLeft * 45) + 1), Cache.Interface[sprite + 3].Frames[(hover ? Cache.MagicConfiguration[i].SmallIconSpriteFrame + 1 : Cache.MagicConfiguration[i].SmallIconSpriteFrame)].GetRectangle(), Color.White * transparency);
                            SpriteHelper.DrawTextWithShadow(spriteBatch, Cache.MagicConfiguration[i].Name, FontType.DialogsSmallSize8, new Vector2(X + 95, Y + 40 + (indexLeft * 45)), color);
                            SpriteHelper.DrawTextWithShadow(spriteBatch, "Click to unlearn", FontType.DialogsSmallSize8, new Vector2(X + 95, Y + 54 + (indexLeft * 45) + 1), color);
                        }
                        else
                        {
                            if (hover) { color = (player.CanBuySpell(i)) ? Cache.Colors[GameColor.Friendly] : Cache.Colors[GameColor.Enemy]; }
                            else { color = Color.White; }
                         
                            if (magic.GoldCost < 0)
                            {
                                spriteBatch.Draw(Cache.Interface[sprite + 3].Texture, new Vector2(X + 55, Y + 41 + (indexLeft * 45) + 1), Cache.Interface[sprite + 3].Frames[(hover ? Cache.MagicConfiguration[i].SmallIconSpriteFrame + 1 : Cache.MagicConfiguration[i].SmallIconSpriteFrame)].GetRectangle(), Color.Gray * transparency);
                                SpriteHelper.DrawTextWithShadow(spriteBatch, Cache.MagicConfiguration[i].Name, FontType.DialogsSmallSize8, new Vector2(X + 95, Y + 40 + (indexLeft * 45)), (hover) ? color : Color.Gray);
                                SpriteHelper.DrawTextWithShadow(spriteBatch, "Can't be purchased", FontType.DialogsSmallSize8, new Vector2(X + 95, Y + 54 + (indexLeft * 45) + 1), (hover) ? color : Color.Gray);
                            }
                            else
                            {
                                spriteBatch.Draw(Cache.Interface[sprite + 3].Texture, new Vector2(X + 55, Y + 41 + (indexLeft * 45) + 1), Cache.Interface[sprite + 3].Frames[(hover ? Cache.MagicConfiguration[i].SmallIconSpriteFrame + 1 : Cache.MagicConfiguration[i].SmallIconSpriteFrame)].GetRectangle(), Color.White * transparency);
                                SpriteHelper.DrawTextWithShadow(spriteBatch, Cache.MagicConfiguration[i].Name, FontType.DialogsSmallSize8, new Vector2(X + 95, Y + 40 + (indexLeft * 45)), color);
                                SpriteHelper.DrawTextWithShadow(spriteBatch, "Int:", FontType.DialogsSmallSize8, new Vector2(X + 95, Y + 54 + (indexLeft * 45) + 1), (hover) ? color : Cache.Colors[GameColor.Orange]);
                                SpriteHelper.DrawTextWithShadow(spriteBatch, magic.RequiredIntelligence.ToString(), FontType.DialogsSmallSize8, new Vector2(X + 115, Y + 54 + (indexLeft * 45) + 1), (hover) ? color : (player.Intelligence + player.IntelligenceBonus >= magic.RequiredIntelligence) ? Color.White : Color.Gray);
                                SpriteHelper.DrawTextWithShadow(spriteBatch, "Gold:", FontType.DialogsSmallSize8, new Vector2(X + 145, Y + 54 + (indexLeft * 45) + 1), (hover) ? color : Cache.Colors[GameColor.Orange]);
                                SpriteHelper.DrawTextWithShadow(spriteBatch, magic.GoldCost.ToString(), FontType.DialogsSmallSize8, new Vector2(X + 172, Y + 54 + (indexLeft * 45) + 1), (hover) ? color : (player.Gold >= magic.GoldCost) ? Color.White : Color.Gray);
                            }
                        }

                        indexLeft++;

                        if (hover) highlightedItemIndex = i; // selected spell for cast
                    }
                    else if (indexRight < 5)
                    {
                        bool hover = Utility.IsSelected(mouseX, mouseY, X + 390, Y + 41 + (indexRight * 45) - 5, X + 390 + 150, Y + 41 + (indexRight * 45) - 5 + 45);

                        if (player.MagicLearned[i])
                        {
                            if (hover) { color = Color.Gray; }
                            else { color = Cache.Colors[GameColor.Orange]; }

                            spriteBatch.Draw(Cache.Interface[sprite + 3].Texture, new Vector2(X + 400, Y + 41 + (indexRight * 45) + 1), Cache.Interface[sprite + 3].Frames[(hover ? Cache.MagicConfiguration[i].SmallIconSpriteFrame + 1 : Cache.MagicConfiguration[i].SmallIconSpriteFrame)].GetRectangle(), Color.White * transparency);
                            SpriteHelper.DrawTextWithShadow(spriteBatch, Cache.MagicConfiguration[i].Name, FontType.DialogsSmallSize8, new Vector2(X + 440, Y + 40 + (indexRight * 45)), color);
                            SpriteHelper.DrawTextWithShadow(spriteBatch, "Click to unlearn", FontType.DialogsSmallSize8, new Vector2(X + 440, Y + 54 + (indexRight * 45) + 1), color);
                        }
                        else
                        {
                            if (hover) { color = (player.CanBuySpell(i)) ? Cache.Colors[GameColor.Friendly] : Cache.Colors[GameColor.Enemy]; }
                            else { color = Color.White; }

                           
                            if (magic.GoldCost < 0) {
                                spriteBatch.Draw(Cache.Interface[sprite + 3].Texture, new Vector2(X + 400, Y + 41 + (indexRight * 45) + 1), Cache.Interface[sprite + 3].Frames[(hover ? Cache.MagicConfiguration[i].SmallIconSpriteFrame + 1 : Cache.MagicConfiguration[i].SmallIconSpriteFrame)].GetRectangle(), Color.White * transparency);
                                SpriteHelper.DrawTextWithShadow(spriteBatch, Cache.MagicConfiguration[i].Name, FontType.DialogsSmallSize8, new Vector2(X + 440, Y + 40 + (indexRight * 45)), (hover) ? color : Color.Gray);
                                SpriteHelper.DrawTextWithShadow(spriteBatch, "Can't be purchased", FontType.DialogsSmallSize8, new Vector2(X + 440, Y + 54 + (indexRight * 45) + 1), (hover) ? color : Color.Gray);
                            }
                            else
                            {
                                spriteBatch.Draw(Cache.Interface[sprite + 3].Texture, new Vector2(X + 400, Y + 41 + (indexRight * 45) + 1), Cache.Interface[sprite + 3].Frames[(hover ? Cache.MagicConfiguration[i].SmallIconSpriteFrame + 1 : Cache.MagicConfiguration[i].SmallIconSpriteFrame)].GetRectangle(), Color.White * transparency);
                                SpriteHelper.DrawTextWithShadow(spriteBatch, Cache.MagicConfiguration[i].Name, FontType.DialogsSmallSize8, new Vector2(X + 440, Y + 40 + (indexRight * 45)), color);
                                SpriteHelper.DrawTextWithShadow(spriteBatch, "Int:", FontType.DialogsSmallSize8, new Vector2(X + 440, Y + 54 + (indexRight * 45) + 1), (hover) ? color : Cache.Colors[GameColor.Orange]);
                                SpriteHelper.DrawTextWithShadow(spriteBatch, magic.RequiredIntelligence.ToString(), FontType.DialogsSmallSize8, new Vector2(X + 460, Y + 54 + (indexRight * 45) + 1), (hover) ? color : (player.Intelligence + player.IntelligenceBonus >= magic.RequiredIntelligence) ? Color.White : Color.Gray);
                                SpriteHelper.DrawTextWithShadow(spriteBatch, "Gold:", FontType.DialogsSmallSize8, new Vector2(X + 490, Y + 54 + (indexRight * 45) + 1), (hover) ? color : Cache.Colors[GameColor.Orange]);
                                SpriteHelper.DrawTextWithShadow(spriteBatch, magic.GoldCost.ToString(), FontType.DialogsSmallSize8, new Vector2(X + 517, Y + 54 + (indexRight * 45) + 1), (hover) ? color : (player.Gold >= magic.GoldCost) ? Color.White : Color.Gray);
                            }
                        }
                        indexRight++;

                        if (hover) highlightedItemIndex = i; // selected spell for cast
                    }
                    index++;
                }

            //Highlighted spell
            if (highlightedItemIndex != -1)
            {
                Magic magic = Cache.MagicConfiguration[highlightedItemIndex];
                int descriptionIndex = 0; Vector2 startloc = new Vector2(X, Y + 80);
                SpriteHelper.DrawTextCenteredWithShadow(spriteBatch, magic.Name, FontType.DialogsSmallSize8, new Vector2(startloc.X, startloc.Y - 2 + (descriptionIndex * 11)), width, Cache.Colors[GameColor.Orange]);
                descriptionIndex++;
                SpriteHelper.DrawTextCenteredWithShadow(spriteBatch, string.Format("Mana cost: {0}", magic.ManaCost), FontType.DialogsSmallerSize7, new Vector2(startloc.X, startloc.Y + (descriptionIndex * 11)), width, Color.White);
                descriptionIndex++;
                SpriteHelper.DrawTextCenteredWithShadow(spriteBatch, string.Format("Category: {0}", magic.Category), FontType.DialogsSmallerSize7, new Vector2(startloc.X, startloc.Y + (descriptionIndex * 11)), width, Color.White);
                descriptionIndex++;

                if (magic.Attribute != MagicAttribute.None)
                {
                    SpriteHelper.DrawTextCenteredWithShadow(spriteBatch, string.Format("Attribute: {0}", magic.Attribute), FontType.DialogsSmallerSize7, new Vector2(startloc.X, startloc.Y + (descriptionIndex * 11)), width, Color.White);
                    descriptionIndex++;
                }

                if (magic.LastTime != TimeSpan.Zero)
                {
                    SpriteHelper.DrawTextCenteredWithShadow(spriteBatch, string.Format("Duration: {0}", magic.LastTime), FontType.DialogsSmallerSize7, new Vector2(startloc.X, startloc.Y + (descriptionIndex * 11)), width, Color.White);
                    descriptionIndex++;
                }

                SpriteHelper.DrawTextCenteredWithShadow(spriteBatch, string.Format("Range X: {0}", magic.RangeX), FontType.DialogsSmallerSize7, new Vector2(startloc.X, startloc.Y + (descriptionIndex * 11)), width, Color.White);
                descriptionIndex++;
                SpriteHelper.DrawTextCenteredWithShadow(spriteBatch, string.Format("Range Y: {0}", magic.RangeY), FontType.DialogsSmallerSize7, new Vector2(startloc.X, startloc.Y + (descriptionIndex * 11)), width, Color.White);
                descriptionIndex++;

                if (magic.DelayTime != TimeSpan.Zero)
                {
                    SpriteHelper.DrawTextCenteredWithShadow(spriteBatch, string.Format("Delay Time: {0}", magic.DelayTime), FontType.DialogsSmallerSize7, new Vector2(startloc.X, startloc.Y + (descriptionIndex * 11)), width, Color.White);
                    descriptionIndex++;
                }

                if (magic.IgnorePFM)
                {
                    SpriteHelper.DrawTextCenteredWithShadow(spriteBatch, "Pierces PFM", FontType.DialogsSmallerSize7, new Vector2(startloc.X, startloc.Y + (descriptionIndex * 11)), width, Color.White);
                    descriptionIndex++;
                }

                switch (magic.Type) //TODO finish this when you finalize spells
                {
                    case MagicType.Poison:
                        if (magic.Effect1 == 1)
                        {
                            SpriteHelper.DrawTextCenteredWithShadow(spriteBatch, string.Format("Poison Damage: {0}", magic.Effect2), FontType.DialogsSmallerSize7, new Vector2(startloc.X, startloc.Y + (descriptionIndex * 11)), width, Color.White);
                            descriptionIndex++;
                        }
                        break;
                    case MagicType.HPUpSingle:
                        SpriteHelper.DrawTextCenteredWithShadow(spriteBatch, string.Format("Replenish HP: {0}d{1}+{2}", magic.Effect1, magic.Effect2, magic.Effect3), FontType.DialogsSmallerSize7, new Vector2(startloc.X, startloc.Y + (descriptionIndex * 11)), width, Color.White);
                        descriptionIndex++;
                        break;
                    case MagicType.SPUpArea:
                    case MagicType.SPUpSingle:
                        SpriteHelper.DrawTextCenteredWithShadow(spriteBatch, string.Format("Replenish SP: {0}d{1}+{2}", magic.Effect1, magic.Effect2, magic.Effect3), FontType.DialogsSmallerSize7, new Vector2(startloc.X, startloc.Y + (descriptionIndex * 11)), width, Color.White);
                        descriptionIndex++;
                        break;
                    case MagicType.SPDownSingle:
                    case MagicType.SPDownArea:
                        SpriteHelper.DrawTextCenteredWithShadow(spriteBatch, string.Format("Deplete SP: {0}d{1}+{2}", magic.Effect1, magic.Effect2, magic.Effect3), FontType.DialogsSmallerSize7, new Vector2(startloc.X, startloc.Y + (descriptionIndex * 11)), width, Color.White);
                        descriptionIndex++;
                        break;
                    case MagicType.DamageArea:
                    case MagicType.DamageAreaNoSingle:
                    case MagicType.DamageAreaNoSingleSPDown:
                    case MagicType.DamageSingle:
                        SpriteHelper.DrawTextCenteredWithShadow(spriteBatch, string.Format("Damge: {0}d{1}+{2}", magic.Effect1, magic.Effect2, magic.Effect3), FontType.DialogsSmallerSize7, new Vector2(startloc.X, startloc.Y + (descriptionIndex * 11)), width, Color.White);
                        descriptionIndex++;
                        break;
                    case MagicType.ArmourBreak:
                    case MagicType.Barbs:
                    case MagicType.Berserk:
                    case MagicType.Cancellation:
                    case MagicType.Confuse:
                    case MagicType.CreateDynamic:
                    case MagicType.CreateItem:
                    case MagicType.DamageLine:
                    case MagicType.DamageLineSPDown:
                    case MagicType.DamageSingleSPDown:
                    case MagicType.ExploitCorpse:
                    case MagicType.GhostSpiral:
                    case MagicType.Ice:
                    case MagicType.IceLine:
                    case MagicType.Inhibition:
                    case MagicType.Invisibility:
                    case MagicType.Kinesis:
                    case MagicType.None:
                    case MagicType.Paralyze:
                    case MagicType.Polymorph:
                    case MagicType.Possession:
                    case MagicType.Protect:
                    case MagicType.Resurrection:
                    case MagicType.Scan:
                    case MagicType.Spiral:
                    case MagicType.Summon:
                    case MagicType.Teleport:
                    case MagicType.Tremor:
                    case MagicType.TurnUndead:
                    case MagicType.UnlimitedSP:
                    default: break;
                }
            }

            if (clickedItemIndex == -1) // if no spell dragging
                if (highlightedItemIndex != -1) // and no spell hovered
                    selectedItemIndex = highlightedItemIndex; // set hover index
                else selectedItemIndex = -1; // cancel hovered spell when not dragging
            
            //DRAGGED ITEM
            //if (Cache.DefaultState.Display.Mouse.IsDragging && clickedItemIndex != -1)
            //    ((MainGame)Cache.DefaultState).SetDraggedItem(DraggedType.MagicIcon, clickedItemIndex);
            
            ///* POPUP */
            //if (element ==  MagicAttribute.All && highlightedItemIndex != -1)
            //{
            //    ((MainGame)Cache.DefaultState).SetMagicPopup(Type, highlightedItemIndex, new Vector2(mouseX + 10, mouseY - 10), 0, 0);
            //}
            
            if (Utility.IsSelected(mouseX, mouseY, X, Y, X + frame.Width, Y + frame.Height))
                return true;
            else return false;
        }

        public void Update(GameTime gameTime)
        {
            mouseX = (int)Cache.DefaultState.Display.Mouse.X;
            mouseY = (int)Cache.DefaultState.Display.Mouse.Y;
            X = config.X;
            Y = config.Y;
            transparency = (Cache.GameSettings.TransparentDialogs ? 0.5F : 1F);
            player = ((MainGame)Cache.DefaultState).Player;
        }

        public void LeftClicked()
        {

                        
            /* PAGINATION SCROLL HIGHLIGHT*/
            if ((Y + 358 - 61 < mouseY) && (Y + 358 - 61 + 7 > mouseY))
            {
                if ((X + 155 + 140 < mouseX) && (X + 155 + 140 + 11 > mouseX)) { Scroll(1); }
                if ((X + 155 + 140 + 11 < mouseX) && (X + 155 + 140 + 11 + 11 > mouseX)) { Scroll(-1); }
            }

            if (clickedItemIndex != -1)
            {
                if (player.MagicLearned[clickedItemIndex])
                {
                    //Unlearn
                    ((MainGame)Cache.DefaultState).UnLearnSpell(clickedItemIndex);
                }
                else
                {
                    //Learn
                    ((MainGame)Cache.DefaultState).BuySpell(clickedItemIndex);
                }
            }
        }

        public void LeftDoubleClicked(ref SelectionMode selectionMode, ref int selectionModeId) {}

        public void LeftHeld(){ }

        public void LeftDragged(){ }

        public void LeftReleased(GameDialogBoxType highlightedDialogBox, int highlightedDialogBoxItemIndex) { }

        public void RightClicked() { }

        public void RightHeld() { }

        public void RightReleased() { }

        public void Scroll(int direction)
        {
            if (config.Page - direction > config.MaxPages) config.Page = 1;
            else if (config.Page - direction < 1) config.Page = config.MaxPages;
            else config.Page -= direction;
        }

        public void OffsetLocation(int x, int y)
        {
            config.X += x;
            config.Y += y;
        }

        public void Toggle(DialogBoxState state = DialogBoxState.Normal)
        {
            if (config.Visible && state == config.State) Hide();
            else
            {
                config.State = state;
                Show();
            }
        }

        public void Show()
        {
            if (!config.AlwaysVisible && Cache.DefaultState != null)
                ((IGameState)Cache.DefaultState).BringToFront(Type);
            highlightedItemIndex = -1;
            config.Hidden = false;
        }

        public void Hide()
        {
            if (!config.AlwaysVisible && Cache.DefaultState != null)
            {
                ((MainGame)Cache.DefaultState).DialogBoxDrawOrder.Remove(Type);
                ((IGameState)Cache.DefaultState).ClickedDialogBox = GameDialogBoxType.None;
            }
            config.Hidden = true;
            clickedItemIndex = selectedItemIndex = highlightedItemIndex = -1;
        }

        public void SetData(byte[] data)
        {

        }
    }
}



