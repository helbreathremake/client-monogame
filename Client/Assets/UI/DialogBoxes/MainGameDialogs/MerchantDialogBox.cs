﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

using Helbreath.Common;
using Helbreath.Common.Assets;
using Client.Assets.State;
using Client.Assets.Game;

namespace Client.Assets.UI
{
    public class MerchantDialogBox : IGameDialogBox
    {
        // IDIALOG
        private bool isUpdating;
        private int clickedItemIndex; // clicked item
        private int selectedItemIndex; // mouse over buy/sell
        private int selectedItemIndex2; // mouse over remove
        private int selectedItemIndexOffsetX;
        private int selectedItemIndexOffsetY;
        private int highlightedItemIndex; // chosen item (non volatile)
        private Vector2 highlightedItemLocation; // chose item location (non volatile)
        private GameDialogBoxConfiguration config;

        //Base graphic info
        int sprite = (int)SpriteId.DialogsV2 + 5; //Main texture
        int spriteFrame; //rectangle in texutre
        int spriteFrameHover = -1; //frame to display when hovering mouse over
        int width = -1; //frame width
        int height = -1; //frame height 
        AnimationFrame frame;

        public int ClickedItemIndex { get { return clickedItemIndex; } set { clickedItemIndex = value; } }
        public int SelectedItemIndex { get { return selectedItemIndex; } set { selectedItemIndex = value; } }
        public int HighlightedItemIndex { get { return highlightedItemIndex; } set { highlightedItemIndex = value; } }
        public int SelectedItemIndexOffsetX { get { return selectedItemIndexOffsetX; } set { selectedItemIndexOffsetX = value; } }
        public int SelectedItemIndexOffsetY { get { return selectedItemIndexOffsetY; } set { selectedItemIndexOffsetY = value; } }
        public GameDialogBoxType Type { get { return GameDialogBoxType.Merchant; } }
        public GameDialogBoxConfiguration Config { get { return config; } set { config = value; } }

        // MERCHANT DIALOG
        public int MerchantId;
        enum FilterMode
        {
            None,
            Stats,
            General
        }
        public List<int> SellList;
        public List<int> RepairList;
        private int totalBuyCount; // store actual total list of items (for pagination)
        private List<MerchantItem> buyList;
        private FilterMode filterMode;
        private Dictionary<ItemStat, bool> statFilters;
        private Dictionary<ItemCategory, bool> categoryFilters;
        private ItemQuality qualityFilter;
        private TextBox textFilter;
        private string previousTextFilter;

        public MerchantDialogBox()
        {
            config = new GameDialogBoxConfiguration(Type, Cache.GameSettings.Resolution);
            this.highlightedItemIndex = selectedItemIndex = selectedItemIndex2 = clickedItemIndex = -1;
            this.SellList = new List<int>();
            this.RepairList = new List<int>();
            this.buyList = new List<MerchantItem>();

            filterMode = FilterMode.None;
            statFilters = new Dictionary<ItemStat, bool>();
            foreach (ItemStat type in (ItemStat[])Enum.GetValues(typeof(ItemStat)))
                if (type != ItemStat.None) statFilters.Add(type, false);

            categoryFilters = new Dictionary<ItemCategory, bool>();
            foreach (ItemCategory type in (ItemCategory[])Enum.GetValues(typeof(ItemCategory)))
                if (type != ItemCategory.None && !categoryFilters.ContainsKey(type))
                    categoryFilters.Add(type, false);

            previousTextFilter = string.Empty;
            textFilter = new TextBox(Cache.BackgroundTexture, Cache.Fonts[FontType.GeneralSize10]);
            textFilter.X = config.X + 380;
            textFilter.Y = config.Y + 50;
            textFilter.Width = 160;
            textFilter.MaxCharacters = 120;
            textFilter.OnTextChanged += OnTextFilterChanged;

            frame = Cache.Interface[sprite].Frames[0];
            //Hide();
        }

        public MerchantDialogBox(GameDialogBoxConfiguration config)
        {
            this.config = config;
            this.highlightedItemIndex = selectedItemIndex = selectedItemIndex2 = clickedItemIndex = -1;
            this.SellList = new List<int>();
            this.RepairList = new List<int>();
            this.buyList = new List<MerchantItem>();

            filterMode = FilterMode.None;
            statFilters = new Dictionary<ItemStat, bool>();
            foreach (ItemStat type in (ItemStat[])Enum.GetValues(typeof(ItemStat)))
                if (type != ItemStat.None) statFilters.Add(type, false);

            categoryFilters = new Dictionary<ItemCategory, bool>();
            foreach (ItemCategory type in (ItemCategory[])Enum.GetValues(typeof(ItemCategory)))
                if (type != ItemCategory.None && !categoryFilters.ContainsKey(type))
                    categoryFilters.Add(type, false);

            previousTextFilter = string.Empty;
            textFilter = new TextBox(Cache.BackgroundTexture, Cache.Fonts[FontType.GeneralSize10]);
            textFilter.X = config.X + 380;
            textFilter.Y = config.Y + 50;
            textFilter.Width = 160;
            textFilter.MaxCharacters = 120;
            textFilter.OnTextChanged += OnTextFilterChanged;

            frame = Cache.Interface[sprite].Frames[0];
            //Hide();
        }

        private void OnTextFilterChanged(TextBox sender)
        {
            if (!sender.Text.Equals(previousTextFilter)) // only if changed
            {
                previousTextFilter = sender.Text;
                UpdateBuyList();
            }
        }

        public bool Draw(SpriteBatch spriteBatch, GameTime gameTime)
        {
            if (config.Hidden)
                if (!config.AlwaysVisible) return false; // dont draw dialogs if not a HUD item
                else if (Cache.GameSettings.LockedDialogs) return false;  // show HUD items when HUD is unlocked (transparent)

            if (((MainGame)Cache.DefaultState).Player.IsCasting && Cache.GameSettings.HideDialogDuringCasting) return false;

            int mouseX = (int)Cache.DefaultState.Display.Mouse.X;
            int mouseY = (int)Cache.DefaultState.Display.Mouse.Y;
            int X = config.X;
            int Y = config.Y;
            float transparency = ((Cache.GameSettings.TransparentDialogs || config.Hidden) ? 0.5F : 1F);
            Player player = ((MainGame)Cache.DefaultState).Player;
            highlightedItemIndex = selectedItemIndex = selectedItemIndex2 = -1;
            int indexY = 0; int indexX = 0;

            spriteBatch.Draw(Cache.Interface[sprite].Texture, new Vector2(X, Y), frame.GetRectangle(), Color.White * transparency);

            // buy button
            if (config.State == DialogBoxState.Buy)
            {
                spriteBatch.Draw(Cache.Interface[sprite].Texture, new Vector2(X + 20, Y + 15), Cache.Interface[sprite].Frames[14].GetRectangle(), Color.White * transparency);
                spriteBatch.Draw(Cache.Interface[sprite].Texture, new Vector2(X + 20 + 105, Y + 15), Cache.Interface[sprite].Frames[13].GetRectangle(), Color.White * transparency);
                spriteBatch.Draw(Cache.Interface[sprite].Texture, new Vector2(X + 20 + 210, Y + 15), Cache.Interface[sprite].Frames[13].GetRectangle(), Color.White * transparency);
                SpriteHelper.DrawTextCenteredWithShadow(spriteBatch, "Buy Mode", FontType.GeneralSize10, new Vector2(X + 20, Y + 18), 97, Color.White * transparency);
                SpriteHelper.DrawTextCenteredWithShadow(spriteBatch, "Sell Mode", FontType.GeneralSize10, new Vector2(X + 20 + 105, Y + 18), 97, Color.White * transparency);
                SpriteHelper.DrawTextCenteredWithShadow(spriteBatch, "Repair Mode", FontType.GeneralSize10, new Vector2(X + 20 + 210, Y + 18), 97, Color.White * transparency);
            }
            else if (config.State == DialogBoxState.Sell)
            {
                spriteBatch.Draw(Cache.Interface[sprite].Texture, new Vector2(X + 20, Y + 15), Cache.Interface[sprite].Frames[13].GetRectangle(), Color.White * transparency);
                spriteBatch.Draw(Cache.Interface[sprite].Texture, new Vector2(X + 20 + 105, Y + 15), Cache.Interface[sprite].Frames[14].GetRectangle(), Color.White * transparency);
                spriteBatch.Draw(Cache.Interface[sprite].Texture, new Vector2(X + 20 + 210, Y + 15), Cache.Interface[sprite].Frames[13].GetRectangle(), Color.White * transparency);
                SpriteHelper.DrawTextCenteredWithShadow(spriteBatch, "Buy Mode", FontType.GeneralSize10, new Vector2(X + 20, Y + 18), 97, Color.White * transparency);
                SpriteHelper.DrawTextCenteredWithShadow(spriteBatch, "Sell Mode", FontType.GeneralSize10, new Vector2(X + 20 + 105, Y + 18), 97, Color.White * transparency);
                SpriteHelper.DrawTextCenteredWithShadow(spriteBatch, "Repair Mode", FontType.GeneralSize10, new Vector2(X + 20 + 210, Y + 18), 97, Color.White * transparency);
            }
            else if (config.State == DialogBoxState.Repair)
            {
                spriteBatch.Draw(Cache.Interface[sprite].Texture, new Vector2(X + 20, Y + 15), Cache.Interface[sprite].Frames[13].GetRectangle(), Color.White * transparency);
                spriteBatch.Draw(Cache.Interface[sprite].Texture, new Vector2(X + 20 + 105, Y + 15), Cache.Interface[sprite].Frames[13].GetRectangle(), Color.White * transparency);
                spriteBatch.Draw(Cache.Interface[sprite].Texture, new Vector2(X + 20 + 210, Y + 15), Cache.Interface[sprite].Frames[14].GetRectangle(), Color.White * transparency);
                SpriteHelper.DrawTextCenteredWithShadow(spriteBatch, "Buy Mode", FontType.GeneralSize10, new Vector2(X + 20, Y + 18), 97, Color.White * transparency);
                SpriteHelper.DrawTextCenteredWithShadow(spriteBatch, "Sell Mode", FontType.GeneralSize10, new Vector2(X + 20 + 105, Y + 18), 97, Color.White * transparency);
                SpriteHelper.DrawTextCenteredWithShadow(spriteBatch, "Repair Mode", FontType.GeneralSize10, new Vector2(X + 20 + 210, Y + 18), 97, Color.White * transparency);
            }

            /* GOLD */
            spriteBatch.Draw(Cache.Interface[(int)SpriteId.DialogsV2 + 2].Texture, new Vector2(X + 20 + 325, Y + 19), Cache.Interface[(int)SpriteId.DialogsV2 + 2].Frames[5].GetRectangle(), Color.White * transparency);
            SpriteHelper.DrawTextWithShadow(spriteBatch, string.Format("{0}", player.Gold), FontType.GeneralSize10, new Vector2(X + 20 + 345, Y + 17), Cache.Colors[GameColor.Orange]);

            switch (config.State)
            {
                case DialogBoxState.Buy:
                    switch (filterMode)
                    {
                        case FilterMode.None:
                            for (int i = 0; i < 7; i++)
                                if (i < buyList.Count)
                                {
                                    spriteBatch.Draw(Cache.Interface[sprite].Texture, new Vector2(X + 65, Y + 81 + (i * 42)), Cache.Interface[sprite].Frames[12].GetRectangle(), Color.White * transparency);

                                    if (Utility.IsSelected(mouseX, mouseY, X + 65, Y + 81 + (i * 42), X + 65 + 513, Y + 81 + (i * 42) + 42))
                                    {
                                        highlightedItemIndex = i;
                                        highlightedItemLocation = new Vector2(68, 86 + (i * 42));
                                    }

                                    Color displayColour = (highlightedItemIndex == i ? Cache.Colors[GameColor.Orange] : Color.White);

                                    Item item = buyList[i].Item;

                                    // get sprite frame
                                    Rectangle f = Cache.Equipment[(int)SpriteId.ItemGround + item.Sprite].Frames[item.SpriteFrame].GetRectangle();
                                    // get draw location offset from center of the slot
                                    Rectangle draw = new Rectangle(X + 70 + 18 - (f.Width > 30 || f.Height > 30 ? 30 : f.Width) / 2,
                                                                   Y + 86 + (i * 42) + 17 - (f.Width > 30 || f.Height > 30 ? 30 : f.Height) / 2,
                                                                   f.Width,
                                                                   f.Height
                                                                   );

                                    if (f.Width > 30 || f.Height > 30)
                                        spriteBatch.Draw(Cache.Equipment[(int)SpriteId.ItemGround + item.Sprite].Texture, new Rectangle(draw.X, draw.Y, 30, 30), f, (item.ColorType == GameColor.Custom) ? SpriteHelper.ColourFromArgb(item.Colour) : Cache.Colors[item.ColorType]);
                                    else spriteBatch.Draw(Cache.Equipment[(int)SpriteId.ItemGround + item.Sprite].Texture, new Vector2(draw.X, draw.Y), f, (item.ColorType == GameColor.Custom) ? SpriteHelper.ColourFromArgb(item.Colour) : Cache.Colors[item.ColorType]);

                                    // name
                                    SpriteHelper.DrawTextWithShadow(spriteBatch, string.Format("{0} {1} {2}", (item.Quality != ItemQuality.None ? item.Quality.ToString() : ""), item.FriendlyName, (item.Count > 1 ? "(x" + item.Count + ")" : "")).Trim(), FontType.GeneralSize10, new Vector2(X + 135, Y + 93 + (i * 42)), displayColour * transparency);

                                    // endurance as a percentage over the top of the item image
                                    double endurance = (((double)item.Endurance / (double)item.MaximumEndurance) * 100);
                                    if (item.IsBroken)
                                        SpriteHelper.DrawTextCenteredWithShadow(spriteBatch, "Broken", FontType.DialogsSmallerSize7, new Vector2(X + 70, Y + 108 + (i * 42)), 40, (endurance < 100 ? Cache.Colors[GameColor.Enemy] : displayColour) * transparency);
                                    else SpriteHelper.DrawTextCenteredWithShadow(spriteBatch, ((int)Math.Max(1, endurance)).ToString() + "%", FontType.DialogsSmallerSize7, new Vector2(X + 70, Y + 108 + (i * 42)), 40, (endurance < 100 ? Cache.Colors[GameColor.Enemy] : displayColour) * transparency);

                                    // value
                                    spriteBatch.Draw(Cache.Interface[(int)SpriteId.DialogsV2 + 2].Texture, new Vector2(X + 335, Y + 93 + (i * 42)), Cache.Interface[(int)SpriteId.DialogsV2 + 2].Frames[5].GetRectangle(), Color.White * transparency);
                                    SpriteHelper.DrawTextWithShadow(spriteBatch, string.Format("{0}", item.GetGoldValue()), FontType.GeneralSize10, new Vector2(X + 355, Y + 92 + (i * 42)), (player.Gold < item.GetGoldValue() ? Cache.Colors[GameColor.Enemy] : displayColour) * transparency);

                                    // buy button
                                    if (Utility.IsSelected(mouseX, mouseY, X + 485, Y + 87 + (i * 42), X + 485 + 65, X + 87 + (i * 42) + 33))
                                    {
                                        spriteBatch.Draw(Cache.Interface[sprite].Texture, new Vector2(X + 485, Y + 87 + (i * 42)), Cache.Interface[sprite].Frames[16].GetRectangle(), Color.White * transparency);
                                        SpriteHelper.DrawTextCenteredWithShadow(spriteBatch, "Buy", FontType.GeneralSize10, new Vector2(X + 485, Y + 90 + (i * 42)), 65, Color.White * transparency);
                                        selectedItemIndex = highlightedItemIndex;
                                    }
                                    else
                                    {
                                        spriteBatch.Draw(Cache.Interface[sprite].Texture, new Vector2(X + 485, Y + 87 + (i * 42)), Cache.Interface[sprite].Frames[15].GetRectangle(), Color.White * transparency);
                                        SpriteHelper.DrawTextCenteredWithShadow(spriteBatch, "Buy", FontType.GeneralSize10, new Vector2(X + 485, Y + 90 + (i * 42)), 65, Color.White * transparency);
                                    }
                                }

                            /* POPUP */
                            if (highlightedItemIndex >= 0 && highlightedItemIndex < Globals.MaximumTotalItems)
                            {
                                /* ITEM DETAILS */
                                if (buyList[highlightedItemIndex] != null)
                                    ((MainGame)Cache.DefaultState).SetItemPopup(Type, buyList[highlightedItemIndex].Item, highlightedItemLocation, X, Y);
                                    //ItemPopup.DrawItemPopup(spriteBatch, buyList[highlightedItemIndex].Item, highlightedItemLocation, X, Y);
                            }


                            /* PAGINATION */
                            if (config.MaxPages > 1)
                            {
                                spriteBatch.Draw(Cache.Interface[(int)SpriteId.DialogsV2 + 2].Texture, new Vector2(X + 554, Y + 382), Cache.Interface[(int)SpriteId.DialogsV2 + 2].Frames[9].GetRectangle(), Color.White * transparency);
                                SpriteHelper.DrawTextRightWithShadow(spriteBatch, (config.Page) + "/" + (config.MaxPages), FontType.DialogsSmallerSize7, new Vector2(X + 524, Y + 382), 25, Cache.Colors[GameColor.Orange]);
                            }
                            break;
                        case FilterMode.Stats:
                            foreach (KeyValuePair<ItemStat, bool> statFilter in statFilters)
                            {
                                if (statFilter.Value)
                                {
                                    spriteBatch.Draw(Cache.Interface[sprite].Texture, new Vector2(X + 85 + (indexX * 120), Y + 90 + (indexY * 32)), Cache.Interface[sprite].Frames[18].GetRectangle(), Color.White * transparency);
                                    SpriteHelper.DrawTextCenteredWithShadow(spriteBatch, ItemHelper.GetAbbreviatedName(statFilter.Key), FontType.GeneralSize10, new Vector2(X + 85 + (indexX * 120), X + 95 + (indexY * 32)), 117, Color.White * transparency);
                                }
                                else
                                {
                                    spriteBatch.Draw(Cache.Interface[sprite].Texture, new Vector2(X + 85 + (indexX * 120), Y + 90 + (indexY * 32)), Cache.Interface[sprite].Frames[7].GetRectangle(), Color.White * transparency);
                                    if (Utility.IsSelected(mouseX, mouseY, X + 85 + (indexX * 120), Y + 90 + (indexY * 32), X + 85 + (indexX * 120) + 117, Y + 90 + (indexY * 32) + 33))
                                        SpriteHelper.DrawTextCenteredWithShadow(spriteBatch, ItemHelper.GetAbbreviatedName(statFilter.Key), FontType.GeneralSize10, new Vector2(X + 85 + (indexX * 120), Y + 95 + (indexY * 32)), 117, Cache.Colors[GameColor.Orange] * transparency);
                                    else SpriteHelper.DrawTextCenteredWithShadow(spriteBatch, ItemHelper.GetAbbreviatedName(statFilter.Key), FontType.GeneralSize10, new Vector2(X + 85 + (indexX * 120), Y + 95 + (indexY * 32)), 117, Color.White * transparency);
                                }
                                indexY++;
                                if (indexY > 0 && indexY % 9 == 0)
                                {
                                    indexX++;
                                    indexY = 0;
                                }
                            }
                            break;
                        case FilterMode.General:
                            foreach (KeyValuePair<ItemCategory, bool> categoryFilter in categoryFilters)
                            {
                                if (categoryFilter.Value)
                                {
                                    spriteBatch.Draw(Cache.Interface[sprite].Texture, new Vector2(X + 85 + (indexX * 120), Y + 90 + (indexY * 32)), Cache.Interface[sprite].Frames[18].GetRectangle(), Color.White * transparency);
                                    SpriteHelper.DrawTextCenteredWithShadow(spriteBatch, ItemHelper.GetAbbreviatedName(categoryFilter.Key), FontType.GeneralSize10, new Vector2(X + 85 + (indexX * 120), Y + 95 + (indexY * 32)), 117, Color.White * transparency);
                                }
                                else
                                {
                                    spriteBatch.Draw(Cache.Interface[sprite].Texture, new Vector2(X + 85 + (indexX * 120), Y + 90 + (indexY * 32)), Cache.Interface[sprite].Frames[7].GetRectangle(), Color.White * transparency);
                                    if (Utility.IsSelected(mouseX, mouseY, X + 85 + (indexX * 120), Y + 90 + (indexY * 32), X + 85 + (indexX * 120) + 117, Y + 90 + (indexY * 32) + 33))
                                        SpriteHelper.DrawTextCenteredWithShadow(spriteBatch, ItemHelper.GetAbbreviatedName(categoryFilter.Key), FontType.GeneralSize10, new Vector2(X + 85 + (indexX * 120), Y + 95 + (indexY * 32)), 117, Cache.Colors[GameColor.Orange] * transparency);
                                    else SpriteHelper.DrawTextCenteredWithShadow(spriteBatch, ItemHelper.GetAbbreviatedName(categoryFilter.Key), FontType.GeneralSize10, new Vector2(X + 85 + (indexX * 120), Y + 95 + (indexY * 32)), 117, Color.White * transparency);
                                }
                                indexY++;
                                if (indexY > 0 && indexY % 9 == 0)
                                {
                                    indexX++;
                                    indexY = 0;
                                }
                            }
                            break;
                    }

                    /* STAT FILTERS */
                    if (Utility.IsSelected(mouseX, mouseY, X + 85, Y + 87 - 42, X + 85 + 117, Y + 87 - 42 + 33))
                    {
                        spriteBatch.Draw(Cache.Interface[sprite].Texture, new Vector2(X + 85, Y + 87 - 42), Cache.Interface[sprite].Frames[8].GetRectangle(), Color.White * transparency);
                        SpriteHelper.DrawTextCenteredWithShadow(spriteBatch, "Stats (" + statFilters.Count(kvp => kvp.Value) + ")", FontType.GeneralSize10, new Vector2(X + 85, Y + 92 - 42), 117, Color.White * transparency);
                    }
                    else
                    {
                        spriteBatch.Draw(Cache.Interface[sprite].Texture, new Vector2(X + 85, Y + 87 - 42), Cache.Interface[sprite].Frames[7].GetRectangle(), Color.White * transparency);
                        SpriteHelper.DrawTextCenteredWithShadow(spriteBatch, "Stats (" + statFilters.Count(kvp => kvp.Value) + ")", FontType.GeneralSize10, new Vector2(X + 85, Y + 92 - 42), 117, Color.White * transparency);
                    }

                    /* CATEGORY FILTERS */
                    if (Utility.IsSelected(mouseX, mouseY, X + 205, Y + 87 - 42, X + 205 + 117, Y + 87 - 42 + 33))
                    {
                        spriteBatch.Draw(Cache.Interface[sprite].Texture, new Vector2(X + 205, Y + 87 - 42), Cache.Interface[sprite].Frames[8].GetRectangle(), Color.White * transparency);
                        SpriteHelper.DrawTextCenteredWithShadow(spriteBatch, "Categories (" + categoryFilters.Count(kvp => kvp.Value) + ")", FontType.GeneralSize10, new Vector2(X + 205, Y + 92 - 42), 117, Color.White * transparency);
                    }
                    else
                    {
                        spriteBatch.Draw(Cache.Interface[sprite].Texture, new Vector2(X + 205, Y + 87 - 42), Cache.Interface[sprite].Frames[7].GetRectangle(), Color.White * transparency);
                        SpriteHelper.DrawTextCenteredWithShadow(spriteBatch, "Categories (" + categoryFilters.Count(kvp => kvp.Value) + ")", FontType.GeneralSize10, new Vector2(X + 205, Y + 92 - 42), 117, Color.White * transparency);
                    }

                    /* TEXT FILTER */
                    spriteBatch.Draw(Cache.Interface[sprite].Texture, new Vector2(X + 370, Y + 45), Cache.Interface[sprite].Frames[10].GetRectangle(), Color.White * transparency);
                    textFilter.Draw(spriteBatch, gameTime);
                    break;
                case DialogBoxState.Sell:
                    if (SellList.Count <= 0)
                    {
                        SpriteHelper.DrawTextCenteredWithShadow(spriteBatch, "Drag items here to sell", FontType.GeneralSize10, new Vector2(X, Y + 90), frame.Width, Color.White * transparency);
                        SpriteHelper.DrawTextCenteredWithShadow(spriteBatch, "Or double-click items in your inventory", FontType.GeneralSize10, new Vector2(X, Y + 105), frame.Width, Color.White * transparency);
                    }

                    for (int i = 0; i < 7; i++)
                    {
                        int listIndex = Math.Max(0, ((config.Page - 1) * 7) + i);
                        if (listIndex < SellList.Count && SellList[listIndex] >= 0)
                        {
                            spriteBatch.Draw(Cache.Interface[sprite].Texture, new Vector2(X + 65, Y + 81 + (i * 42)), Cache.Interface[sprite].Frames[12].GetRectangle(), Color.White * transparency);

                            if (Utility.IsSelected(mouseX, mouseY, X + 65, Y + 81 + (i * 42), X + 65 + 513, Y + 81 + (i * 42) + 42))
                            {
                                highlightedItemIndex = listIndex;
                                highlightedItemLocation = new Vector2(68, 86 + (i * 42));
                            }

                            Color displayColour = (highlightedItemIndex == listIndex ? Cache.Colors[GameColor.Orange] : Color.White);

                            Item item = player.Inventory[SellList[listIndex]];

                            if (item != null)
                            {
                                // get sprite frame
                                Rectangle f = Cache.Equipment[(int)SpriteId.ItemGround + item.Sprite].Frames[item.SpriteFrame].GetRectangle();
                                // get draw location offset from center of the slot
                                Rectangle draw = new Rectangle(X + 70 + 18 - (f.Width > 30 || f.Height > 30 ? 30 : f.Width) / 2,
                                                               Y + 86 + (i * 42) + 17 - (f.Width > 30 || f.Height > 30 ? 30 : f.Height) / 2,
                                                               f.Width,
                                                               f.Height
                                                               );

                                if (f.Width > 30 || f.Height > 30)
                                    spriteBatch.Draw(Cache.Equipment[(int)SpriteId.ItemGround + item.Sprite].Texture, new Rectangle(draw.X, draw.Y, 30, 30), f, (item.ColorType == GameColor.Custom) ? SpriteHelper.ColourFromArgb(item.Colour) : Cache.Colors[item.ColorType]);
                                else spriteBatch.Draw(Cache.Equipment[(int)SpriteId.ItemGround + item.Sprite].Texture, new Vector2(draw.X, draw.Y), f, (item.ColorType == GameColor.Custom) ? SpriteHelper.ColourFromArgb(item.Colour) : Cache.Colors[item.ColorType]);

                                // name
                                SpriteHelper.DrawTextWithShadow(spriteBatch, string.Format("{0} {1} {2}", (item.Quality != ItemQuality.None ? item.Quality.ToString() : ""), item.FriendlyName, (item.Count > 1 ? "(x" + item.Count + ")" : "")).Trim(), FontType.GeneralSize10, new Vector2(X + 135, Y + 93 + (i * 42)), displayColour * transparency);

                                // endurance as a percentage over the top of the item image
                                double endurance = (((double)item.Endurance / (double)item.MaximumEndurance) * 100);
                                if (item.IsBroken)
                                    SpriteHelper.DrawTextCenteredWithShadow(spriteBatch, "Broken", FontType.DialogsSmallerSize7, new Vector2(X + 70, Y + 108 + (i * 42)), 40, (endurance < 100 ? Cache.Colors[GameColor.Enemy] : displayColour) * transparency);
                                else SpriteHelper.DrawTextCenteredWithShadow(spriteBatch, ((int)Math.Max(1, endurance)).ToString() + "%", FontType.DialogsSmallerSize7, new Vector2(X + 70, Y + 108 + (i * 42)), 40, (endurance < 100 ? Cache.Colors[GameColor.Enemy] : displayColour) * transparency);

                                // value - TODO - halved price, sort discounts etc
                                spriteBatch.Draw(Cache.Interface[(int)SpriteId.DialogsV2 + 2].Texture, new Vector2(X + 335, Y + 93 + (i * 42)), Cache.Interface[(int)SpriteId.DialogsV2 + 2].Frames[5].GetRectangle(), Color.White * transparency);
                                SpriteHelper.DrawTextWithShadow(spriteBatch, string.Format("{0}", (item.GetGoldValue() / 2)), FontType.GeneralSize10, new Vector2(X + 355, Y + 92 + (i * 42)), displayColour * transparency);

                                // Remove button
                                if (Utility.IsSelected(mouseX, mouseY, X + 412, Y + 87 + (i * 42), X + 412 + 65, Y + 87 + (i * 42) + 33))
                                {
                                    spriteBatch.Draw(Cache.Interface[sprite].Texture, new Vector2(X + 412, Y + 87 + (i * 42)), Cache.Interface[sprite].Frames[16].GetRectangle(), Color.White * transparency);
                                    SpriteHelper.DrawTextCenteredWithShadow(spriteBatch, "Remove", FontType.GeneralSize10, new Vector2(X + 412, Y + 90 + (i * 42)), 65, Color.White * transparency);
                                    selectedItemIndex2 = highlightedItemIndex;
                                }
                                else
                                {
                                    spriteBatch.Draw(Cache.Interface[sprite].Texture, new Vector2(X + 412, Y + 87 + (i * 42)), Cache.Interface[sprite].Frames[15].GetRectangle(), Color.White * transparency);
                                    SpriteHelper.DrawTextCenteredWithShadow(spriteBatch, "Remove", FontType.GeneralSize10, new Vector2(X + 412, Y + 90 + (i * 42)), 65, Color.White * transparency);
                                }

                                // sell button
                                if (Utility.IsSelected(mouseX, mouseY, X + 485, Y + 87 + (i * 42), X + 485 + 65, Y + 87 + (i * 42) + 33))
                                {
                                    spriteBatch.Draw(Cache.Interface[sprite].Texture, new Vector2(X + 485, Y + 87 + (i * 42)), Cache.Interface[sprite].Frames[16].GetRectangle(), Color.White * transparency);
                                    SpriteHelper.DrawTextCenteredWithShadow(spriteBatch, "Sell", FontType.GeneralSize10, new Vector2(X + 485, Y + 90 + (i * 42)), 65, Color.White * transparency);
                                    selectedItemIndex = highlightedItemIndex;
                                }
                                else
                                {
                                    spriteBatch.Draw(Cache.Interface[sprite].Texture, new Vector2(X + 485, Y + 87 + (i * 42)), Cache.Interface[sprite].Frames[15].GetRectangle(), Color.White * transparency);
                                    SpriteHelper.DrawTextCenteredWithShadow(spriteBatch, "Sell", FontType.GeneralSize10, new Vector2(X + 485, Y + 90 + (i * 42)), 65, Color.White * transparency);
                                }
                            }
                        }
                    }

                    // sell all button
                    if (SellList.Count > 0)
                        if (Utility.IsSelected(mouseX, mouseY, X + 485, Y + 87 -42, X + 485 + 65, Y + 87 -42 + 33))
                        {
                            spriteBatch.Draw(Cache.Interface[sprite].Texture, new Vector2(X + 485, Y + 87 -42), Cache.Interface[sprite].Frames[16].GetRectangle(), Color.White * transparency);
                            SpriteHelper.DrawTextCenteredWithShadow(spriteBatch, "Sell All", FontType.GeneralSize10, new Vector2(X + 485, Y + 90 -  42), 65, Color.White * transparency);
                            selectedItemIndex = highlightedItemIndex;
                        }
                        else
                        {
                            spriteBatch.Draw(Cache.Interface[sprite].Texture, new Vector2(X + 485, Y + 87 - 42), Cache.Interface[sprite].Frames[15].GetRectangle(), Color.White * transparency);
                            SpriteHelper.DrawTextCenteredWithShadow(spriteBatch, "Sell All", FontType.GeneralSize10, new Vector2(X + 485, Y + 90 -42), 65, Color.White * transparency);
                        }

                    /* PAGINATION */
                    if (config.MaxPages > 1)
                    {
                        spriteBatch.Draw(Cache.Interface[(int)SpriteId.DialogsV2 + 2].Texture, new Vector2(X + 554, Y + 382), Cache.Interface[(int)SpriteId.DialogsV2 + 2].Frames[9].GetRectangle(), Color.White * transparency);
                        SpriteHelper.DrawTextRightWithShadow(spriteBatch, (config.Page) + "/" + (config.MaxPages), FontType.DialogsSmallerSize7, new Vector2(X + 524, Y + 382), 25, Cache.Colors[GameColor.Orange]);
                    }             

                    /* POPUP */
                    if (highlightedItemIndex >= 0 && highlightedItemIndex < Globals.MaximumTotalItems)
                    {
                        /* ITEM DETAILS */
                        if (SellList[highlightedItemIndex] != -1 && player.Inventory[SellList[highlightedItemIndex]] != null)
                            ((MainGame)Cache.DefaultState).SetItemPopup(Type, player.Inventory[SellList[highlightedItemIndex]], highlightedItemLocation, X, Y);
                            //ItemPopup.DrawItemPopup(spriteBatch, player.Inventory[SellList[highlightedItemIndex]], highlightedItemLocation, X, Y);
                    }
                    break;
                case DialogBoxState.Repair:
                    if (RepairList.Count <= 0)
                    {
                        SpriteHelper.DrawTextCenteredWithShadow(spriteBatch, "Drag items here to repair", FontType.GeneralSize10, new Vector2(X, Y + 90), frame.Width, Color.White * transparency);
                        SpriteHelper.DrawTextCenteredWithShadow(spriteBatch, "Or double-click items in your inventory", FontType.GeneralSize10, new Vector2(X, Y + 105), frame.Width, Color.White * transparency);
                    }

                    for (int i = 0; i < 7; i++)
                    {
                        int listIndex = Math.Max(0, ((config.Page - 1) * 7) + i);
                        if (listIndex < RepairList.Count && RepairList[listIndex] >= 0)
                        {
                            spriteBatch.Draw(Cache.Interface[sprite].Texture, new Vector2(X + 65, Y + 81 + (i * 42)), Cache.Interface[sprite].Frames[12].GetRectangle(), Color.White * transparency);

                            if (Utility.IsSelected(mouseX, mouseY, X + 65, Y + 81 + (i * 42), X + 65 + 513, Y + 81 + (i * 42) + 42))
                            {
                                highlightedItemIndex = listIndex;
                                highlightedItemLocation = new Vector2(68, 86 + (i * 42));
                            }

                            Color displayColour = (highlightedItemIndex == listIndex ? Cache.Colors[GameColor.Orange] : Color.White);

                            Item item = player.Inventory[RepairList[listIndex]];

                            if (item != null)
                            {
                                // get sprite frame
                                Rectangle f = Cache.Equipment[(int)SpriteId.ItemGround + item.Sprite].Frames[item.SpriteFrame].GetRectangle();
                                // get draw location offset from center of the slot
                                Rectangle draw = new Rectangle(X + 70 + 18 - (f.Width > 30 || f.Height > 30 ? 30 : f.Width) / 2,
                                                               Y + 86 + (i * 42) + 17 - (f.Width > 30 || f.Height > 30 ? 30 : f.Height) / 2,
                                                               f.Width,
                                                               f.Height
                                                               );

                                if (f.Width > 30 || f.Height > 30)
                                    spriteBatch.Draw(Cache.Equipment[(int)SpriteId.ItemGround + item.Sprite].Texture, new Rectangle(draw.X, draw.Y, 30, 30), f, (item.ColorType == GameColor.Custom) ? SpriteHelper.ColourFromArgb(item.Colour) : Cache.Colors[item.ColorType]);
                                else spriteBatch.Draw(Cache.Equipment[(int)SpriteId.ItemGround + item.Sprite].Texture, new Vector2(draw.X, draw.Y), f, (item.ColorType == GameColor.Custom) ? SpriteHelper.ColourFromArgb(item.Colour) : Cache.Colors[item.ColorType]);

                                // name
                                SpriteHelper.DrawTextWithShadow(spriteBatch, string.Format("{0} {1}", (item.Quality != ItemQuality.None ? item.Quality.ToString() : ""), item.FriendlyName).Trim(), FontType.GeneralSize10, new Vector2(X + 135, Y + 93 + (i * 42)), displayColour * transparency);

                                // endurance as a percentage over the top of the item image
                                double endurance = (((double)item.Endurance / (double)item.MaximumEndurance) * 100);
                                if (item.IsBroken)
                                    SpriteHelper.DrawTextCenteredWithShadow(spriteBatch, "Broken", FontType.DialogsSmallerSize7, new Vector2(X + 70, Y + 108 + (i * 42)), 40, (endurance < 100 ? Cache.Colors[GameColor.Enemy] : displayColour) * transparency);
                                else SpriteHelper.DrawTextCenteredWithShadow(spriteBatch, ((int)Math.Max(1, endurance)).ToString() + "%", FontType.DialogsSmallerSize7, new Vector2(X + 70, Y + 108 + (i * 42)), 40, (endurance < 100 ? Cache.Colors[GameColor.Enemy] : displayColour) * transparency);

                                // base repair cost - TODO - halved price, sort discounts etc
                                int repairPrice = item.GetRepairCost();

                                // draw repair cost
                                spriteBatch.Draw(Cache.Interface[(int)SpriteId.DialogsV2 + 2].Texture, new Vector2(X + 335, Y + 93 + (i * 42)), Cache.Interface[(int)SpriteId.DialogsV2 + 2].Frames[5].GetRectangle(), Color.White * transparency);
                                SpriteHelper.DrawTextWithShadow(spriteBatch, string.Format("{0}", repairPrice), FontType.GeneralSize10, new Vector2(X + 355, Y + 92 + (i * 42)), (player.Gold < repairPrice ? Cache.Colors[GameColor.Enemy] : displayColour) * transparency);

                                // Remove button
                                if (Utility.IsSelected(mouseX, mouseY, X + 412, Y + 87 + (i * 42), X + 412 + 65, Y + 87 + (i * 42) + 33))
                                {
                                    spriteBatch.Draw(Cache.Interface[sprite].Texture, new Vector2(X + 412, Y + 87 + (i * 42)), Cache.Interface[sprite].Frames[16].GetRectangle(), Color.White * transparency);
                                    SpriteHelper.DrawTextCenteredWithShadow(spriteBatch, "Remove", FontType.GeneralSize10, new Vector2(X + 412, Y + 90 + (i * 42)), 65, Color.White * transparency);
                                    selectedItemIndex2 = highlightedItemIndex;
                                }
                                else
                                {
                                    spriteBatch.Draw(Cache.Interface[sprite].Texture, new Vector2(X + 412, Y + 87 + (i * 42)), Cache.Interface[sprite].Frames[15].GetRectangle(), Color.White * transparency);
                                    SpriteHelper.DrawTextCenteredWithShadow(spriteBatch, "Remove", FontType.GeneralSize10, new Vector2(X + 412, Y + 90 + (i * 42)), 65, Color.White * transparency);
                                }

                                // repair button
                                if (Utility.IsSelected(mouseX, mouseY, X + 485, Y + 87 + (i * 42), X + 485 + 65, Y + 87 + (i * 42) + 33))
                                {
                                    spriteBatch.Draw(Cache.Interface[sprite].Texture, new Vector2(X + 485, Y + 87 + (i * 42)), Cache.Interface[sprite].Frames[16].GetRectangle(), Color.White * transparency);
                                    SpriteHelper.DrawTextCenteredWithShadow(spriteBatch, "Repair", FontType.GeneralSize10, new Vector2(X + 485, Y + 90 + (i * 42)), 65, Color.White * transparency);
                                    selectedItemIndex = highlightedItemIndex;
                                }
                                else
                                {
                                    spriteBatch.Draw(Cache.Interface[sprite].Texture, new Vector2(X + 485, Y + 87 + (i * 42)), Cache.Interface[sprite].Frames[15].GetRectangle(), Color.White * transparency);
                                    SpriteHelper.DrawTextCenteredWithShadow(spriteBatch, "Repair", FontType.GeneralSize10, new Vector2(X + 485, Y + 90 + (i * 42)), 65, Color.White * transparency);
                                }
                            }
                        }
                    }

                    // add equipment to repair button
                    if (Utility.IsSelected(mouseX, mouseY, X + 85, Y + 87 - 42, X + 85 + 117, Y + 87 - 42 + 33))
                    {
                        spriteBatch.Draw(Cache.Interface[sprite].Texture, new Vector2(X + 85, Y + 87 - 42), Cache.Interface[sprite].Frames[8].GetRectangle(), Color.White * transparency);
                        SpriteHelper.DrawTextCenteredWithShadow(spriteBatch, "Add Equipment", FontType.GeneralSize10, new Vector2(X + 85, Y + 92 - 42), 117, Color.White * transparency);
                    }
                    else
                    {
                        spriteBatch.Draw(Cache.Interface[sprite].Texture, new Vector2(X + 85, Y + 87 - 42), Cache.Interface[sprite].Frames[7].GetRectangle(), Color.White * transparency);
                        SpriteHelper.DrawTextCenteredWithShadow(spriteBatch, "Add Equipment", FontType.GeneralSize10, new Vector2(X + 85, Y + 92 - 42), 117, Color.White * transparency);
                    }

                    // add inventory to repair button
                    if (Utility.IsSelected(mouseX, mouseY, X + 205, Y + 87 - 42, X + 205 + 117, Y + 87 - 42 + 33))
                    {
                        spriteBatch.Draw(Cache.Interface[sprite].Texture, new Vector2(X + 205, Y + 87 - 42), Cache.Interface[sprite].Frames[8].GetRectangle(), Color.White * transparency);
                        SpriteHelper.DrawTextCenteredWithShadow(spriteBatch, "Add Inventory", FontType.GeneralSize10, new Vector2(X + 205, Y + 92 - 42), 117, Color.White * transparency);
                    }
                    else
                    {
                        spriteBatch.Draw(Cache.Interface[sprite].Texture, new Vector2(X + 205, Y + 87 - 42), Cache.Interface[sprite].Frames[7].GetRectangle(), Color.White * transparency);
                        SpriteHelper.DrawTextCenteredWithShadow(spriteBatch, "Add Inventory", FontType.GeneralSize10, new Vector2(X + 205, Y + 92 - 42), 117, Color.White * transparency);
                    }

                    // repair all button
                    if (RepairList.Count > 0)
                        if (Utility.IsSelected(mouseX, mouseY, X + 485, Y + 87 - 42, X + 485 + 65, Y + 87 - 42 + 33))
                        {
                            spriteBatch.Draw(Cache.Interface[sprite].Texture, new Vector2(X + 485, Y + 87 - 42), Cache.Interface[sprite].Frames[16].GetRectangle(), Color.White * transparency);
                            SpriteHelper.DrawTextCenteredWithShadow(spriteBatch, "Repair All", FontType.GeneralSize10, new Vector2(X + 485, Y + 90 - 42), 65, Color.White * transparency);
                            selectedItemIndex = highlightedItemIndex;
                        }
                        else
                        {
                            spriteBatch.Draw(Cache.Interface[sprite].Texture, new Vector2(X + 485, Y + 87 - 42), Cache.Interface[sprite].Frames[15].GetRectangle(), Color.White * transparency);
                            SpriteHelper.DrawTextCenteredWithShadow(spriteBatch, "Repair All", FontType.GeneralSize10, new Vector2(X + 485, Y + 90 - 42), 65, Color.White * transparency);
                        }

                    /* PAGINATION */
                    if (config.MaxPages > 1)
                    {
                        spriteBatch.Draw(Cache.Interface[(int)SpriteId.DialogsV2 + 2].Texture, new Vector2(X + 554, Y + 382), Cache.Interface[(int)SpriteId.DialogsV2 + 2].Frames[9].GetRectangle(), Color.White * transparency);
                        SpriteHelper.DrawTextRightWithShadow(spriteBatch, (config.Page) + "/" + (config.MaxPages), FontType.DialogsSmallerSize7, new Vector2(X + 524, Y + 382), 25, Cache.Colors[GameColor.Orange]);
                    }             

                    /* POPUP */
                    if (highlightedItemIndex >= 0 && highlightedItemIndex < Globals.MaximumTotalItems)
                    {
                        /* ITEM DETAILS */
                        if (RepairList[highlightedItemIndex] != -1 && player.Inventory[RepairList[highlightedItemIndex]] != null)
                            ((MainGame)Cache.DefaultState).SetItemPopup(Type, player.Inventory[RepairList[highlightedItemIndex]], highlightedItemLocation, X, Y);
                            //ItemPopup.DrawItemPopup(spriteBatch, player.Inventory[RepairList[highlightedItemIndex]], highlightedItemLocation, X, Y);
                    }
                    break;
            }

            if (frame != null && Utility.IsSelected(mouseX, mouseY, X, Y, X + frame.Width, Y + frame.Height))
                return true;
            else return false;
        }

        public void Update(GameTime gameTime)
        {
            textFilter.Update(gameTime);
        }

        private void UpdateBuyList()
        {
            if (config.State == DialogBoxState.Buy && filterMode == FilterMode.None && !isUpdating)
            {
                ((MainGame)Cache.DefaultState).GetMerchantData(MerchantId, config.Page, ItemDisplayGroup.All, statFilters, categoryFilters, previousTextFilter);
                isUpdating = true;
            }
        }

        public void LeftClicked()
        {
            int mouseX = (int)Cache.DefaultState.Display.Mouse.X;
            int mouseY = (int)Cache.DefaultState.Display.Mouse.Y;
            int X = config.X;
            int Y = config.Y;
            Player player = ((MainGame)Cache.DefaultState).Player;
            int indexY = 0; int indexX = 0;

            if (Utility.IsSelected(mouseX, mouseY, X + 20, Y + 15, X + 10 + 97, Y + 15 + 32))
            {
                config.State = DialogBoxState.Buy;
                config.MaxPages = (totalBuyCount / 7) + (totalBuyCount % 7 > 0 ? 1 : 0);
                config.Page = 1;
                filterMode = FilterMode.None;
                UpdateBuyList();
            }
            if (Utility.IsSelected(mouseX, mouseY, X + 20 + 105, Y + 15, X + 10 + 105 + 97, Y + 15 + 32))
            {
                config.State = DialogBoxState.Sell;
                config.MaxPages = (SellList.Count / 7) + (SellList.Count % 7 > 0 ? 1 : 0);
                config.Page = 1;
            }
            if (Utility.IsSelected(mouseX, mouseY, X + 20 + 210, Y + 15, X + 10 + 210 + 97, Y + 15 + 32))
            {
                config.State = DialogBoxState.Repair;
                config.MaxPages = (RepairList.Count / 7) + (RepairList.Count % 7 > 0 ? 1 : 0);
                config.Page = 1;
            }

            switch (config.State)
            {
                case DialogBoxState.Buy:
                    switch (filterMode)
                    {
                        case FilterMode.None:
                            // buy button
                            if (selectedItemIndex != -1 && buyList.Count > 0 && buyList[selectedItemIndex] != null)
                            {
                                ((MainGame)Cache.DefaultState).BuyItem(MerchantId, buyList[selectedItemIndex].Id, config.Page, ItemDisplayGroup.All);
                                UpdateBuyList();
                                selectedItemIndex = -1;
                            }
                            break;
                        case FilterMode.Stats:
                            foreach (KeyValuePair<ItemStat, bool> statFilter in statFilters)
                            {
                                if (Utility.IsSelected(mouseX, mouseY, X + 85 + (indexX * 120), Y + 90 + (indexY * 32), X + 85 + (indexX * 120) + 117, Y + 90 + (indexY * 32) + 33))
                                {
                                    statFilters[statFilter.Key] = !statFilters[statFilter.Key];
                                    break;
                                }
                                indexY++;
                                if (indexY > 0 && indexY % 9 == 0) { indexX++; indexY = 0; }
                            }
                            break;
                        case FilterMode.General:
                            foreach (KeyValuePair<ItemCategory, bool> categoryFilter in categoryFilters)
                            {
                                if (Utility.IsSelected(mouseX, mouseY, X + 85 + (indexX * 120), Y + 90 + (indexY * 32), X + 85 + (indexX * 120) + 117, Y + 90 + (indexY * 32) + 33))
                                {
                                    categoryFilters[categoryFilter.Key] = !categoryFilters[categoryFilter.Key];
                                    break;
                                }
                                indexY++;
                                if (indexY > 0 && indexY % 9 == 0) { indexX++; indexY = 0; }
                            }
                            break;
                    }

                    /* STAT FILTERS */
                    if (Utility.IsSelected(mouseX, mouseY, X + 85, Y + 87 - 42, X + 85 + 117, Y + 87 - 42 + 33))
                    {
                        filterMode = (filterMode == FilterMode.Stats ? FilterMode.None : FilterMode.Stats);

                        if (filterMode == FilterMode.None) // refresh with new filters
                            UpdateBuyList();
                    }

                    /* CATEGORY FILTERS */
                    if (Utility.IsSelected(mouseX, mouseY, X + 205, Y + 87 - 42, X + 205 + 117, Y + 87 - 42 + 33))
                    {
                        filterMode = (filterMode == FilterMode.General ? FilterMode.None : FilterMode.General);

                        if (filterMode == FilterMode.None) // refresh with new filters
                            UpdateBuyList();
                    }

                    /* TEXT FILTER */
                    if (Utility.IsSelected(mouseX, mouseY, X + 355, Y + 87 - 42, X + 325 + 160, Y + 87 - 42 + 33))
                        Cache.DefaultState.Display.Keyboard.Dispatcher.Subscriber = textFilter;
                    break;
                case DialogBoxState.Sell:
                    // sell button
                    if (selectedItemIndex != -1 && SellList.Count > 0 && selectedItemIndex < SellList.Count)
                    {
                        ((MainGame)Cache.DefaultState).SellItem(MerchantId, SellList[selectedItemIndex], config.Page, ItemDisplayGroup.All);
                        SellList.Remove(SellList[selectedItemIndex]);
                        selectedItemIndex = -1;
                    }
                    // remove button
                    if (selectedItemIndex2 != -1 && SellList.Count > 0 && selectedItemIndex2 < SellList.Count)
                    {
                        SellList.Remove(SellList[selectedItemIndex2]);
                        selectedItemIndex2 = -1;
                    }
                    // sell all button
                    if (SellList.Count > 0)
                        if (Utility.IsSelected(mouseX, mouseY, X + 485, Y + 87 - 42, X + 485 + 65, Y + 87 - 42 + 33))
                        {
                            ((MainGame)Cache.DefaultState).SellItemList(MerchantId, SellList, config.Page, ItemDisplayGroup.All);
                            SellList.Clear();
                        }

                    config.MaxPages = (SellList.Count / 7) + (SellList.Count % 7 > 0 ? 1 : 0);
                    config.Page = Math.Max(config.MaxPages, 1);
                    break;
                case DialogBoxState.Repair:
                    // repair button
                    if (selectedItemIndex != -1 && RepairList.Count > 0 && selectedItemIndex < RepairList.Count)
                        if (player.Gold >= player.Inventory[RepairList[selectedItemIndex]].GetRepairCost())
                        {
                            ((MainGame)Cache.DefaultState).RepairItem(MerchantId, RepairList[selectedItemIndex], config.Page, ItemDisplayGroup.All);
                            RepairList.Remove(RepairList[selectedItemIndex]);
                            selectedItemIndex = -1;
                        }
                    // remove button
                    if (selectedItemIndex2 != -1 && RepairList.Count > 0 && selectedItemIndex2 < RepairList.Count)
                    {
                        RepairList.Remove(RepairList[selectedItemIndex2]);
                        selectedItemIndex2 = -1;
                    }
                    // sell all button
                    if (RepairList.Count > 0)
                        if (Utility.IsSelected(mouseX, mouseY, X + 485, Y + 87 - 42, X + 485 + 65, Y + 87 - 42 + 33))
                        {
                            ((MainGame)Cache.DefaultState).RepairItemList(MerchantId, RepairList, config.Page, ItemDisplayGroup.All);
                            RepairList.Clear();
                        }

                    // add equipment button
                    if (Utility.IsSelected(mouseX, mouseY, X + 85, Y + 87 - 42, X + 85 + 117, Y + 87 - 42 + 33))
                        for (int i = 0; i < Globals.MaximumEquipment; i++)
                            if (player.Inventory[i] != null && !player.Inventory[i].IsStackable)
                                AddRepairItem(i);

                    // add inventory button
                    if (Utility.IsSelected(mouseX, mouseY, X + 205, Y + 87 - 42, X + 205 + 117, Y + 87 - 42 + 33))
                        for (int i = Globals.MaximumEquipment; i < Globals.MaximumTotalItems; i++)
                            if (player.Inventory[i] != null && !player.Inventory[i].IsStackable)
                                AddRepairItem(i);

                    config.MaxPages = (RepairList.Count / 7) + (RepairList.Count % 7 > 0 ? 1 : 0);
                    config.Page = Math.Max(config.MaxPages, 1);
                    break;
            }
        }

        public void LeftDoubleClicked(ref SelectionMode selectionMode, ref int selectionModeId)
        {
        }

        public void LeftHeld()
        {
        }

        public void LeftDragged()
        {
        }

        public void LeftReleased(GameDialogBoxType highlightedDialogBox, int highlightedDialogBoxItemIndex)
        {
        }

        public void RightClicked() { }

        public void RightHeld() { }

        public void RightReleased() { }

        public void Scroll(int direction)
        {
            if (config.Page - direction > config.MaxPages) config.Page = 1;
            else if (config.Page - direction < 1) config.Page = config.MaxPages;
            else config.Page -= direction;

            UpdateBuyList();
        }

        public void OffsetLocation(int x, int y)
        {
            config.X += x;
            config.Y += y;

            // update subscriber location
            textFilter.X = config.X + 380;
            textFilter.Y = config.Y + 50;
        }

        public void Show()
        {
            if (!config.AlwaysVisible && Cache.DefaultState != null)
                ((IGameState)Cache.DefaultState).BringToFront(Type);
            highlightedItemIndex = -1;
            config.Hidden = false;

            UpdateBuyList();
        }

        public void Hide()
        {
            if (!config.AlwaysVisible && Cache.DefaultState != null)
            {
                ((MainGame)Cache.DefaultState).DialogBoxDrawOrder.Remove(Type);
                ((IGameState)Cache.DefaultState).ClickedDialogBox = GameDialogBoxType.None;
            }
            config.Hidden = true;

            // clean up
            SellList.Clear();
            RepairList.Clear();
            isUpdating = false;
        }

        public void Toggle(DialogBoxState state = DialogBoxState.Vertical)
        {
            if (config.Visible && state == config.State) Hide();
            else
            {
                config.State = state;
                Show();
            }
        }

        public void AddSellItem(int itemId)
        {
            if (!SellList.Contains(itemId)) // max 7 items at a time
                SellList.Add(itemId);

            int oldMaxPages = config.MaxPages;
            config.MaxPages = (SellList.Count / 7) + (SellList.Count % 7 > 0 ? 1 : 0);
            // if we go on to new page, show the new page
            if (config.MaxPages > oldMaxPages) config.Page = config.MaxPages;
        }

        public void AddRepairItem(int itemId)
        {
            if (!RepairList.Contains(itemId)) // max 7 items at a time
                RepairList.Add(itemId);

            int oldMaxPages = config.MaxPages;
            config.MaxPages = (RepairList.Count / 7) + (RepairList.Count % 7 > 0 ? 1 : 0);
            // if we go on to new page, show the new page
            if (config.MaxPages > oldMaxPages) config.Page = config.MaxPages;
        }

        public void SetData(byte[] data)
        {
            isUpdating = false;
            buyList.Clear();

            totalBuyCount = BitConverter.ToInt16(data, 6);
            if (config.State == DialogBoxState.Buy)
            {
                config.MaxPages = (totalBuyCount / 7) + (totalBuyCount % 7 > 0 ? 1 : 0);
                config.Page = Math.Max(Math.Min(config.MaxPages, config.Page), 1); // ensures we are not over max pages after refresh
            }

            MerchantId = (int)data[8];
            int currCount = (int)data[9]; // how many on this page?
            int index = 10;
            for (int i = 0; i < currCount; i++)
            {
                int merchantItemId = BitConverter.ToInt32(data, index);
                index += 4;
                bool unlimited = (((int)data[index]) == 1);
                index++;

                int statCount = (int)data[index + 34];
                byte[] itemData = new byte[36 + (statCount * 2)];
                Buffer.BlockCopy(data, index, itemData, 0, itemData.Length);
                int slot = BitConverter.ToInt16(itemData, 0); // inventoryv2 uses slot id
                int id = BitConverter.ToInt32(itemData, 2);
                Item item = Cache.ItemConfiguration[id].Copy();
                item.ParseData(itemData);
                index += itemData.Length;

                MerchantItem merchantItem = new MerchantItem(MerchantId, item, merchantItemId, unlimited);

                buyList.Add(merchantItem);
            }
        }
    }
}