﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Text;

//using Microsoft.Xna.Framework;
//using Microsoft.Xna.Framework.Graphics;
//using Microsoft.Xna.Framework.Input;

//using Helbreath.Common;
//using Helbreath.Common.Assets;
//using Client.Assets.State;

//namespace Client.Assets.UI
//{
//    public class CharacterV1DialogBox : IGameDialogBox
//    {
//        int selectedItemIndex = -1;
//        int clickedItemIndex = -1;
//        int highlightedItemIndex = -1;
//        int selectedItemIndexOffsetX; //Dragging items
//        int selectedItemIndexOffsetY; //Dragging items
              
//        GameDialogBoxConfiguration config;

//        //Base graphic info
//        int sprite = (int)SpriteId.InterfaceText; //Main texture
//        int spriteFrame = 0; //rectangle in texutre
//        int spriteFrameHover = -1; //frame to display when hovering mouse over
//        int width = -1; //frame width
//        int height = -1; //frame height 

//        public int SelectedItemIndex { get { return selectedItemIndex; } set { selectedItemIndex = value; } }
//        public int ClickedItemIndex { get { return clickedItemIndex; } set { clickedItemIndex = value; } }
//        public int HighlightedItemIndex { get { return highlightedItemIndex; } set { highlightedItemIndex = value; } }
//        public int SelectedItemIndexOffsetX { get { return selectedItemIndexOffsetX; } set { selectedItemIndexOffsetX = value; } }
//        public int SelectedItemIndexOffsetY { get { return selectedItemIndexOffsetY; } set { selectedItemIndexOffsetY = value; } }
//        public GameDialogBoxConfiguration Config { get { return config; } set { config = value; } }
//        public GameDialogBoxType Type { get { return GameDialogBoxType.CharacterV1; } }

//        public CharacterV1DialogBox()
//        {    
//            config = new GameDialogBoxConfiguration(Type, Cache.GameSettings.Resolution);   
//            //Assign height and width if has sprite
//            if (sprite != -1 && spriteFrame != -1)
//            {
//                width = Cache.Interface[sprite].Frames[spriteFrame].Width;
//                height = Cache.Interface[sprite].Frames[spriteFrame].Height;
//            }
//        }

//        public CharacterV1DialogBox(GameDialogBoxConfiguration config)
//        {    
//            this.config = config;
//            //Assign height and width if has sprite
//            if (sprite != -1 && spriteFrame != -1)
//            {
//                width = Cache.Interface[sprite].Frames[spriteFrame].Width;
//                height = Cache.Interface[sprite].Frames[spriteFrame].Height;
//            }
//        }

//        public bool DrawItemPopup(SpriteBatch spriteBatch, GameTime gameTime)
//        {       
//            if (config.Hidden)
//                if (!config.AlwaysVisible) return false; // dont draw dialogs if not a HUD item
//                else if (Cache.GameSettings.LockedDialogs) return false;  // show HUD items when HUD is unlocked (transparent)

//            Player player = ((MainGame)Cache.DefaultState).Player;
//            int mouseX = (int)Cache.DefaultState.Display.Mouse.X;
//            int mouseY = (int)Cache.DefaultState.Display.Mouse.Y;
//            int x = config.X;
//            int y = config.Y;
//            float transparency = ((Cache.GameSettings.TransparentDialogs || config.Hidden) ? 0.5F : 1F);  
//            highlightedItemIndex = clickedItemIndex = selectedItemIndex = -1;
       
//            //Draws base frame if not null
//            AnimationFrame frame; // holds frame in case we need to check bounds later
//            if (sprite != -1 && spriteFrame != -1) // some dialog boxes dont have a box sprite such as minimap
//            {
//                if (spriteFrameHover != -1 &&
//                    (mouseX > x) && (mouseX < x + width) && (mouseY > y) && (mouseY <= y + height)) //Hover
//                    frame = Cache.Interface[sprite].Frames[spriteFrameHover];
//                else frame = Cache.Interface[sprite].Frames[spriteFrame]; //Normal 
//                spriteBatch.DrawItemPopup(Cache.Interface[sprite].Texture, new Vector2(x, y), frame.GetRectangle(), Color.White * transparency);  //draw the base texture
//            }
//            else frame = null;

//            //draw extra 
//            string name = player.Name.ToString() + " : " + ((player.CriminalCount > 0) ? string.Format("Criminal ({0}) ", player.CriminalCount) : "") + string.Format("Contribution ({0})", player.Contribution);

//            spriteBatch.DrawString(Cache.DefaultState.Display.Fonts[FontType.Dialog], name, new Vector2((int)(x + (frame.Width / 2) - (Cache.DefaultState.Display.Fonts[FontType.Dialog].MeasureString(name).X / 2)), (int)(y + 50 + 1)), Color.Black * 0.7f);
//            //spriteBatch.DrawString(Cache.GameState.Display.Fonts[FontType.Dialog], name, new Vector2(x + 25, y + 50), Color.DarkGray);

//            if (player.GuildRank >= 0)
//            {
//                string guild = player.GuildName + ((player.GuildRank == 0) ? " Guildmaster" : " Guildsman");
//                spriteBatch.DrawString(Cache.DefaultState.Display.Fonts[FontType.Dialog], guild, new Vector2((int)(x + (frame.Width / 2) - (Cache.DefaultState.Display.Fonts[FontType.Dialog].MeasureString(guild).X / 2)), (int)(y + 67 + 1)), Color.Black * 0.7f);
//            }

//            spriteBatch.DrawString(Cache.DefaultState.Display.Fonts[FontType.Dialog], player.Level.ToString(), new Vector2((int)(x + 185 + 1), (int)(y + 105 + 1)), Color.Black * 0.7f);
//            spriteBatch.DrawString(Cache.DefaultState.Display.Fonts[FontType.Dialog], player.Experience.ToString(), new Vector2((int)(x + 185 + 1), (int)(y + 123 + 1)), Color.Black * 0.7f);
//            spriteBatch.DrawString(Cache.DefaultState.Display.Fonts[FontType.Dialog], Globals.ExperienceTable[player.Level + 1].ToString(), new Vector2((int)(x + 185 + 1), (int)(y + 141 + 1)), Color.Black * 0.7f);
//            spriteBatch.DrawString(Cache.DefaultState.Display.Fonts[FontType.Dialog], player.HP.ToString() + "/" + player.MaxHP.ToString(), new Vector2((int)(x + 185 + 1), (int)(y + 171 + 1)), Color.Black * 0.7f);
//            spriteBatch.DrawString(Cache.DefaultState.Display.Fonts[FontType.Dialog], player.MP.ToString() + "/" + player.MaxMP.ToString(), new Vector2((int)(x + 185 + 1), (int)(y + 189 + 1)), Color.Black * 0.7f);
//            spriteBatch.DrawString(Cache.DefaultState.Display.Fonts[FontType.Dialog], player.SP.ToString() + "/" + player.MaxSP.ToString(), new Vector2((int)(x + 185 + 1), (int)(y + 207 + 1)), Color.Black * 0.7f);
//            spriteBatch.DrawString(Cache.DefaultState.Display.Fonts[FontType.Dialog], player.EnemyKills.ToString(), new Vector2((int)(x + 185 + 1), (int)(y + 256 + 1)), Color.Black * 0.7f);
//            spriteBatch.DrawString(Cache.DefaultState.Display.Fonts[FontType.Dialog], player.Strength.ToString(), new Vector2((int)(x + 55 + 1), (int)(y + 282 + 1)), Color.Black * 0.7f);
//            spriteBatch.DrawString(Cache.DefaultState.Display.Fonts[FontType.Dialog], player.Dexterity.ToString(), new Vector2((int)(x + 55 + 1), (int)(y + 300 + 1)), Color.Black * 0.7f);
//            spriteBatch.DrawString(Cache.DefaultState.Display.Fonts[FontType.Dialog], player.Vitality.ToString(), new Vector2((int)(x + 222 + 1), (int)(y + 282 + 1)), Color.Black * 0.7f);
//            spriteBatch.DrawString(Cache.DefaultState.Display.Fonts[FontType.Dialog], player.Magic.ToString(), new Vector2((int)(x + 140 + 1), (int)(y + 300 + 1)), Color.Black * 0.7f);
//            spriteBatch.DrawString(Cache.DefaultState.Display.Fonts[FontType.Dialog], player.Intelligence.ToString(), new Vector2((int)(x + 140 + 1), (int)(y + 282 + 1)), Color.Black * 0.7f);
//            spriteBatch.DrawString(Cache.DefaultState.Display.Fonts[FontType.Dialog], player.Agility.ToString(), new Vector2((int)(x + 222 + 1), (int)(y + 300 + 1)), Color.Black * 0.7f);

//            // todo - try and cache instead of this loop, doing this in the game loop is unecessary
//            Dictionary<EquipType, int> equipment = new Dictionary<EquipType, int>(); //Figure out what this is?
//            for (int i = 0; i < Globals.MaximumTotalItems; i++)
//                if (player.Inventory[i] != null && player.Inventory[i].IsEquipped)
//                    equipment.Add(player.Inventory[i].EquipType, i);

//            // draw body, hair and panties
//            switch (player.Gender)
//            {
//                case GenderType.Male:
//                    {
//                        int bodyIndex = (int)SpriteId.EquipmentPack + 0;
//                        int bodyFrame = player.Type - 1;
//                        SpriteHelper.DrawEquipment(spriteBatch, bodyIndex, bodyFrame, x + 171, y + 290, 0, 0, 0, 0, (int)GameColour.Normal, 1.0f);

//                        if (!equipment.ContainsKey(EquipType.Head)) // no helm, so show hair
//                        {
//                            int hairIndex = (int)SpriteId.EquipmentPack + 18;
//                            int hairFrame = (player.Appearance1 & 0x0F00) >> 8;
//                            SpriteHelper.DrawEquipment(spriteBatch, hairIndex, hairFrame, x + 171, y + 290, 0, 0, 0, 0, (int)GameColour.Normal, 1.0f);
//                        }

//                        int undiesIndex = (int)SpriteId.EquipmentPack + 19;
//                        int undiesFrame = player.Appearance1 & 0x000F;
//                        SpriteHelper.DrawEquipment(spriteBatch, undiesIndex, undiesFrame, x + 171, y + 290, 0, 0, 0, 0, (int)GameColour.Normal, 1.0f);
//                    }
//                    break;
//                case GenderType.Female:
//                    {
//                        int bodyIndex = (int)SpriteId.EquipmentPack + 40;
//                        int bodyFrame = player.Type - 4;
//                        SpriteHelper.DrawEquipment(spriteBatch, bodyIndex, bodyFrame, x + 171, y + 290, 0, 0, 0, 0, (int)GameColour.Normal, 1.0f);

//                        if (!equipment.ContainsKey(EquipType.Head)) // no helm, so show hair
//                        {
//                            int hairIndex = (int)SpriteId.EquipmentPack + 58;
//                            int hairFrame = (player.Appearance1 & 0x0F00) >> 8;
//                            SpriteHelper.DrawEquipment(spriteBatch, hairIndex, hairFrame, x + 171, y + 290, 0, 0, 0, 0, (int)GameColour.Normal, 1.0f);
//                        }

//                        int undiesIndex = (int)SpriteId.EquipmentPack + 59;
//                        int undiesFrame = player.Appearance1 & 0x000F;
//                        SpriteHelper.DrawEquipment(spriteBatch, undiesIndex, undiesFrame, x + 171, y + 290, 0, 0, 0, 0, (int)GameColour.Normal, 1.0f);
//                    }
//                    break;
//            }

//            // changes the draw order to draw last if something is being dragged
//            int[] drawOrder = new int[Globals.EquipmentDrawOrder.Length];
//            if (clickedItemIndex != -1)
//            {
//                LinkedList<int> list = new LinkedList<int>(Globals.EquipmentDrawOrder);
//                list.Remove((int)player.Inventory[clickedItemIndex].EquipType);
//                list.AddLast((int)player.Inventory[clickedItemIndex].EquipType);
//                drawOrder = list.ToArray<int>();
//            }
//            else Array.Copy(Globals.EquipmentDrawOrder, drawOrder, Globals.EquipmentDrawOrder.Length);

//            // uses some global arrays to draw equipment to the dialog boxes
//            bool selected = false;
//            for (int i = 0; i < drawOrder.Length; i++)
//            {
//                EquipType equipType = (EquipType)drawOrder[i];
//                if (equipment.ContainsKey(equipType))
//                {
//                    int itemIndex = (int)SpriteId.EquipmentPack + (player.Inventory[equipment[equipType]].Sprite) + (player.Gender == GenderType.Female ? 40 : 0);
//                    int itemFrame = player.Inventory[equipment[equipType]].SpriteFrame;

//                    SpriteHelper.DrawEquipment(spriteBatch, itemIndex, itemFrame, x, y,
//                                                Globals.EquipmentDrawOffsetX[(int)player.Gender, (int)equipType],
//                                                Globals.EquipmentDrawOffsetY[(int)player.Gender, (int)equipType],
//                                                ((clickedItemIndex == equipment[equipType]) ? selectedItemIndexOffsetX : 0),
//                                                ((clickedItemIndex == equipment[equipType]) ? selectedItemIndexOffsetY : 0),
//                                                player.Inventory[equipment[equipType]].Colour, 1.0f,
//                                                out selected);
//                    if (selected) selectedItemIndex = equipment[equipType];
//                }
//            }

//            //Level set, Party, Quest buttons
//            spriteBatch.DrawItemPopup(Cache.Interface[(int)SpriteId.InterfaceButton].Texture, new Vector2(x + 15, y + 336), Cache.Interface[(int)SpriteId.InterfaceButton].Frames[4].GetRectangle(), Color.White);
//            spriteBatch.DrawItemPopup(Cache.Interface[(int)SpriteId.InterfaceButton].Texture, new Vector2(x + 97, y + 336), Cache.Interface[(int)SpriteId.InterfaceButton].Frames[44].GetRectangle(), Color.White);
//            spriteBatch.DrawItemPopup(Cache.Interface[(int)SpriteId.InterfaceButton].Texture, new Vector2(x + 179, y + 336), Cache.Interface[(int)SpriteId.InterfaceButton].Frames[10].GetRectangle(), Color.White);

//            //Level set, Party, Quest Highlight when mouse over
//            if ((mouseY > y + 336) && (mouseY < y + 356))
//            {
//                if ((mouseX > x + 15) && (mouseX < x + 90))
//                    spriteBatch.DrawItemPopup(Cache.Interface[(int)SpriteId.InterfaceButton].Texture, new Vector2(x + 15, y + 336), Cache.Interface[(int)SpriteId.InterfaceButton].Frames[5].GetRectangle(), Color.White * Cache.TransparencyFaders.GlowFrame);
//                if ((mouseX > x + 97) && (mouseX < x + 172))
//                    spriteBatch.DrawItemPopup(Cache.Interface[(int)SpriteId.InterfaceButton].Texture, new Vector2(x + 97, y + 336), Cache.Interface[(int)SpriteId.InterfaceButton].Frames[45].GetRectangle(), Color.White * Cache.TransparencyFaders.GlowFrame);
//                if ((mouseX > x + 179) && (mouseX < x + 250))
//                    spriteBatch.DrawItemPopup(Cache.Interface[(int)SpriteId.InterfaceButton].Texture, new Vector2(x + 179, y + 336), Cache.Interface[(int)SpriteId.InterfaceButton].Frames[11].GetRectangle(), Color.White * Cache.TransparencyFaders.GlowFrame);
//            }
                  
//            //figure out what this is used for
//            if (frame != null && Utility.IsSelected(mouseX, mouseY, x, y, x + frame.Width, y + frame.Height))
//                return true;
//            else return false;                                                         
//        }

//        public void Update(GameTime gameTime)
//        {

//        }

//        public void LeftClick()
//        {
//            int mouseX = (int)Cache.DefaultState.Display.Mouse.X;
//            int mouseY = (int)Cache.DefaultState.Display.Mouse.Y;
//            Player player = ((MainGame)Cache.DefaultState).Player;
//            int x = config.X;
//            int y = config.Y;
//             if ((mouseY > y + 336) && (mouseY < y + 356))
//             {
                    
//                 if ((mouseX > x + 15) && (mouseX < x + 90)) //Quest                              
//                     if (((MainGame)Cache.DefaultState).DialogBoxes[GameDialogBoxType.Quest].Config.Visible)
//                         ((MainGame)Cache.DefaultState).DialogBoxes[GameDialogBoxType.Quest].Hide();
//                     else
//                     {
//                         ((MainGame)Cache.DefaultState).DialogBoxes[GameDialogBoxType.Quest].Show();
//                         ((MainGame)Cache.DefaultState).BringToFront(GameDialogBoxType.Quest);
//                     }
//                 if ((mouseX > x + 97) && (mouseX < x + 172)) //Party                                                       
//                     if (((MainGame)Cache.DefaultState).DialogBoxes[GameDialogBoxType.Party].Config.Visible)
//                         ((IGameState)Cache.DefaultState).DialogBoxes[GameDialogBoxType.Party].Hide();
//                     else
//                     {
//                         ((IGameState)Cache.DefaultState).DialogBoxes[GameDialogBoxType.Party].Show();
//                         ((IGameState)Cache.DefaultState).BringToFront(GameDialogBoxType.Party);
//                     }
//                 if ((mouseX > x + 179) && (mouseX < x + 250)) //Level Up
//                     if (((MainGame)Cache.DefaultState).DialogBoxes[GameDialogBoxType.LevelUpV1].Config.Visible)
//                         ((MainGame)Cache.DefaultState).DialogBoxes[GameDialogBoxType.LevelUpV1].Hide();
//                     else
//                     {
//                         player.RemainderLevelUpPoints = player.LevelUpPoints;
//                         player.LevelUpStrength = player.LevelUpDexterity = player.LevelUpMagic =
//                             player.LevelUpIntelligence = player.LevelUpVitality = player.LevelUpAgility = 0;
//                         ((MainGame)Cache.DefaultState).DialogBoxes[GameDialogBoxType.LevelUpV1].Show();
//                         ((IGameState)Cache.DefaultState).BringToFront(GameDialogBoxType.LevelUpV1);
//                     }
//             }
//        }

//        public bool LeftHeld()
//        {
//            return false;
//        }

//        public void Scroll(int direction)
//        {
//            if (config.Page - direction > config.MaxPages) config.Page = 1;
//            else if (config.Page - direction < 1) config.Page = config.MaxPages;
//            else config.Page -= direction;
//        }

//        public void OffsetLocation(int x, int y)
//        {      
//            config.X += x;
//            config.Y += y;
//        }

//        public void Show()
//        {
                       
//            if (!config.AlwaysVisible && Cache.DefaultState != null)
//                ((IGameState)Cache.DefaultState).BringToFront(Type);
//            highlightedItemIndex = -1;
//            config.Hidden = false;
//        }

//        public void Hide()
//        {                   
//            if (!config.AlwaysVisible && Cache.DefaultState != null)
//            {
//                ((IGameState)Cache.DefaultState).DialogBoxDrawOrder.Remove(Type);
//                ((IGameState)Cache.DefaultState).ClickedDialogBox = GameDialogBoxType.None;
//            }
//            config.Hidden = true;
//        }

//        public void Toggle(DialogBoxState state = DialogBoxState.Normal)
//        {
                      
//            if (config.Visible && state == config.State) Hide();
//            else
//            {
//                config.State = state;
//                Show();
//            }
//        }

//        public void SetData(byte[] data)
//        {

//        }
//    }
//}

  