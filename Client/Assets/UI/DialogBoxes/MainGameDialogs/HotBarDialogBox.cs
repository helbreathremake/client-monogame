﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

using Helbreath.Common;
using Client.Assets.State;
using Client.Assets.Game;

namespace Client.Assets.UI
{
    public class HotBarDialogBox : IGameDialogBox
    {

        private GameDialogBoxConfiguration config;
         
        // selected items
        private int clickedItemIndex; // clicked item
        private int selectedItemIndex; // mouse over item
        private int selectedItemIndexOffsetX;
        private int selectedItemIndexOffsetY;
        private int highlightedItemIndex; // chosen item (non volatile)

        public int ClickedItemIndex { get { return clickedItemIndex; } set { clickedItemIndex = value; } }
        public int SelectedItemIndex { get { return selectedItemIndex; } set { selectedItemIndex = value; } }
        public int HighlightedItemIndex { get { return highlightedItemIndex; } set { highlightedItemIndex = value; } }
        public int SelectedItemIndexOffsetX { get { return selectedItemIndexOffsetX; } set { selectedItemIndexOffsetX = value; } }
        public int SelectedItemIndexOffsetY { get { return selectedItemIndexOffsetY; } set { selectedItemIndexOffsetY = value; } }

        public GameDialogBoxType Type { get { return GameDialogBoxType.HotBar; } }
        public GameDialogBoxConfiguration Config { get { return config; } set { config = value; } }

        int mouseX; 
        int mouseY;
        int X;
        int Y; 
        Player player;
        int sprite = (int)SpriteId.DialogsV2 + 1;
        float transparency;
        AnimationFrame frame;
        bool hover;

        public HotBarDialogBox()
        {
            config = new GameDialogBoxConfiguration(Type, Cache.GameSettings.Resolution);
            highlightedItemIndex = selectedItemIndex = clickedItemIndex = -1;
            mouseX = (int)Cache.DefaultState.Display.Mouse.X;
            mouseY = (int)Cache.DefaultState.Display.Mouse.Y;
            X = config.X;
            Y = config.Y;
            player = ((MainGame)Cache.DefaultState).Player;
            transparency = ((Cache.GameSettings.TransparentDialogs || config.Hidden) ? 0.5F : 1F);
        }

        public HotBarDialogBox(GameDialogBoxConfiguration config)
        {
            this.config = config;
            highlightedItemIndex = selectedItemIndex = clickedItemIndex = -1;
            mouseX = (int)Cache.DefaultState.Display.Mouse.X;
            mouseY = (int)Cache.DefaultState.Display.Mouse.Y;
            X = config.X;
            Y = config.Y;
            player = ((MainGame)Cache.DefaultState).Player;
            transparency = ((Cache.GameSettings.TransparentDialogs || config.Hidden) ? 0.5F : 1F);
        }

        public bool Draw(SpriteBatch spriteBatch, GameTime gameTime)
        {
            if (config.Hidden)
                if (!config.AlwaysVisible) return false; // dont draw dialogs if not a HUD item
                else if (Cache.GameSettings.LockedDialogs) return false;  // show HUD items when HUD is unlocked (transparent)

            if (((MainGame)Cache.DefaultState).Player.IsCasting && Cache.GameSettings.HideDialogDuringCasting) return false; 

            frame = null;
            highlightedItemIndex = -1;

            switch (config.State)
            {
                case DialogBoxState.Vertical:
                    frame = Cache.Interface[sprite].Frames[20];
                    spriteBatch.Draw(Cache.Interface[sprite].Texture, new Vector2(X + 7, Y + 4), frame.GetRectangle(), Color.DarkGray * transparency);

                    for (int i = 0; i < 10; i++)
                    {
                        // hotkeys go 1,2,3,4,5,6,7,8,9,0 due to keyboard layout
                        int hotkeyNumber = (i + 1 > 9 ? 0 : i + 1);
                        if (Cache.HotBarConfiguration.ContainsKey(i))
                        {
                            Color spellTextColour = (player.IsCasting || !player.CanCast(Cache.HotBarConfiguration[i].Index) || !player.HandsFree ? Color.DimGray : Cache.Colors[GameColor.Orange]) * transparency; // grey out during cast or not enough mana
                            Color spellColour = (player.IsCasting || !player.CanCast(Cache.HotBarConfiguration[i].Index) || !player.HandsFree ? Color.Gray : Color.White) * transparency; // grey out during cast or if not enough mana
                            Color skillColour; // todo

                            switch (Cache.HotBarConfiguration[i].Type)
                            {
                                case HotBarType.Spell:
                                    hover = Utility.IsSelected(mouseX, mouseY, X + 10, Y + 13 + (i * 30), X + 10 + 30, Y + 13 + (i * 30) + 30);
                                    spriteBatch.Draw(Cache.Interface[sprite + 3].Texture, new Vector2(X + 10, Y + 13 + (i * 30)), Cache.Interface[sprite + 3].Frames[(hover ? Cache.MagicConfiguration[Cache.HotBarConfiguration[i].Index].SmallIconSpriteFrame + 1 : Cache.MagicConfiguration[Cache.HotBarConfiguration[i].Index].SmallIconSpriteFrame)].GetRectangle(), spellColour);
                                    SpriteHelper.DrawTextRightWithShadow(spriteBatch, hotkeyNumber.ToString(), FontType.DialogsSmallerSize7, new Vector2(X + 10, Y + 13 + (i * 30) + 15), 25, spellTextColour);
                                    if (hover) highlightedItemIndex = i;
                                    break;
                                case HotBarType.Skill:
                                    hover = Utility.IsSelected(mouseX, mouseY, X + 10, Y + 13 + (i * 30), X + 10 + 30, Y + 13 + (i * 30) + 30);
                                    // todo images
                                    SpriteHelper.DrawTextRightWithShadow(spriteBatch, hotkeyNumber.ToString(), FontType.DialogsSmallerSize7, new Vector2(X + 10, Y + 13 + (i * 30) + 15), 25, Cache.Colors[GameColor.Orange]);
                                    if (hover) highlightedItemIndex = i;
                                    break;
                            }
                        }
                        else
                        {
                            hover = Utility.IsSelected(mouseX, mouseY, X + 10, Y + 13 + (i * 30), X + 10 + 30, Y + 13 + (i * 30) + 30);
                            spriteBatch.Draw(Cache.Interface[sprite + 3].Texture, new Vector2(X + 10, Y + 13 + (i * 30)), Cache.Interface[sprite + 3].Frames[(hover ? 7 : 6)].GetRectangle(), Color.Gray * transparency);
                            SpriteHelper.DrawTextRightWithShadow(spriteBatch, hotkeyNumber.ToString(), FontType.DialogsSmallerSize7, new Vector2(X + 10, Y + 13 + (i * 30) + 15), 25, Color.DimGray);
                            if (hover) highlightedItemIndex = i;
                        }
                    }

                    // rotate button
                    if (!Cache.GameSettings.LockedDialogs)
                        spriteBatch.Draw(Cache.Interface[sprite].Texture, new Vector2(X + 13, Y - 25),Cache.Interface[sprite].Frames[22].GetRectangle(), Color.White * transparency);

                    break;
                case DialogBoxState.Horizontal:
                    frame = Cache.Interface[sprite].Frames[21];
                    spriteBatch.Draw(Cache.Interface[sprite].Texture, new Vector2(X + 4, Y + 7), frame.GetRectangle(), Color.DarkGray * transparency);

                    for (int i = 0; i < 10; i++)
                    {
                        // hotkeys go 1,2,3,4,5,6,7,8,9,0 due to keyboard layout
                        int hotkeyNumber = (i + 1 > 9 ? 0 : i + 1);
                        if (Cache.HotBarConfiguration.ContainsKey(i))
                        {
                            Color spellTextColour = (player.IsCasting || !player.CanCast(Cache.HotBarConfiguration[i].Index) || !player.HandsFree ? Color.DimGray : Cache.Colors[GameColor.Orange]) * transparency; // grey out during cast or not enough mana
                            Color spellColour = (player.IsCasting || !player.CanCast(Cache.HotBarConfiguration[i].Index) || !player.HandsFree ? Color.Gray : Color.White) * transparency; // grey out during cast or if not enough mana
                            Color skillColour; // todo

                            switch (Cache.HotBarConfiguration[i].Type)
                            {
                                case HotBarType.Spell:
                                    hover = Utility.IsSelected(mouseX, mouseY, X + 13 + (i * 30), Y + 10, X + 13 + (i * 30) + 30, Y + 10 + 30);
                                    spriteBatch.Draw(Cache.Interface[sprite + 3].Texture, new Vector2(X + 13 + (i * 30), Y + 10), Cache.Interface[sprite + 3].Frames[((hover && Cache.GameSettings.LockedDialogs) ? Cache.MagicConfiguration[Cache.HotBarConfiguration[i].Index].SmallIconSpriteFrame + 1 : Cache.MagicConfiguration[Cache.HotBarConfiguration[i].Index].SmallIconSpriteFrame)].GetRectangle(), spellColour);
                                    SpriteHelper.DrawTextRightWithShadow(spriteBatch, hotkeyNumber.ToString(), FontType.DialogsSmallerSize7, new Vector2(X + 13 + (i * 30), Y + 10 + 15), 25, spellTextColour);
                                    if (hover) highlightedItemIndex = i;
                                    break;
                                case HotBarType.Skill:
                                    hover = Utility.IsSelected(mouseX, mouseY, X + 13 + (i * 30), Y + 10, X + 13 + (i * 30) + 30, Y + 10 + 30);
                                    // todo images
                                    SpriteHelper.DrawTextRightWithShadow(spriteBatch, hotkeyNumber.ToString(), FontType.DialogsSmallerSize7, new Vector2(X + 13 + (i * 30), Y + 10 + 15), 25, Cache.Colors[GameColor.Orange]);
                                    if (hover) highlightedItemIndex = i;
                                    break;
                            }
                        }
                        else
                        {
                            hover = Utility.IsSelected(mouseX, mouseY, X + 13 + (i * 30), Y + 10, X + 13 + (i * 30) + 30, Y + 10 + 30);
                            spriteBatch.Draw(Cache.Interface[sprite + 3].Texture, new Vector2(X + 13 + (i * 30), Y + 10), Cache.Interface[sprite + 3].Frames[((hover && Cache.GameSettings.LockedDialogs) ? 7 : 6)].GetRectangle(), Color.Gray * transparency);
                            SpriteHelper.DrawTextRightWithShadow(spriteBatch, hotkeyNumber.ToString(), FontType.DialogsSmallerSize7, new Vector2(X + 13 + (i * 30), Y + 10 + 15), 25, Color.DimGray);
                            if (hover) highlightedItemIndex = i;
                        }
                    }

                    // rotate button
                    if (!Cache.GameSettings.LockedDialogs)
                        spriteBatch.Draw(Cache.Interface[sprite].Texture, new Vector2(X - 25, Y + 13), Cache.Interface[sprite].Frames[22].GetRectangle(), Color.White * transparency);
                    break;
            }

            if (Cache.GameSettings.LockedDialogs) // not locked dialogs
                if (clickedItemIndex == -1) // if no spell dragging
                    if (highlightedItemIndex != -1) // and no spell hovered
                        selectedItemIndex = highlightedItemIndex; // set hover index
                    else selectedItemIndex = -1; // cancel hovered spell when not dragging

            // handle frame depending on orientation to allow for the rotate button to be x < 0 or y < 0
            switch (config.State)
            {
                case DialogBoxState.Vertical:
                    if (Cache.GameSettings.LockedDialogs)
                    {
                        if (frame != null && Utility.IsSelected(mouseX, mouseY, X + 6, Y + 5, X + 6 + frame.Width, Y + frame.Height + 5))
                            return true;
                        else return false;
                    }
                    else 
                    {
                        if (frame != null && Utility.IsSelected(mouseX, mouseY, X + 6, Y - 23, X + 6 + frame.Width, Y + frame.Height + 5))
                            return true;
                        else return false;
                    }
                case DialogBoxState.Horizontal:
                    if (Cache.GameSettings.LockedDialogs)
                    {
                        if (frame != null && Utility.IsSelected(mouseX, mouseY, X + 6, Y + 6, X + frame.Width + 5, Y + frame.Height + 5))
                            return true;
                        else return false;
                    }
                    else
                    {
                        if (frame != null && Utility.IsSelected(mouseX, mouseY, X - 25, Y + 6, X + frame.Width + 5, Y + frame.Height + 5))
                            return true;
                        else return false;
                    }
                default:
                    if (frame != null && Utility.IsSelected(mouseX, mouseY, X, Y, X + frame.Width, Y + frame.Height))
                        return true;
                    else return false;
            }
        }

        public void Update(GameTime gameTime)
        {
            mouseX = (int)Cache.DefaultState.Display.Mouse.X;
            mouseY = (int)Cache.DefaultState.Display.Mouse.Y;
            X = config.X;
            Y = config.Y;
            player = ((MainGame)Cache.DefaultState).Player;
            transparency = ((Cache.GameSettings.TransparentDialogs || config.Hidden) ? 0.5F : 1F);
        }

        public void LeftClicked()
        {
            if (!Cache.GameSettings.LockedDialogs)
            {
                switch (config.State)
                {
                    case DialogBoxState.Vertical:
                        if (Utility.IsSelected(mouseX, mouseY, X + 13, Y - 25, X + 13 + 20, Y - 25 + 20))
                        {
                            config.State = DialogBoxState.Horizontal;
                            config.X += 38;
                            config.Y -= 38;
                        }
                        break;
                    case DialogBoxState.Horizontal:
                        if (Utility.IsSelected(mouseX, mouseY, X - 25, Y + 13, X - 25 + 20, Y + 13 + 20))
                        {
                            config.State = DialogBoxState.Vertical;
                            config.X -= 38;
                            config.Y += 38;

                        }
                        break;
                }
            }
            else if (clickedItemIndex != -1)
            {
                //Cast spell if there is one
                if (Cache.HotBarConfiguration.ContainsKey(clickedItemIndex) && Cache.HotBarConfiguration[clickedItemIndex] != null)
                {
                    if (player.HandsFree)
                    {
                        int spell = Cache.HotBarConfiguration[clickedItemIndex].Index;
                        if (player.CanCast(spell))
                        {
                            if (!player.MoveReady) player.SpellRequest = spell;
                            else if (!player.IsCasting)
                            {
                                player.Idle(player.Direction, true);
                                ((MainGame)Cache.DefaultState).Idle(player.Direction);

                                player.Cast(spell);
                                ((MainGame)Cache.DefaultState).Cast(spell);
                                clickedItemIndex = -1;
                            }
                            else Cache.DefaultState.AddEvent("Already casting a spell");
                        }
                        else Cache.DefaultState.AddEvent("Not enough mana or intelligence to cast.");
                    }
                    else Cache.DefaultState.AddEvent("Your hands must be free to cast magic.");
                }
                else if (!player.IsCasting)//Open spell book
                {
                    ((MainGame)Cache.DefaultState).DialogBoxes[GameDialogBoxType.HotBarMagic].Toggle(Config.State);
                    ((MainGame)Cache.DefaultState).ClickedDialogBox = GameDialogBoxType.HotBar;

                    HotBarMagicDialogBox box = (HotBarMagicDialogBox)((MainGame)Cache.DefaultState).DialogBoxes[GameDialogBoxType.HotBarMagic];
                    box.HotBarConfigIndex = clickedItemIndex;
                    clickedItemIndex = -1;
                }
            }
        }

        public void LeftHeld() { }

        public void LeftDoubleClicked(ref SelectionMode selectionMode, ref int selectionModeId)  { }

        public void LeftDragged() { }

        public void LeftReleased(GameDialogBoxType highlightedDialogBox, int highlightedDialogBoxItemIndex) {  }

        public void RightClicked() 
        {
            // if hotbaricon at destination, remove
            if (selectedItemIndex != -1 && Cache.HotBarConfiguration.ContainsKey(selectedItemIndex))
                Cache.HotBarConfiguration.Remove(selectedItemIndex);
        }

        public void RightHeld() { }

        public void RightReleased() { }

        public void Scroll(int direction) { }

        public void OffsetLocation(int x, int y)
        {
            config.X += x;
            config.Y += y;
        }

        public void SetLocation(GameDialogBoxConfiguration location)
        {
            config = location;
        }

        public void Show()
        {
            if (!config.AlwaysVisible && Cache.DefaultState != null)
                ((IGameState)Cache.DefaultState).BringToFront(Type);
            highlightedItemIndex = -1;
            config.Hidden = false;
        }

        public void Hide()
        {
            if (!config.AlwaysVisible && Cache.DefaultState != null)
            {
                ((MainGame)Cache.DefaultState).DialogBoxDrawOrder.Remove(Type);
                ((IGameState)Cache.DefaultState).ClickedDialogBox = GameDialogBoxType.None;
            }
            config.Hidden = true;
        }

        public void Toggle(DialogBoxState state = DialogBoxState.Vertical)
        {
            if (config.Visible && state == config.State) Hide();
            else
            {
                config.State = state;
                Show();
            }
        }

        public void SetData(byte[] data)
        {

        }
    }
}