﻿using Helbreath.Common;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Client.Assets.UI
{
    public interface IGameDialogBox 
    {
        GameDialogBoxType Type { get; }
        //Config Stores all important data for loading/saving of dialogboxes
        GameDialogBoxConfiguration Config { get; set; } //Contains X, Y, hidden, alwaysVisible, state, type

        int SelectedItemIndex { get; set; }
        int ClickedItemIndex { get; set; }
        int HighlightedItemIndex { get; set; }
        int SelectedItemIndexOffsetX { get; set; }
        int SelectedItemIndexOffsetY { get; set; }

        bool Draw(SpriteBatch spriteBatch, GameTime gameTime);
        void Update(GameTime gameTime);
        void LeftClicked();
        void LeftDoubleClicked(ref SelectionMode selectionMode, ref int selectionModeId);
        void LeftHeld();
        void LeftDragged();
        void LeftReleased(GameDialogBoxType highlightedDialogBox, int highlightedDialogBoxItemIndex);
        void RightClicked();
        void RightHeld();
        void RightReleased();
        void Scroll(int direction);
        void OffsetLocation(int x, int y);
        void Show();
        void Hide();
        void Toggle(DialogBoxState state = DialogBoxState.Normal);
        void SetData(byte[] data);
    }
}
