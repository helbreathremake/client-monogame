﻿using System.Collections.Generic;
using Helbreath.Common;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Client.Assets.UI
{
    class AdvancedSettingsMenuItem
    {
        private string title;
        private int normalHeight = 32;
        private int normalWidth = 282;
        private int extendedHeight = 70;
        private int extendedWidth = 282;
        private bool selected;
        private Texture2D texture;
        private MenuItemState state;
        private GameDisplay display;
        private bool selectable;
        private List<MenuItem> menuItems;
        private Vector2 normalPosition; 
        private Vector2 extendedPosition;
        private Vector2 drawPositionNormal;
        private Vector2 drawPositionExtended;
        private int scrollPosition;
        private int scrollBarPosition = 0;
        private AdvancedSettingsType settingsType;
        private bool changedStated = false;

        public AdvancedSettingsMenuItem(GameDisplay display, AdvancedSettingsType settingsType)
        {
            //Default Settings
            this.settingsType = settingsType;
            this.display = display;
            selected = false;
            state = MenuItemState.Normal;
            texture = new Texture2D(display.Device, 1, 1);
            texture.SetData(new[] { Color.White * 0.3f });
            this.menuItems = new List<MenuItem>();
            Viewport viewport = display.Device.Viewport;
            Vector2 viewportSize = new Vector2(viewport.Width, viewport.Height);
            int mouseX = (int)display.Mouse.X;
            int mouseY = (int)display.Mouse.Y; 
            int width = viewport.Width;
            int height = viewport.Height;

            //Specalized Settings
            switch (settingsType)
            {
                default: break;
                case AdvancedSettingsType.TeamColor:
                    {
                        extendedHeight = 170;
                        title = "Team Color Options";
                        drawPositionNormal = new Vector2(0, normalHeight * (int)AdvancedSettingsType.TeamColor);
                        drawPositionExtended = new Vector2((int)drawPositionNormal.X, (int)drawPositionNormal.Y + normalHeight - 1);
                        normalPosition = new Vector2((int)(viewport.Width / 2 - 123 - 19), (int)((viewport.Height / 2) - 34) + (normalHeight * (int)AdvancedSettingsType.TeamColor));
                        extendedPosition = new Vector2((int)normalPosition.X, (int)normalPosition.Y + normalHeight - 1);
                        selectable = true;
                        break;
                    }
                case AdvancedSettingsType.Falalal:
                    {
                        title = "Falala Options";
                        drawPositionNormal = new Vector2(0, normalHeight * (int)AdvancedSettingsType.Falalal);
                        drawPositionExtended = new Vector2((int)drawPositionNormal.X, (int)drawPositionNormal.Y + normalHeight - 1);
                        normalPosition = new Vector2((int)(viewport.Width / 2 - 123 - 19), (int)((viewport.Height / 2) - 34) + (normalHeight * (int)AdvancedSettingsType.Falalal));
                        extendedPosition = new Vector2((int)normalPosition.X, (int)normalPosition.Y + normalHeight - 1);
                        selectable = true;
                        break;
                    }
                case AdvancedSettingsType.Lalala:
                    {
                        title = "Lalla Options";
                        drawPositionNormal = new Vector2(0, normalHeight * (int)AdvancedSettingsType.Lalala);
                        drawPositionExtended = new Vector2((int)drawPositionNormal.X, (int)drawPositionNormal.Y + normalHeight - 1);
                        normalPosition = new Vector2((int)(viewport.Width / 2 - 123 - 19), (int)((viewport.Height / 2) - 34) + (normalHeight * (int)AdvancedSettingsType.Lalala));
                        extendedPosition = new Vector2((int)normalPosition.X, (int)normalPosition.Y + normalHeight - 1);
                        selectable = true;
                        break;
                    }
            }
        }

        public bool Update()
        {
            if (selectable)
            {
                if (normalPosition.Y + scrollPosition + scrollBarPosition > (int)((display.Device.Viewport.Height / 2) - 37) && (normalPosition.Y + scrollPosition + scrollBarPosition < (int)((display.Device.Viewport.Height / 2) + 120)))
                {
                    if ((((display.Mouse.Y > normalPosition.Y + scrollPosition + scrollBarPosition) && (display.Mouse.Y < (normalPosition.Y + scrollPosition + normalHeight + scrollBarPosition))) && ((display.Mouse.X > normalPosition.X) && (display.Mouse.X < (normalPosition.X + normalWidth)))) || ((state == MenuItemState.Expanded) && (((display.Mouse.Y > extendedPosition.Y + scrollPosition + scrollBarPosition) && (display.Mouse.Y < (extendedPosition.Y + scrollPosition + extendedHeight + scrollBarPosition))) && ((display.Mouse.X > extendedPosition.X) && (display.Mouse.X < (extendedPosition.X + extendedWidth))))))
                    {
                        selected = true;
                    }
                    else selected = false;
                    //if (!selected) { state = MenuItemState.Normal; }
                }
                else
                {
                    selected = false;
                    //state = MenuItemState.Normal;
                }
            }
            else
            {
                selected = false;
                state = MenuItemState.Normal;
            }
            return changedStated;
        }

        public void Draw(SpriteBatch spriteBatch, GameTime gameTime, float glowFrame)
        {
            int x = (int)drawPositionNormal.X;
            int y = (int)drawPositionNormal.Y + scrollPosition + scrollBarPosition;

            //DrawItemPopup Default Look
            spriteBatch.Draw(texture, new Rectangle(x, y, normalWidth, normalHeight), Color.Black);
            spriteBatch.Draw(texture, new Rectangle(x, y, normalWidth, normalHeight), Color.Black);
            spriteBatch.DrawString(Cache.Fonts[FontType.MagicMedieval14], title, new Vector2(x + (normalWidth / 2) - (int)(Cache.Fonts[FontType.MagicMedieval14].MeasureString(title).X / 2) + 1, (y + (normalHeight / 2) - (int)(Cache.Fonts[FontType.MagicMedieval14].MeasureString(title).Y / 2)) + 1), Color.Black);
            spriteBatch.DrawString(Cache.Fonts[FontType.MagicMedieval14], title, new Vector2(x + (normalWidth / 2) - (int)(Cache.Fonts[FontType.MagicMedieval14].MeasureString(title).X / 2), (y + (normalHeight / 2) - (int)(Cache.Fonts[FontType.MagicMedieval14].MeasureString(title).Y / 2))), Color.DarkOrange);

            //DrawItemPopup Highlight of button
            if (selected)
            {
                if ((display.Mouse.Y > normalPosition.Y + scrollBarPosition + scrollPosition) && (display.Mouse.Y < normalPosition.Y + scrollPosition + scrollBarPosition + normalHeight))
                {
                    if ((display.Mouse.X > normalPosition.X) && (display.Mouse.X < normalPosition.X + normalWidth))
                    {
                        display.Mouse.State = GameMouseState.Pickup;
                        spriteBatch.Draw(texture, new Rectangle(x, y, normalWidth, normalHeight), Color.DarkRed * glowFrame);
                        spriteBatch.DrawString(Cache.Fonts[FontType.MagicMedieval14], title, new Vector2(x + (normalWidth / 2) - (int)(Cache.Fonts[FontType.MagicMedieval14].MeasureString(title).X / 2) + 1, y + (normalHeight / 2) - (int)(Cache.Fonts[FontType.MagicMedieval14].MeasureString(title).Y / 2) + 1), Color.Black * glowFrame);
                        spriteBatch.DrawString(Cache.Fonts[FontType.MagicMedieval14], title, new Vector2(x + (normalWidth / 2) - (int)(Cache.Fonts[FontType.MagicMedieval14].MeasureString(title).X / 2), y + (normalHeight / 2) - (int)(Cache.Fonts[FontType.MagicMedieval14].MeasureString(title).Y / 2)), Color.DarkRed * glowFrame);
                    }
                }
            }

            //DrawItemPopup Expanded
            if (state == MenuItemState.Expanded)
            {
                x = (int)drawPositionExtended.X;
                y = (int)drawPositionExtended.Y + scrollBarPosition + scrollPosition;
                //Background
                spriteBatch.Draw(texture, new Rectangle(x, y, extendedWidth, extendedHeight), Color.Black);

                //DrawItemPopup Specalized Look
                switch (settingsType)
                {
                    case AdvancedSettingsType.TeamColor:
                        {
                            spriteBatch.DrawString(Cache.Fonts[FontType.MagicMedieval14], "Team Color Mode: ", new Vector2(x + 5 + 1, y + 15 + 1), Color.Black);
                            spriteBatch.DrawString(Cache.Fonts[FontType.MagicMedieval14], "Team Color Mode: ", new Vector2(x + 5, y + 15), Color.White);
 
                            spriteBatch.DrawString(Cache.Fonts[FontType.MagicMedieval14], "Party Color: ", new Vector2(x + 5 + 1, y + 45 + 1), Color.Black);
                            spriteBatch.DrawString(Cache.Fonts[FontType.MagicMedieval14], "Party Color: ", new Vector2(x + 5, y + 45), Color.White);

                            spriteBatch.DrawString(Cache.Fonts[FontType.MagicMedieval14], "Guild Color: ", new Vector2(x + 5 + 1, y + 75 + 1), Color.Black);
                            spriteBatch.DrawString(Cache.Fonts[FontType.MagicMedieval14], "Guild Color: ", new Vector2(x + 5, y + 75), Color.White);

                            spriteBatch.DrawString(Cache.Fonts[FontType.MagicMedieval14], "Aresden Color: ", new Vector2(x + 5 + 1, y + 105 + 1), Color.Black);
                            spriteBatch.DrawString(Cache.Fonts[FontType.MagicMedieval14], "Aresden Color: ", new Vector2(x + 5, y + 105), Color.White);

                            spriteBatch.DrawString(Cache.Fonts[FontType.MagicMedieval14], "Elvine Color: ", new Vector2(x + 5 + 1, y + 135 + 1), Color.Black);
                            spriteBatch.DrawString(Cache.Fonts[FontType.MagicMedieval14], "Elvine Color: ", new Vector2(x + 5, y + 135), Color.White);
                            break;
                        }
                }
            }
        }

        public bool HandleMouse(bool LeftClickCheck)
        {
            if (LeftClickCheck && selected)
            {
                switch (state)
                {
                    case MenuItemState.Normal:
                        {
                            state = MenuItemState.Expanded; LeftClickCheck = false;
                            changedStated = true;
                            break;
                        }
                    case MenuItemState.Expanded:
                        {
                            if (((display.Mouse.Y > normalPosition.Y + scrollPosition + scrollBarPosition) && (display.Mouse.Y < (normalPosition.Y + scrollPosition + normalHeight + scrollBarPosition))) && ((display.Mouse.X > normalPosition.X) && (display.Mouse.X < (normalPosition.X + normalWidth)))) 
                            {
                                state = MenuItemState.Normal; LeftClickCheck = false;
                                changedStated = true;
                                break;
                            }
                            else
                            {
                                switch (settingsType)
                                {
                                    case AdvancedSettingsType.TeamColor:
                                        {
                                            break;
                                        }
                                }
                            }

                            break;
                        }
                }
            }
            return LeftClickCheck;
        }

        public void HandleKeyboard()
        {
            
        }

        public void MoveUp(int amount)
        {
            scrollPosition = scrollPosition - amount;
        }

        public void MoveDown(int amount)
        {
            scrollPosition = scrollPosition + amount;
        }

        public bool Selected { get { return selected; } set { selected = value; } }
        public Vector2 NormalPosition { get { return normalPosition; } set { normalPosition = value; } }
        public AdvancedSettingsType SettingsType { get { return settingsType; } set { settingsType = value; } }
        public MenuItemState State { get { return state; } set { state = value; } }
        public int ExtendedHeight { get { return extendedHeight; } }
        public bool HasChangedState { get { return changedStated; } set { changedStated = value; } }
        public int ScrollBarPosition { get { return scrollBarPosition; } set { scrollBarPosition = value; } }
    }
}
