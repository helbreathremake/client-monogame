﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Helbreath.Common;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;


namespace Client.Assets.UI
{
    public class MenuItem
    {
        private string text;
        private MenuItemType type;
        private GameFont font;
        private GameFontType fontType;
        private Vector2 position;
        private int width;
        private int height;
        private bool selected;
        private bool highlighted;
        private Color color;
        private MenuDialogBoxType boxType;
        private bool isScrollable;
        private int scrollPosition;
        private SettingsType settingType;
        private Texture2D texture;
        private MenuItemState itemState;
        private Rectangle destinationRectangle;

        public MenuItem(MenuItemType type, string text)
        {
            this.type = type;
            this.text = text;
            this.isScrollable = false;
            this.selected = false;
        }

        public MenuItem(MenuItemType type, string text, Color color)
        {
            this.type = type;
            this.text = text;
            this.color = color;
            this.isScrollable = false;
            this.selected = false;
        }

        public MenuItem(MenuItemType type, string text, GameFont font, GameFontType fontType)
        {
            this.type = type;
            this.text = text;
            this.font = font;
            this.fontType = fontType;
            this.isScrollable = false;
            this.selected = false;

            if (type == MenuItemType.Text)
            {
                Vector2 size = font.MeasureString(text, fontType);
                width = (int)size.X;
                height = (int)size.Y;
            }
        }

        public void SetPosition(Vector2 defaultPosition)
        {
            this.position = defaultPosition;
        }

        public string Text { get { return text; } set { text = value; } }
        public MenuItemType Type { get { return type; } set { type = value; } }
        public GameFont Font { get { return font; } }
        public GameFontType FontType { get { return fontType; } }
        public Vector2 Position { get { return position; } }
        public int Width { get { return width; } set { width = value; } }
        public int Height { get { return height; } set { height = value; } }
        public bool Highlighted { get { return highlighted; } set { highlighted = value; } }
        public bool Selected { get { return selected; } set { selected = value; } }
        public MenuDialogBoxType BoxType { get { return boxType; } set { boxType = value; } }
        public bool IsScrollable { get { return isScrollable; } set { isScrollable = value; } }
        public int ScrollPosition { get { return scrollPosition; } set { scrollPosition = value; } }
        public SettingsType SettingType { get { return settingType; } set { settingType = value; } }
        public Texture2D Texture { get { return texture; } set { texture = value; } }
        public MenuItemState ItemState { get { return itemState; } set { itemState = value; } }
        public Rectangle DestinationRectangle { get { return destinationRectangle; } set { destinationRectangle = value; } }
    }
}
