﻿using System;
using System.Runtime.InteropServices;
using Microsoft.Xna.Framework;
using System.Threading;
using Microsoft.Xna.Framework.Input;
using System.Collections.Generic;

namespace Client.Assets.UI
{


    public interface IKeyboardSubscriber
    {
        void RecieveTextInput(char inputChar);
        void RecieveTextInput(string text);
        void RecieveCommandInput(char command);
        void RecieveSpecialInput(Microsoft.Xna.Framework.Input.Keys key);

        bool Selected { get; set; } //or Focused
    }

    public class KeyboardDispatcher
    {
        public class CharacterEventArgs : EventArgs
        {
            public char Character { get; private set; }

            public CharacterEventArgs(char character)
            {
                Character = character;
            }
        }

        public class KeyEventArgs : EventArgs
        {
            public Keys KeyCode { get; private set; }

            public KeyEventArgs(Keys keyCode)
            {
                KeyCode = keyCode;
            }
        }

        public delegate void CharEnteredHandler(object sender, CharacterEventArgs e, KeyboardState ks);

        public delegate void KeyEventHandler(object sender, KeyEventArgs e, KeyboardState ks);

        public static readonly List<char> SPECIAL_CHARACTERS = new List<char>{ '\a', '\b', '\n', '\r', '\f', '\t', '\v' };

        private static KeyboardState prevKeyState;

        private static Keys? repChar;
        private static DateTime downSince = DateTime.Now;
        private static float timeUntilRepInMillis;
        private static int repsPerSec;
        private static DateTime lastRep = DateTime.Now;
        private static bool filterSpecialCharacters;

        public KeyboardDispatcher(GameWindow g, float timeUntilRepInMilliseconds, int repsPerSecond,
            bool filterSpecialCharactersFromCharPressed = true)
        {
            timeUntilRepInMillis = timeUntilRepInMilliseconds;
            repsPerSec = repsPerSecond;
            filterSpecialCharacters = filterSpecialCharactersFromCharPressed;
            g.TextInput += TextEntered;
        }

        public static bool ShiftDown
        {
            get
            {
                KeyboardState state = Keyboard.GetState();
                return state.IsKeyDown(Keys.LeftShift) || state.IsKeyDown(Keys.RightShift);
            }
        }

        public static bool CtrlDown
        {
            get
            {
                KeyboardState state = Keyboard.GetState();
                return state.IsKeyDown(Keys.LeftControl) || state.IsKeyDown(Keys.RightControl);
            }
        }

        public static bool AltDown
        {
            get
            {
                KeyboardState state = Keyboard.GetState();
                return state.IsKeyDown(Keys.LeftAlt) || state.IsKeyDown(Keys.RightAlt);
            }
        }

        private void TextEntered(object sender, TextInputEventArgs e)
        {
            if(_subscriber != null)
            {
                if (!filterSpecialCharacters || !SPECIAL_CHARACTERS.Contains(e.Character))
                {
                    _subscriber.RecieveTextInput(e.Character);
                } else
                {
                    _subscriber.RecieveCommandInput(e.Character);
                }
            }
        }

        //private KeyboardState prevState;
        //private KeyboardState currentState;

       
        //public void Update()
        //{
        //    if(_subscriber != null)
        //    {
        //        prevState = currentState;
        //        currentState = Keyboard.GetState();
        //        if (currentState.GetPressedKeyCount() == 1)
        //        {
        //            Keys key = currentState.GetPressedKeys()[0];
        //            if (prevState == null || prevState.IsKeyUp(key))
        //            {
        //                if (currentState.IsKeyDown(Keys.Back))
        //                {
        //                    _subscriber.RecieveCommandInput('\b');
        //                }
        //                else
        //                {
        //                    _subscriber.RecieveTextInput(currentState.GetPressedKeys()[0].ToString());
        //                }
        //            }
                  
        //        }
        //    }
        //}

        void EventInput_KeyDown(object sender, KeyEventArgs e)
        {
            if (_subscriber == null)
                return;

            _subscriber.RecieveSpecialInput(e.KeyCode);
        }

        void EventInput_CharEntered(object sender, CharacterEventArgs e)
        {
            if (_subscriber == null)
                return;
            if (char.IsControl(e.Character))
            {
                //ctrl-v
                if (e.Character == 0x16)
                {
                    //XNA runs in Multiple Thread Apartment state, which cannot recieve clipboard
                    Thread thread = new Thread(PasteThread);
                    thread.SetApartmentState(ApartmentState.STA);
                    thread.Start();
                    thread.Join();
                    _subscriber.RecieveTextInput(_pasteResult);
                }
                else
                {
                    _subscriber.RecieveCommandInput(e.Character);
                }
            }
            else
            {
                _subscriber.RecieveTextInput(e.Character);
            }
        }

        IKeyboardSubscriber _subscriber;
        public IKeyboardSubscriber Subscriber
        {
            get { return _subscriber; }
            set
            {
                if (_subscriber != null)
                    _subscriber.Selected = false;
                _subscriber = value;
                if (value != null)
                    value.Selected = true;
            }
        }

        //Thread has to be in Single Thread Apartment state in order to receive clipboard
        string _pasteResult = "";
        [STAThread]
        void PasteThread()
        {
            //if (Clipboard.ContainsText())
            //{
            //    _pasteResult = Clipboard.GetText();
            //}
            //else
            //{
            //    _pasteResult = "";
            //}

            // TODO - FIX CLIPBOARD
            _pasteResult = "";
        }
    }


}
