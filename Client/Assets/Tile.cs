﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework.Graphics;



namespace Client.Assets
{
    public class Tile
    {
        private Texture2D texture;
        //private List<Rectangle> frames;
        private List<AnimationFrame> frames;

        public Tile(Texture2D texture)
        {
            this.texture = texture;
            //this.frames = new List<Rectangle>();
            this.frames = new List<AnimationFrame>();
        }

        public Texture2D Texture { get { return texture; } }
        //public List<Rectangle> Frames { get { return frames; } set { frames = value; } }
        public List<AnimationFrame> Frames { get { return frames; } set { frames = value; } }
        public System.Drawing.Image MiniMapImage;
    }
}
