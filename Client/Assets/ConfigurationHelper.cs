﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml;
using Microsoft.Xna.Framework;

using Client.Assets.UI;
using Helbreath.Common;
using Client.Assets.Game;

namespace Client.Assets
{
    public static class ConfigurationHelper
    {
        public static GameSettings LoadGameSettings(string configFilePath)
        {
            GameSettings settings = new GameSettings();

            settings.CurrentBGM = string.Empty;
            settings.LockedDialogs = true;
            settings.IsLoggingOut = settings.IsRestarting = false;
            settings.LogOutCount = 11;
            settings.RestartCount = 6;
            settings.LogOutTime = settings.RestartTime = DateTime.Now;
            settings.ChatFilters = new Dictionary<GameColor, bool>();
            settings.NoTeamColor = System.Drawing.Color.White.ToArgb();

            XmlTextReader reader = new XmlTextReader(configFilePath);

            while (reader.Read())
                if (reader.IsStartElement())
                    switch (reader.Name)
                    {
                        case "Bool":
                            {
                                settings.MusicOn = Boolean.Parse(reader["MusicOn"]);
                                settings.SoundsOn = Boolean.Parse(reader["SoundOn"]);
                                settings.TransparentDialogs = Boolean.Parse(reader["TransparentDialogs"]);
                                settings.TeamColorOn = Boolean.Parse(reader["TeamColorOn"]);
                                settings.PartyColorOn = Boolean.Parse(reader["PartyColorOn"]);
                                settings.GuildColorOn = Boolean.Parse(reader["GuildColorOn"]);
                                settings.TownColorOn = Boolean.Parse(reader["TownColorOn"]);
                                settings.FullScreen = Boolean.Parse(reader["FullScreen"]);
                                settings.LowHealthIndicator = Boolean.Parse(reader["LowHealthIndicator"]);
                                settings.RegenRings = Boolean.Parse(reader["RegenRings"]);
                                settings.HpRing = Boolean.Parse(reader["HpRing"]);
                                settings.MpRing = Boolean.Parse(reader["MpRing"]);
                                settings.SpRing = Boolean.Parse(reader["SpRing"]);
                                settings.ExpRing = Boolean.Parse(reader["ExpRing"]);
                                if (reader["InventoryV2"] != null)
                                    settings.InventoryV2 = Boolean.Parse(reader["InventoryV2"]);
                                else settings.InventoryV2 = true;
                                //if (reader["SpellBookV2"] != null)
                                //    settings.SpellBookV2 = Boolean.Parse(reader["SpellBookV2"]);
                                //else settings.SpellBookV2 = true;
                                break;
                            }

                        case "Float":
                            {
                                settings.MusicVolume = float.Parse(reader["MusicVolume"]);
                                settings.SoundVolume = float.Parse(reader["SoundVolume"]);
                                settings.LowHealthFlashValue = float.Parse(reader["LowHealthFlashValue"]);
                                break;
                            }

                        case "Int":
                            {
                                settings.MaxChatHistory = Int32.Parse(reader["MaxChatHistory"]);
                                settings.MiniMapZoomLevel = Int32.Parse(reader["MiniMapZoomLevel"]);
                                settings.PartyTeamColor = Int32.Parse(reader["PartyTeamColor"]);
                                settings.GuildTeamColor = Int32.Parse(reader["GuildTeamColor"]);
                                settings.AresdenTeamColor = Int32.Parse(reader["AresdenTeamColor"]);
                                settings.ElvineTeamColor = Int32.Parse(reader["ElvineTeamColor"]);
                                break;
                            }

                        case "ChatFilters":
                            {
                                settings.ChatFilters.Add(GameColor.Local, Boolean.Parse(reader["Local"]));
                                settings.ChatFilters.Add(GameColor.Whisper, Boolean.Parse(reader["Wisper"]));
                                settings.ChatFilters.Add(GameColor.Global, Boolean.Parse(reader["Global"]));
                                settings.ChatFilters.Add(GameColor.Town, Boolean.Parse(reader["Town"]));
                                settings.ChatFilters.Add(GameColor.Party, Boolean.Parse(reader["Party"]));
                                settings.ChatFilters.Add(GameColor.Guild, Boolean.Parse(reader["Guild"]));
                                break;
                            }

                        case "String":
                            {
                                Resolution resoluton;
                                GraphicsDetail detail;
                                Enum.TryParse(reader["Resolution"], out resoluton);
                                settings.Resolution = resoluton;
                                Enum.TryParse(reader["DetailMode"], out detail);
                                settings.DetailMode = detail;
                                break;
                            }

                    }
            reader.Close();
            return settings;
        }

        public static void SaveGameSettings(string configFilePath)
        {
            //Multiple Clients open causes crash because The process cannot access the file Configs\gamesettings.xml' because it is being used by another process.
            using (TextWriter file = new StreamWriter(configFilePath, false))
            {
                file.WriteLine("<Settings>");

                //BOOL
                StringBuilder lineBool = new StringBuilder();
                lineBool.Append("   <Bool MusicOn=\"");
                lineBool.Append(Cache.GameSettings.MusicOn.ToString());
                lineBool.Append("\" SoundOn=\"");
                lineBool.Append(Cache.GameSettings.SoundsOn.ToString());
                lineBool.Append("\" TransparentDialogs=\"");
                lineBool.Append(Cache.GameSettings.TransparentDialogs.ToString());
                lineBool.Append("\" TeamColorOn=\"");
                lineBool.Append(Cache.GameSettings.TeamColorOn.ToString());
                lineBool.Append("\" PartyColorOn=\"");
                lineBool.Append(Cache.GameSettings.PartyColorOn.ToString());
                lineBool.Append("\" GuildColorOn=\"");
                lineBool.Append(Cache.GameSettings.GuildColorOn.ToString());
                lineBool.Append("\" TownColorOn=\"");
                lineBool.Append(Cache.GameSettings.TownColorOn.ToString());
                lineBool.Append("\" FullScreen=\"");
                lineBool.Append(Cache.GameSettings.FullScreen.ToString());
                lineBool.Append("\" InventoryV2=\"");
                lineBool.Append(Cache.GameSettings.InventoryV2.ToString());
                //lineBool.Append("\" SpellBookV2=\"");
                //lineBool.Append(Cache.GameSettings.SpellBookV2.ToString());
                lineBool.Append("\" LowHealthIndicator=\"");
                lineBool.Append(Cache.GameSettings.LowHealthIndicator.ToString());
                lineBool.Append("\" RegenRings=\"");
                lineBool.Append(Cache.GameSettings.RegenRings.ToString());
                lineBool.Append("\" HpRing=\"");
                lineBool.Append(Cache.GameSettings.HpRing.ToString());
                lineBool.Append("\" MpRing=\"");
                lineBool.Append(Cache.GameSettings.MpRing.ToString());
                lineBool.Append("\" SpRing=\"");
                lineBool.Append(Cache.GameSettings.SpRing.ToString());
                lineBool.Append("\" ExpRing=\"");
                lineBool.Append(Cache.GameSettings.ExpRing.ToString());
                lineBool.Append("\" />");
                file.WriteLine(lineBool.ToString());

                //FLOAT
                StringBuilder lineFloat = new StringBuilder();
                lineFloat.Append("   <Float MusicVolume=\"");
                lineFloat.Append(Cache.GameSettings.MusicVolume.ToString());
                lineFloat.Append("\" SoundVolume=\"");
                lineFloat.Append(Cache.GameSettings.SoundVolume.ToString());
                lineFloat.Append("\" LowHealthFlashValue=\"");
                lineFloat.Append(Cache.GameSettings.LowHealthFlashValue.ToString());
                lineFloat.Append("\" />");
                file.WriteLine(lineFloat.ToString());

                //INT
                StringBuilder lineInt = new StringBuilder();
                lineInt.Append("   <Int MaxChatHistory=\"");
                lineInt.Append(Cache.GameSettings.MaxChatHistory.ToString());
                lineInt.Append("\" MiniMapZoomLevel=\"");
                lineInt.Append(Cache.GameSettings.MiniMapZoomLevel.ToString());
                lineInt.Append("\" PartyTeamColor=\"");
                lineInt.Append(Cache.GameSettings.PartyTeamColor.ToString());
                lineInt.Append("\" GuildTeamColor=\"");
                lineInt.Append(Cache.GameSettings.GuildTeamColor.ToString());
                lineInt.Append("\" AresdenTeamColor=\"");
                lineInt.Append(Cache.GameSettings.AresdenTeamColor.ToString());
                lineInt.Append("\" ElvineTeamColor=\"");
                lineInt.Append(Cache.GameSettings.ElvineTeamColor.ToString());
                lineInt.Append("\" />");
                file.WriteLine(lineInt.ToString());

                //CHATFILTERS
                StringBuilder lineChatFilters = new StringBuilder();
                lineChatFilters.Append("   <ChatFilters Local=\"");
                lineChatFilters.Append(Cache.GameSettings.ChatFilters[GameColor.Local].ToString());
                lineChatFilters.Append("\" Wisper=\"");
                lineChatFilters.Append(Cache.GameSettings.ChatFilters[GameColor.Whisper].ToString());
                lineChatFilters.Append("\" Global=\"");
                lineChatFilters.Append(Cache.GameSettings.ChatFilters[GameColor.Global].ToString());
                lineChatFilters.Append("\" Town=\"");
                lineChatFilters.Append(Cache.GameSettings.ChatFilters[GameColor.Town].ToString());
                lineChatFilters.Append("\" Party=\"");
                lineChatFilters.Append(Cache.GameSettings.ChatFilters[GameColor.Party].ToString());
                lineChatFilters.Append("\" Guild=\"");
                lineChatFilters.Append(Cache.GameSettings.ChatFilters[GameColor.Guild].ToString());
                lineChatFilters.Append("\" />");
                file.WriteLine(lineChatFilters.ToString());

                //STRING
                StringBuilder lineString = new StringBuilder();
                lineString.Append("   <String DetailMode=\"");
                lineString.Append(Cache.GameSettings.DetailMode.ToString());
                lineString.Append("\" Resolution=\"");
                lineString.Append(Cache.GameSettings.Resolution.ToString());
                lineString.Append("\" />");
                file.WriteLine(lineString.ToString());
                file.WriteLine("</Settings>");
            }
        }

        public static Dictionary<int, HotKey> LoadHotKeys(string configFilePath)
        {
            Dictionary<int, HotKey> hotkeys = new Dictionary<int, HotKey>();

            XmlTextReader reader = new XmlTextReader(configFilePath);

            while (reader.Read())
                if (reader.IsStartElement())
                    if (reader.Name.Equals("Hotkey"))
                        if (!hotkeys.ContainsKey(Int32.Parse(reader["Index"])))
                        {
                            HotKey hotkey = HotKey.ParseXml(reader);
                            hotkeys.Add(hotkey.Index, hotkey);
                        }

            reader.Close();

            return hotkeys;
        }

        public static Dictionary<int, HotBarIcon> LoadHotBar(string configFilePath)
        {
            Dictionary<int, HotBarIcon> hotbar = new Dictionary<int, HotBarIcon>();

            XmlTextReader reader = new XmlTextReader(configFilePath);

            while (reader.Read())
                if (reader.IsStartElement())
                    if (reader.Name.Equals("HotBarIcon"))
                    {
                        int slot = Int32.Parse(reader["Slot"]);
                        if (!hotbar.ContainsKey(slot))
                        {
                            HotBarIcon hotbarIcon = HotBarIcon.ParseXml(reader);
                            hotbar.Add(slot, hotbarIcon);
                        }
                    }

            reader.Close();

            return hotbar;
        }

        public static Dictionary<string, Dictionary<GameDialogBoxType, GameDialogBoxConfiguration>[]> LoadDialogLocations(string configFilePath)
        {
            int maxLength = Enum.GetValues(typeof(Resolution)).Length;
            Resolution resolution = 0;
            string character = "None";
            GameDialogBoxConfiguration config;
            GameDialogBoxType type;
            DialogBoxState state;
            Dictionary<string, Dictionary<GameDialogBoxType, GameDialogBoxConfiguration>[]> characters = new Dictionary<string, Dictionary<GameDialogBoxType, GameDialogBoxConfiguration>[]>();
            Dictionary<GameDialogBoxType, GameDialogBoxConfiguration>[] locationsArray = new Dictionary<GameDialogBoxType, GameDialogBoxConfiguration>[maxLength];
            Dictionary<GameDialogBoxType, GameDialogBoxConfiguration> locations = new Dictionary<GameDialogBoxType, GameDialogBoxConfiguration>();
            try
            {
                if (!File.Exists(configFilePath)) return characters;

                XmlTextReader reader = new XmlTextReader(configFilePath);
            
                    while (reader.Read())
                    {
                        if (reader.IsStartElement())
                        {
                                if (reader.Name.Equals("Character"))
                                {
                                    character = reader.GetAttribute("Name");
                                }

                                if (reader.Name.Equals("Resolution"))
                                {
                                    Enum.TryParse<Resolution>(reader["Size"], out resolution);
                                }

                                if (reader.Name.Equals("Dialog"))
                                {
                                    if (Enum.TryParse<GameDialogBoxType>(reader["Type"], out type) && !locations.ContainsKey(type))
                                    {
                                        if (reader["State"] != null && Enum.TryParse<DialogBoxState>(reader["State"], out state))
                                        { }
                                        else state = DialogBoxState.Normal;

                                        config = new GameDialogBoxConfiguration(type, resolution);
                                        config.X = Int32.Parse(reader["X"]);
                                        config.Y = Int32.Parse(reader["Y"]);
                                        config.Hidden = Boolean.Parse(reader["Hidden"]);
                                        config.AlwaysVisible = Boolean.Parse(reader["AlwaysVisible"]);
                                        config.State = state;

                                        locations.Add(type, config);
                                    }
                                }
                        }

                        if (!reader.IsStartElement())
                        {
                            if (reader.Name.Equals("Character"))
                            {
                                characters.Add(character, locationsArray);
                                locationsArray = new Dictionary<GameDialogBoxType, GameDialogBoxConfiguration>[maxLength];
                            }

                            if (reader.Name.Equals("Resolution"))
                            {
                                locationsArray[(int)resolution] = locations;
                                locations = new Dictionary<GameDialogBoxType, GameDialogBoxConfiguration>();
                            }

                        }
                    }
                    reader.Close();
                }
            catch { }

            return characters;
        }

        public static void SaveDialogLocations(string configFilePath, Dictionary<string, Dictionary<GameDialogBoxType, GameDialogBoxConfiguration>[]> characters)
        {
            using (TextWriter file = new StreamWriter(configFilePath, false)) //error from two clients running same time and tying to access this
            {
                file.WriteLine("<Dialogs>");
                int maxResolutions = Enum.GetValues(typeof(Resolution)).Length;

                if (characters != null)
                {
                    foreach (KeyValuePair<string, Dictionary<GameDialogBoxType, GameDialogBoxConfiguration>[]> character in characters)
                    {

                        StringBuilder builder = new StringBuilder();
                        builder.Append("    <Character Name=\""); //TODO Limit types of characters names can have because it causes issues with xml <>! ect.
                        builder.Append(character.Key);
                        builder.Append("\" >");
                        file.WriteLine(builder.ToString());
                        builder.Clear();

                        for (int x = 0; x < maxResolutions; x++)
                        {
                            StringBuilder resolutionLine = new StringBuilder();
                            builder.Append("        <Resolution Size=\"");
                            builder.Append(Enum.GetNames(typeof(Resolution)).ElementAt(x).ToString());
                            builder.Append("\" >");
                            file.WriteLine(builder.ToString());
                            builder.Clear();

                            foreach (KeyValuePair<GameDialogBoxType, GameDialogBoxConfiguration> box in character.Value[x])
                            {
                                builder.Append("            <Dialog Type=\"");
                                builder.Append(box.Key.ToString());
                                builder.Append("\" X=\"");
                                builder.Append(box.Value.X);
                                builder.Append("\" Y=\"");
                                builder.Append(box.Value.Y);
                                builder.Append("\" Hidden=\"");
                                builder.Append(box.Value.Hidden.ToString());
                                builder.Append("\" AlwaysVisible=\"");
                                builder.Append(box.Value.AlwaysVisible.ToString());
                                builder.Append("\" State=\"");
                                builder.Append(box.Value.State.ToString());
                                builder.Append("\" />");
                                file.WriteLine(builder.ToString());
                                builder.Clear();
                            }
                            file.WriteLine("        </Resolution>");
                        }
                        file.WriteLine("    </Character>");
                    }
                }
                file.WriteLine("</Dialogs>");
                file.Close();
            }
        }

        public static void SaveHotKeys(string configFilePath, Dictionary<int, HotKey> hotKeyConfig)
        {
            using (TextWriter file = new StreamWriter(configFilePath, false))
            {
                file.WriteLine("<HotKeys>");

                foreach (HotKey hotkey in hotKeyConfig.Values)
                {
                    StringBuilder line = new StringBuilder();
                    line.Append("   <Hotkey Index =\"");
                    line.Append(hotkey.Index);
                    line.Append("\" Press=\"");
                    line.Append(hotkey.Press.ToString());
                    line.Append("\" Hold=\"");
                    line.Append(hotkey.Hold.ToString());
                    line.Append("\" Action=\"");
                    line.Append(hotkey.Action.ToString());
                    line.Append("\" />");

                    file.WriteLine(line.ToString());
                }

                foreach (KeyValuePair<int, HotBarIcon> icon in Cache.HotBarConfiguration)
                {
                    StringBuilder line = new StringBuilder();
                    line.Append("   <HotBarIcon Slot=\"");
                    line.Append(icon.Key);
                    line.Append("\" Type=\"");
                    line.Append(icon.Value.Type.ToString());
                    line.Append("\" Index =\"");
                    line.Append(icon.Value.Index);
                    line.Append("\" />");

                    file.WriteLine(line.ToString());
                }

                file.WriteLine("</HotKeys>");
                file.Close();
            }
        }

        public static void ResetHotKeys(string configFilePath)
        {
            using (TextWriter file = new StreamWriter(configFilePath, false))
            {
                file.WriteLine("<HotKeys>");
                file.WriteLine("    <Hotkey Index =\"1\" Press=\"F1\" Hold=\"None\" Action=\"HelpScreen\" />");
                file.WriteLine("</HotKeys>");

                file.Close();
            }

        }

        public static Dictionary<GameColor, Color> LoadGameColors()
        {
            Dictionary<GameColor, Color> colors = new Dictionary<GameColor, Color>();

            //DEFAULT
            colors.Add(GameColor.None, new Color(255, 255, 255));
            colors.Add(GameColor.Default, new Color(255, 255, 255));
            colors.Add(GameColor.Custom, new Color(255, 255, 255));

            //GENERAL
            colors.Add(GameColor.Normal, SpriteHelper.ColourFromArgb(-1));
            colors.Add(GameColor.Exploited, SpriteHelper.ColourFromArgb(-8355712));
            colors.Add(GameColor.Shadow, SpriteHelper.ColourFromArgb(-16777216));
            colors.Add(GameColor.Berserk, SpriteHelper.ColourFromArgb(-23296));
            colors.Add(GameColor.Haste, SpriteHelper.ColourFromArgb(-986896));

            //WEAPONS  
            colors.Add(GameColor.Agile, SpriteHelper.ColourFromArgb(-7566181));
            colors.Add(GameColor.Light, SpriteHelper.ColourFromArgb(-7566181));
            colors.Add(GameColor.Strong, SpriteHelper.ColourFromArgb(-7566181));
            colors.Add(GameColor.Poison, SpriteHelper.ColourFromArgb(-7885945));
            colors.Add(GameColor.Critical, SpriteHelper.ColourFromArgb(-1068752));
            colors.Add(GameColor.Sharp, SpriteHelper.ColourFromArgb(-10193980));
            colors.Add(GameColor.Righteous, SpriteHelper.ColourFromArgb(-2302498));
            colors.Add(GameColor.Ancient, SpriteHelper.ColourFromArgb(-4425028));
            colors.Add(GameColor.Legendary, SpriteHelper.ColourFromArgb(-10027009));
            colors.Add(GameColor.Blood, SpriteHelper.ColourFromArgb(-57826));
            colors.Add(GameColor.Golden, SpriteHelper.ColourFromArgb(-2509483));
            colors.Add(GameColor.Vampire, SpriteHelper.ColourFromArgb(-2509483));
            colors.Add(GameColor.Zealot, SpriteHelper.ColourFromArgb(-6723841));
            colors.Add(GameColor.Exhaust, SpriteHelper.ColourFromArgb(-6723841));
            colors.Add(GameColor.Crits, SpriteHelper.ColourFromArgb(-24064));
            colors.Add(GameColor.Piercing, SpriteHelper.ColourFromArgb(-8372224));
            colors.Add(GameColor.Absorption, SpriteHelper.ColourFromArgb(-128));

            //ARMOURS  
            colors.Add(GameColor.Manufactured, SpriteHelper.ColourFromArgb(-6447742));
            colors.Add(GameColor.Health, SpriteHelper.ColourFromArgb(-54016));
            colors.Add(GameColor.Mana, SpriteHelper.ColourFromArgb(-16755201));
            colors.Add(GameColor.Staminar, SpriteHelper.ColourFromArgb(-16711872));

            //QUALITY 
            colors.Add(GameColor.Flimsy, SpriteHelper.ColourFromArgb(-24064));
            colors.Add(GameColor.Sturdy, SpriteHelper.ColourFromArgb(-6723841));
            colors.Add(GameColor.Reinforced, SpriteHelper.ColourFromArgb(-2509483));
            colors.Add(GameColor.Studded, SpriteHelper.ColourFromArgb(-10027009));

            //DAMAGE  
            colors.Add(GameColor.DamageMelee, new Color(230, 230, 25));
            colors.Add(GameColor.DamageRanged, new Color(255, 255, 255));
            colors.Add(GameColor.DamageMagic, new Color(25, 168, 230));
            colors.Add(GameColor.DamagePoison, new Color(61, 158, 0));
            colors.Add(GameColor.DamageEnvironmental, new Color(255, 149, 0));
            colors.Add(GameColor.DamageSpirit, new Color(153, 153, 153));

            //VITALS  
            colors.Add(GameColor.HP, new Color(173, 0, 0));
            colors.Add(GameColor.MP, new Color(0, 114, 196));
            colors.Add(GameColor.SP, new Color(20, 124, 0)); 
            colors.Add(GameColor.Hunger, new Color(204, 142, 0)); 

            //CHAT   
            colors.Add(GameColor.Command, new Color(255, 255, 255));
            colors.Add(GameColor.Local, new Color(255, 225, 255)); 
            colors.Add(GameColor.Whisper, new Color(150, 150, 170));
            colors.Add(GameColor.Global, new Color(255, 130, 130)); 
            colors.Add(GameColor.Town, new Color(130, 130, 255));
            colors.Add(GameColor.GameMaster, new Color(153, 102, 255));//(135, 55, 220) 
            colors.Add(GameColor.Guild, new Color(130, 255, 130));
            colors.Add(GameColor.Party, new Color(230, 230, 130));

            //RELATIONSHIP
            colors.Add(GameColor.Friendly, new Color(30, 255, 30)); 
            colors.Add(GameColor.Enemy, Color.Red); 
            colors.Add(GameColor.Neutral, new Color(50, 50, 255));
            colors.Add(GameColor.Wild, new Color(120, 120, 120));

            //TOWNS  
            colors.Add(GameColor.Aresden, new Color(255, 60, 60));// Aresden (255, 45, 0) //new Color(255, 60, 60)););
            colors.Add(GameColor.Elvine, new Color(60, 60, 255)); //Elvine (0 85, 255) * maybe G = 45? //new Color(60, 60, 255)););

            //DIALOG  
            colors.Add(GameColor.Orange, new Color(255, 162, 0));
            colors.Add(GameColor.Green, new Color(30, 255, 30));
            colors.Add(GameColor.LevelUp, new Color(255, 255, 20));
            colors.Add(GameColor.MajesticUp, new Color(255, 255, 20));
            return colors;
        }
    }
}
