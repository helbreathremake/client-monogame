﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;


using Client.Assets.Effects;
using Client.Assets.State;
using Helbreath.Common;
using Helbreath.Common.Assets;
using Client.Assets.Game;

namespace Client.Assets
{
    public class Player : IOwner
    {
        //Iowner        
        private UInt16 objectId;
        private DateTime lastLogin;
        private bool safeMode;
        private bool moveReady;
        private bool isMoving;
        private bool runningMode;
        private bool isRunning;
        private bool isCasting;
        private int spellRequest; // cast request during movement - avoid cast between cells
        private int spellCharged;
        private bool spellReady;
        private bool spellJustCast;
        private DateTime spellJustCastTime;
        private bool specialAbilityReady;
        private int tradePartner;
        private int prevX;
        private int prevY;
        private int x;
        private int y;
        private int offsetX;
        private int offsetY; // object is moving, animate between cell. note: humans have 8 walk/run frames, mobs have only 4
        private List<int[]> offsetPoints; // dash/fly effect
        private int attackDestinationX;
        private int attackDestinationY;
        private int destinationX;
        private int destinationY;
        private bool haltMovement;
        private MotionDirection haltDirection;
        private int type;
        private MotionDirection direction;
        private MotionType motionType;
        private DateTime animationTime;
        private int animationFrame;
        private int effectFrame;// internal frame counter for condition sprites
        private int tempFrame; // for dash/fly
        private int commands;
        private List<ChatMessage> damageHistoryStageTwo;
        //private List<ChatMessage> vitalHistory;
        private ChatMessage vitalHistory;
        private ChatMessage damageHistory;
        private ChatMessage spellHistory;
        private ChatMessage chatHistory;

        private Map currentMap;

        private PlayType playType;
        private string name;
        private int appearance1;
        private int appearance2;
        private int appearance3;
        private int appearance4;
        private int appearanceColour;
        private bool glowPosition;
        private int glowPoint;
        private int status;

        // player
        private string accountName;
        private string accountPassword;
        private string currentMapName;
        private bool criticalAttack;
        private bool forceAttack;
        private float fadeValue; // for dead fadeout

        private int level;
        private int rebirthLevel;
        private int experience;
        private int strength;
        private int dexterity;
        private int vitality;
        private int magic;
        private int intelligence;
        private int agility;
        private Inventory inventory;
        private int contribution;
        private int majestics;
        private int gold; // EnconomyType.Virtual
        private int gladiatorPoints;
        private int hp;
        private int mp;
        private int sp;
        private bool isPoisoned;
        private bool isParalyzed;
        private bool isHeld;
        private int criticals;
        private int criminalCount;
        private int enemyKills;
        private string guildName;
        private int guildRank;
        private int reputation;
        private int rewardGold;
        private bool runChange;
        private OwnerSide side;
        private OwnerSideStatus sideStatus;
        private bool isCombatMode;
        private bool isDead;
        private bool isCorpseExploited;
        private bool tickHP;
        private bool tickMP;
        private bool tickSP;
        private bool tickEXP;
        private bool isDamaged;
        private DateTime flashDamage;
        private int luck;

        //private List<Item> inventory;
        private List<Item> warehouse;
        private bool[] magicLearned;
        private Dictionary<TitleType,int> titles;
        private int[] skills;

        public Player(string username, string password, string characterName)
        {
            //this.id = objectId;
            this.name = characterName;
            this.accountName = username;
            this.accountPassword = password;
            this.side = OwnerSide.Neutral;
            spellRequest = -1;
            tradePartner = -1;
            offsetX = offsetY = 0;
            destinationX = destinationY = -1;
            fadeValue = 255;
            isMoving = false;
            moveReady = true;
            spellJustCast = false;
            damageHistoryStageTwo = new List<ChatMessage>();
            //vitalHistory = new List<ChatMessage>();
            vitalHistory = new ChatMessage();
            damageHistory = new ChatMessage();
            spellHistory = new ChatMessage();
            chatHistory = new ChatMessage();
            flashDamage = DateTime.Now;

            inventory = new Inventory(Globals.MaximumTotalItems);
            warehouse = new List<Item>(Globals.MaximumWarehouseTabItems);
            InventoryDrawOrder = new LinkedList<int>();
            magicLearned = new bool[Globals.MaximumSpells];
            skills = new int[Globals.MaximumSkills];

            // initializes each title type
            titles = new Dictionary<TitleType, int>();
            foreach (TitleType type in (TitleType[])Enum.GetValues(typeof(TitleType)))
                titles.Add(type, 0);

            Init();
        }

        public Player()
        {
            //this.id = objectId;
            //this.name = characterName;
            //this.accountName = username;
            //this.accountPassword = password;
            this.side = OwnerSide.Neutral;
            spellRequest = -1;
            offsetX = offsetY = 0;
            destinationX = destinationY = -1;
            fadeValue = 255;
            isMoving = false;
            moveReady = true;
            spellJustCast = false;
            damageHistoryStageTwo = new List<ChatMessage>();
            //vitalHistory = new List<ChatMessage>();
            vitalHistory = new ChatMessage();
            damageHistory = new ChatMessage();
            spellHistory = new ChatMessage();
            chatHistory = new ChatMessage();
            flashDamage = DateTime.Now;

            inventory = new Inventory(Globals.MaximumTotalItems);
            warehouse = new List<Item>(Globals.MaximumWarehouseTabItems);
            InventoryDrawOrder = new LinkedList<int>();
            magicLearned = new bool[Globals.MaximumSpells];
            skills = new int[Globals.MaximumSkills];

            // initializes each title type
            titles = new Dictionary<TitleType, int>();
            foreach (TitleType type in (TitleType[])Enum.GetValues(typeof(TitleType)))
                titles.Add(type, 0);           

            Init();
        }

        public void Parse(byte[] data, int countIn, out int count)
        {
            count = countIn;
            type = BitConverter.ToInt16(data, count);
            count += 2;
            direction = (MotionDirection)(int)data[count];
            count++;
            side = (OwnerSide)(int)data[count];
            count++;
            sideStatus = (OwnerSideStatus)(int)data[count];
            count++;

            count += 2;
            count += 4;
            count++;

            count += 2;
            count += 4;
            count++;

            count += 2;
            count += 4;
            count++;

            count += 2;
            count += 4;
            count++;

            count += 2;
            count += 4;
            count++;

            count += 2;
            count += 4;
            count++;

            count += 2;
            count += 4;
            count++;

            count += 2;
            count += 4;
            count++;

            count++;
            isCombatMode = ((int)data[count] == 1);
            count++;
            status = BitConverter.ToInt32(data, count);
            count += 4;

            //if (OwnerType == OwnerType.Player)
            //{
            name = Encoding.ASCII.GetString(data, count, 10).Trim('\0');
            count += 10;
            //}
            //else
            //{
            //    Id = BitConverter.ToInt32(data, count);
            //    count += 4;
            //}
        }

        public void ParseMotion(CommandMessageType messageType, byte[] data)
        {
            int x = BitConverter.ToInt16(data, 11);
            int y = BitConverter.ToInt16(data, 13);
            MotionDirection direction = (MotionDirection)(int)data[15];

            switch (messageType)
            {
                case CommandMessageType.ObjectVitalsChanged:
                case CommandMessageType.ObjectNullAction:
                case CommandMessageType.ObjectTakeDamage: break;
                default:
                    this.direction = direction;
                    break;
            }


            side = (OwnerSide)(int)data[16];
            status = BitConverter.ToInt32(data, 17);
            type = BitConverter.ToInt16(data, 21);
            name = Encoding.ASCII.GetString(data, 23, 10).Trim('\0');

            // 33 - 80 is equipment for owner only
            // 81 = angel for owner only

            isCombatMode = ((int)data[90] == 1);
            // 83 = special ability
            // 84 = item glow
            // 85 = misc - build points etc
            sideStatus = (OwnerSideStatus)(int)data[94]; // was 45

            AttackType attackType = AttackType.Normal; DamageType damageType = DamageType.Melee;
            int damage = 0; bool isStun = false; int weaponId = 0; bool corpseExploited = false;
            int destinationX = 0; int destinationY = 0; int spellId = -1; int hitCount = 0;
            int vitalAmount = 0; VitalType vitalType = VitalType.None;
            switch (messageType)
            {
                case CommandMessageType.ObjectTakeDamage:
                case CommandMessageType.ObjectTakeDamageAndFly:
                case CommandMessageType.ObjectDying:
                    damage = BitConverter.ToInt32(data, 95);
                    isStun = ((int)data[99] == 1) ? true : false; // new, we dont stun on every hit now
                    damageType = (DamageType)((int)data[100]);
                    hitCount = (int)data[101];
                    break;
                case CommandMessageType.ObjectVitalsChanged:
                    vitalAmount = BitConverter.ToInt32(data, 95);
                    vitalType = (VitalType)((int)data[99]);
                    break;
                case CommandMessageType.ObjectAttack:
                    destinationX = BitConverter.ToInt16(data, 95);
                    destinationY = BitConverter.ToInt16(data, 97);
                    weaponId = BitConverter.ToInt32(data, 99);
                    break;
                case CommandMessageType.ObjectDead:
                    corpseExploited = ((int)data[95] == 1 ? true : false);
                    break;
                case CommandMessageType.ObjectMagic:
                    spellId = BitConverter.ToInt16(data, 95);
                    break;
            }

            if (!Cache.OwnerCache.ContainsKey(ObjectId))
                Init(x, y); // add new object to map
            else
            {
                switch (messageType) // handle movement and actions
                {
                    case CommandMessageType.ObjectRun: Run(direction); break;
                    case CommandMessageType.ObjectMove: Walk(direction); break;
                    case CommandMessageType.ObjectAttack: Attack(destinationX, destinationY, attackType); break;
                    case CommandMessageType.ObjectTakeDamage: TakeDamage(damage, damageType, hitCount, isStun); break;
                    case CommandMessageType.ObjectTakeDamageAndFly: Fly(damage, hitCount, direction, damageType); break;
                    case CommandMessageType.ObjectVitalsChanged: VitalChanged(vitalAmount, vitalType); break;
                    case CommandMessageType.ObjectStop: Idle(direction); break;
                    case CommandMessageType.ObjectPickUp: PickUp(); break;
                    case CommandMessageType.ObjectDying: Dying(damage, damageType, hitCount); break;
                    case CommandMessageType.ObjectMagic: Cast(spellId); break;
                    case CommandMessageType.ObjectAttackDash: Dash(direction, destinationX, destinationY, weaponId); break;
                    case CommandMessageType.ObjectDead: IsCorpseExploited = corpseExploited; break;
                }

                switch (messageType)
                {
                    case CommandMessageType.ObjectRun:
                    case CommandMessageType.ObjectMove:
                    case CommandMessageType.ObjectTakeDamageAndFly:
                    case CommandMessageType.ObjectAttackDash:
                        SetLocation(Map, x, y);
                        break;
                }
            }

            // remove owner from screen
            if (messageType == CommandMessageType.Reject)
            {
                if (IsDead) FadeOut(); // fade out if dead
                else Remove();
            }
        }

        public void AddToInventory(Item item)
        {
            item.SetNumber = 0;
            for (int i = Globals.MaximumEquipment; i < Globals.MaximumTotalItems; i++)
            {
                if (inventory[i] == null)
                {
                    inventory[i] = item;
                    InventoryDrawOrder.AddLast(i);
                    break;
                }
            }
            inventory.Update();
        }

        public void RemoveFromInventory(int index)
        {
            if (inventory[index] != null)
                inventory[index] = null;
            inventory.Update();
        }

        public void SwapItem(int itemIndex, int destinationItemIndex)
        {
            if (inventory[itemIndex] != null)
            {
                Item item = inventory[itemIndex];

                // dropping to an empty slot
                if (inventory[destinationItemIndex] == null)
                {
                    inventory[destinationItemIndex] = item;
                    inventory[itemIndex] = null;

                    InventoryDrawOrder.Remove(itemIndex);
                    InventoryDrawOrder.AddLast(destinationItemIndex);
                }
                // dropping on an occupied slot
                else
                {
                    Item destinationItem = inventory[destinationItemIndex];
                    inventory[itemIndex] = destinationItem;
                    inventory[destinationItemIndex] = item;
                    InventoryDrawOrder.AddLast(destinationItemIndex);
                }
            }
        }

        public void SwapWarehouseItem(int itemIndex, int destinationItemIndex)
        {
            if (warehouse[itemIndex] != null)
            {
                Item item = warehouse[itemIndex];

                // dropping to an empty slot
                if (warehouse[destinationItemIndex] == null)
                {
                    warehouse[destinationItemIndex] = item;
                    warehouse[itemIndex] = null;
                }
                // dropping on an occupied slot
                else
                {
                    Item destinationItem = warehouse[destinationItemIndex];
                    warehouse[itemIndex] = destinationItem;
                    warehouse[destinationItemIndex] = item;
                }
            }
        }

        public void EquipItem(int itemIndex)
        {
            EquipType type = inventory[itemIndex].EquipType;
            inventory[itemIndex].IsEquipped = true;
            Item item = inventory[itemIndex];

            // handle special cases
            switch (type)
            {
                case EquipType.DualHand: inventory[(int)EquipType.RightHand] = item; break;
                case EquipType.FullBody: inventory[(int)EquipType.Body] = item; break;
                default: inventory[(int)type] = item; break;
            }

            inventory.Update();
        }

        public void UnEquipItem(int equippedItemIndex, int originalItemIndex, int destinationItemIndex)
        {             
            //int originalItemIndex = -1;
            //for (int itemlocation = Globals.MaximumEquipment; itemlocation < Globals.MaximumInventoryItems; itemlocation++)
            //{
            //    if (inventory[itemlocation] != null)
            //    {
            //        if (inventory[itemlocation].GuidMatch(inventory[equippedItemIndex])) { originalItemIndex = itemlocation; } //FIX by sending guid
            //    }
            //}

            //if (originalItemIndex == -1) return; //cant find original location

            EquipType type = inventory[originalItemIndex].EquipType;

            if (equippedItemIndex == originalItemIndex)
            {
                inventory[originalItemIndex].IsEquipped = false;
            }
            else
            {
                Item item = inventory[originalItemIndex];
                item.IsEquipped = false;
                inventory[equippedItemIndex] = null; //remove from equipment slot


                // if dragged from equipment slot and destination is empty
                if (destinationItemIndex != -1 && inventory[destinationItemIndex] == null)
                {
                    inventory[destinationItemIndex] = item;
                    inventory[originalItemIndex] = null;
                }
                else
                {
                    inventory[originalItemIndex] = item;
                }
            }

            switch (type)
            {
                case EquipType.DualHand: inventory[(int)EquipType.RightHand] = null; break;
                case EquipType.FullBody: inventory[(int)EquipType.Body] = null; break;
                default: inventory[(int)type] = null; break;
            }

            inventory.Update();
        }

        public void AddWarehouse(Item item)
        {
            item.SetNumber = 0;
            for (int i = 0; i < Globals.MaximumWarehouseTabItems; i++)
                if (warehouse[i] == null)
                {
                    warehouse[i] = item;
                    return;
                }
        }

        public void Init()
        {
            isDead = false;

            inventory.Clear();
            warehouse.Clear();
            InventoryDrawOrder.Clear();

            for (int i = 0; i < Globals.MaximumTotalItems; i++) inventory.Add(null);
            for (int i = 0; i < Globals.MaximumWarehouseTabItems; i++) warehouse.Add(null);
        }

        public void Init(int x, int y)
        {
            this.x = x;
            this.y = y;

            if (isDead) { Map[y][x].DeadOwner = this; }
            else Map[y][x].Owner = this;

            Cache.OwnerCache.Add(this.objectId, this);
        }

        public void Remove()
        {
            if (Location != null)
            {
                if (Location.DeadOwner == this) Location.DeadOwner = null;
                if (Location.Owner == this) Location.Owner = null;
            }

            Cache.OwnerCache.Remove(this.objectId);
        }

        public void Idle(MotionDirection direction) { Idle(direction, false); }
        public void Idle(MotionDirection direction, bool waitForFinish = false)
        {
            if (isDead) return;

            this.direction = direction;
            motionType = MotionType.Idle;
            animationFrame = 0;
            isMoving = false;
            if (!waitForFinish) moveReady = true;
            else moveReady = false;
            destinationX = destinationY = -1;
        }

        public void Bow(MotionDirection direction)
        {
            if (isDead) return;

            this.direction = direction;
            motionType = MotionType.Bow;
            animationFrame = 0;
            moveReady = false;
        }
        public void PickUp()
        {
            if (isDead) return;

            motionType = MotionType.PickUp;
            animationFrame = 0;
            moveReady = false;
        }

        public bool WithinRange(IOwner owner) { return WithinRange(owner.X, owner.Y); }
        public bool WithinRange(int destinationX, int destinationY)
        {
            int distanceX, distanceY;

            distanceX = Math.Abs(x - destinationX);
            distanceY = Math.Abs(y - destinationY);

            int range = Utility.GetRange((Weapon != null ? Weapon.RelatedSkill : SkillType.Hand), CriticalMode, (Weapon != null ? Weapon.HasExtendedRange : false));

            if (distanceX <= range && distanceY <= range) return true;
            return false;
        }

        public bool CanDash(IOwner owner) { return CanDash(owner.X, owner.Y); }
        public bool CanDash(int destinationX, int destinationY)
        {
            if (isDead) return false;
            if (isMoving) return false;
            if (isParalyzed || isHeld) return false;
            // cant dash with bow
            if (Weapon != null)
                switch (Weapon.RelatedSkill)
                {
                    case SkillType.Archery:
                    case SkillType.BattleStaff:
                        return false;
                }

            int distanceX, distanceY;

            distanceX = Math.Abs(x - destinationX);
            distanceY = Math.Abs(y - destinationY);

            int range = 2;

            if ((distanceX == range && distanceY <= range) ||
                 distanceY == range && distanceX <= range) 
                return true;

            return false;
        }

        public void Attack(int destinationX, int destinationY, AttackType attackType)
        {
            if (isDead) return;

            criticalAttack = (attackType == AttackType.Critical);
            if (IsCombatMode || OwnerType == OwnerType.Npc)
            {
                this.attackDestinationX = destinationX;
                this.attackDestinationY = destinationY;

                SkillType skillType = (Weapon == null ? SkillType.Hand : Weapon.RelatedSkill);

                if (skillType == SkillType.Archery)
                    BowAttack();
                else
                {
                    motionType = MotionType.Attack;
                    animationFrame = 0;
                    moveReady = false;

                    if (Weapon != null && Weapon.DrawEffect != DrawEffectType.None)
                        Cache.DefaultState.AddEffect(new GameEffect(Weapon.DrawEffect, new Location(x - Cache.MapView.PivotX - Globals.OffScreenCells, y - Cache.MapView.PivotY - Globals.OffScreenCells),
                                                                                         new Location(destinationX - Cache.MapView.PivotX - Globals.OffScreenCells, destinationY - Cache.MapView.PivotY - Globals.OffScreenCells)));
                }
            }
            else Bow(direction);

            if (criticalAttack && (SetBonuses.Contains(ItemSet.HeroWarrior) || SetBonuses.Contains(ItemSet.HeroArcher)))
                Cache.DefaultState.AddEffect(new GameEffect(DrawEffectType.HeroWarrior, new Location(x - (Cache.MapView.PivotX + Globals.OffScreenCells), y - (Cache.MapView.PivotY + Globals.OffScreenCells)), null));
        }

        public void BowAttack()
        {
            if (isDead) return;

            motionType = MotionType.BowAttack;
            animationFrame = 0;
            moveReady = false;
        }

        public void Dash(MotionDirection direction, int destinationX, int destinationY, int weaponId)
        {
            if (isDead) return;
            if (isParalyzed || isHeld) return;

            prevX = x; prevY = y;

            switch (direction)
            {
                case MotionDirection.North: y--; break;
                case MotionDirection.NorthEast: y--; x++; break;
                case MotionDirection.East: x++; break;
                case MotionDirection.SouthEast: x++; y++; break;
                case MotionDirection.South: y++; break;
                case MotionDirection.SouthWest: y++; x--; break;
                case MotionDirection.West: x--; break;
                case MotionDirection.NorthWest: x--; y--; break;
                default: return;
            }

            if (Location.IsMoveAllowed)
            {
                this.direction = direction;
                currentMap[prevY][prevX].Owner = null;
                currentMap[y][x].Owner = this;

                motionType = MotionType.Dash;
                animationFrame = tempFrame = 0;
                isMoving = true;
                moveReady = false;

                if (this.destinationX == x && this.destinationY == y) this.destinationX = this.destinationY = -1; // what is this? forgot ._.

                Item weapon = (Cache.ItemConfiguration.ContainsKey(weaponId) ? Cache.ItemConfiguration[weaponId] : null);

                if (weapon != null && weapon.DrawEffect != DrawEffectType.None)
                    Cache.DefaultState.AddEffect(new GameEffect(weapon.DrawEffect, new Location(x - Cache.MapView.PivotX - Globals.OffScreenCells, y - Cache.MapView.PivotY - Globals.OffScreenCells),
                                                                                     new Location(destinationX - Cache.MapView.PivotX - Globals.OffScreenCells, destinationY - Cache.MapView.PivotY - Globals.OffScreenCells)));
            }
            else
            {
                x = prevX; y = prevY;
            }
        }

        /// <summary>
        /// Specifies if hands are free to cast spells. Considers staff (without shield) and battletaff (two handed) in hand.
        /// </summary>
        public bool HandsFree
        {
            get
            {
                if (isDead) return false;
                if (Weapon == null)
                {
                    return true;
                    //if (!HasShield) return true;
                    //else return false; 
                }
                else
                    switch (Weapon.RelatedSkill)
                    {
                        case SkillType.Staff:
                            return true;
                            //if (HasShield) return false;
                            //else return true;
                        case SkillType.BattleStaff:
                            return true;
                        default: return false;
                    }
            }
        }

        /// <summary>
        /// Calculates required mana for a spell base on mana save values
        /// </summary>
        /// <param name="spellId">Spell Index</param>
        /// <returns>Required mp for the spell</returns>
        public int RequiredMana(int spellId)
        {
            int manaCost = Utility.GetManaCost(Cache.MagicConfiguration[spellId].ManaCost, inventory.ManaSaveBonus);
            return manaCost;
        }

        /// <summary>
        /// Specifies whether a spell can be cast, considering the spell has been learned and you have enough MP.
        /// </summary>
        /// <param name="spellId">Spell Index</param>
        /// <returns>True if the spell can be cast. False if not.</returns>
        public bool CanCast(int spellId)
        {
            if (magicLearned[spellId] == false) return false;
            if (intelligence + IntelligenceBonus < Cache.MagicConfiguration[spellId].RequiredIntelligence) return false;
            if (mp < RequiredMana(spellId)) return false;

            return true;
        }

        public bool CanBuySpell(int spellId)
        {
            if (Cache.MagicConfiguration.ContainsKey(spellId))
            {
                if (magicLearned[spellId] == true) return false; //Already learned
                if (Cache.MagicConfiguration[spellId].GoldCost < 0) return false; 
                if (intelligence + IntelligenceBonus < Cache.MagicConfiguration[spellId].RequiredIntelligence) return false;
             
                switch (Globals.EconomyType)
                {
                    case EconomyType.Classic:
                        if (GoldItem == null || GoldItem.Count < Cache.MagicConfiguration[spellId].GoldCost) { return false; }
                        break;
                    case EconomyType.Virtual:
                        if (gold < Cache.MagicConfiguration[spellId].GoldCost)  { return false; }
                        break;
                }
                return true;
            }
            return false;
        }

        public void Cast(int magicId)
        {
            if (isDead) return;

            spellCharged = magicId;
            isCasting = true;
            motionType = MotionType.Magic;
            animationFrame = 0;

            string spellName = (Cache.MagicConfiguration.ContainsKey(magicId)) ? Cache.MagicConfiguration[magicId].Name : "Unknown-Spell";

            ChatMessage message = new ChatMessage(DateTime.Now);
            message.Message = string.Format("{0}!", spellName); 
            message.Font = FontType.MagicMedieval18;
            message.Color = (((MainGame)Cache.DefaultState).Player.IsAlly(this)) ? GameColor.Friendly : GameColor.Enemy; //TODO use Cached colors, show green spells for friendlies, red spells for enemies.
        
            spellHistory = message;

            if (SetBonuses.Contains(ItemSet.HeroMage) || SetBonuses.Contains(ItemSet.HeroBattleMage))
            {
                Cache.DefaultState.AddEffect(new GameEffect(DrawEffectType.HeroMage, new Location(x - (Cache.MapView.PivotX + Globals.OffScreenCells), y - (Cache.MapView.PivotY + Globals.OffScreenCells)), null));
            }

            if (magicId >= 70)
                Cache.DefaultState.AddEffect(new GameEffect(DrawEffectType.AdvancedMagic, new Location(x - (Cache.MapView.PivotX + Globals.OffScreenCells), y - (Cache.MapView.PivotY + Globals.OffScreenCells)), null));
        }

        public bool IsAlly(IOwner owner)
        {
            if (owner.SideStatus == OwnerSideStatus.Criminal) { return false; }
            else return (side == owner.Side);
        }

        public void Dying(int damage, DamageType damageType, int hitCount)
        {
            if (isDead) return;

            isDamaged = true;
            motionType = MotionType.Die;
            animationFrame = 0;

            if (currentMap[y][x].Owner == this)
                currentMap[y][x].Owner = null;
            currentMap[y][x].DeadOwner = this;
            isDead = true;

            tickHP = tickMP = TickSP = false;
    
            // last hit
            ChatMessage message = new ChatMessage(DateTime.Now);
            //if (hitCount > 1) { message.Message = (damage > 0 && damage < Globals.CriticalDamage) ? string.Format("-{0}! {1}x!", damage, hitCount) : "Critical!"; }
            //else { message.Message = (damage > 0 && damage < Globals.CriticalDamage) ? string.Format("-{0}!", damage) : "Critical!"; }

            message.Message = (damage > 0 && damage < Globals.CriticalDamage) ? string.Format("-{0}!", damage) : "Critical!";

            message.Font = (damage >= Globals.LargeDamage) ? FontType.DamageLargeSize14Bold : (damage >= Globals.MediumDamage) ? FontType.DamageMediumSize13 : FontType.DamageSmallSize11;

            switch (damageType)
            {
                case DamageType.Environment: message.Color = GameColor.DamageEnvironmental; break;
                case DamageType.Poison: message.Color = GameColor.DamagePoison; break;
                case DamageType.Spirit: message.Color = GameColor.DamageSpirit; break;
                case DamageType.Magic: message.Color = GameColor.DamageMagic; break;
                case DamageType.Melee: message.Color = GameColor.DamageMelee; break; //Yellow
                case DamageType.Ranged: message.Color = GameColor.DamageRanged; break; //Purple
                default: message.Color = GameColor.None; break;
            }

            //if (damageHistory.Type != ChatType.None) { damageHistory.StageTwo(); damageHistoryStageTwo.Add(damageHistory); damageHistory = message; }
            //else { damageHistory = message; }

            damageHistory = message;
        }

        public bool CanMove(MotionDirection direction)
        {
            if (isDead) return false;
            if (currentMap == null) return false;
            if (isParalyzed || isHeld) return false;

            int x = this.x;
            int y = this.y;

            int desX = this.destinationX;
            int desY = this.destinationY;

            switch (direction)
            {
                case MotionDirection.North: y--; break;
                case MotionDirection.NorthEast: y--; x++; break;
                case MotionDirection.East: x++; break;
                case MotionDirection.SouthEast: x++; y++; break;
                case MotionDirection.South: y++; break;
                case MotionDirection.SouthWest: y++; x--; break;
                case MotionDirection.West: x--; break;
                case MotionDirection.NorthWest: x--; y--; break;
            }

            if (x < 0 || x > currentMap.Width) return false;
            if (y < 0 || y > currentMap.Height) return false;

            if (currentMap[y][x].IsMoveAllowed) return true;
            return false;
        }

        public void Run(MotionDirection direction)
        {
            if (isDead) return;
            if (isParalyzed || isHeld) return;

            prevX = x; prevY = y;

            switch (direction)
            {
                case MotionDirection.North: y--; break;
                case MotionDirection.NorthEast: y--; x++; break;
                case MotionDirection.East: x++; break;
                case MotionDirection.SouthEast: x++; y++; break;
                case MotionDirection.South: y++; break;
                case MotionDirection.SouthWest: y++; x--; break;
                case MotionDirection.West: x--; break;
                case MotionDirection.NorthWest: x--; y--; break;
                default: return;
            }

            if (Location.IsMoveAllowed)
            {
                this.direction = direction;
                currentMap[prevY][prevX].Owner = null;
                currentMap[y][x].Owner = this;

                motionType = MotionType.Run;
                animationFrame = 0;
                isMoving = true;
                moveReady = false;

                if (destinationX == x && destinationY == y) destinationX = destinationY = -1;
            }
            else 
            {
                x = prevX; y = prevY;
            }
        }

        public void SetLocation(Map map, int x, int y)
        {
            Map prevMap = currentMap;
            prevX = this.x; prevY = this.y;

            if (map[y][x].IsMoveAllowed)
            {
                this.x = x;
                this.y = y;
                this.currentMap = map;

                if (isDead)
                {
                    if (prevMap != null) prevMap[prevY][prevX].DeadOwner = null;
                    map[y][x].DeadOwner = this;
                }
                else
                {
                    if (prevMap != null) prevMap[prevY][prevX].Owner = null;
                    map[y][x].Owner = this;
                }
            }
            else
            {
                this.x = prevX; this.y = prevY;
                // TODO - this would be a problem if it fails when u first log in
            }
        }

        public void Walk(MotionDirection direction)
        {
            if (isDead) return;
            if (isParalyzed || isHeld) return;

            prevX = x; prevY = y;

            switch (direction)
            {
                case MotionDirection.North: y--; break;
                case MotionDirection.NorthEast: y--; x++; break;
                case MotionDirection.East: x++; break;
                case MotionDirection.SouthEast: x++; y++; break;
                case MotionDirection.South: y++; break;
                case MotionDirection.SouthWest: y++; x--; break;
                case MotionDirection.West: x--; break;
                case MotionDirection.NorthWest: x--; y--; break;
                default: return;
            }

            if (Location.IsMoveAllowed)
            {
                this.direction = direction;
                currentMap[prevY][prevX].Owner = null;
                currentMap[y][x].Owner = this;

                motionType = MotionType.Move;
                animationFrame = 0;
                isMoving = true;
                moveReady = false;

                if (destinationX == x && destinationY == y) destinationX = destinationY = -1;
            }
            else
            {
                x = prevX; y = prevY;
            }
        }

        public void VitalChanged(int amount, VitalType vitalType)
        {
            if (amount == 0) return;
            ChatMessage message = new ChatMessage(DateTime.Now);

            if (amount > 0) { message.Message = string.Format("+{0}", amount); }
            else { message.Message = string.Format("{0}", amount); }

            message.Font = FontType.DialogsSmallerSize7;
            //message.FontType = (amount >= Globals.LargeDamage) ? FontType.DamageLarge : (amount >= Globals.MediumDamage) ? FontType.DamageMedium : FontType.DamageSmall;

            switch (vitalType)
            {
                case VitalType.Health: message.Color = GameColor.HP; break;
                case VitalType.Hunger: message.Color = GameColor.Hunger; break;
                case VitalType.Mana: message.Color = GameColor.Mana; break;
                case VitalType.Stamina: message.Color = GameColor.SP; break;
                default: message.Color = GameColor.None; break;
            }

            //vitalHistory.Add(message);
            vitalHistory = message;
        }

        public void TakeDamage(int damage, DamageType damageType, int hitCount, bool stun = true)
        {
            if (isDead) return;       

            if (stun)
            {
                motionType = MotionType.TakeDamage;
                animationFrame = 0;
            }

            isDamaged = true;
            ChatMessage message = new ChatMessage(DateTime.Now);
            //if (hitCount > 1) { message.Message = (damage > 0 && damage < Globals.CriticalDamage) ? string.Format("-{0} {1}x", damage, hitCount) : "Critical"; }
            //else { message.Message = (damage > 0 && damage < Globals.CriticalDamage) ? string.Format("-{0}", damage) : "Critical"; }
            message.Message = (damage > 0 && damage < Globals.CriticalDamage) ? string.Format("-{0}", damage) : "Critical"; 

            message.Font = (damage >= Globals.LargeDamage) ? FontType.DamageLargeSize14Bold : (damage >= Globals.MediumDamage) ? FontType.DamageMediumSize13 : FontType.DamageSmallSize11;

            switch (damageType)
            {
                case DamageType.Environment: message.Color = GameColor.DamageEnvironmental; break;
                case DamageType.Poison: message.Color = GameColor.DamagePoison; break;
                case DamageType.Spirit: message.Color = GameColor.DamageSpirit; break;
                case DamageType.Magic: message.Color = GameColor.DamageMagic; break;
                case DamageType.Melee: message.Color = GameColor.DamageMelee; break; //Yellow
                case DamageType.Ranged: message.Color = GameColor.DamageRanged; break;
                default: message.Color = GameColor.None; break;
            }

            //if (damageHistory.Type != ChatType.None) { damageHistory.StageTwo(); damageHistoryStageTwo.Add(damageHistory); damageHistory = message; }
            //else { damageHistory = message; }

            damageHistory = message;

            if (type >= 0 && type <= 6)
                Cache.DefaultState.AddEffect(new GameEffect(DrawEffectType.Hit, new Location(x - (Cache.MapView.PivotX + Globals.OffScreenCells), y - (Cache.MapView.PivotY + Globals.OffScreenCells)), null));
        }

        public void Fly(int damage, int hitCount, MotionDirection direction, DamageType damageType)
        {
            if (isDead) return;
            if (IsParalyzed || IsHeld) return;

            isDamaged = true;

            prevX = x; prevY = y;

            switch (direction)
            {
                case MotionDirection.North: y--; break;
                case MotionDirection.NorthEast: y--; x++; break;
                case MotionDirection.East: x++; break;
                case MotionDirection.SouthEast: x++; y++; break;
                case MotionDirection.South: y++; break;
                case MotionDirection.SouthWest: y++; x--; break;
                case MotionDirection.West: x--; break;
                case MotionDirection.NorthWest: x--; y--; break;
                default: return;
            }

            currentMap[prevY][prevX].Owner = null;
            currentMap[y][x].Owner = this;

            this.direction = direction;
            isMoving = true;
            moveReady = false;
            motionType = MotionType.Fly;
            animationFrame = tempFrame = 0;

            if (damage > 0)
            {
                ChatMessage message = new ChatMessage(DateTime.Now);

                //if (hitCount > 1) { message.Message = (damage < Globals.CriticalDamage) ? string.Format("-{0} {1}x", damage, hitCount) : "Critical"; }
                //else { message.Message = (damage < Globals.CriticalDamage) ? string.Format("-{0}", damage) : "Critical"; }
                message.Message = (damage < Globals.CriticalDamage) ? string.Format("-{0}", damage) : "Critical";
                message.Font = (damage >= Globals.LargeDamage) ? FontType.DamageLargeSize14Bold : (damage >= Globals.MediumDamage) ? FontType.DamageMediumSize13 : FontType.DamageSmallSize11;

                switch (damageType)
                {
                    case DamageType.Environment: message.Color = GameColor.DamageEnvironmental; break;
                    case DamageType.Poison: message.Color = GameColor.DamagePoison; break;
                    case DamageType.Spirit: message.Color = GameColor.DamageSpirit; break;
                    case DamageType.Magic: message.Color = GameColor.DamageMagic; break;
                    case DamageType.Melee: message.Color = GameColor.DamageMelee; break; //Yellow
                    case DamageType.Ranged: message.Color = GameColor.DamageRanged; break; //Purple
                    default: message.Color = GameColor.Ancient; break;
                }

                //if (damageHistory.Type != ChatType.None) { damageHistory.StageTwo(); damageHistoryStageTwo.Add(damageHistory); damageHistory = message; }
                //else { damageHistory = message; }

                damageHistory = message;
            }

            Flying = false;
        }


        public void Update()
        {
            if (type == 0 || direction == MotionDirection.None) return;

            inventory.Update();//TODO FIGURE OUT IF THIS Should this go here?

            int frameSpeed = Cache.AnimationSpeeds[type, (int)motionType]; // speed of current frame in milliseconds
            int frameCount = (type >= 0 && type <= 6) ? (Cache.HumanAnimations.ContainsKey(Animation) ? Cache.HumanAnimations[Animation].Frames.Count : 1) : (Cache.MonsterAnimations.ContainsKey(Animation) ? Cache.MonsterAnimations[Animation].Frames.Count : 1); // number of frames in current animation
            int step = Globals.CellWidth / frameCount; // number of pixels to step per frame

            WeaponType weaponType = Utility.GetWeaponType(appearance2);
            if (frameSpeed <= 0) frameSpeed = (motionType == MotionType.Move || motionType == MotionType.Run || motionType == MotionType.DeadFadeOut) ? 50 : 100;
            // frame offsets
            switch (motionType)
            {
                case MotionType.Magic:
                    if (skills[(int)SkillType.Magic] >= 100) frameSpeed -= 13; 
                    if (IsHaste) frameSpeed -= (frameSpeed / 5); // +20% faster
                    break;
                case MotionType.Attack:
                case MotionType.AttackStationary:
                case MotionType.BowAttack:
                case MotionType.Dash:
                    if (weaponType != WeaponType.Hand)
                        frameSpeed += (status & 0x000F) * 12; // swing speed
                    if (IsHaste) frameSpeed -= (frameSpeed / 5); // +20% faster
                    break;
            }
            if (IsFrozen) frameSpeed += (frameSpeed / 4); // +25% slower
            frameSpeed = (int)MathHelper.Clamp(frameSpeed, 10, 300);

         
            // dash/fly shadow
            if (motionType == MotionType.Dash)
            {
                if (tempFrame == 0) offsetPoints = new List<int[]>();
                else if (tempFrame >= 1 && tempFrame <= 8) offsetPoints.Add(new int[] { offsetX, offsetY });
                else if (tempFrame >= 13) offsetPoints.RemoveAt(0);
            }
            else if (motionType == MotionType.Fly)
            {
                if (animationFrame == 0) offsetPoints = new List<int[]>();
                else if (animationFrame >= 4) offsetPoints.RemoveAt(0);
                else if (animationFrame >= 1) offsetPoints.Add(new int[] { offsetX, offsetY });
            }

            // object is moving, animate between cell. note: humans have 8 walk/run frames, mobs have only 4
            offsetX = 0; offsetY = 0;
            switch (motionType)
            {
                case MotionType.Move:
                case MotionType.Run:
                    switch (direction)
                    {
                        case MotionDirection.North: offsetY -= (animationFrame - frameCount) * step; break;
                        case MotionDirection.NorthEast: offsetX = (animationFrame - frameCount) * step; offsetY -= (animationFrame - frameCount) * step; break;
                        case MotionDirection.East: offsetX = (animationFrame - frameCount) * step; break;
                        case MotionDirection.SouthEast: offsetX = (animationFrame - frameCount) * step; offsetY = (animationFrame - frameCount) * step; break;
                        case MotionDirection.South: offsetY = (animationFrame - frameCount) * step; break;
                        case MotionDirection.SouthWest: offsetX -= (animationFrame - frameCount) * step; offsetY = (animationFrame - frameCount) * step; break;
                        case MotionDirection.West: offsetX -= (animationFrame - frameCount) * step; break;
                        case MotionDirection.NorthWest: offsetX -= (animationFrame - frameCount) * step; offsetY -= (animationFrame - frameCount) * step; break;
                    }
                    break;
                case MotionType.Fly:
                    switch (direction)
                    {
                        case MotionDirection.North: offsetY -= (animationFrame - 4) * 8; break;
                        case MotionDirection.NorthEast: offsetX = (animationFrame - 4) * 8; offsetY -= (animationFrame - 4) * 8; break;
                        case MotionDirection.East: offsetX = (animationFrame - 4) * 8; break;
                        case MotionDirection.SouthEast: offsetX = (animationFrame - 4) * 8; offsetY = (animationFrame - 4) * 8; break;
                        case MotionDirection.South: offsetY = (animationFrame - 4) * 8; break;
                        case MotionDirection.SouthWest: offsetX -= (animationFrame - 4) * 8; offsetY = (animationFrame - 4) * 8; break;
                        case MotionDirection.West: offsetX -= (animationFrame - 4) * 8; break;
                        case MotionDirection.NorthWest: offsetX -= (animationFrame - 4) * 8; offsetY -= (animationFrame - 4) * 8; break;
                    }
                    break;
                case MotionType.Dash:
                    switch (direction)
                    {
                        case MotionDirection.North: offsetY -= (Math.Min(tempFrame, 8) - 8) * 4; break;
                        case MotionDirection.NorthEast: offsetX = (Math.Min(tempFrame, 8) - 8) * 4; offsetY -= (Math.Min(tempFrame, 8) - 8) * 4; break;
                        case MotionDirection.East: offsetX = (Math.Min(tempFrame, 8) - 8) * 4; break;
                        case MotionDirection.SouthEast: offsetX = (Math.Min(tempFrame, 8) - 8) * 4; offsetY = (Math.Min(tempFrame, 8) - 8) * 4; break;
                        case MotionDirection.South: offsetY = (Math.Min(tempFrame, 8) - 8) * 4; break;
                        case MotionDirection.SouthWest: offsetX -= (Math.Min(tempFrame, 8) - 8) * 4; offsetY = (Math.Min(tempFrame, 8) - 8) * 4; break;
                        case MotionDirection.West: offsetX -= (Math.Min(tempFrame, 8) - 8) * 4; break;
                        case MotionDirection.NorthWest: offsetX -= (Math.Min(tempFrame, 8) - 8) * 4; offsetY -= (Math.Min(tempFrame, 8) - 8) * 4; break;
                    }
                    break;
            }

            switch (type)
            {
                case 1:
                case 2:
                case 3:
                case 4:
                case 5:
                case 6:
                    if (SpriteHelper.SpriteExists(Cache.HumanAnimations, Animation, AnimationFrame))
                        if ((DateTime.Now - animationTime).Milliseconds > frameSpeed)
                        {
                            // internal frame counter for condition sprites
                            effectFrame = (effectFrame > 100 ? 0 : effectFrame + 1);

                            switch (motionType)
                            {
                                case MotionType.Dead:
                                    animationFrame = frameCount - 1;
                                    break;
                                case MotionType.DeadFadeOut:
                                    //animationFrame = frameCount - 1;

                                    if (fadeValue <= 0) Remove();
                                    else fadeValue -= 5;
                                    break;
                                case MotionType.Dash:
                                    if (tempFrame == 2) AudioHelper.PlaySound("C4");
                                    if (tempFrame == 4) AudioHelper.PlaySound("C11");
                                    if (tempFrame == 5)
                                    {
                                        switch (weaponType)
                                        {
                                            case WeaponType.Hand:
                                            case WeaponType.ShortSword: AudioHelper.PlaySound("C1"); break;
                                            case WeaponType.Fencing:
                                            case WeaponType.StormBlade:
                                            case WeaponType.LongSword: AudioHelper.PlaySound("C2"); break;
                                            case WeaponType.Hammer:
                                            case WeaponType.Wand:
                                            case WeaponType.BattleStaff:
                                            case WeaponType.Axe: AudioHelper.PlaySound("C18"); break;
                                            case WeaponType.ShortBow:
                                            case WeaponType.LongBow: AudioHelper.PlaySound("C3"); break;
                                        }
                                    }

                                    if (tempFrame > 16)
                                    {
                                        if (isMoving) moveReady = true;
                                        animationFrame = 0;
                                        tempFrame = 0;
                                        motionType = MotionType.Idle;
                                    }
                                    else if (tempFrame >= 4 && tempFrame <= 13) animationFrame = 4;
                                    else if (tempFrame == 14) animationFrame = 3;
                                    else if (tempFrame == 15) animationFrame = 2;
                                    else if (tempFrame == 16) animationFrame = 1;
                                    else animationFrame++;

                                    tempFrame++;
                                    break;
                                default:
                                    if (animationFrame + 1 < frameCount)
                                    {
                                        // twinkle twinkle little stars
                                        if (StarTwinkle && Cache.GameSettings.DetailMode == GraphicsDetail.High && 
                                            motionType != MotionType.Dead && (animationFrame == 1 || animationFrame == 5) && Cache.MapView != null)
                                        {
                                            Cache.DefaultState.AddEffect(new GameEffect(DrawEffectType.Twinkle,
                                                             new Location(x - (Cache.MapView.PivotX + Globals.OffScreenCells), y - (Cache.MapView.PivotY + Globals.OffScreenCells)), null));
                                        }

                                        if (AngelTinkle && Cache.GameSettings.DetailMode == GraphicsDetail.High &&
                                            motionType != MotionType.Dead && (animationFrame == 1 || animationFrame == 5) && Cache.MapView != null)
                                        {
                                            Cache.DefaultState.AddEffect(new GameEffect(DrawEffectType.AngelTwinkle,
                                                             new Location(x - (Cache.MapView.PivotX + Globals.OffScreenCells), y - (Cache.MapView.PivotY + Globals.OffScreenCells)), null));
                                        }

                                        switch (motionType)
                                        {
                                            case MotionType.Move:
                                                if (animationFrame == 1 || animationFrame == 5) AudioHelper.PlaySound("C8");
                                                break;
                                            case MotionType.Run:
                                                if (animationFrame == 1 || animationFrame == 5) AudioHelper.PlaySound("C10");
                                                break;
                                            case MotionType.BowAttack:
                                            case MotionType.Attack:
                                                if (animationFrame == 2)
                                                {
                                                    if (criticalAttack)
                                                        switch (Gender)
                                                        {
                                                            case GenderType.Male: AudioHelper.PlaySound("C23"); break;
                                                            case GenderType.Female: AudioHelper.PlaySound("C24"); break;
                                                        }

                                                    /*if (motionType == MotionType.BowAttack)
                                                        Cache.GameState.AddEffect(new GameEffect(DrawEffectType.Arrow,
                                                            new Location(x - (Cache.MapView.PivotX + Globals.OffScreenCells), y - (Cache.MapView.PivotY + Globals.OffScreenCells)),
                                                            new Location(attackDestinationX - (Cache.MapView.PivotX + Globals.OffScreenCells), attackDestinationY - (Cache.MapView.PivotY + Globals.OffScreenCells))));
                                                     */
                                                }
                                                if (animationFrame == 5)
                                                {
                                                    switch (weaponType)
                                                    {
                                                        case WeaponType.Hand:
                                                        case WeaponType.ShortSword: AudioHelper.PlaySound("C1"); break;
                                                        case WeaponType.Fencing:
                                                        case WeaponType.StormBlade:
                                                        case WeaponType.LongSword: AudioHelper.PlaySound("C2"); break;
                                                        case WeaponType.Hammer:
                                                        case WeaponType.Wand:
                                                        case WeaponType.BattleStaff:
                                                        case WeaponType.Axe: AudioHelper.PlaySound("C18"); break;
                                                        case WeaponType.ShortBow:
                                                        case WeaponType.LongBow: AudioHelper.PlaySound("C3"); break;
                                                    }
                                                    if (criticalAttack)
                                                        Cache.DefaultState.AddEffect(new GameEffect(DrawEffectType.Critical,
                                                            new Location(x - (Cache.MapView.PivotX + Globals.OffScreenCells), y - (Cache.MapView.PivotY + Globals.OffScreenCells)),
                                                            new Location(attackDestinationX - (Cache.MapView.PivotX + Globals.OffScreenCells), attackDestinationY - (Cache.MapView.PivotY + Globals.OffScreenCells))));
                                                }
                                                break;
                                            case MotionType.Magic:
                                                if (animationFrame == 1) AudioHelper.PlaySound("C16");
                                                break;
                                            case MotionType.TakeDamage:
                                                if (animationFrame == 2)
                                                    switch (Gender)
                                                    {
                                                        case GenderType.Male: AudioHelper.PlaySound("C12"); break;
                                                        case GenderType.Female: AudioHelper.PlaySound("C13"); break;
                                                    }
                                                break;
                                            case MotionType.Die:
                                                if (animationFrame == 2)
                                                    switch (Gender)
                                                    {
                                                        case GenderType.Male: AudioHelper.PlaySound("C14"); break;
                                                        case GenderType.Female: AudioHelper.PlaySound("C15"); break;
                                                    }
                                                break;
                                        }

                                        animationFrame++;
                                        animationTime = DateTime.Now;
                                    }
                                    else
                                    {
                                        switch (motionType)
                                        {
                                            case MotionType.Attack:
                                            case MotionType.BowAttack:
                                            case MotionType.Bow:
                                            case MotionType.PickUp:                                          
                                            case MotionType.Fly:
                                                moveReady = true;
                                                motionType = MotionType.Idle;
                                                animationFrame = 0;
                                                break;
                                            case MotionType.AttackStationary:
                                            case MotionType.TakeDamage:
                                                motionType = MotionType.Idle;
                                                animationFrame = 0;
                                                break;
                                            case MotionType.Idle:
                                                if (!moveReady) moveReady = true;
                                                motionType = MotionType.Idle;
                                                animationFrame = 0;
                                                break;
                                            case MotionType.Die:
                                                motionType = MotionType.Dead;
                                                animationFrame = frameCount - 1;
                                                break;
                                            case MotionType.Move:
                                            case MotionType.Run:
                                                if (isMoving) moveReady = true;
                                                animationFrame = 0;
                                                break;
                                            case MotionType.Magic:
                                                if (isCasting) spellReady = true;
                                                motionType = MotionType.Idle;
                                                animationFrame = 0;
                                                break;
                                        }
                                    }
                                    break;
                            }
                        }
                    break;
                default: break; // monster animations not implemented yet (polymorph?)
            }

            /* DAMAGE HISTORY */
            if (damageHistory.Color != GameColor.None)
            {
                //Remove
                if ((DateTime.Now - damageHistory.MessageTime).Seconds >= 3) { damageHistory.Clear(); }

                if (((int)(DateTime.Now - damageHistory.MessageTime).TotalMilliseconds / 20) > damageHistory.AnimationTime)
                {
                    damageHistory.AnimationTime += 1;

                    //Handle Scale
                    if (damageHistory.AnimationTime > 5 && damageHistory.AnimationTime < 15) { damageHistory.Scale -= 0.05f; }
                    else if (damageHistory.AnimationTime > 15 && damageHistory.Scale != 1.0f) { damageHistory.Scale = 1.0f; }

                    //Transparency
                    if (damageHistory.AnimationTime > 140) { damageHistory.Transparency -= 0.1f; }
                }
            }

            /* Vital HISTORY */
            if (vitalHistory.Color != GameColor.None)
            {
                //Remove
                if ((DateTime.Now - vitalHistory.MessageTime).Seconds >= 3) { vitalHistory.Clear(); }

                if (((int)(DateTime.Now - vitalHistory.MessageTime).TotalMilliseconds / 20) > vitalHistory.AnimationTime)
                {
                    vitalHistory.AnimationTime += 1;

                    //Handle Scale
                    if (vitalHistory.AnimationTime > 5 && vitalHistory.AnimationTime < 15) { vitalHistory.Scale -= 0.05f; }
                    else if (vitalHistory.AnimationTime > 15 && vitalHistory.Scale != 1.0f) { vitalHistory.Scale = 1.0f; }

                    //Transparency
                    if (vitalHistory.AnimationTime > 140) { vitalHistory.Transparency -= 0.1f; }
                }
            }

            /* VITAL HISTORY SHOW MULTIPLE*/
            //if (vitalHistory.Count > 0)
            //{
            //    //Remove expired
            //    for (int i = 0; i < vitalHistory.Count; i++)
            //    {
            //        if ((DateTime.Now - vitalHistory[i].MessageTime).Seconds >= 3) { vitalHistory.RemoveAt(i); }
            //    }

            //    //trim if needed
            //    if (vitalHistory.Count > 5)
            //    {
            //        int removeAmount = vitalHistory.Count - 5;
            //        vitalHistory.RemoveRange(0, removeAmount);
            //    }

            //    for (int i = 0; i < vitalHistory.Count; i++)
            //    {
            //        if (((int)(DateTime.Now - vitalHistory[i].MessageTime).TotalMilliseconds / 20) > vitalHistory[i].AnimationTime)
            //        {
            //            vitalHistory[i].AnimationTime += 1;
            //            if (vitalHistory[i].AnimationTime > 100) { vitalHistory[i].Transparency -= 0.02f; }
            //        }
            //    }
            //}

            /*SPELL HISTORY*/
            if (spellHistory.Color != GameColor.None)
            {
                if (((int)(DateTime.Now - spellHistory.MessageTime).TotalMilliseconds / 20) > spellHistory.AnimationTime)
                {
                    spellHistory.AnimationTime += 1;
                    if (spellHistory.AnimationTime < 10) { spellHistory.Y += 1; }
                    if (spellHistory.AnimationTime > 100) { spellHistory.Transparency -= 0.02f; }
                }

                //Remove
                if (!isCasting) { spellHistory.Clear(); }
                else if ((DateTime.Now - spellHistory.MessageTime).Seconds >= 3) { spellHistory.Clear(); }
            }

            /*CHAT HISTORY*/
            if (chatHistory.Color != GameColor.None)
            {
                if (((int)(DateTime.Now - chatHistory.MessageTime).TotalMilliseconds / 20) > chatHistory.AnimationTime)
                {
                    chatHistory.AnimationTime += 1;
                    if (chatHistory.AnimationTime < 10) { chatHistory.Y += 1; }
                    if (chatHistory.AnimationTime > 200) { chatHistory.Transparency -= 0.02f; }
                }

                //Remove
                if ((DateTime.Now - chatHistory.MessageTime).Seconds >= 5) { chatHistory.Clear(); }
            }
              
            if (glowPosition)
            {
                if (glowPoint >= 255)
                {
                    glowPoint = 255;
                    glowPosition = false;
                } else glowPoint++;
            }
            else
            {
                if (glowPoint <= 150)
                {
                    glowPoint = 150;
                    glowPosition = true;
                } else glowPoint--;
            }
        }
        public bool Draw(SpriteBatch spriteBatch, GameDisplay display, int x, int y, int playerOffsetX, int playerOffsetY)
        {
            if (direction == MotionDirection.None) return false;

            bool selected = false;
            bool showAura = (!isDead);

            float fade = fadeValue / 255.0f;
            Color colour = Cache.Colors[GameColor.Normal];
            if (!isDead && IsInvisible)
            {
                fade = 0.4f;
                colour = Cache.Colors[GameColor.Normal];
            }
            if (isDead && isCorpseExploited) colour = Cache.Colors[GameColor.Exploited];

            // only draw weapons and armour in certain animations
            bool drawWeapons = (!isDead && motionType != MotionType.PickUp && motionType != MotionType.Bow && motionType != MotionType.Magic);

            if (Type <= 6) //Player
            {

                GameColor teamColor = GameColor.None; //No color
                if (Cache.GameSettings.TeamColorOn)
                {
                    if (Cache.GameSettings.PartyColorOn && HasParty) { teamColor = GameColor.Party; }
                    else if (Cache.GameSettings.GuildColorOn && guildName != "NONE") { teamColor = GameColor.Guild; }
                    else if (Cache.GameSettings.TownColorOn)
                    {
                        switch (side)
                        {
                            case OwnerSide.Aresden: teamColor = GameColor.Aresden; break;
                            case OwnerSide.Elvine: teamColor = GameColor.Elvine; break;
                        }
                    }
                }

                //DrawItemPopup Shadow
                if (!isDead && Cache.GameSettings.DetailMode != GraphicsDetail.Low) SpriteHelper.DrawBodyShadow(spriteBatch, Type, Animation, AnimationFrame,
                            x, y, playerOffsetX, playerOffsetY, offsetX, offsetY, Cache.Colors[GameColor.Shadow], fade, out selected);

                if (showAura)
                {
                    // Defence shield
                    if ((status & 0x02000000) != 0)
                        SpriteHelper.DrawEffect(spriteBatch, 80, effectFrame % 17, x + 75, y + 107, -offsetX - playerOffsetX, -offsetY - playerOffsetY, Color.White * 0.5f);

                    // Protection From Magic
                    if ((status & 0x04000000) != 0)
                        SpriteHelper.DrawEffect(spriteBatch, 79, effectFrame % 15, x + 101, y + 135, -offsetX - playerOffsetX, -offsetY - playerOffsetY, Color.White * 0.7f);

                    // Protection From Arrow
                    if ((status & 0x08000000) != 0)
                        SpriteHelper.DrawEffect(spriteBatch, 72, effectFrame % 30, x, y + 35, -offsetX - playerOffsetX, -offsetY - playerOffsetY, Color.White * 0.7f);

                    // Illusion
                    if ((status & 0x01000000) != 0)
                        SpriteHelper.DrawEffect(spriteBatch, 73, effectFrame % 24, x + 125, y + 130, -offsetX - playerOffsetX, -offsetY - playerOffsetY, Color.White * 0.7f);

                    // Illusion movement
                    if ((status & 0x00200000) != 0)
                        SpriteHelper.DrawEffect(spriteBatch, 151, effectFrame % 24, x + 90, y + 90, -offsetX - playerOffsetX, -offsetY - playerOffsetY, Color.White * 0.7f);

                    // HP Slate
                    if ((status & 0x00400000) != 0)
                        SpriteHelper.DrawEffect(spriteBatch, 149, effectFrame % 15, x + 90, y + 120, -offsetX - playerOffsetX, -offsetY - playerOffsetY, Color.White * 0.7f);

                    // Mana Slate
                    if ((status & 0x00800000) != 0)
                        SpriteHelper.DrawEffect(spriteBatch, 150, effectFrame % 15, x + 1, y + 26, -offsetX - playerOffsetX, -offsetY - playerOffsetY, Color.White * 0.7f);

                    // XP Slate
                    if ((status & 0x00010000) != 0)
                        SpriteHelper.DrawEffect(spriteBatch, 148, effectFrame % 23, x, y + 32, -offsetX - playerOffsetX, -offsetY - playerOffsetY, Color.White * 0.7f);

                    // Hero Flag (Heldenian)
                    if ((status & 0x00020000) != 0)
                        SpriteHelper.DrawEffect(spriteBatch, 87, effectFrame % 29, x + 53, y + 54, -offsetX - playerOffsetX, -offsetY - playerOffsetY, Color.White * 0.7f);

                    if (IsAbilityActivated)
                        switch ((appearance4 & 0x00F0) >> 4)
                        {
                            case 1: SpriteHelper.DrawEffect(spriteBatch, 26, effectFrame % 14, x, y, -offsetX - playerOffsetX, -offsetY - playerOffsetY, Color.White * 0.7f); break;
                            case 2: SpriteHelper.DrawEffect(spriteBatch, 27, effectFrame % 12, x, y, -offsetX - playerOffsetX, -offsetY - playerOffsetY, Color.White * 0.7f); break;
                        }
                }

                SpriteHelper.DrawBody(spriteBatch, Type, Animation, AnimationFrame,
                                        x, y, playerOffsetX, playerOffsetY, offsetX, offsetY, colour, fade, out selected);

                switch (Type)
                {
                    case 1:
                    case 2:
                    case 3: // men
                        {
                            int undiesIndex = (int)SpriteId.MaleUndies + ((Appearance1 & 0x000F) * 15) + AnimationMotionOffset;
                            int undiesFrame = (((int)Direction - 1) * AnimationFrameCount) + AnimationFrame;
                            SpriteHelper.DrawEquipment(spriteBatch, undiesIndex, undiesFrame, x, y, playerOffsetX, playerOffsetY, offsetX, offsetY, colour, fade);

                            /* DRAW CAPE BEHIND BODY */
                            if (!AnimationDrawCapeFront)
                            {
                                if (Cache.GameSettings.TeamColorOn && teamColor != GameColor.None)
                                {
                                    int capeIndex = (int)SpriteId.MaleCape + 45 + AnimationMotionOffset;
                                    int capeFrame = (((int)Direction - 1) * AnimationFrameCount) + AnimationFrame;
                                    SpriteHelper.DrawEquipment(spriteBatch, capeIndex, capeFrame, x, y, playerOffsetX, playerOffsetY, offsetX, offsetY, Cache.Colors[teamColor], fade);
                                }
                                else if (Cape != null)
                                {
                                    int capeIndex = ((int)SpriteId.MaleCape + (Cape.Appearance * 15)) + AnimationMotionOffset;
                                    int capeFrame = (((int)Direction - 1) * AnimationFrameCount) + AnimationFrame;
                                    SpriteHelper.DrawEquipment(spriteBatch, capeIndex, capeFrame, x, y, playerOffsetX, playerOffsetY, offsetX, offsetY, (Cape.ColorType == GameColor.Custom) ? SpriteHelper.ColourFromArgb(Cape.Colour) : Cache.Colors[Cape.ColorType], fade, Cape.SpriteFileMale);
                                }
                            }

                            /* DRAW WEAPONS AND SHIELDS BEHIND BODY */
                            if (drawWeapons)
                            {
                                /* HARD CODED EXCEPTION FOR NORTH EAST */
                                if (direction == MotionDirection.NorthEast)
                                {
                                    if (!AnimationDrawShieldFront && Shield != null) // shield
                                    {
                                        int shieldIndex = (int)SpriteId.MaleShield + Shield.Appearance * 8 + AnimationMotionOffsetWeapon;
                                        int shieldFrame = (((int)Direction - 1) * AnimationFrameCount) + AnimationFrame;
                                        /*if (Cache.GameSettings.DetailMode == GraphicsDetail.High)
                                        {
                                            spriteBatch.End();
                                            spriteBatch.Begin(SpriteSortMode.Deferred, null, null, null, null, Cache.PixelShaders[(int)PixelShader.Weapon]);
                                            SpriteHelper.DrawEquipment(spriteBatch, shieldIndex, shieldFrame, x, y, playerOffsetX, playerOffsetY, offsetX, offsetY, (Shield != null ? Shield.Colour : (int)GameColour.Normal), fade, Shield.SpriteFileMale);
                                            spriteBatch.End();
                                            spriteBatch.Begin();
                                        }
                                        else
                                        {*/
                                            SpriteHelper.DrawEquipment(spriteBatch, shieldIndex, shieldFrame, x, y, playerOffsetX, playerOffsetY, offsetX, offsetY, (Shield.ColorType == GameColor.Custom) ? SpriteHelper.ColourFromArgb(Shield.Colour) : Cache.Colors[Shield.ColorType], fade, Shield.SpriteFileMale);
                                        //}
                                    }
                                    if (!AnimationDrawWeaponFront && Weapon != null) // weapon
                                    {
                                        int weaponIndex = ((int)SpriteId.MaleWeapon + Weapon.Appearance * 64 + 8 * AnimationMotionOffsetWeapon) + ((int)Direction - 1);
                                        int weaponFrame = AnimationFrame;
                                        /*if (Cache.GameSettings.DetailMode == GraphicsDetail.High)
                                        {
                                            spriteBatch.End();
                                            spriteBatch.Begin(SpriteSortMode.Deferred, null, null, null, null, Cache.PixelShaders[(int)PixelShader.Weapon]);
                                            SpriteHelper.DrawEquipment(spriteBatch, weaponIndex, weaponFrame, x, y, playerOffsetX, playerOffsetY, offsetX, offsetY, (Weapon != null ? Weapon.Colour : (int)GameColour.Normal), fade, Weapon.SpriteFileMale);
                                            spriteBatch.End();
                                            spriteBatch.Begin();
                                        }
                                        else
                                        {*/
                                            SpriteHelper.DrawEquipment(spriteBatch, weaponIndex, weaponFrame, x, y, playerOffsetX, playerOffsetY, offsetX, offsetY, (Weapon.ColorType == GameColor.Custom) ? SpriteHelper.ColourFromArgb(Weapon.Colour) : Cache.Colors[Weapon.ColorType], fade, Weapon.SpriteFileMale);
                                        //}
                                    }
                                }
                                else
                                {
                                    if (!AnimationDrawWeaponFront && Weapon != null) // weapon
                                    {
                                        int weaponIndex = ((int)SpriteId.MaleWeapon + Weapon.Appearance * 64 + 8 * AnimationMotionOffsetWeapon) + ((int)Direction - 1);
                                        int weaponFrame = AnimationFrame;

                                        /*if (Cache.GameSettings.DetailMode == GraphicsDetail.High)
                                        {
                                            spriteBatch.End();
                                            spriteBatch.Begin(SpriteSortMode.Deferred, null, null, null, null, Cache.PixelShaders[(int)PixelShader.Weapon]);
                                            SpriteHelper.DrawEquipment(spriteBatch, weaponIndex, weaponFrame, x, y, playerOffsetX, playerOffsetY, offsetX, offsetY, (Weapon != null ? Weapon.Colour : (int)GameColour.Normal), fade, Weapon.SpriteFileMale);
                                            spriteBatch.End();
                                            spriteBatch.Begin();
                                        }
                                        else
                                        {*/
                                            SpriteHelper.DrawEquipment(spriteBatch, weaponIndex, weaponFrame, x, y, playerOffsetX, playerOffsetY, offsetX, offsetY, (Weapon.ColorType == GameColor.Custom) ? SpriteHelper.ColourFromArgb(Weapon.Colour) : Cache.Colors[Weapon.ColorType], fade, Weapon.SpriteFileMale);
                                        //}
                                    }
                                    if (!AnimationDrawShieldFront && Shield != null) // shield
                                    {
                                        int shieldIndex = (int)SpriteId.MaleShield + Shield.Appearance * 8 + AnimationMotionOffsetWeapon;
                                        int shieldFrame = (((int)Direction - 1) * AnimationFrameCount) + AnimationFrame;
                                        /*if (Cache.GameSettings.DetailMode == GraphicsDetail.High)
                                        {
                                            spriteBatch.End();
                                            spriteBatch.Begin(SpriteSortMode.Deferred, null, null, null, null, Cache.PixelShaders[(int)PixelShader.Weapon]);
                                            SpriteHelper.DrawEquipment(spriteBatch, shieldIndex, shieldFrame, x, y, playerOffsetX, playerOffsetY, offsetX, offsetY, (Shield != null ? Shield.Colour : (int)GameColour.Normal), fade, Shield.SpriteFileMale);
                                            spriteBatch.End();
                                            spriteBatch.Begin();
                                        }
                                        else
                                        {*/
                                            SpriteHelper.DrawEquipment(spriteBatch, shieldIndex, shieldFrame, x, y, playerOffsetX, playerOffsetY, offsetX, offsetY, (Shield.ColorType == GameColor.Custom) ? SpriteHelper.ColourFromArgb(Shield.Colour) : Cache.Colors[Shield.ColorType], fade, Shield.SpriteFileMale);
                                        //}
                                    }
                                }
                            }

                            /* DRAW LEGS */
                            if (Leggings != null) // legs
                            {
                                int legsIndex = ((int)SpriteId.MaleLegs + Leggings.Appearance * 15) + AnimationMotionOffset;
                                int legsFrame = (((int)Direction - 1) * AnimationFrameCount) + AnimationFrame;
                                SpriteHelper.DrawEquipment(spriteBatch, legsIndex, legsFrame, x, y, playerOffsetX, playerOffsetY, offsetX, offsetY, (Leggings.ColorType == GameColor.Custom) ? SpriteHelper.ColourFromArgb(Leggings.Colour) : Cache.Colors[Leggings.ColorType], fade, Leggings.SpriteFileMale);
                            }

                            /* DRAW BOOTS */
                            if (Cache.GameSettings.TeamColorOn && teamColor != GameColor.None)
                            {
                                int bootsIndex = (int)SpriteId.MaleBoots + 30 + AnimationMotionOffset;
                                int bootsFrame = (((int)Direction - 1) * AnimationFrameCount) + AnimationFrame;
                                SpriteHelper.DrawEquipment(spriteBatch, bootsIndex, bootsFrame, x, y, playerOffsetX, playerOffsetY, offsetX, offsetY, Cache.Colors[teamColor], fade);
                            }
                            else if (Boots != null) // boots
                            {
                                int bootsIndex = ((int)SpriteId.MaleBoots + Boots.Appearance * 15) + AnimationMotionOffset;
                                int bootsFrame = (((int)Direction - 1) * AnimationFrameCount) + AnimationFrame;
                                SpriteHelper.DrawEquipment(spriteBatch, bootsIndex, bootsFrame, x, y, playerOffsetX, playerOffsetY, offsetX, offsetY, (Boots.ColorType == GameColor.Custom) ? SpriteHelper.ColourFromArgb(Boots.Colour) : Cache.Colors[Boots.ColorType], fade, Boots.SpriteFileMale);
                            }

                            /* DRAW ARMS */
                            if (Hauberk != null) // arms
                            {
                                int armsIndex = ((int)SpriteId.MaleArms + Hauberk.Appearance * 15) + AnimationMotionOffset;
                                int armsFrame = (((int)Direction - 1) * AnimationFrameCount) + AnimationFrame;
                                SpriteHelper.DrawEquipment(spriteBatch, armsIndex, armsFrame, x, y, playerOffsetX, playerOffsetY, offsetX, offsetY, (Hauberk.ColorType == GameColor.Custom) ? SpriteHelper.ColourFromArgb(Hauberk.Colour) : Cache.Colors[Hauberk.ColorType], fade, Hauberk.SpriteFileMale);
                            }

                            /* DRAW BODY */
                            if (BodyArmour != null) // body
                            {
                                int bodyIndex = ((int)SpriteId.MaleBody + BodyArmour.Appearance * 15) + AnimationMotionOffset;
                                int bodyFrame = (((int)Direction - 1) * AnimationFrameCount) + AnimationFrame;
                                SpriteHelper.DrawEquipment(spriteBatch, bodyIndex, bodyFrame, x, y, playerOffsetX, playerOffsetY, offsetX, offsetY, (BodyArmour.ColorType == GameColor.Custom) ? SpriteHelper.ColourFromArgb(BodyArmour.Colour) : Cache.Colors[BodyArmour.ColorType], fade, BodyArmour.SpriteFileMale);
                            }

                            /* DRAW HELM OR HAIR */
                            if (Helmet != null) // helm
                            {
                                int headIndex = ((int)SpriteId.MaleHead + Helmet.Appearance * 15) + AnimationMotionOffset;
                                int headFrame = (((int)Direction - 1) * AnimationFrameCount) + AnimationFrame;
                                SpriteHelper.DrawEquipment(spriteBatch, headIndex, headFrame, x, y, playerOffsetX, playerOffsetY, offsetX, offsetY, (Helmet.ColorType == GameColor.Custom) ? SpriteHelper.ColourFromArgb(Helmet.Colour) : Cache.Colors[Helmet.ColorType], fade, Helmet.SpriteFileMale);
                            }
                            else
                            {
                                int hairIndex = ((int)SpriteId.MaleHair + ((Appearance1 & 0x0F00) >> 8) * 15) + AnimationMotionOffset;
                                int hairFrame = (((int)Direction - 1) * AnimationFrameCount) + AnimationFrame;
                                SpriteHelper.DrawEquipment(spriteBatch, hairIndex, hairFrame, x, y, playerOffsetX, playerOffsetY, offsetX, offsetY, colour, fade);
                            }

                            /* DRAW WEAPONS AND SHIELDS IN FRONT OF BODY */
                            if (drawWeapons)
                            {
                                /* HARD CODED EXCEPTION FOR NORTH EAST */
                                if (direction == MotionDirection.NorthEast)
                                {
                                    if (AnimationDrawShieldFront && Shield != null) // shield
                                    {
                                        int shieldIndex = (int)SpriteId.MaleShield + Shield.Appearance * 8 + AnimationMotionOffsetWeapon;
                                        int shieldFrame = (((int)Direction - 1) * AnimationFrameCount) + AnimationFrame;
                                        /*if (Cache.GameSettings.DetailMode == GraphicsDetail.High)
                                        {
                                            spriteBatch.End();
                                            spriteBatch.Begin(SpriteSortMode.Deferred, null, null, null, null, Cache.PixelShaders[(int)PixelShader.Weapon]);
                                            SpriteHelper.DrawEquipment(spriteBatch, shieldIndex, shieldFrame, x, y, playerOffsetX, playerOffsetY, offsetX, offsetY, (Shield != null ? Shield.Colour : (int)GameColour.Normal), fade, Shield.SpriteFileMale);
                                            spriteBatch.End();
                                            spriteBatch.Begin();
                                        }
                                        else
                                        {*/
                                            SpriteHelper.DrawEquipment(spriteBatch, shieldIndex, shieldFrame, x, y, playerOffsetX, playerOffsetY, offsetX, offsetY, (Shield.ColorType == GameColor.Custom) ? SpriteHelper.ColourFromArgb(Shield.Colour) : Cache.Colors[Shield.ColorType], fade, Shield.SpriteFileMale);
                                        //}
                                    }
                                    if (AnimationDrawWeaponFront && Weapon != null) // weapon
                                    {
                                        int weaponIndex = ((int)SpriteId.MaleWeapon + Weapon.Appearance * 64 + 8 * AnimationMotionOffsetWeapon) + ((int)Direction - 1);
                                        int weaponFrame = AnimationFrame;
                                        /*if (Cache.GameSettings.DetailMode == GraphicsDetail.High)
                                        {
                                            spriteBatch.End();
                                            spriteBatch.Begin(SpriteSortMode.Deferred, null, null, null, null, Cache.PixelShaders[(int)PixelShader.Weapon]);
                                            SpriteHelper.DrawEquipment(spriteBatch, weaponIndex, weaponFrame, x, y, playerOffsetX, playerOffsetY, offsetX, offsetY, (Weapon != null ? Weapon.Colour : (int)GameColour.Normal), fade, Weapon.SpriteFileMale);
                                            spriteBatch.End();
                                            spriteBatch.Begin();
                                        }
                                        else
                                        {*/
                                            SpriteHelper.DrawEquipment(spriteBatch, weaponIndex, weaponFrame, x, y, playerOffsetX, playerOffsetY, offsetX, offsetY, (Weapon.ColorType == GameColor.Custom) ? SpriteHelper.ColourFromArgb(Weapon.Colour) : Cache.Colors[Weapon.ColorType], fade, Weapon.SpriteFileMale);
                                        //}
                                    }
                                }
                                else
                                {
                                    if (AnimationDrawWeaponFront && Weapon != null) // weapon
                                    {
                                        int weaponIndex = ((int)SpriteId.MaleWeapon + Weapon.Appearance * 64 + 8 * AnimationMotionOffsetWeapon) + ((int)Direction - 1);
                                        int weaponFrame = AnimationFrame;
                                        /*if (Cache.GameSettings.DetailMode == GraphicsDetail.High)
                                        {
                                            spriteBatch.End();
                                            spriteBatch.Begin(SpriteSortMode.Deferred, null, null, null, null, Cache.PixelShaders[(int)PixelShader.Weapon]);
                                            SpriteHelper.DrawEquipment(spriteBatch, weaponIndex, weaponFrame, x, y, playerOffsetX, playerOffsetY, offsetX, offsetY, (Weapon != null ? Weapon.Colour : (int)GameColour.Normal), fade, Weapon.SpriteFileMale);
                                            spriteBatch.End();
                                            spriteBatch.Begin();
                                        }
                                        else
                                        {*/
                                            SpriteHelper.DrawEquipment(spriteBatch, weaponIndex, weaponFrame, x, y, playerOffsetX, playerOffsetY, offsetX, offsetY, (Weapon.ColorType == GameColor.Custom) ? SpriteHelper.ColourFromArgb(Weapon.Colour) : Cache.Colors[Weapon.ColorType], fade, Weapon.SpriteFileMale);
                                        //}
                                    }
                                    if (AnimationDrawShieldFront && Shield != null) // shield
                                    {
                                        int shieldIndex = (int)SpriteId.MaleShield + Shield.Appearance * 8 + AnimationMotionOffsetWeapon;
                                        int shieldFrame = (((int)Direction - 1) * AnimationFrameCount) + AnimationFrame;
                                        /*if (Cache.GameSettings.DetailMode == GraphicsDetail.High)
                                        {
                                            spriteBatch.End();
                                            spriteBatch.Begin(SpriteSortMode.Deferred, null, null, null, null, Cache.PixelShaders[(int)PixelShader.Weapon]);
                                            SpriteHelper.DrawEquipment(spriteBatch, shieldIndex, shieldFrame, x, y, playerOffsetX, playerOffsetY, offsetX, offsetY, (Shield != null ? Shield.Colour : (int)GameColour.Normal), fade, Shield.SpriteFileMale);
                                            spriteBatch.End();
                                            spriteBatch.Begin();
                                        }
                                        else
                                        {*/
                                            SpriteHelper.DrawEquipment(spriteBatch, shieldIndex, shieldFrame, x, y, playerOffsetX, playerOffsetY, offsetX, offsetY, (Shield.ColorType == GameColor.Custom) ? SpriteHelper.ColourFromArgb(Shield.Colour) : Cache.Colors[Shield.ColorType], fade, Shield.SpriteFileMale);
                                        //}
                                    }
                                }
                            }

                            /* DRAW CAPE IN FRONT OF BODY */
                            if (AnimationDrawCapeFront)
                            {
                                if (Cache.GameSettings.TeamColorOn && teamColor != GameColor.None)
                                {
                                    int capeIndex = (int)SpriteId.MaleCape + 45 + AnimationMotionOffset;
                                    int capeFrame = (((int)Direction - 1) * AnimationFrameCount) + AnimationFrame;
                                    SpriteHelper.DrawEquipment(spriteBatch, capeIndex, capeFrame, x, y, playerOffsetX, playerOffsetY, offsetX, offsetY, Cache.Colors[teamColor], fade);

                                }
                                else if (Cape != null) // cape
                                {
                                    int capeIndex = ((int)SpriteId.MaleCape + Cape.Appearance * 15) + AnimationMotionOffset;
                                    int capeFrame = (((int)Direction - 1) * AnimationFrameCount) + AnimationFrame;
                                    SpriteHelper.DrawEquipment(spriteBatch, capeIndex, capeFrame, x, y, playerOffsetX, playerOffsetY, offsetX, offsetY, (Cape.ColorType == GameColor.Custom) ? SpriteHelper.ColourFromArgb(Cape.Colour) : Cache.Colors[Cape.ColorType], fade, Cape.SpriteFileMale);
                                }
                            }
                        }
                        break;
                    case 4:
                    case 5:
                    case 6: // woman
                        {
                            int undiesIndex = (int)SpriteId.FemaleUndies + ((Appearance1 & 0x000F) * 15) + AnimationMotionOffset;
                            int undiesFrame = (((int)Direction - 1) * AnimationFrameCount) + AnimationFrame;
                            SpriteHelper.DrawEquipment(spriteBatch, undiesIndex, undiesFrame, x, y, playerOffsetX, playerOffsetY, offsetX, offsetY, Cache.Colors[GameColor.Normal], fade);

                            /* DRAW CAPE BEHIND BODY */
                            if (!AnimationDrawCapeFront)
                            {
                                if (Cache.GameSettings.TeamColorOn && teamColor != GameColor.None)
                                {
                                    int capeIndex = (int)SpriteId.FemaleCape + 45 + AnimationMotionOffset;
                                    int capeFrame = (((int)Direction - 1) * AnimationFrameCount) + AnimationFrame;
                                    SpriteHelper.DrawEquipment(spriteBatch, capeIndex, capeFrame, x, y, playerOffsetX, playerOffsetY, offsetX, offsetY, Cache.Colors[teamColor], fade);
                                }
                                else if (Cape != null) // cape
                                {
                                    int capeIndex = ((int)SpriteId.FemaleCape + Cape.Appearance * 15) + AnimationMotionOffset;
                                    int capeFrame = (((int)Direction - 1) * AnimationFrameCount) + AnimationFrame;
                                    SpriteHelper.DrawEquipment(spriteBatch, capeIndex, capeFrame, x, y, playerOffsetX, playerOffsetY, offsetX, offsetY, (Cape.ColorType == GameColor.Custom) ? SpriteHelper.ColourFromArgb(Cape.Colour) : Cache.Colors[Cape.ColorType], fade, Cape.SpriteFileFemale);
                                }
                            }

                            /* DRAW WEAPONS AND SHIELDS BEHIND BODY */
                            if (drawWeapons)
                            {
                                /* HARD CODED EXCEPTION FOR NORTH EAST */
                                if (direction == MotionDirection.NorthEast)
                                {
                                    if (!AnimationDrawShieldFront && Shield != null) // shield
                                    {
                                        int shieldIndex = (int)SpriteId.FemaleShield + Shield.Appearance * 8 + AnimationMotionOffsetWeapon;
                                        int shieldFrame = (((int)Direction - 1) * AnimationFrameCount) + AnimationFrame;
                                        /*(if (Cache.GameSettings.DetailMode == GraphicsDetail.High)
                                        {
                                            spriteBatch.End();
                                            spriteBatch.Begin(SpriteSortMode.Deferred, null, null, null, null, Cache.PixelShaders[(int)PixelShader.Weapon]);
                                            SpriteHelper.DrawEquipment(spriteBatch, shieldIndex, shieldFrame, x, y, playerOffsetX, playerOffsetY, offsetX, offsetY, (Shield != null ? Shield.Colour : (int)GameColour.Normal), fade, Shield.SpriteFileFemale);
                                            spriteBatch.End();
                                            spriteBatch.Begin();
                                        }
                                        else
                                        {*/
                                            SpriteHelper.DrawEquipment(spriteBatch, shieldIndex, shieldFrame, x, y, playerOffsetX, playerOffsetY, offsetX, offsetY, (Shield.ColorType == GameColor.Custom) ? SpriteHelper.ColourFromArgb(Shield.Colour) : Cache.Colors[Shield.ColorType], fade, Shield.SpriteFileFemale);
                                        //}
                                    }
                                    if (!AnimationDrawWeaponFront && Weapon != null) // weapon
                                    {
                                        int weaponIndex = ((int)SpriteId.FemaleWeapon + Weapon.Appearance * 64 + 8 * AnimationMotionOffsetWeapon) + ((int)Direction - 1);
                                        int weaponFrame = AnimationFrame;
                                        /*if (Cache.GameSettings.DetailMode == GraphicsDetail.High)
                                        {
                                            spriteBatch.End();
                                            spriteBatch.Begin(SpriteSortMode.Deferred, null, null, null, null, Cache.PixelShaders[(int)PixelShader.Weapon]);
                                            SpriteHelper.DrawEquipment(spriteBatch, weaponIndex, weaponFrame, x, y, playerOffsetX, playerOffsetY, offsetX, offsetY, (Weapon != null ? Weapon.Colour : (int)GameColour.Normal), fade, Weapon.SpriteFileFemale);
                                            spriteBatch.End();
                                            spriteBatch.Begin();
                                        }
                                        else
                                        {*/
                                            SpriteHelper.DrawEquipment(spriteBatch, weaponIndex, weaponFrame, x, y, playerOffsetX, playerOffsetY, offsetX, offsetY, (Weapon.ColorType == GameColor.Custom) ? SpriteHelper.ColourFromArgb(Weapon.Colour) : Cache.Colors[Weapon.ColorType], fade, Weapon.SpriteFileFemale);
                                        //}
                                    }
                                }
                                else
                                {
                                    if (!AnimationDrawWeaponFront && Weapon != null) // weapon
                                    {
                                        int weaponIndex = ((int)SpriteId.FemaleWeapon + Weapon.Appearance * 64 + 8 * AnimationMotionOffsetWeapon) + ((int)Direction - 1);
                                        int weaponFrame = AnimationFrame;
                                        /*if (Cache.GameSettings.DetailMode == GraphicsDetail.High)
                                        {
                                            spriteBatch.End();
                                            spriteBatch.Begin(SpriteSortMode.Deferred, null, null, null, null, Cache.PixelShaders[(int)PixelShader.Weapon]);
                                            SpriteHelper.DrawEquipment(spriteBatch, weaponIndex, weaponFrame, x, y, playerOffsetX, playerOffsetY, offsetX, offsetY, (Weapon != null ? Weapon.Colour : (int)GameColour.Normal), fade, Weapon.SpriteFileFemale);
                                            spriteBatch.End();
                                            spriteBatch.Begin();
                                        }
                                        else
                                        {*/
                                            SpriteHelper.DrawEquipment(spriteBatch, weaponIndex, weaponFrame, x, y, playerOffsetX, playerOffsetY, offsetX, offsetY, (Weapon.ColorType == GameColor.Custom) ? SpriteHelper.ColourFromArgb(Weapon.Colour) : Cache.Colors[Weapon.ColorType], fade, Weapon.SpriteFileFemale);
                                        //}
                                    }
                                    if (!AnimationDrawShieldFront && Shield != null) // shield
                                    {
                                        int shieldIndex = (int)SpriteId.FemaleShield + Shield.Appearance * 8 + AnimationMotionOffsetWeapon;
                                        int shieldFrame = (((int)Direction - 1) * AnimationFrameCount) + AnimationFrame;
                                        /*if (Cache.GameSettings.DetailMode == GraphicsDetail.High)
                                        {
                                            spriteBatch.End();
                                            spriteBatch.Begin(SpriteSortMode.Deferred, null, null, null, null, Cache.PixelShaders[(int)PixelShader.Weapon]);
                                            SpriteHelper.DrawEquipment(spriteBatch, shieldIndex, shieldFrame, x, y, playerOffsetX, playerOffsetY, offsetX, offsetY, (Shield != null ? Shield.Colour : (int)GameColour.Normal), fade, Shield.SpriteFileFemale);
                                            spriteBatch.End();
                                            spriteBatch.Begin();
                                        }
                                        else
                                        {*/
                                            SpriteHelper.DrawEquipment(spriteBatch, shieldIndex, shieldFrame, x, y, playerOffsetX, playerOffsetY, offsetX, offsetY, (Shield.ColorType == GameColor.Custom) ? SpriteHelper.ColourFromArgb(Shield.Colour) : Cache.Colors[Shield.ColorType], fade, Shield.SpriteFileFemale);
                                        //}
                                    }
                                }
                            }

                            /* DRAW LEGS */
                            if (Leggings != null) // legs
                            {
                                int legsIndex = ((int)SpriteId.FemaleLegs + Leggings.Appearance * 15) + AnimationMotionOffset;
                                int legsFrame = (((int)Direction - 1) * AnimationFrameCount) + AnimationFrame;
                                SpriteHelper.DrawEquipment(spriteBatch, legsIndex, legsFrame, x, y, playerOffsetX, playerOffsetY, offsetX, offsetY, (Leggings.ColorType == GameColor.Custom) ? SpriteHelper.ColourFromArgb(Leggings.Colour) : Cache.Colors[Leggings.ColorType], fade, Leggings.SpriteFileFemale);
                            }

                            /* DRAW BOOTS */
                            if (Cache.GameSettings.TeamColorOn && teamColor != GameColor.None)
                            {
                                int bootsIndex = (int)SpriteId.FemaleBoots + 30 + AnimationMotionOffset;
                                int bootsFrame = (((int)Direction - 1) * AnimationFrameCount) + AnimationFrame;
                                SpriteHelper.DrawEquipment(spriteBatch, bootsIndex, bootsFrame, x, y, playerOffsetX, playerOffsetY, offsetX, offsetY, Cache.Colors[teamColor], fade);
                            }
                            else if (Boots != null) // boots
                            {
                                int bootsIndex = ((int)SpriteId.FemaleBoots + Boots.Appearance * 15) + AnimationMotionOffset;
                                int bootsFrame = (((int)Direction - 1) * AnimationFrameCount) + AnimationFrame;
                                SpriteHelper.DrawEquipment(spriteBatch, bootsIndex, bootsFrame, x, y, playerOffsetX, playerOffsetY, offsetX, offsetY, (Boots.ColorType == GameColor.Custom) ? SpriteHelper.ColourFromArgb(Boots.Colour) : Cache.Colors[Boots.ColorType], fade, Boots.SpriteFileFemale);
                            }

                            /* DRAW ARMS */
                            if (Hauberk != null) // arms
                            {
                                int armsIndex = ((int)SpriteId.FemaleArms + Hauberk.Appearance * 15) + AnimationMotionOffset;
                                int armsFrame = (((int)Direction - 1) * AnimationFrameCount) + AnimationFrame;
                                SpriteHelper.DrawEquipment(spriteBatch, armsIndex, armsFrame, x, y, playerOffsetX, playerOffsetY, offsetX, offsetY, (Hauberk.ColorType == GameColor.Custom) ? SpriteHelper.ColourFromArgb(Hauberk.Colour) : Cache.Colors[Hauberk.ColorType], fade, Hauberk.SpriteFileFemale);
                            }

                            /* DRAW BODY */
                            if (BodyArmour != null) // body
                            {
                                int bodyIndex = ((int)SpriteId.FemaleBody + BodyArmour.Appearance * 15) + AnimationMotionOffset;
                                int bodyFrame = (((int)Direction - 1) * AnimationFrameCount) + AnimationFrame;
                                SpriteHelper.DrawEquipment(spriteBatch, bodyIndex, bodyFrame, x, y, playerOffsetX, playerOffsetY, offsetX, offsetY, (BodyArmour.ColorType == GameColor.Custom) ? SpriteHelper.ColourFromArgb(BodyArmour.Colour) : Cache.Colors[BodyArmour.ColorType], fade, BodyArmour.SpriteFileFemale);
                            }

                            /* DRAW HELM OR HAIR */
                            if (Helmet != null) // helm
                            {
                                int headIndex = ((int)SpriteId.FemaleHead + Helmet.Appearance * 15) + AnimationMotionOffset;
                                int headFrame = (((int)Direction - 1) * AnimationFrameCount) + AnimationFrame;
                                SpriteHelper.DrawEquipment(spriteBatch, headIndex, headFrame, x, y, playerOffsetX, playerOffsetY, offsetX, offsetY, (Helmet.ColorType == GameColor.Custom) ? SpriteHelper.ColourFromArgb(Helmet.Colour) : Cache.Colors[Helmet.ColorType], fade, Helmet.SpriteFileFemale);
                            }
                            else
                            {
                                int hairIndex = ((int)SpriteId.FemaleHair + ((Appearance1 & 0x0F00) >> 8) * 15) + AnimationMotionOffset;
                                int hairFrame = (((int)Direction - 1) * AnimationFrameCount) + AnimationFrame;
                                SpriteHelper.DrawEquipment(spriteBatch, hairIndex, hairFrame, x, y, playerOffsetX, playerOffsetY, offsetX, offsetY, colour, fade);
                            }

                            /* DRAW WEAPONS AND SHIELDS IN FRONT OF BODY */
                            if (drawWeapons)
                            {
                                /* HARD CODED EXCEPTION FOR NORTH EAST */
                                if (direction == MotionDirection.NorthEast)
                                {
                                    if (AnimationDrawShieldFront && Shield != null) // shield
                                    {
                                        int shieldIndex = (int)SpriteId.FemaleShield + Shield.Appearance * 8 + AnimationMotionOffsetWeapon;
                                        int shieldFrame = (((int)Direction - 1) * AnimationFrameCount) + AnimationFrame;
                                        /*if (Cache.GameSettings.DetailMode == GraphicsDetail.High)
                                        {
                                            spriteBatch.End();
                                            spriteBatch.Begin(SpriteSortMode.Deferred, null, null, null, null, Cache.PixelShaders[(int)PixelShader.Weapon]);
                                            SpriteHelper.DrawEquipment(spriteBatch, shieldIndex, shieldFrame, x, y, playerOffsetX, playerOffsetY, offsetX, offsetY, (Shield != null ? Shield.Colour : (int)GameColour.Normal), fade, Shield.SpriteFileFemale);
                                            spriteBatch.End();
                                            spriteBatch.Begin();
                                        }
                                        else
                                        {*/
                                            SpriteHelper.DrawEquipment(spriteBatch, shieldIndex, shieldFrame, x, y, playerOffsetX, playerOffsetY, offsetX, offsetY, (Shield.ColorType == GameColor.Custom) ? SpriteHelper.ColourFromArgb(Shield.Colour) : Cache.Colors[Shield.ColorType], fade, Shield.SpriteFileFemale);
                                        //}
                                    }
                                    if (AnimationDrawWeaponFront && Weapon != null) // weapon
                                    {
                                        int weaponIndex = ((int)SpriteId.FemaleWeapon + Weapon.Appearance * 64 + 8 * AnimationMotionOffsetWeapon) + ((int)Direction - 1);
                                        int weaponFrame = AnimationFrame;
                                        /*if (Cache.GameSettings.DetailMode == GraphicsDetail.High)
                                        {
                                            spriteBatch.End();
                                            spriteBatch.Begin(SpriteSortMode.Deferred, null, null, null, null, Cache.PixelShaders[(int)PixelShader.Weapon]);
                                            SpriteHelper.DrawEquipment(spriteBatch, weaponIndex, weaponFrame, x, y, playerOffsetX, playerOffsetY, offsetX, offsetY, (Weapon != null ? Weapon.Colour : (int)GameColour.Normal), fade, Weapon.SpriteFileFemale);
                                            spriteBatch.End();
                                            spriteBatch.Begin();
                                        }
                                        else
                                        {*/
                                            SpriteHelper.DrawEquipment(spriteBatch, weaponIndex, weaponFrame, x, y, playerOffsetX, playerOffsetY, offsetX, offsetY, (Weapon.ColorType == GameColor.Custom) ? SpriteHelper.ColourFromArgb(Weapon.Colour) : Cache.Colors[Weapon.ColorType], fade, Weapon.SpriteFileFemale);
                                        //}
                                    }
                                }
                                else
                                {
                                    if (AnimationDrawWeaponFront && Weapon != null) // weapon
                                    {
                                        int weaponIndex = ((int)SpriteId.FemaleWeapon + Weapon.Appearance * 64 + 8 * AnimationMotionOffsetWeapon) + ((int)Direction - 1);
                                        int weaponFrame = AnimationFrame;
                                        /*if (Cache.GameSettings.DetailMode == GraphicsDetail.High)
                                        {
                                            spriteBatch.End();
                                            spriteBatch.Begin(SpriteSortMode.Deferred, null, null, null, null, Cache.PixelShaders[(int)PixelShader.Weapon]);
                                            SpriteHelper.DrawEquipment(spriteBatch, weaponIndex, weaponFrame, x, y, playerOffsetX, playerOffsetY, offsetX, offsetY, (Weapon != null ? Weapon.Colour : (int)GameColour.Normal), fade, Weapon.SpriteFileFemale);
                                            spriteBatch.End();
                                            spriteBatch.Begin();
                                        }
                                        else
                                        {*/
                                            SpriteHelper.DrawEquipment(spriteBatch, weaponIndex, weaponFrame, x, y, playerOffsetX, playerOffsetY, offsetX, offsetY, (Weapon.ColorType == GameColor.Custom) ? SpriteHelper.ColourFromArgb(Weapon.Colour) : Cache.Colors[Weapon.ColorType], fade, Weapon.SpriteFileFemale);
                                        //}                                     
                                    }
                                    if (AnimationDrawShieldFront && Shield != null) // shield
                                    {
                                        int shieldIndex = (int)SpriteId.FemaleShield + Shield.Appearance * 8 + AnimationMotionOffsetWeapon;
                                        int shieldFrame = (((int)Direction - 1) * AnimationFrameCount) + AnimationFrame;
                                        /*if (Cache.GameSettings.DetailMode == GraphicsDetail.High)
                                        {
                                            spriteBatch.End();
                                            spriteBatch.Begin(SpriteSortMode.Deferred, null, null, null, null, Cache.PixelShaders[(int)PixelShader.Weapon]);
                                            SpriteHelper.DrawEquipment(spriteBatch, shieldIndex, shieldFrame, x, y, playerOffsetX, playerOffsetY, offsetX, offsetY, (Shield != null ? Shield.Colour : (int)GameColour.Normal), fade, Shield.SpriteFileFemale);
                                            spriteBatch.End();
                                            spriteBatch.Begin();
                                        }
                                        else
                                        {*/
                                            SpriteHelper.DrawEquipment(spriteBatch, shieldIndex, shieldFrame, x, y, playerOffsetX, playerOffsetY, offsetX, offsetY, (Shield.ColorType == GameColor.Custom) ? SpriteHelper.ColourFromArgb(Shield.Colour) : Cache.Colors[Shield.ColorType], fade, Shield.SpriteFileFemale);
                                        //}                          
                                    }
                                }
                            }

                            /* DRAW CAPE IN FRONT OF BODY */
                            if (AnimationDrawCapeFront)
                            {
                                if (Cache.GameSettings.TeamColorOn && teamColor != GameColor.None)
                                {
                                    int capeIndex = (int)SpriteId.FemaleCape + 45 + AnimationMotionOffset;
                                    int capeFrame = (((int)Direction - 1) * AnimationFrameCount) + AnimationFrame;
                                    SpriteHelper.DrawEquipment(spriteBatch, capeIndex, capeFrame, x, y, playerOffsetX, playerOffsetY, offsetX, offsetY, Cache.Colors[teamColor], fade);
                                }
                                else if (Cape != null) // cape
                                {
                                    int capeIndex = ((int)SpriteId.FemaleCape + Cape.Appearance * 15) + AnimationMotionOffset;
                                    int capeFrame = (((int)Direction - 1) * AnimationFrameCount) + AnimationFrame;
                                    SpriteHelper.DrawEquipment(spriteBatch, capeIndex, capeFrame, x, y, playerOffsetX, playerOffsetY, offsetX, offsetY, (Cape.ColorType == GameColor.Custom) ? SpriteHelper.ColourFromArgb(Cape.Colour) : Cache.Colors[Cape.ColorType], fade, Cape.SpriteFileFemale);
                                }
                            }
                        }
                        break;
                }

                if (Cache.GameSettings.DetailMode != GraphicsDetail.Low && offsetPoints != null &&
                    (motionType == MotionType.Dash || motionType == MotionType.Fly)) // dash/fly shadow
                {
                    foreach (int[] offset in offsetPoints)
                        SpriteHelper.DrawBody(spriteBatch, Type, Animation, AnimationFrame,
                                        x, y, playerOffsetX, playerOffsetY, offset[0], offset[1], Cache.Colors[GameColor.Shadow], 0.2f, out selected);

                    SpriteHelper.DrawBody(spriteBatch, Type, Animation, AnimationFrame,
                                        x, y, playerOffsetX, playerOffsetY, offsetX, offsetY, Cache.Colors[GameColor.Shadow], 0.2f, out selected);
                }

                if (showAura)
                {
                    if (IsBerserked)
                    {
                        if (Cache.GameSettings.DetailMode == GraphicsDetail.High)
                        {
                            int skintype = (Gender == GenderType.Male ? skintype = 2 : skintype = 4);
                            spriteBatch.End();
                            spriteBatch.Begin(SpriteSortMode.Deferred, BlendState.AlphaBlend, null, null, null, Cache.PixelShaders[(int)PixelShader.Berserk]);
                            SpriteHelper.DrawBody(spriteBatch, skintype, Animation, AnimationFrame,
                                                x, y, playerOffsetX, playerOffsetY, offsetX, offsetY, Cache.Colors[GameColor.Normal], 0.5f, out selected);
                            spriteBatch.End();
                            spriteBatch.Begin();
                        }
                        else
                        {
                            SpriteHelper.DrawBody(spriteBatch, Type, Animation, AnimationFrame,
                                                x, y, playerOffsetX, playerOffsetY, offsetX, offsetY, Cache.Colors[GameColor.Berserk], 0.5f, out selected);
                        }

                    }
                    if (IsHaste)
                        SpriteHelper.DrawBody(spriteBatch, Type, Animation, AnimationFrame,
                                            x, y, playerOffsetX, playerOffsetY, offsetX, offsetY, Cache.Colors[GameColor.Haste], 0.5f, out selected);

                    if (IsPoisoned)
                        SpriteHelper.DrawEffect(spriteBatch, 81, effectFrame % 21, x + 115, y + 120 - 40, -offsetX - playerOffsetX, -offsetY - playerOffsetY, Color.White * 0.7f);

                    bool angelSelected = false;
                    if (Angel != null)
                        switch ((AngelType)Angel.Effect1)
                        {
                            case AngelType.Str: SpriteHelper.DrawAngel(spriteBatch, motionType, direction, 0, animationFrame, x, y, offsetX + playerOffsetX, offsetY + playerOffsetY, out angelSelected, Angel.SpriteFile); break;
                            case AngelType.Dex: SpriteHelper.DrawAngel(spriteBatch, motionType, direction, 1, animationFrame, x, y, offsetX + playerOffsetX, offsetY + playerOffsetY, out angelSelected, Angel.SpriteFile); break;
                            case AngelType.Int: SpriteHelper.DrawAngel(spriteBatch, motionType, direction, 2, animationFrame, x, y - 20, offsetX + playerOffsetX, offsetY + playerOffsetY, out angelSelected, Angel.SpriteFile); break;
                            case AngelType.Mag: SpriteHelper.DrawAngel(spriteBatch, motionType, direction, 3, animationFrame, x, y - 20, offsetX + playerOffsetX, offsetY + playerOffsetY, out angelSelected, Angel.SpriteFile); break;
                            case AngelType.Vit: SpriteHelper.DrawAngel(spriteBatch, motionType, direction, 0, animationFrame, x, y, offsetX + playerOffsetX, offsetY + playerOffsetY, out angelSelected, Angel.SpriteFile); break;
                            case AngelType.Agi: SpriteHelper.DrawAngel(spriteBatch, motionType, direction, 1, animationFrame, x, y, offsetX + playerOffsetX, offsetY + playerOffsetY, out angelSelected, Angel.SpriteFile); break;
                        }    
                }

                if (!isDead && IsInvisible)
                    SpriteHelper.DrawBody(spriteBatch, Type, Animation, AnimationFrame,
                                        x, y, playerOffsetX, playerOffsetY, offsetX, offsetY, Cache.Colors[GameColor.Normal], (-0.3f + Cache.TransparencyFaders.GlowFrame), out selected);


            }
            else
            {
                // TODO - polymorph?
            }

            //Damage History
            if (damageHistory.Color != GameColor.None)
            {
                //Outline
                spriteBatch.DrawString(Cache.Fonts[damageHistory.Font], damageHistory.Message, new Vector2((int)(x - (Cache.Fonts[damageHistory.Font].MeasureString(damageHistory.Message).X / 2) + 10 + playerOffsetX + offsetX + 1 + damageHistory.X), (int)(y - (Cache.Fonts[damageHistory.Font].MeasureString(damageHistory.Message).Y / 2) - 45 + playerOffsetY + offsetY + 1 + damageHistory.Y)), Color.Black * damageHistory.Transparency, 0.0f, new Vector2((int)Cache.Fonts[damageHistory.Font].MeasureString(damageHistory.Message).X / 2, (int)Cache.Fonts[damageHistory.Font].MeasureString(damageHistory.Message).Y / 2), damageHistory.Scale, SpriteEffects.None, 0.0f);
                //spriteBatch.DrawString(display.Fonts[damageHistory.FontType], damageHistory.Message, new Vector2((int)(x - (display.Fonts[damageHistory.FontType].MeasureString(damageHistory.Message).X / 2) + 10 + playerOffsetX + offsetX - 1 + damageHistory.X), (int)(y - (display.Fonts[damageHistory.FontType].MeasureString(damageHistory.Message).Y / 2) - 45 + playerOffsetY + offsetY - 1 + damageHistory.Y)), Color.Black * damageHistory.Transparency, 0.0f, new Vector2((int)display.Fonts[damageHistory.FontType].MeasureString(damageHistory.Message).X / 2, (int)display.Fonts[damageHistory.FontType].MeasureString(damageHistory.Message).Y / 2), damageHistory.Scale, SpriteEffects.None, 0.0f);
                //spriteBatch.DrawString(display.Fonts[damageHistory.FontType], damageHistory.Message, new Vector2((int)(x - (display.Fonts[damageHistory.FontType].MeasureString(damageHistory.Message).X / 2) + 10 + playerOffsetX + offsetX + 1 + damageHistory.X), (int)(y - (display.Fonts[damageHistory.FontType].MeasureString(damageHistory.Message).Y / 2) - 45 + playerOffsetY + offsetY - 1 + damageHistory.Y)), Color.Black * damageHistory.Transparency, 0.0f, new Vector2((int)display.Fonts[damageHistory.FontType].MeasureString(damageHistory.Message).X / 2, (int)display.Fonts[damageHistory.FontType].MeasureString(damageHistory.Message).Y / 2), damageHistory.Scale, SpriteEffects.None, 0.0f);
                //spriteBatch.DrawString(display.Fonts[damageHistory.FontType], damageHistory.Message, new Vector2((int)(x - (display.Fonts[damageHistory.FontType].MeasureString(damageHistory.Message).X / 2) + 10 + playerOffsetX + offsetX - 1 + damageHistory.X), (int)(y - (display.Fonts[damageHistory.FontType].MeasureString(damageHistory.Message).Y / 2) - 45 + playerOffsetY + offsetY + 1 + damageHistory.Y)), Color.Black * damageHistory.Transparency, 0.0f, new Vector2((int)display.Fonts[damageHistory.FontType].MeasureString(damageHistory.Message).X / 2, (int)display.Fonts[damageHistory.FontType].MeasureString(damageHistory.Message).Y / 2), damageHistory.Scale, SpriteEffects.None, 0.0f);

                //Text
                spriteBatch.DrawString(Cache.Fonts[damageHistory.Font], damageHistory.Message, new Vector2((int)(x - (Cache.Fonts[damageHistory.Font].MeasureString(damageHistory.Message).X / 2) + 10 + playerOffsetX + offsetX + damageHistory.X), (int)(y - (Cache.Fonts[damageHistory.Font].MeasureString(damageHistory.Message).Y / 2) - 45 + playerOffsetY + offsetY + damageHistory.Y)), Cache.Colors[damageHistory.Color] * damageHistory.Transparency, 0.0f, new Vector2((int)Cache.Fonts[damageHistory.Font].MeasureString(damageHistory.Message).X / 2, (int)Cache.Fonts[damageHistory.Font].MeasureString(damageHistory.Message).Y / 2), damageHistory.Scale, SpriteEffects.None, 0.0f);
            }

            //Vital History
            if (vitalHistory.Color != GameColor.None)
            {
                spriteBatch.DrawString(Cache.Fonts[vitalHistory.Font], vitalHistory.Message, new Vector2((int)(x - (Cache.Fonts[vitalHistory.Font].MeasureString(vitalHistory.Message).X / 2) + playerOffsetX + offsetX + 1 + vitalHistory.X), (int)(y - (Cache.Fonts[vitalHistory.Font].MeasureString(vitalHistory.Message).Y / 2) - 30 + playerOffsetY + offsetY + 1 + vitalHistory.Y)), Color.Black * vitalHistory.Transparency, 0.0f, new Vector2((int)Cache.Fonts[vitalHistory.Font].MeasureString(vitalHistory.Message).X / 2, (int)Cache.Fonts[vitalHistory.Font].MeasureString(vitalHistory.Message).Y / 2), vitalHistory.Scale, SpriteEffects.None, 0.0f);
                spriteBatch.DrawString(Cache.Fonts[vitalHistory.Font], vitalHistory.Message, new Vector2((int)(x - (Cache.Fonts[vitalHistory.Font].MeasureString(vitalHistory.Message).X / 2) + playerOffsetX + offsetX + vitalHistory.X), (int)(y - (Cache.Fonts[vitalHistory.Font].MeasureString(vitalHistory.Message).Y / 2) - 30 + playerOffsetY + offsetY + vitalHistory.Y)), Cache.Colors[vitalHistory.Color] * vitalHistory.Transparency, 0.0f, new Vector2((int)Cache.Fonts[vitalHistory.Font].MeasureString(vitalHistory.Message).X / 2, (int)Cache.Fonts[vitalHistory.Font].MeasureString(vitalHistory.Message).Y / 2), vitalHistory.Scale, SpriteEffects.None, 0.0f);
            }

            ///* Vital History Show multiple*/
            //int index = 0;
            //if (vitalHistory.Count > 0)
            //{
            //    for (int i = vitalHistory.Count - 1; i >= 0; i--)
            //    {
            //        Color textColor = vitalHistory[i].Color;
            //        if (vitalHistory[i].Message.Contains("+")) { textColor = SpriteHelper.Lighten(textColor.R, textColor.G, textColor.B); }

            //        spriteBatch.DrawString(display.Fonts[vitalHistory[i].FontType], vitalHistory[i].Message, new Vector2((int)(x - (display.Fonts[vitalHistory[i].FontType].MeasureString(vitalHistory[i].Message).X / 2) + playerOffsetX + offsetX + 1), (int)(y - (display.Fonts[vitalHistory[i].FontType].MeasureString(vitalHistory[i].Message).Y / 2) - 36 + (9 * index) + playerOffsetY + offsetY + 1)), Color.Black * (vitalHistory[i].Transparency - (index * 0.1f)));
            //        //spriteBatch.DrawString(display.Fonts[vitalHistory[i].FontType], vitalHistory[i].Message, new Vector2((int)(x - (display.Fonts[vitalHistory[i].FontType].MeasureString(vitalHistory[i].Message).X / 2) + playerOffsetX + offsetX - 1), (int)(y - (display.Fonts[vitalHistory[i].FontType].MeasureString(vitalHistory[i].Message).Y / 2) - 36 + (9 * index) + playerOffsetY + offsetY - 1)), Color.Black * (vitalHistory[i].Transparency - (index * 0.1f)));
            //        //spriteBatch.DrawString(display.Fonts[vitalHistory[i].FontType], vitalHistory[i].Message, new Vector2((int)(x - (display.Fonts[vitalHistory[i].FontType].MeasureString(vitalHistory[i].Message).X / 2) + playerOffsetX + offsetX + 1), (int)(y - (display.Fonts[vitalHistory[i].FontType].MeasureString(vitalHistory[i].Message).Y / 2) - 36 + (9 * index) + playerOffsetY + offsetY - 1)), Color.Black * (vitalHistory[i].Transparency - (index * 0.1f)));
            //        //spriteBatch.DrawString(display.Fonts[vitalHistory[i].FontType], vitalHistory[i].Message, new Vector2((int)(x - (display.Fonts[vitalHistory[i].FontType].MeasureString(vitalHistory[i].Message).X / 2) + playerOffsetX + offsetX - 1), (int)(y - (display.Fonts[vitalHistory[i].FontType].MeasureString(vitalHistory[i].Message).Y / 2) - 36 + (9 * index) + playerOffsetY + offsetY + 1)), Color.Black * (vitalHistory[i].Transparency - (index * 0.1f)));

            //        spriteBatch.DrawString(display.Fonts[vitalHistory[i].FontType], vitalHistory[i].Message, new Vector2((int)(x - (display.Fonts[vitalHistory[i].FontType].MeasureString(vitalHistory[i].Message).X / 2) + playerOffsetX + offsetX), (int)(y - (display.Fonts[vitalHistory[i].FontType].MeasureString(vitalHistory[i].Message).Y / 2) - 36 + (9 * index) + playerOffsetY + offsetY)), textColor * (vitalHistory[i].Transparency - (index * 0.1f)));
            //        index++;
            //    }
            //}

            //Spell History
            if (spellHistory.Color != GameColor.None)
            {
                spriteBatch.DrawString(Cache.Fonts[spellHistory.Font], spellHistory.Message, new Vector2((int)(x - (Cache.Fonts[spellHistory.Font].MeasureString(spellHistory.Message).X / 2) + playerOffsetX + offsetX + 1), (int)(y - (Cache.Fonts[spellHistory.Font].MeasureString(spellHistory.Message).Y / 2) - 65 - spellHistory.Y + playerOffsetY + offsetY + 1)), Color.Black * spellHistory.Transparency);
                spriteBatch.DrawString(Cache.Fonts[spellHistory.Font], spellHistory.Message, new Vector2((int)(x - (Cache.Fonts[spellHistory.Font].MeasureString(spellHistory.Message).X / 2) + playerOffsetX + offsetX), (int)(y - (Cache.Fonts[spellHistory.Font].MeasureString(spellHistory.Message).Y / 2) - 65 - spellHistory.Y + playerOffsetY + offsetY)), Cache.Colors[spellHistory.Color] * spellHistory.Transparency);
            }

            //Chat History
            if (chatHistory.Color != GameColor.None)
            {
                spriteBatch.DrawString(Cache.Fonts[chatHistory.Font], chatHistory.Message, new Vector2((int)(x - (Cache.Fonts[chatHistory.Font].MeasureString(chatHistory.Message).X / 2) + playerOffsetX + offsetX + 1), (int)(y - (Cache.Fonts[chatHistory.Font].MeasureString(chatHistory.Message).Y / 2) - 45 - chatHistory.Y + playerOffsetY + offsetY + 1)), Color.Black * chatHistory.Transparency);
                spriteBatch.DrawString(Cache.Fonts[chatHistory.Font], chatHistory.Message, new Vector2((int)(x - (Cache.Fonts[chatHistory.Font].MeasureString(chatHistory.Message).X / 2) + playerOffsetX + offsetX), (int)(y - (Cache.Fonts[chatHistory.Font].MeasureString(chatHistory.Message).Y / 2) - 45 - chatHistory.Y + playerOffsetY + offsetY)), Cache.Colors[chatHistory.Color] * chatHistory.Transparency);
            }    
            return selected;
        }

        public OwnerRelationShip GetRelationship(OwnerSide playerSide)
        {
            //unchecked
            //{
            /*bool criminal = ((status & (int)0x80000000) == 0) ? false : true;
            bool citizen = ((status & (int)0x40000000) == 0) ? false : true;
            bool hunter = ((status & (int)0x10000000) == 0) ? false : true;

            if (!citizen)
            {
                if (playerSide == OwnerSide.Neutral) return OwnerRelationShip.Neutral;
                else return OwnerRelationShip.Enemy;
            }
            else */
            if (side == OwnerSide.Neutral) return OwnerRelationShip.Neutral;
            else if (side == playerSide)
            {
                if (sideStatus == OwnerSideStatus.Criminal) return OwnerRelationShip.Enemy;
                else
                {
                    if (playerSide == OwnerSide.Neutral) return OwnerRelationShip.Neutral;
                    else return OwnerRelationShip.Friendly;
                }
            }
            else return OwnerRelationShip.Enemy;
            //}
        }

        public bool Draw(SpriteBatch spriteBatch, GameDisplay display)
        {
            //DrawItemPopup(spriteBatch, gInterface, this.x, this.y);
            return false;
        }

        public void FadeOut()
        {
            motionType = MotionType.DeadFadeOut;
            //animationFrame = 0;
        }

        public string AbsorptionValue(MagicAttribute element)
        {
            string value;
            int elementalAbsorption = 0;
            if ((inventory.CompleteEarthProtection && element == MagicAttribute.Earth) || (inventory.CompleteSpiritProtection && element == MagicAttribute.Spirit) ||
                (inventory.CompleteAirProtection && element == MagicAttribute.Air) || (inventory.CompleteUnholyProtection && element == MagicAttribute.Unholy) ||
                (inventory.CompleteFireProtection && element == MagicAttribute.Fire) || (inventory.CompleteKinesisProtection && element == MagicAttribute.Kinesis) ||
                (inventory.CompleteWaterProtection && element == MagicAttribute.Water) || inventory.CompleteMagicProtection)
            {
                value = "Complete"; return value;
            }

            elementalAbsorption += inventory.MagicAbsorptionBonus + VitalityAbsorption;
            switch (element)
            {
                case MagicAttribute.Earth: elementalAbsorption += inventory.EarthAbsorptionBonus; break;
                case MagicAttribute.Fire: elementalAbsorption += inventory.FireAbsorptionBonus; break;
                case MagicAttribute.Air: elementalAbsorption += inventory.AirAbsorptionBonus; break;
                case MagicAttribute.Water: elementalAbsorption += inventory.WaterAbsorptionBonus; break;
                case MagicAttribute.Unholy: elementalAbsorption += inventory.UnholyAbsorptionBonus; break;
                case MagicAttribute.Spirit: elementalAbsorption += inventory.SpiritAbsorptionBonus; break;
                case MagicAttribute.None:break;
                case MagicAttribute.Kinesis: elementalAbsorption += inventory.KinesisAbsorptionBonus; break;
                case MagicAttribute.All: elementalAbsorption += inventory.EarthAbsorptionBonus + inventory.AirAbsorptionBonus + inventory.FireAbsorptionBonus + inventory.WaterAbsorptionBonus +
                    inventory.UnholyAbsorptionBonus + inventory.SpiritAbsorptionBonus + inventory.KinesisAbsorptionBonus; break;
            }


            if (elementalAbsorption > Globals.MaximumMagicalAbsorption)
            {
                elementalAbsorption = Globals.MaximumMagicalAbsorption;
                value = "Max: " + elementalAbsorption.ToString() + "%"; return value;
            }

            value = elementalAbsorption.ToString() + "%";
            return value;
        }

        // Iowner
        public UInt16 ObjectId { get { return objectId; } set { objectId = value; } }
        public int Id { get; set; }
        public bool IsMoving { get { return isMoving; } set { isMoving = value; } }
        public bool MoveReady { get { return moveReady; } set { moveReady = value; } }
        public bool HaltMovement { get { return haltMovement; } set { haltMovement = value; } }
        public MotionDirection HaltDirection { get { return haltDirection; } set { haltDirection = value; } } 
        public bool CanHalt { get { return !MoveReady && Motion != MotionType.Idle && direction != MotionDirection.None;}}
        public bool IsCasting { get { return isCasting; } set { isCasting = value; } }
        public int SpellRequest { get { return spellRequest; } set { spellRequest = value; } }
        public int SpellCharged { get { return spellCharged; } set { spellCharged = value; } }
        public bool SpellReady { get { return spellReady; } set { spellReady = value; } }
        public bool SpellJustCast { get { return spellJustCast; } set { spellJustCast = value; } } //not used?
        public DateTime SpellJustCastTime { get { return spellJustCastTime; } set { spellJustCastTime = value; } } //not used
        public bool SpecialAbilityReady { get { return specialAbilityReady; } set { specialAbilityReady = value; } }
        public List<ItemSpecialAbilityType> SpecialAbilitiesPassive { get { return inventory.SpecialAbilitiesPassive; } }
        public Dictionary<ItemSpecialAbilityType, int> SpecialAbilitiesActive { get { return inventory.SpecialAbilitiesActive; } }
        public bool IsTrading { get { return (tradePartner != -1); } }
        public int TradePartnerID { get { return tradePartner; } set { tradePartner = value; } }
        public bool RunningMode { get { return runningMode; } set { runningMode = value; } }
        public bool IsRunning { get { return isRunning && sp > 0; } set { isRunning = value; } }
        public int X { get { return x; } }
        public int Y { get { return y; } }
        public int DestinationX { get { return destinationX; } set { destinationX = value; } }
        public int DestinationY { get { return destinationY; } set { destinationY = value; } }
        public int Type { get { return type; } set { type = value; } }
        public MotionDirection Direction { get { return direction; } set { direction = value; } }
        public MotionType Motion { get { return motionType; } set { motionType = value; } }
        public DateTime AnimationTime { get { return animationTime; } set { animationTime = value; } }
        public int AnimationFrame { get { return animationFrame; } set { animationFrame = value; } }
        public int Commands { get { return commands; } set { commands = value; } }
        public Map Map { get { return currentMap; } set { currentMap = value; } }
        public MapTile Location { get { return (Map.LocationExists(x, y) ? Map[y][x] : null); } }
        public string Name { get { return name; } set { name = value; } }
        public PlayType PlayType { get { return playType; } set { playType = value; } }
        public List<NpcPerk> Perks { get { throw new NotImplementedException("Player cannot have Npc perks."); } }
        public int Appearance1 { get { return appearance1; } set { appearance1 = value; } }
        public int Appearance2 { get { return appearance2; } set { appearance2 = value; } }
        public int Appearance3 { get { return appearance3; } set { appearance3 = value; } }
        public int Appearance4 { get { return appearance4; } set { appearance4 = value; } }
        public int AppearanceColour { get { return appearanceColour; } set { appearanceColour = value; } }
        public int Status { get { return status; } set { status = value; } }

        //account
        public string Username { get { return accountName; } }
        public string Password { get { return accountPassword; } }

        // player
        public int Level { get { return level; } set { level = value; } }
        public int RebirthLevel { get { return rebirthLevel; } set { rebirthLevel = value; } }
        public int GladiatorPoints { get { return gladiatorPoints; } set { gladiatorPoints = value; } }
        public int Experience { get { return experience; } set { experience = value; } }
        public int Strength { get { return strength; } set { strength = value; } }
        public int Dexterity { get { return dexterity; } set { dexterity = value; } }
        public int Vitality { get { return vitality; } set { vitality = value; } }
        public int Magic { get { return magic; } set { magic = value; } }
        public int Intelligence { get { return intelligence; } set { intelligence = value; } }
        public int Agility { get { return agility; } set { agility = value; } }
        public int LevelUpPoints;
        public string FriendlyMapName;
        public string MapName { get { return currentMapName; } set { currentMapName = value; } }
        public int Hunger;
        public int HP { get { return hp; } set { hp = value; } }
        public int MP { get { return mp; } set { mp = value; } }
        public int SP { get { return sp; } set { sp = value; } }
        public Inventory EquipmentBonuses { get { return inventory; } }
        public bool IsPoisoned { get { return (isPoisoned || (status & 0x80) != 0); } set { isPoisoned = value; } }
        public bool IsParalyzed { get { return isParalyzed; } set { isParalyzed = value; } }
        public bool IsHeld { get { return isHeld; } set { isHeld = value; } }
        public int Luck { get { return luck; } set { luck = value; } }
        public int LuckBonus { get { return inventory.LuckBonus; } }

        /// <summary>
        /// Calculates the maximum hit points this character can have.
        /// </summary>
        public int MaxHP
        {
            get
            {
                int value = ((vitality ) * 3) + (level * 2) + ((strength ) / 2);

                // blood weapons reduce max hp by 20%
                if (inventory.Blood) 
                    value -= (value / 5);

                // hp increase bonus (v2)
                if (inventory.MaxHPBonus > 0)
                    value += (int)(((double)value / (double)100) * (double)inventory.MaxHPBonus);

                return value;
            }
        }

        /// <summary>
        /// Calculates the maximum magic points this character can have.
        /// </summary>
        public int MaxMP
        {
            get
            {

                int value = ((magic ) * 2) + (level * 2) + ((intelligence ) / 2);

                // mp increase bonus (v2)
                if (inventory.MaxMPBonus > 0)
                    value += (int)(((double)value / (double)100) * (double)inventory.MaxMPBonus);

                return value;
            }
        }

        /// <summary>
        /// Calculates the maximum stamina points this character can have.
        /// </summary>
        public int MaxSP
        {
            get
            {
                int value = ((strength ) * 2) + (level * 2);

                // sp increase bonus (v2)
                if (inventory.MaxSPBonus > 0)
                    value += (int)(((double)value / (double)100) * (double)inventory.MaxSPBonus);

                return value;
            }
        }

        public int MaxLuck { get { return (luck + LuckBonus < Globals.MaximumLuck) ? luck + LuckBonus : Globals.MaximumLuck; } }

        public int Contribution { get { return contribution; } set { contribution = value; } }
        public int Majestics { get { return majestics; } set { majestics = value; } }
        public int Criticals { get { return criticals; } set { criticals = value; } }
        public int CriminalCount { get { return criminalCount; } set { criminalCount = value; } }
        public int EnemyKills { get { return enemyKills; } set { enemyKills = value; } }
        public string GuildName { get { return guildName; } set { guildName = value; } }
        public int GuildRank { get { return guildRank; } set { guildRank = value; } }
        public int MerchantId { get { return 0; } }
        public MerchantType MerchantType { get { return MerchantType.None; } }
        public bool IsMerchant { get { return false; } }
        public int Reputation { get { return reputation; } set { reputation = value; } }
        public int RewardGold { get { return rewardGold; } set { rewardGold = value; } }
        public OwnerSide Side { get { return side; } set { side = value; } }
        public OwnerSideStatus SideStatus { get { return sideStatus; } set { sideStatus = value; } }
        public int Gold { get { return gold; } set { gold = value; } }
        public bool RunChange { get { return runChange; } set { runChange = value; } }
        public bool Flying;
        public MotionDirection FlyDirection;
        public int FlyDamage;
        public int FlyHitCount;
        public DamageType FlyDamageType;
        public Party Party;
        public bool HasParty { get { return Party != null; } }
        public bool TickHP { get { return tickHP; } set { tickHP = value; } }
        public bool TickMP { get { return tickMP; } set { tickMP = value; } }
        public bool TickSP { get { return tickSP; } set { tickSP = value; } }
        public bool TickEXP { get { return tickEXP; } set { tickEXP = value; } } 
        public bool IsDamaged { get { return isDamaged; } set { isDamaged = value; } }
        public DateTime FlashDamage { get { return flashDamage; } set { flashDamage = value; } }

        public OwnerType OwnerType { get { return OwnerType.Player; } }
        public int Animation
        {
            get
            {
                int temp = ((int)type * 100);
                if (type <= 6)
                {
                    switch (motionType)
                    {
                        case MotionType.Idle: temp += ((IsCombatMode) ? 8 : 0); break;
                        case MotionType.Move: temp += ((IsCombatMode) ? 24 : 16); break;
                        case MotionType.Run: temp += 32; break;
                        case MotionType.Bow: temp += 40; break;
                        case MotionType.Dash:
                        case MotionType.Attack: temp += 48; break;
                        case MotionType.BowAttack: temp += 56; break;
                        case MotionType.Magic: temp += 64; break;
                        case MotionType.PickUp: temp += 72; break;
                        case MotionType.Fly:
                        case MotionType.TakeDamage: temp += 80; break;
                        case MotionType.Dead:
                        case MotionType.DeadFadeOut:
                        case MotionType.Die: temp += 88; break;
                    }
                }
                else
                {
                    switch (motionType)
                    {
                        case MotionType.Idle: temp += 0; break;
                        case MotionType.Move: temp += 8; break;
                        case MotionType.Attack: temp += 16; break;
                        case MotionType.Fly:
                        case MotionType.TakeDamage: temp += 24; break;
                        case MotionType.Dead:
                        case MotionType.DeadFadeOut:
                        case MotionType.Die: temp += 32; break;
                    }
                }
                return temp + ((int)direction - 1);
            }
        }
        public int AnimationMotionOffset
        {
            get
            {
                int index = 0;
                switch (Motion)
                {
                    case MotionType.Idle: index = (IsCombatMode) ? 1 : 0; break;
                    case MotionType.Move: index = (IsCombatMode) ? 3 : 2; break;
                    case MotionType.Run: index = 4; break;
                    case MotionType.Bow: index = 5; break;
                    case MotionType.Dash:
                    case MotionType.Attack: index = 6; break;
                    case MotionType.BowAttack: index = 7; break;
                    case MotionType.Magic: index = 8; break;
                    case MotionType.PickUp: index = 9; break;
                    case MotionType.Fly:
                    case MotionType.TakeDamage: index = 10; break;
                    case MotionType.Die: index = 11; break;
                    case MotionType.DeadFadeOut:
                    case MotionType.Dead: index = 11; break;
                }

                return index;
            }
        }
        public int AnimationMotionOffsetWeapon
        {
            get
            {
                int index = 0;
                switch (Motion)
                {
                    case MotionType.Idle: index = (IsCombatMode) ? 1 : 0; break;
                    case MotionType.Move: index = (IsCombatMode) ? 3 : 2; break;
                    case MotionType.Dash:
                    case MotionType.BowAttack:
                    case MotionType.Attack: index = 4; break;
                    case MotionType.Fly:
                    case MotionType.TakeDamage: index = 5; break;
                    case MotionType.Run: index = 6; break;
                }

                return index;
            }
        }
        public bool AnimationDrawCapeFront
        {
            get
            {
                if (isDead) return true;
                if (motionType == MotionType.PickUp) return true;

                switch (direction)
                {
                    case MotionDirection.West:
                    case MotionDirection.NorthWest:
                    case MotionDirection.North:
                    case MotionDirection.NorthEast:
                    case MotionDirection.East:
                        return true;
                    case MotionDirection.SouthEast:
                    case MotionDirection.South:
                    case MotionDirection.SouthWest:
                        return false;
                    default: return false;
                }
            }
        }
        public bool AnimationDrawShieldFront
        {
            get
            {

                if (IsCombatMode)
                {
                    switch (direction)
                    {
                        case MotionDirection.North:
                        case MotionDirection.NorthEast:
                        case MotionDirection.NorthWest:
                            return false;
                        case MotionDirection.East:
                        case MotionDirection.South:
                        case MotionDirection.SouthEast:
                        case MotionDirection.West:
                        case MotionDirection.SouthWest:
                            return true;
                        default: return false;
                    }
                }
                else
                {
                    switch (direction)
                    {
                        case MotionDirection.North:
                        case MotionDirection.NorthEast:
                        case MotionDirection.East:
                        case MotionDirection.SouthEast:
                            return false;
                        case MotionDirection.South:
                        case MotionDirection.NorthWest:
                        case MotionDirection.West:
                        case MotionDirection.SouthWest:
                            return true;
                        default: return false;
                    }
                }
            }
        }
        public bool AnimationDrawWeaponFront
        {
            get
            {
                switch (direction)
                {
                    case MotionDirection.North:
                    case MotionDirection.NorthEast:
                    case MotionDirection.NorthWest:
                    case MotionDirection.West:
                        return false;
                    case MotionDirection.SouthWest:
                    case MotionDirection.South:
                    case MotionDirection.SouthEast:
                    case MotionDirection.East:
                        return true;
                    default: return false;
                }
            }
        }
        public int AnimationFrameCount
        {
            get
            {
                switch (Motion)
                {
                    case MotionType.Idle:
                    case MotionType.Move:
                    case MotionType.Run:
                    case MotionType.Bow:
                    case MotionType.Dash:
                    case MotionType.Attack:
                    case MotionType.BowAttack:
                    case MotionType.Die:
                    default:
                        return 8;
                    case MotionType.Fly:
                    case MotionType.TakeDamage:
                    case MotionType.PickUp:
                        return 4;
                    case MotionType.Dead:
                        return 8; // fix from 1
                    case MotionType.Magic:
                        return 16;
                }
            }
        }
        public bool IsDead { get { return isDead; } }
        public bool IsCorpseExploited { get { return isCorpseExploited; } set { isCorpseExploited = value; } }
        public bool IsAbilityActivated { get { return (appearance4 & 0x00F0) != 0; } }
        public bool IsInvisible { get { return (status & 0x10) != 0; } }
        public bool IsBerserked { get { return (status & 0x20) != 0; } }
        public bool IsHaste { get { return false; } }
        public bool IsFrozen { get { return (status & 0x40) != 0; } }
        public bool IsCombatMode { get { return isCombatMode; } }
        public DateTime LastLogin { get { return lastLogin; } set { lastLogin = value; } }
        public bool SafeMode { get { return safeMode; } set { safeMode = value; } }
        public bool CriticalMode;
        public bool ForceAttack { get { return forceAttack; } set { forceAttack = value; } }
        public int OffsetX { get { return offsetX; } }
        public int OffsetY { get { return offsetY; } }
        public bool HasShield { get { return (Shield != null); } }
        public List<ChatMessage> DamageHistoryStageTwo { get { return damageHistoryStageTwo; } set { damageHistoryStageTwo = value; } }
        //public List<ChatMessage> VitalHistory { get { return vitalHistory; } set { vitalHistory = value; } }
        public ChatMessage VitalHistory { get { return vitalHistory; } set { vitalHistory = value; } }
        public ChatMessage DamageHistory { get { return damageHistory; } set { damageHistory = value; } }
        public ChatMessage SpellHistory { get { return spellHistory; } set { spellHistory = value; } }
        public ChatMessage ChatHistory { get { return chatHistory; } set { chatHistory = value; } }

        public bool StarTwinkle
        {
            get
            {
                return (Inventory.SpecialAbilitiesActive.Count > 0);
            }
        }

        public bool AngelTinkle
        {
            get
            {
                return (Angel != null && Angel.EffectType == ItemEffectType.Angel);
            }
        }

        public AttackType AttackType { get { return ((CriticalMode && Criticals > 0) ? AttackType.Critical : AttackType.Normal); } }
        public LinkedList<int> InventoryDrawOrder;
        public List<Item> Warehouse { get { return warehouse; } set { warehouse = value; } }
        public bool[] MagicLearned { get { return magicLearned; } set { magicLearned = value; } }
        public Dictionary<TitleType, int> Titles { get { return titles; } set { titles = value; } }
        public int[] Skills { get { return skills; } set { skills = value; } }
        public GenderType Gender
        {
            get
            {
                switch (type)
                {
                    case 1:
                    case 2:
                    case 3:
                        return GenderType.Male;
                    case 4:
                    case 5:
                    case 6:
                        return GenderType.Female;
                    default: return GenderType.None;
                }
            }
        }

        // for level up dialog
        public int RemainderLevelUpPoints;
        public int LevelUpStrength;
        public int LevelUpDexterity;
        public int LevelUpVitality;
        public int LevelUpMagic;
        public int LevelUpIntelligence;
        public int LevelUpAgility;

        // statistics
        public int MinMeleeDamage;
        public int MaxMeleeDamage;
        public int MinMeleeDamageCritical;
        public int MaxMeleeDamageCritical;
        public double DPS
        {
            get
            {
                double avgDamage = (MinMeleeDamage + MaxMeleeDamage) / 2;
                double frames = 0;

                if (Weapon != null)
                {
                    switch (Weapon.RelatedSkill)
                    {
                        case SkillType.Archery:
                            frames = Cache.AnimationSpeeds[type, (int)AnimationType.Archery];
                            break;
                        default:
                            frames = Cache.AnimationSpeeds[type, (int)AnimationType.Attacking];
                            break;
                    }
                    frames += (status & 0x000F) * 12; // swing speed
                }
                else frames = Cache.AnimationSpeeds[type, (int)AnimationType.Attacking];

                if (IsFrozen) frames /= 2; // frozen modifier

                double hitsPerSecond = (1000 / (frames * 8));

                return avgDamage * hitsPerSecond;
            }
        }

        public double DPSCritical
        {
            get
            {
                double avgDamage = (MinMeleeDamageCritical + MaxMeleeDamageCritical) / 2;
                double frames = 0;

                if (Weapon != null)
                {
                    switch (Weapon.RelatedSkill)
                    {
                        case SkillType.Archery:
                            frames = Cache.AnimationSpeeds[type, (int)AnimationType.Archery];
                            break;
                        default:
                            frames = Cache.AnimationSpeeds[type, (int)AnimationType.Attacking];
                            break;
                    }
                    frames += (status & 0x000F) * 12; // swing speed
                }
                else frames = Cache.AnimationSpeeds[type, (int)AnimationType.Attacking];

                if (IsFrozen) frames /= 2; // frozen modifier

                double hitsPerSecond = (1000 / (frames * 8));

                return avgDamage * hitsPerSecond;
            }
        }

        public int VitalityAbsorption { get { return ((vitality + inventory.VitalityBonus) / 10); } }

        //All Things Inventory related
        public Inventory Inventory { get { return inventory; } } //All of inventory

        /// <summary>
        /// Quick access to the Weapon currently equipped.
        /// </summary>
        public Item Weapon { get { return inventory.Weapon; } }

        /// <summary>
        /// Quick access to the Shield currently equipped.
        /// </summary>
        public Item Shield { get { return inventory.Shield; } }

        /// <summary>
        /// Quick access to the Necklace currently equipped.
        /// </summary>
        public Item Necklace { get { return inventory.Necklace; } }

        /// <summary>
        /// Quick access to the Helmet currently equipped.
        /// </summary>
        public Item Helmet { get { return inventory.Helmet; } }

        /// <summary>
        /// Quick access to the Body Armour currently equipped.
        /// </summary>
        public Item BodyArmour { get { return inventory.BodyArmour; } }

        /// <summary>
        /// Quick access to the Hauberk currently equipped.
        /// </summary>
        public Item Hauberk { get { return inventory.Hauberk; } }

        /// <summary>
        /// Quick access to the Leggings currently equipped.
        /// </summary>
        public Item Leggings { get { return inventory.Leggings; } }

        /// <summary>
        /// Quick access to the Boots currently equipped.
        /// </summary>
        public Item Boots { get { return inventory.Boots; } }

        /// <summary>
        /// Quick access to the Cape currently equipped.
        /// </summary>
        public Item Cape { get { return inventory.Cape; } }

        /// <summary>
        /// Quick access to the Costume currently equipped.
        /// </summary>
        public Item Costume { get { return inventory.Costume; } }

        /// <summary>
        /// Quick access to the Angel currently equipped
        /// </summary>
        public Item Angel { get { return inventory.Angel; } }

        /// <summary>
        /// Quick access to the RightRing currently equipped 
        /// </summary>
        public Item RightRing { get { return inventory.RightRing; } }

        /// <summary>
        /// Quick access to the LeftRing currently equipped (wasn't used)
        /// </summary>
        public Item LeftRing { get { return inventory.LeftRing; } }

        /// <summary>
        /// Quick access to Dual Handed Weapon
        /// </summary>
        public Item DualHandedWeapon { get { return inventory.DualHandedWeapon; } }

        //Item sets for extra Bonuses
        public List<ItemSet> SetBonuses { get { return inventory.SetBonuses; } }


        public List<Item> Equipment
        {
            get
            {
                List<Item> equipped = new List<Item>(Globals.MaximumEquipment);
                for (int i = 0; i < Globals.MaximumEquipment; i++)
                {
                    equipped[i] = inventory[i];
                }
                return equipped;
            }
        }

        //TODO - check out what this does
        //public int[] Equipment { get { return equipment; } }

        //public Item SpecialAbilityItem { get { return inventory[specialAbilityItemIndex]; } }
        public int InventoryCount { get { return inventory.InventoryCount; } }
        public int EquipmentCount { get { return inventory.EquipmentCount; } }
        public int GoldItemIndex { get { return inventory.GoldItemIndex; } }
        public Item GoldItem { get { return inventory.GoldItem; } }
        public bool HasArrows { get { return inventory.HasArrows; } }
        public Item Arrows { get { return inventory.Arrows; } }
        public int ArrowsIndex { get { return inventory.ArrowsIndex; } }
        public int StrengthBonus { get { return inventory.StrengthBonus; } }
        public int DexterityBonus { get { return inventory.DexterityBonus; } }
        public int VitalityBonus { get { return inventory.VitalityBonus; } }
        public int MagicBonus { get { return inventory.MagicBonus; } }
        public int IntelligenceBonus { get { return inventory.IntelligenceBonus; } }
        public int AgilityBonus { get { return inventory.AgilityBonus; } }
    }
}
