﻿using Helbreath.Common;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;

namespace Client.Assets.Game
{
    /// <summary>
    /// .spr file format
    /// </summary>
    public class SpriteFile : IDisposable
    {
        private string fileName;
        private List<Sprite> sprites;

        public SpriteFile()
        {
            sprites = new List<Sprite>();
        }

        public SpriteFile(string fileName, bool fullLoad = true)
        {
            sprites = new List<Sprite>();

            if (fullLoad) Load(fileName);
            else LoadMetaData(fileName);
        }

        public void AddSprite(Sprite sprite)
        {
            sprites.Add(sprite);
        }

        public static SpriteFile Convert(LegacyPakFile legacyPak)
        {
            SpriteFile file = new SpriteFile();

            foreach (LegacySprite legacySprite in legacyPak.Sprites.Values)
            {
                Sprite s = new Sprite();
                foreach (LegacySpriteFrame legacyFrame in legacySprite.Frames)
                {
                    SpriteFrame f = new SpriteFrame();
                    f.Left = legacyFrame.Left;
                    f.Top = legacyFrame.Top;
                    f.Height = legacyFrame.Height;
                    f.Width = legacyFrame.Width;
                    f.PivotX = legacyFrame.PivotX;
                    f.PivotY = legacyFrame.PivotY;
                    s.AddFrame(f);
                }
                s.Width = legacySprite.Width;
                s.Height = legacySprite.Height;
                using (Bitmap img = (Bitmap)legacySprite.Image)
                {
                    //img.MakeTransparent(img.GetPixel(0, 0));
                    using (MemoryStream stream = new MemoryStream())
                    {
                        img.Save(stream, System.Drawing.Imaging.ImageFormat.Png);
                        s.ImageData = stream.GetBuffer();
                    }
                }

                file.AddSprite(s);
            }

            return file;
        }

        public static void ConvertAndSave(LegacyPakFile legacyPak, string fileName)
        {
            SpriteFile file = Convert(legacyPak);

            file.Save(fileName);
        }

        public void LoadMetaData(string fileName)
        {
            this.fileName = fileName;
            // values
            int fileCount = 0;
            int frameCount = 0;

            // buffers
            byte[] countBuffer = new byte[2];
            byte[] spriteBuffer = new byte[15];
            byte[] spriteFrameBuffer = new byte[12];
            byte[] startBuffer = new byte[4];

            try
            {
                using (FileStream fs = new FileStream(fileName, FileMode.Open, FileAccess.Read))
                {
                    // read first 2 bytes to get file count
                    fs.Read(countBuffer, 0, countBuffer.Length);
                    fileCount = BitConverter.ToInt16(countBuffer, 0);

                    for (int s = 0; s < fileCount; s++)
                    {
                        Sprite sprite = new Sprite();

                        // read 15 bytes of the sprite meta data
                        fs.Read(spriteBuffer, 0, spriteBuffer.Length);
                        frameCount = BitConverter.ToInt16(spriteBuffer, 0);
                        sprite.ImageLength = BitConverter.ToInt32(spriteBuffer, 2);
                        sprite.Width = BitConverter.ToInt32(spriteBuffer, 6);
                        sprite.Height = BitConverter.ToInt32(spriteBuffer, 10);
                        // 14 ignored

                        for (int sf = 0; sf < frameCount; sf++)
                        {
                            SpriteFrame frame = new SpriteFrame();

                            fs.Read(spriteFrameBuffer, 0, spriteFrameBuffer.Length);
                            frame.Left = BitConverter.ToInt16(spriteFrameBuffer, 0);
                            frame.Top = BitConverter.ToInt16(spriteFrameBuffer, 2);
                            frame.Width = BitConverter.ToInt16(spriteFrameBuffer, 4);
                            frame.Height = BitConverter.ToInt16(spriteFrameBuffer, 6);
                            frame.PivotX = BitConverter.ToInt16(spriteFrameBuffer, 8);
                            frame.PivotY = BitConverter.ToInt16(spriteFrameBuffer, 10);
                            sprite.AddFrame(frame);
                        }

                        sprites.Add(sprite);
                    }

                    for (int i = 0; i < fileCount; i++)
                    {
                        fs.Read(startBuffer, 0, startBuffer.Length);
                        sprites[i].StartLocation = BitConverter.ToInt32(startBuffer, 0) + 4;
                        fs.Position += sprites[i].ImageLength;
                    }

                    fs.Close();
                }
            }
            catch (Exception ex)
            {
                
            }
        }

        public void LoadSprite(int index)
        {
            Sprite spriteToLoad = sprites[index];
            spriteToLoad.ImageData = new byte[spriteToLoad.ImageLength];

            try
            {
                using (FileStream fs = new FileStream(fileName, FileMode.Open, FileAccess.Read))
                {
                    fs.Position = spriteToLoad.StartLocation;
                    fs.Read(spriteToLoad.ImageData, 0, spriteToLoad.ImageData.Length);
                    fs.Close();
                }

            }
            catch (Exception ex)
            {

            }
        }

        public void Load(string fileName) { Load(File.ReadAllBytes(fileName)); }
        public void Load(byte[] buffer)
        {
            int size = 0;

            //int fileCount = buffer[size];
            //size++;
            int fileCount = BitConverter.ToInt16(buffer, size);
            size += 2;
            
            int[] imageLength = new int[fileCount];
            int[] startLocations = new int[fileCount];
            for (int i = 0; i < fileCount; i++)
            {
                Sprite sprite = new Sprite();
                int frameCount = BitConverter.ToInt16(buffer, size);
                size += 2;
                imageLength[i] = BitConverter.ToInt32(buffer, size);
                size+= 4;
                sprite.Width = BitConverter.ToInt32(buffer, size);
                size += 4;
                sprite.Height = BitConverter.ToInt32(buffer, size);
                size += 4;
                startLocations[i] = buffer[size];
                size++;

                for (int f = 0; f < frameCount; f++)
                {
                    SpriteFrame frame = new SpriteFrame();
                    frame.Left =BitConverter.ToInt16(buffer, size);
                    size +=2;
                    frame.Top =BitConverter.ToInt16(buffer, size);
                    size +=2;
                    frame.Width =BitConverter.ToInt16(buffer, size);
                    size +=2;
                    frame.Height =BitConverter.ToInt16(buffer, size);
                    size +=2;
                    frame.PivotX =BitConverter.ToInt16(buffer, size);
                    size +=2;
                    frame.PivotY =BitConverter.ToInt16(buffer, size);
                    size +=2;
                    sprite.Frames.Add(frame);
                }

                sprites.Add(sprite);
            }

            for (int i = 0; i < fileCount; i++)
            {
                sprites[i].StartLocation = BitConverter.ToInt32(buffer, size);
                size += 4;
                sprites[i].ImageData = new byte[imageLength[i]];
                Buffer.BlockCopy(buffer, size, sprites[i].ImageData, 0, imageLength[i]);
                size += imageLength[i];
            }
        }

        public void Save(string fileName)
        {
            int size = 0; 
            int[] startLocations = new int[sprites.Count];
            byte[] buffer = new byte[102400000];

            //buffer[size] = (byte)sprites.Count;
            Buffer.BlockCopy(sprites.Count.GetBytes(), 0, buffer, size, 2);
            size+=2;

            int index = 0;
            foreach (Sprite sprite in sprites)
            {
                Buffer.BlockCopy(sprite.Frames.Count.GetBytes(), 0, buffer, size, 2);
                size += 2;
                Buffer.BlockCopy(sprite.ImageData.Length.GetBytes(), 0, buffer, size, 4);
                size+=4;
                Buffer.BlockCopy(sprite.Width.GetBytes(), 0, buffer, size, 4);
                size += 4;
                Buffer.BlockCopy(sprite.Height.GetBytes(), 0, buffer, size, 4);
                size += 4;
                startLocations[index] = size; // START LOCATION BYTE TO BE DONE LATER
                size++;

                foreach (SpriteFrame frame in sprite.Frames)
                {
                    Buffer.BlockCopy(frame.Left.GetBytes(), 0, buffer, size, 2);
                    size+=2;
                    Buffer.BlockCopy(frame.Top.GetBytes(), 0, buffer, size, 2);
                    size+=2;
                    Buffer.BlockCopy(frame.Width.GetBytes(), 0, buffer, size, 2);
                    size+=2;
                    Buffer.BlockCopy(frame.Height.GetBytes(), 0, buffer, size, 2);
                    size+=2;
                    Buffer.BlockCopy(frame.PivotX.GetBytes(), 0, buffer, size, 2);
                    size+=2;
                    Buffer.BlockCopy(frame.PivotY.GetBytes(), 0, buffer, size, 2);
                    size+=2;
                }
                index++;
            }

            index = 0;
            foreach (Sprite s in sprites)
            {
                // SET THE START LOCATION EARLIER IN THE FILE FOR QUICK ACCESS
                Buffer.BlockCopy(size.GetBytes(), 0, buffer, size, 4);
                size+=4;
                Buffer.BlockCopy(s.ImageData, 0, buffer, size, s.ImageData.Length);
                size += s.ImageData.Length;

                index++;
            }

            using (FileStream stream = new FileStream(fileName, FileMode.Create))
            {
                stream.Write(buffer, 0, size);
            }
        }

        public List<Sprite> Sprites
        {
            get { return sprites; }
        }

        public Sprite this[int index]
        {
            get { return sprites[index]; }
        }

        public void Dispose()
        {
            foreach (Sprite s in sprites)
            {
                s.ImageData = null;
                if (s.Image != null)
                    s.Image.Dispose();
            }
        }
    }

    public class Sprite
    {
        private List<SpriteFrame> frames;
        private byte[] imageData;
        private int startLocation;
        private int width;
        private int height;

        private int imageLength;
        private Image image;
        private bool imageLoaded;

        public Sprite()
        {
            frames = new List<SpriteFrame>();
        }

        public void AddFrame(SpriteFrame frame)
        {
            frames.Add(frame);
        }

        public List<SpriteFrame> Frames
        {
            get { return frames; }
        }

        public int StartLocation
        {
            get { return startLocation; }
            set { startLocation = value; }
        }

        public int ImageLength { get { return imageLength; } set { imageLength = value; } }
        public byte[] ImageData
        {
            get { return imageData; }
            set { imageData = value; imageLoaded = false; }
        }

        public bool ImageLoaded { get { return imageLoaded; } }

        public void LoadImage()
        {
            try
            {
                using (MemoryStream stream = new MemoryStream(imageData))
                {
                    Bitmap bitmap = new Bitmap(stream);
                    this.image = (Image)bitmap;       
                }
                imageLoaded = true;
            }
            catch
            {
                imageLoaded = false;
            }
        }

        public Image GetFrame(int frameIndex, bool showFrames)
        {
            if (imageLoaded)
            {
                SpriteFrame frame = frames[frameIndex];

                Rectangle frameBox = new Rectangle(frame.Left, frame.Top, frame.Width, frame.Height);

                Bitmap frameImage = new Bitmap(frame.Width * 2, frame.Height * 2);
                Graphics g = Graphics.FromImage(frameImage);

                g.DrawImage(image, Math.Abs(frames[0].PivotX) + frame.PivotX, Math.Abs(frames[0].PivotY) + frame.PivotY, frameBox, GraphicsUnit.Pixel);

                if (showFrames)
                {
                    
                }

                return (Image)frameImage;
            }
            else return null;
        }

        public Image Image { get { if (imageLoaded) return image; else return null; } }
        public Image ImageWithFrames
        {
            get
            {
                if (imageLoaded)
                {
                    Bitmap framedImage = new Bitmap(this.image);
                    Graphics g = Graphics.FromImage(framedImage);

                    int index = 0;
                    foreach (SpriteFrame frame in frames)
                    {
                        g.DrawRectangle(new Pen(Color.Red), new Rectangle(frame.Left, frame.Top, frame.Width, frame.Height));
                        g.DrawString(index.ToString(), new Font(FontFamily.GenericSansSerif, 10, FontStyle.Bold), Brushes.Red, frame.Left + 3, frame.Top + 3);
                        index++;
                    }

                    return (Image)framedImage;
                }
                else return null;
            }
        }

        public int AnimationWidth
        {
            get
            {
                int ret=0;
                foreach (SpriteFrame frame in frames)
                    ret = Math.Max(ret, frame.Width);
                return ret;
            }
        }

        public int AnimationHeight
        {
            get
            {
                int ret=0;
                foreach (SpriteFrame frame in frames)
                    ret = Math.Max(ret, frame.Height);

                return ret;
            }
        }

        public int Width { get { return width; } set { width = value; } }
        public int Height { get { return height; } set { height = value; } }
    }

    public struct SpriteFrame
    {
        public int Left;
        public int Top;
        public int Width;
        public int Height;
        public int PivotX;
        public int PivotY;
    }

    public class SpriteLoad
    {
        public SpriteType Type;
        public string Name;
        public int Count;
        public int StartIndex;
        public int Id;

        public SpriteLoad(SpriteType type, string name, int id, int count, int startIndex)
        {
            this.Type = type;
            this.Name = name;
            this.Id = id;
            this.Count = count;
            this.StartIndex = startIndex;
        }
    }
}
