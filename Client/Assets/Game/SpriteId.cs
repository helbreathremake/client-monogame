﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Client.Assets.Game
{
    public enum SpriteId
    {
        None = -1,
        Mouse = 0,
        SpriteFont1 = 22,
        SpriteFont2 = 28,
        Font1 = 30,
        Font2 = 31,
        MaleBody = 800,
        MaleArms = 1300,
        MaleLegs = 1500,
        MaleBoots = 1700,
        MaleCape = 1800,
        MaleHead = 2100,
        MaleUndies = 2400,
        MaleWeapon = 2600,
        MaleShield = 5500,
        MaleHair = 7500,
        FemaleBody = 9800,
        FemaleArms = 10300,
        FemaleLegs = 10500,
        FemaleBoots = 10700,
        FemaleCape = 10800,
        FemaleHead = 11000,
        FemaleUndies = 11400,
        FemaleWeapon = 11600,
        FemaleShield = 14500,
        FemaleHair = 16500,
        EquipmentPack = 17500,
        Monster = 20000,
        ItemGround = 25000,
        ItemBag = 25500,
        Interface1 = 26000,
        Interface2 = 26001,
        Interface3 = 26002,
        Interface4 = 26003,
        InterfaceCrusade = 26004,
        InterfaceIconPanel = 26006,
        InterfaceInventory = 26007,
        InterfaceCharacterSelect = 26008,
        InterfaceNewCharacter = 26009,
        InterfaceTrading = 26010,
        InterfaceText = 26011,
        InterfaceButton = 26012,
        Interface5 = 26013,
        Interface6 = 26014,
        InterfaceMiniMap = 26100,
        InterfaceMiniMapMarker = 26101,
        DialogsV2 = 26200, // by kinger
        DynamicObject = 27000,
        Angels = 28000,
        Blood = 29000,
        BloodLarge = 29001,
        ScreenEffect = 29002,
    }
}
