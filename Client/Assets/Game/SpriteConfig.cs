﻿using Helbreath.Common;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Xml;

namespace Client.Assets.Game
{
    public class SpriteConfig
    {
        private string name;
        private SpriteType type;
        private SpriteId id;
        private int range;
        private int count;
        private int startIndex;
        private int appearance;

        public SpriteConfig(string name)
        {
            this.name = name;
        }

        public void ParseData(XmlTextReader reader)
        {
            Enum.TryParse<SpriteType>(reader["Type"].ToString(), out type);
            Enum.TryParse<SpriteId>(reader["Id"].ToString(), out id);
            if (reader["Range"] != null)
                Int32.TryParse(reader["Range"].ToString(), out range);
            if (reader["Count"] != null)
                Int32.TryParse(reader["Count"].ToString(), out count);
            if (reader["StartIndex"] != null)
                Int32.TryParse(reader["StartIndex"].ToString(), out startIndex);
            else startIndex = 0;
            if (reader["Appearance"] != null)
                Int32.TryParse(reader["Appearance"].ToString(), out appearance);
        }

        public static SpriteConfig Parse(XmlTextReader reader)
        {
            SpriteConfig config = new SpriteConfig(reader["Name"].ToString());
            config.ParseData(reader);
            return config;
        }

        public string Name { get { return name; } }
        public SpriteType Type { get { return type; } }
        public SpriteId Id { get { return id; } }
        public int Range { get { return range; } }
        public int Count { get { return count; } }
        public int StartIndex { get { return startIndex; } }
        public int Appearance { get { return appearance; } }
    }
}
