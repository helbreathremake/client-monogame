﻿using System;
using System.Collections.Generic;
using Helbreath.Common;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Client.Assets
{
    public class Weather
    {
        private WeatherType type;
        private List<WeatherObject> weatherObjects;
        private DateTime animationTime;
        private bool removeFlag; // flag for removal - TODO wait till all objects have completed

        public Weather(WeatherType type)
        {
            this.type = type;
            weatherObjects = new List<WeatherObject>();
        }

        public void Init()
        {
            switch (type)
            {// TODO rain sound and christmas music
                case WeatherType.Rain:
                    for (int i = 0; i < Globals.MaximumWeatherObjects/5; i++)
                        weatherObjects.Add(new WeatherObject(1, 1, 1, -1 * (Dice.Roll(1, 40))));
                    break;
                case WeatherType.RainMedium:
                    for (int i = 0; i < Globals.MaximumWeatherObjects/2; i++)
                        weatherObjects.Add(new WeatherObject(1, 1, 1, -1 * (Dice.Roll(1, 40))));
                    break;
                case WeatherType.RainHeavy:
                    for (int i = 0; i < Globals.MaximumWeatherObjects; i++)
                        weatherObjects.Add(new WeatherObject(1, 1, 1, -1 * (Dice.Roll(1, 40))));
                    break;
                case WeatherType.Snow:
                    for (int i = 0; i < Globals.MaximumWeatherObjects/5; i++)
                        weatherObjects.Add(new WeatherObject(1, 1, 1, -1 * (Dice.Roll(1, 40))));
                    break;
                case WeatherType.SnowMedium:
                    for (int i = 0; i < Globals.MaximumWeatherObjects/2; i++)
                        weatherObjects.Add(new WeatherObject(1, 1, 1, -1 * (Dice.Roll(1, 40))));
                    break;
                case WeatherType.SnowHeavy:
                    for (int i = 0; i < Globals.MaximumWeatherObjects; i++)
                        weatherObjects.Add(new WeatherObject(1, 1, 1, -1 * (Dice.Roll(1, 40))));
                    break;
            }
        }

        public void Update()
        {
            if ((DateTime.Now - animationTime).Milliseconds > 30)
            {
                int step;
                switch (type)
                {
                    case WeatherType.Rain:
                    case WeatherType.RainMedium:
                    case WeatherType.RainHeavy:
                        for (int i = 0; i < weatherObjects.Count; i++)
                        {
                            weatherObjects[i].Step++;
                            if (weatherObjects[i].Step >= 0 && weatherObjects[i].Step < 20)
                            {
                                step = Math.Max(40 - weatherObjects[i].Step, 0);
                                weatherObjects[i].Y += step;
                                if (step != 0) weatherObjects[i].X--;
                            }
                            else if (weatherObjects[i].Step >= 25)
                            {
                                // TODO - handle weather = null half way through
                                //weatherObjects[i].X = (pivotX * 32) + (Dice.Roll(1, 940) - 200) + 300;
                                //weatherObjects[i].Y = (pivotY * 32) + (Dice.Roll(1, 800) - 600) + 240;
                                weatherObjects[i].X = Dice.Roll(1, Cache.DefaultState.Display.ResolutionWidth);
                                weatherObjects[i].Y = Dice.Roll(1, Cache.DefaultState.Display.ResolutionHeight)-100;
                                weatherObjects[i].Step = -1 * (Dice.Roll(1, 10));
                            }
                        }
                        break;
                    case WeatherType.Snow:
                    case WeatherType.SnowMedium:
                    case WeatherType.SnowHeavy:
                        for (int i = 0; i < weatherObjects.Count; i++)
                        {
                            weatherObjects[i].Step++;
                            if (weatherObjects[i].Step >= 0 && weatherObjects[i].Step < 80)
                            {
                                step = Math.Max(80 - weatherObjects[i].Step, 0) / 10;
                                weatherObjects[i].Y += step;

                                if (step != 0) weatherObjects[i].X += 1 - (Dice.Roll(1, 3));

                                // TODO - snow on lower bar
                            }
                            else if (weatherObjects[i].Step >= 80)
                            {
                                // TODO - handle weather = null half way through
                                //weatherObjects[i].X = (pivotX * 32) + (Dice.Roll(1, 940) - 200) + 300;
                                //weatherObjects[i].Y = (pivotY * 32) + (Dice.Roll(1, 800) - 600) + 240;
                                weatherObjects[i].X = Dice.Roll(1, Cache.DefaultState.Display.ResolutionWidth);
                                weatherObjects[i].Y = Dice.Roll(1, Cache.DefaultState.Display.ResolutionHeight) - 100;
                                weatherObjects[i].Step = -1 * (Dice.Roll(1, 10));
                                weatherObjects[i].BarX = 0;
                            }
                        }
                        break;
                }

                animationTime = DateTime.Now;
            }
        }

        public void Draw(SpriteBatch spriteBatch, int offsetX, int offsetY)
        {
            switch (type)
            {
                case WeatherType.Rain:
                case WeatherType.RainMedium:
                case WeatherType.RainHeavy:
                    for (int i = 0; i < weatherObjects.Count; i++)
                        if (weatherObjects[i].Step >= 0 && weatherObjects[i].Step < 20)
                            SpriteHelper.DrawEffect(spriteBatch, 11, (16 + (weatherObjects[i].Step / 6)), weatherObjects[i].X, weatherObjects[i].Y, -offsetX, -offsetY, Color.White);
                        else if (weatherObjects[i].Step >= 25)
                            SpriteHelper.DrawEffect(spriteBatch, 11, weatherObjects[i].Step, weatherObjects[i].X, weatherObjects[i].Y, -offsetX, -offsetY, Color.White);
                    break;
                case WeatherType.Snow:
                case WeatherType.SnowMedium:
                case WeatherType.SnowHeavy:
                    for (int i = 0; i < weatherObjects.Count; i++)
                        if (weatherObjects[i].Step >= 0 && weatherObjects[i].Step < 80)
                            SpriteHelper.DrawEffect(spriteBatch, 11, (39 + (weatherObjects[i].Step / 20)*3 + Dice.Roll(1,3)), weatherObjects[i].X, weatherObjects[i].Y, -offsetX, -offsetY, Color.White);

                    // TODO - snow on HUD bar
                    break;
            }
        }

        public WeatherType Type { get { return type; } }
    }

    public class WeatherObject
    {
        public int X;
        public int Y;
        public int BarX; // old code put snow on the HUD bar
        public int Step;
        public WeatherObject(int x, int y, int bX, int step)
        {
            this.X = x;
            this.Y = y;
            this.BarX = bX;
            this.Step = step;
        }
    }
}
