using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using Helbreath.Common;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace Client.Assets
{

    public class MapTile
    {
        public DynamicObject DynamicObject;
        public IOwner DeadOwner;
        private IOwner owner;
        public IOwner Owner
        {
            get { return owner; }
            set { owner = value; }
        }

        public bool IsOccupied
        {
            get { return (Owner != null); }
        }

        public bool hasOwnerTarget;
        public int Sprite, SpriteFrame, ObjectSprite, ObjectSpriteFrame, ItemSprite, ItemSpriteFrame, ItemSpriteColour;
        public GameColor ItemColorType = GameColor.None;
        private bool isTeleport;
        private bool isMoveAllowed;
        private bool isFarmingAllowed;
        private bool isWater;
        private bool isSafe;

        public MapTile()
        {
            Sprite = -1;
            SpriteFrame = -1;
            ObjectSprite = -1;
            ObjectSpriteFrame = -1;
            ItemSprite = -1;
            ItemSpriteFrame = -1;
        }

        public bool Parse(byte[] data)
        {
            Sprite = BitConverter.ToInt16(data, 0);
            SpriteFrame = BitConverter.ToInt16(data, 2);
            ObjectSprite = BitConverter.ToInt16(data, 4);
            ObjectSpriteFrame = BitConverter.ToInt16(data, 6);


            if ((data[8] & 0x80) != 0)
                isMoveAllowed = false;
            else isMoveAllowed = true;

            if ((data[8] & 0x40) != 0)
                isTeleport = true;
            else isTeleport = false;

            if ((data[8] & 0x20) != 0)
                isFarmingAllowed = false;
            else isFarmingAllowed = true;

            if ((int)data[0] == 19)
                isWater = true;
            else isWater = false;



            return true;
        }
        public bool IsMoveAllowed { get { return (isMoveAllowed && Owner == null && (DynamicObject == null || DynamicObject.IsTraversable)); } }
        public bool IsTeleport { get { return isTeleport; } }
        public bool HasTargetOwner { get { return hasOwnerTarget; } set { hasOwnerTarget = value; } }
    }

    public class MapRow
    {
        public List<MapTile> Tiles;

        public void AddTile(MapTile tile)
        {
            Tiles.Add(tile);
        }

        public MapTile this[int x]
        {
            get { return Tiles[x]; }
        }
    }

    /// <summary>
    /// This is a game component that implements IUpdateable.
    /// </summary>
    public class Map
    {

        private int sizeX, sizeY, tileSize;
        private List<MapRow> rows;

        public Map()
        {
            rows = new List<MapRow>();
        }

        public bool Load(string amdFileName)
        {
            if (!File.Exists(amdFileName)) return false;
            FileStream reader = new FileStream(amdFileName, FileMode.Open, FileAccess.Read);

            byte[] headerBuffer = new byte[256];
            reader.Read(headerBuffer, 0, 256);
            string[] header = Encoding.ASCII.GetString(headerBuffer).Replace('\0', ' ').Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);

            // loads the map header which defines the width, height and tile size of the map (in bytes)
            for (int i = 0; i < header.Length; i++)
                switch (header[i])
                {
                    case "MAPSIZEX":
                        this.sizeX = Int32.Parse(header[i + 2]);
                        break;
                    case "MAPSIZEY":
                        this.sizeY = Int32.Parse(header[i + 2]);
                        break;
                    case "TILESIZE":
                        this.tileSize = Int32.Parse(header[i + 2]);
                        break;
                }

            // loads the tile information from the .amd file and creates a grid of maprow and maptile objects.
            int tileCount = 0;
            for (int y = 0; y < sizeY; y++)
            {
                MapRow row = new MapRow();
                row.Tiles = new List<MapTile>();
                for (int x = 0; x < sizeX; x++)
                {
                    MapTile tile = new MapTile();

                    byte[] tileBuffer = new byte[tileSize];
                    reader.Read(tileBuffer, 0, tileSize);
                    if (tile.Parse(tileBuffer)) row.AddTile(tile);
                    tileCount++;
                }
                rows.Add(row);
            }

            

            return true;
        }

        public bool LocationExists(int x, int y)
        {
            if (x < 0) return false;
            if (y < 0) return false;
            if (y > rows.Count) return false;
            if (x > rows[y].Tiles.Count) return false;

            return true;
        }

        public bool LocationMovable(int x, int y)
        {
            if (rows[y][x].IsMoveAllowed) return true;
            else return false;
        }


        public bool NextLocationIsDestination(MotionDirection direction, int currentX, int currentY, int destinationX, int destinationY)
        {
            int x = currentX;
            int y = currentY;

            switch (direction)
            {
                case MotionDirection.North: y--; break;
                case MotionDirection.NorthEast: y--; x++; break;
                case MotionDirection.East: x++; break;
                case MotionDirection.SouthEast: x++; y++; break;
                case MotionDirection.South: y++; break;
                case MotionDirection.SouthWest: y++; x--; break;
                case MotionDirection.West: x--; break;
                case MotionDirection.NorthWest: x--; y--; break;
            }

            if (x == destinationX && y == destinationY) return true;

            return false;
        }

        public bool LocationCanMove(MotionDirection direction, int x, int y)
        {
            switch (direction)
            {
                case MotionDirection.North: y--; break;
                case MotionDirection.NorthEast: y--; x++; break;
                case MotionDirection.East: x++; break;
                case MotionDirection.SouthEast: x++; y++; break;
                case MotionDirection.South: y++; break;
                case MotionDirection.SouthWest: y++; x--; break;
                case MotionDirection.West: x--; break;
                case MotionDirection.NorthWest: x--; y--; break;
            }

            if (x < 0 || x > this.Width) return false;
            if (y < 0 || y > this.Height) return false;

            if (LocationMovable(x,y)) return true;
            return false;
        }

        public List<MapRow> Rows { get { return rows; } }
        public MapRow this[int index] { get { return rows[index]; } }
        public int Width { get { return sizeX; } }
        public int Height { get { return sizeY; } }

        public string MusicName;
        public bool MiniMapComplete;
        public Texture2D MiniMap;
        public Texture2D ZoomMap;
    }
}