﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Helbreath.Common;
using Microsoft.Xna.Framework;

namespace Client.Assets
{
    public class ChatMessage
    {
        private GameColor color;
        private DateTime messageTime;
        private string message;
        private UInt16 ownerId;
        private string ownerName;
        private int ownerX;
        private int ownerY;
        private FontType font;
        private float transparency;
        private float x;
        private float y;
        private int animationTime = 1;
        private MotionDirection direction;
        private float scale = 1.0f;
        private float velocity;

        public ChatMessage()
        {
            color = GameColor.None;
            transparency = 1.0f;
            x = 0;
            y = 0;
            animationTime = 1;
            int diceRoll = Dice.Roll(1, 3);
            direction = MotionDirection.North;
            velocity = Dice.Roll(1, 100) / 100;
            scale = 1.5f;
            message = "";
        }

        public ChatMessage(DateTime messageTime)
        {
            this.messageTime = messageTime;
            color = GameColor.None;
            transparency = 1.0f;
            x = 0;
            y = 0;
            animationTime = 1;
            direction = MotionDirection.North;
            velocity = Dice.Roll(1, 100) / 100;
            scale = 1.5f;
            message = "";
        }

        public void Clear()
        {
            color = GameColor.None;
            transparency = 1.0f;
            x = 0;
            y = 0;
            animationTime = 1;
            velocity = Dice.Roll(1, 100) / 100;
            direction = MotionDirection.North;
            scale = 1.5f;
            message = "";
        }

        public void StageTwo()
        {
            x = 0;
            y = -15;
            animationTime = 1;
            scale = 1.0f;
            transparency = 0.5f;
            //velocity = Dice.Roll(1, 100) / 100;
            //int roll = Dice.Roll(1, 3);
            //switch (roll)
            //{
            //    case 1:
            //        x -= Dice.Roll(1, 20);
            //        direction = MotionDirection.NorthWest; break;
            //    case 2:
            //        x = (Dice.Roll(1, 2) == 1) ? - Dice.Roll(1, 10) : + Dice.Roll(1, 10);
            //        direction = MotionDirection.North; break;
            //    case 3:
            //        x += Dice.Roll(1, 20);
            //        direction = MotionDirection.NorthEast; break;
            //    default:
            //        direction = MotionDirection.South; break;
            //}
        }

        public DateTime MessageTime { get { return messageTime; } set { messageTime = value;  } }
        public String Message { get { return message; } set { message = value; } }
        public UInt16 OwnerId { get { return ownerId; } set { ownerId = value; } }
        public String OwnerName { get { return ownerName; } set { ownerName = value; } }
        public int OwnerX { get { return ownerX; } set { ownerX = value; } }
        public int OwnerY { get { return ownerY; } set { ownerY = value; } }
        public GameColor Color { get { return color; } set { color = value; } }
        public FontType Font { get { return font; } set { font = value; } }
        public float Transparency { get { return transparency; } set { transparency = value; } }
        public float X { get { return x; } set { x = value; } }
        public float Y { get { return y; } set { y = value; } }
        public int AnimationTime { get { return animationTime; } set { animationTime = value; } }
        public MotionDirection Direction { get { return direction; } set { direction = value; } }
        public float Scale { get { return scale; } set { scale = value; } }
        public float Velocity { get { return velocity; } set { velocity = value; } }
    }
}
